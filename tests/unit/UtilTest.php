<?php

use Codeception\Test\Unit;

class UtilTest extends Unit {
    /** @var DBX $db */
    private $db;

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @throws
     */
    protected function _before() {
        $testdbconfig = require(dirname(__DIR__) . '/dbconfig.php');
        $this->db = Component::create($testdbconfig);
        UApp::setLang('hu');
        UApp::$config = [
            'baseurl' => 'https://test.uhisoft.hu/app/',
            'uapp' => [
                'path' => dirname(dirname(__DIR__)),
            ],
        ];
        L10nBase::init([]);
    }


    function test_tonameid() {
        $data = [
            ['', ''],
            ['alma', 'alma'],
            ['alma há mér', 'alma_ha_mer'],
            ['alma há_mé', 'alma-ha-me', '-'],
            ['alma há_mé', 'alma-ha_me', '-', '_'],
            ['egyéb', 'egyeb'],
        ];

        for ($i = 0; $i < count($data); $i++) {
            $d = $data[$i];
            $def = isset($d[2]) ? $d[2] : null;
            $ena = isset($d[3]) ? $d[3] : null;

            $r = Util::toNameID($d[0], $def, $ena);
            $this->assertEquals($d[1], $r, "Case $i: %s");
        }

        $ena = '_';
        $ena = mb_ereg_replace('(.)', '\\\\1', $ena);
        $this->assertEquals($ena, '\\_', 'Ena: %s');
    }

    function test_camelize() {
        $this->assertEquals('cASE', Util::camelize('CASE'));
        $this->assertEquals('c_a_s_e', Util::uncamelize('cASE'));

        $data = [ // $input, $separator, $pascal, $expected
            ['alma', null, false, 'alma'],
            ['case', null, true, 'Case'],
            ['alma_ata', null, false, 'almaAta'],
            ['alma_ata', '_', true, 'AlmaAta'],
            ['alma-ata-c', '-', true, 'AlmaAtaC'],
        ];
        for ($i = 0; $i < count($data); $i++) {
            list($input, $separator, $pascal, $expected) = $data[$i];

            $result = Util::camelize($input, $pascal);
            $this->assertEquals($expected, $result);
            $result = $separator ? Util::uncamelize($result, $separator) : Util::uncamelize($result);
            $this->assertEquals($input, $result);
        }
    }

    function test_endate() {
        $d = '2017.10.23.';
        $r = L10n::formatDate(Util::createDateTime($d), IntlDateFormatter::SHORT, IntlDateFormatter::NONE, 'en');
        $this->assertEquals('10/23/17', $r);
    }

    function test_substitute() {
        $app = UApp::hasInstance();
        $d = '2017.10.23.';
        $e = '23/10/2017';
        $dt = Util::createDateTime($d);
        L10nBase::formatDate($dt, IntlDateFormatter::SHORT, IntlDateFormatter::NONE, 'en-GB');
        $d = UApp::df($d, IntlDateFormatter::SHORT, IntlDateFormatter::NONE, 'en-GB');
        $this->assertEquals($e, $d);

        $data = [ // $data, $input, $expected
            [['egy' => 'körte', 'egyes' => 'alma'], 'Ez egy {$egyes} és egy {$egy}.', 'Ez egy alma és egy körte.'],
            [['egy' => 'körte', 'db' => ['egy' => 'banán']], 'Ez egy {$db/egy}!', 'Ez egy banán!'],
            [['egy' => 'körte', 'db' => ['egy' => 'banán']], 'Ez egy {$sem}, {$db/sem}!', 'Ez egy {$sem}, {$db/sem}!'],
            [['egy' => '2017.10.23.'], 'Expired on {DMY$egy}.', 'Expired on 23/10/2017.'],
            [['egy' => '2017.10.23.'], 'Expired on {MDY$egy}.', 'Expired on 10/23/17.'],
            [['egy' => new DateTime('2017-10-23')], 'Lejárt {$egy}-án', 'Lejárt 2017.10.23.-án'],
        ];
        UApp::setLang('hu-HU');
        for ($i = 0; $i < count($data); $i++) {
            list($dd, $input, $expected) = $data[$i];

            $result = Util::substitute($input, $dd);
            $this->assertEquals($expected, $result, "Case $i/A");
        }
    }

    function test_canonRegex() {
        $data = [ // $fsre, $expected
            ['^[\w]+(?#alm?a)|b{1,2}$', '^[\w]+|b{1,2}$'],
            ['a l m
			a', 'alma'],
            ['sor|     # comment
			sor2
			', 'sor|sor2'],
        ];
        for ($i = 0; $i < count($data); $i++) {
            list($fsre, $expected) = $data[$i];
            $result = Util::canonRegex($fsre);
            $this->assertEquals($expected, $result, "Case $i/A: %s");
        }
    }

    function test_createUrl() {
        $data = [ // $from, $to, $expected
            ['', null, ''],
            ['', '/', '/'],
            ['dom', '23', 'dom/23'],
            ['/', ['login'], '/login'],
            ['alap', ['login'], 'alap/login'],
            ['/alap', ['login'], '/alap/login'],
            ['', ['login'], 'login'],
            ['http://rr.pte.hu', [], 'http://rr.pte.hu'],
            ['http://rr.pte.hu/', [], 'http://rr.pte.hu/'],
            ['http://rr.pte.hu', ['login'], 'http://rr.pte.hu/login'],
            ['http://rr.pte.hu/', ['login'], 'http://rr.pte.hu/login'],
            ['http://rr.pte.hu/dom', null, 'http://rr.pte.hu/dom/'],
            ['http://rr.pte.hu/dom', [], 'http://rr.pte.hu/dom'],
            ['http://rr.pte.hu/dom', [23], 'http://rr.pte.hu/dom/23'],
            ['http://rr.pte.hu/dom', 23, 'http://rr.pte.hu/dom/23'],
            ['http://rr.pte.hu/dom', ['alma', 'q' => 'mi'], 'http://rr.pte.hu/dom/alma?q=mi'],
            ['http://rr.pte.hu/dom', ['#' => 'erre'], 'http://rr.pte.hu/dom#erre'],
        ];
        for ($i = 0; $i < count($data); $i++) {
            list($from, $to, $expected) = $data[$i];
            $result = Util::createUrl($from, $to);
            #$this->compare($expected, $result);
            $this->assertEquals($expected, $result, "Case $i/A: %s");
        }
    }

    /**
     * @dataProvider provider_createDateTime
     *
     * @param $source
     * @param $expected
     *
     * @throws InternalException
     */
    function test_createDateTime($source, $expected) {
        /** @var DateTime $result */
        $result = Util::createDateTime($source);
        $this->assertEquals($expected, $result->format('Y-m-d H:i:s'));
    }

    function provider_createDateTime() {
        return [
            ['2018.03.15', '2018-03-15 00:00:00'],
            ['2018.03.15.', '2018-03-15 00:00:00'],
            ['2018. 03. 15.', '2018-03-15 00:00:00'],
            ['2018. 03. 15.', '2018-03-15 00:00:00'],
            ['2018.03.15 0:10:01', '2018-03-15 00:10:01'],
            ['2018.03.15 0:10', '2018-03-15 00:10:00'],
            ['2018.03.15. 0:10:59', '2018-03-15 00:10:59'],
            ['2018.03.15. 23:10', '2018-03-15 23:10:00'],
            ['2018. 03. 15. 1:10:59', '2018-03-15 01:10:59'],
            ['2018. 03. 15. 6:30', '2018-03-15 06:30:00'],
            ['2018-03-15', '2018-03-15 00:00:00'],
        ];
    }

    /**
     * @dataProvider provider_toString
     *
     * @param mixed $obj
     * @param string $expected
     *
     * @throws InternalException
     * @throws ConfigurationException
     */
    function test_toString($obj, $locale, $expected) {
        if ($obj === 'x') {
            $this->db->literal('a'); // force connect
            $obj = $this->db->connection;
        }
        $result = Util::toString($obj, $locale);
        $this->assertSame($expected, $result);
    }

    /**
     * @return array
     * @throws InternalException
     */
    function provider_toString() {
        return [
            [new DateTime('2018-03-15'), null, '2018-03-15'],
            [new DateTime('2018-03-15'), 'hu', '2018.03.15.'],
            [new DateTime('2018-03-15T12:31'), 'hu', '2018.03.15. 12:31'],
            ['Kalap', null, 'Kalap'],
            [['alma', 'körte'], null, '[alma, körte]'],
            [['a' => 12, 'b' => new DateTime('2018-03-15')], null, '[a:12, b:2018-03-15]'],
            [['a' => 12, 'b' => new DateTime('2018-03-15')], 'hu', '[a:12, b:2018.03.15.]'],
            [new Inet('192.168.1.1'), null, '192.168.1.1'],
            [null, null, 'not set'],
            [true, null, 'true'],
            [false, null, 'false'],
            [null, 'hu', 'nem beállított'],
            [true, 'hu', 'igaz'],
            [false, 'hu', 'hamis'],
            [19, null, '19'],
            [new MacAddr('aabbccddeeff'), null, 'AA:BB:CC:DD:EE:FF'],
            [new DOMDocument(), null, 'DOMDocument'],
//            ['x', 'hu', 'resource'], // After php8, 'PgSql\Connection'
        ];
    }

    /**
     * @dataProvider provider_relativePath
     * @throws Exception
     */
    function test_relativePath($from, $to, $expected) {
        $result = Util::getRelativePath($from, $to);
        $this->assertEquals($expected, $result);
    }

    function provider_relativePath() {
        return [
            // $from, $to, $expected
            ['/home/a', '/home/rot/b', './rot/b'],
            ['/home/apache/a/', '/home/root/b/b', '../../root/b/b'],

            ['foo', 'foo', ''],
            ['foo/bar', 'foo', ''],
            ['foo/bar', 'foo/', './'],

            ['foo', 'foo/bar', 'bar'],

            ['/kalap/', '/bela', '../bela'],
            ['kalap', 'bela', './bela'],
            ['c://kalap/', 'c://bela/', '../bela/'],
            ['c://kalap/', 'c://bela', '../bela'],
            ['c://kalap/bela', 'c://bela', '../bela'],
        ];
    }

    /**
     * @dataProvider provCoalesce
     * @param $arg1
     * @param $arg2
     * @param $arg3
     * @param $expected
     */
    function testCoalesce($arg1, $arg2, $arg3, $expected) {
        $this->assertSame($expected, Util::coalesce($arg1, $arg2, $arg3));
    }

    function provCoalesce() {
        return [ // $arg1, $arg2, $arg3, $expected
            [1, 2, 3, 1],
            [0, 2, 3, 2],
            [true, 2, 3, true],
            [false, [], [null], [null]],
            [false, [], null, false],
        ];
    }

    /**
     * @dataProvider provStartsWith
     * @param string $text
     * @param string $pattern
     * @param bool $cs
     * @param bool $expected
     */
    function testStartsWith($text, $pattern, $cs, $expected) {
        $this->assertSame($expected, Util::startsWith($text, $pattern, $cs));
    }

    function provStartsWith() {
        return [
            ['alma', 'al', false, true],
            ['alma', 'AL', true, false],
            ['alma', '', false, true],
            ['', '', false, true],
        ];
    }

    /**
     * @dataProvider provEndsWith
     * @param $arg1
     * @param $arg2
     * @param $arg3
     * @param $expected
     */
    function testEndsWith($text, $pattern, $cs, $expected) {
        $this->assertSame($expected, Util::endsWith($text, $pattern, $cs));
    }

    function provEndsWith() {
        return [
            ['alma', 'ma', false, true],
            ['alma', 'MA', true, false],
            ['alma', '', false, true],
            ['', '', false, true],
        ];
    }

    /**
     * @dataProvider provImplode_with_keys
     * @param array|string $glue1
     * @param array|string $glue2
     * @param array $array
     * @param array|bool $resurse
     * @param string $expected
     */
    function testImplode_with_keys($glue1, $glue2, $array, $resurse, $expected) {
        $this->assertSame($expected, Util::implode_with_keys($glue1, $glue2, $array, $resurse));
    }

    function provImplode_with_keys() {
        return [ // $glue1, $glue2, $array, $resurse, $expected
            [',', '=', ['a' => 1, 'b' => new DateTime('2018-12-31')], true, 'a=1,b=2018-12-31 00:00'],
            [',', '=', ['a' => 1, 'b' => ['c' => 3]], false, 'a=1,b=Array'],
            [',', '=', ['a' => 1, 'b' => ['c' => 3]], true, 'a=1,b=[c=3]'],
            [',', '=', ['a' => 1, 'b' => ['c' => 3]], ['(', ')'], 'a=1,b=(c=3)'],
            [[',', ';'], '=', ['a' => 1, 'b' => ['c' => 3, 'd' => 4]], ['[', ']'], 'a=1,b=[c=3;d=4]'],
            [[',', ';'], ['=', ':'], ['a' => 1, 'b' => ['c' => 3, 'd' => ['e' => 5, 'f' => 6]]], ['[', ']'], 'a=1,b=[c:3;d:[e:5;f:6]]'],
            [[',', ';'], ['=', ':'], ['a' => 1, 'b' => ['c' => 3, 'd' => ['e' => 5, 'f' => 6]]], [['[', ']'], ['{', '}']], 'a=1,b=[c:3;d:{e:5;f:6}]'],
        ];
    }


    /**
     * @dataProvider prov_mb_str_split
     * @param string $string
     * @param int $len
     * @param array $expected
     */
    function test_mb_str_split($string, $len, $expected) {
        $this->assertSame($expected, Util::mb_str_split($string, $len));
    }

    function prov_mb_str_split() {
        return [ // $string, $len, $expected
            ['elkerülhetetlenül', 3, ['elk', 'erü', 'lhe', 'tet', 'len', 'ül']],
            ['zártkörű nőújító ülés', 5, ['zártk', 'örű n', 'őújít', 'ó ülé', 's']],
            ['', 0, []],
            ['', 3, []],
            ['Ő az', 0, ['Ő', ' ', 'a', 'z']],
            ['Ő az', 1, ['Ő', ' ', 'a', 'z']],
        ];
    }

    function testRandStr() {
        $len = 6;
        $ss = [];
        for ($i = 0; $i < 50; $i++) {
            $ss[$i] = Util::randStr($len);
            $this->assertEquals($len, strlen($ss[$i]));
            for ($j = 0; $j < $i; $j++) $this->assertNotEquals($ss[$i], $ss[$j], "Case $i/$j");
        }
    }

    /**
     * @dataProvider provBitString
     * @param $arg1
     * @param $arg2
     * @param $arg3
     * @param $expected
     */
    function testBitString($v, $len, $expected) {
        $this->assertSame($expected, Util::bitString($v, $len));
    }

    function provBitString() {
        return [
            [31, 8, '00011111'],
            [0, 1, '0'],
            [63, 0, ''],
            [62, 3, '110'],
            [-1, 4, '1111'],
        ];
    }

    /**
     * @throws InternalException
     * @throws Exception
     */
    function testMinor() {
        $this->assertSame('image/jpeg', Util::mimetype('/barhol/barmi.jpg'));
        $this->assertSame('E_ERROR', Util::friendlyErrorType(E_ERROR));

        $this->assertSame('/app/', Util::baseurl());

        $_SERVER['REQUEST_URI'] = 'https://test.uhisoft.hu/app/alma.html';
        $this->assertSame('/app/alma.html', Util::getUriFile());
        $_SERVER['REQUEST_URI'] = 'https://test.uhisoft.hu/app/alma.html';
        $this->assertSame('alma.html', Util::getUriFile('https://test.uhisoft.hu/app/'));
        unset($_SERVER['REQUEST_URI']);
        $this->expectException('Exception');
        $uf = Util::getUriFile();


    }

    function testGetReqMultiOr() {
        $_REQUEST = ['f' => [1, 4]];
        $this->assertSame(5, Util::getReqMultiOr('f'));
        $_REQUEST = [];
        $this->assertSame(null, Util::getReqMultiOr('f'));
        $_REQUEST = ['f' => 0];
        $this->assertSame(null, Util::getReqMultiOr('f'));
        $_REQUEST = ['f' => [0]];
        $this->assertSame(0, Util::getReqMultiOr('f'));
    }

    function testMb_strcspn() {
        $this->assertEquals(0, Util::mb_strcspn(' őrült', ' .'));
        $this->assertEquals(5, Util::mb_strcspn(' őrült', ' .', 1));
        $this->assertEquals(2, Util::mb_strcspn(' őrü. lt', ' .', 2));
        $this->assertEquals(4, Util::mb_strcspn('őrül', '. ;'));
        $this->assertEquals(0, Util::mb_strcspn('', '. ;'));
        $this->assertEquals(0, Util::mb_strcspn('', '. ;', 5, 7));
    }

    function testMb_strspn() {
        $this->assertEquals(1, Util::mb_strspn(' őrült', ' .'));
        $this->assertEquals(2, Util::mb_strspn(' őr .ült', ' .', 3, 3));
    }

    /**
     * @dataProvider provCompareString
     * @param string $a
     * @param string $b
     * @param string $expected
     * @small
     */
    function testCompareString($a, $b, $op, $expected) {
        $expected = preg_replace('~\033\[([\d;]+)m~', '[$1]', $expected);

        $opcodes = FineDiff::getDiffOpcodes($a, $b);
        $this->assertEquals($op, $opcodes);
        $result = FineDiff::renderDiffToANSIFromOpcodes($a, $opcodes);


        $result1 = preg_replace('~\033\[([\d;]+)m~', '[$1]', $result);
        $this->assertEquals($expected, $result1, "Actual: '" . Util::diff($a, $b) . "', " . Util::objtostr($result));
    }

    function provCompareString() {
        return [
            ['jó', 'jó', 'c2', 'jó'],
            ['jó', 'jó nagy', 'c2i5: nagy', 'jó' . Ansi::color('␣nagy', 'green')],
            ['egyik', 'másik', 'd3i3:másc2', Ansi::color('egy', 'red') . Ansi::color('más', 'green') . 'ik'],
            ['vége jó', 'másik jó', 'd4i5:másikc3',
                Ansi::color('vége', 'red') .
                Ansi::color('másik', 'green') .
                ' jó'],
            ['eleje jó és a vége', 'eleje rossz és a vége', 'c6d2i5:rosszc10',
                'eleje ' .
                Ansi::color('jó', 'red') .
                Ansi::color('rossz', 'green') .
                ' és a vége',
            ],
            ['eleje jó és a vége', 'eleje és a vége nem jó', 'c6d3c9i7: nem jó',
                'eleje ' .
                Ansi::color('jó ', 'red') .
                'és a vége' .
                Ansi::color('␣nem␣jó', 'green')
            ],
            ['aabbcccxxx', 'aacccbbxxx', 'c2d2c3i2:bbc3',
                'aa' .
                Ansi::color('bb', 'red') .
                'ccc' .
                Ansi::color('bb', 'green') .
                'xxx'
            ],
            ['aacccbbxxx', 'aabbcccxxx', 'c2i2:bbc3d2c3',
                'aa' .
                Ansi::color('bb', 'green') .
                'ccc' .
                Ansi::color('bb', 'red') .
                'xxx'
            ],
            ['egy kis malac röfröfröf', 'egy xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx röf kis malac', 'c4i68:xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx röf c9d10',
                'egy ' .
                Ansi::color('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx␣röf␣', 'green') .
                'kis malac' .
                Ansi::color('␣röfröfröf', 'red')
            ],
        ];
    }

    /**
     * @dataProvider provOrgPathToDN
     * @param $expected
     * @param $orgPath
     */
    function testOrgPathToDN($expected, $orgPath) {
        $this->assertEquals($expected, Util::orgPathToDN($orgPath));
    }

    function provOrgPathToDN() {
        return [
            [
                'ou=Üzemeltetési\ és\ Ügyfél-Támogatási\ Főosztály,ou=Informatikai\ Igazgatóság,ou=Kancellária,ou=Önálló\ Központi\ Igazgatási\ szervezeti\ egységek,o=Pécsi\ Tudományegyetem,c=hu',
                'Pécsi Tudományegyetem\0;Önálló Központi Igazgatási szervezeti egységek\6;Kancellária\40;Informatikai Igazgatóság\67;Üzemeltetési és Ügyfél-Támogatási Főosztály\386',
            ],
            [
                'o=,c=hu',
                '',
            ]
        ];
    }

}
