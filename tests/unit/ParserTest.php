<?php

use Codeception\Test\Unit;

class ParserTest extends Unit {
    private $syntax;
    private $parser;
    private $testdatadir;
    /**
     * @var UnitTester
     */
    protected $tester;

    protected function _before() {
        $this->testdatadir = dirname(__DIR__) . '/_data';
        $this->syntax = file_get_contents($this->testdatadir . '/dhcpconfig.xml');
        $input = <<<EOT
#PTE-KK-VERESE
host pc1.BMK.local {
	hardware ethernet 00:0c:f1:e0:3e:db;
	fixed-address 10.0.34.1;
}
EOT;
        $this->parser = new Parser_String($input, $this->syntax);
    }

    protected function _after() {
    }

    function test_parse1() {
        $c = $this->parser->getc();
        $this->assertEquals('#', $c);
        $c = $this->parser->getc();
        $this->assertEquals('P', $c);
        $this->parser->ungetc();
        $c = $this->parser->getc();
        $this->assertEquals('P', $c);
        $this->parser->ungetc();
        $this->parser->ungetc();

        $node_root = $this->parser->result->createElement('root');
        $r = $this->parser->matchRule('comment', $node_root);
        $this->assertNotNull($node_root);
        $this->assertNotNull($node_root->ownerDocument);
        $this->assertEquals(true, $r);
        $this->assertEquals('#PTE-KK-VERESE', $node_root->nodeValue);

        $res = $this->parser->getSymbol('name');
        $this->assertEquals('host', $res->nodeValue);

        $res = $this->parser->getSymbol('s1');
        $this->assertEquals('', $res->nodeValue);

        $res = $this->parser->getSymbol('dottedname');
        $this->assertEquals('pc1.BMK.local', $res->nodeValue);

        $this->parser->setPos(0);
        $res = $this->parser->getSymbol('host');
        $this->parser->result->formatOutput = false;
        $this->assertEquals(
            '<host><dottedname>pc1.BMK.local</dottedname><hostattr type="hardware">000cf1e03edb</hostattr><hostattr type="fixed-address">10.0.34.1</hostattr></host>',
            $this->parser->export($res));

        $this->parser->setPos(0);
        $res = $this->parser->apply();
        $this->parser->result->formatOutput = false;
        $this->assertEquals(
            '<hostlist><host><dottedname>pc1.BMK.local</dottedname><hostattr type="hardware">000cf1e03edb</hostattr><hostattr type="fixed-address">10.0.34.1</hostattr></host></hostlist>',
            $this->parser->export($res));
    }

    function test_parse2() {
        $fh = fopen($this->testdatadir . '/dhcpsample.txt', 'r');
        $this->parser = new Parser_File($fh, $this->syntax);
        $res = $this->parser->apply();
        $this->parser->result->formatOutput = false;
        $this->assertEquals(
            '<hostlist><host><dottedname>pc1.KK.local</dottedname><hostattr type="hardware">000cf2e33edb</hostattr><hostattr type="fixed-address">10.0.29.1</hostattr></host><host><dottedname>pc2.KK.local</dottedname><hostattr type="hardware">000df2e33edb</hostattr><hostattr type="fixed-address">10.0.29.2</hostattr></host><host><dottedname>pc3.KK.local</dottedname><hostattr type="hardware">000cf2e33fdb</hostattr><hostattr type="fixed-address">10.0.29.3</hostattr></host></hostlist>',
            $this->parser->export($res));
    }

    function test_parse_or() {
        $this->syntax = <<<EOT
<syntax>
<match name="elemlista" />

<rule name="s"><list><char>\s</char></list></rule>

<rule name="elemlista">
	<list><match name="elem" /></list>
</rule>

<rule name="elem">
	<or>
		<case><match name="fruit" /><s /><match name="number" /></case>
		<case><match name="fruit" /><s /><match name="numberlist" /></case>
		<match name="number" />
		<match name="numberlist" />
		<node name="error"><return><list><char>[^,]</char></list><opt><char>,</char></opt></return></node>
	</or>
	<opt><char>,</char></opt>
</rule>

<rule name="fruit">
	<s />
	<return><list min="1"><char>[A-Za-z_-]</char></list></return>
	<s />
</rule>

<rule name="number">
	<s />
	<return><list min="1"><char>[0-9.]</char></list></return>
	<s />
</rule>

<rule name="numberlist">
	<attr name="a0" value="literal-0" />
	<attr name="a1"><return><value>literal-1</value></return></attr>
	<attr name="a2"><temp><term>(</term><return><match name="number"/></return></temp></attr>
	<term>(</term>
	<opt><match name="number"/></opt>
	<list><term>,</term><match name="number" /></list>
	<term>)</term>
</rule>

</syntax>
EOT;
        $input = <<<EOT
Alma 12, 55, Barack 23, Citrom (4, 7)
EOT;
        $this->parser = new Parser_String($input, $this->syntax);
        $node_root = $this->parser->result->appendChild($this->parser->result->createElement('root'));

        $r = $this->parser->matchRule('elem', $node_root, 1);
        $this->assertEquals($r, true, 'Case A: %s');
        $this->assertEquals($this->parser->export($node_root, 2),
            '<root><fruit>Alma</fruit><number>12</number></root>',
            'Case B: %s');

        $res = $this->parser->apply();
        $this->parser->result->formatOutput = false;
        $this->assertEquals($this->parser->export($res),
            '<elemlista><elem><number>55</number></elem><elem><fruit>Barack</fruit><number>23</number></elem><elem><fruit>Citrom</fruit><numberlist a0="literal-0" a1="literal-1" a2="4"><number>4</number><number>7</number></numberlist></elem><elem><error/></elem></elemlista>',
            'Case C: %s');
    }
}
