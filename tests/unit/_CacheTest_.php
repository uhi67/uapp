<?php
/** @noinspection PhpUnhandledExceptionInspection */

/**
 * @author Peter Uherkovich
 * @copyright 2014-2018
 * @filesource CacheTest.php
 */

use Codeception\Test\Unit;

/**
 * CacheTest
 *
 * @package rr.dev
 * @author Peter Uherkovich
 * @copyright 2018
 * @version $Id$
 * @access public
 */
class CacheTest extends Unit {
    /**
     * @var UnitTester
     */
    protected $tester;

    protected function _before() {
        $outputpath = dirname(__DIR__) . '/_output/Cache';
        $this->cache = new Cache(['path' => $outputpath, 'enabled' => 1, 'default' => 5]);
    }

    protected function _after() {
    }

    private $cache;

    function test_get1() {
        $this->assertNull($this->cache->get('test_x'));
        $this->assertNull($this->cache->get('test_x', 200));
    }

    /**
     * @dataProvider provider_set_get
     *
     * @return void
     */
    function test_set_get($name, $value, $ttl, $wait, $ttl2, $result) {
        $this->cache->set($name, $value, $ttl);
        sleep($wait);
        $result = $this->cache->get($name, $ttl2);
        if ($result) $this->assertEquals($value, $result);
        else $this->assertNull($result);
    }

    function provider_set_get() {
        return [
            // test case data: name, value, ttl, wait, ttl2, result
            ['test_a', [1, 2, '3'], 5, 4, 0, true],    // Időben
            ['test_b', [1, 2, '3'], 5, 4, 3, false],    // késői lejárat
            ['test_c', [1, 2, '3'], 4, 5, 0, false],    // korai lejárat
            ['test_d', ['a' => 1, 2, '3'], 5, 4, 0, true], // Időben
        ];
    }
}
