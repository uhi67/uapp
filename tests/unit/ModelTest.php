<?php
/** @noinspection PhpIllegalPsrClassPathInspection */
require_once "models/City.php";
require_once "models/Dhcphost.php";
require_once "models/Dhcpdomain.php";
require_once "models/Dhcpoption.php";
require_once "models/Dhcpoptionname.php";
require_once "models/Dhcpoptiontype.php";
require_once "models/Person.php";
require_once "models/Org.php";

require_once dirname(dirname(__DIR__)) . '/lib/testhelper/TestDebug.php';
require_once dirname(__DIR__) . '/_support/Helper/UnitTest.php';

/**
 * QueryTest -- Testing Query class
 *
 * No UApp instance created!
 *
 */
class ModelTest extends UnitTest {
    /** @var DBX $db */
    private $db;
    /** @var UXMLDoc $xmldoc */
    private $xmldoc;

    /**
     * @var UnitTester $tester
     */
    protected $tester;

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @throws
     */
    protected function _before() {
        $testdbconfig = require(dirname(__DIR__) . '/dbconfig.php');
        $this->db = Component::create($testdbconfig);
        $this->xmldoc = new UXMLDoc();
        $this->xmldoc->locale = 'hu';
        UApp::setLang('hu');
        UApp::$config = [
            'uapp' => [
                'path' => dirname(dirname(__DIR__)),
            ],
            'datapath' => dirname(__DIR__) . '/_output',
        ];
        L10nBase::init([]);
        Person::first(2)->setAttribute('descr', 'Tesztelek')->save();
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    protected function _after() {
    }


    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @throws ConfigurationException
     */
    function test_la() {
        $this->assertEquals(dirname(dirname(__DIR__)), UApp::config('!uapp/path'));
        $uappDir = UApp::config('uapp/def', UApp::config('!uapp/path') . '/def') . '/translations/';
        $this->assertEquals(dirname(dirname(__DIR__)) . '/def/translations/', $uappDir);

        $expected = 'egyedi kell legyen';
        $text = 'must be unique';

        $result = L10nBase::getText('uapp', $text, null, 'hu');
        $this->assertEquals($expected, $result);

        $result = UApp::la('uapp', $text);
        $this->assertEquals($expected, $result);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @throws DatabaseException
     * @throws InternalException
     * @throws QueryException
     * @throws UAppException
     */
    function test_firstx() {
        /** @var Person $person */
        $person = Person::first(3, null, null, $this->db);
        $person->unit = 3;
        $person->save();

        // Null condition
        $person = Person::first(null, null, null, $this->db);
        $this->assertEquals(null, $person);

        // Order join
        /** @var Person $person */
        $person = Person::first(true, [['unit1.name']], null, $this->db);
        $this->assertEquals(3, $person->id);

        // UniqueValidate
        $r = $person->uniqueValidate('name');
        $this->assertEquals(true, $r);
        $r = $person->uniqueValidate('org');
        $this->assertEquals(false, $r);
        $this->assertEquals(['org' => ['egyedi kell legyen']], $person->errors);

        $person->resetErrors(); // Reset validation results
        $r = $person->uniqueifnullValidate('org', 'deleted');
        $this->assertEquals(false, $r);
        $this->assertEquals(['org' => ['egyedi kell legyen']], $person->errors);

        $org = $person->unit1;
        $this->assertEquals('Hegyellenőrző osztály', $org->name);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @dataProvider provider_first
     *
     * @param int $id
     * @param string $name
     * @param $unit
     * @param $unitname
     *
     * @throws DatabaseException
     * @throws InternalException
     * @throws QueryException
     * @throws UAppException
     */
    function test_first($id, $name, $unit, $unitname) {
        /** @var Person $person */
        $person = Person::first($id, null, null, $this->db);
        $this->assertEquals($name, $person->name);
        $this->assertEquals($unit, $person->unit);
        $person = Person::first(['name' => $name], ['id'], null, $this->db);
        $this->assertEquals($id, $person->id);
        $this->assertEquals($unitname, $person->unit1->name);
        $this->assertEquals($id, $person->oldAttributes['id']);
        $person->id = 999;
        $this->assertEquals($id, $person->oldAttributes['id']);
        $this->assertEquals(['id' => $id], $person->getOldPrimaryKey(true));
        $this->assertEquals($id, $person->getOldPrimaryKey(false));
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    function provider_first() {
        return [
            [1, 'Adminisztrátor', 1, 'Kukutyin Zabhegyező Vállalat'],
            [2, 'Test Elek Egy', 2, 'Zabhegyező főosztály'],
        ];
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @dataProvider provider_insert
     *
     * @param array $data
     * @param bool $expected
     * @param array $errors
     *
     * @throws DatabaseException
     * @throws InternalException
     * @throws ModelException
     * @throws QueryException
     * @throws UAppException
     */
    function test_insert($data, $expected, $errors) {
        Dhcphost::deleteAll(['name' => 'insert_test_1'], null, $this->db);

        $countQuery = Dhcphost::createSelect([['count()', 'id']], null, ['deleted' => null], null, $this->db);
        $count1 = $countQuery->scalar();

        $dhcphost = new Dhcphost($data, $this->db);
        $dhcphost->validate();

        $result = $dhcphost->save();
        $this->assertEquals($errors, $dhcphost->errors, 'case B');
        $this->assertEquals($expected, $result, 'Case A');
        if ($result) {
            // Sikeres rögzítés
            $count2 = $countQuery->scalar();
            $this->assertEquals($count1 + 1, $count2, 'Case C');
            $r = $dhcphost->delete();
            $this->assertEquals(1, $r, 'Case D');
            $count3 = $countQuery->scalar();
            $this->assertEquals($count1, $count3, 'Case E');

            // Modify
            $dhcphost->modified = new DateTime();
            $dhcphost->setAttributes(['descr' => 'ddd', 'modifier' => 2]);
            $result = $dhcphost->save();
            $this->assertEquals(true, $result);

            // Delete
            $result = $dhcphost->delete();
            $this->assertEquals(1, $result);
        }
        #$this->tester->see();
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    function provider_insert() {
        return [ // $data, $expected, $errors
            [
                [
                    "name" => "insert_test_1",
                    "descr" => "",
                    "mac" => "b7:c3:7b:94:58:15",
                    "dhcpdomain" => 1,
                    "org" => null,
                    "owner" => null,
                    "notify" => null,
                    "creator" => 1,
                    "modifier" => null,
                    "deleter" => null,
                    "created" => new DateTime(),
                    "modified" => null,
                    "deleted" => null,
                    "ip" => "1.5.11.213",
                    "active" => true,
                    "expires" => null,
                    "reserved" => false,
                    "ipend" => null
                ],
                true,
                []
            ],
            [
                [
                    "name" => "insert_test_1",
                    "descr" => "",
                    "mac" => "b7:c3:7b:94:58:15",
                    "dhcpdomain" => 1,
                    "org" => null,
                    "owner" => null,
                    "notify" => null,
                    "creator" => 1,
                    "modifier" => null,
                    "deleter" => null,
                    "created" => '1967. 09. 03. 11:30',
                    "modified" => null,
                    "deleted" => null,
                    "ip" => "1.5.11.213",
                    "active" => true,
                    "expires" => null,
                    "reserved" => false,
                    "ipend" => null
                ],
                true,
                []
            ],
            [
                [
                    "name" => "in",
                    "descr" => "",
                    "mac" => "b7:c3:7b:94:58:15",
                    "dhcpdomain" => 2,
                    "created" => 'a',
                    "ip" => "1.5.11.213/24",
                    "reserved" => true,
                    "ipend" => '1.5.11'
                ],
                false,
                [
                    'created' => ['érvénytelen dátum-idő'],
                    'name' => ['nem lehet rövidebb, mint 5 karakter'],
                    'ip' => ['is invalid host address'],
                    'ipend' => ['érvénytelen IP cím', 'Nem IP cím'],
                ]
            ],
        ];
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @throws DatabaseException
     * @throws InternalException
     * @throws QueryException
     * @throws UAppException
     */
    function test_updateAll() {
        $descr = 'ModelTest_updateAll'; // id to recognize test data
        // -1: pre-cleanup (of failed test)
        Dhcpdomain::deleteAll(['descr' => $descr], null, $this->db);
        // 0. Counting original data
        $countQuery = Dhcpdomain::createSelect([['count()', 'id']], null, ['deleted' => null], null, $this->db);
        $count1 = $countQuery->scalar();

        // 1. Creating test data
        $data = [
            [10001, 'testdomain_1', $descr, 1, new DateTime(), 1],
            [10002, 'testdomain_2', $descr, 2, '2018-01-01', 1],
            [10003, 'testdomain_3', $descr, 3, '2018-01-02 12:00', 2],
            [10004, 'testdomain_4', $descr, 4, '2018-01-03 13:30', 2],
            [10005, 'testdomain_5', $descr, 5, '2018-01-04 23:59:59', 3],
        ];
        $count = count($data);
        $insertQuery = Query::createInsert('Dhcpdomain', ['id', 'name', 'descr', 'creator', 'created', 'org'], $data, $this->db);
        $result = $insertQuery->execute(); // Inserts without validation
        $this->assertEquals($count, $result);

        $count2 = $countQuery->scalar();
        $this->assertEquals($count1 + $count, $count2);

        // 2. Modifíying data
        $result = Dhcpdomain::updateAll(['modified' => new DateTime(), 'modifier' => 1], ['descr' => $descr, 'org' => 2], $this->db);
        $this->assertEquals(2, $result);

        $domains = Dhcpdomain::all(['descr' => $descr, 'modifier' => '$1'], [['creator', 'desc']], [1], $this->db);
        $this->assertEquals($domains[0]->descr, $descr);


        // 3. Cleanup
        Dhcpdomain::deleteAll(['descr' => $descr], null, $this->db);
        $count3 = $countQuery->scalar();
        $this->assertEquals($count1, $count3);

    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @dataProvider provider_getReferers
     *
     * @param int $orgid
     * @param array $expected
     *
     * @throws DatabaseException
     * @throws InternalException
     * @throws ModelException
     * @throws QueryException
     * @throws UAppException
     */
    function test_getReferers($orgid, $expected) {
        /** @var Org $org */
        $org = Org::first(['id' => '$id'], null, ['id' => $orgid], $this->db);
        $this->assertNotNull($org);
        $this->assertEquals($orgid, $org->id);

        /*
        // partial tests of tested method
        $reference_name = 'unit1';
        $fk = Person::getForeignKey($reference_name);
        array_shift($fk);
        $this->assertEquals(array('unit'=>'id'), $fk);
        $condition = array_combine(array_keys($fk), array_values($org->getAttributes(array_values($fk))));
        $this->assertEquals(array('unit'=>$org->id), $condition);
        $orders = array('id');
        $query = Query::createSelect(array(
            'from' => 'Person',
            'where' => $this->db->asExpression($condition),
            'orders' => $orders,
            'db' => $this->db
        ));
        $this->assertEquals('SELECT "created", "creator", "deleted", "descr", "dn", "email", "enabled", "id", "idp", "lastclick", "lastlogin", "loggedin", "logins", "name", "org", "pagelen", "tel", "title", "tooltip", "type", "uid", "unit" '.
            'FROM "person" WHERE "unit" = '.$orgid.' ORDER BY "id"',
         $query->sql);
        $result=array();
        $n = $this->db->select_all($result, $query->sql);
        $this->assertEquals(count($expected), $n);
        $this->assertEquals($expected, array_map(function($item) { return $item['id']; }, $result));

        $attributes = Person::attributes($this->db);
        $result = @array_map(function($row) use($attributes) {

            //Restrict row to attributes of the model
            $row = array_filter($row, function($v, $k) use($attributes) { return in_array($k, $attributes); }, ARRAY_FILTER_USE_BOTH);

            $model = new Person($row, $this->db);
            $model->setOldAttributes($row);
            return $model;
        }, $result);
        $this->assertEquals($expected, ArrayUtils::map($result, null, 'id'));


        $values = $query->all();
        $this->assertEquals($expected, ArrayUtils::map($values, null, 'id'));
        */

        /** @var Person[] $persons */
        $persons = $org->getReferers('Person', 'unit1', ['id']);
        $this->assertEquals($expected, ArrayUtils::map($persons, null, 'id'));
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    function provider_getReferers() {
        return [
            [0, []],
            [1, [1]],
            [2, [2, 4, 5]],
            [3, [3]],
        ];
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @dataProvider provide_validate
     *
     * @param array $data
     * @param array|null $attributes -- attributes to validate or null for all
     * @param array $expected
     * @param array $nodes
     *
     * @throws
     */
    function test_validate($data, $attributes, $expected, $nodes) {
        $person = new Person($data, $this->db);
        $valid = $person->validate($attributes);
        $result = $person->errors;
        $this->assertEquals(empty($expected), $valid, $valid ? "Errors was expected: " . json_encode($expected) : "Got errors: " . json_encode($person->errorNodes));
        $this->assertEquals($expected, $result);
        $this->assertEquals(json_encode($nodes), json_encode($person->errorNodes));
    }

    /** @noinspection PhpMethodNamingConventionInspection */
    function provide_validate() {
        return [ // $data, $attributes, $expected, $nodes
            0 => [
                [
                    'name' => '_TestPerson_1',
                ],
                null,
                [],
                [],
            ],
            1 => [
                [
                    'name' => 'Test Elek Egy',
                    'unit' => 2
                ],
                null,
                [
                    'name' => ['a `Egység` mezővel együtt egyedi kell legyen'],
                ],
                [['error', ['field' => 'name'], 'A `Név` a `Egység` mezővel együtt egyedi kell legyen']],
            ],
            2 => [
                [
                    'idp' => 'x',
                ],
                null,
                [
                    'name' => ['kötelező'],
                    'idp' => ['érvénytelen URL'],
                ],
                [
                    ['error', ['field' => 'name'], 'A `Név` kötelező'],
                    ['error', ['field' => 'idp'], 'A `idp` érvénytelen URL'],
                ],
            ],
            3 => [ // $data, $attributes, $expected, $nodes
                [
                    'name' => '_TestPerson_1',
                    'uid' => 'test1',
                    'idp' => 'idp'
                ],
                null,
                [
                    'idp' => ['érvénytelen URL'],
                    'uid' => ['a `idp` mezővel együtt egyedi kell legyen'],
                ],
                [
                    ['error', ['field' => 'uid'], 'A `uid` a `idp` mezővel együtt egyedi kell legyen'],
                    ['error', ['field' => 'idp'], 'A `idp` érvénytelen URL'],
                ],
            ],
            4 => [
                [
                    'name' => '_TestPerson_3',
                    'uid' => 'test999',
                    'idp' => 'http://idp.test/metadata',
                    'creator' => 1,
                    'unit' => 1,
                ],
                ['creator', 'unit'],
                [],
                [],
            ],
            5 => [
                [
                    'name' => '_TestPerson_3',
                    'uid' => 'test999',
                    'idp' => 'http://idp.test/metadata',
                    'creator' => 999,
                    'unit' => 999,
                ],
                ['unit'],
                [
                    'unit' => ['érvénytelen hivatkozás', 'érvénytelen hivatkozás'],
                ],
                [
                    ['error', ['field' => 'unit'], 'A `Egység` érvénytelen hivatkozás, érvénytelen hivatkozás'],
                ],
            ],
            6 => [
                [
                    'name' => '_TestPerson_3',
                    'uid' => 'test999',
                    'idp' => 'http://idp.test/metadata',
                    'creator' => 999,
                    'unit' => 999,
                ],
                ['creator'],
                [
                    'creator' => ['érvénytelen hivatkozás'],
                ],
                [
                    ['error', ['field' => 'creator'], 'A `creator` érvénytelen hivatkozás'],
                ],
            ],
        ];
    }

    /**
     * @throws InternalException
     * @throws ModelException
     * @throws UAppException
     */
    function testAutoIncrementValidate() {
        $data = ['name' => 'Áron', 'email' => 'uhi@uhisoft.hu'];
        $person = new Person($data, $this->db);
        $valid = $person->autoincrementValidate('id', []);
        $this->assertTrue($valid);
        $this->assertEquals(6, $person->id);
        $person->autoincrementValidate('unit', ['email']);
        $this->assertEquals(3, $person->unit);
    }

    /**
     * @throws InternalException
     * @throws ModelException
     */
    function testGetRelation() {
        $data = ['name' => 'Áron', 'email' => 'uhi@uhisoft.hu', 'unit' => 1];
        $person = new Person($data, $this->db);
        /** @var Org $unit */
        $unit = $person->getRelation('unit1');
        $this->assertEquals('Kukutyin Zabhegyező Vállalat', $unit->name);
    }

    /**
     * @throws
     */
    function testUpdate() {
        /** @var Person $person */
        $person = Person::first(5, null, null, $this->db);
        $old_title = $person->title;
        $new_title = 'test-4-test';
        $person->title = $new_title;
        $this->assertTrue($person->save(null, false), Util::objtostr($person->friendlyErrors));

        $person = Person::first(5, null, null, $this->db);
        $this->assertEquals($new_title, $person->title);
        $person->title = $old_title;
        $this->assertTrue($person->save(null, false));
    }

    /**
     * @throws
     */
    function testUpdateAttributes() {
        /** @var Person $person */
        $person = Person::first(5, null, null, $this->db);
        $old_title = $person->title;
        $new_title = 'test-4-test';
        $this->assertEquals(1, $person->updateAttributes(['title' => $new_title]));

        $person = Person::first(5, null, null, $this->db);
        $this->assertEquals($new_title, $person->title);
        $this->assertEquals(1, $person->updateAttributes(['title' => $old_title]));
    }

    /**
     * @dataProvider provide_CreateNode
     * @throws
     */
    function testCreateNode($id, $expected, $def) {
        $person = Person::first($id, null, null, $this->db);
        $node = $person->createNode($this->xmldoc->documentElement, $def);
        $this->assertXmlEquals($expected, $node);
    }

    function provide_CreateNode() {
        return [
            [
                1,
                '<person id="1" name="Adminisztrátor"><unit1 id="1" name="Kukutyin Zabhegyező Vállalat"/></person>',
                [
                    'attributes' => ['id', 'name'],
                    'associations' => ['unit1' => ['id', 'name']],
                ]
            ],
            [
                2,
                '<user uid="test1" name="Test Elek Egy">
  <descr>Tesztelek</descr>
  <org orgName="Zabhegyező főosztály" type="0">A végtelenbe, és tovább!</org>
</user>
',
                [
                    'associations' => [
                        'unit1' => [ // foreign key name
                            'nodeName' => 'org',
                            'orgName' => 'name', // field map attribute=>field
                            'type',
                            '__content' => 'descr' // field is mapped to XML content
                        ]
                    ],
                    'nodeName' => 'user',
                    'attributes' => ['uid', 'name'],
                    'subnodeFields' => ['descr'],
                ]
            ]
        ];
    }

    /**
     * @dataProvider provBytea
     */
    public function testBytea($xid, $name, $found) {
        $id = substr(md5($name, true), 0, 8);
        $this->assertEquals($xid, $id);

        $this->assertEquals('Bytea', call_user_func([City::class, 'attributeType'], 'id', $this->db));

        $this->assertSame('\'\x' . bin2hex($id) . '\'', $this->db->escape_literal($id, true));

        $query = Query::createSelect(City::class, null, null, $id, null, null, null, null, $this->db);
        $this->assertEquals('SELECT "city", "id" FROM "city" WHERE "id" = \'\x' . bin2hex($id) . '\'', $query->finalSQL);

        /** @var City $city */
        $city = City::first($id, null, null, $this->db);
        if ($found) {
            $this->assertNotNull($city);
            $this->assertEquals($name, $city->city);
        } else {
            City::deleteAll(['city' => $name], null, $this->db);
            $this->assertNull($city);
            $city = new City([
                'id' => $id,
                'city' => $name
            ], $this->db);
            $city->save();
            $city = City::first(['city' => $name], null, null, $this->db);
            $this->assertNotNull($city);
            $this->assertEquals($id, $city->id);
            $city->delete();
        }
    }

    public function provBytea() {
        return [
            [hex2bin('4f01e8bf5ebce8fd'), 'Pécs', true],
            [hex2bin('85f168b516da58ce'), 'Hódmezővásárhely', true],
            [hex2bin('5f067c5a58ee27fe'), 'Karakószörcsök', false],
        ];
    }

    public function testPrimaryKey() {
        $person = Person::first(5);
        $this->assertEquals(['id' => 5], $person->getOldPrimaryKey(true));
    }

    public function testDhcpValidet() {
        $dhcpHost = new Dhcphost(['dhcpdomain' => 3, 'name' => 'test17', 'created' => '2022-10-19 8:00', 'mac' => '1122339988FF', 'ip' => '10.250.17.233']);
        $valid = $dhcpHost->validate();
        if (!$valid) {
            $this->assertEquals(['created' => []], $dhcpHost->errors);
        }
        $this->assertTrue($valid);
    }
}
