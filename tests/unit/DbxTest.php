<?php

use Codeception\Test\Unit;

require_once "models/Dhcpdomain.php";
require_once "models/Dhcpoption.php";
require_once "models/Dhcpoptionname.php";
require_once "models/Dhcpoptiontype.php";
require_once "models/Person.php";

class DbxTest extends Unit {
    /** @var DBX $db */
    private $db;

    /**
     * @var UnitTester
     */
    protected $tester;

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @throws InternalException
     * @throws ReflectionException
     * @throws Exception
     */
    protected function _before() {
        UApp::setLang('hu');
        $testdbconfig = require(dirname(__DIR__) . '/dbconfig.php');
        $this->db = Component::create($testdbconfig);
        try {
            $this->db->autoconnect();
        } catch (Exception $e) {
            throw new Exception('Database connection error with ' . json_encode($testdbconfig), 0, $e);
        }
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    protected function _after() {
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @dataProvider provider_isOperator
     *
     * @param string $op
     * @param mixed $expected
     * @return void
     */
    function test_isOperator($op, $expected) {
        $result = DBX::isOperator($op);
        $this->assertSame($expected, $result);
    }

    /** @noinspection PhpMethodNamingConventionInspection */
    function provider_isOperator() {
        return [
            // test case data
            ['', false],
            ['xxxx', false],
            ['true', 0],
            ['TRUE', 0],
            ['not', 1],
            ['or', 4],
            ['-', 2],
        ];
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @dataProvider provider_operatorPrecedence
     *
     * @param string $op
     * @param mixed $expected
     * @return void
     */
    function test_operatorPrecedence($op, $expected) {
        $result = DBX::operatorPrecedence($op);
        $this->assertSame($expected, $result);
    }

    /** @noinspection PhpMethodNamingConventionInspection */
    function provider_operatorPrecedence() {
        return [
            // test case data
            ['', false],
            ['xxxx', false],
            ['true', 1],
            ['FALSE', 2],
            ['-', 13],
            ['not', 40],
            ['or', 42],
        ];
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @dataProvider provider_getModelFields
     *
     * @param string $expected
     * @param string $model
     *
     * @return void
     * @throws DatabaseException
     */
    function test_getModelFields($expected, $model) {
        $result = $this->db->getModelFields($model);
        $this->assertEquals($expected, $result);
    }

    /** @noinspection PhpMethodNamingConventionInspection */
    function provider_getModelFields() {
        return [
            [['descr', 'id', 'name'], Dhcpoptiontype::class],
            [['host', 'num', 'option', 'value'], 'Dhcpoption'],
        ];
    }


    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @throws DatabaseException
     */
    function test_getModelForeignKey() {
        $model = 'Dhcpoption';
        $this->assertEquals(DBX::getModelForeignKey($model, 'host1'), ['Dhcphost', 'host' => 'id']);
        $this->assertEquals(DBX::getModelForeignKey($model, 'option2'), false);

        $this->assertEquals(DBX::doubleQuote('alma'), '"alma"');
        $this->assertEquals(DBX::doubleQuote('alma"'), '"alma\'"');

        $this->assertEquals($this->db->escape_literal('alma'), "'alma'");
        $this->assertEquals($this->db->escape_literal("'alma'"), "'''alma'''");
    }


    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @dataProvider provider_createWhere
     *
     * @param string $expected
     * @param array $attributes
     *
     * @return void
     * @throws DatabaseException
     */
    function test_createWhere($expected, $attributes) {
        /** @noinspection PhpDeprecationInspection */
        $r = DBX::createWhere($attributes);
        $this->assertEquals($expected, $r);
    }

    /** @noinspection PhpMethodNamingConventionInspection */
    function provider_createWhere() {
        return [ // $expected, $attributes
            ['', null],
            ['', []],
            ['where id=23', ['id' => 23]],
            ['where id=23 and x=4', ['id' => 23, 'x' => 4]],
            ['where id=23 and (x=4 or x=5)', ['id' => 23, ['x' => 4, 'x=5']]],
            ['where id is null', ['id is null']],
            ['where (id is null or parent is not null)', [['id is null', 'parent is not null']]],
            ['where (id is null or (parent is not null and y>3))', [['id is null', ['parent is not null', 'y>3']]]],
        ];
    }


    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @dataProvider provider_createSqlUpdate
     *
     * @param $expected
     * @param $base
     * @param $params
     * @param $attributes
     * @param $where
     *
     * @return void
     * @throws DatabaseException
     */
    function test_createSqlUpdate($expected, $base, $params, $attributes, $where) {
        /** @noinspection PhpDeprecationInspection */
        $r = DBX::createSqlUpdate($base, $params, $attributes, $where);
        $this->assertEquals($expected, $r);
    }

    /** @noinspection PhpMethodNamingConventionInspection */
    function provider_createSqlUpdate() {
        return [
            // test case data: $expected, $base, $params, $attributes, $where
            ['', null, null, null, null],
            ['', 'update table %s %s', [], null, ['id' => 3]],
            ['', 'update table %s where id=$1', [3], null, null],
            [/** @lang text */ 'update table set z=12 where id=3 and k is null', 'update table %s %s', null, ['z' => 12], ['id' => 3, 'k is null']],
            [/** @lang text */ "update table set z=13, id='sss' where id=3", 'update table %s where id=$1', [3], ['z' => 13, 'id' => 'sss'], null],
        ];

    }

    /** @noinspection PhpMethodNamingConventionInspection */
    function test_replace() {
        $this->assertSame('d:\\\\prog', preg_replace('/\\\\/', '\\\\\\\\', 'd:\\prog'));
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @dataProvider provider_literal
     *
     * @param string|integer $literal
     * @param string|integer $expected
     *
     * @return void
     * @throws DatabaseException
     * @throws InternalException
     */
    function test_literal($literal, $expected) {
        if (is_string($expected) && substr($expected, 0, 1) == '*') $this->expectException(substr($expected, 1));
        $this->assertSame($expected, $this->db->literal($literal));
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @return array
     */
    function provider_literal() {
        return [// test case data: $literal, $expected
            ['a', "'a'"],
            [12, 12],
            ['\a', " E'\\\\a'"], // Ez hülyén néz ki, de így jó!
            [['alma', 'körte'], ["'alma'", "'körte'"]],
            [new DateTime('2017-09-03'), "'2017-09-03 00:00:00+02:00'"],
            [new Inet('193.6.50.100'), "'193.6.50.100'"],
            [new Macaddr('112233445566'), "'11:22:33:44:55:66'"],
            [new Exception(), '*DatabaseException'],
            [[1, 2], [1, 2]],
            [['a', 'b'], ["'a'", "'b'"]],
            [['a' => 'a', 'b' => 'b'], ['a' => "'a'", 'b' => "'b'"]],
        ];
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @dataProvider provider_selectValue
     *
     * @param string $sql
     * @param array $params
     * @param string $expected
     *
     * @return void
     * @throws DatabaseException
     * @throws InternalException
     */
    function test_selectValue($sql, $params, $expected) {
        $r = $this->db->selectvalue($sql, $params);
        $this->assertEquals($expected, $r);
    }

    /** @noinspection PhpMethodNamingConventionInspection */
    function provider_selectValue() {
        return [
            // test case data: $sql, $params, $expected
            ['select name from org where id=$1', [1], 'Kukutyin Zabhegyező Vállalat'],
            ['select name from org where id=$1', [null], null],
            ['select id from org where name=$1', ['d:\\\\data\\\\uapp'], null],
        ];
    }

    /**
     * @dataProvider provDesc
     *
     * @param array $ord
     * @param array $expected
     *
     * @throws ServerErrorException
     */
    function testDesc($ord, $expected) {
        $result = DBX::desc($ord);
        $this->assertEquals($expected, $result, Util::objtostr($ord) . ' -> ' . Util::objtostr($result));
    }

    function provDesc() {
        return [ // $ord, $expected
            [
                ['name'],
                [['name', DBX::ORDER_DESC, DBX::NULLS_LAST]],
            ],
            [
                [['name', DBX::ORDER_ASC, DBX::NULLS_LAST]],
                [['name', DBX::ORDER_DESC, DBX::NULLS_FIRST]],
            ],
            [
                [['name', DBX::ORDER_DESC]],
                [['name', DBX::ORDER_ASC, DBX::NULLS_LAST]],
            ],
        ];
    }


    /**
     * @dataProvider provParamsx
     * @param string $sql
     * @param array $params
     * @param string $expected
     *
     * @throws DatabaseException
     */
    function testParamsx($sql, $params, $expected) {
        /** @noinspection PhpDeprecationInspection */
        $this->assertEquals($expected, DBX::paramsx($sql, $params));
    }

    function provParamsx() {
        return [
            ['select id', [], 'select id'],
            [/** @lang text */ 'select id from person where name={$name}', ['name' => 'Dezső'], 'select id from person where name=\'Dezső\''],
            ['select id from person where name=$name', ['name' => 'Dezső'], 'select id from person where name=\'Dezső\''],
            ['select id from person where name=$name', ['Dezső'], 'select id from person where name=$name'],
        ];
    }

    /**
     * @dataProvider provArrayValue
     * @param array $value
     * @param string $expected
     *
     * @throws DatabaseException
     */
    function testArrayValue($value, $expected) {
        /** @noinspection PhpDeprecationInspection */
        $this->assertEquals($expected, DBX::arrayValue($value));
    }

    function provArrayValue() {
        return [
            [[], "{}"],
            [['alma', 'körte'], "ARRAY['alma','körte']"],
            [['alma' => 12, 'körte' => 23], "ARRAY[12,23]"],
            [[[12, 13], [22, 23]], "ARRAY[ARRAY[12,13],ARRAY[22,23]]"],
        ];
    }

    /**
     * @dataProvider provParseArray
     *
     * @param string $s
     * @param int $start
     * @param int $end
     * @param array $expected
     */
    function testParseArray($s, $start, $end, $expected) {
        $this->assertEquals($expected, DBX::parseArray($s, $start, $ends));
        $this->assertEquals($end, $ends);
    }

    function provParseArray() {
        return [
            ['ARRAY[12,23]', 0, 0, null],
            ['{}', 0, 1, []],
            ['{12,23}', 0, 6, ['12', '23']],
            ['{{12,13},{22,23}}', 0, 16, [['12', '13'], ['22', '23']]],
            ['{{12,13},{22xxxx', 1, 7, ['12', '13']],
        ];
    }

    function testParseBoolean() {
        $this->assertEquals(true, DBX::parseBoolean('13'));
        $this->assertEquals(true, DBX::parseBoolean('-1'));
        $this->assertEquals(true, DBX::parseBoolean('t'));
        $this->assertEquals(true, DBX::parseBoolean('true'));

        $this->assertEquals(false, DBX::parseBoolean(''));
        $this->assertEquals(false, DBX::parseBoolean('0'));
        $this->assertEquals(false, DBX::parseBoolean('f'));
        $this->assertEquals(false, DBX::parseBoolean('false'));
        $this->assertEquals(false, DBX::parseBoolean('yes'));
    }

    /**
     * @dataProvider provDecode
     * @param string $encoding
     * @param string $dbtext
     * @param string $expected
     *
     * @throws ConfigurationException
     * @throws DatabaseException
     * @throws InternalException
     */
    function testDecode($encoding, $dbtext, $expected) {
        $db = new DBXPG(null, null, null, null, null, $encoding);
        $this->assertEquals($expected, $db->decode($dbtext));
        $this->assertEquals($dbtext, $db->encode($expected));
    }

    function provDecode() {
        return [
            ['UTF-8', 'Őrület', 'Őrület'],
            ['LATIN2', iconv('UTF-8', 'LATIN2', 'Őrület'), 'Őrület'],
            ['ISO-8859-2', iconv('UTF-8', 'LATIN2', 'Őrület'), 'Őrület'],
        ];
    }

    /**
     * @dataProvider provNx
     * @param mixed $value
     * @param mixed $nx
     * @param mixed $ns
     */
    function testNx($value, $nx, $ns) {
        $this->assertEquals($nx, DBX::nx($value));
        $this->assertEquals($ns, DBX::ns($value));
    }

    function provNx() {
        return [ // $value, $nx, $ns
            [null, null, null],
            [1, 1, 1],
            [0, null, 0],
            ['alma', null, 'alma'],
            ['', null, null]
        ];
    }

    /**
     * @throws DatabaseException
     */
    function testFieldNames() {
        $rs = $this->db->query('select * from org limit 1');
        $fieldnames = ['id', 'name', 'parent', 'orgid', 'descr', 'type', 'deleted', 'created', 'deleter', 'tk_id'];
        $this->assertEquals($fieldnames, $this->db->fieldnames($rs));
    }

    function testGetMetaData() {
        $this->assertEquals('DBXPG', $this->db->type);
        $this->assertEquals(null, $this->db->getMetaData('xxx'));
        $expected = [
            'id' => [
                'num' => 1,
                'type' => 'bigint',
                'len' => 20,
                'not null' => true,
                'has default' => false,
            ],
            'name' => [
                'num' => 2,
                'type' => 'varchar',
                'len' => 20,
                'not null' => true,
                'has default' => false,
            ],
            'descr' => [
                'num' => 3,
                'type' => 'text',
                'len' => -1,
                'not null' => false,
                'has default' => false,
            ],
        ];
        // Vagy ez
        $expected2 = [
            'id' => [
                'num' => 1,
                'type' => 'integer',
                'len' => null,
                'not null' => true,
                'has default' => true,
            ],
            'name' => [
                'num' => 2,
                'type' => 'character varying',
                'len' => 20,
                'not null' => true,
                'has default' => false,
            ],
            'descr' => [
                'num' => 3,
                'type' => 'text',
                'len' => null,
                'not null' => false,
                'has default' => false,
            ],
        ];
        $md = $this->db->getMetaData('dhcpoptiontype');
        if ($md['id']['type'] == 'bigint') {
            $this->assertEquals($expected, $md);
        } else {
            if ($md['id']['type'] == 'bigint') {
                $this->assertEquals($expected2, $md);
            }
        }
    }


    /**
     * @dataProvider provBuildExpression
     * @param string $expression
     * @param string $alias
     * @param integer $precedence
     * @param string $expected
     *
     * @throws DatabaseException
     * @throws QueryException
     * @throws UAppException
     */
    function testBuildExpression($expression, $alias, $precedence, $expected) {
        $this->assertEquals($expected, $this->db->buildExpression($expression, $alias, $precedence));
    }

    function provBuildExpression() {
        return [
            [true, null, null, 'true'],
            [false, null, null, 'false'],
            [new DateTime('1999-12-31'), null, null, "'1999-12-31 00:00:00+01:00'"],

            ['alma', null, null, '"alma"'],
            ['alma', 't1', null, '"t1"."alma"'],
            ['t2.alma', 't1', null, '"t2"."alma"'],
            ['t2.alma as a', 't1', null, '"t2"."alma" AS "a"'],
            ['(t2.alma as a)', 't1', null, '(t2.alma as a)'],
            [['t2.alma' => null], 't1', null, '"t2"."alma" IS NULL'],
            [['=', 'a', 1], null, null, '"a" = 1'],
            [['IN', 'a', [1, 2, 3]], null, null, '"a" IN (1, 2, 3)'],
            [['IN', 'a', ["'a'", "'b'", "'c'"]], null, null, '"a" IN (\'a\', \'b\', \'c\')'],
            [['IN', 'a', []], null, null, '"a" IN ()'],
            [['IN', 'a', [1]], null, null, '"a" IN (1)'],
            [['a' => null, ['b' => null]], null, null, '"b" IS NULL AND "a" IS NULL'],
        ];
    }

}
