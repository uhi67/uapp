<?php
/** @noinspection PhpMethodNamingConventionInspection */

/** @noinspection PhpClassNamingConventionInspection */

use Codeception\Test\Unit;

require_once "models/Dhcphost.php";
require_once "models/Dhcpdomain.php";
require_once "models/Dhcpoption.php";
require_once "models/Dhcpoptionname.php";
require_once "models/Dhcpoptiontype.php";
require_once "models/Person.php";
require_once "models/Org.php";

/**
 * QueryTest -- Testing Query class
 */
class QueryDataSourceTest extends Unit {
    /** @var DBX $db */
    private $db;
    /** @var UnitTester $tester */
    protected $tester;

    /**
     * @throws InternalException
     * @throws ReflectionException
     */
    protected function _before() {
        $testdbconfig = require(dirname(__DIR__) . '/dbconfig.php');
        $this->db = Component::create($testdbconfig);
    }

    protected function _after() {
    }

    /**
     * @dataProvider provCreateQueryDataSource
     * @param $expected
     * @param Query $query
     *
     * @throws
     */
    function testCreateQueryDataSource($sql, $countSQL, $queryOptions) {
        $query = Query::createSelect($queryOptions)->setDb($this->db);
        $queryDataSource = new QueryDataSource([
            'query' => $query
        ]);

        $this->assertEquals($sql, $queryDataSource->query->finalSQL);
        $this->assertEquals($countSQL, $queryDataSource->countQuery->finalSQL);
    }

    function provCreateQueryDataSource() {
        /** @noinspection PhpUnhandledExceptionInspection */
        return [
            // $sql, $countSQL, $queryOptions
            0 => [
                'SELECT "id", "name" FROM "dhcpoptiontype"',
                'SELECT (count(*)) FROM "dhcpoptiontype"',
                [
                    'fields' => ['id', 'name'],
                    'from' => Dhcpoptiontype::class,
                ],
            ],
            1 => [
                'SELECT count("name") AS "c", max("modified") AS "last" FROM "dhcpdomain" GROUP BY "admin"',
                'SELECT (count(distinct "admin")) FROM "dhcpdomain"',
                [
                    'fields' => ['c' => ['count()', 'name'], 'last' => ['max()', 'modified']],
                    'from' => Dhcpdomain::class,
                    'groupby' => ['admin']
                ],
            ],
            2 => [
                'SELECT count("name") AS "c", max("modified") AS "last" FROM "dhcpdomain" GROUP BY "admin", "creator"',
                'SELECT (count(*)) FROM (SELECT "admin", "creator" FROM "dhcpdomain" GROUP BY "admin", "creator") "main"',
                [
                    'fields' => ['c' => ['count()', 'name'], 'last' => ['max()', 'modified']],
                    'from' => Dhcpdomain::class,
                    'groupby' => ['admin', 'creator']
                ],
            ],
        ];
    }

}
