<?php

use Codeception\Test\Unit;

/**
 * Test file for Codeception
 */
class ArrayUtilsTest extends Unit {
    /** @var UnitTester */
    protected $tester;

    protected function _before() {
    }

    protected function _after() {
    }

    /**
     * @dataProvider provider_delete
     *
     * @param $array
     * @param $value
     * @param $strict
     * @param $expected
     * @param $items
     */
    function test_array_delete($array, $value, $strict, $expected, $items) {
        $result = ArrayUtils::array_delete($array, $value, $strict);
        $this->assertEquals($items, $result);
        $this->assertEquals($expected, $array);
    }

    function provider_delete() {
        return [
            [[1, 2, 3, 4, '3'], 3, false, [1, 2, 3 => 4], 2],
            [[1, 2, 3, 4, '3'], 3, true, [1, 2, 3 => 4, 4 => '3'], 1],
            [[1 => 'A', 2 => 'B', 3 => 'C'], 'C', true, [1 => 'A', 2 => 'B'], 1],
            [[1, 2, 3], 'C', true, [1, 2, 3], 0],
            [[], '', true, [], 0],
            [[1, 1, 1], '1', false, [], 3],
        ];
    }

    /**
     * @dataProvider provider_array_find
     *
     * @param array $array
     * @param mixed $expected
     */
    function test_array_find($array, $expected) {
        $result = ArrayUtils::array_find($array, function ($v, $k) {
            return $v == '3' || $k === 'D';
        });
        $this->assertSame($expected, $result);
    }

    function provider_array_find() {
        return [
            [[1, 2, 3], 2],
            [[1, 2, 4], false],
            [[1, 2, 'D' => 4], 'D'],
            [['A' => 1, 'B' => 2, 'C' => 3], 'C'],
            [[], false],
        ];
    }

    /**
     * @dataProvider provider_array_find_key
     *
     * @param array $array
     * @param mixed $expected
     */
    function test_array_find_key($array, $params, $expected) {
        $result = ArrayUtils::array_find_key($array, function ($k, $v, $p) {
            return ($v + $k) == $p;
        }, $params);
        $this->assertSame($expected, $result);
    }

    function provider_array_find_key() {
        return [
            [[1, 2, 3], 3, 1],
            [[1, 2, 4], 0, null],
            [[], 0, null],
        ];
    }

    /**
     * @dataProvider provide_firstScalar
     *
     * @param $value
     * @param $expected
     */
    function test_firstScalar($value, $expected) {
        $result = ArrayUtils::firstScalar($value);
        $this->assertSame($expected, $result);
    }

    function provide_firstScalar() {
        return [
            ['B', 'B'],
            [new DateTime(), null],
            [['A', 'B'], 'A'],
            [[[1], 2], 1],
            [[[[]], [2]], 2],
            [[], null],
        ];
    }

    /**
     * @dataProvider paginateProvider
     *
     * @return void
     */
    function test_paginate($input, $order, $offset, $limit, $expected) {
        // test method
        $r = ArrayUtils::paginate($input, $order, $offset, $limit);
        if ($expected === '') $this->assertEquals($r, []);
        else {
            $result = $r[0]['name'] . ',' . $r[count($r) - 1]['name']; // Első,utolsó neve
            $this->assertEquals($result, $expected);
        }
    }

    function paginateProvider() {
        $testdata = [
            ['name' => 'alma', 'id' => 31, 'x' => 1],
            ['name' => 'banán', 'id' => 52, 'x' => 1],
            ['name' => 'citrom', 'id' => 113, 'x' => 2],
            ['name' => 'dió', 'id' => 4, 'x' => 2],
        ];
        return [
            // test case data: $input, $order, $offset, $limit, $expected
            [$testdata, 'id', 2, 2, 'banán,citrom'],
            [$testdata, 'name', 0, 1, 'alma,alma'],
            [$testdata, 'name desc', 0, 4, 'dió,alma'],
            [[], 'name', 0, 1, ''],
            [$testdata, 'x desc, name', 0, 3, 'citrom,alma'],
            [$testdata, 'x desc, name desc', 0, 3, 'dió,banán'],
        ];

    }


    /**
     * @dataProvider array_usearch_provider
     *
     * @return
     */
    function test_array_usearch($aa, $value, $f, $expected) {
        $r = ArrayUtils::array_usearch($aa, $value, function ($item, $value) use ($f) {
            return $item[$f] == $value;
        });
        $this->assertEquals($expected, $r);
    }

    function array_usearch_provider() {
        $testdata = [
            ['name' => 'alma', 'id' => 31, 'x' => 1],
            ['name' => 'banán', 'id' => 52, 'x' => 1],
            ['name' => 'citrom', 'id' => 113, 'x' => 2],
            ['name' => 'dió', 'id' => 4, 'x' => 2],
        ];
        return [
            // input, value, key
            [$testdata, 'alma', 'name', 0],
            [$testdata, 113, 'id', 2],
            [$testdata, 99, 'id', false],
        ];
    }

    /**
     * @dataProvider provider_getValue
     *
     * @return void
     */
    function test_getValue($expected, $array, $key, $default) {
        $result = ArrayUtils::getValue($array, $key, $default);
        $this->assertEquals($expected, $result);
    }

    function provider_getValue() {
        return [
            // test case data
            [null, [1, 2, 3], 4, null],
            [3, [1, 2, 3], 2, null],
            ['a', ['a' => 1, 'b' => 2, 'c' => 3], 'd', 'a'],
            [1, ['a' => 1, 'b' => 2, 'c' => 3], 'a', 'a'],
            [23, ['a' => 1, 'b' => ['aa' => 21, 'bb' => 22, 'cc' => 23], 'c' => 3], ['b', 'cc'], 'x'],
            ['x', ['a' => 1, 'b' => ['aa' => 21, 'bb' => 22, 'cc' => 23], 'c' => 3], ['b', 'cc', 'dd'], 'x'],
        ];
    }

    /**
     * @dataProvider provider_isAssociative
     *
     * @return void
     */
    function test_isAssociative($expected, $array, $strict) {
        $result = ArrayUtils::isAssociative($array, $strict);
        $this->assertEquals($expected, $result);
    }

    function provider_isAssociative() {
        return [
            // test case data
            [true, ['a' => 1, 'b' => 2, 3], false],
            [false, ['a' => 1, 'b' => 2, 3], true],
            [true, ['a' => 1, 'b' => 2, 'c' => 3], false],
            [true, [], true],
            [false, [], false],
        ];
    }

    /**
     * @dataProvider provider_map
     *
     * @param $aa
     * @param $i
     * @param $v
     * @param $expected
     */
    function test_map($aa, $i, $v, $expected) {
        $result = ArrayUtils::map($aa, $i, $v);
        $this->assertEquals($expected, $result);
    }

    function provider_map() {
        return [
            // test case data
            [['a' => [1, 2, 3], 'b' => [11, 21, 31], 3 => [91, 92, 93]], null, null, ['a' => [1, 2, 3], 'b' => [11, 21, 31], 3 => [91, 92, 93]]],
            [['a' => [1, 2, 3], 'b' => [11, 21, 31], 3 => [91, 92, 93]], null, 1, ['a' => 2, 'b' => 21, 3 => 92]],
            [['a' => [1, 2, 3], 'b' => [11, 21, 31], 3 => [91, 92, 93]], 0, 2, [1 => 3, 11 => 31, 91 => 93]],
            3 => [['a' => [1, 2, 3], 'b' => [11, 21, 31], 3 => [91, 92, 93]], 'A', 'B', []],
            [['a' => ['A' => 1, 'B' => 2, 'C' => 3], 'b' => [11, 21, 31], 3 => [91, 92, 93]], 'A', 'B', [1 => 2]],
            [
                ['a' => ['A' => 1, 'B' => 2, 'C' => 3], 'b' => [11, 21, 31], 3 => [91, 4 => 92, 93]],
                function ($o) {
                    return isset($o[0]) ? $o[0] + 100 : null;
                },
                function ($o) {
                    return isset($o[1]) ? $o[1] + 200 : null;
                },
                [111 => 221, 191 => null]
            ],
        ];
    }

    /**
     * @dataProvider provider_rreplace
     */
    function test_rreplace($aa, $expected) {
        $rr = ['~(\d+)~' => '[$1]', '~A~' => 'B'];
        $result = ArrayUtils::rreplace($aa, $rr);
        $this->assertEquals($expected, $result);
    }

    function provider_rreplace() {
        return [
            ['Auto23', 'Buto[23]'],
            [
                ['Auto23', 'A', ['49']],
                ['Buto[23]', 'B', ['[49]']]
            ],
        ];
    }

    /**
     * @dataProvider provider_shiftDefault
     * @param $aa
     * @param $def
     * @param $expected
     * @param $bb
     */
    function test_shiftDefault($aa, $def, $expected, $bb) {
        $result = ArrayUtils::shiftDefault($aa, $def);
        $this->assertEquals($expected, $result);
        $this->assertEquals($bb, $aa);
    }

    function provider_shiftDefault() {
        return [
            [[1, 2, 3], 0, 1, [2, 3]],
            [[], 'A', 'A', []],
        ];
    }

    /**
     * @throws InternalException
     */
    function test_toArray() {
        $aa = ['a' => 1, 'b' => 2, 'c' => 13];
        $obj = new stdClass();
        foreach ($aa as $k => $v) $obj->$k = $v;

        $pp = ['stdClass' => ['a', 'x' => 'b', 'h' => function ($o) {
            return $o->a + $o->b;
        }]];
        $expected = ['a' => 1, 'x' => 2, 'h' => 3];
        $result = ArrayUtils::toArray($obj, $pp);
        $this->assertEquals($expected, $result);
    }
}
