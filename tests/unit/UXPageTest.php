<?php

require_once dirname(__DIR__) . '/_support/Helper/UnitTest.php';

class UXPageTest extends UnitTest {

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @throws
     */
    protected function _before() {
        UApp::$config = [
            'baseurl' => 'https://test.uhisoft.hu/app/',
            'datapath' => dirname(__DIR__) . '/_output',
            'xslpath' => dirname(__DIR__) . '/_data/xsl',
            'uapp' => [
                'path' => dirname(dirname(__DIR__)),
            ],
        ];

        $_SERVER['REQUEST_URI'] = 'http://test.uapp.uhisoft.hu/uxpagetest';
        $_SERVER['HTTP_USER_AGENT'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0';
        $_SERVER['HTTPS'] = 'off';
        $_SERVER['HTTP_HOST'] = 'test.uapp.uhisoft.hu';
        $_SERVER['SERVER_NAME'] = 'localhost';
        $_SERVER["SERVER_PORT"] = 80;
        $_SERVER['SCRIPT_NAME'] = 'UXPageTest.php';
    }

    /**
     * @throws ConfigurationException
     * @throws UAppException
     */
    function tesCertifyAgent() {
        $this->assertEquals(true, UXPage::certifyAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0'));
        $this->assertEquals(false, UXPage::certifyAgent('(Unknown browser'));
    }

    /**
     * @throws ConfigurationException
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     */
    function testRegister() {
        $page = new UXPage();
        $this->assertEquals(true, $page->register('UList'));

    }

    /**
     * @throws ConfigurationException
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     */
    function testAddMenu() {
        $page = new UXPage();
        $menudata = [[1, 'New item', 'newitem']];
        $expected = '<menu><item enabled="1" caption="New item" url="newitem"/></menu>';
        /** @noinspection PhpDeprecationInspection */
        $this->assertXmlEquals($expected, $page->addMainMenu($menudata));
    }


    /**
     * @throws ConfigurationException
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     */
    function testRender() {
        // Prepare page
        $page = new UXPage();
        $page->xsl = 'uxpagetest';

        // Catch the render
        ob_start();
        $page->render();
        $result = ob_get_contents();
        ob_end_clean();

        // Check result
        $tracex = Debug::$trace ? ' trace="test-trace"' : '';
        $paramsx = Debug::$trace ? "<params/>\n    <debug>test-trace</debug>" : "<params/>";
        $expected = str_replace("\r", "", /** @lang XML */ '<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="https://test.uhisoft.hu/app//xsl/uxpagetest.xsl"?>
<data script="https://test.uhisoft.hu/app//uxpagetest" url="http://test.uapp.uhisoft.hu/uxpagetest" session_id=""' . $tracex . '>
  <head>
    <base href="https://test.uhisoft.hu/app/"/>
  </head>
  <content/>
  <control>
    ' . $paramsx . '
    <agent xml="Y" name="Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0"/>
  </control>
</data>
');
        $this->assertEquals($expected, $result, Util::diff($expected, $result));
    }

}
