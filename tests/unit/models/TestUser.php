<?php

/**
 * # TestUser
 * Example for a base datamodel
 *
 * @property int $descr -- attribute with getter and setter
 * @property-read int $other -- attribute without getter and setter
 */
class TestUser extends BaseModel {
    public $id;
    public $name;
    public $email;
    public $loggedin;
    public $lastclick;
    public $lastlogin;
    public $uid;
    public $org;
    public $idp;
    public $enabled;
    public $logins;

    private $_descr;

    public static function attributes() {
        return array_merge(parent::attributes(), ['descr', 'other']);
    }

    public static function fields() {
        $f = array_merge(parent::attributes(), ['descr']); // Except of other
        return array_combine($f, $f);
    }

    public static function attributeTypes($connection = null) {
        return [
            'id' => 'integer',
            'name',
            'email',
            'loggedin' => '',
            'lastclick' => 'DateTime',
            'lastlogin' => 'DateTime',
            'uid',
            'org',
            'idp',
            'enabled',
            'logins'
        ];
    }

    public static function rules() {
        return [
            ['custom', ['uid', 'email']],
            'id' => ['integer'],
            'name' => ['mandatory', ['length', 3, 15]],
            'email' => ['email', ['type', 'string']],
            'idp' => ['url'],
            'uid' => [['if', 'idp', null, ['empty']]],
            'descr' => ['xml'],
            'logins' => ['int'],
        ];
    }

    /**
     * Samlpe global validator for two fields.
     *
     * @param string $fieldname
     * @param array $fieldnames -- uid, email
     *
     * @return bool|false -- passes if both null, or uid is user part of email
     * @throws InternalException
     */
    public function customValidate($fieldname, $fieldnames) {
        if ($fieldname || !is_array($fieldnames) || count($fieldnames) != 2)
            throw new InternalException('Can only be used as global validator with two fieldnames');
        Debug::tracex('fieldnames', $fieldnames);
        $fieldname1 = $fieldnames[0];
        $fieldname2 = $fieldnames[1];
        $value1 = $this->getAttribute($fieldname1);
        $value2 = $this->getAttribute($fieldname2);
        Debug::tracex('values', [$value1, $value2]);
        if ($value1 === null && $value2 === null) return true;
        if (substr($value2, 0, strlen($value1) + 1) !== $value1 . '@') {
            $label2 = self::attributeLabel($fieldname2);
            return $this->setError($fieldname1, 'must be user part of ' . $label2);
        }
        return true;
    }

    public function getDescr() {
        return $this->_descr;
    }

    public function setDescr($value) {
        $this->_descr = $value;
        return $this;
    }
}
