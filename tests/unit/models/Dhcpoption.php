<?php

/**
 * Dhcpoption model
 *
 * @package rr.dev
 * @author Peter Uherkovich
 * @copyright 2018
 *
 * @property int $host references Dhcphost
 * @property int $option references Dhcpoptionname
 * @property int $num
 * Serial number of option value in the host
 * @property string $value
 */
class Dhcpoption extends Model {

    /**
     * Returns the array of primary key fields
     *
     * @return array of PK fieldnames
     */
    public static function primaryKey() {
        return ['host', 'option', 'num'];
    }

    /**
     * Returns autoincrement fields.
     * @return array -- empty array currently
     */
    public static function autoIncrement() {
        return [];
    }

    /**
     * Returns an associative array with foreign key symbolic names mapped to modelnames and field mappings
     *
     * @return array of keyname => array(modelname, reference_field=>foreign_field)
     */
    public static function foreignKeys() {
        return [
            // name => array(modelname, foreign_id=>reference_field)
            'host1' => ['Dhcphost', 'host' => 'id'],
            'option1' => ['Dhcpoptionname', 'option' => 'id'],
        ];
    }

    /**
     * Must return the attribute labels.
     * Attribute labels are mainly used for display purpose
     * Order of labels is the default order of fields.
     * @return array attribute labels (name => label)
     */
    public static function attributeLabels() {
        return [
            'host' => 'Host',
            'option' => 'Option',
            'num' => 'Sorszám',
            'value' => 'Érték',
        ];
    }

    /**
     * Returns validation rules for fields
     * Models are validated against rules before saving to database
     *
     *  - rule -- numeric indexed rules are global rule, e.g. unique for multiple fields
     *  - fieldname => array(rule, rule...) -- fieldname indexed rules are for single field
     *
     * Rule:
     *
     *    - rulename
     *  - array(rulename, arguments, ...)
     *
     * Rulename always refers to method `rulenameValidate` and will be added to the field class as `rule-rulename`
     * Arguments will be added to field as data-rulename-data (always an array, for all remaining arguments)
     * Associative arguments 'key'=>value will be used in php validator method as normal argument,
     * but in forms will be added as data-rulename-key and excluded from data-rulename.
     *
     * Global rules:
     *
     *    - 'unique' fieldnamelist
     *    - custom arglist -- any custom name refers to validateCustom(array $arglist) method
     *
     * Field rules:
     *
     *      - 'unique' -- field is unique without condition
     *    - 'not null' -- field is not null
     *    - 'mandatory' -- field is not null and not empty string
     *    - typenames (boolean, int, float, number, string, xml, time, date, datetime, )
     *      - ['length', min, max]
     *    - ['pattern', pattern(s)] -- valid if at least one of RE patterns is valid (second level: all of them)
     *      - ['type', typename]
     *    - ['between', lower, upper] -- valid value between (including) limits
     *    - [custom, arglist] -- any custom name refers to validateCustom($fieldname, array $arglist) method.
     *
     * `rulenameValidate` functions:
     *
     *    - must accept parameters (fieldname, arg1, arg2, ...) where fieldname is null on global calls.
     *  - first element of args is custom message or null if default should be used.
     *  - $1 is the place of the fieldname in the message
     *  - other elments of args are rule data, e.g. RegExp pattern for pattern rule.
     *  - must return boolean
     *  - may set error text of the invalid field on the form by setError(fieldname, message)
     *
     * @return array
     * @see validate()
     */
    public static function rules() {
        return [
            [['unique', ['host', 'option', 'num']]],
            'num' => [['autoincrement', ['host', 'option']]],
        ];
    }

}
