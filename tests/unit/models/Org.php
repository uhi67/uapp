<?php

/**
 * Model for org table
 *
 * @property int $id
 * @property int $parent
 * @property string $name
 * @property int $orgid
 * @property string $descr
 * @property int $type
 * @property DateTime $created
 * @property int $deleter
 * @property DateTime $deleted
 */
class Org extends Model {
    public static function foreignKeys() {
        return [
            // name => array(modelname, foreign_id=>reference_field)
            'parent1' => ['Org', 'parent' => 'id'],
            'deleter1' => ['Person', 'deleter' => 'id'],
        ];
    }
}
