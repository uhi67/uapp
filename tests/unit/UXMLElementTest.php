<?php
/**
 * Created by PhpStorm.
 * User: uhi
 * Date: 2018. 09. 14.
 * Time: 20:05
 */
require_once "models/Dhcpdomain.php";
require_once "models/Dhcpoption.php";
require_once "models/Dhcpoptionname.php";
require_once "models/Dhcpoptiontype.php";
require_once "models/Person.php";

require_once dirname(__DIR__) . '/_support/Helper/UnitTest.php';

class UXMLElementTest extends UnitTest {
    /** @var UnitTester */
    protected $tester;

    /** @var DBX */
    private $db;
    /** @var UXMLDoc */
    private $xmldoc;
    /** @var UXMLElement */
    private $data;

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @throws InternalException
     * @throws ReflectionException
     */
    protected function _before() {
        mb_internal_encoding("UTF-8");
        $testdbconfig = require(dirname(__DIR__) . '/dbconfig.php');
        $this->db = Component::create($testdbconfig);

        $this->xmldoc = new UXMLDoc('UTF-8', 'data', null, 'hu');
        $this->data = $this->xmldoc->documentElement;
        UApp::setLang('hu');
        UApp::$config = [
            'uapp' => [
                'path' => dirname(dirname(__DIR__)),
            ],
        ];
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    protected function _after() {
    }


    /**
     * @dataProvider providerAddNode
     *
     * @param string|array $name
     * @param array $attr
     * @param array $cont
     * @param string|array $ns
     * @param string $expected
     * @param int $nodes
     *
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     */
    function testAddNode($name, $attr, $cont, $ns, $expected, $nodes) {
        $node_result = $this->data->addNode($name, $attr, $cont, $ns);
        $this->assertEquals($nodes, $node_result->childNodes->length);
        $this->assertXmlEquals($expected, $node_result);
    }

    function providerAddNode() {
        return [
            // Without attributes and content
            ['node', null, null, null, '<node/>', 0],
            // Empty subnode
            ['node', [['sub']], null, null, '<node><sub/></node>', 1],
            // Without namespace
            ['node0',
                ['alma' => 1, 'sub1' => ['banán', 'citrom']],
                'Tartalom',
                null,
                '<node0 alma="1"><sub1>banán</sub1><sub1>citrom</sub1>Tartalom</node0>', 3
            ],
            // Simple namespace
            2 => ['node1',
                ['alma' => 1, 'sub1' => ['banán', 'citrom']],
                'Tartalom',
                'https://ns1.uhisoft.hu',
                '<node1 xmlns="https://ns1.uhisoft.hu" alma="1"><sub1>banán</sub1><sub1>citrom</sub1>Tartalom</node1>', 3
            ],
            // Complex namespace without attribute and content
            ['ns1:node2',
                null,
                null,
                ['ns1' => 'https://ns1.uhisoft.hu', 'ns2' => 'https://ns2.uhisoft.hu'],
                '<ns1:node2 xmlns:ns1="https://ns1.uhisoft.hu"/>', 0
            ],
            // Complex namespace with attribute
            4 => ['ns1:node2',
                ['ns2:alma' => 1],
                'Tartalom',
                ['ns1' => 'https://ns1.uhisoft.hu', 'ns2' => 'https://ns2.uhisoft.hu'],
                '<ns1:node2 xmlns:ns1="https://ns1.uhisoft.hu" xmlns:ns2="https://ns2.uhisoft.hu" ns2:alma="1">Tartalom</ns1:node2>', 1
            ],
            // Complex namespace with subnodes
            ['ns1:node2',
                ['alma' => 1, 'ns2:sub1' => ['banán', 'citrom']],
                'Tartalom',
                ['ns1' => 'https://ns1.uhisoft.hu', 'ns2' => 'https://ns2.uhisoft.hu'],
                '<ns1:node2 xmlns:ns1="https://ns1.uhisoft.hu" alma="1"><ns2:sub1 xmlns:ns2="https://ns2.uhisoft.hu">banán</ns2:sub1><ns2:sub1 xmlns:ns2="https://ns2.uhisoft.hu">citrom</ns2:sub1>Tartalom</ns1:node2>', 3
            ],
            // Complex namespace with complex subcontent
            ['ns1:node2',
                ['alma' => 1, 'ns2:sub1' => [
                    ['id' => 1, 'banán', 'ns2:sss' => ['Bent1']],
                    ['id' => 2, 'citrom', 'sss' => ['Bent2']],
                ]],
                'Tartalom',
                ['ns1' => 'https://ns1.uhisoft.hu', 'ns2' => 'https://ns2.uhisoft.hu'],
                '<ns1:node2 xmlns:ns1="https://ns1.uhisoft.hu" alma="1"><ns2:sub1 xmlns:ns2="https://ns2.uhisoft.hu" id="1">banán<ns2:sss>Bent1</ns2:sss></ns2:sub1><ns2:sub1 xmlns:ns2="https://ns2.uhisoft.hu" id="2">citrom<sss>Bent2</sss></ns2:sub1>Tartalom</ns1:node2>', 3
            ],
        ];
    }

    /**
     * @throws InternalException
     */
    function testAddDoc() {
        $insert = '<node1 name="Ábel">Bálint</node1><node2/>Ábel';
        $doc = UXMLDoc::createFragment($insert);
        /** @noinspection PhpDeprecationInspection */
        $this->xmldoc->documentElement->addDoc($doc);
        $expected = /** @lang XML */
            '<data><node1 name="Ábel">Bálint</node1><node2/>Ábel</data>';
        $this->assertEquals(UXMLDoc::formatFragment($expected), $this->xmldoc->saveXML($this->xmldoc->documentElement));
    }

    /**
     * @dataProvider providerQuery
     *
     * @param $name
     * @param $sql
     * @param $params
     * @param $valuenodes
     * @param $expected
     *
     * @throws ConfigurationException
     * @throws DatabaseException
     * @throws InternalException
     */
    function testQuery($name, $sql, $params, $valuenodes, $expected) {
        if (is_array($sql)) {
            $sql = new Query($sql);
            $sql->setDb($this->db);
            $model = $sql->from;
            if (is_array($model)) $model = $model[0];
            $this->assertEquals(strtolower($model), $this->db->getTableName($model));
            //$this->assertEquals('*', $sql->sql);

        }
        $node_result = $this->data->query($this->db, $name, $sql, $params, $valuenodes);
        $result = $this->xmldoc->saveXML($node_result);
        $this->assertEquals($expected, $result);
    }

    function providerQuery() {
        return [
            // $name, $sql, $params, $valuenodes, $expected
            ['item-0', 'SELECT "id", "name", "descr", "datatype", "multiple" FROM "dhcpoptionname" ORDER BY "id"', null, false,
                '<item-0 id="6" name="domain-name-servers" descr="Ennek egy text mező az értéke, ahol vesszővel fel vannak sorolva a nameserverek valahogy így néz ki:  option domain-name-servers 193.6.50.100, 10.128.2.2;" datatype="7" multiple="0"/>',
            ],
            ['item-1', ['type' => 'select', 'from' => 'Dhcpoptionname', 'fields' => ['id', 'name', 'descr', 'datatype', 'multiple'], 'orders' => ['id']], null, false,
                '<item-1 id="6" name="domain-name-servers" descr="Ennek egy text mező az értéke, ahol vesszővel fel vannak sorolva a nameserverek valahogy így néz ki:  option domain-name-servers 193.6.50.100, 10.128.2.2;" datatype="7" multiple="0"/>',
            ]
        ];
    }

    /**
     * @dataProvider provCreateSubnode
     * @param $tagname
     * @param $value
     * @param $snf
     * @param $expected
     *
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     */
    function testCreateSubnode($tagname, $value, $snf, $expected) {
        $node = $this->xmldoc->documentElement->addNode('node');
        /** @noinspection PhpDeprecationInspection */
        $node->createSubnode($tagname, $value, $snf);

        $expected = UXMLDoc::createFragment($expected);
        $expected = $expected->saveXML($expected->firstChild->firstChild);

        $this->assertEquals($expected, $this->xmldoc->saveXML($node));
    }

    function provCreateSubnode() {
        return [
            ['egy', null, false, '<node/>'],
            ['egy', 'szöveg', false, '<node><egy>szöveg</egy></node>'],
            ['egy', '<b>szöveg</b>', true, '<node><egy><b>szöveg</b></egy></node>'],
            ['egy', ['alma', 'körte'], null, '<node><egy>almakörte</egy></node>'],
            ['egy', ['alma' => 1, 'körte' => 2], null, '<node><egy alma="1" korte="2"/></node>'],
        ];
    }


    /**
     * @dataProvider provAddContent
     *
     * @param array|string $nodeData
     * @param array $namespace
     * @param string $expected
     *
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     */
    function testAddContent($nodeData, $namespace, $expected) {
        $node = $this->xmldoc->documentElement->addNode('node');
        $node->addContent($nodeData, $namespace);
        $this->assertXmlEquals($expected, $node);
    }

    function provAddContent() {
        return [
            [
                'Csak szöveg',
                null,
                '<node>Csak szöveg</node>'
            ],
            [
                ['id' => 1, 'name' => 'Egy', 'child' => ['Ábel', 'Áron'], 'Jó szülő'],
                null,
                '<node id="1" name="Egy"><child>Ábel</child><child>Áron</child>Jó szülő</node>'
            ],
            [
                [
                    ['child', 'cid' => 11, 'cname' => 'Ábel', 'Jó gyerek Ábel'],
                    ['child', 'cid' => 12, 'cname' => 'Bálint', 'Kis gyerek'],
                    ['ns2:child', 'cid' => 13, 'cname' => 'Csaba', 'Mostohagyerek'],
                    [['ns3:ch3', 'https://uhisoft.hu/ns3'], 'cid' => 14, 'Névteres'],
                    ['Kóbor Dezső'],
                ],
                ['ns2' => 'https://uhisoft.hu/ns2'],
                '<node><child cid="11" cname="Ábel">Jó gyerek Ábel</child><child cid="12" cname="Bálint">Kis gyerek</child><ns2:child xmlns:ns2="https://uhisoft.hu/ns2" cid="13" cname="Csaba">Mostohagyerek</ns2:child><ns3:ch3 xmlns:ns3="https://uhisoft.hu/ns3" cid="14">Névteres</ns3:ch3><Kobor_Dezso/></node>'
            ],
            [
                ['id' => 1, 'name' => 'Egy', 'child' => [
                    ['cid' => 11, 'cname' => 'Ábel', 'Jó gyerek'],
                    ['cid' => 12, 'cname' => 'Bálint', 'Kis gyerek'],
                ], 'Jó szülő'],
                ['ns2' => 'https://uhisoft.hu/ns2'],
                '<node id="1" name="Egy"><child cid="11" cname="Ábel">Jó gyerek</child><child cid="12" cname="Bálint">Kis gyerek</child>Jó szülő</node>'
            ],
        ];
    }

    /**
     * @dataProvider provAddNodePathIfNotExists
     *
     * @param string $xml
     * @param string $spec
     * @param array[] $attr
     * @param string[] $content
     * @param string $expected
     *
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     */
    function testAddNodePathIfNotExists($xml, $spec, $attr, $content, $expected) {
        $doc = new UXMLDoc();
        $doc->loadXML($xml);
        $parent = $doc->documentElement;

        $r = $parent->addNodePathIfNotExists($spec, $attr, $content);

        $this->assertNotNull($r);
        $this->assertEquals(UXMLDoc::formatFragment($expected), $doc->saveXML($doc->documentElement));
    }

    function provAddNodePathIfNotExists() {
        return [ //$xml, $spec, $attr, $content, $result
            ['<root></root>', 'alma', null, null, '<root><alma/></root>'],
            ['<root><alma/></root>', 'alma/banan', [null, ['id' => 2]], null, '<root><alma><banan id="2"/></alma></root>'],
            ['<root></root>', 'alma/banan', [null, ['id' => 2]], ['gala'], '<root><alma>gala<banan id="2"/></alma></root>'],
            ['<root><alma id="1"><banan/></alma><alma id="2"><citrom/></alma></root>', 'alma[@id="1"]/banan', null, null, '<root><alma id="1"><banan/></alma><alma id="2"><citrom/></alma></root>'],
            ['<root><alma id="1"><banan/></alma><alma id="2"><citrom/></alma></root>', 'alma[@id="2"]/banan', null, null, '<root><alma id="1"><banan/></alma><alma id="2"><citrom/><banan/></alma></root>'],
            ['<root><alma id="1"><banan/></alma><alma id="2"><citrom/></alma></root>', 'alma[@id="3"]/korte', [['id' => 31]], null, '<root><alma id="1"><banan/></alma><alma id="2"><citrom/></alma><alma id="31"><korte/></alma></root>'],
        ];
    }

    /**
     * @dataProvider provCreateNode
     *
     * @param array $values
     * @param array $options
     * @param string $expected
     *
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     */
    function testCreateNodes($values, $options, $expected) {
        $node = $this->xmldoc->documentElement->addNode('node');
        $node->createNodes($values, $options);
        /** @var DOMElement $expected */
        $compare = Util::diff(UXMLDoc::formatFragment($expected), UXMLDoc::formatFragment($this->xmldoc->saveXML($node)));
        $this->assertXmlEquals($expected, $node, $compare);
    }

    function provCreateNode() {
        return [ // $values, $options, $expected
            [
                [
                    ['id' => 11, 'a'],
                    ['id' => 12, 'b'],
                    ['id' => 13],
                ],
                [
                    'nodeName' => 'elem',
                    'attributes' => ['eid' => 'id', 'name' => 0],
                    'contentField' => 0,
                    'contentValue' => 'default',
                    'subnodeFields' => ['sub' => function ($index, $item) {
                        $result = [];
                        for ($i = 0; $i < $index; $i++)
                            $result[] = ['sid' => $i + 1, $item['id'] + 100 * ($i + 1)];
                        return $result;
                    }],
                    'extra' => [
                        'extra' => function (/** @noinspection PhpUnusedParameterInspection */ $index, $item) {
                            return Util::objtostr($item);
                        }
                    ],
                ],
                /** @lang XML */ '<?xml version="1.0" encoding="UTF-8" ?>
				<node>
					<elem eid="11" name="a" extra="[id:11, 0:\'a\']">a</elem>
					<elem eid="12" name="b" extra="[id:12, 0:\'b\']">
						<sub sid="1">112</sub>b</elem>
					<elem eid="13" extra="[id:13]">
						<sub sid="1">113</sub>
						<sub sid="2">213</sub>default</elem>
				</node>'
            ]
        ];
    }


    /**
     * @dataProvider provAddXmlContent
     * @param string $target
     * @param string $type
     * @param string $content
     * @param bool $omitroot
     * @param string $expected
     *
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     */
    function testAddXmlContent($target, $type, $content, $omitroot, $expected) {
        $node_target = $this->xmldoc->documentElement->addNode($target);
        switch ($type) {
            case 'xml':
                $node_target->addXmlContent($content, $omitroot);
                break;
            case 'doc':
                $contentDoc = UXMLDoc::createFragment($content);
                $node_target->addXmlContent($contentDoc, $omitroot);
                break;
            case 'node':
                $contentNode = UXMLDoc::createFragment($content)->documentElement->firstChild;
                $node_target->addXmlContent($contentNode, $omitroot);
        }
        $this->assertXmlEquals($expected, $node_target);
    }

    function provAddXmlContent() {
        return [ // $target, $type, $content, $expected
            ['node1', 'xml', '<item id="1" name="Ábel">Káin</item><item>Áron</item>', false,
                '<node1><item id="1" name="Ábel">Káin</item><item>Áron</item></node1>'],
            ['node1', 'node', '<item id="1" name="Ábel"><subitem>Áron</subitem>Káin</item>', false,
                '<node1><item id="1" name="Ábel"><subitem>Áron</subitem>Káin</item></node1>'],
            ['node1', 'doc', '<item id="1" name="Ábel">Káin</item><item>Áron</item>', false,
                '<node1><root><item id="1" name="Ábel">Káin</item><item>Áron</item></root></node1>'],
            ['node1', 'doc', '<item id="1" name="Ábel">Káin</item><item>Áron</item>', true,
                '<node1><item id="1" name="Ábel">Káin</item><item>Áron</item></node1>'],
        ];
    }

    /**
     * @dataProvider provLoadBitNodes
     * @param $bitarray
     * @param $nodename
     * @param $json
     * @param $expected
     *
     * @throws ConfigurationException
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     */
    function testLoadBitNodes($bitarray, $nodename, $json, $expected) {
        $node1 = $this->xmldoc->documentElement->addNode('bitnodes');
        $this->assertXmlEquals($expected, $node1->loadBitNodes($bitarray, $nodename, $json));
    }

    function provLoadBitNodes() {
        return [ // $bitarray, $nodename, $json, $expected
            [[
                0 => [1, 'bit0', '', ''],
                1 => [2, 'bit1', '', ''],
                2 => [4, 'bit2', '', ''],
            ], 'bit', 0, '<bitnodes><bit bit="0" value="1" name="bit0" descr="" const=""/><bit bit="1" value="2" name="bit1" descr="" const=""/><bit bit="2" value="4" name="bit2" descr="" const=""/></bitnodes>']
        ];
    }
}
