<?php

use Codeception\Test\Unit;

class PIntTest extends Unit {
    /**
     * @var UnitTester
     */
    protected $tester;

    protected function _before() {
    }

    protected function _after() {
    }

    // tests

    /**
     * @dataProvider packProvider
     *
     * @param int $int
     * @param string $expected
     * @return void
     */
    public function testPack($int, $packed) {
        $this->assertEquals($packed, PInt::pack($int));
        $this->assertEquals($packed === false ? false : $int, PInt::unpack($packed));
    }

    public function packProvider() {
        return [
            [0, ''],
            [1, chr(1)],
            [65, 'A'],
            [12851, '23'],
            [-1, false],
            [1.1, false],
        ];
    }

    /**
     * @dataProvider pandProvider
     *
     * @param int $int
     * @param string $expected
     * @return void
     */
    public function testPand($args, $expected) {
        $this->assertEquals($expected, PInt::pand($args));
        $this->assertEquals($expected, call_user_func_array(['PInt', 'pand'], $args));
    }

    public function pandProvider() {
        return [
            [[], ''],
            [['A'], 'A'],
            [['1A', 'C'], 'A'], // #3141, #43, #41
            [['6545', '123', '2345'], '101'],
        ];
    }

    /**
     * @dataProvider porProvider
     *
     * @param int $int
     * @param string $expected
     * @return void
     */
    public function testPor($args, $expected) {
        $this->assertEquals($expected, PInt::por($args));
        $this->assertEquals($expected, call_user_func_array(['PInt', 'por'], $args));
    }

    public function porProvider() {
        return [
            [[], ''],
            [["\x00"], ''],
            [['A'], 'A'],
            [['1A', 'B'], '1C'], // #3141, #43, #41
            [['6545', '123', '2345'], '6767'],
        ];
    }

    /**
     * @dataProvider invProvider
     *
     * @param int $v
     * @param string $expected
     * @return void
     */
    public function testInv($v, $expected) {
        $this->assertEquals(bin2hex($expected), bin2hex(PInt::inv($v)));
    }

    public function invProvider() {
        return [
            ['', ''],
            ["\x00", "\xFF"],
            ["\x0A", "\xF5"],
            ["\x11\x0A", "\xEE\xF5"],
        ];
    }
}
