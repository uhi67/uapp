<?php

use Codeception\Test\Unit;

/**
 * Class Utf8Test
 *
 * Based on source code encoded in UTF8
 *
 */
class Utf8Test extends Unit {

    /**
     * @dataProvider provider_valid
     *
     * @param string $ip
     * @param boolean $expected
     * @return void
     */
    function test_encode2($ansi, $expected, $upper) {
        $this->assertSame($expected, Utf8::encodeif($ansi));
        $this->assertSame($ansi, Utf8::decodeif($expected));
    }

    function provider_valid() {
        return [ // $ip, $expected
            ["\xF5", 'ő', 'Ő'],
            ["\xE1", 'á', 'Á'],
            ["t\xFBr", 'tűr', 'TŰR'],
            [utf8_decode('árvíz'), 'árvíz', 'ÁRVÍZ'],
            ["\xE1rv\xEDzt\xFBr\xF5 t\xFCk\xF6rf\xFAr\xF3g\xE9p", 'árvíztűrő tükörfúrógép', 'ÁRVÍZTŰRŐ TÜKÖRFÚRÓGÉP'],
            ["\xAFnin, \xA3\xF3dz, \xB6\xEA, \xFE", 'Żnin, Łódz, śę, ţ', 'ŻNIN, ŁÓDZ, ŚĘ, Ţ'],
        ];
    }

    /**
     * @dataProvider provider_valid
     *
     * @param string $ip
     * @param boolean $expected
     * @return void
     */
    function test_iconv($iso, $utf, $upper) {
        $this->assertSame($utf, iconv('ISO-8859-2', 'UTF-8', $iso));
        $this->assertSame($iso, iconv('UTF-8', 'ISO-8859-2', $utf));
        $this->assertSame($upper, mb_strtoupper($utf, 'UTF-8'));
    }

}
