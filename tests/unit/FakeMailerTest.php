<?php

use Codeception\Test\Unit;

/**
 * Created by PhpStorm.
 * User: uhi
 * Date: 2018. 10. 06.
 * Time: 20:53
 */
class FakeMailerTest extends Unit {
    /** @var FakeMailer $mailer */
    public $mailer;

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @throws InternalException
     * @throws ReflectionException
     */
    protected function _before() {
        $mailerConfig = [
            'class' => 'FakeMailer',
            'dir' => dirname(__DIR__) . '/_output/FakeMail',
        ];
        $this->mailer = Component::create($mailerConfig);
    }


    function testSend() {
        $address = 'uhi@uhisoft.hu';
        $subject = 'subject';
        $message = 'Hello, world!';
        $now = date('ymd_His');
        $result = $this->mailer->send($address, $subject, $message);
        $this->assertTrue($result);
        $dir = $this->mailer->dir;
        $this->assertEquals(dirname(__DIR__) . '/_output/FakeMail', $this->mailer->dir);
        $filename = $dir . '/' . $now . '_' . Util::toNameID($address) . '.eml';
        $this->assertTrue(file_exists($filename));
        $file = file($filename);
        $this->assertEquals('To: ' . $address . "\n", $file[0]);
        $this->assertEquals('Subject: ' . $subject . "\n", $file[1]);
        $this->assertEquals("\n", $file[2]);
        $this->assertEquals($message, $file[3]);
    }
}
