<?php

use Codeception\Test\Unit;

require_once "models/Dhcpdomain.php";
require_once "models/Dhcpoption.php";
require_once "models/Dhcpoptionname.php";
require_once "models/Dhcpoptiontype.php";
require_once "models/Dhcphost.php";
require_once "models/Org.php";
require_once "models/Person.php";

/** @noinspection PhpUndefinedClassInspection */

/** @noinspection PhpUndefinedNamespaceInspection */

class FormTest extends Unit {
    private $db;
    /** @var UXMLDoc $xmldoc */
    private $xmldoc;

    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @throws ConfigurationException
     * @throws InternalException
     * @throws ReflectionException
     */
    protected function _before() {
        $testdbconfig = require(dirname(__DIR__) . '/dbconfig.php');
        $this->db = Component::create($testdbconfig);
        $this->xmldoc = new UXMLDoc();
        UApp::setLang('hu');
        UApp::$config = [
            'uapp' => [
                'path' => dirname(dirname(__DIR__)),
            ],
        ];
    }

    protected function _after() {
    }


    /**
     * @dataProvider provCreateAttributeNode
     *
     * @param $attribute
     * @param $options
     * @param $expected
     *
     * @throws ConfigurationException
     * @throws InternalException
     * @throws ReflectionException
     */
    function testCreateAttributeNode($attribute, $options, $expected) {
        $model = new Dhcphost([
        ], $this->db);
        Model::$defaultConnection = $this->db;
        $form = new Form(null, [
            'model' => $model,
        ]);
        $form->createNode($this->xmldoc->documentElement, [], false);
        $node = $form->createAttributeNode($attribute, $options);
        $this->assertEquals('attribute', $node->nodeName);
        $this->assertEquals($expected, $node->ownerDocument->saveXML($node));
    }

    function provCreateAttributeNode() {
        return [ // $attribute, $options, $expected
            [
                'dhcpdomain', [], '<attribute id="dhcphost-dhcpdomain" name="dhcpdomain" type="integer" label="Tartomány" order="999" ref="dhcpdomain1" refmodel="Dhcpdomain" refname="name"/>',
            ]
        ];
    }


    /**
     * @throws ConfigurationException
     * @throws InternalException
     * @throws ReflectionException
     * @throws UAppException
     */
    function testExport() {
        $model = new Dhcphost($h1 = [
            'id' => 1,
            'name' => 'semmi',
            'created' => new DateTime('2018-12-31'),
        ], $this->db);
        $form = new Form(null, [
            'name' => $name = 'form1',
            'model' => $model,
        ]);

        $data = ['name' => 'valami', 'mac' => '112233556688', 'ip' => '12.23.56.89'];
        $_REQUEST = [$name => array_merge($data, ['_form' => $form->id])];

        $this->assertTrue($form->posted);

        $r = $form->export($model);
        $this->assertEquals([], $model->errors);
        $this->assertTrue($r);
        $expected = ['id' => 1, 'name' => 'valami', 'created' => new DateTime('2018-12-31'), 'mac' => new Macaddr('112233556688'), 'ip' => new Inet('12.23.56.89')];
        $result = $model->toArray();
        $kk = array_keys($expected);
        foreach ($result as $key => $value) if (!in_array($key, $kk)) unset($result[$key]);
        $this->assertEquals($expected, $result);


    }

}
