<?php

use Codeception\Test\Unit;

require_once "models/Systext.php";

class CacheTest extends Unit {
    /** @var DBX $db */
    private $db;
    /** @var Cache $cache */
    private $cache;

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @throws
     */
    protected function _before() {
        $testdbconfig = require(dirname(__DIR__) . '/dbconfig.php');
        UApp::$config = [
            'baseurl' => 'https://test.uhisoft.hu/app/',
            'datapath' => dirname(__DIR__) . '/_output',
            'uapp' => [
                'path' => dirname(dirname(__DIR__)),
            ],
        ];
        $this->cache = new Cache([
            'path' => UApp::$config['datapath'] . '/cache',        // Writeable path to store cache files
            'enabled' => 1,                        // Cache is enabled
            'ttl' => 2,                        // Default time-to-live in seconds
            'hash' => 2
        ]);
    }

    /**
     * @throws
     */
    function testGet() {
        // Unknown item
        $this->assertNull($this->cache->get('unknown'));

        // Expired item from previous test
        $this->assertNull($this->cache->get('new_item_1'));

        // Storing and retrieving
        $this->assertTrue($this->cache->set('new_item_1', 'new_value_1', 1000) !== false);
        $this->assertEquals('new_value_1', $this->cache->get('new_item_1'));
        $this->assertTrue($this->cache->set('new_item_2', 'new_value_2') !== false);
        $this->assertEquals('new_value_2', $this->cache->get('new_item_2'));
        $this->assertEquals('new_value_1', $this->cache->get('new_item_1'));

        // Purge
        $this->cache->purge('~2~');
        $this->assertEquals('new_value_1', $this->cache->get('new_item_1', -999)); // Még nem járt le
        $this->assertNull($this->cache->get('new_item_2'));
        $this->assertNull($this->cache->get('new_item_1', -1001)); // Így már lejárt (törli, de az üres dir marad)
        $this->cache->purge('~2~'); // Törli az üres directorykat mind
    }

}
