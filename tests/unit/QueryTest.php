<?php

use Codeception\Test\Unit;

require_once "models/City.php";
require_once "models/Dhcphost.php";
require_once "models/Dhcpdomain.php";
require_once "models/Dhcpoption.php";
require_once "models/Dhcpoptionname.php";
require_once "models/Dhcpoptiontype.php";
require_once "models/Person.php";
require_once "models/Org.php";

/**
 * QueryTest -- Testing Query class
 */
class QueryTest extends Unit {
    /** @var DBX $db */
    private $db;
    /** @var UXMLDoc $xmldoc */
    private $xmldoc;
    /** @var UnitTester $tester */
    protected $tester;

    /** @noinspection PhpMethodNamingConventionInspection */
    /**
     * @throws InternalException
     * @throws ReflectionException
     */
    protected function _before() {
        $testdbconfig = require(dirname(__DIR__) . '/dbconfig.php');
        $this->db = Component::create($testdbconfig);
        $this->xmldoc = new UXMLDoc();
//	    date_default_timezone_set('Europe/Budapest');
    }

    /** @noinspection PhpMethodNamingConventionInspection */
    protected function _after() {
    }

    /** @noinspection PhpMethodNamingConventionInspection */
    /**
     * @dataProvider provider_createSelect
     *
     * @param string $expected -- expected raw SQL without parameters
     * @param string $model
     * @param array $fields
     * @param array $joins
     * @param array $condition
     * @param array $orders
     * @param int $limit
     * @param int $offset
     * @param array $params
     * @param string $r
     * @param array $selectexpected -- expected result of the select (all records)
     *
     * @return void
     * @throws DatabaseException
     * @throws InternalException
     * @throws QueryException
     * @throws UAppException
     */
    function test_createSelect($expected, $model, $fields, $joins, $condition, $orders, $limit, $offset, $params, $r, $selectexpected) {
        if (is_array($model) && $fields == null) {
            $query = Query::createSelect($model);
            $query->connection = $this->db;
        } else $query = Query::createSelect($model, $fields, $joins, $condition, $orders, $limit, $offset, $params, $this->db);

        $expected = preg_replace('~\s+~', ' ', $expected);
        $sql = $this->db->buildSQL($query);
        $this->assertEquals($expected, $sql, Util::diff($expected, $sql));

        $result = $query->sql;
        #$result = preg_replace('/\s{2,}/', ' ', trim($result));
        #$this->compare($expected, $result);
        $this->assertEquals($expected, $result);

        if ($r) {
            $f = preg_replace('/\s{2,}/', ' ', trim($query->finalSQL));
            #Debug::trace($r);
            #Debug::trace($f);
            $this->assertEquals($r, $f);
        }

        if ($selectexpected) {
            $selectresult = $query->all(false);
            #$this->compare($selectexpected, $selectresult);
            $this->assertEquals($selectexpected, $selectresult);

            $selectresult = $query->selectAll();
            $valuesonly = array_map(function ($row) {
                return array_values($row);
            }, $selectexpected);
            #$this->compare($valuesonly, $selectresult);
            $this->assertEquals($valuesonly, $selectresult);
        }
    }

    /** @noinspection PhpMethodNamingConventionInspection */
    function provider_createSelect() {
        /** @noinspection PhpUnhandledExceptionInspection */
        return [
            // $expected, $model, $fields, $joins, $condition, $orders, $limit, $offset, $params, $r, $selectexpected
            0 => ['SELECT "host", "num", "option", "value" FROM "dhcpoption"',
                'Dhcpoption', null, null, null, null, null, null, null, null, null
            ],
            1 => ['SELECT "id", "name" FROM "dhcpoptiontype" WHERE (id=1) ORDER BY "id"',
                'Dhcpoptiontype', ['id', 'name'], null, ['(id=1)'], ['id'], null, null, null,
                null,
                [['id' => 1, 'name' => 'boolean']],
            ],
            2 => ['SELECT "id", "descr" FROM "dhcpoptiontype" WHERE "id" = $1 ORDER BY "id" LIMIT 1 OFFSET 10',
                'Dhcpoptiontype', ['id', 'descr'], null, ['id' => '$1'], ['id'], 1, 10, ['banán'],
                'SELECT "id", "descr" FROM "dhcpoptiontype" WHERE "id" = \'banán\' ORDER BY "id" LIMIT 1 OFFSET 10',
                null
            ],
            3 => ['SELECT "descr" FROM "dhcpoptiontype" WHERE "id" = $var',
                'Dhcpoptiontype', ['descr'], null, ['id' => '$var'], null, null, null, ['var' => 'banán'],
                'SELECT "descr" FROM "dhcpoptiontype" WHERE "id" = \'banán\'',
                null
            ],
            4 => ['SELECT "descr" FROM "dhcpoptiontype" WHERE "id" = $var AND "name" = $1',
                'Dhcpoptiontype', ['descr'], null, ['id' => '$var', 'name' => '$1'], null, null, null, ['var' => 'banán', 'alma'],
                'SELECT "descr" FROM "dhcpoptiontype" WHERE "id" = \'banán\' AND "name" = \'alma\'',
                null
            ],
            5 => [/** @lang text */ 'SELECT "dhcpoption"."host", "dhcpoption"."name", "c"."name" AS "c_name" FROM "dhcpoption" LEFT JOIN "dhcpoptionname" "c" ON "dhcpoption"."option" = "c"."id" WHERE "c"."name" IS NOT NULL',
                'Dhcpoption', ['host', 'name', 'c.name'], ['c' => 'option1'], ['IS NOT NULL', 'c.name'], null, null, null, null,
                null,
                null
            ],
            6 => [
                /** @lang text */ 'SELECT "dhcpoption"."host", "dhcpoption"."name", "c"."name" AS "c_name" FROM "dhcpoption" LEFT JOIN "dhcpoptionname" "c" ON "dhcpoption"."option" = "c"."id" WHERE "c"."name" IS NOT NULL',
                [
                    'modelname' => 'Dhcpoption',
                    'fields' => ['host', 'name', 'c.name'],
                    'joins' => ['c' => 'option1'],
                    'condition' => ['IS NOT NULL', 'c.name']
                ],
                null, null, null, null, null, null, null, null, null
            ],
            7 => [ //$expected, $model, $fields, $joins, $condition, $orders, $limit, $offset, $params, $r, $selectexpected
                'SELECT host(coalesce("minip" - 1, "r"."range")) AS "ip", ' .
                'inet_int(coalesce("minip", "r"."range" + 1 & hostmask("r"."range"))) AS "host", ' .
                'host(coalesce("minip" - 1, "r"."range") + 1) AS "next", ' .
                'NULL AS "prev", CASE WHEN "minip" IS NOT NULL THEN \'adminisztratívan foglalt\' WHEN NULL THEN \'net\' ELSE \'net\' END AS "name", ' .
                '\'range-edge\' AS "netclass" ' .
                'FROM "dhcpdomain" WHERE "id" = 1',
                'Dhcpdomain',
                [
                    'ip' => ['host()', ['coalesce()', ['-', 'minip', 1], 'r.range']],
                    'host' => ['inet_int()', ['coalesce()', 'minip', ['&', ['+', 'r.range', 1], ['hostmask()', 'r.range']]]],
                    'next' => ['host()', ['+', ['coalesce()', ['-', 'minip', 1], 'r.range'], 1]],
                    'prev' => ['null'],
                    'name' => ['case',
                        // conditions
                        [
                            [['is not null', 'minip'],],
                            null
                        ],
                        // values
                        [
                            "'adminisztratívan foglalt'",
                            "'net'",
                        ]
                    ],
                    'netclass' => "'range-edge'",
                ],
                [],
                ['id' => 1],
                null, null, null, null, null, null
            ],
            8 => [
                'SELECT host(coalesce("dhcpdomain"."minip" - 1, "r"."range")) AS "ip", ' .
                'inet_int(coalesce("dhcpdomain"."minip", "r"."range" + 1 & hostmask("r"."range"))) AS "host", ' .
                'host(coalesce("dhcpdomain"."minip" - 1, "r"."range") + 1) AS "next", ' .
                'NULL AS "prev", CASE WHEN "dhcpdomain"."minip" IS NOT NULL THEN \'adminisztratívan foglalt\' WHEN NULL THEN \'net\' ELSE \'net\' END AS "name", ' .
                '\'range-edge\' AS "netclass" ' .
                'FROM "dhcpdomain" ' .
                'LEFT JOIN "dhcpdomain" "p" ON "dhcpdomain"."parent" = "p"."id" ' .
                'WHERE "dhcpdomain"."id" = 1',
                [
                    'from' => 'Dhcpdomain',
                    'fields' => [
                        'ip' => ['host()', ['coalesce()', ['-', 'minip', 1], 'r.range']],
                        'host' => ['inet_int()', ['coalesce()', 'minip', ['&', ['+', 'r.range', 1], ['hostmask()', 'r.range']]]],
                        'next' => ['host()', ['+', ['coalesce()', ['-', 'minip', 1], 'r.range'], 1]],
                        'prev' => ['null'],
                        'name' => ['case',
                            // conditions
                            [
                                [['is not null', 'minip'],],
                                null
                            ],
                            // values
                            [
                                "'adminisztratívan foglalt'",
                                "'net'",
                            ]
                        ],
                        'netclass' => "'range-edge'",
                    ],
                    'joins' => ['p' => 'parent1'],
                    'condition' => ['id' => 1],
                ],
                null, null, null, null, null, null, null, null, null
            ],
            // $expected, $model, $fields, $joins, $condition, $orders, $limit, $offset, $params, $r, $selectexpected
            9 => [
                'SELECT count("host"), "value" FROM "dhcpoption" WHERE "host" IS NOT NULL GROUP BY "value"',
                [
                    'modelname' => 'Dhcpoption',
                    'fields' => [['count()', 'host'], 'value'],
                    'groupby' => ['value'],
                    'condition' => ['IS NOT NULL', 'host']
                ],
                null, null, null, null, null, null, null, null, null
            ],
            // $expected, $model, $fields, $joins, $condition, $orders, $limit, $offset, $params, $r, $selectexpected
            10 => [
                'SELECT "c".* FROM (SELECT count(*) AS "count", "value" FROM "dhcpoption" GROUP BY "value") "c" WHERE "c"."count" = 0',
                [
                    'from' => [
                        'c' => Query::createSelect([
                            'modelname' => 'Dhcpoption',
                            'fields' => ['count' => ['count()', '*'], 'value'],
                            'groupby' => ['value'],
                        ]),
                    ],
                    'fields' => ['*'],
                    'condition' => ['c.count' => 0],
                ],
                null, null, null, null, null, null, null, null, null
            ],
            11 => [
                'SELECT "dhcphost"."id", 
	"dhcphost"."name",
	"dhcphost"."descr", "dhcphost"."mac", "dhcphost"."ip", "dhcphost"."owner", "dhcphost"."org", "dhcphost"."vendor", 
	"dhcphost"."created", "dhcphost"."deleted", "dhcphost"."creator", "dhcphost"."deleter", "dhcphost"."active", 
	"dhcphost"."lastuse", "dhcphost"."reserved", 
	"org1"."name" AS "org_name", 
	"owner1"."name" AS "owner_name", 
	"deleter1"."name" AS "deleter_name", 
	"creator1"."name" AS "creator_name" 
FROM "dhcphost" 
	LEFT JOIN "org" "org1" ON "dhcphost"."org" = "org1"."id" 
	LEFT JOIN "person" "owner1" ON "dhcphost"."owner" = "owner1"."id" 
	LEFT JOIN "person" "deleter1" ON "dhcphost"."deleter" = "deleter1"."id" 
	LEFT JOIN "person" "creator1" ON "dhcphost"."creator" = "creator1"."id" 
WHERE  ("dhcphost"."deleted" IS NULL OR $df = 1) AND "dhcphost"."dhcpdomain" = $parent
ORDER BY "name" LIMIT 50',
                [
                    'from' => 'Dhcphost',
                    'fields' => ['id', 'name', 'descr', 'mac', 'ip', 'owner', 'org', 'vendor',
                        'created', 'deleted', 'creator', 'deleter', 'active', 'lastuse', 'reserved',
                        'org_name' => 'org1.name',
                        'owner_name' => 'owner1.name',
                        'deleter_name' => 'deleter1.name',
                        'creator_name' => 'creator1.name'
                    ],
                    'joins' => ['org1', 'owner1', 'deleter1', 'creator1'],
                    'condition' => [
                        'dhcpdomain' => '$parent',
                        ['or', ['is null', 'deleted'], ['=', '$df', 1]],
                    ],
                    'orders' => ['name'],
                    'offset' => 0,
                    'limit' => 50,
                    'params' => ['parent' => 1, 'df' => 1],
                ],
                null, null, null, null, null, null, null, null, null
            ]
        ];
    }

    /**
     * @throws InternalException
     * @throws DatabaseException
     */
    function test_literal() {
        $this->assertEquals("'''i'''", $this->db->literal('\'i\''));
    }

    /**
     * @dataProvider prov_createExpression
     * @param string $expected
     * @param array $models
     * @param array $expressions
     * @return void
     * @throws InternalException
     * @throws QueryException
     * @throws UAppException
     */
    function test_createExpression($expected, $models, $expressions) {
        $query = Query::createSelect($models, $expressions);
        $query->connection = $this->db;
        $this->assertEquals($expected, $query->finalSQL);
    }

    function prov_createExpression() {
        return [ //$expected, $models, $expression
            ['SELECT 1 + 2 AS "a"', [], ['a' => ['+', 1, 2]]],
            [
                "SELECT (regexp_matches(\"d\".\"name\", '''i'''))[1] AS \"match\"", [],
                ['match' => ['[]', ['regexp_matches()', 'd.name', "'''i'''"], 1]],
            ],
            ['SELECT "city"[1:2][3] FROM "city"', [City::class], [['[]', ['[:]', "city", 1, 2], 3]]],
        ];
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @dataProvider provider_createInsert
     *
     * @param string $expected
     * @param string $model
     * @param array $fields
     * @param array $values
     *
     * @return void
     * @throws InternalException
     * @throws QueryException
     * @throws Exception
     */
    function test_createInsert($expected, $model, $fields, $values) {
        $dt = new DateTime('1367-09-01T12:30', new DateTimeZone('+01:00'));
        $this->assertEquals('1367-09-01 12:30:00+01:00', $dt->format('Y-m-d H:i:sP'));
        $dt = new DateTime('1367-09-01T12:30');
        $this->assertEquals('1367-09-01 12:30:00+01:16', $dt->format('Y-m-d H:i:sP'));
        $this->assertEquals('1367-09-01T12:30:00+01:16', $dt->format(DateTime::ATOM));
        $this->assertEquals("'1367-09-01 12:30:00+01:16'", $this->db->literal($dt));

        $values = $this->tester->evaluateItems($values, $this);
        Debug::tracex('values', $values);

        /** @noinspection PhpUnhandledExceptionInspection */
        $query = Query::createInsert($model, $fields, $values, $this->db);
        $result = $query->sql;
        #$result = preg_replace('/\s{2,}/', ' ', trim($result));
        $this->assertEquals($expected, $result);
    }

    /** @noinspection PhpMethodNamingConventionInspection */
    function provider_createInsert() {
        return [
            // test case data: $expected, $model, $fields, $values
            ['INSERT INTO "dhcpoptiontype" ("name", "descr") VALUES (\'kakukk\', \'marci\')',
                'Dhcpoptiontype', ['name', 'descr'], [['kakukk', 'marci']]
            ],
            ['INSERT INTO "dhcpoptiontype" ("name", "descr") VALUES ' . "('kakukk', 'marci'),\n('fekete', 'kutya')",
                'Dhcpoptiontype', ['name', 'descr'], [['kakukk', 'marci'], ['fekete', 'kutya']]
            ],
            ['INSERT INTO "dhcpoptiontype" ("name", "descr") SELECT "name", "descr" FROM "dhcpoptionname" WHERE "datatype" IS NULL',
                'Dhcpoptiontype', ['name', 'descr'],
                [['Query', 'createSelect'], 'Dhcpoptionname', ['name', 'descr'], null, ['IS NULL', 'datatype']]
            ],
            // TODO: a default time zone valamiért nem stimmel.
            ['INSERT INTO "person" ("name", "created") VALUES ' . "('alma', '1367-09-01 12:30:00+01:16')",
                'Person', ['name', 'created'], [['alma', new DateTime('1367-09-01T12:30')]]
            ],
        ];
    }


    /** @noinspection PhpMethodNamingConventionInspection */
    /**
     * @dataProvider provider_createUpdate
     *
     * @param string $expected
     * @param string $model
     * @param array $values
     * @param array $conditions
     * @param array $params
     *
     * @return void
     * @throws InternalException
     * @throws QueryException
     * @throws UAppException
     */
    function test_createUpdate($expected, $model, $values, $conditions, $params) {
        $query = Query::createUpdate($model, $values, $conditions, $params, $this->db);
        $result = $params ? $query->finalSQL : $query->sql;
        #$result = preg_replace('/\s{2,}/', ' ', trim($result));
        #Debug::tracex($expected);
        #Debug::tracex($result);
        $this->assertEquals($expected, $result);
    }

    /** @noinspection PhpMethodNamingConventionInspection */
    function provider_createUpdate() {
        return [
            // test case data: $expected, $model, $values, $conditions, $params
            ['UPDATE "dhcpoptiontype" SET "name"=\'próba2\' WHERE "name" = \'próba1\'',
                'Dhcpoptiontype', ['name' => '\'próba2\''], ['name' => '\'próba1\''], null],
            ['UPDATE "dhcpoptiontype" SET "name"=\'próba2\', "descr"=\'leír\' WHERE "name" = \'próba1\' AND "descr" IS NOT NULL',
                'Dhcpoptiontype', ['name' => '\'próba2\'', 'descr' => '\'leír\''], ['AND', ['=', 'name', '\'próba1\''], ['IS NOT NULL', 'descr']], null],
            ['UPDATE "dhcpoptiontype" SET "name"=\'próba2\' WHERE "name" = \'próba1\'',
                'Dhcpoptiontype', ['name' => '$mit'], ['name' => '$mibol'], ['mibol' => 'próba1', 'mit' => 'próba2']],
        ];
    }

    /** @noinspection PhpMethodNamingConventionInspection */
    /**
     * @dataProvider provider_createDelete
     *
     * @param mixed $expected
     * @param mixed $model
     * @param mixed $conditions
     * @param mixed $params
     *
     * @return void
     * @throws InternalException
     * @throws QueryException
     * @throws UAppException
     */
    function test_createDelete($expected, $model, $conditions, $params) {
        $query = Query::createDelete($model, $conditions, $params, $this->db);
        $result = $params ? $query->finalSQL : $query->sql;
        #$result = preg_replace('/\s{2,}/', ' ', trim($result));
        #Debug::tracex($expected);
        #Debug::tracex($result);
        $this->assertEquals($expected, $result);
    }

    /** @noinspection PhpMethodNamingConventionInspection */
    function provider_createDelete() {
        return [
            // test case data: $expected, $model, $conditions, $params
            ['DELETE FROM "dhcpoptiontype" WHERE "name" = \'próba1\' AND "descr" IS NOT NULL',
                'Dhcpoptiontype', ['AND', ['=', 'name', '\'próba1\''], ['IS NOT NULL', 'descr']], null],
            ['DELETE FROM "dhcpoptiontype" WHERE "id" = 97865',
                'Dhcpoptiontype', ['id' => 97865], null],
            ['DELETE FROM "dhcpoptiontype" WHERE "id" = 97865',
                'Dhcpoptiontype', ['id' => '$1'], [97865]],
        ];
    }

    /** @noinspection PhpMethodNamingConventionInspection */
    /**
     * @throws DOMException
     * @throws DatabaseException
     * @throws InternalException
     * @throws ModelException
     * @throws QueryException
     * @throws UAppException
     */
    function test_node() {
        $query = Query::createSelect('Dhcpoptiontype', ['id', 'name'], null, ['<', 'id', 3], 'id', null, null, null, $this->db);
        $query->nodes($this->xmldoc->documentElement, 'type');
        $result = $this->xmldoc->saveXML($this->xmldoc->documentElement);
        $expected = UXMLDoc::formatFragment(/** @lang xml */ '<data><type id="0" name="none"/><type id="1" name="boolean"/><type id="2" name="uint8"/></data>');
        #$this->compare($expected, $result);
        $this->assertEquals($expected, $result); //, "Case $i: %s");
    }

    /** @noinspection PhpMethodNamingConventionInspection */
    /**
     * @throws InternalException
     * @throws ModelException
     * @throws QueryException
     * @throws UAppException
     */
    function test_first() {
        $expected = 'SELECT "created", "creator", "deleted", "descr", "dn", "email", "enabled", "id", "idp", "lastclick", "lastlogin", "loggedin", ' .
            '"logins", "name", "org", "pagelen", "tel", "title", "tooltip", "type", "uid", "unit" ' .
            'FROM "person" ' .
            'WHERE ("idp" IS NULL OR "idp" = \'http://testidp.pte.hu/saml2/idp/metadata.php\') AND "uid" = \'uhi@pte.hu\' OR ' .
            '("uid" || \'testidp.pte.hu\') = \'uhi@pte.hu\' AND "idp" = \'http://testidp.pte.hu/saml2/idp/metadata.php\' OR ' .
            '"uid" IS NULL AND ("idp" IS NULL OR "idp" = \'http://testidp.pte.hu/saml2/idp/metadata.php\') AND "email" = \'uhi@uhisoft.hu\'';

        $params = ['uhi@pte.hu', 'uhi@uhisoft.hu', 'http://testidp.pte.hu/saml2/idp/metadata.php', 'testidp.pte.hu'];
        $condition = [
            'or',
            ['uid' => '$1', ['or', ['is null', 'idp'], 'idp' => '$3']],
            ['idp' => '$3', ['=', ['||', 'uid', '$4'], '$1']],
            ['email' => '$2', ['is null', 'uid'], ['or', ['is null', 'idp'], 'idp' => '$3']]
        ];
        $classname = 'Person';
        $fields = Person::attributes($this->db); //array_diff(, array('old_id', 'szemid'));

        $conditionExpression = $this->db->asExpression($condition);
        $query = Query::createSelect($classname, $fields, null, $conditionExpression, null, null, null, $params, $this->db);
        $sql = $this->db->bindParams($query->sql, $query->params);

        $this->assertEquals($expected, $sql);
    }

    /** @noinspection PhpMethodNamingConventionInspection */
    /**
     * @dataProvider provider_createSql
     *
     * @param string $sql
     *
     * @param $params
     * @param $expected
     *
     * @throws InternalException
     * @throws UAppException
     */
    function test_createSql($sql, $params, $expected) {
        $query = Query::createSql($sql, $params, $this->db);
        $this->assertEquals($expected, $query->finalSQL);

    }

    /** @noinspection PhpMethodNamingConventionInspection */
    function provider_createSql() {
        return [
            ['select id from person where email=$1', ['uhi@pte.hu'], "select id from person where email='uhi@pte.hu'"],
        ];
    }

    /** @noinspection PhpMethodNamingConventionInspection */
    /**
     * @dataProvider provider_create
     *
     * @param $options
     * @param $expected
     *
     * @throws InternalException
     * @throws UAppException
     */
    function test_create($options, $expected) {
        $query = new Query($options);
        $query->connection = $this->db;
        $this->assertEquals($expected, $query->finalSQL);

    }

    /** @noinspection PhpMethodNamingConventionInspection */
    function provider_create() {
        return [
            [
                [
                    'type' => 'select',
                    'from' => 'person',
                    'fields' => 'id',
                    'where' => ['email' => '$1'],
                    'params' => ['uhi@pte.hu'],
                ],
                'SELECT "id" FROM "person" WHERE "email" = \'uhi@pte.hu\''
            ],
            [
                ['type' => 'select', 'fields' => 'a', 'from' => 'aa'],
                /** @lang text */ 'SELECT "a" FROM "aa"',
            ],
        ];
    }


    /**
     * @throws DatabaseException
     * @throws InternalException
     * @throws QueryException
     * @throws UAppException
     */
    function testFirstCache() {
        $title_first = 'title-4';
        $title_mod = 'title-4-test';
        // Restore original
        $this->assertEquals(1, Person::updateAll(['title' => $title_first], ['id' => 5], $this->db));
        /** @var Person $person */
        $person = Person::first(5, null, null, $this->db, true);
        $this->assertEquals($title_first, $person->title);
        $this->assertEquals(1, Person::updateAll(['title' => $title_mod], ['id' => 5], $this->db));
        $person = Person::first(5, null, null, $this->db);
        $this->assertEquals($title_first, $person->title); // cached
        $person = Person::first(5, null, null, $this->db, true); // no-cache
        $this->assertEquals($title_mod, $person->title);
        // Restore original
        $this->assertEquals(1, Person::updateAll(['title' => $title_first], ['id' => 5], $this->db));
        $person = Person::first(5, null, null, $this->db, true); // no-cache
        $this->assertEquals($title_first, $person->title);
    }

    /**
     * @throws DatabaseException
     * @throws InternalException
     * @throws QueryException
     * @throws UAppException
     * @throws Exception
     */
    function testJoin() {
        $this->assertEquals(1, Org::updateAll(['name' => 'Kukutyin Zabhegyező Vállalat'], 1, $this->db));
        $this->assertEquals(1, Person::updateAll(['name' => 'Adminisztrátor', 'org' => 1], 1, $this->db));
        $query = Person::createSelect(['name', 'unit1.name'])->setDb($this->db)->join('unit1')->where(['id' => '$1'])->bind(1);
        $this->assertEquals('SELECT "person"."name", "unit1"."name" AS "unit1_name" FROM "person" LEFT JOIN "org" "unit1" ON "person"."unit" = "unit1"."id" WHERE "person"."id" = 1', $query->finalSQL);
        $result = $query->all();
        $this->assertEquals([['name' => 'Adminisztrátor', 'unit1_name' => 'Kukutyin Zabhegyező Vállalat']], $result);
    }

    /**
     * @dataProvider provFilterIntegerRange
     * @param $expected
     * @param $pattern
     * @return void
     * @throws InternalException
     * @throws QueryException
     * @throws UAppException
     * @throws Exception
     */
    function testFilterIntegerRange($expected, $pattern) {
        $this->assertEquals(1, preg_match(Query::$integerRangePattern, $pattern), $pattern);
        $query = Query::createSelect([Dhcphost::class], null)->setDb($this->db);
        $this->assertEquals($expected, $query->filterIntegerRange('id', $pattern)->condition, $pattern . ': ' . json_encode($query->condition));
    }

    function provFilterIntegerRange() {
        return [
            [['AND', ['=', 'id', 1]], '1'],
            [['AND', ['<', 'id', 2]], '< 2'],
            [['AND', ['<', 'id', 2]], '... 2'],
            [['AND', ['<=', 'id', 2]], '<= 2'],
            [['AND', ['>', 'id', 3]], '> 3'],
            [['AND', ['>=', 'id', 3]], '>= 3'],
            [['AND', ['>', 'id', 3]], '3 <'],
            [['AND', ['>=', 'id', 3]], '3 <='],
            [['AND', ['>', 'id', 3]], '3 -'],
            [['AND', ['!=', 'id', 9]], '!= 9'],
            [['AND', ['!=', 'id', 9]], '<> 9'],
            [['AND', ['IS NULL', 'id']], 'null'],
            [['AND', ['AND', ['>', 'id', 1], ['<', 'id', 4]]], '1 - 4'],
            [['AND', ['AND', ['>', 'id', 1], ['<', 'id', 5]]], '1 -- 5'],
            [['AND', ['AND', ['>', 'id', 1], ['<', 'id', 5]]], '1 .. 5'],
            [['AND', ['AND', ['>', 'id', 1], ['<', 'id', 5]]], '1 ... 5'],
            [['AND', ['AND', ['>', 'id', 1], ['<', 'id', 6]]], '1 < 6'],
            [['AND', ['AND', ['>=', 'id', 1], ['<=', 'id', 6]]], '1 <= 6'],
            [['AND', ['IN', 'id', [7, 17, 27]]], '7, 17, 27'],
            [['AND', ['IN', 'id', [7, 17, 27]]], ' 7, 17 ;  27 '],
            [['AND', ['OR', ['=', 'id', 0], ['AND', ['>', 'id', 1], ['<', 'id', 7]]]], '0, 1 < 7'],
            [['AND', ['OR', ['=', 'id', 0], ['AND', ['>', 'id', 1], ['<', 'id', 7]], ['IS NULL', 'id']]], '0, 1 < 7; <null>'],
        ];
    }

    function test1() {
        $dt1 = '\d[\d.-]+([T ]\d[\d:,.]+)?(Z|[+-]\d[\d:,.]+)?';
        $exp1 = '~^\s*(?:(' . $dt1 . ')|null|<null>|(?:\.\.\.?|--?|<=?|>=?|<>|!=)\s*(' . $dt1 . ')|(' . $dt1 . ')\s*(?:\.\.\.?|--?|<|<=)\s*(' . $dt1 . ')?)$~';
        $explist = '~^\s*(?:(' . $dt1 . ')|null|<null>|(?:\.\.\.?|--?|<=?|>=?|<>|!=)\s*(' . $dt1 . ')|(' . $dt1 . ')\s*(?:\.\.\.?|--?|<|<=)\s*(' . $dt1 . ')?)' .
            '(?:\s*[,;]\s*(?:(' . $dt1 . ')|null|<null>|(?:\.\.\.?|--?|<=?|>=?|<>|!=)\s*(' . $dt1 . ')|(' . $dt1 . ')\s*(?:\.\.\.?|--?|<|<=)\s*(' . $dt1 . ')))*\s*$~';
        $value = '2024';
        $this->assertEquals(1, preg_match("~$dt1~", $value));
        $this->assertEquals(1, preg_match($exp1, $value));
        $value = '2024-01-08 11:21:01.000+01:00';
        $this->assertEquals(1, preg_match("~^$dt1~", $value, $mm));
        $this->assertEquals($value, $mm[0]);
        $this->assertEquals(1, preg_match($exp1, $value));
        $this->assertEquals(1, preg_match($explist, $value));
    }

    /**
     * @dataProvider provDatetimePattern
     * @param int $expected
     * @param string $pattern
     * @return void
     */
    function testDatetimePattern($expected, $pattern) {
        if ($expected == 1 || $expected == 2) $this->assertEquals(1, preg_match(Query::$partialDateTimePattern, $pattern), $expected . ': ' . $pattern);
        if ($expected == 2) $this->assertEquals(1, preg_match(Query::$dateTimePattern, $pattern), $expected . ': ' . $pattern);
        if (in_array($expected, [1, 2, 3])) $this->assertEquals(1, preg_match(Query::$datetimeRangePattern, $pattern), $expected . ': ' . $pattern);
    }

    function provDatetimePattern() {
        return [
            [0, 'null'],
            [0, 'kalap'],
            [0, '8:30'],
            [1, '2024'],
            [1, '12-31'],
            [2, '2024.01.08'], #5
            [2, '2024.01.08.'],
            [2, '2024.01.08. 15:55'],
            [2, '2024.01.08. 15:55+02:30'],
            [1, '2024-01-08 11:21:01.000+01:00'], #9
            [1, '2024.01'],
            [3, '... 2024-01-08'],
            [3, '<= 2024-01-08T11'],
            [3, '> 2024-01-08T11:19'],
            [3, '>= 2024-01-08T11:20:00'],
            [2, '2024-01-08 11:21:01.000'],
            [2, '2024-01-08 11:21:01.000Z'], #16
            [3, '2024-01-08 11:21:01.000+01 -'],
            [3, '!= 2024-01-08 11:21:01.000+0100'],
            [3, '<> 2024.01.08. 11:21:01.000+01:00'], #19
        ];
    }

    /**
     * @dataProvider provFilterDatetimeRange
     * @param $expected
     * @param $pattern
     * @return void
     * @throws InternalException
     * @throws QueryException
     * @throws UAppException
     * @throws Exception
     */
    function testFilterDatetimeRange($expected, $pattern) {
        $validrange = preg_match(Query::$datetimeRangePattern, $pattern, $mm);
        if ($expected !== null) $this->assertEquals(1, $validrange, $pattern);
        else $this->assertEquals(0, $validrange, $pattern);
        for ($i = 1; $i < count($mm); $i++) {
            if ($mm[$i] != '') $this->assertEquals(1, preg_match(Query::$partialDateTimePattern, $mm[$i]), $pattern . ' [' . $i . ']= `' . $mm[$i] . '` // ' . json_encode($mm));
        }
        $query = Query::createSelect([Dhcphost::class], null)->setDb($this->db);
        if ($expected === null) $this->expectException(Exception::class);
        $this->assertEquals($expected, $query->filterDateTimeRange('id', $pattern)->condition, $pattern . ': ' . json_encode($query->condition));
    }

    function provFilterDatetimeRange() {
        return [
            [['AND', ['<', 'id', "'2024-01-08'"]], '... 2024-01-08'],
            [['AND', ['<=', 'id', "'2024-01-08T11'"]], '<= 2024-01-08T11'],
            [['AND', ['RLIKE', 'id', "'2024'"]], '2024'],
            [null, '< 2024.01'],
            [['AND', ['>', 'id', "'2024-01-08T11:19'"]], '> 2024-01-08T11:19'],
            [['AND', ['>=', 'id', "'2024-01-08T11:20:00'"]], '>= 2024-01-08T11:20:00'],
            [['AND', ['>', 'id', "'2024-01-08 11:21:01.000'"]], '2024-01-08 11:21:01.000 <'],
            [['AND', ['>=', 'id', "'2024-01-08 11:21:01.000Z'"]], '2024-01-08 11:21:01.000Z <='],
            [['AND', ['>', 'id', "'2024-01-08 11:21:01.000+01'"]], '2024-01-08 11:21:01.000+01 -'],
            [['AND', ['!=', 'id', "'2024-01-08 11:21:01.000+0100'"]], '!= 2024-01-08 11:21:01.000+0100'],
            [['AND', ['!=', 'id', "'2024/01/08 11:21:01.000+01:00'"]], '<> 2024/01/08 11:21:01.000+01:00'], #10
            [['AND', ['IS NULL', 'id']], 'null'],
            [['AND', ['AND', ['>', 'id', "'2024-1-8'"], ['<', 'id', "'2024-1-9'"]]], '2024-1-8 -- 2024-1-9'],
            [['AND', ['AND', ['>', 'id', "'2024-01-13'"], ['<', 'id', "'2024-02-13 13:00'"]]], '2024-01-13 -- 2024-02-13 13:00'],
//            [['AND', ['AND', ['>', 'id', 1], ['<', 'id', 5]]], '1 .. 5'],
//            [['AND', ['AND', ['>', 'id', 1], ['<', 'id', 5]]], '1 ... 5'],
//            [['AND', ['AND', ['>', 'id', 1], ['<', 'id', 6]]], '1 < 6'],
//            [['AND', ['AND', ['>=', 'id', 1], ['<=', 'id', 6]]], '1 <= 6'],
            [['AND', ['IN', 'id', ["'2024-01-18'", "'2024-02-18'", "'2024-03-18'"]]], '2024-01-18, 2024-02-18, 2024-03-18'],
            [['AND', ['IN', 'id', ["'2024-01-18'", "'2024-02-18'", "'2024-03-18'"]]], '2024-01-18, 2024-02-18 ; 2024-03-18'],
//            [['AND', ['OR', ['=', 'id', 0], ['AND', ['>', 'id', 1], ['<', 'id', 7]]]], '0, 1 < 7'],
//            [['AND', ['OR', ['=', 'id', 0], ['AND', ['>', 'id', 1], ['<', 'id', 7]], ['IS NULL', 'id']]], '0, 1 < 7; <null>'],
        ];
    }
}
