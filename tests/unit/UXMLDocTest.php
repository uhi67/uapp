<?php /** @noinspection PhpMethodNamingConventionInspection */
/** @noinspection RequiredAttributes */
/** @noinspection PhpUnhandledExceptionInspection */
/**
 * Test file for Codeception
 */
require_once dirname(__DIR__) . '/_support/Helper/UnitTest.php';

class UXMLDocTest extends UnitTest {
    /** @var UnitTester */
    protected $tester;

    /** @var UXMLDoc */
    private $xmldoc;
    /** @var DOMElement */
    private $data;

    /**
     * @throws InternalException
     */
    protected function _before() {
        $this->xmldoc = new UXMLDoc('UTF-8', 'data', null, 'hu');
        $this->data = $this->xmldoc->documentElement;
        UApp::setLang('hu');
        UApp::$config = [
            'uapp' => [
                'path' => dirname(dirname(__DIR__)),
            ],
        ];
        L10nBase::init([]);
    }

    protected function _after() {
    }

    /**
     * Checks precreated xmldoc in _before
     */
    function test_xmldoc() {
        $this->assertEquals($this->xmldoc->documentElement->nodeName, 'data');
    }

    /**
     * @dataProvider provider_addAttribute
     *
     * @param $name
     * @param $value
     * @param $ns
     * @param $expected
     *
     * @param $exn
     *
     * @throws
     */
    function test_addAttribute($name, $value, $ns, $expected, $exn) {
        /** @var UXMLElement $node */
        $node = $this->data->addNode('node');
        $an = $node->addAttribute($name, $value, $ns);
        // XML result
        $this->assertEquals(UXMLDoc::formatFragment($expected), $this->xmldoc->saveXML($node));
        // Node result
        if ($exn === null) $this->assertSame($exn, $an);
        else $this->assertEquals($exn, $an->nodeName);
    }

    /**
     * @return array
     * @throws InternalException
     */
    function provider_addAttribute() {
        return [ // $name, $value, $ns, $expected, $exn
            // Invalid nodename
            ['-*/,"', '', null, '<node _-----=""/>', '_-----'],
            // Null value
            ['attr1', null, null, '<node/>', null],
            // Simple attribute
            ['attr1', 'value1', null, '<node attr1="value1"/>', 'attr1'],
            // Array value
            ['attr2', ['v1', 'v2'], null, '<node><attr2>v1</attr2><attr2>v2</attr2></node>', 'attr2'],
            // special object values
            4 => ['date', new DateTime('2018-01-01'), null, '<node date="2018.01.01."/>', 'date'],
            ['ip', new Inet('192.168.1.1'), null, '<node ip="192.168.1.1"/>', 'ip'],
            ['mac', new Macaddr('112233445566'), null, '<node mac="11:22:33:44:55:66"/>', 'mac'],
            ['any', new TestObj(['name' => 23]), null, '<node any="[23]"/>', 'any'],
            // Simple namespace
            8 => ['ns:attr1', 'value1', 'https://ns1.uhisoft.hu', '<node xmlns:ns="https://ns1.uhisoft.hu" ns:attr1="value1"/>', 'ns:attr1'],
            // Namespaced subnodes
            ['ns2:attr2',
                ['v1', 'v2'],
                ['ns2' => 'https://ns2.uhisoft.hu',],
                '<node xmlns:ns2="https://ns2.uhisoft.hu"><ns2:attr2>v1</ns2:attr2><ns2:attr2>v2</ns2:attr2></node>',
                'ns2:attr2'
            ],
        ];
    }

    function test_translate() {
        $this->assertEquals('hu', UApp::getLang());
        $this->assertEquals('hu', $this->xmldoc->locale);
        $this->assertEquals('nem beállított', L10nBase::getText('uapp', 'not set', null, 'hu'));
        $this->assertEquals('nem beállított', UApp::la('uapp', 'not set', null, 'hu'));
    }

    /**
     * @dataProvider provFormatFragment
     * @param string $expected
     * @param string $xml
     * @throws DOMException
     */
    function testFormatFragment($expected, $xml) {
        $this->assertEqualsFine($expected, UXMLDoc::formatFragment($xml));
    }

    function provFormatFragment() {
        return [
            [
                "<data xmlns:ns=\"https://ns1.uhisoft.hu\" xmlns:ns2=\"https://ns2.uhisoft.hu\">\n  <node>\n    <ns:attr1 xmlns:ns=\"https://ns1.uhisoft.hu\">value1</ns:attr1>\n    <ns2:attr2 xmlns:ns2=\"https://ns2.uhisoft.hu\">value2</ns2:attr2>\n  </node>\n</data>",
                '<data><node><ns:attr1 xmlns:ns="https://ns1.uhisoft.hu">value1</ns:attr1><ns2:attr2 xmlns:ns2="https://ns2.uhisoft.hu">value2</ns2:attr2></node></data>'
            ],
            [
                '<data xmlns="uri:oid:private">text</data>',
                '<data xmlns="uri:oid:private">text</data>',
            ],
            // Mindenképpen bevezeti a default namespace-t
            [
                '<data xmlns="uri:oid:private" xmlns:default="uri:oid:intern">' . "\n" . '  <default:sub xmlns="uri:oid:intern"/>' . "\n" . '</data>',
                '<data xmlns="uri:oid:private"><sub xmlns="uri:oid:intern"/></data>',
            ],
        ];
    }

    function testAddTextNode1() {
        $ns = 'https://ns1.uhisoft.hu';
        $this->xmldoc->documentElement->appendChild($this->xmldoc->createElementNS($ns, 'ns1:text', 'Szöveg'));
        $expected = '<?xml version="1.0" encoding="UTF-8"?><data><ns1:text xmlns:ns1="https://ns1.uhisoft.hu">Szöveg</ns1:text></data>';
        $this->assertEqualsFine($expected, preg_replace('~\n\s*~', '', $this->xmldoc->saveXML()));
        $expected = '<data><ns1:text xmlns:ns1="https://ns1.uhisoft.hu">Szöveg</ns1:text></data>';
        $this->assertEqualsFine($expected, preg_replace('~\n\s*~', '', $this->xmldoc->saveXML($this->xmldoc->documentElement)));
        $expected = '<data xmlns:ns1="https://ns1.uhisoft.hu"><ns1:text xmlns:ns1="https://ns1.uhisoft.hu">Szöveg</ns1:text></data>';
        $this->assertXmlEquals($expected, $this->xmldoc->saveXML($this->xmldoc->documentElement));
    }

    /**
     * @dataProvider provider_addTextNode
     *
     * @param $name
     * @param $value
     * @param $ns
     * @param $expected
     *
     * @throws
     */
    function test_addTextNode($name, $value, $ns, $expected) {
        $node = UXMLDoc::addNode($this->data, 'node');
        $an = UXMLDoc::addTextNode($node, $name, $value, $ns);
        // XML result
        $this->assertXmlEquals($expected, $node);
        // Node result
        $this->assertSame('UXMLElement', get_class($an));
    }

    /**
     * @return array
     * @throws InternalException
     */
    function provider_addTextNode() {
        return [
            // Null value
            ['attr1', null, null, '<node><attr1>nem beállított</attr1></node>', null],
            // Simple node
            ['attr1', 'value1', null, '<node><attr1>value1</attr1></node>', 'attr1'],
            // Array value
            ['attr2', ['v1', 'v2'], null, '<node><attr2>[v1, v2]</attr2></node>', 'attr2'],
            // special object values
            3 => ['date', new DateTime('2018-01-01'), null, '<node><date>2018.01.01.</date></node>', 'date'],
            ['ip', new Inet('192.168.1.1'), null, '<node><ip>192.168.1.1</ip></node>', 'ip'],
            ['mac', new Macaddr('112233445566'), null, '<node><mac>11:22:33:44:55:66</mac></node>', 'mac'],
            ['any', new TestObj(['name' => 23]), null, '<node><any>[23]</any></node>', 'any'],
            // Simple namespace
            7 => [
                'ns:attr1', 'value1', 'https://ns1.uhisoft.hu',
                '<node><ns:attr1 xmlns:ns="https://ns1.uhisoft.hu">value1</ns:attr1></node>',
                'ns:attr1'
            ],
            // Namespaced subnodes
            ['ns2:attr2',
                ['v1', 'v2'],
                ['ns2' => 'https://ns2.uhisoft.hu',],
                '<node><ns2:attr2 xmlns:ns2="https://ns2.uhisoft.hu">[v1, v2]</ns2:attr2></node>',
                'ns2:attr2'
            ],
        ];
    }

    /**
     * @dataProvider provAddNodePathIfNotExists
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     */
    function testAddNodePathIfNotExists($xml, $spec, $attr, $content, $expected) {
        $doc = new UXMLDoc();
        $doc->loadXML($xml);
        $parent = $doc->documentElement;

        $r = UXMLDoc::addNodePathIfNotExists($parent, $spec, $attr, $content);

        $this->assertNotNull($r);
        $this->assertEquals(UXMLDoc::formatFragment($expected), $doc->saveXML($doc->documentElement));
    }

    function provAddNodePathIfNotExists() {
        return [ //$xml, $spec, $attr, $content, $result
            ['<root></root>', 'alma', null, null, '<root><alma/></root>'],
            ['<root><alma/></root>', 'alma/banan', [null, ['id' => 2]], null, '<root><alma><banan id="2"/></alma></root>'],
            ['<root></root>', 'alma/banan', [null, ['id' => 2]], ['gala'], '<root><alma>gala<banan id="2"/></alma></root>'],
            ['<root><alma id="1"><banan/></alma><alma id="2"><citrom/></alma></root>', 'alma[@id="1"]/banan', null, null, '<root><alma id="1"><banan/></alma><alma id="2"><citrom/></alma></root>'],
            ['<root><alma id="1"><banan/></alma><alma id="2"><citrom/></alma></root>', 'alma[@id="2"]/banan', null, null, '<root><alma id="1"><banan/></alma><alma id="2"><citrom/><banan/></alma></root>'],
            ['<root><alma id="1"><banan/></alma><alma id="2"><citrom/></alma></root>', 'alma[@id="3"]/korte', [['id' => 31]], null, '<root><alma id="1"><banan/></alma><alma id="2"><citrom/></alma><alma id="31"><korte/></alma></root>'],
        ];
    }

    /**
     * @dataProvider provCanonizePrefixes
     * @param string $expected
     * @param string $source
     * @param array $prefixes
     */
    function testCanonizePrefixes($expected, $source, $prefixes) {
        $this->assertXmlEquals($expected, UXMLDoc::createDocument($source)->canonizePrefixes($prefixes)->xml, UXMLDoc::createDocument($source)->canonizePrefixes($prefixes)->xml);
    }

    function provCanonizePrefixes() {
        return [
            ['<?xml version="1.0" encoding="UTF-8"?><data/>', '<data/>', null],
            ['<?xml version="1.0" encoding="UTF-8"?><data/>', '<data/>', []],
            ['<?xml version="1.0" encoding="UTF-8"?><zz:data xmlns:zz="kalap"/>', '<zz:data xmlns:zz="kalap"/>', []],
            ['<?xml version="1.0" encoding="UTF-8"?><ss:data xmlns:ss="kalap"/>', '<zz:data xmlns:zz="kalap"/>', ['kalap' => 'ss']],
            ['<data z="1">text<sub a="23"/></data>', '<data z="1">text<sub a="23"/></data>', null],
            [
                '<aa:data xmlns:aa="egyik" xmlns:bb="uri:oid:masik" z="1">text<bb:sub xmlns:bb="uri:oid:masik" a="23"/></aa:data>',
                '<xx:data xmlns:xx="egyik" z="1">text<sub xmlns="uri:oid:masik" a="23"/></xx:data>',
                ['egyik' => 'aa', 'uri:oid:masik' => 'bb']
            ],
            [
                '<aa:data xmlns:aa="egyik" xmlns:bb="uri:oid:masik" z="1">text<bb:sub xmlns:bb="uri:oid:masik" a="23"><bb:sub2 /></bb:sub></aa:data>',
                '<xx:data xmlns:xx="egyik" z="1">text<sub xmlns="uri:oid:masik" a="23"><sub2 /></sub></xx:data>',
                ['egyik' => 'aa', 'uri:oid:masik' => 'bb']
            ],
            [
                '<aa:data xmlns:aa="egyik" z="1">text<sub xmlns="uri:oid:masik" a="23"><sub2 /></sub></aa:data>',
                //'<aa:data xmlns:aa="egyik" xmlns:default="uri:oid:masik" z="1">text<default:sub xmlns="uri:oid:masik" a="23"><default:sub2 /></default:sub></aa:data>',
                '<xx:data xmlns:xx="egyik" z="1">text<sub xmlns="uri:oid:masik" a="23"><sub2 /></sub></xx:data>',
                ['egyik' => 'aa']
            ],
        ];
    }

}

class TestObj extends Component {
    public $name;

    public function toString() {
        return '[' . $this->name . ']';
    }
}
