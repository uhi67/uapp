<?php

use Codeception\Test\Unit;

/**
 * Testing Inet data type class
 *
 * @author Peter Uherkovich
 * @copyright 2018
 * @filesource InetTest.php
 */
class InetTest extends Unit {

    /**
     * @dataProvider provider_valid
     *
     * @param string $ip
     * @param boolean $expected
     * @return void
     * @throws InternalException
     * @throws UAppException
     */
    function test_valid($ip, $expected) {
        $result = Inet::isValid($ip);
        $this->assertSame($expected, $result);
    }

    function provider_valid() {
        return [ // $ip, $expected
            ['were', false],
            ['193.6.50.100', true],
            ['1111:2222:3333:4444:5555::6666', true],
            ['11:22:33:44:55:66', false],
        ];
    }

    /**
     * @dataProvider provider_create
     *
     * @param $ip
     * @param $expected
     * @param $ver
     * @param $mask
     * @param $ishost
     * @param $tostring
     * @return void
     * @throws InternalException
     * @throws UAppException
     */
    function test_create($ip, $expected, $ver, $mask, $ishost, $tostring) {
        $inet = new Inet($ip);
        $this->assertSame($inet->valid, $expected);
        $this->assertSame($inet->ver, $ver);
        $this->assertSame($inet->mask, $mask);
        $this->assertSame($inet->isHost(), $ishost);
        $this->assertSame($inet->toString(), $tostring);
    }

    function provider_create() {
        return [ // $ip, $expected, $ver, $mask, $ishost, $tostring
            ['were', false, 0, 0, false, ''],
            ['193.6.50.100', true, 4, 32, true, '193.6.50.100'],
            ['1111:2222:3333:4444:5555::6666', true, 6, 128, true, '1111:2222:3333:4444:5555::6666'],
            ['11:22:33:44:55:66', false, 0, 0, false, ''],
            ['193.6.50.100/30', true, 4, 30, false, '193.6.50.100/30'],
            ['1111:2222:3333:4444:5555::6666/64', true, 6, 64, false, '1111:2222:3333:4444:5555::6666/64'],
        ];
    }

    /**
     * @dataProvider provider_getMaskValue
     *
     * @param Inet $range
     * @param string $mask
     * @return void
     */
    function test_getMaskValue($range, $mask) {
        $this->assertEquals($mask, $range->maskValue());
    }

    function provider_getMaskValue() {
        return [
            [new Inet('193.6.48.0/20'), "\xFF\xFF\xF0\x00"],
            [new Inet('193.6.50.100/20'), "\xFF\xFF\xF0\x00"],
            [new Inet('193.6.50.100/24'), "\xFF\xFF\xFF\x00"],
            [new Inet('193.6.50.100'), "\xFF\xFF\xFF\xFF"],
            [new Inet('193.6.50.100/32'), "\xFF\xFF\xFF\xFF"],
        ];
    }

    /**
     * @dataProvider provider_getFirst
     *
     * @param Inet $range
     * @param string $first
     * @return void
     */
    function test_getFirst($range, $first) {
        $this->assertEquals($first, $range->first);
    }

    function provider_getFirst() {
        return [
            [new Inet('193.6.48.0/20'), '193.6.48.0'],
            [new Inet('193.6.50.100/20'), '193.6.48.0'],
            [new Inet('193.6.50.100/24'), '193.6.50.0'],
            [new Inet('193.6.50.100'), '193.6.50.100'],
            [new Inet('193.6.50.100/32'), '193.6.50.100'],
        ];
    }

    /**
     * @dataProvider provider_getLast
     *
     * @param Inet $range
     * @param string $first
     * @return void
     */
    function test_getLast($range, $first) {
        $this->assertEquals($first, $range->last);
    }

    function provider_getLast() {
        return [
            [new Inet('193.6.48.0/20'), '193.6.63.255'],
            [new Inet('193.6.50.100/20'), '193.6.63.255'],
            [new Inet('193.6.50.100/24'), '193.6.50.255'],
            [new Inet('193.6.50.100'), '193.6.50.100'],
            [new Inet('193.6.50.100/32'), '193.6.50.100'],
        ];
    }


    /**
     * @dataProvider provider_contains
     *
     * @param Inet $range
     * @param Inet $ip
     * @param boolean $expected
     * @return void
     * @throws InternalException
     * @throws UAppException
     */
    function test_contains($range, $ip, $expected) {
        $this->assertEquals($expected, $range->contains($ip));
    }

    function provider_contains() {
        return [
            [new Inet('193.6.48.0/20'), '193.6.48.0', true],
            [new Inet('193.6.50.100/20'), '193.6.50.110', true],
            [new Inet('193.6.50.100/24'), '193.6.51.0', false],
            [new Inet('193.6.50.100'), new Inet('193.6.50.100'), true],
            [new Inet('193.6.50.100'), '193.6.50.101', false],
            [new Inet('193.6.50.100/32'), '193.6.50.0', false],
            [new Inet('193.6.50.100/32'), '193.6.50.100/32', true],
        ];
    }

    /**
     * @dataProvider provAdd
     * @param $ip
     * @param $num
     * @param $expected
     * @return void
     */
    public function testAdd($ip, $num, $expected) {
        $this->assertEquals($expected, inet_ntop(Inet::add(inet_pton($ip), $num)));
    }

    function provAdd() {
        return [
            ['193.6.48.0', 1, '193.6.48.1'],
            ['193.6.48.0', -1, '193.6.47.255'],
            ['127.0.0.1', -2, '126.255.255.255'],
            ['126.255.255.255', 2, '127.0.0.1'],
        ];
    }

    /**
     * @dataProvider provNext
     * @param $ip
     * @param $expected
     * @return void
     * @throws InternalException
     * @throws UAppException
     */
    public function testNext($ip, $expected) {
        $inet = new Inet($ip);
        $this->assertEquals($expected, $inet->next()->toString());
    }

    function provNext() {
        return [
            ['193.6.48.0', '193.6.48.1'],
            ['126.255.255.255', '127.0.0.0'],
        ];
    }

    /**
     * @dataProvider provSize
     * @param $ip
     * @param $expected
     * @return void
     * @throws InternalException
     * @throws UAppException
     */
    public function testSize($ip, $mask, $expected) {
        $inet = new Inet($ip, $mask);
        $this->assertEquals($expected, $inet->size);
    }

    function provSize() {
        return [
            ['193.6.48.0', null, 1],
            ['193.6.48.0', 24, 256],
        ];
    }

    /**
     * @dataProvider provOffset
     * @param string $ip
     * @param int $offset
     * @param string $expected
     * @return void
     * @throws InternalException
     * @throws UAppException
     */
    public function testOffest($ip, $offset, $expected) {
        $inet = new Inet($ip);
        $this->assertEquals($expected, $inet->relative($offset)->toString());
    }

    function provOffset() {
        return [
            ['193.6.48.0/30', 1, '193.6.48.1'],
            ['193.6.48.0/16', 256, '193.6.49.0'],
            ['127.0.0.0/24', -1, '127.0.0.255'],
            ['127.0.0.0/16', -1, '127.0.255.255'],
        ];
    }

}
