<?php

use Codeception\Test\Unit;

require_once "models/Systext.php";

class L10nTest extends Unit {
    /** @var DBX $db */
    private $db;

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @throws
     */
    protected function _before() {
        $testdbconfig = require(dirname(__DIR__) . '/dbconfig.php');
        $this->db = Component::create($testdbconfig);
        UApp::setLang('hu');
        UApp::$config = [
            'baseurl' => 'https://test.uhisoft.hu/app/',
            'uapp' => [
                'path' => dirname(dirname(__DIR__)),
            ],
        ];
        L10nBase::init([]);
        L10n::$db = $this->db;
    }


    /**
     * @throws ConfigurationException
     * @throws DatabaseException
     * @throws InternalException
     * @throws ModelException
     * @throws QueryException
     * @throws ReflectionException
     * @throws UAppException
     */
    function testGetText() {
        $this->assertEquals('igaz', L10n::getText('uapp', 'true'));

        // Insert unknown text
        $this->assertEquals('*ismeretlen_*', L10n::getText('app', 'ismeretlen_'));
        // TODO: Purge unknown text
        /** @var Systext $new */
        $this->assertTrue(($new = Systext::first(['value' => 'ismeretlen_'], null, null, L10n::$db)) !== null);
        $this->assertEquals(1, Systext::deleteAll(['source' => $new->id], null, L10n::$db));
        $this->assertSame(1, $new->delete());

        $this->assertEquals('Töröl', L10n::getText('app', 'Delete'));

    }

}
