<?php

use Codeception\Test\Unit;

class FineDiffTest extends Unit {
    /** @var DBX $db */
    private $db;

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @throws
     */
    protected function _before() {
        $testdbconfig = require(dirname(__DIR__) . '/dbconfig.php');
        $this->db = Component::create($testdbconfig);
        UApp::setLang('hu');
        UApp::$config = [
            'baseurl' => 'https://test.uhisoft.hu/app/',
            'uapp' => [
                'path' => dirname(dirname(__DIR__)),
            ],
        ];
        L10nBase::init([]);
    }

    /**
     * @dataProvider provRenderUTF8FromOpcode
     * @param string $from
     * @param string $to
     * @param string $expected
     */
    function testRenderUTF8FromOpcode($from, $to, $expected) {
        $opcodes = FineDiff::getDiffOpcodes($from, $to);
        $this->assertEquals($expected, $opcodes);
        $result = FineDiff::renderToTextFromOpcodes($from, $opcodes);
        $this->assertEquals($result, $to);

    }

    function provRenderUTF8FromOpcode() {
        return [
            ['egyik Ábel', 'másik Áron', 'd3i3:másc4d3i3:ron'],
        ];
    }

}
