<?php
/**
 * @author Peter Uherkovich
 * @copyright 2018
 * @filesource MacaddrTest.php
 */

use Codeception\Test\Unit;

require_once dirname(dirname(__DIR__)) . '/lib/UAppLoader.php';
UAppLoader::registerAutoload();

/**
 * MacaddrTest
 *
 */
class MacaddrTest extends Unit {

    function test_isvalid() {
        $data = [ // $mac, $returnvalue, $expected
            ['were', false, false],
            ['112233445566', false, true],
            ['11:22:33:44:55:66', false, true],
            ['00.00.33.44.55.66', false, true],
            ['00-00-33-44-55-66', false, true],
            ['00/00/33/44/55/66', false, false],
            ['00:2a:33:44:55:66', true, '002a33445566'],
        ];
        for ($i = 0; $i < count($data); $i++) {
            list($mac, $returnvalue, $expected) = $data[$i];
            $result = Macaddr::isValid($mac, $returnvalue);
            $this->assertSame($result, $expected, "Case $i");
        }
    }

    function test_create() {
        $data = [ // $mac, $expected
            ['712233445566', 'q"3DUf'],
            ['71-22-33-44-55-66', 'q"3DUf'],
            ['00:aa:33:44:55:66', chr(0) . chr(0xaa) . '3DUf'],
            ['were', false],
        ];
        for ($i = 0; $i < count($data); $i++) {
            list($mac, $expected) = $data[$i];
            if ($expected === false) $this->expectException('InternalException');
            $result = new Macaddr($mac);
            $this->assertSame($result->value, $expected, "Case $i/A");
            $this->assertSame($result->toString(), Macaddr::canonize($mac), "Case $i/B");
        }
    }

    function test_canonize() {
        $data = [ // $mac, $expected
            [null, null],
            ['', false],
            ['were', false],
            ['112233445566', '11:22:33:44:55:66'],
            ['11:22:33:44:55:66', '11:22:33:44:55:66'],
            ['11-2a-33-44-55-6f', '11:2A:33:44:55:6F'],
        ];
        for ($i = 0; $i < count($data); $i++) {
            list($mac, $expected) = $data[$i];
            $result = Macaddr::canonize($mac);
            $this->assertSame($result, $expected, "Case $i: %s");
        }
    }

    /**
     * @dataProvider provider_canonizePart
     * @param string $mac
     * @param string $expected
     */
    function test_canonize_part($mac, $expected) {
        $result = Macaddr::canonize_part($mac);
        $this->assertSame($result, $expected);
    }

    function provider_canonizePart() {
        return [ // $mac, $expected
            [null, null],
            ['', false],
            ['were', false],
            ['1', false],
            ['11223344', '11:22:33:44'],
            ['11:22:33:44:55', '11:22:33:44:55'],
            ['11-22.3c:44:55:66', '11:22:3C:44:55:66'],
        ];
    }
}
