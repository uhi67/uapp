<?php

use Codeception\Test\Unit;

require_once "models/Dhcpdomain.php";
require_once "models/Dhcpoption.php";
require_once "models/Dhcpoptionname.php";
require_once "models/Dhcpoptiontype.php";
require_once "models/Dhcphost.php";
require_once "models/Org.php";
require_once "models/Person.php";

/** @noinspection PhpUndefinedClassInspection */

/** @noinspection PhpUndefinedNamespaceInspection */

class UListTest extends Unit {
    private $db;
    /** @var UXMLDoc $xmldoc */
    private $xmldoc;

    /**
     * @var UnitTester
     */
    protected $tester;

    /**
     * @throws InternalException
     * @throws ReflectionException
     */
    protected function _before() {
        $testdbconfig = require(dirname(__DIR__) . '/dbconfig.php');
        $this->db = Component::create($testdbconfig);
        $this->xmldoc = new UXMLDoc();
    }

    protected function _after() {
    }


    /**
     * @dataProvider provider_descField
     *
     * @param mixed $order
     * @param mixed $expected
     * @return void
     */
    function test_descField($order, $expected) {
        $r = UList::descField($order);
        $this->assertEquals($expected, $r);
    }

    function provider_descField() {
        return [
            ['', ''],
            ['alma', 'alma desc'],
            ['alma desc', 'alma'],
            ['alma nulls first', 'alma desc nulls last'],
            ['alma desc nulls last', 'alma nulls first'],
            ['alma NULLS first', 'alma desc NULLS last'],
            ['alma DESC nulls LAST', 'alma nulls first'],
        ];
    }

    /**
     * @dataProvider provider_descOrders
     *
     * @param mixed $orders
     * @param mixed $expected
     * @return void
     */
    function test_descOrders($orders, $expected) {
        $r = UList::descOrders($orders);
        $this->assertEquals($expected, $r);
    }

    function provider_descOrders() {
        return [
            [[], []],
            [[''], ['', '']],
            [['alma'], ['alma', 'alma desc']],
            [['alma desc'], ['alma desc', 'alma']],
            [['alma nulls first'], ['alma nulls first', 'alma desc nulls last']], #4
            [['alma desc nulls last'], ['alma desc nulls last', 'alma nulls first']],
            [['alma, banan'], ['alma, banan', 'alma desc, banan desc']],
            [
                ['alma desc, banan', 'citrom'],
                ['alma desc, banan', 'alma, banan desc', 'citrom', 'citrom desc']
            ],
            [
                ['citrom', 'alma nulls FIRST, banan desc'],
                ['citrom', 'citrom desc', 'alma nulls FIRST, banan desc', 'alma desc nulls last, banan']
            ],
            [
                ['banan, alma DESC nulls last', 'citrom desc'],
                ['banan, alma DESC nulls last', 'banan desc, alma nulls first', 'citrom desc', 'citrom']
            ],
        ];
    }

    /**
     * @throws ConfigurationException
     * @throws InternalException
     */
    function test_pageCount() {
        $list = new UList(null, [
            'count' => 5
        ]);
        $this->assertEquals(5, $list->count);
    }

    /**
     * @throws ConfigurationException
     */
    function test_getUserPageLength() {
        $this->assertEquals(UList::getUserPageLength(), 50);
    }

    /**
     * @dataProvider provider_usingList
     *
     * @param mixed $order
     * @param mixed $pattern
     * @param mixed $df // Including deleted
     * @param mixed $count // All elements
     * @param mixed $items // currently showed
     * @param mixed $first // The first visible
     *
     * @return void
     * @throws ConfigurationException
     * @throws DOMException
     * @throws DatabaseException
     * @throws InternalException
     * @throws QueryException
     * @throws UAppException
     * @throws ReflectionException
     */
    function test_usingList($order, $pattern, $df, $count, $items, $first) {
        // Preparing fake request
        Util::setReq([
            'list_name' => 'hostlist',
        ]);
        if ($order) Util::setReq('list_order', $order);
        if ($pattern) Util::setReq('list_search', $pattern);

        $node_parent = $this->xmldoc->documentElement;

        /** @var Dhcpdomain $dhcpdomain */
        $dhcpdomain = Dhcpdomain::first(3, null, null, $this->db);

        $orders = ['name', 'owner', 'created', 'mac', 'ip', 'active', 'creator', 'lastuse'];
        $itemname = 'dhcphost';

        $list = new UList(null, [
            'id' => 'hostlist_' . $dhcpdomain->id,
            'name' => 'hostlist',
            'item_name' => $itemname,
            'orders' => UList::descOrders($orders),
        ]);

        if ($order == 3) {
            $this->assertEquals('owner desc', $list->orderName);
            $this->assertEquals([['owner', DBX::ORDER_DESC, null]], $list->order());
        }

        $pattern = $list->setSearch();
        $params = ['parent' => $dhcpdomain->id, 'df' => $df, 'pattern' => $pattern];
        $condition = [
            'dhcpdomain' => '$parent',
            ['or', ['is null', 'deleted'], ['=', '$df', 1]],
        ];
        if ($pattern == ':lastmove') {
            $lastmoves = Util::getSession('lastmove');
            if (!is_array($lastmoves)) $lastmoves = [$lastmoves];
            $condition[] = ['in', 'id', $lastmoves];
        } else if ($pattern) {
            $condition[] = ['RLIKE', ['text()', $list->fieldName()], '$pattern'];
        }
        $query1 = Query::createSelect([
            'from' => 'Dhcphost',
            'fields' => ['c' => ['count()', 'id']],
            'condition' => $condition,
            'params' => $params,
        ]);
        $list->count = $query1->scalar(0, $this->db);

        $this->assertLessThan($count + 3, $list->count);
        $this->assertGreaterThan($count - 3, $list->count);

        $node_list = $list->createNode($node_parent);
        $query = Query::createSelect([
            'from' => 'Dhcphost',
            'fields' => ['id', 'name', 'descr', 'mac', 'ip', 'owner', 'org', 'vendor',
                'created', 'deleted', 'creator', 'deleter', 'active', 'lastuse', 'reserved',
                'org_name' => 'org1.name',
                'owner_name' => 'owner1.name',
                'deleter_name' => 'deleter1.name',
                'creator_name' => 'creator1.name'
            ],
            'joins' => ['org1', 'owner1', 'deleter1', 'creator1'],
            'condition' => $condition,
            'orders' => $list->order(),
            'offset' => $list->offset(),
            'limit' => $list->limit(),
            'params' => $params,
            'db' => $this->db,
        ]);
        // Creates data elements
        $node_list->query($this->db, $itemname, $query->sql, $params);

        Debug::tracex('list', $node_list->C14N());
        $this->assertEquals('list', $node_list->tagName);
        $this->assertEquals($items, $node_list->childNodes->length);
        $this->assertRegExp(Util::toRegExp($first), $node_list->firstChild->getAttribute('name'));
    }

    function provider_usingList() {
        return [ // $order, $pattern, $df, $count, $items, $first
            [3, null, 0, 951, 50, 'XBpcms|WZla2U|TAwOC0|mkyLms'],
            [0, null, 0, 951, 50, ''],
            [1, null, 1, 1258, 50, 'zQxMHQ'],
            [1, 'y1z', 1, 7, 7, 'y1zYWY'],
        ];
    }

    /**
     * @dataProvider provider_datasource
     *
     * @param mixed $order
     * @param mixed $pattern
     * @param mixed $df // Including deleted
     * @param mixed $count // All elements
     * @param mixed $items // currently showed
     * @param mixed $first // The first visible
     *
     * @return void
     * @throws ConfigurationException
     * @throws DatabaseException
     * @throws InternalException
     * @throws QueryException
     * @throws DOMException
     * @throws UAppException
     * @throws ReflectionException
     */
    function test_datasource($order, $pattern, $df, /** @noinspection PhpUnusedParameterInspection */ $count, $items, $first) {
        // Preparing fake request
        Util::setReq([
            'list_name' => 'hostlist',
        ]);
        if ($order) Util::setReq('list_order', $order);
        if ($pattern) Util::setReq('list_search', $pattern);

        $node_parent = $this->xmldoc->documentElement;

        /** @var Dhcpdomain $dhcpdomain */
        $dhcpdomain = Dhcpdomain::first(3, null, null, $this->db);

        $orders = ['name', 'owner', 'created', 'mac', 'ip', 'active', 'creator', 'lastuse'];
        $itemname = 'dhcphost';
        $params = ['parent' => $dhcpdomain->id, 'df' => $df];

        $tester = $this;
        $ds = new QueryDataSource([
            'modelClass' => 'Dhcphost',
            'query' => Query::createSelect([
                'from' => 'Dhcphost',
                'fields' => ['id', 'name', 'descr', 'mac', 'ip', 'owner', 'org', 'vendor',
                    'created', 'deleted', 'creator', 'deleter', 'active', 'lastuse', 'reserved',
                    'org_name' => 'org1.name',
                    'owner_name' => 'owner1.name',
                    'deleter_name' => 'deleter1.name',
                    'creator_name' => 'creator1.name'
                ],
                'joins' => ['org1', 'owner1', 'deleter1', 'creator1'],
                'condition' => [
                    'dhcpdomain' => '$parent',
                    ['or', ['is null', 'deleted'], ['=', '$df', 1]],
                ],
                'params' => $params,
                'db' => $this->db,
            ]),
            'patternSetter' =>
            /**
             * @param $dataSource
             * @param $p
             * @param $params
             */
                function ($dataSource, $p, $params) use ($pattern, $tester) {
                    if ($pattern != $p) throw new Exception('Pattern lost...');
                    $tester->assertEquals($pattern, $p);
                    /** @var QueryDataSource $dataSource */
                    if ($p == ':lastmove') {
                        $lastmoves = Util::getSession('lastmove');
                        if (!is_array($lastmoves)) $lastmoves = [$lastmoves];
                        $dataSource->query->andWhere(['in', 'id', $lastmoves]);
                    } else if ($p) {
                        $dataSource->query->andWhere(['RLIKE', ['text()', $params['fieldName']], '$pattern']);
                        $dataSource->query->bind(['pattern' => $p]);
                    }
                },
        ]);

        $list = new UList(null, [
            'id' => 'hostlist_' . $dhcpdomain->id,
            'name' => 'hostlist',
            'item_name' => $itemname,
            'orders' => UList::descOrders($orders),
            'dataSource' => $ds,
            'withSearch' => true,
        ]);

        $tester->assertEquals($list->name, Util::getReq('list_name'));
        $tester->assertEquals($list->setSearch(), Util::getReq('list_search'));

        $this->assertEquals('Dhcphost', $ds->query->modelname);
        $node_list = $list->createNode($node_parent);

        Debug::tracex('list', $node_list->C14N());
        $this->assertEquals('list', $node_list->tagName);
        $this->assertEquals($items, $node_list->childNodes->length);
        $this->assertRegExp(Util::toRegExp($first), $node_list->firstChild->getAttribute('name'), Util::objtostr($node_list->firstChild));
    }

    function provider_datasource() {
        return [ // $order, $pattern, $df, $count, $items, $first
            [3, null, 0, 951, 50, 'XBpcms|WZla2U|TAwOC0|mkyLms'],
            [0, null, 0, 951, 50, ''],
            [1, null, 1, 1258, 50, 'zQxMHQ'],
            [1, 'y1z', 1, 7, 7, 'y1zYWY'],
        ];
    }

    /**
     * @dataProvider provPrepareColumns
     *
     * @param array $columns
     * @param array $orders
     * @param array $expected
     * @param string $colattr
     *
     * @throws ConfigurationException
     * @throws DOMException
     * @throws InternalException
     * @throws QueryException
     * @throws ReflectionException
     * @throws UAppException
     */
    function testPrepareColumns($columns, $orders, $expected, $colattr) {
        $list = new UList(null, [
            'dataSource' => new QueryDataSource(['query' => Query::createSelect(['from' => 'Dhcphost', 'db' => $this->db, 'condition' => ['id' => null]])]),
            'columns' => $columns,
        ]);
        foreach ($expected as $i => $col) {
            $this->assertEquals($orders, $list->orders, Util::objtostr($list->orders));
            foreach ($col as $key => $value) {
                $this->assertEquals($value, $list->columns[$i][$key], "Case $i/$key");
            }
        }
        $node_list = $list->createNode($this->xmldoc->documentElement);
        $this->assertEquals($colattr, $node_list->getAttribute('columns'));
    }

    function provPrepareColumns() {
        return [ // $columns, $orders, $expected, $colattr
            [
                [
                    ['name' => 'Első', 'ord' => [['name', DBX::ORDER_DESC]], 'width' => 100],
                    ['field' => ['Dhcphost', 'mac'], 'width' => [100, 200], 'ord' => true, 'limit' => 720],
                ],
                ['"name" DESC', '"name" NULLS LAST', '"mac"', '"mac" DESC'],
                [
                    ['name' => 'Első', 'ord1' => 0, 'ord2' => 1, 'width1' => 100, 'width2' => 100],
                    ['name' => 'MAC cím', 'title' => '12 hexa számjegy bármilyen szeparátorral vagy anélkül', 'width1' => 100, 'width2' => 200, 'ord1' => 2, 'ord2' => 3],
                ],
                'Első,,0,1,100,100,|MAC cím,12 hexa számjegy bármilyen szeparátorral vagy anélkül,2,3,100,200,720|',
            ]
        ];
    }
}
