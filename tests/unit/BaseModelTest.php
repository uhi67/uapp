<?php

namespace unit;

require_once "models/TestUser.php";
require_once "models/TestUser.php";
require_once dirname(__DIR__) . '/_support/Helper/UnitTest.php';

use Ansi;
use ArrayUtils;
use BaseModel;
use DateTime;
use Inet;
use InternalException;
use L10nBase;
use ModelException;
use ReflectionException;
use TestUser;
use UApp;
use UAppException;
use UnitTest;
use UnitTester;
use Util;
use UXMLDoc;
use UXMLElement;

/**
 * QueryTest -- Testing Query class
 */
class BaseModelTest extends UnitTest {
    /** @var UXMLDoc $xmldoc */
    private $xmldoc;

    /** @var UnitTester $tester */
    protected $tester;

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @throws InternalException
     */
    protected function _before() {
        UApp::$config = [
            'uapp' => [
                'path' => dirname(dirname(__DIR__)),
            ],
            'datapath' => dirname(__DIR__) . '/_output',
        ];
        /** @noinspection PhpUnhandledExceptionInspection */
        UApp::setLang('hu');
        $this->xmldoc = new UXMLDoc();
        $this->xmldoc->locale = 'hu';
        L10nBase::init([]);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    protected function _after() {
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     */
    function test_attributes() {
        $aa = ['id', 'name', 'email', 'loggedin', 'lastclick', 'lastlogin', 'uid', 'org', 'idp', 'enabled', 'logins', 'descr', 'other'];
        $this->assertEquals($aa, TestUser::attributes());

        $ff = array_diff($aa, ['other']);
        $this->assertEquals(array_combine($ff, $ff), TestUser::fields());

        $user = new TestUser(['name' => 'Aladár', 'descr' => 'setter']);
        // Attribute without loader
        $this->assertSame(null, $user->other);
        $this->assertFalse(isset($user->other));
        $this->assertTrue(isset($user->name));
        $this->assertTrue(isset($user->descr));

        $this->assertEquals(['name' => 'Aladár', 'descr' => 'setter'], $user->namedAttributes(['name', 'descr']));
        $this->assertSame(['name' => 'Aladár', 'descr' => 'setter'], array_filter($user->attributes));

        $user->setAttributes(['name' => 'Dezső', 'email' => 'desire@antoine.hu']);
        $this->assertSame(['name' => 'Dezső', 'email' => 'desire@antoine.hu', 'descr' => 'setter'], array_filter($user->attributes));

        // Exception test must be the last one in the test method
        $this->expectException('ModelException');
        BaseModel::attributeType('id');
    }


    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @dataProvider provide_validate
     *
     * @param array $data
     * @param array $expected
     *
     * @throws InternalException
     * @throws UAppException
     */
    function test_validate($data, $expected) {
        $user = new TestUser($data);
        $valid = $user->validate();
        $result = $user->errors;
        $this->assertEquals($expected, $result);
        $this->assertEquals(empty($expected), $valid);

        // Partial validation
        $ff = ['name', 'logins'];
        $valid = $user->validate($ff);
        $result = $user->errors;
        // Array_filter on keys before php 5.6
        $ee = array_intersect_key($expected, array_flip(array_filter(array_keys($expected), function ($key) use ($ff) {
            return in_array($key, $ff);
        })));
        $this->assertEquals($ee, $result);
        $this->assertEquals(empty($ee), $valid);
    }

    /** @noinspection PhpMethodNamingConventionInspection */
    function provide_validate() { // $data, $expected
        return [
            [ // $data, $expected
                [
                    'name' => '_TestPerson_1',
                ],
                [
                ],
            ],
            [
                [
                    'name' => '_TestPerson_1ÉOREITUMQFWE',
                    'uid' => 'user',
                    'email' => 'user@test.test',
                    'logins' => '1E3',
                ],
                [
                    'name' => ['nem lehet hosszabb, mint 15 karakter'],
                    'uid' => ['üresnek kell lennie, ha `idp` nem meghatározott'],
                ],
            ],
            [
                [
                    'name' => 'Test Elek Egy',
                    'org' => 2,
                    'descr' => 'kalap',
                    'logins' => 'x',
                ],
                [
                    'descr' => ['érvénytelen xml szöveg'],
                    'logins' => ['érvénytelen egész szám'],
                ],
            ],
            [
                [
                    'idp' => 'x',
                    'uid' => 'user',
                    'email' => 'other@test.test',
                    'descr' => new UXMLDoc(),
                    'logins' => '123456789101112131415161718192078946572365670845708504219967',
                ],
                [
                    'name' => ['kötelező'],
                    'idp' => ['érvénytelen URL'],
                    'uid' => ['must be user part of email'],
                ],
            ],
            [
                [
                    'name' => '_TestPerson_1',
                    'email' => 'user',
                    'idp' => 'http://idp.test/metadata',
                    'descr' => '<a></a>',
                    'logins' => 12.34,
                ],
                [
                    'email' => ['érvénytelen e-mail cím'],
                    'uid' => ['must be user part of email'],
                ],
            ],
        ];
    }

    /**
     * @throws InternalException
     */
    function testDateValidate() {
        $user = new TestUser(['lastlogin' => '2018-12-31']);
        $this->assertTrue($user->dateValidate('lastlogin'));
        $user->lastlogin = 'rgcwer';
        $this->assertFalse($user->dateValidate('lastlogin'));
    }

    /**
     * @throws InternalException
     */
    function testTimeValidate() {
        $user = new TestUser(['lastlogin' => '12:59']);
        $this->assertTrue($user->timeValidate('lastlogin'));
        $this->assertEquals('12:59', $user->lastlogin);
        $user->lastlogin = 'rgcwer';
        $this->assertFalse($user->timeValidate('lastlogin'));
    }

    /**
     * @dataProvider provIntValidate
     *
     * @param mixed $data
     * @param mixed $int
     * @param mixed $integer
     * @param boolean $boolean
     * @param float $float
     *
     * @throws InternalException
     */
    function testIntValidate($data, $int, $integer, $boolean, $float) {
        $user = new TestUser(['logins' => $data]);
        if ($int !== 'fail') {
            $this->assertTrue($user->intValidate('logins'));
            $this->assertSame($int, $user->logins, Ansi::color('int ', 'red') . $data . '(' . gettype($data) . ')');
        } else $this->assertFalse($user->intValidate('logins'));

        $user->logins = $data;
        if ($integer !== 'fail') {
            $this->assertTrue($user->integerValidate('logins'), Ansi::color(Util::objtostr($user->friendlyErrors), 'red'));
            $this->assertSame($integer, $user->logins, Ansi::color('integer ', 'red') . $data . '(' . gettype($data) . ')');
        } else $this->assertFalse($user->integerValidate('logins'));

        $user->logins = $data;
        if ($boolean !== 'fail') {
            $this->assertTrue($user->booleanValidate('logins'));
            $this->assertSame($boolean, $user->logins, Ansi::color('boolean ', 'red') . $data . '(' . gettype($data) . ')');
        } else $this->assertFalse($user->booleanValidate('logins'));

        $user->logins = $data;
        if ($float !== 'fail') {
            $this->assertTrue($user->floatValidate('logins'));
            $this->assertSame($float, $user->logins, Ansi::color('float: ', 'red') . $data . '(' . gettype($data) . ')');
        } else $this->assertFalse($user->floatValidate('logins'));
    }

    function provIntValidate() {
        return [ // $data, $int, $integer, $boolean, $float
            [1, '1', 1, true, 1.0],
            [0, '0', 0, false, 0.0],
            ['1', '1', 1, true, 1.0],
            ['0', '0', 0, false, 0.0],
            ['-1', '-1', -1, true, -1.0],
            [-1, '-1', -1, true, -1.0],
            [13, '13', 13, 'fail', 13.0],
            [14.7, '14', 14, 'fail', 14.7], // 7
            ['27', '27', 27, 'fail', 27.0],
            ['27.833', '27', 27, 'fail', 27.833],
            ['27A', 'fail', 'fail', 'fail', 'fail'],
            ['27E6', '27000000', 27000000, 'fail', 27E6],
            ['27E20', '2700000000000000000000', 'fail', 'fail', 27E20], // 12
            ['', null, null, 'fail', 'fail'], // 13
            [false, '0', 0, false, 'fail'],
            [true, '1', 1, true, 'fail'],
            ['f', 'fail', 'fail', false, 'fail'],
            ['t', 'fail', 'fail', true, 'fail'],
            ['y', 'fail', 'fail', true, 'fail'], // 18
            ['n', 'fail', 'fail', false, 'fail'],
            [null, null, null, null, null],
            ['alma', 'fail', 'fail', 'fail', 'fail'],
            [new DateTime(), 'fail', 'fail', 'fail', 'fail'],
            [[], 'fail', 'fail', 'fail', 'fail'],
        ];
    }

    /**
     * @dataProvider provTypeValidate
     * @param mixed $data
     * @param string $type
     * @param boolean $valid
     *
     * @throws InternalException
     * @throws UAppException
     */
    function testTypeValidate($data, $type, $valid) {
        $user = new TestUser(['descr' => $data]);
        $this->assertSame($valid, $user->typeValidate('descr', $type));
    }

    /**
     * @return array
     * @throws InternalException
     */
    function provTypeValidate() {
        $this->xmldoc = new UXMLDoc();
        $this->xmldoc->locale = 'hu';
        return [ // $data, $type, $valid
            [null, 'boolean', true],
            ['null', 'string', true],
            ['null', 'integer', false],
            [new TestUser(), 'object', true],
            [new TestUser(), 'TestUser', true], // #4
            [new TestUser(), 'BaseModel', true],
            [new TestUser(), 'Model', false],
            ['123', 'integer', true],
            ['<uapp:data/>', 'xml', true],
            ['<uapp:data>', 'xml', false],
            [$this->xmldoc, 'xml', true],
            [$this->xmldoc->documentElement, 'xml', true],
            [23, 'time', true],
            ['23', 'time', false],
            [new DateTime(), 'time', true],
        ];
    }


    /**
     * @dataProvider provTrimValidate
     *
     * @param mixed $data
     * @param string $trim
     * @param string $lc
     *
     * @throws InternalException
     */
    function testTrimValidate($data, $trim, $lc) {
        $user = new TestUser(['descr' => $data]);
        if ($trim === false) {
            $this->assertFalse($user->trimValidate('descr'), 'Error: ' . Util::objtostr(ArrayUtils::getValue($user->friendlyErrors, 'descr')));
        } else {
            $this->assertTrue($user->trimValidate('descr'), 'Error: ' . Util::objtostr(ArrayUtils::getValue($user->friendlyErrors, 'descr')));
            $this->assertSame($trim, $user->descr);
        }
        if ($lc === false) {
            $this->assertFalse($user->lowercaseValidate('descr'), 'Error: ' . Util::objtostr(ArrayUtils::getValue($user->friendlyErrors, 'descr')));
        } else {
            $this->assertTrue($user->lowercaseValidate('descr'), 'Error: ' . Util::objtostr(ArrayUtils::getValue($user->friendlyErrors, 'descr')));
            $this->assertSame($lc, $user->descr);
        }
    }

    /**
     * @return array
     * @throws InternalException
     */
    function provTrimValidate() {
        return [ // $data, $trim, $lc
            [null, null, null],
            ['', '', ''],
            [' Alma', 'Alma', 'alma'],
            [new Inet('193.6.50.100'), '{193.6.50.100}', '{193.6.50.100}'],
            [new TestUser(), false, false],
        ];
    }

    /**
     * @dataProvider provDefaultNowValidate
     * @param mixed $data
     * @param mixed $expected
     *
     * @throws InternalException
     * @throws UAppException
     */
    function testDefaultNowValidate($data, $expected) {
        $user = new TestUser(['lastlogin' => $data]);
        $result = $user->defaultNowValidate('lastlogin');
        if ($expected === false) $this->assertFalse($result);
        else $this->assertEqualsWithDelta($expected, $user->lastlogin, 5.0, '');
    }

    /**
     * @return array
     */
    function provDefaultNowValidate() {
        return [ // $data, $expected
            [null, new DateTime()],
            ['2000.01.01.', new DateTime('2000-01-01')],
            ['kalap', false],
        ];
    }

    /**
     * @dataProvider provDefaultValidate
     *
     * @param mixed $data
     * @param mixed $default
     * @param mixed $expected
     *
     * @throws InternalException
     */
    function testDefaultValidate($data, $default, $expected) {
        $user = new TestUser(['descr' => $data]);
        $result = $user->defaultValidate('descr', $default);
        if ($expected === false) $this->assertFalse($result);
        else $this->assertEquals($expected, $user->descr);
    }

    /**
     * @return array
     */
    function provDefaultValidate() {
        return [ // $data, $default, $expected
            [null, 'kalap', 'kalap'],
            ['', 'kalap', 'kalap'],
            [[], ['kalap'], ['kalap']],
            ['alma', 'kalap', 'alma'],
        ];
    }

    /**
     * @throws InternalException
     */
    function testTimeStampValidate() {
        $user = new TestUser(['lastlogin' => new DateTime('2009-12-31T23:59+0100')]);
        $user->timestampValidate('lastlogin');
        $this->assertEquals(new DateTime('2009-12-31T23:59+0100'), $user->lastlogin);

        $user->lastlogin = new DateTime('2009-12-31T23:59+0200');
        $user->timestampValidate('lastlogin');
        $this->assertEquals(new DateTime('2009-12-31T22:59+0100'), $user->lastlogin);
    }


    /**
     * @dataProvider provCheckPatterns
     * @param string|string[] $values
     * @param string[] $patterns
     * @param bool $expected
     *
     * @throws InternalException
     * @throws ModelException
     */
    function testCheckPatterns($values, $patterns, $expected) {
        $this->assertEquals($expected, BaseModel::checkPatterns($values, $patterns));
    }

    function provCheckPatterns() {
        return [
            ['', [], false],
            ['alma', ['/lm/'], true],
            ['alma', ['/lm/', '/jjj/'], true],
            ['alma', ['/ggg/', '/lm/', '/jjj/'], true],
            ['alma', ['/ggg/', ['/lm/', '/ma/'], '/jjj/'], true],
            ['alma', ['/ggg/', ['/lm/', '/max/'], '/jjj/'], false],
            [['alma', ''], ['/ggg/', ['/lm/', '/max/'], '/jjj/'], false],
        ];
    }


    /**
     * @dataProvider provIf2Validate
     *
     * @param string $value
     * @param string $value2
     * @param string $refvalue
     * @param string[] $rules
     * @param bool $expected
     *
     * @throws InternalException
     */
    function testIf2Validate($value, $value2, $refvalue, $rules, $expected) {
        $user = new TestUser(['descr' => $value, 'name' => $value2]);
        $this->assertEquals($expected, call_user_func_array([$user, 'if2Validate'], array_merge(['descr', 'name', $refvalue], $rules)));
    }

    function provIf2Validate() {
        return [
            ['alma', 'feri', 'feri', [['pattern', ['/aa/', ['/al/', '/ma/']]]], true],
            ['alma', 'feri', 'feri', [['pattern', ['/aa/', ['/al/', '/max/']]]], false],
            ['alma', 'feri', 'fer', [['pattern', ['/aa/', ['/al/', '/max/']]]], true],
            ['alma', 'feri', 'feri', [['pattern', ['/aa/', ['/al/', '/ma/']]], 'number'], false],
            ['alma', 'feri', 'feri', [['pattern', ['/aa/', ['/al/', '/ma/']]], ['length', 2, 5]], true],
            ['alma', 'feri', 'feri', [['pattern', ['/aa/', ['/al/', '/ma/']]], ['between', 'körte', 'szilva']], false],
            ['alma', 'feri', 'feri', [['pattern', ['/aa/', ['/al/', '/ma/']]], ['between', 'ablak', 'zsiráf']], true],
        ];
    }

    /**
     * @dataProvider provArrayValidate
     *
     * @param string $data
     * @param string $pattern
     * @param array $expected
     *
     * @throws InternalException
     */
    function testArrayValidate($data, $pattern, $expected) {
        $user = new TestUser(['descr' => $data]);
        $this->assertTrue($user->arrayValidate('descr', $pattern));
        $this->assertEquals($expected, $user->descr);
    }

    /**
     * @return array
     */
    function provArrayValidate() {
        return [ // $data, $pattern, $expected
            [null, ' ', null],
            ['Egy kis malac', ' ', ['Egy', 'kis', 'malac']],
            [['Egy', 'kis', 'malac'], 'x', ['Egy', 'kis', 'malac']],
            ['alma', 'kalap', ['alma']],
        ];
    }

    /**
     * @dataProvider provCreateNode
     */
    function testCreateNode($expected, $data, $options) {
        $node_list = $this->xmldoc->documentElement->addNode(substr(md5(Util::objtostr($data)), 0, 6));
        $testUser = new TestUser($data);
        $node = $testUser->createNode($node_list, $options);
        $this->assertEquals($expected, $node_list->xml);
    }

    function provCreateNode() {
        return [ // $expected, $data, $options
            [ // case 0
                "<_0c8067>\n  <testuser name=\"_TestPerson_1\"/>\n</_0c8067>",
                [
                    'name' => '_TestPerson_1',
                ],
                null,
            ],
            [
                "<f0e177>\n  <user name=\"_TestPerson_2\" email=\"user2@test.hu\" x=\"user2\"/>\n</f0e177>",
                [
                    'name' => '_TestPerson_2',
                    'email' => 'user2@test.hu',
                    'uid' => 'user2'
                ],
                ['nodeName' => 'user', 'attributes' => ['name', 'email'], 'extra' => ['x' => 'uid']],
            ]
        ];
    }

    /**
     * @dataProvider provCreateNodes
     */
    function testCreateNodes($expected, $data, $options) {
        $node_list = $this->xmldoc->documentElement->addNode(substr(md5(Util::objtostr($data)), 0, 6));
        $testUsers = array_map(function ($item) {
            return new TestUser($item);
        }, $data);
        $this->assertEquals(count($data), count($testUsers));
        $first = TestUser::createNodes($node_list, $testUsers, $options);
        $this->assertTrue($first instanceof UXMLElement);
        $this->assertXmlEquals($expected, $node_list->xml);
        if ($options === null) {
            $node_list->removeChildren();
            $first = TestUser::createNodes($node_list, $testUsers, $options);
            $this->assertTrue($first instanceof UXMLElement);
            $this->assertXmlEquals($expected, $node_list->xml);
        }
    }

    function provCreateNodes() {
        return [ // $expected, $data, $options
            [ // case 0
                "<_6fadb3>\n  <testuser name=\"_TestPerson_1\"/>\n  <testuser name=\"_TestPerson_2\" email=\"user2@test.hu\" uid=\"user2\"/>\n</_6fadb3>",
                [
                    [
                        'name' => '_TestPerson_1',
                    ],
                    [
                        'name' => '_TestPerson_2',
                        'email' => 'user2@test.hu',
                        'uid' => 'user2'
                    ],
                ],
                null,
            ],
            [ // case 1
                "<_8c14a6>
  <testuser name=\"_TestPerson_1\">
    <x>0</x>
  </testuser>
  <testuser name=\"_TestPerson_2\">
    <u>user2</u>
    <x>1</x>
  </testuser>
</_8c14a6>",
                [
                    [
                        'name' => '_TestPerson_1',
                        'descr' => 'izé',
                    ],
                    [
                        'name' => '_TestPerson_2',
                        'email' => 'user2@test.hu',
                        'uid' => 'user2'
                    ],
                ],
                [
                    'attributes' => ['name'],
                    'subnodeFields' => [
                        'u' => 'uid',
                        'x' => function ($index, $model) {
                            return $index;
                        }
                    ],
                ],
            ],
        ];
    }

	function testProperty() {
		$user = new TestUser(['descr'=>23]);
		$user->descr++;
		$this->assertEquals(24, $user->descr);
	}

}
