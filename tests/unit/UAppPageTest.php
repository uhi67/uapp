<?php

require_once dirname(__DIR__) . '/_support/Helper/UnitTest.php';

class SimplePage extends UAppPage {
    function loadData() {
    }

    function preConstruct() {
        $this->setDesign('simple/basic');
        $this->setView('simple');
    }

    function initUser() {
    }
}

class UAppPageTest extends UnitTest {
    /** @var UApp */
    public $app;
    /** @var DBX $db */
    public $db;

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @throws
     */
    protected function _before() {
        $config = [
            'baseurl' => 'https://test.uhisoft.hu/app/',
            'datapath' => dirname(__DIR__) . '/_data',
            'xslpath' => dirname(__DIR__) . '/_data/xsl',
            'modulepath' => dirname(__DIR__) . '/_data/testmodules',
            'uapp' => [
                'path' => dirname(dirname(__DIR__)),
            ],
            'database' => require(dirname(__DIR__) . '/dbconfig.php'),
            'l10n' => [
                'class' => 'L10nBase',    // Localization classname. Must provide getText(text, language) and formatDate(DateTime, language, type) for UApp::la/UApp:fd
                'language' => 'en',        // Default language with optional locale, may be changed by UApp::setLang(lang/locale)
            ],

        ];
        $this->db = Component::create($config['database']);

        $_SERVER['REQUEST_URI'] = '/app/simple/action';
        $_SERVER['HTTP_USER_AGENT'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0';
        $_SERVER['HTTPS'] = 'off';
        $_SERVER['HTTP_HOST'] = 'test.uapp.uhisoft.hu';
        $_SERVER['SERVER_NAME'] = 'localhost';
        $_SERVER["SERVER_PORT"] = 80;
        $_SERVER['SCRIPT_NAME'] = 'UXPageTest.php';

        if ($uapp = UApp::hasInstance()) UApp::destroyInstance();
        $this->app = new UApp($config, $this->db);

    }

    /**
     * @throws ConfigurationException
     * @throws InternalException
     * @throws UAppException
     */
    function x_testCreate() {
        /** @var UAppPage $page */
        $page = new SimplePage($this->app);

        $expected = /** @lang XML */
            '<data script="https://test.uhisoft.hu/app//simple" url="/app/simple/action" session_id="" ver="">
  <head>
    <base href="https://test.uhisoft.hu/app/"/>
    <redirect pause="1">https://test.uhisoft.hu/app//app/simple/action</redirect>
    <js>jQuery/compat.js</js>
    <js>jQuery/jquery-3.2.1.min.js</js>
    <css>jQueryUI/jquery-ui.css</css>
    <js>jQueryUI/jquery-ui.js</js>
    <css>jQueryUI/jquery-ui.min.css</css>
    <css>jQueryUI/jquery-ui.structure.css</css>
    <css>jQueryUI/jquery-ui.structure.min.css</css>
    <css>jQueryUI/jquery-ui.theme.css</css>
    <css>jQueryUI/jquery-ui.theme.min.css</css>
    <css>Main/button.css</css>
    <css>Main/main.css</css>
    <xsl module="Main" path="../../../modules/Main/main.xsl" order="0">Main/main.xsl</xsl>
    <js>Main/uapp.js</js>
    <appversion/>
    <environment/>
  </head>
  <content/>
  <control>
    <params/>
    <debug>test-trace</debug>
    <agent xml="Y" name="Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0"/>
    <script>https://test.uhisoft.hu/app//simple</script>
    <path/>
    <render>client</render>
  </control>
</data>';
        $this->assertXmlEquals($expected, $page->xmldoc->documentElement);

        $this->assertEquals('basic', $page->getDesign());
        $this->assertEquals('simple', $page->getView());

        ob_start();
        $page->render();
        $result = ob_get_contents();
        $expected = str_replace("\r", "", /** @lang XML */ '<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="https://test.uhisoft.hu/app//xsl/uxpagetest.xsl"?>
<data script="https://test.uhisoft.hu/app//uxpagetest" url="http://test.uapp.uhisoft.hu/uxpagetest" session_id="" ver="" trace="test-trace">
  <head>
    <base href="https://test.uhisoft.hu/app/"/>
    <redirect pause="1">https://test.uhisoft.hu/app//app/simple/action</redirect>
    <js>jQuery/compat.js</js>
    <js>jQuery/jquery-3.2.1.min.js</js>
    <css>jQueryUI/jquery-ui.css</css>
    <js>jQueryUI/jquery-ui.js</js>
    <css>jQueryUI/jquery-ui.min.css</css>
    <css>jQueryUI/jquery-ui.structure.css</css>
    <css>jQueryUI/jquery-ui.structure.min.css</css>
    <css>jQueryUI/jquery-ui.theme.css</css>
    <css>jQueryUI/jquery-ui.theme.min.css</css>
    <css>Main/button.css</css>
    <css>Main/main.css</css>
    <xsl module="Main" path="../../../modules/Main/main.xsl" order="0">Main/main.xsl</xsl>
    <js>Main/uapp.js</js>
    <appversion/>
    <environment/>
    <name>redirect</name>
    <view path="./simple.xsl">simple</view>
    <design module="simple">basic</design>
    <xsl module="simple" path="../testmodules/simple/design/basic.xsl" order="10">simple/design/basic.xsl</xsl>
    <title/>
  </head>
  <content/>
  <control>
    <params/>
    <debug>test-trace</debug>
    <agent xml="Y" name="Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0"/>
    <script>https://test.uhisoft.hu/app//simple</script>
    <path></path>
    <render>client</render>
    <xsl>__redirect_simple</xsl>
    <pageview>redirect_simple</pageview>
  </control>
</data>
');
        $this->assertEquals($expected, $result, Util::diff($expected, $result));
    }

}
