<?xml version="1.0" encoding="UTF-8"?>
<!-- design.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="xml" version="1.0" indent="yes"
	            doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	            doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	            omit-xml-declaration="yes"
	/>

	<xsl:template match="head" mode="user-head">
	</xsl:template>

	<xsl:template match="data">
		<div class="simple-content">
			<h1>
				<xsl:value-of select="head/h1"/>
			</h1>
			<xsl:apply-templates select="head/menu" mode="normal"/>
			<xsl:apply-templates select="control/msg"/>
			<xsl:apply-templates select="content"/>
		</div>
	</xsl:template>

</xsl:stylesheet>
