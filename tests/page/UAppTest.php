<?php

use Codeception\Test\Unit;

class UAppTest extends Unit {

    /**
     * @dataProvider provider_df
     *
     * @param $d
     * @param $locale
     * @param $expected
     *
     * @throws ConfigurationException
     * @throws InternalException
     */
    function test_df($d, $locale, $expected) {
        $date = new DateTime($d);
        $r = UApp::df($date, IntlDateFormatter::SHORT, IntlDateFormatter::SHORT, $locale);
        $this->AssertSame($expected, $r);
    }

    function provider_df() {
        return [
            ['2018-01-01 7:59', 'hu', '2018.01.01. 7:59'],
            ['2018-01-01 7:59', 'en', '1/1/18, 7:59 AM'],
            ['2018-01-01 7:59', 'xx', '2018. 01. 01. 7:59'],
            ['2018-01-01 7:59', null, '01/01/2018, 07:59'],
        ];
    }
}
