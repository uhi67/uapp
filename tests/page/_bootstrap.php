<?php
include_once dirname(dirname(__DIR__)) . '/lib/testhelper/TestDebug.php';

use TestDebug as Debug;

Debug::trace('TestDebug#1');
include_once dirname(dirname(__DIR__)) . '/lib/UAppLoader.php';
UAppLoader::registerAutoload();
$_SESSION = [];
Debug::trace('TestDebug#2');
