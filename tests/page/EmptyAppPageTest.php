<?php

namespace page;

use Codeception\Test\Unit;
use ConfigurationException;
use DOMException;
use EmptyAppPage;
use Exception;
use InternalException;
use ModelException;
use ReflectionException;
use UApp;
use UAppException;
use UAppLoader;
use UAppPage;

require_once dirname(__DIR__) . '/_support/Helper/UnitTest.php';
include_once dirname(dirname(__DIR__)) . '/lib/UApp.php';
include_once dirname(dirname(__DIR__)) . '/lib/UAppLoader.php';
UAppLoader::registerAutoload();

class EmptyAppPageTest extends Unit {
    /** @var UApp $app */
    private $app;
    /** @var UAppPage $page */
    private $page;

    /**
     * Runs before every test
     *
     * @throws ConfigurationException
     * @throws InternalException
     * @throws Exception|ReflectionException
     */
    protected function _before() {
        $_SERVER['REQUEST_URI'] = 'http://uapp.test';
        $config = [
            'uapp' => [
                'path' => dirname(dirname(__DIR__)),
            ],
            'baseurl' => 'http://uapp.test',
            'database' => require(dirname(__DIR__) . '/dbconfig.php'),
        ];
        $this->app = UApp::getInstance($config);
        $this->page = new EmptyAppPage($this->app);
    }

    protected function _after() {
        $this->page = null;
        $this->app = null;
    }

    /**
     * @dataProvider provider_createUrl
     *
     * @param array $data
     * @param string $expected
     *
     * @throws ConfigurationException
     * @throws InternalException
     */
    function test_createUrl($data, $expected) {
        $r = $this->page->createUrl($data);
        $this->AssertSame($expected, $r);
    }

    function provider_createUrl() {
        return [
            [['domain', 8, 'focus' => 1277, '#' => 'focus'], 'http://uapp.test/domain/8?focus=1277#focus'],
        ];
    }

    /**
     * @dataProvider provider_sendMessage
     *
     * @param $msg
     * @param $class
     * @param $unique
     * @param $content
     * @param $expected
     * @param $control
     * @throws ConfigurationException
     * @throws InternalException
     * @throws ReflectionException
     * @throws DOMException
     * @throws ModelException
     * @throws UAppException
     */
    function test_sendMessage($msg, $class, $unique, $content, $expected, $control) {
        unset($_SESSION['sendmessage']);
        $this->page->sendMessage($msg, $class, $unique, $content);
        $msgs = $_SESSION['sendmessage'];
        $this->AssertSame($expected, $msgs);

        $this->page->getMessage();

        $result = '';
        $nodelist = $this->page->node_control->selectNodes('msg');
        foreach ($nodelist as $node) {
            $result .= $this->page->xmldoc->saveXML($node);
        }
        $this->assertXmlStringEqualsXmlString($control, $result);
    }

    function provider_sendMessage() {
        return [ //$msg, $class, $unique, $content, 	$expected, 	$control
            [
                'Sima', 'ok', true, null,
                [['Sima', 'ok', null]],
                '<msg class="flash ok"><title>Sima</title></msg>',
            ],
            [
                '`Ez [markdown](http://uapp.test) link.', 'ok', true, null,
                [['`Ez [markdown](http://uapp.test) link.', 'ok', null]],
                #'<msg class="flash ok"><title>Ez <a href="http://uapp.test">markdown</a> link.</title></msg>', # showdown kliens oldalon
                '<msg class="flash ok"><title>`Ez [markdown](http://uapp.test) link.</title></msg>',
            ],
        ];
    }
}
