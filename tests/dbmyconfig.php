<?php
/**
 * Returns database config for testing
 *
 * First time create test database manually using tests/_data/create_tst_db.sql
 * (Use db_update_... files if exist)
 */
return [
    'DBXMY',
    'name' => 'uapp_test',
    'user' => 'uapp',
    'password' => 'kalap123456',
    'host' => 'db_test_uapp_my',
    'port' => null,
    'encoding' => 'UTF8',
];
