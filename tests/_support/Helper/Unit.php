<?php

namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I
// *** Run `codecept build` after modifiing this file! ***

use Codeception\Module;

class Unit extends Module {
    /**
     * Evaluates arrays or items runtime.
     *
     * Use this function in testers on with data providers providing post-evaluatable data,
     * because dataprovider has no valid scope to evaluate.
     *
     * This method can evaluate:
     * - callables (an array with callable reference on 0-index, and arguments on the rest)
     * - evaluatable strings (beginning with '#eval:')
     * This method evaluates arrays recursively: on any level you may define callables or evaluatables, including arguments of callables.
     *
     * Using in tester method:
     * ```
     * // arg1 is an argument of tester method got from dataProvider.
     * $arg1 = $this->tester->evaluateItems($arg1, $this);
     * ```
     * Data in dataprovider (one data item for each example):
     * ```
     * return array(
     *    array(array(array('classname', 'method1'), arguments-for-method1)),    // example1: callable
     *  array('#eval:return $scope->testerProperty;'),                        // example2: evaluatable
     * );
     * ```
     *
     * @param array|string $values -- value(s) to evaluate. Any other datatype will be returned identically.
     * @param Object $scope -- the object scope for evaluation, e.g. the tester itself. May be referenced in evaluatables.
     * @param integer $recursive -- depth level of recursion. Zero means no recursion, negative number will result infinite recursion.
     * @return mixed -- the evaluated result.
     */
    function evaluateItems($values, $scope, $recursive = -1) {
        if (is_array($values) && isset($values[0]) && is_callable($values[0])) {
            $callable = array_shift($values);
            if ($recursive) $values = $this->evaluateItems($values, $scope, $recursive - 1);
            return call_user_func_array($callable, $values);
        }
        if (is_array($values) && $recursive) {
            foreach ($values as &$value) $value = $this->evaluateItems($value, $scope, $recursive - 1);
            return $values;
        }
        if (is_string($values) && substr($values, 0, 6) == '#eval:') {
            $result = eval($expr = substr($values, 6));
            return $result;
        }
        return $values;
    }
}
