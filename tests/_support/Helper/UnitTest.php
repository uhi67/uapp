<?php /** @noinspection PhpHierarchyChecksInspection */

use Codeception\Test\Unit;

class UnitTest extends Unit {
    /**
     * Compares two strings showing fine granulated difference in message.
     *
     * @param string $expected
     * @param string $actual
     * @param string $message -- additional message
     */
    public function assertEqualsFine($expected, $actual, $message = '') {
        $diff = Util::diff($expected, $actual, true);
        $this->assertEquals($expected, $actual, "Diff:\n" . $diff . ($message ? "\nMessage:\n$message" : ''));
    }

    /**
     * Compares two XML strings or DOMElements in common standard form.
     *
     * @param string|DOMElement $expected
     * @param string|DOMElement $actual
     * @param string $message
     *
     * @return void
     * @throws DOMException
     * @throws Exception
     */
    public function assertXmlEquals($expected, $actual, $message = '') {
        if ($expected instanceof DOMElement) $expected = $expected->ownerDocument->saveXML($expected);
        if ($actual instanceof DOMElement) $actual = $actual->ownerDocument->saveXML($actual);
        if (!is_string($expected)) throw new Exception('Expected must be a string');
        if (!is_string($actual)) {
            $this->assertIsString($expected);
            return;
        }

        $actual = UXMLDoc::formatFragment($actual); // TODO: document fragments are formatted badly
        $this->assertEqualsFine(UXMLDoc::formatFragment($expected), $actual, $message);
    }
}
