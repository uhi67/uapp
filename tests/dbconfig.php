<?php
/**
 * Returns database config for testing
 *
 * First time create test database manually using tests/_data/create_tst_db.sql
 * (Use db_update_... files if exist)
 */
return [
    'DBXPG',
    'name' => 'uapp_test',
    'port' => 5432,
    'user' => 'uapp',
    'password' => 'kalap123',
    'host' => 'db_test_uapp',
    'encoding' => 'UTF-8',
];
