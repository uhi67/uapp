<?php
/**
 * Returns an array of certified user agent XSLT capabilities
 * Items are rules as array(pattern, min, max, result)
 * - `pattern` is a preg RegEx witjh an optional submatch
 * - `min` is minimum value of submatch (if exists). Null will match always
 * - `max` is maximum value of submatch (if exists). Null will match always
 * - The first match will return it's `result`.
 */
return [
    // Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36
    ['/Chrome\/(\d+)\./', 68, null, false],    // jqueryui autocomplete nem működik
    ['/Chrome\/(\d+)\./', 45, null, true],
    // Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0
    ['/Firefox\/(\d+)\./', 122, null, false],
    ['/Firefox\/(\d+)\./', 40, null, true],

    ['/MSIE 6\.0/', null, null, false],

    // MS-Edge:
    // Mozilla/5.0 (Windows NT 10.0; <64-bit tags>) AppleWebKit/<WebKit Rev> (KHTML, like Gecko) Chrome/<Chrome Rev> Safari/<WebKit Rev> Edge/<EdgeHTML Rev>.<Windows Build>
    // Mozilla/5.0 (WM 10.0; Android <Android Version>; <Device Manufacturer>; <Device Model>) AppleWebKit/<WebKit Rev> (KHTML, like Gecko) Chrome/<Chrome Rev> Mobile Safari/<WebKit Rev> Edge/<EdgeHTML Rev>.<Windows Build>
    ['/Edge\/(\d+)\.0/', 11, null, true],

    // MSIE 11:
    // Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko
    // Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko
    // Mozilla/5.0 (Mobile; Windows Phone 8.1; Android 4.0; ARM; Trident/7.0; Touch; rv:11.0; IEMobile/11.0; NOKIA; Lumia 520) like iPhone OS 7_0_3 Mac OS X AppleWebKit/537 (KHTML, like Gecko) Mobile Safari/537
    // Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0; Xbox; Xbox One)
    ['/Trident\/7.0; rv:(\d+).|MSIE 10.|IEMobile\/(\d+)./', 11, null, true],

    ['/MSIE (\d+)\.0/', 9, null, true],

    ['/Safari\/4/', null, null, false],        // Nokia gyári mobil böngésző
    ['/NetFront\/3/', null, null, false],        // mobil böngésző
    ['/Chrome\/6/', null, null, false],        // tiny_mce nem megy
    ['/Safari\/5/', null, null, false],        // tiny_mce nem megy
    // Safari >= 7
];

