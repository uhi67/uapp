<?xml version="1.0" encoding="UTF-8"?>
<stylesheet version="1.0" xmlns="http://www.w3.org/1999/XSL/Transform">
	<output method="html" version="1.0" indent="yes"
	        doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	        doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	        omit-xml-declaration="yes"
	/>
	<!-- The purpose of this file only to connect module xsls together to make language validator of the IDE work correctly -->
	<!--Modules-->
	<include href="../../modules/Main/main.xsl"/>
	<include href="../../../uapp/modules/Menu/menu.xsl"/>
	<include href="../../../uapp/modules/UList/UList.xsl"/>
	<include href="../../../uapp/modules/SelectFrom/SelectFrom.xsl"/>
	<include href="../../../uapp/modules/TabControl/TabControl.xsl"/>
	<include href="../../../uapp/modules/Form/form.xsl"/>
</stylesheet>
