<?php /** @noinspection PhpUnused */

class EnvHelper {
    /** @var array $environment */
    static $environment;
    /** @var string $envFile */
    static $envFile;

    /**
     * Returns an environment variable or default or Exception
     * For array `$default` retrieves multiple variables, each for all keys in `$default` array.
     * Specify default=false for no default. Null will result default null.
     *
     * @param string $var
     * @param string|array|null|false $default
     *
     * @return string|array
     * @throws Exception
     */
    static function getEnv($var, $default = false) {
        if (is_array($default)) {
            array_walk($default, function (&$value, $key) use ($var) {
                if (!is_integer($key)) $value = static::getEnv($var . '_' . $key, $value);
            });
            return $default;
        }
        $var = strtoupper(static::underscore($var));

        if ($var !== 'ENV_FILE') {
            if (!static::$envFile) static::$envFile = dirname(__DIR__) . '/' . static::getEnv('envFile', 'config/env.php');
            if (!static::$environment && file_exists(static::$envFile)) {
                static::$environment = require static::$envFile;
            }
            if (static::$environment && array_key_exists($var, static::$environment)) return static::$environment[$var];
        }

        $env = 'APP_' . $var;
        $value = getenv($env);
        if ($value !== false) return $value;

        if ($default === false) {
            throw new Exception("Configuration error: Environment variable '$env' is missing");
        }
        return $default;
    }

    /**
     * Reads file containing environment variables (for development environment, command line)
     * Specified file must exist and contain lines like this:
     *
     * `SetEnv APPLICATION_ENV "local"`
     *
     * @param string $file -- configuration file
     * @param string $var
     *
     * @return null
     */
    static function getFileEnv($file, $var) {
        if (!$f = fopen($file, 'r')) return null;
        while ($l = fgets($f)) {
            $d = preg_split('~\s+~', trim($l), 3);
            if (count($d) < 3) continue;
            if (strtolower($d[0]) == 'setenv' && $d[1] == $var) return preg_replace('~^"|"$~', '', $d[2]);
        }
        return null;
    }

    /**
     * @param mixed $date
     *
     * @return string
     * @throws Exception
     */
    public static function formatDate($date) {
        return (new DateTime($date))->format('Y-m-d');
    }

    /**
     * Converts any "CamelCased" into an "underscored_word".
     *
     * @param string $words the word(s) to underscore
     *
     * @return string
     */
    public static function underscore($words) {
        return mb_strtolower(preg_replace('/(?<=\\pL)(\\p{Lu})/u', '_\\1', $words), 'UTF-8');
    }

    /**
     * @param &$protocol -- returns current protocol prefix: http or https
     *
     * @return string|false -- detected current base url of the application or false on error
     * @throws Exception
     */
    public static function getBaseUrl(&$protocol = null) {
        if (!isset($_SERVER["HTTP_HOST"])) return false;
        $https = isset($_SERVER['HTTPS']) ? $_SERVER['HTTPS'] : EnvHelper::getEnv('https', '');
        $protocol = $https == 'on' ? 'https' : 'http';
        if (isset($_SERVER['HTTP_X_FORWARDED_PROTO'])) $protocol = $_SERVER['HTTP_X_FORWARDED_PROTO'];
        return $protocol . '://' . $_SERVER["HTTP_HOST"];
    }
}
