<?php /** @noinspection PhpUnhandledExceptionInspection */
/**
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * Alkalmazás konfigurációs mintája
 * Másold le config.php néven, és töltsd ki a környezetre jellemző adatokkal!
 *
 * Application's 'global configuration file template: copy to config.php and fill the real data
 */

require_once dirname(__DIR__, 2) . '/inc/EnvHelper.php';

$baseurlpath = EnvHelper::getBaseUrl();    # detected baseurl with protocol
$rootpath = dirname(__DIR__);
$datapath = $rootpath . '/data';
$defpath = $rootpath . '/def';
$prefix = EnvHelper::getEnv('prefix', '');                        //*** Application path prefix after domain name
$baseurl = EnvHelper::getEnv('baseurl', $baseurlpath . $prefix);
$domain = EnvHelper::getEnv('domain', $_SERVER["HTTP_HOST"]);
/* Directory for runtime data. Include in .gitignore */
$datapath = $rootpath . '/data';
/* Directory for application data. */
$defpath = $rootpath . '/def';
$baseurl = 'http://uapptemplate.test';    //*** use http://uapptemplate.test/index.php if mod_rewrite is not available

/**
 * @var array $config
 * Application's 'global configuration array
 */
$config = [
    /** Application basic setup parameters */
    'baseurl' => $baseurl,
    'apppath' => $rootpath,
    'incpath' => $rootpath . '/inc',
    'pages' => $rootpath . '/pages',
    'basepath' => $rootpath . '/www',
    'modulepath' => $rootpath . '/modules',
    'configpath' => $rootpath . '/config',
    'xslpath' => $rootpath . '/xsl',
    'xslurl' => $baseurl . '/xsl',
    'datapath' => $datapath,                // Runtime data, logs, ignore from git
    'defpath' => $defpath,                    // xml definitions, syntaxes, other data included in git
    'logfile' => $datapath . '/app.log',
    'errorlogfile' => $datapath . '/error.log',
    'appname' => 'apptemplate',
    'title' => 'UApp Application Template',
    'default' => 'start',            // Name of application's default page
    'render' => 'server',            // default render mode: server (default), client
    'design' => 'simpledesign',        // Module name contains design theme (may be in application or in UApp library)

    'environment' => isset($_SERVER['APPLICATION_ENV']) ? $_SERVER['APPLICATION_ENV'] : 'production', // must be 'development' or 'production'
    #'message' => 'Warning! Test version!', // Message to display on every page

    /** UApp framework's properties */
    'uapp' => [
        'path' => dirname($rootpath) . '/UApp',
    ],

    // Localization
    'l10n' => [
        'class' => 'L10nBase',    // Localization classname. Must provide getText(category, text, params, language) and formatDate(DateTime, datetype, timetype, language) for UApp::la/UApp:fd
        'language' => 'hu',        // Default language with optional locale, may be changed by UApp::setLang(lang/locale)
        'source' => 'en',        // Default source language, default is 'en' (not used in L10nBase)
    ],

    /** Session handling */
    'session' => [
        'name' => 'sess_idp',
        'save_path' => $datapath . '/session',
        'lifetime' => 1800,
        'cookie_path' => '',
        'cookie_domain' => 'apptempate.dev',
        'logfile' => $datapath . '/session.log',
    ],

    /** UApp built-in debug configuration */
    'debug' => [
        'path' => $datapath . '/debug',        // writeable path to store trace files
    ],

    /** Cache configuration for speeding up directory queries */
    'cache' => [
        'path' => $datapath . '/cache',        // Writeable path to store cache files
        'enabled' => 1,                        // Cache is enabled
        'ttl' => 1800                        // Default time-to-live in seconds
    ],

    /** Default database connection configuration */
    'database' => [
        'name' => 'apptemplate',
        'port' => 5432,
        'user' => '',
        'password' => '',
        'host' => '',
        'encoding' => 'UTF-8',
    ],

    /** Configuration for updatedb command function and owner database access */
    'updatedb' => [
        'database' => [
            'name' => 'apptempate',
            'port' => 5432,
            'user' => '',
            'password' => '',
            'host' => '',
            'encoding' => 'UTF-8',
        ],
    ],

    /** Configuration for pageable lists of {@see UList} module */
    'list' => [
        'default_page_length' => 50,
    ],

    /** Configuration for SAML login module */
    'saml' => [
        'authsource' => 'default-sp',                            // id from simplesamlphp/config/authsources.php; Default is 'default-sp'
        'dir' => '/usr/share/simplesamlphp/',                    // simplesamlphp base dir (local: dirname($rootpath).'/simplesamlphp/')
        'idp' => 'https://idp.myinstitution.hu/saml2/idp/metadata.php',    // Default idp for new users
        'org' => 'My Institution',                                // Default org name for new users
        'uidfield' => 'eduPersonPrincipalName',                    // source of unique uid attribute
    ],

    // Config for mailer function (UApp)
    'mailer' => [
        'class' => 'FakeMailer', // 'class' => 'Mailer' #is the default if mailer section is omitted.
        'dir' => $datapath . '/FakeMail',
    ],
];

return $config;
