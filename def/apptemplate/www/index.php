<?php
/**
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * This is the main router/dispatcher of UApp framework.
 * It also responsible for including libraries and serving assets.
 * In the framework, this file can be found at /lib/_index.php
 * Please copy this file to the into application's web-root directory as 'index.php'
 * Redirect to this file in .htaccess if file not found
 * You may also copy this file into the root dir of the application with name 'app', and use it for invoke app's cli commands: app <command-name>
 *
 * This file must not be in .gitignore
 */

$configFile = dirname(__DIR__) . '/config/config.php';
if (!file_exists($configFile)) {
    echo "Internal error: Main configuration file is missing.";
    exit;
}
/** @noinspection PhpIncludeInspection */
$config = require $configFile;    // path to config
/** @noinspection PhpIncludeInspection */
require $config['uapp']['path'] . '/lib/_uapp.php';
