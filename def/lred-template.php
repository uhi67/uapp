<?php /** @noinspection ALL */

/**
 * Controller template for LRED actions
 * (List, Read, Edit=create+update, Delete)
 *
 * Placeholders:
 * - Replace 'YourController' to your pagename
 * - Replace 'YourModel' and '$yourmodel' to your model name
 * - 'id', 'name', 'descr' and 'parent' attributes of model are assumed for example
 * - a bitmapped permission system is assumed, using YOURROLE constant, and R_ADD, R_READ, R_MODIFY, R_DELETE bitmask-constants
 * - generalized translated messages are used, replace them to fit characteristics of your resource
 *
 * @author Peter Uherkovich
 * @copyright 2018
 */
class YourControllerPage extends AppPage {

    /**
     * Default action usually lists the available resources
     *
     * @return void
     */
    function actDefault() {
        /** @var array $fields -- field names to show in the columns */
        $fields = array_keys(YourModel::attributeLabels());
        $itemname = strtolower(YourModel::className());

        $list = new UList($this, [
            'id' => $itemname . '_list_1',    // You shold make it unique if this list is a sublist of another object
            'name' => $itemname . '_list',
            'item_name' => $itemname,
            'orders' => UList::descOrders($orders),
            'columns' => array_map(function ($index, $name, $label) {
                return [
                    'name' => $label,
                    'title' => YourModel::getAttributeHint($name, 'hint'),
                    'ord1' => $index * 2,
                    'ord2' => $index * 2 + 1
                ];
            }, array_keys($fields), $fields, array_value(YourModel::attributeLabels())),
            'attributes' => [
                'width' => '100%',
                'with-navigator' => 1,
                'with-search' => 1,
            ]
        ]);

        $pattern = $list->setSearch();

        /** @var array $condition -- a Query-condition array to filter records */
        $condition = [
        ];
        if ($pattern) {
            $condition[] = ['RLIKE', ['text()', $list->fieldName()], '$pattern'];
        }
        $query1 = Query::createSelect([
            'from' => 'YourModel',
            'fields' => [['count()', 'id']],
            'condition' => $condition,
            'params' => $params,
        ]);
        $list->count = $query1->scalar();
        $node_list = $list->createNode($node_parent);
        $query = Query::createSelect([
            'from' => 'YourModel',
            'fields' => $fields,
            'condition' => $condition,
            'orders' => $list->order(),
            'offset' => $list->offset(),
            'limit' => $list->limit(),
        ]);
        // Creates data elements in the XML output
        $r = $this->xmldoc->query($this->db, $node_list, $itemname, $query->sql, $params);
    }

    /**
     * Reading a resource
     *
     * Request parameters:
     * - int id -- the id of the resource
     *
     * @return void
     */
    function actRead() {
        $this->back_url = $this->script;
        // Permission checking (in a bitmapped permission model)
        $this->right = $this->user->rightValue(YOURROLE, $this->id);
        if (!$this->right & R_READ) throw new ForbiddenException(App::la('app', 'You have no permission to access this resource'), $this->id, $this->back_url);

        $yourmodel = YourModel::first($this->id);
        if (!$yourmodel) throw new NotFoundException(App::la('app', 'Resource not found'), $this->id, $this->back_url);
        $yourmodel->createNode($this, [
            // XML node options
        ]);

        // Men�
        $itemmenu = new Menu($this, ['nodename' => 'titlemenu']);
        $itemmenu->addItems([
            [
                'enabled' => true,
                'title' => UApp::la('app', 'List'),
                'url' => $this->script,
                'icon' => 'fa-list',
                'hint' => UApp::la('app', 'Back to the list of resources'),
            ],
            [
                'enabled' => $this->right & R_MODIFY,
                'title' => UApp::la('app', 'Edit'),
                'url' => $this->script . '/edit/' . $this->id,
                'icon' => 'fa-pencil-square-o',
                'hint' => UApp::la('app', 'Edit and update this resource'),
            ],
            [
                'enabled' => $this->right & R_DELETE,
                'title' => UApp::la('app', 'Delete'),
                'url' => $this->script . '/delete/' . $this->id,
                'icon' => 'fa-trash-o',
                'hint' => UApp::la('app', 'Delete this resource'),
                'confirm' => UApp::la('app', 'Are you sure to delete this resource permanently?'),
            ],
        ]);

        // The rest is in the view
    }

    /**
     * Create & Update
     *
     * Editing an existing record or creating a new resource.
     *
     * Request parameters:
     * - int id -- the id of the resource if existing or 0 if new
     * - int parent -- the id of the parent resource if new or 0 if root.
     *
     * @return void
     */
    function actEdit() {
        $this->back_url = $this->script;
        // Loading model and Permission checking (in a bitmapped permission model)
        if ($this->id) {
            $this->right = $this->user->rightValue(YOURROLE, $this->id);
            if (!$this->right & R_MODIFY) throw new ForbiddenException(App::la('app', 'You have no permission to modify this resource'), $this->id, $this->back_url);
            $yourmodel = YourModel::first($this->id);
            if (!$yourmodel) throw new NotFoundException(App::la('app', 'Resource not found'), $this->id, $this->back_url);
            $this->back_url = $this->createUrl('read', $this->id);
            $this->setH2(App::la('app', 'Editing {$name}', ['name' => $yourmodel->name]));
        } else {
            $parent = Util::getReqInt('parent');
            $this->back_url = $this->createUrl(['read', $parent]);
            $this->right = $this->user->rightValue(YOURROLE, $parent);
            if (!$this->right & R_ADD) throw new ForbiddenException(App::la('app', 'You have no permission to create new resource here'), $this->parent, $this->back_url);

            $newtitle = App::la('app', 'New YourModel');
            $yourModel = new YourModel([
                'parent' => $parent,
                'name' => Util::getReq('name', $newtitle),
                'creator' => $this->user->id,
            ]);
            $this->setH2($newtitle);
        }

        /** @var Menu $itemmenu -- men� associated to the resource item, appears in the title row of the item */
        $itemmenu = new Menu($this, ['nodename' => 'titlemenu']);
        $itemmenu->addItems([
            [
                'enabled' => true,
                'title' => App::la('app', 'List'),
                'url' => $this->script,
                'icon' => 'fa-list',
                'hint' => App::la('app', 'Back to the list of resources'),
            ],
            [
                'enabled' => $this->right & R_DELETE,
                'title' => App::la('app', 'Delete'),
                'url' => $this->script . '/delete/' . $this->id,
                'icon' => 'fa-trash-o',
                'hint' => App::la('app', 'Deletes this resource'),
                'confirm' => App::la('app', 'Are you sure to delete this resource permanently?'),
            ],
        ]);
        $itemmenu->createNode($this->node_head);

        $form = new Form($this, [
            'name' => 'yourmodeledit',
            'model' => $yourmodel,        // You may have to use a derived model if the rules of the form validation differ from the base model.
            'class' => 'full',
        ]);

        // Loads posted form data into the model, performs update and redirects to READ action
        if ($form->export($yourmodel)) {
            if (!$yourmodel->save()) {
                $this->internalError(App::la('app', 'Updating yourmodel failed'));
            }
            $this->log($yourmodel->toArray());
            $this->redirect($this->back_url);
        }

        // Generating form
        /** @var array $formoptions -- Options for all attributes of your model if default is not appropriate */
        $formoptions = [
            'id' => ['type' => 'view'],
            'descr' => ['type' => 'textarea', 'width' => 480, 'height' => 120],
            'parent' => ['cancel' => App::la('app', 'Standalone'), 'refmodel' => 'YourModel'], // You should have a selector widget for this class defined in app.js
        ];
        /** @var array $modeloptions -- Options for model record generating in the form */
        $modeloptions = [
            'associations' => [
                'parent1' => ['name'],
            ],
            'subnodeFields' => ['descr'],
        ];
        $node_form = $form->createNode($this, $formoptions, true, $modeloptions);
        $node_form->setAttribute('edit', 1);

        /** @var Menu $contextmenu -- row of buttons appears on the bottom of the form */
        $contextmenu = new Menu($this);
        $contextmenu->addItems([
            ['enabled' => true, 'caption' => App::la('app', 'OK'), 'click' => $form->submit],
            ['enabled' => true, 'caption' => App::la('app', 'Cancel'), 'url' => $this->back_url],
        ]);
        $contextmenu->createNode($node_form);
    }

    function actDelete() {
        $this->back_url = $this->script;
        $this->right = $this->user->rightValue(YOURROLE, $this->id);
        if (!$this->right & R_DELETE) throw new ForbiddenException(App::la('app', 'You have no permission to delete this resource'), $this->id, $this->back_url);
        $yourmodel = $this->id ? YourModel::first($this->id) : null;
        if (!$yourmodel) throw new NotFoundException(App::la('app', 'Resource not found'), $this->id, $this->back_url);
        $this->back_url = $this->createUrl('read', $this->id);

        if ($yourmodel->delete()) {
            $this->back_url = $this->script;
            Util::sendMessage(App::la('Resource {$name} has been deleted successfully', ['name' => $yourmodel->name]), 'ok');
            $this->redirect($this->script);
        } else {
            throw new ServerErrorException(App::la('app', 'Failed deleting resource {$name}', ['name' => $yourmodel->name]), $this->id, $this->back_url);
        }
        $this->redirect($this->back_url);
    }

    function preConstruct() {
        // Implement preConstruct() method.
    }

    function loadData() {
        // Implement loadData() method.
    }
}
