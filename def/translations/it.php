<?php
return [
    'false' => 'falso',
    'true' => 'vero',
    'not set' => 'non impostato',
    'Unknown action `{$act}`' => 'azione "{$act}" non valida',
    'Configuration variable `{$var}` is missing' => 'La variabile di configurazione `{$ var}` è mancante',
    'Database connection error' => 'Errore di connessione al database',
    'Database connect failed' => 'Connessione al database fallita',

    // Exception details
    'Internal error' => 'Errore interno',
    'Too bad! The page cannot be loaded.' => 'Peccato! La pagina non può essere caricata.',
    'You have no permission for this action.' => 'Non hai il permesso per questa azione.',
    'Invalid parameters' => 'Parametri non validi',
    'The requested page or resource is not found.' => 'La pagina o la risorsa richiesta non è stata trovata.',
    'Application configuration error' => 'AErrore di configurazione dell\'applicazione',

    // SamlUser
    'Missing `eduPersonPrincipalName` or `eduPersonTargetedID` attribute.' => 'Attributo `eduPersonPrincipalName` o` eduPersonTargetedID` mancante.',

    // Model and validation errors
    'Field `{$field}`: pattern is not a string or array' => 'Il campo `{$field}` ha uno schema non valido',
    'with {$fields} {$message}' => 'e campo `{$fields}` {$message}',
    'Field `{$fieldname}` {$message} ({$value})' => 'Il campo `{$fieldname}` {$message} ({$value})',
    'Field `{$fieldname}` {$message}' => 'Il campo `{$fieldname}` {$message}',
    'has invalid format' => 'ha un formato non valido',
    'must be unique' => 'deve essere unico',
    'is too long or too short' => 'è troppo lungo o troppo corto',
    'is mandatory' => 'è obbligatorio',
    'is invalid integer' => 'non è valido intero',
    'is invalid DateTime' => 'non è valido per data e ora',
    'is invalid boolean' => 'non è valido come booleano',
    'is invalid url address' => 'è un indirizzo URL non valido',
    'is invalid e-mail address' => 'è un indirizzo e-mail non valido',
    'must be between {$min} and {$max}' => 'deve essere compreso tra {$ min} e {$ max}',
    'is invalid IP address' => 'è un indirizzo IP non valido',
    'must be empty' => 'deve essere vuoto',
    '{$err} if `{$fieldname2}` is {$refvalue}' => '{$err}, se `{$fieldname2}` ha valore {$refvalue}',
    '{$err} if `{$fieldname2}`' => '{$err}, se `{$fieldname2}`',
    '{$err} if not `{$fieldname2}`' => '{$err}, se non `{$fieldname2}`',
    '$1 is too short' => '$1 è troppo corto',
];
