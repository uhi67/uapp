<?xml version="1.0" encoding="UTF-8"?>
<!-- message.xsl -->
<!-- Az egyszerű rendszerüzenet (pl. hibaüzenet) oldala -->
<!-- Szerkezetileg a page-xsl-eknek felel meg -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="xml" version="1.0" indent="yes"
	            doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	            doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	            omit-xml-declaration="yes"
	/>

	<xsl:template match="content">
		<!-- Az üzenetek a design.xsl-ben már megjönnek -->
		<div class="content-block">
			<xsl:apply-templates select="/data/control/info"/>
		</div>
		<xsl:apply-templates select="/data/control/url"/>
	</xsl:template>

	<xsl:template match="h3">
		<h3>
			<xsl:value-of select="text()" disable-output-escaping="yes"/>
		</h3>
	</xsl:template>

	<xsl:template match="info">
		<p>
			<xsl:value-of select="text()" disable-output-escaping="yes"/>
		</p>
	</xsl:template>

	<xsl:template match="url">
		<p>
			<a class="button" href="{.}">Tovább</a>
		</p>
	</xsl:template>

</xsl:stylesheet>
