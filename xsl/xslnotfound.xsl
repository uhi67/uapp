﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:include href="Main/main.xsl"/>

	<xsl:template match="content">
		<div id="popup-content">
			<h2>Hiba</h2>
			<p class="error">Az xsl fájl nem található.</p>
			<p>(<xsl:value-of select="/data/@xsl"/>)
			</p>
			<p>A keresett funkció még valószínűleg nem készült el, türelmedet kérem.</p>
		</div>
	</xsl:template>

</xsl:stylesheet>
