<?xml version="1.0" encoding="UTF-8"?>
<!-- select2.xsl -->

<!--suppress XmlDefaultAttributeValue -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="xml" version="1.0" indent="yes"
	            doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	            doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	            omit-xml-declaration="yes"
	/>

	<!-- Declaration of module calss and callbacks-->
	<xsl:template match="module-form-rule" mode="classname"/>
	<xsl:template match="module-form-attribute" mode="comment"/>
	<xsl:template match="module-form-attribute" mode="help"/>
	<xsl:template match="module-form-rule" mode="data"/>

	<xsl:template match="xsl-call[@name='select2']">
		<!-- Callback to place static fragments onto the page -->
	</xsl:template>

	<xsl:template match="attribute[@type='select2']" mode="field" name="select2-attribute-field">
		<xsl:param name="formname" select="../@name"/>
		<xsl:param name="model" select="../*[name()=current()/../@modelnode]"/>
		<xsl:param name="value">
			<xsl:choose>
				<!-- Value given to attribute overrides model -->
				<xsl:when test="@value">
					<xsl:value-of select="@value"/>
				</xsl:when>
				<!-- Model's attribute with this name -->
				<xsl:when test="$model/@*[name()=current()/@name]">
					<xsl:value-of select="$model/@*[name()=current()/@name]"/>
				</xsl:when>
				<!-- Model's subnode with attribute name -->
				<xsl:otherwise>
					<xsl:value-of select="$model/*[name()=current()/@name]"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:param>
		<xsl:comment>select2/select2-attribute-field</xsl:comment>
		<xsl:variable name="namevalue1">
			<xsl:choose>
				<xsl:when test="@namevalue">
					<xsl:value-of select="@namevalue"/>
				</xsl:when>
				<!-- attribute/ref információk felhasználása -->
				<xsl:otherwise>
					<xsl:value-of select="$model/*[name()=current()/@ref]/@*[name()=current()/@refname]"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="namevalue">
			<xsl:value-of select="$namevalue1"/>
			<xsl:if test="$namevalue1=''">
				<xsl:value-of select="@emptyvalue"/>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="error">
			<xsl:if test="error">invalid</xsl:if>
		</xsl:variable>
		<xsl:variable name="rules">
			<xsl:apply-templates select="rule" mode="classname"/>
		</xsl:variable>
		<xsl:variable name="style">
			<xsl:if test="@width">width:<xsl:value-of select="@width"/>
				<xsl:if test="not(contains(@width, '%'))">px</xsl:if>;
			</xsl:if>
			<xsl:if test="@height and ../@edit!=0">height:<xsl:value-of select="@height"/>px;
			</xsl:if>
			<xsl:if test="@height and ../@edit=0">max-height:<xsl:value-of select="@height"/>px;
			</xsl:if>
		</xsl:variable>

		<xsl:variable name="link">
			<xsl:choose>
				<xsl:when test="not(contains(@link, '{$value}'))">
					<xsl:value-of select="@link"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="substring-before(@link, '{$value}')"/>
					<xsl:value-of select="$value"/>
					<xsl:value-of select="substring-after(@link, '{$value}')"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:comment>$value=<xsl:value-of select="$value"/> $link=<xsl:value-of select="$link"/>
		</xsl:comment>

		<xsl:choose>
			<xsl:when test="../@edit=0 and @link">
				<div class="{@class}">
					<a href="{$link}">
						<xsl:value-of select="$namevalue"/>
					</a>
				</div>
			</xsl:when>
			<xsl:when test="../@edit=0">
				<div class="{@class}">
					<xsl:value-of select="$namevalue"/>
				</div>
			</xsl:when>
			<xsl:when test="(@readonly='true' or @readonly=1) and @link">
				<input type="hidden" id="field_{@name}_name" name="{$formname}[{@name}_name]" value="{$namevalue}"/>
				<div class="inline {@class}">
					<a href="{$link}">
						<xsl:value-of select="$namevalue"/>
					</a>
				</div>
				<xsl:apply-templates select="." mode="comment"/>
			</xsl:when>
			<xsl:otherwise>
				<div class="input-group">
					<select id="field_{@name}" name="{$formname}[{@name}]" data-label="{@label}"
					        class="{@class} {$rules} {$error}"
					        data-url="{@url}"
					        data-placeholder="{@placeholder}"
					        data-template-pattern="{@template-pattern}"
					        data-call-params="{@call-params}"
					>
						<xsl:if test="$style!=''">
							<xsl:attribute name="style">
								<xsl:value-of select="$style"/>
							</xsl:attribute>
						</xsl:if>
						<xsl:apply-templates select="rule" mode="data"/>
						<xsl:if test="$value!=''">
							<option selected="selected" value="{$value}">
								<xsl:value-of select="$namevalue"/>
							</option>
						</xsl:if>
					</select>

					<!-- Törlés -->
					<xsl:if test="not(rule[@name='mandatory'])">
						<xsl:variable name="cancel_disabled">
							<xsl:if test="$value=''">disabled</xsl:if>
						</xsl:variable>
						<span id="empty-{@name}" class="button select2-empty {$cancel_disabled}"
						      title="Törlés, üres érték beállítása">
							<i class="fa fa-times" aria-hidden="true"/>
						</span>
					</xsl:if>

					<xsl:if test="@link">
						<a id="link-{@name}" class="button " title="A hivatkozott rekordra ugrik: {$namevalue}"
						   target="_blank" href="{$link}">
							<i class="fa fa-link" aria-hidden="true"/>
						</a>
					</xsl:if>
					<xsl:apply-templates select="." mode="help"/>
				</div>
				<xsl:apply-templates select="." mode="comment"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
