/**
 * select2 initialization for UApp
 * Usage:
 *
 * <select class="select2"
 *    data-url              -- base-url of the ajax call
 *    data-timeout          -- ajax timeout
 *    data-templatePattern  -- RegEx to display the result (uses subpattern[1])
 * >
 *
 * See also {@see https://select2.org/data-sources/ajax}
 * @type {{init: init}}
 */
const ModuleSelect2 = (function () {
	console.log('ModuleSelect2 module');
	const init = function (context) {
		$('select.select2', context).each(function () {
			const $select2 = $(this);
			const url = $select2.data('url');
			const templatePattern = $select2.data('template-pattern');
			const callParams = $select2.data('call-params') || {};
			$select2.select2({
				width: '100%',
				minimumInputLength: 3,
				maximumInputLength: 256,
				dropdownAutoWidth: true,
				dropdownParent: $select2.closest('form'),
				ajax: {
					url: url,
					dataType: 'json',
					delay: 250,
					data: function (params) {
						// console.log(callParams, Object.assign(params, callParams, {page: params.page||1}));
						return Object.assign(params, callParams);
					},
				},
				templateSelection: function (data) {
					const $option = $(data.element);
					const value = $option.text();
					$select2.trigger('templateSelection', [value]);
					let displayValue = value;
					const pattern = new RegExp(templatePattern);
					if (value && templatePattern) displayValue = value.match(pattern)[1];
					return displayValue;
				},
			})
		});
		$('.select2-empty').on('click', function () {
			console.log($('select.select2', $(this).closest('.form-group')));
			$('select.select2', $(this).closest('.input-group')).val(null).trigger('change');
		});
	};
	return {
		init: init
	}
})();

$(function () {
	ModuleSelect2.init();
	UApp.registerModule('select2', ModuleSelect2);
});
