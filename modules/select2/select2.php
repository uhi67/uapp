<?php
/**
 * Select2 module for UApp
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright 2019
 * @license MIT
 * @package modules
 * @see https://github.com/select2/select2
 */

/** @noinspection PhpUnused */
/** @noinspection PhpClassNamingConventionInspection */

/**
 * select2 module
 *
 * Using:
 *
 * 1. Prepare selectfrom-page to support json response
 * 2. Use javascript $(input).select2 method
 *
 * Example:
 *
 * in javascript:
 *
 * ```
 *    $('select#tr_new_id').select2({
 *      tags: false,
 *      minimumInputLength: 3,
 *      maximumInputLength: 256,
 *      ajax: {
 *          url: 'select_modo/s2',
 *          dataType: 'json',
 *          delay: 250,
 *          data: function(params) {
 *              params.lax = 1 - $(this).data('lang');
 *              params.modo = $(this).data('modo');
 *              return params;
 *          }
 *      }
 *  }).on('select2:select', function(e) {
 *      let data = e.params.data;
 *      let modo_id = $(this).data('modo');
 *        console.log(data);
 *        let container = $('#tr_con');
 *        container.load('modo/new_tr #translations', {
 *            id: modo_id,
 *            nid: data.id,
 *            nval: data.text,
 *            nla: data.lang,
 *        }, function() { container.trigger('bind'); });
 *        $(this).val(null).trigger('change');
 *    });
 * ```
 *
 */
class select2 {
    /**
     * @param UAppPage $page
     * @throws ConfigurationException
     * @throws InternalException
     */
    static public function init($page) {
        // select2 distributable
        $page->addJs(self::class . '/dist/js/select2.full.js');
        $page->addJs(self::class . '/dist/js/i18n/it.js');
        $page->addJs(self::class . '/dist/js/i18n/hu.js');
        $page->addCss(self::class . '/dist/css/select2.min.css');
        $page->addXslCall('select2'); // it must be called at body start
    }
}
