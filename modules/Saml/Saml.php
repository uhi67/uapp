<?php
/**
 * Saml module
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright 2018
 * @license MIT
 */

use SimpleSAML\Auth\Simple;

/**
 * Class Saml
 */
abstract class Saml extends Module {
    static $as;

    static function init() {
        self::$as = new Simple('default-sp');
    }
}
