/*
 * Showdown.js
 * UApp interface to showdown module
 * Replaces md content of pre.markdown tags to html
 * Converts relative urls to configured prefix
 */

$(function () {
	const $body = $('body');
	let relativeUrl = $body.data('markdown-relative');
	if (!relativeUrl) relativeUrl = 'start/doc/';
	showdown.setFlavor('github');
	const rr = function () {
		return [
			{
				type: 'output',
				regex: /(href|src)="([^:"]+)"/g,
				replace: '$1="' + relativeUrl + '$2"'
			}
		];
	};
	showdown.extension('rr', rr);

	$body.on('showdown-convert', function () {
		$('pre.markdown').on('convert', function () {
			const p = this.parentNode;
			const converter = new showdown.Converter({extensions: ['rr']});
			const cont = converter.makeHtml($(this).text());
			//console.log(cont);
			if (cont) $(this).replaceWith(cont.replace('&', '&amp;'));

			// Replaces _icons_ to spans
			$('em', p).each(function () {
				if (this.innerText.match(/^(glyphicon |fa |fas |far )/)) this.outerHTML = '<span class="' + this.innerText + '"></span>';
			});

			// Replaces **{class} text** to spans with class
			$('strong', p).each(function () {
				if (this.innerText.substr(0, 1) === '{') {
					this.outerHTML = this.innerHTML.replace(/{(.*)}(.*)$/, '<span class="$1">$2</span>');
				}
			});

		}).trigger('convert');
	}).trigger('showdown-convert');
});
