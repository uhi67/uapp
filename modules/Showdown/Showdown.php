<?php
/**
 * Showdown module
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright 2017
 * @license MIT
 * @package modules
 */

/** @noinspection PhpUnused */

/**
 * Class Showdown
 *
 * Replaces md content of pre.markdown tags to html
 *
 * Usage
 * -----
 *
 * ### Configuration (e.g. in main config)
 * ```php
 *    'showdown' => [
 *        'relativeUrl' => '/start/doc', // prefix for relative urls in converted content
 *    ],
 * ```
 *
 * ### Initialize at page preload:
 * ```php
 *    $showdown = new Showdown($this, UApp::config('showdown'));
 * ```
 *
 * ### Place markdown texts into the view:
 * ```html
 *    <pre class="markdown">markdown text</pre>
 * ```
 */
class Showdown extends Module {
    /** @var string $relativeUrl -- prefix for relative urls in converted content */
    public $relativeUrl;

    /**
     * @throws ConfigurationException
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     */
    public function prepare() {
        parent::prepare();
        $this->page->addControlVar('markdown-relative', $this->relativeUrl);
        $this->page->addCss('Showdown/Showdown.css');
        $this->page->addJs('Showdown/Showdown.js');
        $this->page->addJs('Showdown/dist/showdown.min.js');
    }

    /**
     * @param UAppPage $page
     * @throws
     */
    public static function init($page) {
        $page->addJs('Showdown/dist/showdown.min.js');
    }

    /**
     * @inheritDoc
     */
    function action() {
        // TODO: Implement action() method.
    }
}
