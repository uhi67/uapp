/*
	uapp.js
	UApp main module and polyfills
*/

/*
	Ensures there will be no 'console is undefined' errors
*/
window.console = window.console || (function () {
	const c = {};
	c.log = c.warn = c.debug = c.info = c.error = c.time = c.dir = c.profile = c.clear = c.exception = c.trace = c.assert = function () {
	};
	return c;
})();

/**
 * The UApp module
 * @returns {{getModule: (function(*): *), UMessage: (function(*, *): boolean), UConfirm: (function(*, *, *, *): boolean), registerModule: (function(*, *): registerModule), getModules: (function(): *[]), polyfills: polyfills}}
 */
const UApp = defineUappModule();
UApp.polyfills();

/**
 * defines the body of the UApp module
 * @returns {{getModule: (function(*): *), UMessage: (function(*, *): boolean), UConfirm: (function(*, *, *, *): boolean), registerModule: (function(*, *): registerModule), getModules: (function(): *[]), polyfills: polyfills}}
 */
function defineUappModule() {
	return (function () {
		console.log('UApp module');
		const modules = {};
		/**
		 * Using: UApp.UConfirm('Confirmation', 'Are you sure?', okAction, cancelAction);
		 * @return {boolean}
		 */
		const UConfirm = function (title, message, okaction, cancelaction) {
			// noinspection JSUnusedGlobalSymbols
			$('<div></div>')
				.appendTo('body')
				.html('<div><h6>' + message + '</h6></div>')
				.dialog({
					modal: true, title: title, zIndex: 10000, autoOpen: true,
					width: 'auto', resizable: false,
					buttons: {
						Yes: function () {
							okaction();
							$(this).dialog("close");
						},
						No: function () {
							if (cancelaction) cancelaction();
							$(this).dialog("close");
						}
					},
					close: function () {
						$(this).remove();
					}
				});
			return false;
		};

		/**
		 * Using: UApp.UMessage('Figyelem!', 'Valami hiányzik.');
		 * @return {boolean}
		 */
		const UMessage = function (title, message) {
			$('<div></div>')
				.appendTo('body')
				.html('<div><h6>' + message + '</h6></div>')
				.dialog({
					modal: true, title: title, zIndex: 10000, autoOpen: true,
					width: 'auto', resizable: false,
					buttons: {},
					close: function () {
						$(this).remove();
					}
				});
			return false;
		};
		/**
		 * Defines polyfills:
		 * - String.substringBefore
		 * - String.substringAfter
		 * - String.substitute
		 * - String.urlencode
		 * - Object.values
		 * - Object.concat
		 * - $.unserialize
		 */
		const polyfills = function () {
			/*
                String.substringBefore(pattern)
                Returns a new substring from the start until to the given pattern
                or the end of the string if not found.
            */
			if (!String.prototype.substringBefore)
				String.prototype.substringBefore = function (p) {
					const pos = this.indexOf(p);
					if (pos == -1) return this;
					return this.substring(0, pos);
				};

			/*
                String.substringAfter(pattern)
                Returns a new substring from the end of the given pattern to the end,
                or the whole string if not found.
            */
			if (!String.prototype.substringAfter)
				String.prototype.substringAfter = function (p) {
					const pos = this.indexOf(p);
					if (pos == -1) return this;
					return this.substring(pos + p.length);
				};

			/*
                String.substitute(arguments)
                Returns a new string $var's substituted with values of arguments object
                Unmatched variable patterns remain in original string
            */
			if (!String.prototype.substitute) {
				/**
				 * @param {object} a
				 * @returns {String}
				 */
				String.prototype.substitute = function (a) {
					let r = this;
					for (const v in a) r = r.replace(new RegExp('\\$' + v), a[v]);
					return r;
				};
			}
			/*
                Returns new string urlencoded
            */
			if (!String.prototype.urlencode)
				String.prototype.urlencode = function () {
					return encodeURIComponent(this).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
				};


			Object.values = function (obj) {
				let a = [];
				for (const i in obj) {
					// noinspection JSUnfilteredForInLoop
					a.push(obj[i]);
				}
				return a;
			};

			Object.concat = function () {
				const obj = {};
				for (let i = 0; i < arguments.length; i++) {
					let b = arguments[i];
					for (let p in b)
						if (b.hasOwnProperty(p)) obj[p] = b[p];
				}
				return obj;
			};

			$.unserialize = function (str) {
				str = decodeURI(str);
				const obj = {};
				str.replace(
					new RegExp("([^?=&]+)(=([^&]*))?", "g"),
					function ($0, $1, $2, $3) {
						obj[$1] = $3;
					}
				);
				return obj;
			};
		};

		/**
		 * @param name
		 * @param module
		 */
		const registerModule = function (name, module) {
			if (typeof name !== 'string') {
				throw 'Name must be a string';
			}
			if (typeof module !== 'object') {
				throw 'Module must be an object';
			}
			if (typeof modules[name] === 'undefined') modules[name] = module;
			return this;
		};

		/**
		 * @param name
		 * @returns {*}
		 */
		const getModule = function (name) {
			return modules[name];
		};

		/**
		 * @returns {unknown[]}
		 */
		const getModules = function () {
			return Object.values(modules);
		};

		return {
			UConfirm: UConfirm,
			UMessage: UMessage,
			polyfills: polyfills,
			registerModule: registerModule,
			getModule: getModule,
			getModules: getModules,
		};
	})();
}

