<?xml version="1.0" encoding="UTF-8"?>
<!-- design_raw.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="xml" version="1.0" indent="yes"
	            doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	            doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	            omit-xml-declaration="yes"
	/>

	<!-- raw design is for non-html output. This skips the basic html structure defined in main.xls -->
	<xsl:template match="/">
		<xsl:apply-templates select="data"/>
	</xsl:template>

</xsl:stylesheet>
