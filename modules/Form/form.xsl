<?xml version="1.0" encoding="UTF-8"?>
<!-- form.xsl -->
<!--suppress XmlDefaultAttributeValue -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="xml" version="1.0" indent="yes"
	            doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	            doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	            omit-xml-declaration="yes"
	/>
	<!--
		Templates you may override and recall original:

		<xsl:template match="form" mode="content" name="form-content-default">
		<xsl:template match="form" mode="before-table" />
		<xsl:template match="form" mode="begin-table" />
		<xsl:template match="form" mode="end-table" />
		<xsl:template match="form" mode="after-table" />
		<xsl:template match="form" mode="after-form" />

		<xsl:template match="attribute" mode="row" name="attribute-row">
		<xsl:template match="attribute" mode="data-row" name="attribute-data-row">
		<xsl:template match="attribute" mode="rowclass" name="attribute-rowclass">
		<xsl:template match="attribute" mode="error-row" name="attribute-error-row">
		<xsl:template match="attribute" mode="descr-row" name="attribute-descr-row">
		<xsl:template match="attribute" mode="before-row" />
		<xsl:template match="attribute" mode="after-row" />

		<xsl:template match="attribute" mode="data-row-end"> === extra cells in the data row after field cell

		<xsl:template match="attribute" mode="field" name="attribute-field"> === The input field and the comments
		<xsl:template match="attribute" mode="input" name="attribute-input"> === The input field without comments
		<xsl:template match="attribute" mode="extra"> === extra content just after the field
		<xsl:template match="attribute" mode="comment" name="attribute-comment"> === The after-field comment span without the [?] div
		<xsl:template match="attribute" mode="help" name="attribute-help"> === The [?] div
	 -->
	<xsl:template match="form">
		<xsl:comment>form</xsl:comment>
		<xsl:variable name="enctype">
			<xsl:choose>
				<xsl:when test="@enctype">
					<xsl:value-of select="@enctype"/>
				</xsl:when>
				<xsl:when test="attribute[@type='file']">multipart/form-data</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<form class="form {@class}" id="form_{@name}" name="{@name}" method="post">
			<xsl:if test="$enctype!=''">
				<xsl:attribute name="enctype">
					<xsl:value-of select="$enctype"/>
				</xsl:attribute>
			</xsl:if>
			<input type="hidden" name="{@name}[_form]" value="{@id}"/>

			<xsl:apply-templates select="." mode="content"/>
		</form>
		<xsl:apply-templates select="menu" mode="context"/>
		<xsl:apply-templates select="." mode="after-form"/>
	</xsl:template>

	<!-- The default content is a table. May be overridden -->
	<xsl:template match="form" mode="content" name="form-content-default">
		<xsl:comment>form-content-default</xsl:comment>
		<xsl:apply-templates select="." mode="before-table"/><!-- callback -->
		<table class="grid data {@table-class}">
			<xsl:apply-templates select="." mode="begin-table"/><!-- callback -->

			<!-- table rows for visible attributes -->
			<xsl:apply-templates select="attribute[@type!='hidden' and @type!='skipped']" mode="row">
				<xsl:sort select="@order" data-type="number"/>
			</xsl:apply-templates>

			<xsl:apply-templates select="." mode="end-table"/><!-- callback -->
		</table>
		<xsl:for-each select="attribute[@type='hidden']">
			<xsl:apply-templates select="." mode="field"/>
		</xsl:for-each>
		<xsl:apply-templates select="." mode="after-table"/><!-- callback -->
	</xsl:template>

	<!-- form callback defaults -->
	<xsl:template match="form" mode="before-table"/>
	<xsl:template match="form" mode="begin-table"/>
	<xsl:template match="form" mode="end-table"/>
	<xsl:template match="form" mode="after-table"/>
	<xsl:template match="form" mode="after-form"/>

	<xsl:template match="attribute" mode="rowgroupclass" name="attribute-rowgroupclass">
		<xsl:value-of select="@rowgroupclass"/>
		<xsl:if test="@hidden=1">hidden</xsl:if>
	</xsl:template>

	<xsl:template match="attribute" mode="create-rowgroupclass">
		<xsl:variable name="rowgroupclass">
			<xsl:apply-templates select="." mode="rowgroupclass"/>
		</xsl:variable>
		<xsl:if test="$rowgroupclass!=''">
			<xsl:attribute name="class">
				<xsl:value-of select="$rowgroupclass"/>
			</xsl:attribute>
		</xsl:if>
	</xsl:template>

	<!-- default table rows for visible attribute -->
	<xsl:template match="attribute" mode="row" name="attribute-row">
		<!-- <xsl:comment>form/attribute-row</xsl:comment> -->
		<tbody>
			<xsl:apply-templates select="." mode="before-row"/>
			<xsl:apply-templates select="." mode="create-rowgroupclass"/>
			<xsl:apply-templates select="." mode="data-row"/>
			<xsl:apply-templates select="." mode="error-row"/>
			<xsl:apply-templates select="." mode="descr-row"/>
			<xsl:apply-templates select="." mode="after-row"/>
		</tbody>
	</xsl:template>

	<xsl:template match="attribute" mode="before-row"/>
	<xsl:template match="attribute" mode="after-row"/>

	<!-- data row. 'invalid' class on row indicates invalid value in the field -->
	<xsl:template match="attribute" mode="data-row" name="attribute-data-row">
		<tr>
			<xsl:apply-templates select="." mode="create-rowclass"/>
			<!-- <xsl:if test="$rowclass!=''"><xsl:attribute name="class"><xsl:value-of select="$rowclass" /></xsl:attribute></xsl:if> -->
			<th>
				<xsl:apply-templates select="." mode="label"/>
			</th>
			<td>
				<xsl:if test="@colspan">
					<xsl:attribute name="colspan">
						<xsl:value-of select="@colspan"/>
					</xsl:attribute>
				</xsl:if>
				<xsl:apply-templates select="." mode="field"/>
			</td>
			<xsl:apply-templates select="." mode="data-row-end"/>
		</tr>
	</xsl:template>

	<!-- template for a new attributum in extension cells -->
	<xsl:template match="attribute" mode="data-cells" name="attribute-data-cells">
		<xsl:param name="type" select="@type"/>
		<th>
			<xsl:apply-templates select="." mode="label"/>
		</th>
		<td>
			<xsl:apply-templates select="." mode="field">
				<xsl:with-param name="type" select="$type"/>
			</xsl:apply-templates>
		</td>
	</xsl:template>

	<xsl:template match="attribute" mode="rowclass" name="attribute-rowclass">
		<xsl:value-of select="@rowclass"/>
		<xsl:if test="error">invalid</xsl:if>
	</xsl:template>

	<xsl:template match="attribute" mode="create-rowclass">
		<xsl:variable name="rowclass">
			<xsl:apply-templates select="." mode="rowclass"/>
		</xsl:variable>
		<xsl:if test="$rowclass!=''">
			<xsl:attribute name="class">
				<xsl:value-of select="$rowclass"/>
			</xsl:attribute>
		</xsl:if>
	</xsl:template>

	<!-- error message row. Hidden if the value is valid. -->
	<xsl:template match="attribute" mode="error-row" name="attribute-error-row">
		<xsl:variable name="class">
			<xsl:if test="not(error)">hidden</xsl:if>
		</xsl:variable>
		<tr class="field-error-area {$class}" data-field="{@name}">
			<td>&#160;</td>
			<td class="field-error-container">
				<xsl:for-each select="error">
					<xsl:choose>
						<xsl:when test="substring(.,1,1)='~'">
							<div class="field-error">
								<pre class="markdown">
									<xsl:value-of select="substring(.,2)"/>
								</pre>
							</div>
						</xsl:when>
						<xsl:otherwise>
							<div class="field-error">
								<xsl:value-of select="." disable-output-escaping="yes"/>
							</div>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</td>
		</tr>
	</xsl:template>

	<xsl:template match="attribute" mode="descr-row" name="attribute-descr-row">
		<xsl:variable name="model" select="../*[name()=current()/../@modelnode]"/>
		<xsl:if test="@refdescr">
			<xsl:variable name="refset" select="@refset"/>
			<xsl:variable name="value">
				<xsl:choose>
					<!-- Value given to attribute overrides model -->
					<xsl:when test="@value">
						<xsl:value-of select="@value"/>
					</xsl:when>
					<!-- Model's attribute with this name -->
					<xsl:when test="$model/@*[name()=current()/@name]">
						<xsl:value-of select="$model/@*[name()=current()/@name]"/>
					</xsl:when>
					<!-- Model's subnode with attribute name -->
					<xsl:otherwise>
						<xsl:value-of select="$model/*[name()=current()/@name]"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<tr>
				<xsl:if test="../@descr-class">
					<xsl:attribute name="class">
						<xsl:value-of select="../@descr-class"/>
					</xsl:attribute>
				</xsl:if>
				<td colspan="2">
					<span id="refdescr_{@name}" class="descr refdescr">
						<xsl:value-of select="$content/sets/*[name()=$refset and @id=$value]/@descr"/>
					</span>
				</td>
			</tr>
		</xsl:if>
		<xsl:for-each select="descr">
			<tr>
				<td/>
				<td>
					<span class="descr">
						<xsl:value-of select="."/>
					</span>
				</td>
			</tr>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="attribute" mode="label">
		<xsl:value-of select="@label"/>
	</xsl:template>

	<!--
		creates an input field of the form with given values.
		Displays extra and comment values after the input.

		@name - field name
		@type - field type (ref means reference: hidden integer, readonly string)
		@value - main input value
		@namevalue - name field input value in reference fields
		@title - alt for input
		@class - class for input
		@size - size for input
		@maxlength - maxlength for input
		hint - hint under cursor
		help - long text of help button after input: [?] Multiple texts are paragraphs.
		comment - short descr after field
		descr - longer descr in the next line
		tooltip - tutorial
		rule - json rule data for client-side data validation

	 -->
	<xsl:template match="attribute" mode="field" name="attribute-field">
		<xsl:param name="type" select="@type"/>
		<xsl:param name="formname" select="../@name"/>
		<xsl:param name="name" select="@name"/>
		<xsl:apply-templates select="." mode="input">
			<xsl:with-param name="type" select="$type"/>
			<xsl:with-param name="formname" select="$formname"/>
			<xsl:with-param name="name" select="$name"/>
		</xsl:apply-templates>
		<xsl:apply-templates select="." mode="extra"/>
		<xsl:if test="$type!='hidden' and $type!='skipped'">
			<xsl:apply-templates select="." mode="help"/>
			<xsl:apply-templates select="." mode="comment"/>
		</xsl:if>
	</xsl:template>

	<!--
		creates an input field of the form without extra and comment parts.

		@name - field name
		@type - field type (ref means reference: hidden integer, readonly string)
		@value - main input value
		@namevalue - name field input value in reference fields
		@title - alt for input
		@class - class for input
		@size - size for input
		@maxlength - maxlength for input
		hint - hint under cursor
		help - long text of help button after input: [?] Multiple texts are paragraphs.
		comment - short descr after field
		descr - longer descr in the next line
		tooltip - tutorial
		rule - json rule data for client-side data validation

	 -->
	<xsl:template match="attribute" mode="input" name="attribute-input">
		<xsl:param name="type" select="@type"/>
		<xsl:param name="formname" select="../@name"/>
		<xsl:param name="name" select="@name"/>
		<xsl:param name="class" select="@class"/>
		<!-- form/attribute-field -->
		<xsl:comment>form/attribute-field type=<xsl:value-of select="$type"/>
		</xsl:comment>
		<xsl:variable name="model" select="../..//*[name()=current()/../@modelnode]"/>
		<xsl:variable name="value">
			<xsl:choose>
				<!-- Value given to attribute overrides model -->
				<xsl:when test="@value">
					<xsl:value-of select="@value"/>
				</xsl:when>
				<!-- Model's attribute with this name -->
				<xsl:when test="$model/@*[name()=$name]">
					<xsl:value-of select="$model/@*[name()=$name]"/>
				</xsl:when>
				<!-- Model's subnode with attribute name -->
				<xsl:otherwise>
					<xsl:value-of select="$model/*[name()=$name]"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="namevalue">
			<xsl:choose>
				<xsl:when test="@namevalue">
					<xsl:value-of select="@namevalue"/>
				</xsl:when>
				<!-- using attribute/ref data for namevalue -->
				<xsl:otherwise>
					<xsl:value-of select="$model/*[name()=current()/@ref]/@*[name()=current()/@refname]"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="rules">
			<xsl:apply-templates select="rule" mode="classname"/>
		</xsl:variable>
		<xsl:variable name="error">
			<xsl:if test="error">invalid</xsl:if>
		</xsl:variable>
		<xsl:variable name="style">
			<xsl:if test="@width">width:<xsl:value-of select="@width"/>
				<xsl:if test="not(contains(@width, '%'))">px</xsl:if>;
			</xsl:if>
			<xsl:if test="@height and ../@edit!=0">height:<xsl:value-of select="@height"/>px;
			</xsl:if>
			<xsl:if test="@height and ../@edit=0">max-height:<xsl:value-of select="@height"/>px;
			</xsl:if>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$type='hidden'">
				<input type="hidden" id="field_{$name}" name="{$formname}[{$name}]" value="{$value}"
				       data-label="{@label}"
				       class="{$error}">
					<xsl:if test="../@edit=0">
						<xsl:attribute name="disabled">disabled</xsl:attribute>
					</xsl:if>
				</input>
			</xsl:when>
			<xsl:when test="$type='skipped'">
			</xsl:when>
			<xsl:when test="$type='disabled'"> <!-- disabled fields will be skipped on validation -->
				<input id="field_{$name}" name="{$formname}[{$name}]" value="{$value}" disabled="disabled"
				       onclick="return false;" data-label="{@label}" class="{$class} {$error}"/>
			</xsl:when>
			<xsl:when test="$type='view'">
				<span id="field_{$name}" class="fieldtype_view {$class}">
					<xsl:if test="$class!=''">
						<xsl:attribute name="class">
							<xsl:value-of select="$class"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:if test="$style!=''">
						<xsl:attribute name="style">
							<xsl:value-of select="$style"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:value-of select="$value"/>
				</span>
			</xsl:when>
			<xsl:when test="$type='link'">
				<xsl:variable name="link">
					<xsl:value-of select="substring-before(@link, '{$value}')"/>
					<xsl:if test="not(contains(@link, '{$value}'))">
						<xsl:value-of select="@link"/>
					</xsl:if>
					<xsl:value-of select="$value"/>
					<xsl:value-of select="substring-after(@link, '{$value}')"/>
				</xsl:variable>
				<a class="{$class}" href="{$link}">
					<xsl:if test="$style!=''">
						<xsl:attribute name="style">display:inline-block;
							<xsl:value-of select="$style"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:value-of select="$namevalue"/>
				</a>
			</xsl:when>
			<!-- Form readonly makes all other types to readonly -->
			<xsl:when test="../@edit=0">
				<xsl:choose>
					<xsl:when test="($type='boolean' or $type='checkbox') and ../@edit=0">
						<xsl:choose>
							<xsl:when test="$value &gt; 0 or $value='t' or $value='true'">
								<i class="fa fa-check-square-o"/>
							</xsl:when>
							<xsl:otherwise>
								<i class="fa fa-square-o"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="$type='textarea'">
						<textarea id="field_{$name}" name="{$formname}[{$name}]" title="{@title}" data-label="{@label}"
						          readonly="readonly" disabled="disabled" onclick="return false;">
							<xsl:if test="$class!=''">
								<xsl:attribute name="class">
									<xsl:value-of select="$class"/>
								</xsl:attribute>
							</xsl:if>
							<xsl:if test="$style!=''">
								<xsl:attribute name="style">
									<xsl:value-of select="$style"/>
								</xsl:attribute>
							</xsl:if>
							<xsl:copy-of select="$value"/>
						</textarea>
					</xsl:when>
					<xsl:when test="$type='ref'">
						<input id="field_{$name}_name" name="{$formname}[{$name}_name]" alt="{@title}"
						       class="{$class} {$error}" value="{$namevalue}" readonly="readonly" disabled="disabled"
						       onChange="0;" data-label="{@label}">
							<xsl:if test="@size!=''">
								<xsl:attribute name="size">
									<xsl:value-of select="@size"/>
								</xsl:attribute>
							</xsl:if>
							<xsl:if test="@maxlength!=''">
								<xsl:attribute name="maxlength">
									<xsl:value-of select="@maxlength"/>
								</xsl:attribute>
							</xsl:if>
						</input>
					</xsl:when>
					<xsl:otherwise>
						<input id="field_{$name}" name="{$formname}[{$name}]" value="{$value}" readonly="readonly"
						       disabled="disabled" onclick="return false;" data-label="{@label}"
						       class="{$class} {$error} {$rules}">
							<xsl:if test="$style!=''">
								<xsl:attribute name="style">
									<xsl:value-of select="$style"/>
								</xsl:attribute>
							</xsl:if>
						</input>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>

			<xsl:when test="$type='readonly'"> <!-- readonly fields will be validated -->
				<input id="field_{$name}" name="{$formname}[{$name}]" value="{$value}" readonly="readonly"
				       onclick="return false;" data-label="{@label}" class="{$class} {$error} {$rules}">
					<xsl:if test="$style!=''">
						<xsl:attribute name="style">
							<xsl:value-of select="$style"/>
						</xsl:attribute>
					</xsl:if>
				</input>
			</xsl:when>
			<xsl:when test="$type='ref'">
				<input type="hidden" id="field_{$name}" name="{$formname}[{$name}]" value="{$value}"/>
				<input id="field_{$name}_name" name="{$formname}[{$name}_name]" alt="{@title}"
				       class="selectfrom {$class} {$rules} {$error}" value="{$namevalue}" disabled="disabled"
				       onChange="0;" data-label="{@label}">
					<xsl:if test="@readonly">
						<xsl:attribute name="readonly">readonly</xsl:attribute>
						<xsl:attribute name="onclick">return false;</xsl:attribute>
					</xsl:if>
					<xsl:if test="$style!=''">
						<xsl:attribute name="style">
							<xsl:value-of select="$style"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:if test="@size!=''">
						<xsl:attribute name="size">
							<xsl:value-of select="@size"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:if test="@maxlength!=''">
						<xsl:attribute name="maxlength">
							<xsl:value-of select="@maxlength"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:apply-templates select="rule" mode="data"/>
				</input>
			</xsl:when>
			<xsl:when test="$type='boolean' or $type='checkbox'">
				<xsl:comment>value="<xsl:value-of select="$value"/>"
				</xsl:comment>
				<input id="field_{$name}_default" type="hidden" name="{$formname}[{$name}]"
				       value="0"/> <!-- unchecked value -->
				<input id="field_{$name}" type="checkbox" name="{$formname}[{$name}]" value="1" data-label="{@label}"
				       class="{$class} {$error} {$rules}">
					<xsl:if test="@readonly">
						<xsl:attribute name="readonly">readonly</xsl:attribute>
						<xsl:attribute name="onclick">return false;</xsl:attribute>
					</xsl:if>
					<xsl:if test="$value &gt; 0 or $value='t' or $value='true'">
						<xsl:attribute name="checked">checked</xsl:attribute>
					</xsl:if>
					<xsl:apply-templates select="rule" mode="data"/>
				</input>
			</xsl:when>
			<xsl:when test="$type='date'">
				<xsl:variable name="datepicker">
					<xsl:if test="not(@readonly)">datepicker</xsl:if>
				</xsl:variable>
				<input id="field_{$name}" name="{$formname}[{$name}]" value="{$value}" data-label="{@label}"
				       class="{$datepicker} {$class} {$error} {$rules}">
					<xsl:apply-templates select="rule" mode="data"/>
				</input>
			</xsl:when>
			<xsl:when test="$type='select'">
				<xsl:variable name="refset" select="@refset"/>
				<xsl:variable name="refdescr" select="@refdescr"/>
				<select id="field_{$name}" name="{$formname}[{$name}]" data-label="{@label}"
				        class="{$class} {$rules} {$error}" data-setname="{$refset}">
					<xsl:if test="$style!=''">
						<xsl:attribute name="style">
							<xsl:value-of select="$style"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:apply-templates select="rule" mode="data"/>
					<option value=""/>
					<xsl:for-each select="set">
						<option value="{@id}">
							<xsl:if test="@id=$value">
								<xsl:attribute name="selected">yes</xsl:attribute>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="@value">
									<xsl:value-of select="@value"/>
								</xsl:when>
								<xsl:when test=".">
									<xsl:value-of select="."/>
								</xsl:when>
								<xsl:otherwise>-</xsl:otherwise>
							</xsl:choose>
						</option>
					</xsl:for-each>
					<xsl:for-each select="$content/sets/*[name()=$refset]">
						<option value="{@id}">
							<xsl:if test="@id=$value">
								<xsl:attribute name="selected">yes</xsl:attribute>
							</xsl:if>
							<xsl:if test="$refdescr">
								<xsl:attribute name="data-descr">
									<xsl:value-of select="@descr"/>
								</xsl:attribute>
							</xsl:if>
							<xsl:value-of select="@value"/>
							<xsl:if test="not(@value)">
								<xsl:value-of select="$name"/>
							</xsl:if>
						</option>
					</xsl:for-each>
				</select>
			</xsl:when>
			<xsl:when test="$type='radio'">
				<!-- name of node in content/sets/ where selectable values reside -->
				<xsl:variable name="refset" select="@refset"/>
				<!-- name of description attribute in refset elements -->
				<xsl:variable name="refdescr" select="@refdescr"/>

				<xsl:variable name="label" select="@label"/>

				<xsl:for-each select="set">
					<input type="radio" id="field_{$name}" name="{$formname}[{$name}]" value="{@id}"
					       class="{$class} {$rules} {$error}"
					       data-label="{$label}" data-setname="{$refset}" data-orig="{@id}"
					>
						<xsl:if test="$style!=''">
							<xsl:attribute name="style">
								<xsl:value-of select="$style"/>
							</xsl:attribute>
						</xsl:if>
						<xsl:apply-templates select="rule" mode="data"/>
						<xsl:if test="@id=$value">
							<xsl:attribute name="checked">yes</xsl:attribute>
						</xsl:if>
					</input>
					<span class="radiolabel">
						<xsl:choose>
							<xsl:when test="@value">
								<xsl:value-of select="@value"/>
							</xsl:when>
							<xsl:when test=".">
								<xsl:value-of select="."/>
							</xsl:when>
							<xsl:otherwise>-</xsl:otherwise>
						</xsl:choose>
					</span>
				</xsl:for-each>
				<xsl:for-each select="$content/sets/*[name()=$refset]">
					<input type="radio" id="field_{$name}" name="{$formname}[{$name}]" value="{@id}"
					       class="{$class} {$rules} {$error}"
					       data-label="{$label}" data-setname="{$refset}" data-orig="{@id}"
					>
						<xsl:if test="$style!=''">
							<xsl:attribute name="style">
								<xsl:value-of select="$style"/>
							</xsl:attribute>
						</xsl:if>
						<xsl:apply-templates select="rule" mode="data"/>
						<xsl:if test="@id=$value">
							<xsl:attribute name="checked">yes</xsl:attribute>
						</xsl:if>
						<xsl:if test="$refdescr">
							<xsl:attribute name="data-descr">
								<xsl:value-of select="@descr"/>
							</xsl:attribute>
						</xsl:if>
					</input>
					<span class="radiolabel">
						<xsl:value-of select="@value"/>
					</span>
				</xsl:for-each>
			</xsl:when>
			<xsl:when test="$type='file'">
				<input type="file" id="field_{$name}" name="{$formname}[{$name}]" alt="{@title}"
				       class="{$class} {$rules} {$error}" data-label="{@label}">
					<xsl:apply-templates select="rule" mode="data"/>
				</input>
			</xsl:when>
			<xsl:when test="$type='textarea'">
				<textarea id="field_{$name}" name="{$formname}[{$name}]" title="{@title}" data-label="{@label}"
				          data-orig="{$value}">
					<xsl:if test="$class!=''">
						<xsl:attribute name="class">
							<xsl:value-of select="$class"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:if test="$style!=''">
						<xsl:attribute name="style">
							<xsl:value-of select="$style"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:if test="@readonly">
						<xsl:attribute name="readonly">readonly</xsl:attribute>
						<xsl:attribute name="onclick">return false;</xsl:attribute>
					</xsl:if>
					<xsl:apply-templates select="rule" mode="data"/>
					<xsl:copy-of select="$value"/>
				</textarea>
			</xsl:when>
			<xsl:otherwise>
				<input id="field_{$name}" name="{$formname}[{$name}]" alt="{@title}" value="{$value}"
				       class="{$class} {$rules} {$error} type-{$type}"
				       data-orig="{$value}" data-label="{@label}"
				>
					<xsl:if test="@size!=''">
						<xsl:attribute name="size">
							<xsl:value-of select="@size"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:if test="@maxlength!=''">
						<xsl:attribute name="maxlength">
							<xsl:value-of select="@maxlength"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:if test="@readonly">
						<xsl:attribute name="readonly">readonly</xsl:attribute>
						<xsl:attribute name="onclick">return false;</xsl:attribute>
					</xsl:if>
					<xsl:if test="$style!=''">
						<xsl:attribute name="style">
							<xsl:value-of select="$style"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:apply-templates select="rule" mode="data"/>
				</input>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="attribute/rule" mode="classname">
		<xsl:variable name="rulename" select="translate(@name, ' ', '_')"/>
		rule-<xsl:value-of select="$rulename"/>
		<xsl:text> </xsl:text>
	</xsl:template>

	<!-- generates validation rule data attributes and maxlength -->
	<xsl:template match="rule[@name='length']" mode="data">
		<xsl:call-template name="rule-data"/>
		<xsl:if test="starts-with(data, '[') and not(contains(data, ','))">
			<xsl:attribute name="maxlength">
				<xsl:value-of select="substring-before(substring-after(data, '['), ']')"/>
			</xsl:attribute>
		</xsl:if>
		<xsl:if test="starts-with(data, '[') and contains(data, ',')">
			<xsl:attribute name="maxlength">
				<xsl:value-of select="substring-before(substring-after(data, ','), ']')"/>
			</xsl:attribute>
		</xsl:if>
	</xsl:template>

	<!-- generates validation rule data attributes (default) -->
	<xsl:template match="rule" mode="data" name="rule-data">
		<xsl:variable name="rulename" select="translate(@name, ' ', '_')"/>
		<xsl:for-each select="node()">
			<xsl:attribute name="data-{$rulename}-{name()}">
				<xsl:value-of select="."/>
			</xsl:attribute>
		</xsl:for-each>
	</xsl:template>

	<!-- Extra content right after input field within table cell -->
	<xsl:template match="attribute" mode="extra">
	</xsl:template>

	<!-- Extra content after field cell of data-row -->
	<xsl:template match="attribute" mode="data-row-end">
	</xsl:template>

	<!-- Comment and help button right after input field -->
	<xsl:template match="attribute" mode="comment" name="attribute-comment">
		<xsl:if test="comment">
			<span class="comment {../@comment-class}">
				<xsl:value-of select="comment"/>
			</span>
		</xsl:if>
	</xsl:template>

	<!-- Help button right after input field and comment -->
	<xsl:template match="attribute" mode="help" name="attribute-help">
		<xsl:if test="help">
			<div id="help-{@name}" class="hint {../@help-class} markdown">
				<xsl:if test="@readonly=1">
					<p>A mező értéke nem módosítható.</p>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="count(help) &gt; 1">
						<xsl:for-each select="help">
							<p>
								<xsl:value-of select="."/>
							</p>
						</xsl:for-each>
					</xsl:when>
					<xsl:when test="substring(help,1,1)='~'">
						<pre class="markdown">
							<xsl:value-of select="substring(help,2)"/>
						</pre>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="help"/>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</xsl:if>
	</xsl:template>

	<xsl:template match="xsl-call[@name='form']">
		<div id="form-error" title="Hiba" style="display:none">
			<p id="error-text">Ismeretlen hiba</p>
		</div>
	</xsl:template>

	<!-- External call templates -->
	<xsl:template match="menu[false()]" mode="context"/>

</xsl:stylesheet>
