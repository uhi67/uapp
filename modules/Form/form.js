/*
	form.js
    (c)uhi 2014-2017

    Features:

    1. formModule represents a main object of the form module (contains validation rules and methods)
    2. Catches form submit to validate form
    3. Declares `validate` event on fields
    4. Stores original values of all fields.
    5. Extends jQuery prototype with `restoreForm, cancelForm, validateForm, validateField` methods
    6. .clearbutton class makes a button for fields emptiing value
    7. .datepicker class for datepicker with Hungarian dates
	8. displays .multistate checkboxes based on data-values, data-name, data-icons
	9. displays reference descriptions for selects

    Usage of validation feature:

    add class="form" to form (automatic if using Form module)
    add alt="fieldname" to name inputs for error messages
    add validation classes (rulenames) like "mandatory" to validate inputs. It's automatic based on model rules in Form module.
    add data-rulename-data for validation data (e.g RegExp pattern form pattern rule)
    add data-rulename-message to override default rule message

    Multiple rules may be declared for a field, in this case all of them must be match.
    `Mandatory` rule must always match independently. Empty values in non-mandatory fields pass always.

    use $(form).restoreForm() to restore original values
    use $(form).cancelForm(question, url) to restore original values

    Customize form in page's js:

    form.checkform -- custom check of entire form as `boolean function()`
    $(form).data('rules', customrules).trigger('addRules') -- adding custom rules to be merged with original ruleset. This method is independent of js load order.

	Rules are hash of
		classname: {body, message}

	Rulebody must be
		- empty array: special value for mandatory rule.
		- RegExp
		- function(input, value, ruledata) returning boolean
		- array of rulebodies. All of rulebodies must be match for a rule.

	Ruleset may be a plain string. It will mean BNF rule, but is not yet implemented.

	Sample rules:

	'telnum': [[/^\+(\d\-?){8,15}$/], 'érvénytelen telefonszám. Csak +-szal kezdődő nemzetközi szám lehet.'], 				// +-szal kezdődő teljes nemzetközi szám
    'telnum2': [[/^((\+\d[0-9-]{8,17})|(\d{5}))$/], 'érvénytelen telefonszám. Nemzetközi szám vagy ötjegyű mellék lehet.'],	// +-szal kezdődő teljes nemzetközi szám vagy egyetemi mellék
    'email': [[/^\w+[\w\-\.]*\@\w+[\w\-\.]+$/],'érvénytelen e-mail cím'],
    'date': [[/^\d{4}\.\d{2}\.\d{2}\.?$/], 'érvénytelen dátum. Formátum: 1999.12.31'],
    'mac': [[/^[\dA-Fa-f]{2}[:.-]?[\dA-Fa-f]{2}[:.-]?[\dA-Fa-f]{2}[:.-]?[\dA-Fa-f]{2}[:.-]?[\dA-Fa-f]{2}[:.-]?[\dA-Fa-f]{2}$/], 'érvénytelen MAC. Szeperátorként : . - használható'],
    'ip': [/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/],
    'iprange': [[/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,2}$/], 'Érvénytelen ip tartomány. Formátum: 1.2.3.0/24'],
    'integer': [[/^[0-9]*$/], 'érvénytelen egész szám]'],
    'dnsname': [[/^(((([_a-z\d][-_a-z\d]*)|\*)(\.([_a-z\d][-_a-z\d]*))*\.?)|@)$/, /^.{1,253}$/, /^[^\.]{1,63}(\.[^\.]{1,63})*\.?$/], 'érvénytelen név'], // Lehet relatív és abszolút is, @ is.
    'relativename': [[/^((([_a-z\d](-*[_a-z\d])*)(\.([_a-z\d](-*[_a-z\d])*))*)|@)$/, /^.{1,253}$/, /^[^\.]{1,63}(\.[^\.]{1,63})*$/], 'érvénytelen relatív név'],
    'absolutename': [[/^(([_a-z\d](-*[_a-z\d])*)|\*)(\.([_a-z\d](-*[_a-z\d])*))*\.$/, /^.{1,253}$/, /^[^\.]{1,63}(\.[^\.]{1,63})*\.$/], 'érvénytelen abszolút név'],
    'relativenames': [[/^((((([_a-z\d][-_a-z\d]*)|\*)(\.[_a-z\d][-_a-z\d]*)*)|@)((\s*;\s*)(([_a-z\d][-_a-z\d]*)(\.[_a-z\d][-_a-z\d]*)*))*)$/], 'érvénytelen relatív név vagy nevek'],
    'dnsnames': [[/^((((([_a-z\d][-_a-z\d]*)|\*)((\.[_a-z\d][-_a-z\d]*)+\.?)?)|@)((\s*;\s*)(([_a-z\d][-_a-z\d]*)((\.[_a-z\d][-_a-z\d]*)+\.?)?))*)$/], 'érvénytelen név vagy nevek'],
    'url': [[/^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i], 'érvénytelen URL']

    Custom client rules may be added in user js:
    ```
	$('form.form[name^="dom"]').each(function(){
		$(this).daa('rules', {
			'parentVariantValidate': function(input, value, ruledata) {
				console.log(this);
				return false;
			}
		});
	});
    ```

    Forms may contain error displaying areas connected to the field, marked with classes:
	  - .field-error-area is the outermost container which will be hidden when the field is valid.
	  - .field-error-container is the place of one or more <div class="field-error"> exists or will be generated.
	  - .field-error - the actual message of a validation rule

	@module formModule
*/
var formModule = (function () {
	// Private variables

	// noinspection JSUnusedGlobalSymbols
	// noinspection JSUnusedLocalSymbols
	return {
		// Public variables and methods of `formModule` global object

		/**
		 * @typedef {(Rulebodyfunction|string|RegExp|Rulebody[])} Rulebody
		 */
		/**
		 * @callback Rulebodyfunction
		 * @param {object} $input - The field to be validated (not used)
		 * @param {string} value - The value to be validated
		 * @param {Array} data - [patterns]: patterns to match (first level: one of them must match, second level: all of them must match)
		 * @return {boolean}
		 */
		/**
		 * @typedef {object} Rule
		 * @property {string|Rulebodyfunction} body -- rule body is a string or function($input, value, data)
		 * @property {string} message -- the error message if rule doesn't match
		 */
		/**
		 * @var {object} formRules -- the default rules. May be completed or overwritten by custom rules in a specific form
		 * @property {Rule} * -- Keys of formRules are rule names, matching with field's `rule-rulename` classnames.
		 */
		formRules: {
			mandatory: {
				body: null,
				message: 'is mandatory'
			},
			/**
			 * @member {Rule} pattern -- validates by list of patterns
			 */
			pattern: {
				/**
				 * @param {object} $input - The field to be validated (not used)
				 * @param {string} value - The value to be validated
				 * @param {Array} data - [patterns]: patterns to match (first level: one of them must match, second level: all of them must match)
				 */
				body: function ($input, value, data) {
					//console.log('checking pattern', value, data);
					if ($input.isEmpty()) return true;
					if (data === undefined) return true; // missing data: readonly field
					var patterns = data[0]; // array of RegExp or array of array of RegExp
					if (patterns.constructor !== Array) patterns = [patterns];
					for (var i = 0; i < patterns.length; i++) { // Have to be fulfilled at least one of them
						var pattern = patterns[i];
						var f = true;
						if (typeof pattern === 'string') {
							re = new RegExp(pattern.substr(1, pattern.length - 2));
							f = re.test(value);
							console.log(pattern, re, value, f);
						} else if (pattern.constructor === Array) {
							for (var j = 0; j < pattern.length; j++) { // Have to be fulfilled all of them
								re = new RegExp(pattern[j].substr(1, pattern[j].length - 2));
								if (!re.test(value)) {
									f = false;
									break;
								}
							}
							console.log(f);
						} else if (pattern.constructor === RegExp) {
							f = pattern.test(value);
							console.log(f);
						} else {
							console.log('Invalid pattern definition', pattern, pattern.constructor);
							return false;
						}
						if (f) return true;
					}
					return false;
				},
				message: 'has invalid format'
			},

			/**
			 * @var {Rule} length -- validates to data = [minlen, maxlen]
			 * @function length#body
			 * @param {?int} data[0] -- maxlen
			 * @param {?int} data[1] -- minlen
			 */
			length: {
				body: function ($input, value, data) {
					//console.log('checking length', value, data);
					if ($input.isEmpty()) return true;
					if (data === undefined) return true; // missing data: readonly field
					minlen = data[0];
					maxlen = data[1];
					return !(maxlen && value.length > maxlen || minlen && value.length < minlen);
				},
				message: function ($input, value, data) {
					min = data[0];
					max = data[1];
					return 'length must be between ' + min + ' and ' + max;
				}
			},
			/**
			 * @var {Rule} between -- validates to data = [min, max]
			 * @function length#body
			 * @param {?int} data[0] -- maxlen
			 * @param {?int} data[1] -- minlen
			 */
			between: {
				body: function ($input, value, data) {
					//console.log('checking between', value, data);
					if ($input.isEmpty()) return true;
					if (data === undefined) return true; // missing data: readonly field
					min = data[0];
					max = data[1];
					return !(max && value > max || min && value < min);
				},
				message: 'must be between $1 and $2'
			},
			integer: {
				body: function ($input, value, data) {
					if ($input.isEmpty()) return true;
					//if(data===undefined) return true; // missing data: readonly field - ez nem megy, mert Inetnek amúgy sincs data része
					var pattern = /^[0-9]*$/;
					f = pattern.test(value);
					return f;
				},
				message: 'is invalid integer'
			},
			Inet: {
				body: function ($input, value, data) {
					if ($input.isEmpty()) return true;
					//if(data===undefined) return true; // missing data: readonly field - ez nem megy, mert Inetnek amúgy sincs data része
					var pattern = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$|^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:)(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/;
					f = pattern.test(value);
					return f;
				},
				message: 'is invalid ip address'
			},
			Macaddr: {
				body: function ($input, value, data) {
					if ($input.isEmpty()) return true;
					//if(data===undefined) return true; // missing data: readonly field - ez nem megy, mert Inetnek amúgy sincs data része
					var pattern = /^([\dA-Fa-f]{2})[:.-]?([\dA-Fa-f]{2})[:.-]?([\dA-Fa-f]{2})[:.-]?([\dA-Fa-f]{2})[:.-]?([\dA-Fa-f]{2})[:.-]?([\dA-Fa-f]{2})$/;
					f = pattern.test(value);
					return f;
				},
				message: 'is invalid MAC address'
			},

			/**
			 * @var {Rule} ajax -- validates by ajax callback with data
			 * @function ajax#body
			 * @param {string} data[0] -- url
			 * @param {object} data[1] -- parameters
			 */
			ajax: {
				body: function ($input, value, data) {
					if ($input.isEmpty()) return true;
					if (data === undefined) return true; // missing data: readonly field
					// TODO
					console.log('ajax on', $input, value, data);
				},
				message: 'is invalid'
			}
		},

		/**
		 * Returns message of the rulename
		 *
		 * Message may be a string or function($field, value, ruledata)
		 * Replaces $1,... with elements of rulename-data array
		 *
		 * @param def
		 * @param $field
		 * @param rulename
		 * @returns {*}
		 */
		getRuleMessage: function (def, $field, rulename) {
			var data = $field.data(rulename + '-data');
			if (typeof def === 'function') {
				if (typeof data === 'undefined') def = '';
				else {
					var v = $field.val();
					def = def($field, v, data); // returns string
				}
			}
			if (!(data instanceof Array)) data = [data];
			for (var i = 0; i < data.length; i++) {
				/** @var def {string} */
				def = def.replace(new RegExp('\\$' + (i + 1) + '\\b'), data[i]);
			}
			return def;
		},

		/**
		 *	Validates a field against a specific rule body. No side effects.
		 *	@param {array} rules -- array of all rules
		 *	@param {object} $field -- the field. If undefined, value check will be performed only.
		 *	@param {string} rulename -- name of the original rule
		 *	@param {any|Function} rulebody -- current rulebody to validate with, may be:
		 *		- function
		 *		- RegExp
		 *		- %rulename
		 *		- array of any type above
		 *		- first item of array may be an operator: AND, OR (default is AND)
		 *	@param {string} v - Value to check. if undefined, use the value of the field.
		 *	@return boolean
		 */
		validateRulebody: function (rules, $field, rulename, rulebody, v) {
			if (rulebody === undefined) {
				rulebody = rules[rulename].body;
				if (rulebody === undefined) {
					console.log('No rule named ' + rulename + '. ');
					return false;
				}
			}
			if (typeof v === 'undefined' && v === null) {
				console.log('No field and value to check. ');
				return false;
			}
			if (typeof v === 'undefined') v = $field.val();
			if (rulebody instanceof RegExp) {
				var re = new RegExp(rulebody);
				if (!re.test(v)) {
					console.log('validateRulebody/re* failed', $field.attr('name'), v, rulename, rulebody);
					return false;
				}
				return true;
			}
			if (typeof rulebody === 'function') {
				if (rulename === undefined) {
					console.log('Missing rulename. ');
					return false;
				}
				if (!$field) {
					console.log('No field for rule function.');
					return false;
				}

				// data may missing on readonly fields (undefined) -- check may be skipped in the rule
				var data = $field.data(rulename + '-data');
				var f1 = rulebody($field, v, data); // returns boolean
				if (!f1) {
					$field.data('error', rules[rulename][1]);
					//console.log('validateRulebody/fn failed', $field.attr('name'), v);
				}
				return f1;
			}
			// Recurse on array elements (ignore keys of object)
			if (rulebody instanceof Array) {
				var f = true;
				if (rulebody.length === 0) return f;
				var operator = 'AND';
				for (var r = 0; r < rulebody.length; r++) {
					var rr = rulebody[r];
					if (r === 0 && typeof rr === 'string' && rr[0] !== '%') {
						operator = rr;
						if (operator === 'OR') f = false;
						continue;
					}
					f2 = formModule.validateRulebody(rules, $field, rulename, rr, v);
					if (operator === 'AND' && !f2) return false;
					if (operator === 'OR' && f2) return true;
					if (operator === 'NOT') return !f2;
				}
				return f;
			}
			return true;
		},

		/**
		 * Validates a field in the form
		 * No side effects
		 * @return boolean -- the field is valid
		 */
		checkField: function ($form, field) {
			var rules = $form.data('allrules');
			//var id = $(this).attr('id');
			//var $errorArea = $('.field-error-area', $form).filter(function(){return 'field_'+$(this).data('field') == id;});
			var name = $(this).attr('name');

			var v = $(this).attr('type') === 'radio' ? $('input:radio[name=' + name + ']:checked', $(this).parents('form')).val() : $(this).val().trim();
			if (v === undefined) v = '';
			var emptyvalue = $(field).data('emptyvalue');
			var isEmpty = (v === '' || v === '_undefined_' || v === emptyvalue);

			var f = true;   // Is the field valid?
			// Runs all rules on the field
			for (var rulename in rules) {
				if (!rules.hasOwnProperty(rulename)) continue;
				if (!$(field).hasClass('rule-' + rulename)) continue;

				var rulebody = rules[rulename].body;

				if (rulename === 'mandatory') {
					if (isEmpty) return false;
				} else if (!isEmpty) {
					f = f && formModule.validateRulebody(rules, field, rulename, rulebody, v);
				}
			}
			return f;
		}
	};
})();

if (typeof (String.prototype.trim) === "undefined") {
	String.prototype.trim = function () {
		return String(this).replace(/^\s+|\s+$/g, '');
	};
}

// noinspection JSUnusedGlobalSymbols
/**
 * jQuery extensions for basic form operations
 */
$.fn.extend({
	/**
	 *	Cancel form changes and optionally proceed to an url.
	 *	Asks confirmation question is changes are made.
	 *	Returns false on cancelled confirmation.
	 *	Usage: `$(form).cancelForm(question, url);`
	 *	@return boolean -- false if confirmation is cancelled
	 */
	cancelForm: function (question, url) {
		var changed = false;
		$('input, select', this).each(function () {
			if ($(this).val() !== $(this).data('originalValue')) changed = true;
		});
		var conf = !changed || confirm(question);
		if (conf && url) document.location.href = url;
		return conf;
	},

	/**
	 * Restores original values of all fields of the form.
	 * @return void
	 */
	restoreForm: function () {
		$('input, select', this).each(function () {
			$(this).val($(this).data('originalValue'));
		});
	},

	/**
	 * Determines if the field's vallue is empty
	 * @return boolean
	 */
	isEmpty: function () {
		var v = $(this).attr('type') === 'radio' ? $('input:radio[name=' + this.name + ']:checked', $(this).parents('form')).val() : $(this).val().trim();
		if (v === undefined) v = '';
		var emptyvalue = $(this).data('emptyvalue');
		return (v === '' || v === '_undefined_' || v === emptyvalue);
	},

	/**
	 *	Validates a field of the form
	 *	Decorates with color and messages
	 *	@return boolean
	 */
	validateField: function () {
		var $field = $(this);
		var $form = $field.closest('form');
		var rules = $form.data('allrules');

		//var title = this.alt ? this.alt : (this.title ? this.title : this.name);
		//var title = $(this).data('label') ? $(this).data('label') : this.name;
		var name = $(this).attr('name');

		var emptyvalue = $field.data('emptyvalue');
		var v = $(this).attr('type') === 'radio' ? $('input[type=radio][name="' + name + '"]:checked', $(this).parents('form')).val() : $(this).val();
		if (typeof v === 'string') v = v.trim();
		if (v === undefined) v = '';
		var isEmpty = (v === '' || v === '_undefined_' || v === emptyvalue);

		var f = true;

		if ($field.hasClass('preselect')) {
			// Checking mandatory only
			if (($field.hasClass('mandatory') || $field.hasClass('rule-mandatory')) && isEmpty) {
				// Removing result classes mandatory class will color yellow.
				$(this).removeClass('ok').removeClass('invalid');
				$(this).parent('.field').removeClass('ok').removeClass('invalid');
				return false;
			}
			// Unknown state (last state will not change)
			return true;
		}

		var id = $field.attr('id');
		var $errorArea = $('.field-error-area', $form).filter(function () {
			return 'field_' + $(this).data('field') === id;
		});

		// Empty errorarea
		$errorArea.find('.field-error-container').html('');

		// Iterate rules
		for (var rulename in rules) {
			if (!rules.hasOwnProperty(rulename)) continue;
			if (!$field.hasClass('rule-' + rulename)) continue;

			var rulemessage = rules[rulename].message;
			var custommessage = $field.data(rulename + '-message');
			if (custommessage) rulemessage = custommessage;
			rulemessage = formModule.getRuleMessage(rulemessage, $field, rulename);

			var rulebody = rules[rulename].body;
			var f1 = true; // rule result

			if (rulename === 'mandatory') {
				f1 = !isEmpty;
			} else {
				f1 = formModule.validateRulebody(rules, $field, rulename, rulebody, v);
			}
			// rule decoration
			if (!f1 && $errorArea) {
				$('<div />').addClass('field-error').data('rulename', rulename).text(rulemessage).appendTo($('.field-error-container', $errorArea));
				$field.data('error', rulemessage);
				console.log(rulename, name, rulemessage);
			}

			f = f && f1;
			if (!f) console.log(rulename, 'failed', $field.attr('name'), v);
		}

		// field decoration on result
		if (!f) {
			$(this).removeClass('ok').addClass('invalid');
			$(this).parent('.field').removeClass('ok').addClass('invalid');
			$errorArea.removeClass('hidden');
		} else {
			if (!$(this).hasClass('selectfrom')) $(this).addClass('ok');
			$(this).removeClass('invalid');
			$(this).parent('.field').addClass('ok').removeClass('invalid');
			$errorArea.addClass('hidden').find('.field-error-container').html('');
		}
		return f;
	},

	/**
	 * Validates a (html) form before submit.
	 * Validates all fields against all validation rules including custom rules appended to the form.
	 * If a value of a field is not valid:
	 * 	- the field's title gets the rule-message of the failed validation rule rule.
	 *  - the background of the field changes to red
	 *  - the error-container in the error-container (if exist connected to the field) gets the rule-message
	 *  - the first invalid field gets the focus
	 *
	 * @return boolean -- false if any of the field values is not valid
	 */
	validateForm: function () {
		var $form = $(this);
		var r = true;

		// Iterates all fields
		$('input, select', $form).each(function () {
			var field = this;
			if (this.disabled) return;

			//console.log('Validating '+field.name);
			var f1 = $(field).validateField();

			var title = $(field).data('label');
			if (!title) title = field.name;
			//console.log(field, title);
			var msg = $(field).data('error');
			if (!msg) msg = 'has invalid value';
			// Form message and focus only at first invalid field
			if (r && !f1) {
				$form_error = $('#form-error');
				$('p#error-text', $form_error).text(title + ' ' + msg + '.');
				$form_error.dialog({
					resizable: false,
					height: 'auto',
					width: 400,
					modal: true,
					buttons: {
						OK: function () {
							$(this).dialog("close");
						}
					},
					close: function () {
						field.scrollIntoView();
						$(field).focus();
					}
				});
			}

			if (!f1) {
				// Empty errorarea
				var id = $(field).attr('id');
				var $errorArea = $('.field-error-area', $form).filter(function () {
					return 'field_' + $(this).data('field') === id;
				});
				var $errorContainer = $errorArea.find('.field-error-container');
				$errorContainer.html('');

				// Search for error line, if exists, show error message
				$errorArea.removeClass('hidden');
				console.log(title, msg);

				$errorContainer.append($('<div />').addClass('field-error submit-error').text(title + ' ' + msg + '.'));
			}

			r = r && f1;
		});
		if (!r) {
			$(this).addClass('invalid');
			console.log('Form check failed');
		} else {
			$(this).removeClass('invalid');
		}
		return r;
	}
});

$(function () {
	var $form = $('form.form');
	// noinspection JSUnusedLocalSymbols
	$form.submit(function (evt) {
		if (this.checkform) if (!this.checkform()) return false;    // custom check if exists
		return $(this).validateForm();
	});

	/**
	 *	Declares and immediately caal the 'addRules' event which
	 *	generates the merged set of standard and custom rules for all forms
	 *	Custom rules may be defined as data-rules of the form.
	 *	The structure of custorm rules are identical to standard rules in form.formRules.
	 *	Custom rules with the same name overwite standard ones.
	 */
	$form.on('addRules', function () {
		//console.log('addRules');
		var rules = formModule.formRules;
		var customRules = $(this).data('rules');
		if (customRules) {
			for (var r in customRules) {
				if (!customRules.hasOwnProperty(r)) continue;
				rules[r] = customRules[r];
			}
		}
		$(this).data('allrules', rules);
	}).trigger('addRules');

	$('input, select', $form).each(function () {

		$(this).data('originalValue', $(this).val());

		/**
		 *	Validates the field against all form rules.
		 *	Used from trigger when value is changed.
		 *	If the value of the field is invalid:
		 * 	- the field's title gets the rule-message of the failed validation rule rule.
		 *  - the backgound of the field changes to red
		 *  - if a field-error-area is connected to the field:
		 *		- any invalid rule generates a new field-error message in the field-error-container from the rule-message
		 *		- the field-error-area will be made visible
		 */
		$(this).on('validate', function () {
			$(this).validateField();
		});

		$(this).change(function () {
			$(this).trigger('validate');
		});
		// Gépelés közben késleltetett ellenőrzés
		$(this).keyup(function () {
			var $input = $(this);
			var old = $input.data('timer');
			if (old) clearTimeout(old);
			var timer = setTimeout(function () {
				$input.trigger('validate');
			}, 1000);
			$input.data('timer', timer);
		});
	});

	$('input.clearbutton').each(function () {
		var input = this;
		$('<span class="button"><i class="fa fa-times" aria-hidden="true"></i></span>').insertAfter(this).click(function () {
			$(input).val('');
		});
	});

	$datepicker = $('input.datepicker');
	$datepicker.datepicker({
		dateFormat: 'yy.mm.dd',
		constrainInput: true,
		dayNamesMin: ['V', 'H', 'K', 'Sz', 'Cs', 'P', 'Sz'],
		firstDay: 1,
		monthNamesShort: ["Jan", "Febr", "Márc", "Ápr", "Máj", "Jún", "Júl", "Aug", "Szept", "Okt", "Nov", "Dec"],
		monthNames: ["Január", "Február", "Március", "Április", "Május", "Június", "Július", "Augusztus", "Szeptember", "Október", "November", "December"],
		showMonthAfterYear: true,
		gotoCurrent: true,
		showAnim: 'clip',
		buttonText: 'választ',
		showOn: "button",
		buttonImage: "img/calendar24.png"
	});
	$datepicker.trigger('userInit'); // Mivel az előbb fut le

	/**
	 * Add .multistate class for any input to convert it to multistate icon
	 * Add value list in data-values, icon list in data-icons, title list in data-names
	 * The resulting fa-icon will cycle through icons and set the correponding value in original input.
	 */
	$('input.multistate').each(function () {
		var control = $(this);
		var values = control.hide().data('values');
		var icons = control.data('icons');
		//console.log(icons);
		var names = control.data('names');
		var index = values.indexOf(control.val());
		if (index === -1) index = 0;
		$('<i></i>').insertAfter(control).addClass('fa').addClass('fa-' + icons[index]).attr('title', names[index]).click(function () {
			// Switch to next value
			var index = values.indexOf(control.val());
			if (index === -1) index = 0;
			$(this).removeClass('fa-' + icons[index]);
			//console.log(index, control.val());
			index = (++index) % values.length;
			//console.log(index, values.length);
			control.val(values[index]);
			$(this).addClass('fa-' + icons[index]).attr('title', names[index]);
		});

	});

	$('form.form select').change(function () {
		var descr = $(this.options[this.selectedIndex]).data('descr');
		var name = this.id.substr(6);
		var dsn = '#refdescr_' + name;
		$(this).closest('form').find(dsn).text(descr);
	});

	$form.each(function () {
		var $invalidfields = $('input.invalid, select.invalid, textarea.invalid', this);
		if ($invalidfields.length) $invalidfields.first().focus().get(0).scrollIntoView(); // scrollIntoView is experiemental, but focus is not effective on disabled fields.
		else $('.focus', this).focus();
	});
});
