/*
	 SelectFrom.js

	 Usage is detailed in `SelectFrom.php`
*/

const SelectFrom = defineSelectFromModule();

$(function () {
	SelectFrom.init();
	UApp.registerModule('SelectFrom', SelectFrom);
});

function defineSelectFromModule() {
	const popups = [];
	return (function () {
		console.log('SelectFrom module');
		/**
		 * @typedef {Object} jQueryObject
		 * @property {function} jQuery.draggable
		 *
		 */

		/**
		 * Displays a popup data selection window (layer block) with ajax content, and stores selection results into associated input fields
		 *
		 * @param {string} url - the url of the ajax data source
		 * @param {string} title - the title of the popup window
		 * @param {string|HTMLElement|object} field_id - the target field of returned id data (JQuery selector or object)
		 * @param {string|HTMLElement|object} field_name  - the target field of returned name data (JQuery selector or object)
		 * @param {boolean} modal - the popup div is modal
		 * @param {object} params - request parameters of the ajax call
		 * @param {object} options - other options as follows: (all options are optional)
		 *
		 * @param {CallableFunction} [options.after] - runs after popup close
		 * @param {string|HTMLElement|object} [options.field_act]: field to load the action value and submit its form
		 * @param {string|HTMLElement|object} [options.field_opt]: field to get more data from popup selection result
		 * @param {string} [options.act]: action value. Only together with field_act option
		 * @param {number} [options.top]: vertical position of the popup window (not used)
		 * @param {CallableFunction} [options.confirm]: text of confirmation
		 * @returns {void}
		 */
		const selectFrom = function (url, title, field_id, field_name, modal, params, options) {
			//console.log('selectFrom: '+url+'; title='+title);
			(function ($) {
				// create popup
				const id = popups.length;
				const $template = $('div#popup-template');
				if ($template.length === 0) console.log('Creating SelectFrom: div not found. Xsl include? ');
				const $popup = $template.clone(true, true).attr('id', 'popup_' + id);
				/** @member {HTMLElement} popup - the popup div object */
				const popup = $popup.get(0);
				popup.xid = id;
				popups[id] = popup;
				$template.after(popup);
				$popup.find('h2').html(title);
				$popup.draggable(); //.find('.popup-inner').resizable();
				popup.field_id = $(field_id);
				popup.field_name = $(field_name);
				if (options && options.field_opt) popup.field_opt = $(options.field_opt); // További érték
				popup.url = url;
				$popup.data('options', options);
				//console.log(options);

				/**
				 * create content with parameters
				 *
				 * @param {Object} params
				 */
				$popup.on('select-value', function (event, params) {
					let url = this.url;
					if (params && params.url) {
						url = params.url;
						delete params.url;
					}

					console.log('Calling', url, params);
					$('div#popup-content', this).html('<p>Kérem, várjon...</p>').show();
					//console.log('Requesting popup content...', params);
					// noinspection JSUnusedLocalSymbols
					$.ajax(url, {
						data: params, // params with same nam will override url params (appended at the end)
						dataType: 'xml',
						cache: false,
						context: this,
						success: function (data, textStatus, jqXHR) {
							// this=popup
							let $container = $('div#popup-container', this);
							if ($container.length === 0) $container = $('div#popup-content', this);
							const parent = $container.get(0).parentNode;
							$container.replaceWith($('div#popup-content', data));
							const $newContent = $('div#popup-content', parent);

							/* Initialize custom controls on loaded page */
							$('.init-controls', this).trigger('init', {target: $newContent.get(0)});
							captureSubmit(this);
						},
						error: function (jqXHR, textStatus, errorThrown) {
							console.log(textStatus + ';' + errorThrown);
							$('div#popup-content', this).html('<p>Ajax transaction error</p>');
						},
						complete: function () {
							$('div#popup-loader', this).hide();
						}
					});
				});

				/**
				 * A Popup visszaküld egy értéket a hívó űrlap megadott mezőibe
				 *
				 * @param id {int} -- Ez kerül az id mezőbe
				 * @param name {string} -- Ez kerül a name mezőbe
				 * @param opt {string} -- Ez kerül az opt mezőbe, ha van.
				 * @returns {boolean}
				 */
				$popup.on('result', function (event, id, name, opt) {
					if (options && options.confirm) if (!options.confirm(id, name)) return false;
					if (this.field_id) this.field_id.val(id).trigger('change');
					if (this.field_name && name) {
						this.field_name.val(name).data('lastvalue', name);
						this.field_name.trigger('selected');

					}
					if (this.field_opt && opt) this.field_opt.val(opt).trigger('change');
					//console.log('result=',this.field_id,id,this.field_name,name,this.field_opt,opt);
					$(this).hide();
					popups[this.xid] = void 0;
					//options = this.options;
					if (options) {
						if (options.after) {
							//console.log('after');
							options.after(this);
						}
						if (options.field_act && options.act) {
							//console.log(options);
							$(options.field_act).val(options.act).trigger('change');
							$(options.field_act).closest('form').submit();
						}
					}
					$(this).remove();
				});

				$popup.find('div#popup-loader').show();
				$popup.trigger('select-value', params);

				// show modal
				$popup.show();

				// pozicionálás
				if (options && options.top) {
					//$(popup).offset({'top': options.top, 'left': $(popup).offset().left});
					$popup.offset({
						'top': $(window).height() / 2 - $popup.height() / 2 + $(window).scrollTop(),
						'left': $(window).width() / 2 - $popup.width() / 2
					});
					//console.log('scrt=', $(window).scrollTop());
				}
				popup.scrollIntoView();

				if (modal) {
					$popup.on('click', function (e) {
						if (e.target.className === 'popup-curtain') popup.scrollIntoView();
					}).keydown(function (event) {
						//console.log('popup keyup');
						if (event.keyCode === 27) popupClose(this.firstChild);
					});
				}
			})(jQuery);
		};

		/* popup ablak hívásai: */

		// popup újratöltése más paraméterekkel
		const sf_select = function (a, params) {
			jQuery(a).parents('div.popup').trigger('select-value', params);
			return false;
		};

		// noinspection JSUnusedGlobalSymbols
		/**
		 * Popupból eredmény visszaadása, ablak becsuk
		 *
		 * @param {HTMLElement} a -- kezdenényező DOM objektum
		 * @param {int} id -- visszaadott azonosító
		 * @param {string} name  -- visszaadott név
		 * @param {string} opt -- visszaadott egyéb érték
		 * @returns {boolean}
		 */
		const sf_result = function (a, id, name, opt) {
			jQuery(a).parents('div.popup').trigger('result', [id, name, opt]);
			return false;
		};

		/* creates {var:value,...} object from [{name:var,value:val},...] array of objects */
		const createParams = function (aa) {
			const r = {};
			jQuery.each(aa, function (i, v) {
				r[v.name] = v.value
			});
			return r;
		};

		/* replaces submit events to ajax recall */
		const captureSubmit = function (content) {
			//console.log('captureSubmit:'+content.id);
			const $form = $('form', content);
			$form.addClass('captured');
			$form.submit(function (event) {
				//console.log('submit captured');
				const params = createParams($(this).serializeArray());
				event.preventDefault();
				return SelectFrom.sf_select(this, params);
			});
		};

		const popupClose = function (button) {
			const popup = $(button).parents('.closeable');
			const options = popup.data('options');
			popup.remove();
			if (options) {
				if (options.cancel) {
					options.cancel(popup.get(0));
				}
			}
		};

		/**
		 Adds selector buttons to inp field.
		 Adds empty button if not mandatory
		 Adds quicklist if field is not disabled

		 If the inp field's id is '<name1>_name', then hidden idfield's id must be '<name1>'
		 If the inp field's id is 'field_<name1>_name', then hidden idfield's id must be 'field_<name1>'

		 @param {HTMLInputElement} inp -- target input (the name input)
		 @param {object} opt

		 @param {string} [opt.source]: name of selector page, default 'dir'
		 @param {string} [opt.caption]: name of selector button, default 'Választ'
		 @param {string} [opt.title]: title of popup, default is inp.title
		 @param {string|boolean} [opt.search]: search based on text value (default true). Use 'false' text value to disable.
		 @param {object} [opt.options]: selector page options, default depends on source
		 @param {string} [opt.cancel_caption]: title of cancel button, default is X. inp.data-cancel-title overrides it.
		 */
		const addSelect = function (inp, opt) {
			$(inp).data('lastvalue', $(inp).val());
			let source = 'dir';
			let caption = 'Választ';
			let options = {f: 7};
			let title = inp.title;
			let cancel_caption = $(inp).data('select-cancel-caption');
			if (!cancel_caption) { // noinspection HtmlPresentationalElement
				cancel_caption = '<i class="fa fa-times" aria-hidden="true"></i>';
			}
			let search = true;
			if (typeof opt === 'object') {
				if (opt.source) source = opt.source;
				if (opt.caption) caption = opt.caption;
				if (opt.options) options = opt.options;
				if (opt.title) title = opt.title;
				if (opt.search === 'false') search = false;
				if (opt.cancel_caption) cancel_caption = opt.cancel_caption;
			}
			const user_f = $(inp).data('select-f');
			if (user_f) options.f = user_f;
			let name = inp.id.substringBefore('_');
			let $inpid;
			if (name !== inp.id) $inpid = $('input#' + name, inp.parentNode);
			if (name === 'field') {
				name = inp.id.substringAfter('_').substringBefore('_');
				$inpid = $('input#field_' + name, inp.parentNode);
			}
			if (!$inpid) {
				name = inp.id.replace(/_name$/, '');
				$inpid = $('input#' + name, inp.parentNode);
			}
			if (!$inpid) {
				console.log('Id field `input#' + name + '` not found.');
			}
			if ($(inp).hasClass('rule-mandatory') && $(inp).val() !== '' && $inpid.val() !== '') $(inp).addClass('ok');

			if (!$(inp).hasClass('rule-mandatory')) {
				const cancel_disabled = $inpid.val() === '' ? 'disabled' : '';
				$(inp).after('<span id="empty-' + name + '" class="button selectfrom-empty ' + cancel_disabled + '" title="Törlés, üres érték beállítása">' + cancel_caption + '</span>');
			}
			$(inp).after('<span id="select-' + name + '" class="button selectfrom-select" title="Másik érték választása">' + caption + '</span>');
			$(inp).attr('autocomplete', 'off');

			if (!inp.disabled) {
				//Kézzel gépelés hatása
				$(inp).keyup(function () {
					if (this.value === '') {
						$(this).removeClass('invalid').removeClass('ok');
						$inpid.val('');
					} else if (this.value !== $(this).data("lastvalue")) {
						$(this).addClass('invalid').removeClass('ok');
						$inpid.val('');
					} else {
						$(inp).addClass('ok').removeClass('invalid');
						$inpid.val($(this).data("lastid"));
					}
				});
			}

			// Választógomb
			$('#select-' + name, inp.parentNode).on('click', function () {

				// Megkezdett gépelés beküldése keresési értékként, ha éppen nem OK
				let value = $(inp).val();
				if ($(inp).hasClass('ok') || !search) value = ''; // Zöld érték nem keresési alap, vagy ha tiltott
				//console.log('select p='+value);
				const options1 = options;
				const offset = $(inp).offset();
				if (value !== '' && value !== $(inp).data("lastvalue")) {
					options1.p = value;
				}

				// Választóablak meghívása
				// noinspection JSCheckFunctionSignatures
				selectFrom('select_' + source/*+'.php'*/, title, $inpid, inp, true, options1, {
					after: function () {
						const value = $('input#' + name).val();
						if (value !== '') {
							$(inp).addClass('ok').removeClass('invalid');
							$('#empty-' + name, inp.parentNode).removeClass('disabled');
						}
						$('#original-' + name, inp.parentNode).removeClass('disabled');
						$('img#' + name + '_preview', inp.parentNode).each(function () {
							this.src = $(this).data('pattern').replace(/\$id/, value);
						});
						$('a#' + name + '_preview', inp.parentNode).each(function () {
							this.href = $(this).data('pattern').replace(/\$id/, value);
						});
						$(inp).change();
						$inpid.change();
					},
					top: parseInt(offset.top)
				})
			});

			// Törlőgomb
			$('#empty-' + name).click(function () {
				$inpid.val('').change();
				$(inp).val('').data('lastvalue', '').change();
				$('#empty-' + name, inp.parentNode).addClass('disabled');
				$('img#' + name + '_preview', inp.parentNode).each(function () {
					this.src = '';
				});
			});
		};

		/**
		 Adds selector buttons to a select2 field.
		 Adds empty button if not mandatory

		 @param {HTMLInputElement} inp -- target select
		 @param {object} opt

		 @param {string} [opt.source]: name of selector page, default 'dir'
		 @param {string} [opt.caption]: name of selector button, default 'Választ'
		 @param {string} [opt.title]: title of popup, default is inp.title
		 @param {string|boolean} [opt.search]: search based on text value (default true). Use 'false' text value to disable.
		 @param {object} [opt.options]: selector page options, default depends on source
		 @param {string} [opt.cancel_caption]: title of cancel button, default is X. inp.data-cancel-title overrides it.
		 */
		const addSelect2 = function (inp, opt) {
			if (inp.nodeName.toUpperCase() !== 'SELECT') throw "Target field must be a 'select', got " + inp.nodeName;

			$(inp).data('lastvalue', $(inp).val());
			let source = 'dir';
			let caption = 'Választ';
			let options = {f: 7};
			let title = inp.title;
			let cancel_caption = $(inp).data('select-cancel-caption');
			if (!cancel_caption) { // noinspection HtmlPresentationalElement
				cancel_caption = '<i class="fa fa-times" aria-hidden="true"></i>';
			}
			if (typeof opt === 'object') {
				if (opt.source) source = opt.source;
				if (opt.caption) caption = opt.caption;
				if (opt.options) options = opt.options;
				if (opt.title) title = opt.title;
				if (opt.cancel_caption) cancel_caption = opt.cancel_caption;
			}
			const user_f = $(inp).data('select-f');
			if (user_f) options.f = user_f;
			let name = inp.id.substringBefore('_');

			if ($(inp).hasClass('rule-mandatory') && $(inp).val() !== '') $(inp).addClass('ok');

			if (!$(inp).hasClass('rule-mandatory')) {
				const cancel_disabled = $(inp).val() === '' ? 'disabled' : '';
				$(inp).after('<span id="empty-' + name + '" class="button selectfrom-empty ' + cancel_disabled +
					'" title="Törlés, üres érték beállítása">' +
					cancel_caption +
					'</span>');
			}
			$(inp).after('<span id="select-' + name + '" class="button selectfrom-select" title="Másik érték választása">' +
				caption +
				'</span>');
			$(inp).attr('autocomplete', 'off');

			// Rejtett cél-input mezők beszúráse
			const field_id = $('<input type="hidden" />').insertAfter(inp);
			const field_name = $('<input type="hidden" />').insertAfter(inp);

			// Választógomb
			$('#select-' + name, inp.parentNode).on('click', function () {

				const options1 = options;
				const offset = $(inp).offset();

				// Választóablak meghívása
				// noinspection JSCheckFunctionSignatures
				selectFrom('select_' + source/*+'.php'*/, title, field_id, field_name, true, options1, {
					after: function () {
						const value = $(field_id).val();
						const name = $(field_name).val();
						let $option = $('option[value="' + value + '"]', inp);
						if (!$option.length) $option = $('<option/>').appendTo(inp);
						$option.attr('value', value).prop('selected', true).text(name);

						if (value !== '') {
							$(inp).addClass('ok').removeClass('invalid');
							$('#empty-' + name, inp.parentNode).removeClass('disabled');
						}
						$('#original-' + name, inp.parentNode).removeClass('disabled');
						$('img#' + name + '_preview', inp.parentNode).each(function () {
							this.src = $(this).data('pattern').replace(/\$id/, value);
						});
						$('a#' + name + '_preview', inp.parentNode).each(function () {
							this.href = $(this).data('pattern').replace(/\$id/, value);
						});
						$(inp).change();
					},
					top: parseInt(offset.top)
				})
			});

			// Törlőgomb
			$('#empty-' + name).click(function () {
				$inpid.val('').change();
				$(inp).val('').data('lastvalue', '').change();
				$('#empty-' + name, inp.parentNode).addClass('disabled');
				$('img#' + name + '_preview', inp.parentNode).each(function () {
					this.src = '';
				});
			});
		};

		const init = function () {
			if ($('#popup-template').length === 0) console.log('Missing SelectFrom.xsl incude or initialization.');
			$('#popup-template span.close').click(function () {
				SelectFrom.popupClose(this);
			});

			$('input.select-from').each(function () {
				console.log('input.select-from', this.id);
				SelectFrom.addSelect(this, {
					'source': $(this).data('source'),
					'options': $(this).data('options'),
					'caption': $(this).data('caption'),
					'title': $(this).data('title'),
					'cancel_caption': $(this).data('cancel_caption'),
					'search': $(this).data('search')
				});
			});

			/* mágikus nyomógombok rejtett mezőkkel, autosubmittel */
			$('span.select-from').click(function () {
				const target_id = $(this).data('target-id');	// selector a célmezőre, pl 'input#field_owner'
				const target_name = $(this).data('target-name'); // selector a cél-név-mezőre, pl 'input#field_owner_name'
				const source = $(this).data('source');
				const offset = $(target_name).offset();
				// noinspection JSCheckFunctionSignatures
				SelectFrom.selectFrom('select_' + source, $(this).data('title'), target_id, target_name, true, $(this).data('params'), {
					after: function () {
						//var value = $(target_id).val();
						$(target_name).change();
						$(target_id).change();
					},
					top: parseInt(offset.top),
					act: $(this).data('act'),
					field_act: $(this).data('field_act')
				})
			});

			// Initialize ajax-loaded popup contents
			$('body').on('init', '.init-controls', function (evt, data) {
				const target = data.target;
				const $target = $(target);
				if ($target.hasClass('initialized')) {
					return false;
				}
				$(target).addClass('initialized');
				$('.sf-result', target).on('click', function () {
					return SelectFrom.sf_result(this, $(this).data('id'), $(this).data('name'), $(this).data('opt'));
				});

				$('.sf-select', target).on('click', function () {
					return SelectFrom.sf_select(this, $(this).data('params'));
				});
				$('a.disabled', target).on('click', function () {
					return false;
				});
				UApp.getModules().forEach(function (module) {
					if (typeof module.init === 'function') {
						module.init(target);
					}
				});
			});

			// Popup return objects
			$('.select-from-result').on('click', function () {
				sf_result(this, $(this).data('id'), $(this).data('name'), $(this).data('opt'));
				return false;
			});
		};

		return {
			selectFrom: selectFrom,
			sf_select: sf_select,
			sf_result: sf_result,
			popupClose: popupClose,
			addSelect: addSelect,
			addSelect2: addSelect2,
			init: init,
		};
	})();
}
