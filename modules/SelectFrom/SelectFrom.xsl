<?xml version="1.0" encoding="UTF-8"?>
<!-- SelectFrom.xsl -->

<!--suppress XmlDefaultAttributeValue -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="xml" version="1.0" indent="yes"
	            doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	            doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	            omit-xml-declaration="yes"
	/>

	<!-- Declaration of module calss and callbacks-->
	<xsl:template match="module-form-rule" mode="classname"/>
	<xsl:template match="module-form-attribute" mode="comment"/>
	<xsl:template match="module-form-attribute" mode="help"/>

	<xsl:template match="xsl-call[@name='select-from']">
		<div id="popup-template" class="closeable popup" style="display:none; pointer-events:none;">
			<div class="popup-curtain" style="pointer-events:auto"/>
			<div id="popup-loader" style="pointer-events:auto"/>
			<div class="popup-inner init-controls select-from-inner" style="pointer-events:auto">
				<span class="button close">
					<i class="fa fa-close" aria-hidden="true"/>
				</span>
				<h2 id="popup-title"/>
				<h3 id="popup-subtitle"/>
				<form class="form-popup" action="{/data/@script}">
					<input id="edit_act" name="act" type="hidden" value="updateright"/>
					<div id="popup-container">
						<div style="width:400px;height:200px;">
							<p>...</p>
						</div>
					</div>
				</form>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="attribute[@ref!='' and @type='selectFrom']" mode="field" name="selectfrom-attribute-field">
		<xsl:comment>SelectFrom/selectfrom-attribute-field, <xsl:value-of select="@type"/>,
			<xsl:value-of select="@link"/>
		</xsl:comment>
		<xsl:variable name="formname" select="../@name"/>
		<xsl:variable name="model" select="../*[name()=current()/../@modelnode]"/>
		<xsl:variable name="value">
			<xsl:choose>
				<!-- Value given to attribute overrides model -->
				<xsl:when test="@value">
					<xsl:value-of select="@value"/>
				</xsl:when>
				<!-- Model's attribute with this name -->
				<xsl:when test="$model/@*[name()=current()/@name]">
					<xsl:value-of select="$model/@*[name()=current()/@name]"/>
				</xsl:when>
				<!-- Model's subnode with attribute name -->
				<xsl:otherwise>
					<xsl:value-of select="$model/*[name()=current()/@name]"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="namevalue1">
			<xsl:choose>
				<xsl:when test="@namevalue">
					<xsl:value-of select="@namevalue"/>
				</xsl:when>
				<!-- attribute/ref információk felhasználása -->
				<xsl:otherwise>
					<xsl:value-of select="$model/*[name()=current()/@ref]/@*[name()=current()/@refname]"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="namevalue">
			<xsl:value-of select="$namevalue1"/>
			<xsl:if test="$namevalue1=''">
				<xsl:value-of select="@emptyvalue"/>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="select">
			<xsl:if test="@refmodel and not(@readonly='true' or @readonly=1)">select-<xsl:value-of
					select="translate(@refmodel, $uppercase, $smallcase)"/>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="error">
			<xsl:if test="error">invalid</xsl:if>
		</xsl:variable>
		<xsl:variable name="readonly">
			<xsl:if test="not(@pretype!='') or @readonly='true' or @readonly=1">1</xsl:if>
		</xsl:variable>
		<xsl:variable name="style">
			<xsl:if test="@width">width:<xsl:value-of select="@width"/>
				<xsl:if test="not(contains(@width, '%'))">px</xsl:if>;
			</xsl:if>
			<xsl:if test="@height and ../@edit!=0">height:<xsl:value-of select="@height"/>px;
			</xsl:if>
			<xsl:if test="@height and ../@edit=0">max-height:<xsl:value-of select="@height"/>px;
			</xsl:if>
		</xsl:variable>

		<xsl:variable name="link">
			<xsl:choose>
				<xsl:when test="not(contains(@link, '{$value}'))">
					<xsl:value-of select="@link"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="substring-before(@link, '{$value}')"/>
					<xsl:value-of select="$value"/>
					<xsl:value-of select="substring-after(@link, '{$value}')"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:comment>$value=<xsl:value-of select="$value"/> $link=<xsl:value-of select="$link"/>
		</xsl:comment>

		<xsl:choose>
			<xsl:when test="../@edit=0 and @link">
				<div class="{@class}">
					<a href="{$link}">
						<xsl:value-of select="$namevalue"/>
					</a>
				</div>
			</xsl:when>
			<xsl:when test="../@edit=0">
				<div class="{@class}">
					<xsl:value-of select="$namevalue"/>
				</div>
			</xsl:when>
			<xsl:when test="(@readonly='true' or @readonly=1) and @link">
				<input type="hidden" id="field_{@name}_name" name="{$formname}[{@name}_name]" value="{$namevalue}"/>
				<div class="inline {@class}">
					<a href="{$link}">
						<xsl:value-of select="$namevalue"/>
					</a>
				</div>
				<xsl:apply-templates select="." mode="help"/>
				<xsl:apply-templates select="." mode="comment"/>
			</xsl:when>
			<xsl:when test="@source">
				<input type="hidden" id="field_{@name}" name="{$formname}[{@name}]" value="{$value}"/>
				<input id="field_{@name}_name" name="{$formname}[{@name}_name]" alt="{@title}"
				       class="select-from {@class} {$error}"
				       value="{$namevalue}" data-label="{@label}" autocomplete="off" data-source="{@source}">
					<xsl:if test="$readonly!=''">
						<xsl:attribute name="readonly">
							<xsl:value-of select="$readonly"/>
						</xsl:attribute>
						<xsl:attribute name="onChange">0;</xsl:attribute>
					</xsl:if>
					<xsl:if test="$style!=''">
						<xsl:attribute name="style">
							<xsl:value-of select="$style"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:if test="@maxlength!=''">
						<xsl:attribute name="maxlength">
							<xsl:value-of select="@maxlength"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:if test="@cancel!=''">
						<xsl:attribute name="data-select-cancel-caption">
							<xsl:value-of select="@cancel"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:if test="@options!=''">
						<xsl:attribute name="data-options">
							<xsl:value-of select="@options"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:if test="@caption!=''">
						<xsl:attribute name="data-caption">
							<xsl:value-of select="@caption"/>
						</xsl:attribute>
					</xsl:if>
				</input>
				<xsl:apply-templates select="." mode="help"/>
				<xsl:apply-templates select="." mode="comment"/>
			</xsl:when>
			<xsl:otherwise>
				<div class="input-group">
					<input type="hidden" id="field_{@name}" name="{$formname}[{@name}]" value="{$value}"/>
					<input id="field_{@name}_name" name="{$formname}[{@name}_name]" alt="{@title}"
					       class="selectfrom {@class} {$select} {$error}"
					       value="{$namevalue}" data-label="{@label}" autocomplete="off">
						<xsl:if test="$readonly!=''">
							<xsl:attribute name="readonly">
								<xsl:value-of select="$readonly"/>
							</xsl:attribute>
							<xsl:attribute name="onChange">0;</xsl:attribute>
						</xsl:if>
						<xsl:if test="$style!=''">
							<xsl:attribute name="style">
								<xsl:value-of select="$style"/>
							</xsl:attribute>
						</xsl:if>
						<xsl:if test="@data-user">
							<xsl:attribute name="data-user">
								<xsl:value-of select="@data-user"/>
							</xsl:attribute>
						</xsl:if>
						<xsl:if test="@data-opt">
							<xsl:attribute name="data-opt">
								<xsl:value-of select="@data-opt"/>
							</xsl:attribute>
						</xsl:if>
						<xsl:if test="@size!=''">
							<xsl:attribute name="size">
								<xsl:value-of select="@size"/>
							</xsl:attribute>
						</xsl:if>
						<xsl:if test="@maxlength!=''">
							<xsl:attribute name="maxlength">
								<xsl:value-of select="@maxlength"/>
							</xsl:attribute>
						</xsl:if>
						<xsl:if test="@cancel!=''">
							<xsl:attribute name="data-select-cancel-caption">
								<xsl:value-of select="@cancel"/>
							</xsl:attribute>
						</xsl:if>
						<xsl:if test="@select-f!=''">
							<xsl:attribute name="data-select-f">
								<xsl:value-of select="@select-f"/>
							</xsl:attribute>
						</xsl:if>
						<!-- <xsl:apply-templates select="rule" mode="data" /> -->
					</input>
					<xsl:if test="@link">
						<a id="link-{@name}" class="button " title="A hivatkozott rekordra ugrik: {$namevalue}"
						   target="_blank" href="{$link}">
							<i class="fa fa-link" aria-hidden="true"/>
						</a>
					</xsl:if>
					<xsl:apply-templates select="." mode="help"/>
				</div>
				<xsl:apply-templates select="." mode="comment"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
