selectFrom modul
====================

## 1. A modul célja

Popup választó, külső forrásból.
A cél, hogy egy űrlap mezőjének értékét egy popup listából lehessen kiválasztani, melyet egy másik weblap állít elő.
Több mező egyidejű kezelésére is képes, tipikus használat, hogy egy van egy rejtett id mező, és egy látható név mező.
Az id mezőbe kerül a kiválasztott objektum id-je, ez a választás valódi tétje, de a kiválasztás név alapján történik,
és a név a látható név mezőbe kerül. A név mező célszerűen readonly, de lehet "disabled" is, hiszen a tényleges
elküldése általában nem szükséges.

## 2. Használata

### 2.1. php oldalon

Az oldal inicializálásában (preConstruct metódus) elhelyezzük a modul regisztrációját:
$this->register('SelectFrom', true);
Ennek hatására az outputba bekerül az xsl tartalma, mely tartalmazza a popup réteg template-jét.

### 2.2. Hagyományos hívás xsl oldalon

Az xsl include-jai közé fel kell venni modul xsl-jét. (Az újabb UApp verzióban ez a regisztráció alapján automatikus.)
<xsl:include href="../../UApp/modules/SelectFrom/SelectFrom.xsl" />

(a) a kívánt input mező mellett elhelyezünk egy nyomógombot, melynek megnyomása indítja a választást, a js részben
leírtak szerint.
(b) Az input mező select-from class alapján kapja meg a nyomógombot. Beállítások:

- data-source tartalmazza a selector oldal nevét ("select<Source>Page")
- data-caption: name of selector button, default 'Választ'
- data-title: title of popup, default is inp.title
- data-search: search based on text value (defult true). Use 'false' text value to disable.
- data-options: selector page options, default depends on source
- data-cancel_caption: title of cancel button, default is X. inp.data-cancel-title overrides it.

### 2.3 js oldalon

A nyomógomb vagy más esemény meghívja a

	SelectFrom.selectFrom(url, title, field_id, field_name, modal, params, options)

függvényt, mely felhozza az url-ben megadott tartalmat a popup-ban, majd kiválasztáskor az eredményt a megfelelő mezőbe
teszi.
Lásd még a függvény részletes dokumentációját a SelectFrom.js-ben.

3 A külső forrás előírásai
--------------------------

Az előállított oldal érdemi tartalmának <div id="popup-content"> mezőben kell lennie. Ami ebben van, az jelenik meg a
popup ablakban.
A tételsorain kattintásra a következő hívások valamelyikének kell lefutnia:

(a) SelectFrom.sf_result(this, id, name, opt)
Ennek hatására megtörténik a kiválasztás, a popup bezárul, az id, name és opt értékek visszatöltődnek a hívó űrlapba,
és megtörténik az after esemény, vagy a submit, ha kérték.

Valójában ez az egyetlen, amit tényleg kódolni kell a hívott lapon, a következő két hívást csak a teljesség és a
megértés kedvéért dokumentálom,
de azok alkalmazása automatikus.

(b) SelectFrom.sf_select(this, params)
Ennek hatására a popup újratöltődik a megadott url-ről, a params objektumban megadott új GET paraméterekkel.
Lapozós listáknál ez roppant hasznos, ámde mivel a lapozós lista a listaűrlap submit()-ján alapul, ezt a hívást nem kell
kézzel megcsinálni,
mert a belső submit()-ok automatikusan SelectFrom.sf_select() hívásokra fordulnak a popup betöltésekor!

(c) SelectFrom.popupClose(this)
Ennek hatására érték kiválasztása nélkül bezárul a popup, és a submit esemény nem történik meg.
Az after viszont igen, de ott vizsgálható, hogy id nem került visszaadásra.
Üres érték kiválasztását (tehát a meglévő érték törlését) viszont nem ezzel kell végezni, hanem a hívó űrlap saját
gombjával,
melyet ez a modul nem tartalmaz. A popup ablak egyébként gyárilag tartalmaz egy [X] bezár gombot, így ilyet elhelyezni
valójában nem szükséges.

## 4. Példák

### 4.1 Egyszerű példa

<input type="hidden" id="notify" name="notify" />
<input type="text" id="notify_name" name="notify_name" disabled="disabled" />
<span id="select-notify" class="button" title="Másik érték választása">Választ</span>
<span id="empty-notify" class="button" title="Törlés, üres érték beállítása">X</span>

$('input#select-notify').click(function(){
SelectFrom.selectFrom('select_dir', 'Válasszon értesítendő személyt', $('input#notify'), $('input#notify_name'),
true, {})
});

### 4.2 Választás után submit

Van arra is lehetőség, hogy a sikeres felhasználó választás után a hívó mező elküldésre kerüljön. Ehhez:

- meg kell adni a options.field_act mezőt (jellemzően rejtett mező), ahová beíródik az options.act érték (statikus)
- meg kell adni az options.act-ban is egy nemüres értéket.

## 5. Hívás Form-ból

Ha a `Form` modullal együtt használjuk, a deklarációk php oldalon egyszerűen elintézhetőek:

$form->createNode() hívás `$formoptions` paraméterében a választó mező definíciójában minden szükséges paramétert be
lehet állítani, és nem kell egyedi xsl megoldást csinálni.

    'service_id' => [
        'type'=>'selectFrom',
        'class'=>'quicklist prefetch',
        'pretype'=>1,
        'source'=>'samlres',
        'options' => [
            'f' => 17,
            'u' => $this->user->id,
        ],    
    ],

Ahol `class` értéke az input mező `class` attributumát állítja be. Ennek speciális értékei:

- quicklist: quicklist (egyedi) modul értelmezi, ha van
- prefetch: quicklist (egyedi) modul értelmezi, ha van

További paraméterek:

- **`pretype`** értéke a mező előgépelhetőségét állítja be. A begépelt érték a select hívás p paramétere lesz.
- **`source`**: ha meg van adva, a refmodel helyett ez a forrás lesz használva, és az app.js nincs használatban.
- **`options`** értéke: (tömb vagy JSON string): A select hívás paraméterei lesznek.
- **`caption`**: nyomógomb felirata
- **`cancel`**: cancel gomb felirata

Ha source nem szerepel (legacy mode), az űrlapmező refmodel értékéből lesz egy `class="select-refmodel"` érték, mely
triggereli az alkalmazás
`app.js` megfelelő eljárását, ez állítja össze a selectFrom paramétereit és meghívását. Lásd `addSelect` függvény
a `SelectFrom.js`-ben.

Feldolgozási sorrend:

1. SelectFrom.xsl (`match="attribute[@ref!='' and @type='selectFrom']" mode="field"`)
2. SelectFrom.js (`$('input.select-from')`)
3. app.js (`$('input.select-xxx')` ha source nincs megadva)
