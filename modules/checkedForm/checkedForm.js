/*
	checkedForm.js
    (c)uhi 2014-2017

    add alt="fieldname" to name inputs for error messages

    add class="checkedform" to form,
    add class="mandatory" to mandatory inputs
    add class="xxx" to check input formats:
    	telnum
    	telnum2
    	email
    	date
    	mac
    	ip
    	iprange
    	integer
    	dnsname
    	relativename
    	relativenames
    	absolutename
    	url -- RE from https://gist.github.com/dperini/729294

    Multiple rules may be declared, in this case one of them must be match.
    Mandatory must always match independently. Empty values in non-mandatory fields pass always.

    use form.submit() to submit form
    use form.cancelForm() to restore original values

    Customize:

    form.checkform -- custom check (displaying the field error and coloring the field is job of the custom function)
    form.rules -- custom rules combined with original

	Rules are hash of
		classname: [ruleset, message]

	Ruleset must be array of RE-s. All RE-s must be match for a rule.
	Ruleset may be a plain string. It will mean BNF rule, but is not yet implemented.

	@deprecated use Form module
*/

/**
 * @type {{date: [[RegExp], string], relativename: [RegExp[], string], dnsnames: [[RegExp], string], inet: [RegExp], inetr: [[RegExp], string], ip: [RegExp], integer: [[RegExp], string], relativenames: [[RegExp], string], mandatory: [[], string], mac: [[RegExp], string], url: [[RegExp], string], absolutename: [RegExp[], string], telnum2: [[RegExp], string], iprange: [[RegExp], string], dnsname: [RegExp[], string], email: [[RegExp], string], telnum: [[RegExp], string]}}
 */
const checkedformRules = {
	//'custom': [customcheck, 'egyedi hiba'],
	'mandatory': [[], 'kötelező mező'],
	'telnum': [[/^\+(\d-?){8,15}$/], 'érvénytelen telefonszám. Csak +-szal kezdődő nemzetközi szám lehet.'], 				// +-szal kezdődő teljes nemzetközi szám
	'telnum2': [[/^((\+\d[0-9-]{8,17})|(\d{5}))$/], 'érvénytelen telefonszám. Nemzetközi szám vagy ötjegyű mellék lehet.'],	// +-szal kezdődő teljes nemzetközi szám vagy egyetemi mellék
	'email': [[/^\w+[\w\-.]*@\w+[\w\-.]+$/], 'érvénytelen e-mail cím'],
	'date': [[/^\d{4}\.\d{2}\.\d{2}\.?$/], 'érvénytelen dátum. Formátum: 1999.12.31'],
	'mac': [[/^[\dA-Fa-f]{2}[:.-]?[\dA-Fa-f]{2}[:.-]?[\dA-Fa-f]{2}[:.-]?[\dA-Fa-f]{2}[:.-]?[\dA-Fa-f]{2}[:.-]?[\dA-Fa-f]{2}$/], 'érvénytelen MAC. Szeperátorként : . - használható'],
	'ip': [/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/],
	'iprange': [[/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,2}$/], 'Érvénytelen ip tartomány. Formátum: 1.2.3.0/24'],
	'inet': [/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$|^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$|^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:)(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/],
	'inetr': [[/^((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])|(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])|\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:)(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*)\/\d{1,2}$/], 'Érvénytelen ip tartomány. Formátum: 1.2.3.0/24 vagy 1111:FFFF::0000/112'],
	'integer': [[/^[0-9]*$/], 'érvénytelen egész szám]'],
	'dnsname': [[/^(((([_a-z\d][-_a-z\d]*)|\*)(\.([_a-z\d][-_a-z\d]*))*\.?)|@|\*)$/, /^.{1,253}$/, /^[^.]{1,63}(\.[^.]{1,63})*\.?$/], 'érvénytelen név'], // Lehet relatív és abszolút is, @ is.
	'relativename': [[/^((([_a-z\d](-*[_a-z\d])*)(\.([_a-z\d](-*[_a-z\d])*))*)|@|\*)$/, /^.{1,253}$/, /^[^.]{1,63}(\.[^.]{1,63})*$/], 'érvénytelen relatív név'],
	'absolutename': [[/^(([_a-z\d](-*[_a-z\d])*)|\*)(\.([_a-z\d](-*[_a-z\d])*))*\.$/, /^.{1,253}$/, /^[^.]{1,63}(\.[^.]{1,63})*\.$/], 'érvénytelen abszolút név'],
	'relativenames': [[/^((((([_a-z\d][-_a-z\d]*)|\*)(\.[_a-z\d][-_a-z\d]*)*)|@|\*)((\s*;\s*)(([_a-z\d][-_a-z\d]*)(\.[_a-z\d][-_a-z\d]*)*))*)$/], 'érvénytelen relatív név vagy nevek'],
	'dnsnames': [[/^((((([_a-z\d][-_a-z\d]*)|\*)((\.[_a-z\d][-_a-z\d]*)+\.?)?)|@|\*)((\s*;\s*)(([_a-z\d][-_a-z\d]*)((\.[_a-z\d][-_a-z\d]*)+\.?)?))*)$/], 'érvénytelen név vagy nevek'],
	'url': [[/^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i], 'érvénytelen URL']
};

if (typeof (String.prototype.trim) === "undefined") {
	String.prototype.trim = function () {
		return String(this).replace(/^\s+|\s+$/g, '');
	};
}

// noinspection JSUnusedGlobalSymbols
$.fn.extend({
	submitCheck: function () {
		const form = this.get(0);
		//console.log(['form', form]);
		if (form.checkform) if (!form.checkform()) return false;    // custom check if exists
		if (checkform(form)) form.submit();
		return false;
	},
	cancelForm: function (url) {
		let changed = false;
		//console.log('cancelForm: '+this.get(0).name);
		$('input, select', this).each(function () {
			//console.log('cancel input: '+this.name + ', "' + $(this).data('originalValue') + '", "'+$(this).val()+'"');
			if ($(this).val() !== $(this).data('originalValue')) changed = true;
		});
		const conf = !changed || confirm('Biztosan elveti a változásokat?');
		if (conf) {
			location = url;
		}
		return false;
	},
	/**
	 * True, if the item is in a disabled block
	 */
	isHidden: function () {
		return this.closest('.hidden').length > 0;
	}
});
$(function () {
	const $form = $('form.checkedform');
	// noinspection JSUnusedLocalSymbols
	$form.on('submit', function (evt) {
		console.log('checkedform submit.');
		if (this.checkform) if (!this.checkform()) return false;    // custom check if exists
		return checkform(this);
	});
	$('input, select', $form).each(function () {
		$(this).data('originalValue', $(this).val());

		$(this).on('checkall', function () {
			// hidden blokkban lévő inputok kihagyása
			if ($(this).isHidden()) return;

			const $form = $(this).closest('form');
			//console.log('checkall', this.name);
			const field = this;
			const m = testmandatory(this);
			if ($(this).is('.preselect')) {
				// Csak mandatory ellenőrzés, és csak hibára
				if (!m) {
					//console.log('checkall', 'empty');
					$(this).removeClass('ok').removeClass('invalid');
					$(this).parent('.field').removeClass('ok').removeClass('invalid');
				}
				return;
			}
			let f = true;   // Volt-e érvénytelen szabály (false, ha volt)
			let ff = false; // Volt-e érvényes szabály (true, ha volt)

			// Szabálykészlet összefésülése
			const rules = checkedformRules;
			const customRules = $form.data('rules');
			if (customRules) for (let r in customRules) if (customRules.hasOwnProperty(r)) rules[r] = customRules[r];

			//console.log(rules);
			let f1;
			for (let c in rules) {
				const name = $(this).attr('name');
				let v = $(this).attr('type') === 'radio' ?
					$('input:radio[name=' + name + ']:checked', $(this).parents('form')).val() :
					$(this).val() ? $(this).val().trim() : $(this).val();
				if (v === undefined) v = '';
				if (c !== 'mandatory' && $(this).hasClass(c)) {
					//console.log('rule ', c);
					const rule = rules[c][0];
					f1 = testall(rules, field, rule, v);
					f = f && f1;
					ff = ff || (f1 && (f1 !== -1));
				}
			}

			// Kiértékelés
			if (!m) {
				//console.log('checkall', 'empty');
				$(this).removeClass('ok').removeClass('invalid');
				$(this).parent('.field').removeClass('ok').removeClass('invalid');
			} else if (!f && !ff) { // Volt érvénytelen szabály, és nem volt érvényes
				//console.log('checkall', 'bad');
				$(this).removeClass('ok').addClass('invalid');
				$(this).parent('.field').removeClass('ok').addClass('invalid');
			} else { // Volt érvényes szabály, vagy nem volt egyáltalán szabály
				$(this).removeClass('invalid').addClass('ok');
				$(this).parent('.field').removeClass('invalid').addClass('ok');
			}
		});

		$(this).change(function () {
			$(this).trigger('checkall');
		});
		// Gépelés közben késleltetett ellenőrzés
		$(this).keyup(function () {
			const $input = $(this);
			const old = $input.data('timer');
			if (old) clearTimeout(old);
			const timer = setTimeout(function () {
				$input.trigger('checkall');
			}, 1000);
			$input.data('timer', timer);
		});
	});

	/*
		$('input.mandatory', $('form.checkedform')).each(function() {
			if(this.value!='') { $(this).addClass('ok'); } // Ez először a *_name mezőket is!
			$(this).trigger('checkmandatory');
			$(this).on('checkmandatory', function() {
				var name = $(this).attr('name');
				var f = testmandatory(this);
				if(!f) { $(this).removeClass('ok'); $(this).parent('.field').removeClass('ok'); }
				if(f && !this.id.match('_name')) {
					$(this).addClass('ok');
					$(this).parent('.field').addClass('ok');
					if($(this).attr('type')=='radio')  $('input:radio[name='+name+']', $(this).parents('form')).parent('.field').addClass('ok');
				}
			});
			$(this).keyup(function() {$(this).trigger('checkmandatory');});
			$(this).click(function() {$(this).trigger('checkmandatory');});
			$(this).blur(function() {$(this).trigger('checkmandatory');});
			$(this).focus(function() {$(this).trigger('checkmandatory');});
		});

		$('select.mandatory', $('form.checkedform')).each(function() {
			var emptyvalue = $(this).data('emptyvalue');
			var val = $(this).val();
			if(val!='' && val!=emptyvalue && val!='_undefined_') { $(this).addClass('ok'); }
			$(this).trigger('checkmandatory');
			$(this).on('checkmandatory', function() {
				var name = $(this).attr('name');
				var f = testmandatory(this);
				if(!f) { $(this).removeClass('ok'); $(this).parent('.field').removeClass('ok'); }
				if(f && !this.id.match('_name')) {
					$(this).addClass('ok');
					$(this).parent('.field').addClass('ok');
				}
			});
			$(this).click(function() {$(this).trigger('checkmandatory');});
			$(this).blur(function() {$(this).trigger('checkmandatory');});
			$(this).focus(function() {$(this).trigger('checkmandatory');});
		});
	*/
	$('input.datepicker').datepicker({
		dateFormat: 'yy.mm.dd',
		constrainInput: true,
		dayNamesMin: ['V', 'H', 'K', 'Sz', 'Cs', 'P', 'Sz'],
		firstDay: 1,
		monthNamesShort: ["Jan", "Febr", "Márc", "Ápr", "Máj", "Jún", "Júl", "Aug", "Szept", "Okt", "Nov", "Dec"],
		monthNames: ["Január", "Február", "Március", "Április", "Május", "Június", "Július", "Augusztus", "Szeptember", "Október", "November", "December"],
		showMonthAfterYear: true,
		gotoCurrent: true,
		showAnim: 'clip',
		buttonText: 'választ',
		showOn: "button",
		buttonImage: "img/calendar24.png"
	}).trigger('userInit'); // Mivel az előbb fut le
});

function checkform(form) {
	let r;
// Szabálykészlet összefésülése
	const rules = checkedformRules;
	const customRules = $(form).data('rules');
	if (customRules) for (r in customRules) if (customRules.hasOwnProperty(r)) rules[r] = customRules[r];

	r = true;

	const es = $('input, select', form);
	// Minden mezőt
	es.each(function () {
		if ($(this).isHidden()) return;

		let msg;
		const field = this;
		if (this.disabled) return;

		let m = true;	// Mandatory ellenőrzés eredménye
		let f = true; 	// Amíg nincs hibás szabály, addig true
		let ff = false; // Az első jó szabálynál (mandatoryt kivéve) true lesz

		const title = $(this).data('label') ? $(this).data('label') : this.name;
		let v = undefined;
		const xtype = this.nodeName.toLowerCase() + '/' + (this.type ? this.type : '');
		const $form = $(this).closest('form');
		switch (xtype) {
			case 'input/checkbox':
				v = $('input[name=' + this.name + ']:checked', $form);
				break;
			case 'input/radio':
				v = $('input[name=' + this.name + ']:checked', $form);
				break;
			case 'select/':
				v = $('option:selected', this).text();
				break;
			default:
				v = $(this).val();
		}
		if (v === undefined) v = '';
		if (typeof v === 'string') v = v.trim();

		// v érték ellenőrzése minden szabályra
		for (let c in rules) {
			if (!$(field).hasClass(c)) continue;
			//var msg = 'Ismeretlen hiba ('+c+')';
			// Egy mező ellenőrzése egy szabályra
			if (c === 'mandatory') {
				const emptyvalue = $(field).data('emptyvalue');
				if (v === '' || v === '_undefined_' || v === emptyvalue) {
					m = false;
					console.log(field, v, emptyvalue);
					msg = 'kötelező érték';
				}
			} else if ((v !== '') && (!testall(rules, field, rules[c][0], v))) {
				msg = rules[c][1] ? rules[c][1] : 'formátuma nem megfelelő (' + c + ')';
				// TODO: egyedi hibaüzenet kiolvasása a mezőből
				f = false;
			} else {
				//var msg = '`'+v+'` értéke nem felel meg a '+c+' szabálynak';
				ff = true;
			}
		}

		// Eredmény kezelése
		if ((!f && !ff) || !m) {
			// Mező színezése minden hibánál
			$(this).css("background-color", '#fff').delay(500).css("background-color", '#fee');
			// Üzenet és mezőkijelölés csak ez elsőnél
			if (r) {
				alert(title + ' ' + msg);
				this.focus();
			}
			r = false;
		}
	});
	return r;
}

/*
	Mező egyedi ellenőrzése
	on(checkfield) eseménnyel
*/

// noinspection JSUnusedGlobalSymbols
function customcheck(field) {
	// TODO: on(checkfield) futtatása
	console.log('Egyedi ellenőrzés nincs kész.', field);
	return true;
}

/*
	Kötelezőség ellenőrzése.
	Ha a mező kötelező és értéke üres, az eredmény hamis.
*/
function testmandatory(input) {
	const name = $(input).attr('name');
	const emptyvalue = $(this).data('emptyvalue');
	let val = $(input).attr('type') === 'radio' ? $('input:radio[name=' + name + ']:checked', $(input).parents('form')).val() : $(input).val();
	//console.log('checkform-testmandatory', input, val);
	val = val ? val.trim() : '';
	return !(val === '' || val === emptyvalue || val === '_undefined_');

}

/*
	Ellenőriz egy mezőt vagy értéket egy szabály alapján (tömb minden re eleme)
	@param rules - teljes szabálykészlet a rekurzióhoz
	@param field - mező vagy null (ekkor csak értéket ellenőriz)
	@param rule - szabály:
		tömb: (a) minden eleme egy RE, egy másik szabály neve %-kal, vagy egy függvény(értékre).
			(b) ABNF stringek is (% kezdet nélkül)
			(c) függvény(mezőre)
		function: futtatja
		egyéb: -- N/A
	@parem v - ellenőrizendő érték. Ha nincs megadva, a mező értéke
	@return boolean
*/
function testall(rules, field, rule, v) {
	//console.log('testall', rule, v);
	let msg = '';
	if (typeof v === 'undefined' && v === null) {
		console.log('Nincs mező és érték');
		return false;
	}
	if (typeof v === 'undefined') v = $(field).val();
	if (typeof rule === 'function') {
		console.log(v + ' function ');
		// függvény futtatása. hiba esetén false
		if (field === null) {
			console.log('Nincs a függvényhez mező.');
			return false;
		}
		msg = rule(field);
		const f = (msg === '');
		if (!f) {
			$(field).data('error', msg);
			console.log('testall/fn failed', field.name, v);
		}
		return f;
	}
	// Tömb minden elemének teljesülnie kell
	if (typeof rule === 'object') {
		for (let r = 0; r < rule.length; r++) {
			const rr = rule[r];
			if (typeof rr === 'function') {
				const msg1 = rr(v);
				const f1 = (msg1 === true || msg1 === '');
				if (!f1) {
					$(field).data('error', msg);
					console.log('testall/fn failed', field.name, v, rr);
					return false;
				}
			} else if ((typeof rr) === 'string' && rr.substr(0, 1) === '%') {
				const name = rr.substr(1);
				if (!testall(rules, field, rules[name][0], v)) return false;
			} else {
				const re = new RegExp(rr);
				//console.log('rule', re);
				if (!re.test(v)) {
					console.log('testall/re* failed', field, v, re);
					return false;
				}
			}
		}
		return true;
	}
	return testexp(rule, rule, v);
}

/*
	Checks a value against an ABNF rule.
	ABNF rule may contain:
	( 	group
	* 	optional: 0 or more
	*1 	optional one: 0 or 1
	1* 	one or more
	2*5 two..five
	4 	exactly 4
	[ 	optional (0 or 1)
	" 	terminal symbol (literal)
	| 	or
*/
function testexp(rules, rule, v) {
	console.log('ABNF', rule, v);
	return -1;
}
