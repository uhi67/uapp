<?php /** @noinspection PhpUnused */
/**
 * UList module
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright 2014-2017
 * @license MIT
 * @package modules
 */

/**
 * # UList - the pageable, filterable, orderable list widget
 *
 * ## Simple example:
 * ### In Controller
 *
 * ```php
 * $sql1 = $sql1 ? $sql1 : "select count(*) from ($sql) x";
 * $count = $db->selectvalue($sql1, $params);
 * $list = new UList($listname, $itemname, $count, $orders, 0);
 * if($withSearch) $list->setSearch($search);
 * $node_list = $list->createNode($node_parent);
 * $sql = $list->sqlTail($sql, 0, $exp);
 * $node_parent->query($db, $itemname, $sql, $params);
 * ```
 *
 * ### In XSL view
 *
 * ```XML
 * <xsl:apply-templates select="list[@name='listanév']">       <!-- (névvel, ha több lista is van) -->
 *   <xsl:with-param name="spacing" select="2" />
 * </xsl:apply-templates>
 *
 * <xsl:template match="elemnév" mode="list-item">
 *   <tr>
 *     <td>cella-1</td>...
 *   </tr>
 * </xsl:template>
 *
 * <xsl:apply-templates select="list[@name='listanév']" mode="list-empty">       <!-- (névvel, ha több lista is van) -->
 *   Üres lista
 * </xsl:apply-templates>
 * ```
 *
 * ### XSL callbacks
 * - `<template match="@item-name" mode="list-item">`
 * - `<xsl:template match="list" mode="in-form">`        - user tartalom (pl. input hidden) - a form-ban, de listablokk előtt
 * - `<xsl:template match="list" mode="views">`            - user vezérlők listablokkba, navigátor elé
 * - `<xsl:template match="list" mode="filter">`        - user vezérlők listablokkba, navigátor elé
 * - `<xsl:template match="list" mode="options">`        - navigátorba user vezérlők
 * - `<xsl:template match="list" mode="before-table">`    - table előtt, de navigátor után
 * - `<xsl:template match="list" mode="list-start">`    - table-ban a tbody előtt
 * - `<xsl:template match="list" mode="list-first">`    - table-ban az első sor előtt
 * - `<xsl:template match="list" mode="list-last">`        - table-ban az utolsó sor után
 * - `<xsl:template match="list" mode="list-end">`        - table-ban a tbody után
 * - `<xsl:template match="list" mode="list-empty"/>`    - table után, ha nincs eleme, a kiírt szöveg
 * - `<xsl:template match="list" mode="end-list">`        - table után, alsó navigátor előtt
 * - `<xsl:template match="list" mode="end-form">`        - alsó navigátor után, listablokk után, form vége előtt
 * - `<xsl:template match="list" mode="after-form">`    - form után
 *
 * {@see __construct() }
 *
 * @uses Menu
 * @property mixed orderName
 */
class UList extends Xlet {
    /** @var string $id -- Unique identifier of this list instance including all parameters (for caching, etc. Include username if matters) */
    public $id = 'list1';
    /** @var string $name -- Name of this kind of list in the application */
    public $name = 'list';
    /** @var string $item_name -- Name of item tags to display, default is data source model name or 'item' */
    public $item_name = null;
    /** @var int $count -- Number of all items in the data source */
    public $count = 0;
    /** @var string[] $orders -- Numeric indexed array of all available orders for column-dependent ordering (usually alternating asc and desc variants) */
    public $orders = [];
    /** @var int $order -- index of current order (set the default, but may change) */
    public $order = 0;
    /** @var array[] $attributes -- Other attributes to add to node (UList does not use them, except columns) */
    public $attributes;
    /** @var array[] $columns -- array of column definitions: {field/name,title,ord1,ord2,width1,width2,limit,class,headerClass} */
    public $columns;
    /** @var array[] $headerfields -- fieldname-indexed array of field definitions displayed in the list header {caption,type,value} */
    public $headerfields;
    /** @var mixed[] $variables -- name=>value pairs to create hidden inputs in the list form */
    public $variables = [];
    /** @var DataSourceInterface|QueryDataSource $dataSource -- data source if provided */
    public $dataSource;
    /** @var string $withSearch -- use search with dataSource */
    public $withSearch;
    /** @var array $modelOptions -- item node creation options if data source is given */
    public $modelOptions;
    /** @var bool $paging -- set to false to switch off paging (default is on) */
    public $paging = true;
    /** @var Menu $menu -- menu connected to the list */
    public $menu;
    /** @var string|BaseModel -- model classname from data source or specified explicitly */
    public $modelClass;

    /** @var int $_userPageLength -- user page length preference -- @see $userPageLength property */
    static $_userPageLength;

    /** @var bool $req_list -- True, if this list has been submitted */
    private $req_list = false;
    /** @var int $page_show -- Number of current page */
    private $page_show;
    /** @var int $page_length -- Number of rows on a page */
    private $page_length;
    /** @var int $page_count -- Number of pages */
    private $page_count;
    /** @var bool $with_search -- Display search input (default is false) */
    private $with_search = false;
    /** @var string $search -- Search pattern */
    private $search;
    /** @var string $script -- Az oldalhoz rendelt script, ha ismer. A célja csak annyi, hogy az XML-be beleteszi. */
    private $script;
    /** @var array $scriptparams -- Lapozáskor az url-hez fűzendő paraméterek. {@see setScriptParam()} állítja be */
    private $scriptparams = [];

    /**
     * List module must be initialized
     *
     * @param UAppPage $page
     *
     * @return void
     * @throws ConfigurationException
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     */
    static function init($page) {
        $page->addXslCall('list-init', 1); // it must be called once at body start
    }

    /**
     * ## UList constructor
     *
     * Call schemas:
     * - New mode: ($page, $options)
     * - Legacy mode: ($listname, $nodename, $count, $orders, $deforder, $attributes)
     *
     * ### Options:
     * - string **id** -- unique identifier for cache (default is 'list1')
     * - string **name** -- common name for list type (e.g. use paging) (default is 'list')
     * - string **item_name** -- tagname of items (default is 'item')
     * - int **count** -- number of elements - must be calculated before using page data (default is 0)
     * - array **orders** -- numeric indexed array of orders for sql order (default is empty)
     * - int **order** -- default index for order array (default is 0)
     * - array **columns** -- array of {name,title,ord1,ord2,width1,width2,limit}. **Shortcuts:**
     *      - field = array(modelclass, fieldname, [max-label-length]) or 'Model:fieldname' generates name (label) and title.
     *      - ord = true generates ord1, ord2 from name; ord=ord-def generates ord1, ord2 and corresponding orders elements (see {@see DBX::buildOrder()})
     *      - width = single value for both width1 and width2, or a two-element array for them
     *        null elements will be ignored.
     * - array **attributes** -- associative array of other attributes to add to list node:
     *        - url -- action of list form (default is page url)
     *        - popup -- 1 = called in ajax window: pagination will call SelectFrom.sf_select(this, params) instead of post
     *        - width -- width of list table in % or in pixel (simple number)
     *        - divclass -- custom class for containing div
     *        - formclass -- custom class for the form element
     *        - class -- custom class for list table. "client" means client-side ordering // TODO: nem működik
     *        - columns -- 'name,title,ord1,ord2,width1,width2,limit|...|' (deprecated, use columns option)
     *        - selectall -- name of column where 'select all' checkbox appears in the header
     *        - with-navigator -- show navigator bar: 0=no (default), 1=above, 2=above and below
     *          - with-search -- (deprecated, see legacy list->setSearch() or new withSearch option)
     *        - empty -- text displayed when list is empty.
     *        - exp -- 1 = Enable CSV export (display a button resulting downloadable csv file. A separate name-export view must be defined)
     *        null elements will be ignored.
     * - array **headerfields** -- fieldname-indexed array of input field definitions displayed above the list header {caption,type,option,value}
     * - array **variables** -- name=>value pairs to generate hidden inputs into list form
     * - {@see DatasourceInterface} **dataSource** -- pageable data source for auto generating list data
     * - bool **withSearch** -- use search with dataSource (see {@see DatasourceInterface::setPattern})
     * - array **modelOptions** -- item node creation options if data source is given (see {@see BaseModel::createNode()})
     * - bool **paging** -- paging is enabled (default is true)
     * - Menu **menu** -- context menu attached to the list, may contain items active on selections
     *
     * @param UAppPage|string $page
     *        -- The page uses this widget
     *    *-- legacy mode: list name (also identifies cache entry)
     * @param array|string $options
     *        -- associative array, containing options
     *        *-- legacy mode:  nodename of items
     * @param int $count
     * @param array $orders
     * @param int $deforder
     * @param array $attributes
     *
     * @throws InternalException
     * @throws ConfigurationException
     */
    function __construct($page, $options, $count = 0, $orders = null, $deforder = 0, $attributes = null) {
        if ($page instanceof UAppPage || $page === null) {
            Util::assertArray($options);
            parent::__construct($page, $options);
        } // @codeCoverageIgnore
        else {
            // Legacy mode call (@deprecated)
            /** @var string $name -- list name (also identifies cache entry) */
            $name = $page;
            /** @var string $item_name -- tagname of items */
            $item_name = $options;

            $this->name = $name;
            $this->item_name = $item_name;
            $this->count = $count;
            $this->orders = $orders;
            $this->order = $deforder;
            $this->attributes = $attributes;
            $this->initParameters();
        }
    }

    /**
     * Initializes UList parameters based on http request.
     * Recalculates paging
     *
     * @throws InternalException
     * @throws ConfigurationException
     * @codeCoverageIgnore
     */
    private function initParameters() {
        $this->script = Util::getPath(0, '');
        $this->req_list = Util::getReq('list_name') == $this->name;
        $this->initOrders();
    }

    /**
     * @throws ConfigurationException
     * @throws InternalException
     */
    private function initOrders() {
        if (is_array($this->orders)) {
            $this->order = Util::getSession('list_order_' . $this->name, $this->order);
            if ($this->req_list && Util::getReq('list_order') != '') $this->order = Util::getReqInt('list_order');
            if ($this->req_list) $_SESSION['list_order_' . $this->name] = $this->order;
        }
        $this->createPageLength();
        $this->impose();
    }

    /**
     * Komponens attributumok beállítása utáni előkészítés
     *
     * @throws InternalException
     * @throws UAppException
     * @throws Exception
     */
    public function prepare() {
        parent::prepare();

        $this->script = Util::getPath(0, '');
        $this->req_list = Util::getReq('list_name') == $this->name;
        if ($this->req_list) {
            $_SESSION['list_length_' . $this->name] = $this->page_length;
            $_SESSION['page_length'] = $this->page_length; // Erre a sessionre mindig az utolsó lapbeállítás a default
            $this->search = Util::getReq('list_search');
            $_SESSION['list_search_' . $this->name] = $this->search;
        }

        if (is_array($this->columns)) $this->columns = array_filter($this->columns);
        if (is_array($this->attributes)) $this->attributes = array_filter($this->attributes);

        if ($this->dataSource) {
            if ($this->dataSource instanceof QueryDataSource && !$this->modelClass) {
                $this->modelClass = $this->dataSource->modelClass;
            }
            if ($this->withSearch) {
                $this->dataSource->setPattern($this->setSearch(), ['fieldName' => $this->fieldName()]);
            }
            $this->count = $this->dataSource->count;
        }

        if ($this->modelClass) {
            if ($this->modelOptions === null) $this->modelOptions = [];

            // ### Default értékek ###
            // nodeName --> item_name
            if (isset($this->modelOptions['nodeName']) && !$this->item_name)
                $this->item_name = $this->modelOptions['nodeName'];
            // modelClass --> item_name
            if (!$this->item_name && $this->modelClass) {
                $this->item_name = Util::uncamelize($this->modelClass, '-');
            }
            // item_name --> nodeName
            if (!isset($this->modelOptions['nodeName'])) {
                if ($this->item_name) $this->modelOptions['nodeName'] = $this->item_name;
            }
        }

        // Computes width1, width2 from width; ord1, ord2 from ord; name, title from field
        $db = ($this->dataSource instanceof QueryDataSource) ? $this->dataSource->query->connection : null;
        Debug::tracex('db', $db);
        if ($this->columns) {
            foreach ($this->columns as &$column) {
                $fieldName = '';
                if (isset($column['field'])) {
                    $field = $column['field'];
                    // Breaking up 'class:field' notation
                    if (is_string($field) && strpos($field, ':')) $field = explode(':', $field);
                    // Using datasource model for single fieldname
                    if (is_string($field) && $this->modelClass) $field = [$this->modelClass, $field];
                    if (is_array($field) && is_a($field[0], BaseModel::className(), true)) {
                        $className = $field[0];
                        $fieldName = $field[1];
                        $label = call_user_func([$className, 'attributeLabel'], $fieldName);
                        if (isset($field[2])) {
                            $labelLength = (int)$field[2];
                            if (mb_strlen($label) > $labelLength) $label = mb_substr($label, 0, $labelLength - 1) . '.';
                        }
                        if (!isset($column['name'])) $column['name'] = $label;
                        $column['title'] = call_user_func([$className, 'attributeHint'], $fieldName, 'hint') ?: null;
                    } else throw new Exception('Invalid field definition in List: ' . Util::objtostr($field));
                }
                if (isset($column['width'])) {
                    $width = $column['width'];
                    if (is_numeric($width)) {
                        $column['width1'] = (int)$width;
                        $column['width2'] = (int)$width;
                    } else if (is_array($width)) {
                        $column['width1'] = $width[0];
                        $column['width2'] = isset($width[1]) ? $width[1] : $width[0];
                    }
                }

                /** @noinspection PhpAssignmentInConditionInspection */
                if (isset($column['ord']) && !empty($ord = $column['ord'])) {
                    if ($ord === true && $fieldName) {
                        $column['ord1'] = [$fieldName];
                        $column['ord2'] = [[$fieldName, DBX::ORDER_DESC]];
                    } else if (is_array($ord) && is_int($ord[0])) {
                        $column['ord1'] = $ord[0];
                        $column['ord2'] = $ord[1];
                    } else if (is_array($ord)) {
                        $column['ord1'] = $ord;
                        $column['ord2'] = DBX::desc($ord);
                    } else if (!is_bool($ord)) {
                        $column['ord1'] = [$ord];
                        $column['ord2'] = DBX::desc([$ord]);
                    }
                }
                /** @noinspection PhpAssignmentInConditionInspection */
                if (isset($column['ord1']) && is_array($ord1 = $column['ord1'])) {
                    $this->orders[] = $db ? $db->buildOrderList($ord1) : $ord1;
                    if (!array_key_exists('ord2', $column)) $column['ord2'] = DBX::desc($ord1);
                    $column['ord1'] = count($this->orders) - 1;
                }
                /** @noinspection PhpAssignmentInConditionInspection */
                if (isset($column['ord2']) && is_array($ord2 = $column['ord2'])) {
                    $this->orders[] = $db ? $db->buildOrderList($ord2) : $ord2;
                    $column['ord2'] = count($this->orders) - 1;
                }
            }
        }

        $this->initOrders();
    }

    /**
     * Processes runtime actions when request contains act=module
     *
     * @return void
     */
    function action() {
    }

    /**
     * Sets the number of elements of the list.
     *
     * @param int $count
     *
     * @return void
     * @throws ConfigurationException
     * @throws InternalException
     */
    public function setCount($count) {
        $this->count = $count;
        $this->createPageLength();
        $this->impose();
    }

    /**
     * @param int $n
     *
     * @throws ConfigurationException
     * @throws InternalException
     */
    public function setPageLength($n) {
        $this->page_length = $n;
        $this->createPageLength();
        $this->impose();
    }

    function setScript($script) {
        $this->script = $script;
    }

    /**
     * Returns page_length preference of current user
     *
     * @return integer
     * @throws ConfigurationException
     */
    public static function getUserPageLength() {
        if (self::$_userPageLength) return self::$_userPageLength;
        $configlength = class_exists('App') ? UApp::config('list/default_page_length') : 50;
        $user_page_length = Util::getSession('page_length', Util::getSession('user_page_length', $configlength));
        if ($user_page_length < 1) $user_page_length = $configlength;
        return self::$_userPageLength = $user_page_length;
    }

    /**
     * Recalculates page_length, page_show;
     * Stores paging parameters into the session
     *
     * @return void
     * @throws ConfigurationException
     * @throws InternalException
     */
    private function createPageLength() { // setCount után ezt is újra kell számolni, ugyanis impose elrontja a page_show értékét.
        if ($this->paging) {
            Util::assertString($this->name);
            $this->page_show = (int)($this->req_list ? Util::getReq('list_page') : Util::getSession('list_page_' . $this->name, 1));
            $this->page_length = $this->page_length ? $this->page_length : (int)($this->req_list ? Util::getReq('list_length') : Util::getSession('list_length_' . $this->name, static::getUserPageLength()));
        } else {
            $this->page_show = 1;
            $this->page_length = $this->count;

        }
        if ($this->req_list) {
            $_SESSION['list_length_' . $this->name] = $this->page_length;
            $_SESSION['page_length'] = $this->page_length; // Erre a sessionre mindig az utolsó lapbeállítás a default
            $this->search = Util::getReq('list_search');
            $_SESSION['list_search_' . $this->name] = $this->search;
        }
    }

    /**
     * Recalculates page_count, page_show
     *
     * Modifies page_length if is out of range.
     *
     * @return void
     * @throws ConfigurationException
     */
    private function impose() {
        if ($this->paging) {
            if ($this->page_length < 1) $this->page_length = static::getUserPageLength(); else if ($this->page_length > 500) $this->page_length = static::getUserPageLength();
            #Debug::tracex('count', "$this->count, $this->page_count");
            if ($this->count % $this->page_length > 0) $this->page_count = (int)($this->count / $this->page_length) + 1;
            else $this->page_count = (int)($this->count / $this->page_length);
            if ($this->page_show > $this->page_count) $this->page_show = $this->page_count;
            if ($this->page_show < 1) $this->page_show = 1;
        } else {
            $this->page_length = $this->count;
            $this->page_count = 1;
            $this->page_show = 1;
        }
        if ($this->page_show != 1)
            $_SESSION['list_page_' . $this->name] = $this->page_show;
        else
            unset($_SESSION['list_page_' . $this->name]);
    }

    /**
     * Sets the page number to show
     *
     * If $n is out of the range of the list, the first or last will be set.
     *
     * @param int $n -- value in 1...page_count range
     *
     * @return void
     * @throws ConfigurationException
     */
    function setPage($n) {
        $this->page_show = $n;
        $this->impose();
    }

    /**
     * A listát odalapozza a megadott sorszámú elemhez
     *
     * @param mixed $pos - hányadik elemhez
     *
     * @return void
     * @throws ConfigurationException
     */
    function jump($pos) {
        $this->setPage((int)(($pos - 1) / $this->page_length) + 1);
    }

    function setScriptParam($name, $value) {
        $this->scriptparams[$name] = $value;
    }

    function getScript() {
        $q = http_build_query($this->scriptparams);
        return $this->script . ($q ? '?' . $q : '');
    }

    function getFirst() {
        return ($this->page_show - 1) * $this->page_length + 1;
    }

    function getLast() {
        return $this->page_show * $this->page_length;
    }

    /**
     * Creates <list> node
     *
     * @param UXMLElement|Xlet|UAppPage $node_parent - Node to create list node under. Default is page/content
     * @param null $options -- not used
     *
     * @return UXMLElement the list node
     *
     * @throws DOMException
     * @throws InternalException
     * @throws UAppException
     * @throws ReflectionException
     */
    public function createNode($node_parent = null, $options=null) {
        $node_parent = $this->parentNode($node_parent);
        if (!$node_parent) throw new InternalException('Invalid parent node');
        $node = $node_parent->addNode('list', [
            'script' => $this->getScript(),
            'name' => $this->name,
            'id' => $this->id,
            'item-name' => $this->item_name,
            'count' => $this->count,
            'order' => $this->order,
            'page-count' => $this->page_count,
            'page' => $this->page_show,
            'page-length' => $this->page_length,
            'item-first' => $this->getFirst(),
            'item-last' => $this->getLast(),
        ]);
        if ($this->with_search) {
            $node->setAttribute("with-search", 'Y');
            if ($this->search !== null) $node->setAttribute("search", $this->search);
        }
        if ($this->columns) {
            $columns = ''; // name,title,ord1,ord2,width1,width2,limit|
            foreach ($this->columns as $column) {
                if (is_string($column)) {
                    $node->addNode('column', ['field' => 'column']);
                    $field = $column;
                } else {
                    $nodeColumn = $node->addNode('column', array_filter($column, function ($k) {
                        return !in_array($k, ['width', 'ord', 'field']);
                    }, ARRAY_FILTER_USE_KEY));
                    /** @var array $field -- field definition to retrieve name and title from model */
                    $field = ArrayUtils::getValue($column, 'field');
                    if ($field) $nodeColumn->setAttribute('field', is_array($field) ? implode(':', $field) : (string)$field);
                }
                $name = '';
                $title = '';
                if ($field && is_string($field)) $field = explode(':', $field);
                // Field name can accidentaly be a Class name...
                if (is_array($field) && isset($field[0]) && isset($field[1]) && is_a($field[0], BaseModel::className(), true)) {
                    $name = call_user_func([$field[0], 'attributeLabel'], $field[1]);
                    $title = call_user_func([$field[0], 'attributeHint'], $field[1], 'hint');
                }
                $columns .=
                    ArrayUtils::getValue($column, 'name', $name) . ',' .
                    ArrayUtils::getValue($column, 'title', $title) . ',' .
                    ArrayUtils::getValue($column, 'ord1', '') . ',' .
                    ArrayUtils::getValue($column, 'ord2', '') . ',' .
                    ArrayUtils::getValue($column, 'width1', '') . ',' .
                    ArrayUtils::getValue($column, 'width2', '') . ',' .
                    ArrayUtils::getValue($column, 'limit', '') . '|';
            }
            $node->setAttribute('columns', $columns);
            unset($this->attributes['columns']);
        }
        if (is_array($this->attributes)) foreach ($this->attributes as $name => $value) {
            if (is_array($value)) $node->addNodes('attr-' . $name, [$value]);
            else $node->setAttribute($name, $value);
        }
        if ($this->headerfields) {
            foreach ($this->headerfields as $name => $value) {
                $node->addNode('headerfield', array_merge(['name' => $name], $value));
            }
        }
        if (is_array($this->variables)) foreach ($this->variables as $name => $value) $node->addNode('variable', ['name' => $name, 'value' => $value]);

        if ($this->dataSource) {
            Debug::tracex('count', $this->dataSource->count);
            Debug::tracex('list', sprintf('%d, %d, %s', $this->offset(), $this->limit(), Util::objtostr($this->order())));
            // Creates data elements
            $items = $this->dataSource->fetch($this->offset(), $this->limit(), $this->order());
            if ($this->item_name && !isset($this->modelOptions['nodeName'])) {
                $this->modelOptions['nodeName'] = $this->item_name;
            }
            if ($this->dataSource->modelClass) {
                Model::createNodes($node, $items, $this->modelOptions);
            } else {
                $node->createNodes($items, $this->modelOptions);
            }
        }

        if ($this->menu) $this->menu->createNode($node);

        return $this->setNode($node);
    }

    /**
     * Set list to searchable, and sets/returns current search pattern
     * @param string p -- sets current pattern if not null
     * @return string
     */
    function setSearch($p = null) {
        if ($p !== null) $this->search = $p;
        $this->with_search = true;
        return $this->search;
    }

    /**
     * Megadja, hogy a kurrens oldal hányadik elemmel kezdődik. 0-based
     *
     * @return integer
     */
    function offset() {
        $o = ($this->page_show - 1) * $this->page_length;
        if ($o < 0) $o = 0;
        return $o;
    }

    /**
     * Returns current page number. 1-based.
     * @return integer
     */
    function getPage() {
        return $this->page_show;
    }

    /**
     * Returns current maximum number of items per page
     * @return integer
     */
    function limit() {
        return $this->page_length;
    }

    /**
     * Returns current order including order direction
     * @return string
     */
    function getOrderName() {
        if (!$this->orders) return null;
        return isset($this->orders[$this->order]) ? $this->orders[$this->order] : $this->orders[$this->order & 1];
    }

    /**
     * Returns index of current order. Odd numbers are the opposite (descending) orders
     * @return int
     */
    function getOrder() {
        return $this->order;
    }

    /**
     * Returns current order as order array for Query
     * Example: converts 'domain, name desc, variant nulls first' to array('domain', array('name', DBX::ORDER_DESC), array('variant', DBX::NULLS_FIRST))
     * @return array|null
     * @see Query::buildOrders()
     */
    function order() {
        if (!$this->orderName) return null;
        if (is_array($this->orderName)) return $this->orderName;
        $order1 = explode(',', $this->orderName);
        foreach ($order1 as &$oe) {
            $oe = trim($oe);
            $fieldname = Util::substring_before($oe, ' ', true);
            $qualifiers = strtolower(trim(Util::substring_after($oe, ' ')));
            if ($qualifiers) {
                $dir = null;
                $nls = null;
                if (preg_match('/\bdesc\b/', $qualifiers)) $dir = DBX::ORDER_DESC;
                if (preg_match('/\basc\b/', $qualifiers)) $dir = DBX::ORDER_ASC;
                if (preg_match('/\bnulls first\b/', $qualifiers)) $nls = DBX::NULLS_FIRST;
                if (preg_match('/\bnulls last\b/', $qualifiers)) $nls = DBX::NULLS_LAST;
                $oe = [$fieldname, $dir, $nls];
            }
        }
        return $order1;
    }

    /**
     * Returns search field name based on order
     * @return string
     */
    function fieldName() {
        $order = $this->getOrderName();
        if (!$order) return null;
        $oa = explode(',', $this->getOrderName());
        return str_replace(' desc', '', trim($oa[0]));
    }

    /**
     * adds order, limit, offset to SQL string according to list parameters
     *
     * @param string $sql
     * @param int $start -- if true, first order will be "start desc"
     * @param int $all -- if true, all records (skips offset and limit)
     * @return string
     */
    function sqlTail($sql = '', $start = 0, $all = 0) {
        $startx = $start ? 'start desc,' : '';
        $sql .= " order by $startx " . $this->getOrderName();
        if (!$all) $sql .= " limit " . $this->limit() . " offset " . $this->offset();
        return $sql;
    }

    /**
     * Adds desc elements to order names array
     *
     * @param array $orders
     * @return array
     */
    static function descOrders($orders) {
        $r = [];
        for ($i = 0; $i < count($orders); $i++) {
            $r[] = $orders[$i];
            #$r[] = $orders[$i].' desc';
            $r[] = self::descField(trim($orders[$i]));
        }
        return $r;
    }

    /**
     * Reverses one or more order clauses
     *
     * Multiple orders are separated by `,`
     *
     * @param string $order
     * @return string
     */
    static function descField($order) {
        $f2 = [];
        $fields = explode(',', $order);
        for ($i = 0; $i < count($fields); $i++) {
            $field = trim($fields[$i]);
            if ($field == '') return '';
            $words = explode(' ', $field);
            $wn = count($words);
            // Először a nullkezelés
            if ($wn > 2 && strtolower($words[$wn - 2]) == 'nulls') {
                if (strtolower($words[$wn - 1]) == 'first') $words[$wn - 1] = 'last';
                else if (strtolower($words[$wn - 1]) == 'last') $words[$wn - 1] = 'first';
            }
            if ($wn < 2) $words[] = 'desc';
            else if (strtolower($words[1]) == 'desc') array_splice($words, 1, 1);
            else array_splice($words, 1, 0, 'desc');
            $f2[$i] = implode(' ', $words);
        }
        return implode(', ', $f2);
    }

    /**
     * Megállapítja, hogy a keresett értéket tartalmazó sor hányadik az sql-ben lekérdezett készletben, és oda lapoz.
     * A keresett értéket a készlet első oszlopában keresi. Az első előfordulást adja.
     * Az SQL-hez az aktuális ORDER-t hozzáteszi a lista beállításai alapján.
     *
     * @param string $item -- a keresett érték
     * @param DBX $db
     * @param string $sql -- az SQL, mely a teljes készlet keresett oszlopát adja vissza, ORDER nélkül
     * @param array $params -- SQL paraméterei, ha paraméteres az SQL
     *
     * @return bool -- true, ha megtaláltuk, false, ha nem.
     * @throws ConfigurationException
     * @throws DatabaseException
     * @throws InternalException
     */
    function jumpitem($item, $db, $sql, $params) {
        $sql = $sql . " order by " . $this->orderName;
        $aa = $db->select_column($sql, $params);
        $i = array_search($item, $aa);
        if ($i === false) return false;
        $this->jump($i);
        return true;
    }

    /**
     * Creates a displayable and pageable list object with sql parameters
     * Static version
     *
     * @param UXMLElement $node_parent -- target node
     * @param DBX $db
     * @param string $sql
     * @param array $params
     * @param string $listname
     * @param string $itemname
     * @param array $orders
     * @param string $sql1
     * @param bool $withSearch
     * @param string $search -- amit a search mezőbe kell írni (a standard mechanizmus felülírása)
     * @param bool $exp -- minden rekordot
     *
     * @return UXMLElement -- lista node
     * @throws ConfigurationException
     * @throws DOMException
     * @throws DatabaseException
     * @throws InternalException
     * @throws UAppException
     * @throws ReflectionException
     * @deprecated use UList with datasource property
     */
    static function loadList($node_parent, $db, $sql, $params, $listname, $itemname, $orders, $sql1 = '', $withSearch = false, $search = '', $exp = false) {
        $sql1 = $sql1 ? $sql1 : "select count(*) from ($sql) x";
        $count = $db->selectvalue($sql1, $params);

        $list = new UList($listname, $itemname, $count, $orders, 0);
        if ($withSearch) $list->setSearch($search);

        $node_list = $list->createNode($node_parent);
        $sql = $list->sqlTail($sql, 0, $exp);
        Debug::tracex('sql', [$sql, $params]);
        $node_parent->query($db, $itemname, $sql, $params);
        return $node_list;
    }
}
