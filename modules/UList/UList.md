UList modul használati példák
==============================

Lapozott listák létrehozása és megjelenítése. Képességek:

- Szerver oldali SQL alapú lapozás
- Szerver oldali SQL alapú rendezés
- Szerver oldali keresés (szűkítés), SQL alapon, RE kifejezéssel
- vagy kliens oldali js alapú rendezés (csak nem lapozott lista)
- reszponzív tulajdonságok
- sorok kijelölése, minden sor kijelölése
- user vezérlők beilleszése a fejlécbe
- listaparaméterek beillesztése
- popup listák támogatása (lapozáskor újrahívás ajaxszal)
- navigátor (lapozó) sáv megjelenítése igéyn szerint: sehol, felül, vagy felül és alul
- felhasználói laphosszakat tárol, kezel

Egyszerű lapozatlan lista létrehozása
-------------------------------------
_Szerver oldali rendezéssel_

### php kód

```php
	$wherex = ' user where feltételek ';
	$sql = "select ... where $wherex";
	$sqlc = "select count(id) ... where $wherex";
	$params = [...]; // SQL paraméterek
	$count = $this->db->selectvalue($sqlc, $params);
	$item_name = 'group';
	$orders = ['type, name', 'name', 'title', 'enabled']; // SQL order by után írható kifejezések
	
	$list = new UList($item_name.'_list', $item_name, $count, UList::descOrders($orders), 2);
	$node_list = $list->createNode($node_parent);
	$this->xmldoc->query($this->db, $node_parent, $item_name, $sql, $params);		
```

### xslt kód

```xml
<xsl:apply-templates select="list[@name='listanév']">		<!-- (névvel, ha több lista is van) -->
    <xsl:with-param name="columns">		<!-- name,title,ord1,ord2,width|...| -->
            #,,16,17,60,120|			<!-- tágítható táblázatnál a width két érték: min,max -->
            név,,0,1,80,200|
            domain,,0,1,100,300|
            ...
    </xsl:with-param>
    <xsl:with-param name="spacing" select="2" />
</xsl:apply-templates>
...
<xsl:template match="elemnév" mode="list-item">
    <tr>
        <td>cella-1</td>...
    </tr>
</xsl:template>
```

Szerver oldali lapozás beillesztése
------------------------------------

### php kód

```php
$wherex = ' user where feltételek ';
$sql = "select ... where $wherex";
$sqlc = "select count(id) ... where $wherex";
$params = [...]; // SQL paraméterek
$count = $this->db->selectvalue($sqlc, $params);
$item_name = 'group';
$orders = ['type, name', 'name', 'title', 'enabled']; // SQL order by után írható kifejezések

$list = new UList($item_name.'_list', $item_name, $count, UList::descOrders($orders), 2);
$node_list = $list->createNode($node_parent);
$this->xmldoc->query($this->db, $node_parent, $item_name, $list->sqlTail($sql), $params);
```	
A lényeg az sqlTail() metódus alkalmazása az elemek lehívásánál.		

### xslt kód
```xml
	<xsl:apply-templates select="list[@name='listanév']">		<!-- (névvel, ha több lista is van) -->
		<xsl:with-param name="with-navigator" select="1" />		<!-- 0,1,2 -->
		<xsl:with-param name="columns">		<!-- name,title,ord1,ord2,width|...| -->
				#,,16,17,60,120|			<!-- tágítható táblázatnál a width két érték: min,max -->
				név,,0,1,80,200|
				domain,,0,1,100,300|
				...
		</xsl:with-param>
		<xsl:with-param name="spacing" select="2" />
	</xsl:apply-templates>
	...
 	<xsl:template match="elemnév" mode="list-item">
 		<tr>
 			<td>cella-1</td>...
		</tr>
	</xsl:template>
```

A lényeg a navigátor megjelenítése a "with-navigator" paraméterrel. 2-es érték esetén alul is megjelenik.

Keresés (szűkítés) beillesztése
-------------------------------
A php kódban kell egy
$p = $list->setSearch(); // $p a felhasználó által beírt adatot adja vissza
hívás, és az SQL összeállításakor az alábbihoz hasonlóan
if($p) $wherex .= " and r.name ~* $p ";
az SQl-be be kell tenni a szűkítő feltételt.
Az XSLT-hez nem kell nyúlni, a setSearch() hatására a keresőmező automatikusan megjelenik.

Custom vezérlők
---------------

Az xsl-ben a következő callback-ben helyezhetők el az user vezérlők, ezek a fejlécben a keresés mellett jelennek meg.

```
	<xsl:template match="list" mode="options">
		...
	</xsl:template>
```

Sorok kijelölése csoportos műveletekhez
---------------------------------------

A sorokban a megfelelő oszlopban meg kell jeleníteni:
<input type="checkbox" id="select_{@id}" class="selectrow ..."
name="select[]" value="{@id}" title="Sor kijelölése. Ctr vagy Shift a többszörös kijelöléshez." />
A selectrow class biztosítja a többszörös kiválasztás és a megjelenítés helyes működését.

Ha a fejlécben szeretnénk "minden sort kijelöl" checkboxot, akkor a lista template hívásban kell egy selectall
paraméter:

```
	<xsl:apply-templates select="list..."> 
		<xsl:with-param name="selectall" select="'oszlopnév'" /> 
```

Ha az üres nevű oszlopba kell, akkor oszlopnévnek '.'-ot kell megadni.

Ha a listához kontextus-menüt kapcsolunk, a kijeölt elemeken végezhető műveletek menüpontjait `'block'=>1` opcióval kell
megjelölni.
Ezek a menüpontok csak akkor fognek megjelenni, ha van kijelölt sor. Ugyanakkor a `'block'=>0` jelölésű menüpontok
elrejtődnek.
A `'block'=>2` menüpontok mindvégig látszódnak.

Reszponzivitás: Automatikusan táguló táblázat
---------------------------------------------

Megesik, hogy ugyanazt a táblázatot különböző szélességű képernyőkön is meg kell jeleníteni.
Vannak azonban oszlopok, melyek szélessége nem változik, más oszlopokban meg a hely függvényében több adatot
jeleníthetünk meg.

A megoldás lényege, hogy oszloponként megadható a minimális és maximális szélesség.
A tágulás során az oszlopok a megadott határokat nem lépik túl, így az eredmény egy értelmes, nemlineáris tágulás.

A tágulással kapcsolatos adatok az xsl-ben vannak megadva.

Az első két paraméter kötelező ebben az esetben: width=100% és class=strecth
Az oszlopdefiníciós lista 5. és 6. eleme a minimális és maximális szélesség-érték pixelben.
(a 6. pozíció opcionélis, default=min)
A 7.pozíción opcionálisan elhelyezhető az a minimális táblázatszélesség, ami alatt az oszlop teljesen eltűnik.
Rendezett oszlop minimális szélessége kb. 30 pixel + a címke szélessége.
Rendezetlen oszlop minimális szélessége kb. 20 pixel + a címke szélessége.

Példa:

```
	<xsl:apply-templates select="list">
		<xsl:with-param name="width" select="'100%'" />
		<xsl:with-param name="class" select="'stretch'" />
		<xsl:with-param name="columns">
			#,,,,30,30|
			név,,0,1,100,250|
			ttl,,2,3,80,80|
			nt,,4,5,40,40|
			típus,,6,7,50,50|
			pri,,8,9,80,80|
			érték,,10,11,120,250|
			megj.,,12,13,80,500|
			akt,,14,15,50,50|
		</xsl:with-param>
	</xsl:apply-templates>
```

Az egyes adatcellák tartalmát is igazítani kell, különös tekintettel a hosszú adatok levágására.
Ez félautomatikus, mert lehet akár az adatnak csak egy részét zsugorítani. Példa:

```html
<td>
	<div class="fix stretch" style="width:200px" title="{@comment}"><xsl:value-of select="@comment"/></div>
</td>
```

A "fix" class gondoskodik a levágásról, ha van width is megadva. A width kötelező.
A stretch class gondoskodik a width érték automatikus módosításáról, egyébként fix marad.
A width-ben a legkisebb oszlopszélességhez tartozó értéket(-6) használjuk, ez lesz az oszlopszélességgel
arányosan (lineárisan) növelve.

A stretch tulajdonságot javascript működteti, és az ablak változtatásakor nem érvényesül, csak újratöltéskor.

Lehetnek persze nem levágható adatok, ezek esetleg szét fogják nyomni az oszlopot a többiek rovására, de ez nem a
stretch
mechanizmus keretében, hanem a táblázat böngésző általi automatikus méretezésével fog történni, ezért eredménye nem
jósolható.


Kliens oldali rendezés (nincs kész)
----------------------
Ebben az esetben a lista nem tördelhető lapokra, és nem szűkíthető.
Az SQL-t úgy kell megírnni, hogy a teljes készletet adja vissza, célszerűen a default oszlop szerint rendezve.
A php-ban nem adunk meg orders értéket, $deforder lehet, az oszlop számára utal (1-based).

Az XSL-ben a lista hívásban a class paraméterben meg kell adni az "client" classt.

```
	<xsl:with-param name="class" select="'client'" />
```

A "columns" paraméterben az ord1. ord2 értékei ...
