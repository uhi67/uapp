/*
	UList module for UApp
    UList.js
*/

/**
 * @return {void|boolean}
 */
$.fn.UList = function (options) {
	if (options === 'allSelected') {
		let allsel = true;
		$('input.selectrow', this).each(function () {
			if (!$(this).prop('checked')) allsel = false;
		});
		return allsel;
	}
	if (options === 'refreshOperations') {
		const list = $(this);
		const scope = list.parents('form.ulist');
		const selected = list.data('selected'); 	// Kijelölt szamok készlete
		if (selected.length > 0) {
			$('.block_operation', scope).show().trigger('popup-show');
			$('.nonblock_operation', scope).hide();
		} else {
			$('.block_operation', scope).hide().trigger('popup-hide');
			$('.nonblock_operation', scope).show();
		}

	}

};
// noinspection JSUnusedLocalSymbols
const UList = {
	running: false,
	runagain: false,
	rowClick: function (event) {
		const cb = $(this);
		let $cb;
		if (this.nodeName.toLowerCase() !== 'input') {
			$cb = $(this).parent().find('input.selectrow');
			$cb.data('shiftKey', event.shiftKey);
			$cb.data('ctrlKey', event.ctrlKey);
			return $cb.trigger('click'); //{shiftKey: event.shiftKey, ctrlKey: event.ctrlKey}
		}
		const szam = cb.data('num');
		const list = cb.closest('table.list'); // Konkrét lista
		const max = parseInt($('input.selectrow', list).last().data('num')); // Legnagyobb kijelölhető szám
		let lastsel = list.data('lastsel'); 	// Utoljára kijelölt szam
		let selected = list.data('selected'); 	// Kijelölt szamok készlete
		let i, j;

		if (cb.prop('checked')) {
			cb.parents('tr').addClass('selected');
			if (event.shiftKey || cb.data('shiftKey')) {
				if (szam !== lastsel) {
					const d = szam < lastsel ? -1 : 1;
					for (i = lastsel + d; i != szam && i >= 0 && i <= max; i = i + d) {
						$('input.selectrow', list).filter(function () {
							return $(this).data('num') == i;
						}).prop('checked', true).parents('tr').addClass('selected');
						j = selected.indexOf(i);
						if (j == -1) selected.push(i);
					}
				}
				selected.push(szam);
			} else if (event.ctrlKey || cb.data('ctrlKey')) {
				selected.push(szam);
			} else {
				$('input.selectrow', list).filter(function () {
					return $(this).data('num') != szam;
				}).prop('checked', false).parents('tr').removeClass('selected');
				selected = [szam];
			}
			if (list.UList('allSelected')) $('input.selectall', list).prop('checked', true);
		} else {
			cb.parents('tr').removeClass('selected');
			if (event.shiftKey) {
				if (szam != lastsel) {
					const d1 = szam < lastsel ? -1 : 1;
					for (i = lastsel; i != szam && i >= 0 && i <= max; i = i + d1) {
						$('input.selectrow', list).filter(function () {
							return $(this).data('num') == i;
						}).prop('checked', false).parents('tr').removeClass('selected');
						j = selected.indexOf(i);
						if (j != -1) selected.splice(j, 1);
					}
				}
				j = selected.indexOf(szam);
				if (j != -1) selected.splice(j, 1);
			} else if (event.ctrlKey) {
				const i1 = selected.indexOf(szam);
				if (i1 != -1) selected.splice(i1, 1);
			} else {
				$('input.selectrow', list).filter(function () {
					return $(this).data('num') != szam;
				}).prop('checked', false).parents('tr').removeClass('selected');
				selected = [];
			}
			$('input.selectall', list).prop('checked', false);
		}
		lastsel = szam;
		//console.log('['+selected.join(',')+']');
		list.data('lastsel', lastsel);
		list.data('selected', selected);
		list.UList('refreshOperations');
		return true;
	},

	allClick: function (event) {
		const cb = $(this);
		const list = cb.parents('table.list'); // Konkrét lista
		const max = parseInt($('input.selectrow', list).last().data('num')); // Legnagyobb kijelölhető szám
		const selected = list.data('selected'); 	// Kijelölt szamok készlete
		let i, j;
		//console.log('selectall');
		// Mindent kijelöl (vagy töröl)
		if (list.UList('allSelected')) {
			// Mindent töröl
			for (i = 0; i <= max; i++) {
				$('input.selectrow', list).filter(function () {
					return $(this).data('num') == i;
				}).prop('checked', false).parents('tr').removeClass('selected');
				j = selected.indexOf(i);
				if (j != -1) selected.splice(j, 1);
			}
		} else {
			// Mindent kijelöl
			for (i = 0; i <= max; i++) {
				$('input.selectrow', list).filter(function () {
					return $(this).data('num') == i;
				}).prop('checked', true).parents('tr').addClass('selected');
				j = selected.indexOf(i);
				if (j == -1) selected.push(i);
			}
		}
		list.data('selected', selected);
		list.UList('refreshOperations');
	},

	/**
	 * Adjusts table's column widths to max width using <col> limits
	 *
	 * @param index integer -- not used (for foreach use only)
	 * @param table DOMElement -- table to adjust
	 */
	stretchTable: function (index, table) {
		//console.log('try', table);
		if (UList.running) return true; // TODO: lockolás táblánként
		UList.running = true;
		//console.log('stretch', $(table).closest('form').attr('id'));
		let width = $(table).css('width'); // tényleges szélesség // TODO: nem látható tab füleken nem működik!
		if (width === '100%') return true;
		width = parseInt(width.replace(/px/, ''));
		const parentwidth = parseInt($(table).parent().css('width').replace(/px/, ''));

		if (parentwidth >= 240) width = Math.min(width, parentwidth);
		if (width === 0) width = parentwidth;

		//console.log('stretch to', width, parentwidth, $(table).closest('form').attr('id'));

		const cols = $('col', table);
		const wa = [];
		let m0, m1;
		let min = 0;
		let max = 0;
		let lim = 0; // minimum table width to display column. Under this, column will be hidden.
		for (let i = 0; i < cols.length; i++) {
			//var minmax = cols.eq(i).attr('width').split(/,/);
			const widths = cols.eq(i).data('width');
			/** @var {array} minmax */
			const minmax = (typeof widths === 'string' && widths > '') ? widths.split(/,/) : [0, 0];
			wa[i] = {
				'min': m0 = minmax[0] ? parseInt(minmax[0]) : 0,
				'max': m1 = minmax[1] ? parseInt(minmax[1]) : m0,
				'lim': lim = minmax[2] ? parseInt(minmax[2]) : 0
			};
			if (lim === 0 || width >= lim) {
				min += m0;
				max += m1;
			} else {
				// Hide i. columns under limit
				$('tr', table).each(function () {
					// (megelőző) colspan-ok figyelembe vétele
					const cells = $('td,th', this);
					let n = 0;
					for (let j = 0; j < cells.length; j++) {
						const $cell = $(cells[j]);
						const colspan = parseInt($cell.attr('colspan'));
						if (n === i && !(colspan > 1)) {
							$cell.addClass('hidden').hide();
						}
						if (colspan > 1 && i >= n && i <= n + colspan) {
							$cell.attr('colspan', colspan - 1);
						}
						n += colspan ? colspan : 1;
					}
					wa[i].hidden = true;
				});
			}
		}

		const refreshColumns = function () {
			width = $(table).css('width');
			width = parseInt(width.replace(/px/, '')) - cols.length * 5;
			width = Math.max(width, parentwidth); // tényleges szélesség újramérve rejtés után

			const d = (max - min); // Teljes táblázat tágulási határai (az adott oszlopkonfigurációban, rejtéseket már figyelembe véve)
			if (d === 0) {
				console.log('StrecthTable: No enough space for minimum column widths.');
				return false;
			}
			const r = (width - min) / d; // A szükséges tágulási együttható: 0=min, 1=max
			for (let i = 0; i < cols.length; i++) {
				//var lim = wa[i].lim;
				// Ignore hidden columns
				if (wa[i].hidden) continue;
				const s = wa[i].max - wa[i].min; // mozgástér a mezőben
				const w = wa[i].min + Math.floor(s * r); // A mező szükséges mérete a minimumhoz képest
				const rr = s * r / wa[i].min + 1; // mező szélességi szorzója
				cols.eq(i).attr('width', w - 2);
				// Cellákon belüli elemek átméretezése
				$('tr', table).each(function () {
					$('td,th', this).eq(i).find('.stretch').each(function () {
						const style = $(this).attr('style');
						const matches = style ? style.match(/width:\s*(\d+)px/) : null;
						const aw = matches ? parseInt(matches[1]) : 100;
						const nw = Math.floor((aw + 7) * rr) - 7;
						$(this).css('width', '' + nw + 'px');
					});
				});
			}
			if (UList.runagain) {
				UList.runagain = false;
				$('table.stretch').each(UList.stretchTable);
			}
			UList.running = false;
		};
		setTimeout(refreshColumns, 100);
		return true;
	}
};

/* onload */
$(function () {
	$('div.dialog').dialog({
		autoOpen: false,
		modal: true,
		buttons: {
			Ok: function () {
				$(this).dialog('close')
			}
		}
	});

	/* Többszörös kijelölések */
	const lists = $('table.list'); // Listák készlete, amelyeken a funkció függetlenül működik
	lists.each(function () {
		$(this).data('selected', [])    // Kiválasztott id-k tömbje
			.data('lastsel', null);			// Utolsó kiválasztott id
		// Minden sort ellátunk egyesével növekvő sorszámmal
		const rows = $('input.selectrow', this);
		for (let i = 0; i < rows.length; i++) {
			rows.eq(i).data('num', i);
		}
	});
	$('.selectrow', lists).on('click', {boudData: 'test'}, UList.rowClick);
	$('input.selectall', lists).on('click', UList.allClick);

	// Ha az url-ben # van, akkor jump oda
	const rowpos = $('form#form_' + document.location.hash.substring(1)).position();
	if (rowpos) $('html, body').scrollTop(rowpos.top);

	$('input#list_search').keypress(function (evt) {
		evt = evt || window.event;
		const code = evt.keyCode || evt.which;
		let formname = $(this).closest('form').attr('name').substringAfter('_');
		if (code == 13) return list_search_start(formname);
	});

	// noinspection JSUnusedLocalSymbols
	$('.list_length').keypress(function (evt) {
		const name = this.id.split('_')[1];

		evt = evt || window.event;
		const code = evt.keyCode || evt.which;
		if (code == 13) return list_submit(name);
	});

	/* Automatikus oszlop-méretezés */
	setTimeout(function () {
		//console.log($('table.stretch'));
		$('table.stretch').each(UList.stretchTable);
	}, 200);

	/* Selectrow magyarázat */
	const title = 'Kattintással a sor kijelölhető vagy a kijelölés visszavonható. Többszörös kijelölés Ctrl gomb nyomása mellett, tartomány kijelölése Shift gombbal.';
	$('input.selectrow', lists).first().parent().data('title', title).attr('id', 'ulist-selectrow').addClass('tooltip');

	$('form.ulist .filter-submit').on('change', function () {
		$(this).closest('form').submit();
	});

	$('form.ulist div.navigator .auto-submit').on('click', function () {
		$(this).closest('form').submit();
	});
});


// noinspection JSUnusedGlobalSymbols
function list_setpage(formname, oldal) {
	const form = jQuery('#form_list_' + formname);
	//console.log(formname);
	form.find('#list_page').val(oldal);
	form.submit();
	return false;
}

/* oszlop rendező nyílra kattintottak */

// noinspection JSUnusedLocalSymbols, JSUnusedGlobalSymbols
function list_setorder(formname, ord, button) {
	const form = $('#form_list_' + formname);
	const $table = $('table', form).first();
	let $first;
	if ($table.hasClass('client')) {
		// Kliens oldali
		const $th = $(button).closest('th');
		const colnum = $th.prevAll('th').length + 1;
		console.log('client order ', colnum);
		// Rendezés iránya
		let dir = 'down';
		const $sp = $('span.btn', button);
		if ($sp.hasClass('r_down')) dir = 'up';
		// Előző rendező nyíl eltüntetése
		$(button).closest('tr').find('th span.btn').removeClass('r_down r_up').addClass('r_updown');
		// lokális rendezés az oszlop szerint tr.data sorokat
		$first = $('tr.data', $table).first();
		if ($first) {
			const coldata = $('tr.data td:nth-of-type(' + colnum + ')', $table).map(function (i, e) {
				return [[$(e).text(), e.parentNode]];
			}).get();
			coldata.sort(function (a, b) {
				if (dir === 'down') {
					if (!parseInt(a[0]) || !parseInt(a[0])) return a[0].toLowerCase() < b[0].toLowerCase();
					return a[0] < b[0];
				} else {
					if (!parseInt(a[0]) || !parseInt(a[0])) return a[0].toLowerCase() > b[0].toLowerCase();
					return a[0] > b[0];
				}
			}); // Növekvő
			//console.log(coldata);
			const $placeholder = $('<tr>').insertBefore($first);
			for (let i = 0; i < coldata.length; i++) {
				$placeholder.after(coldata[i][1]);
			}
			$placeholder.remove();
			// Rendező nyíl kirajzolása
			$sp.removeClass('r_updown').addClass('r_' + dir);
		}
	} else {
		// Szerver oldali
		$('#list_order', form).val(ord);
		form.submit();
	}
	return false;
}

// noinspection JSUnusedGlobalSymbols
function list_setlength(formname, pagelength) {
	const $form = $('#form_list_' + formname);
	$('length', $form).val(pagelength);
	$form.submit();
	return false;
}

function list_submit(formname) {
	$('#form_list_' + formname).submit();
	return false;
}

// noinspection JSUnusedGlobalSymbols
function list_search_start(formname) {
	const form = $('#form_list_' + formname);
	if ($('#list_search', form).val() > ' '
		|| $('#_list_checkpattern', form).length === 0
		|| $('#user-flags input', form).length > 0) {
		$('#list_go', form).val(1);
		$('#list_page', form).val(1);
		form.submit();
	} else {
		//console.log(form.find('#search_hint'));
		$('#search_hint').dialog('open');
	}
	return false;
}

// noinspection JSUnusedGlobalSymbols
function list_search_cancel(formname) {
	const form = $('#form_list_' + formname);
	$('#list_search', form).val('');
	$('#list_go', form).val(0);
	form.submit();
	return false;
}

// noinspection JSUnusedGlobalSymbols
function list_export(formname) {
	const $form = $('#form_list_' + formname);
	$form.attr('target', '_blank');
	$('#list_exp', $form).val(1);
	$form.submit();
	$('#list_exp', $form).val('');
	$form.removeAttr('target');
}
