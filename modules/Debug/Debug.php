<?php /** @noinspection PhpUnused */
/**
 * The Debug module
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright 2014-2016
 * @license MIT
 * @package modules
 */

/**
 * A static helper module to collect trace information and display the framed debug screen.
 *
 * Usage
 * =====
 * Debug module is always included and initialized.
 * The effect of the module may be disabled:
 * - with a `Debug::disable()` call to disable for the current request.
 * - setting `nodebug` global variable to true, will switch off Debug completely
 *
 * In the application, Debug::trace, tracex() and similar calls will collect debug information.
 *
 * On the user interface, `&trace=on` request variable swithes on the data collecting and displaying for the current session.
 * Then, press the `Debug off` button in the debug control pane to swith off the debug feature.
 */
class Debug extends Module {
    /** @var int|bool $nodebug -- debug is swithed off temporary */
    static $nodebug;
    /** @var string $trace -- identifier of trace session, or '' if trace is off. Contains start timestamp and a random sequence. */
    static $trace;
    /** @var string $tracefile -- actual filename (full path) of the current trace logfile, based on self::$trace */
    static $tracefile;
    /** @var float $ts -- debug start microtime on this page */
    static $ts;
    /** @var string project root path to display file locations relative to. */
    static $rootpath;

    /**
     * @param bool $enable
     * @param string $basepath
     *
     * @throws InternalException
     * @throws ConfigurationException
     */
    static function init($enable = true, $basepath = null) {
        self::$rootpath = $basepath ?: dirname(__DIR__, 5);
        if (php_sapi_name() == "cli") {
            self::$ts = microtime(true);
            self::$trace = isset($_SESSION['trace']) ? $_SESSION['trace'] : false;
            $tracex = ArrayUtils::getValue(UApp::$args, 'trace', 'nincs');
            if ($tracex == 'on') {
                #unset(UApp::$args['trace']);
                $tx = date('ymd_His_') . substr(md5(uniqid()), 0, 4);
                self::$trace = $tx;
                echo "Debug is $tracex\n";
            }
            if (self::$trace) {
                ini_set('display_errors', 'stdout');
                self::trace_start();
            }
            return;
        }
        if (Util::getReq('debug')) { #isset($_REQUEST['debug'])) {
            self::callback();
            exit;
        }
        if (!$enable || self::$nodebug) {
            self::$nodebug = true;
            return;
        }
        self::$ts = microtime(true);
        self::$trace = isset($_SESSION['trace']) ? $_SESSION['trace'] : false;
        $tracex = Util::getReq('trace');
        $traced = Util::getReq('traced');
        if ($tracex == 'on') self::debug_start();
        if ($traced != '') self::$trace = $_SESSION['trace'] = $traced;
        if ($tracex == 'off') self::trace_off();
        if (self::$trace) {
            ini_set('display_errors', 'stdout');
            self::trace_start();
            /*
            self::trace_post();
            self::trace_query();
            self::trace_session();
            self::trace_saml();
            */
        }
        if ($tracex == 'on') exit;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @throws InternalException
     */
    static function debug_start() {
        $querya = $_GET;
        unset($querya['trace']);
        $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH); #$_SERVER['SCRIPT_NAME'];
        $url .= '?' . Util::implode_with_keys('&', '=', $querya);
        self::debug_frame($url);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @throws ConfigurationException
     * @throws InternalException
     */
    static function trace_start() {
        if (self::$nodebug) return;

        $debugpath = $GLOBALS['config']['debug']['path'];
        if (!file_exists($debugpath)) mkdir($debugpath, 0774);
        if (!file_exists($debugpath)) {
            if (php_sapi_name() == "cli") echo "Cannot create debug directory\n";
            throw new InternalException("Cannot create debug directory", $debugpath);
        }

        self::$tracefile = $debugpath . '/' . self::$trace;
        if (self::$trace == '') return;

        if (file_exists(self::$tracefile . '.log')) {
            rename(self::$tracefile . '.log', self::$tracefile . '.bak');
        }
        #UApp::log('* Trace start: '.self::$tracefile);
        $fp = @fopen(self::$tracefile, 'w');
        if (!$fp) return;
        $url = php_sapi_name() == "cli" ? $GLOBALS['argv'][1] : $_SERVER['REQUEST_URI'];
        $urlx = preg_replace("/traced=" . self::$trace . "/", '', $url);
        $url1 = str_replace("'", '\\\'', $url);
        //fwrite($fp, '<html><head><link rel="stylesheet" type="text/css" href="main_ie.css"></link></head><body>');
        if (php_sapi_name() == "cli") {
            fwrite($fp, Ansi::color("command\t$urlx", 'light blue') . Ansi::color("\n", 'blue', 'light gray', false));
            fwrite($fp, Ansi::color("start:\t" . date('Y-m-d H:i:s') . "\n", 'black', 'light gray'));
            fwrite($fp, Ansi::color("id:\t" . self::$trace, 'black', 'light gray') . "\n");
        } else {
            $txt =
                '<div class="dt"><b>url:</b> ' . $urlx . "</div>\n<script>window.uri='" . $url1 . "';</script>\n" .
                '<div id="tabcontrol">
					<span class="tab head active">head</span>
					<span class="tab main">main</span>
					<span class="tab app">app</span>
					<span class="tab sql">sql</span>
					<span class="tab output">output</span>
				</div>' .
                '<div id="head">' . "\n" .
                '<div class="de"><b>trace started:</b> ' . date('Y-m-d H:i:s') . "</div>\n" .
                '<div class="de"><b>trace id:</b> ' . self::$trace . "</div>\n" .
                self::trace_post_inner() .
                self::trace_files_inner() .
                self::trace_query_inner() .
                self::trace_session_inner() .
                self::trace_saml_inner() .
                '</div>' . "\n" .
                '<div id="main">' . "\n";
            fwrite($fp, $txt);
        }
        fclose($fp);
    }

    /**
     * Call this to cancel trace on the page
     * (Also deletes existing tracefile and restores the previous)
     *
     * @return void
     */
    static function disable() {
        if (self::$nodebug) return;
        self::$nodebug = true;
        if (file_exists(self::$tracefile . '.bak')) rename(self::$tracefile . '.bak', self::$tracefile . '.log');
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Switches off the trace mode.
     */
    static function trace_off() {
        if (self::$trace == '') return;
        //if(self::$trace=='' or self::$tracefile=='') return;
        ini_set('display_errors', 'stdout');
        try {
            if (self::$tracefile != '') {
                if (file_exists(self::$tracefile)) {
                    $fp = fopen(self::$tracefile, 'w');
                    if (!$fp) return;
                    fwrite($fp, "</div>\n</body></html>\n");
                    fclose($fp);
                    unlink(self::$tracefile);
                }
                if (file_exists(self::$tracefile . '.log')) unlink(self::$tracefile . '.log');
            }
            $_SESSION['trace'] = self::$trace = '';
        } catch (Exception $e) {
            echo "trace_off: " . $e->getMessage();
        }
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Terminates tracing and closes tracefile
     *
     * @return void
     */
    static function trace_end() {
        if (self::$nodebug) return;
        if (self::$trace == '' or self::$tracefile == '') return;
        if (file_exists(self::$tracefile)) @rename(self::$tracefile, self::$tracefile . '.log');
        if (file_exists(self::$tracefile . '.bak')) @unlink(self::$tracefile . '.bak');
        if (file_exists(self::$tracefile)) @unlink(self::$tracefile);
        self::$tracefile = '';
    }

    /**
     * Writes out a string into tracefile
     *
     * @param string $msg
     * @param string $params
     * @param integer $depth -- effective source depth
     * @param string $tags -- e.g. uapp, app, sql
     * @return string|null  -- the message written out or null on failure
     * @throws ConfigurationException
     * @throws InternalException
     * @throws ReflectionException
     */
    static function trace($msg = '*', $params = '', $depth = 1, $tags = 'app') {
        if (is_null($depth)) $depth = -1;
        if (is_array($tags)) $tags = implode(',', $tags);

        if (self::$trace == '' || self::$tracefile == '') return '';
        if (is_array($msg) || is_object($msg)) $msg = static::objtostr($msg);
        $da = debug_backtrace();

        // determines depth if sql tag used
        if ($tags == 'sql') {
            for ($i = $depth; $i < count($da); $i++) {
                $dx = $da[$i];
                if (!preg_match('#uapp[/\\\\]lib[/\\\\](DBX|Query)[a-z]*.php#i', ArrayUtils::getValue($dx, 'file', '$?'))) {
                    $depth = $i + 1;
                    break;
                }
            }
        }

        $dd = isset($da[$depth]) ? $da[$depth] : [];

        $where = (isset($dd['class']) ? $dd['class'] : '') . (isset($dd['type']) ? $dd['type'] : '') . (isset($dd['function']) ? $dd['function'] : '');
        $ts1 = sprintf('%.3f', microtime(true) - self::$ts);
        $dad1 = $da[$depth - 1];
        $args = count($dad1['args'])
            ?
            " \n(" .
            implode(', ',
                array_map("print_r",
                    $dad1['args'],
                    array_fill(0, count($dad1['args']), true)) // Egy halom true, a var_export 2. argumentum�nak
            )
            . ')'
            : '';

        $file = ArrayUtils::getValue($dad1, 'file', '$?') . '#' . ArrayUtils::getValue($dad1, 'line', '#?');
        if (is_array($params)) $msg = Util::substitute($msg, $params); else if ($params > '') $msg .= ', ' . $params;
        $file = str_replace(self::$rootpath, '..', $file);

        if (php_sapi_name() == "cli") {
            $wx = Ansi::color("$ts1\t$file\t$where", 'purple', 'black', false);
            $txt = Ansi::color("$wx\t$msg\n", 'cyan', 'black');
        } else {
            $wx = '<b style="color:#009" title="[' . $ts1 . '] ' . $file . htmlspecialchars($args) . '">' . $where . '</b>';
            $txt = "<div class='dd' data-tags='$tags'>$wx: $msg</div>\n";
        }
        #log('Open tracefile: '.self::$tracefile);
        $fp = fopen(self::$tracefile, 'a');
        if (!$fp) return null;
        fwrite($fp, $txt);
        fclose($fp);
        return $txt;
    }

    /** @noinspection PhpDocMissingThrowsInspection */

    /**
     * Writes out a name-value pair to the tracefile
     * Value also may be an object or array
     *
     * @param string $name
     * @param mixed $value
     * @param string $color
     * @param string $params
     * @param integer $depth -- backtrace stack depth to skip
     * @param string $tags -- any text tag separated by spaces, e.g: uapp, app, sql
     * @param float $mt -- elapsed time (for SQL-s) in float seconds
     *
     * @return void
     */
    static function tracex($name, $value = '', $color = '#00C', $params = '', $depth = 2, $tags = 'app', $mt = null) {
        if (is_null($depth)) $depth = 2;
        if (self::$trace == '') return;
        if (!is_string($name)) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $name = Util::objtostr($name);
        }
        /** @noinspection PhpUnhandledExceptionInspection */
        $value = static::objtostr($value);
        if ($tags == 'sql' && strpos($value, 'information_schema.columns')) $tags = 'sql uapp';
        if ($mt) $mtx = $mt > 2.0 ? floor($mt * 10) / 10 . ' s ' : floor($mt * 1000.0) . ' ms '; else $mtx = '';
        if (php_sapi_name() == "cli") {
            if ($mt) $mtx = Ansi::color($mtx, 'yellow', null, false);
            $msg = $mtx . Ansi::color($name, 'green', null, false) . "\t" . Ansi::color($value, Ansi::htmltoansi($color), null, false);

        } else {
            if ($mt) $mtx = '<i>' . $mtx . '</i> ';
            $msg = $mtx . '<b style="color=#090">' . $name . '</b>=<b style="color:' . $color . '">' . $value . '</b>';
        }
        /** @noinspection PhpUnhandledExceptionInspection */
        self::trace($msg, $params, $depth, $tags);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Writes out an XML document or string fragment to the tracefile
     *
     * @param string $name
     * @param DOMDocument|DOMElement|string $xmldoc
     * @param string $tags
     * @return void
     * @throws ConfigurationException
     * @throws InternalException
     * @throws ReflectionException
     */
    static function trace_xml($name, $xmldoc, $tags = 'app') {
        if (!self::$trace) return;
        if (is_null($xmldoc)) {
            self::tracex($name, null);
            return;
        }
        #$p = Loader::getPath('Debug');
        $p = __DIR__;
        if (is_string($xmldoc)) {
            $doc = new DOMDocument();
            if (!$doc->loadXML($xmldoc)) {
                self::tracex($name, 'Invalid XML:' . $xmldoc, '#E00');
                return;
            }
            $xmldoc = $doc;
        }

        try {
            $result = UXMLDoc::processXml($xmldoc, $p . '/copy.xsl', null, true);
        } catch (InternalException $e) {
            $result = '{Invalid xml}';
        }
        if ($tags == 'output') {
            $txt = '</div><div id="output"><div class="dx">' . $result . '</div></div>';
            $fp = fopen(self::$tracefile, 'a');
            if (!$fp) return;
            fwrite($fp, $txt);
            fclose($fp);
        } else {
            $txt = $name . '=<div class="dx">' . $result . '</div>';
            self::trace($txt, '', 2, $tags . ' xml');
        }
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns a human readable-formatted XML document or string
     *
     * @param DOMDOcument $xmldoc
     * @return string
     * @throws InternalException
     */
    static function format_xml($xmldoc) {
        $p = __DIR__;
        if (is_string($xmldoc)) {
            $doc = new DOMDocument();
            if (!$doc->loadXML($xmldoc)) return 'Invalid XML:' . $xmldoc;
            $xmldoc = $doc;
        }
        return UXMLDoc::processXml($xmldoc, $p . '/copy.xsl', null, true);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Writes out a DOMElement to the tracefile
     *
     * @param string $name
     * @param DOMElement $node
     * @param string $tags
     *
     * @return void
     * @throws ConfigurationException
     * @throws InternalException
     * @throws ReflectionException
     */
    static function trace_node($name, $node, $tags = 'app') {
        if (!self::$trace) return;
        if (!is_a($node, 'DOMNode')) {
            self::tracex($name, '(*)' . Util::objtostr($node), '#C00', null, 3);
            return;
        }
        $node->ownerDocument->formatOutput = true;
        $result = $node->ownerDocument->saveXML($node);
        self::trace($name . '=<div class="dx"><pre>' . htmlspecialchars($result) . '</pre></div>', '', 2, $tags . ' node');
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @return string
     */
    static function trace_post_inner() {
        if (!self::$trace) return '';
        $txt = "<div class='dt'><b>POST:</b></div>";
        foreach ($_POST as $i => $value) {
            $value = htmlspecialchars(Util::objtostr($value));
            $txt .= "<div class='dv'><b>$i</b>=$value</div>\n";
        }
        $txt .= "<br />\n";
        return $txt;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     */
    static function trace_post() {
        if (!self::$trace) return;
        $fp = fopen(self::$tracefile, 'a');
        if (!$fp) return;
        fwrite($fp, self::trace_post_inner());
        fclose($fp);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @return string
     */
    static function trace_files_inner() {
        if (!self::$trace) return '';
        if (empty($_FILES)) return '';
        $txt = "<div class='dt'><b>FILES:</b></div>";
        foreach ($_FILES as $i => $value) {
            $value = htmlspecialchars(Util::objtostr($value));
            $txt .= "<div class='dv'><b>$i</b>=$value</div>\n";
        }
        $txt .= "<br />\n";
        return $txt;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     *
     */
    static function trace_files() {
        $fp = fopen(self::$tracefile, 'a');
        if (!$fp) return;
        fwrite($fp, self::trace_post_inner());
        fclose($fp);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @return string
     */
    static function trace_query_inner() {
        if (self::$trace == '') return '';
        $txt = "<div class='dt'><b>GET:</b></div>";
        foreach ($_GET as $i => $value) {
            $value = htmlspecialchars(Util::objtostr($value));
            $txt .= "<div class='dv'><b>$i</b>=$value</div>\n";
        }
        $txt .= "<br />\n";
        return $txt;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     *
     */
    static function trace_query() {
        $fp = fopen(self::$tracefile, 'a');
        if (!$fp) return;
        fwrite($fp, self::trace_query_inner());
        fclose($fp);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @return string
     * @throws ConfigurationException
     * @throws InternalException
     */
    static function trace_session_inner() {
        if (!self::$trace) return '';
        $sid = session_id();
        $txt = "<div class='dt'><b>SESSION:</b> $sid</div>";
        foreach ($_SESSION as $i => $value) {
            $value = self::objtostr($value);
            $txt .= "<div class='dv'><b>$i</b>=$value</div>\n";
        }
        $txt .= "<br />\n";
        return $txt;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @throws ConfigurationException
     * @throws InternalException
     */
    static function trace_session() {
        $fp = fopen(self::$tracefile, 'a');
        if (!$fp) return;
        fwrite($fp, self::trace_session_inner());
        fclose($fp);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @return string
     * @throws ConfigurationException
     * @throws InternalException
     */
    static function trace_saml_inner() {
        if (!self::$trace) return '';
        if (!isset($_SESSION['SimpleSAMLphp_SESSION'])) return '';
        $txt = "<div class='dt'><b>SAML:</b></div>";
        $saml = $_SESSION['SimpleSAMLphp_SESSION'];
        $obj = unserialize($saml);
        $x = gettype($obj);
        if (is_array($obj) || is_object($obj)) {
            foreach ($obj as $i => $value) {
                $value = self::objtostr($value);
                $txt .= "<div class='dv'><b>$i</b>=$value</div>\n";
            }
        } else {
            $xobj = print_r($obj, true);
            $txt .= "<div class='dv'>$x=>$xobj</div>\n";
        }
        $txt .= "<br />\n";
        return $txt;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     *
     * @throws ConfigurationException
     * @throws InternalException
     */
    static function trace_saml() {
        $fp = fopen(self::$tracefile, 'a');
        if (!$fp) return;
        fwrite($fp, self::trace_saml_inner());
        fclose($fp);
    }

    /**
     * Creates html-displayable backtrace using <div class="dv"...> tags
     * The result will be written out to the tracefile
     * Uses objtostr on arguments
     * @return void
     */
    static function backtrace() {
        try {
            if (!self::$trace) return;
            $txt = "<div class='dt'><b>BACKTRACE:</b></div>";
            $bt = debug_backtrace();
            foreach ($bt as $s) {
                $txt .= sprintf("<div class='dv'><b>%s(%d)</b>: %s%s%s(%s)</div>\n",
                    isset($s['file']) ? $s['file'] : '---',
                    isset($s['line']) ? $s['line'] : '--',
                    isset($s['object']) ? get_class($s['object']) : '',
                    isset($s['method']) ? $s['method'] : '',
                    $s['function'],
                    self::objtostr($s['args']));
            }
            $txt .= "<br />\n";
            $fp = fopen(self::$tracefile, 'a');
            if (!$fp) return;
            fwrite($fp, $txt);
            fclose($fp);
        } catch (Exception $e) {
            echo "<div id='debug'><style>div#debug, div#debug *{color:#006;background-color:#eee;}</style>";
            echo "<h3>Exception in backtrace</h3>";
            echo '<p>' . $e->getMessage() . '</p><pre style="font-size:0.85em">';
            debug_print_backtrace();
            echo "</pre></div></style>";
        }
    }

    /**
     * Replaces all values to string, structured values to typename only
     *
     * @param mixed $a
     * @return string
     */
    static function simpleValues($a) {
        array_walk($a, function (&$v, /** @noinspection PhpUnusedParameterInspection */ $i = 0) {
            $t = isset($v) ? gettype($v) : 'unset';
            switch ($t) {
                case 'integer':
                case 'double':
                case 'string':
                case 'boolean':
                    $v = (string)$v;
                    break;
                default:
                    $v = $t;
            }
        });
        return $a;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Outputs main frameset containing debug, control and application frames
     *
     * @param string $url
     *
     * @return void
     * @throws InternalException
     */
    static function debug_frame($url) {
        // Cache tiltás
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    // Date in the past
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
        header("Cache-Control: no-cache, must-revalidate");  // HTTP/1.1
        header("Pragma: no-cache");                          // HTTP/1.0
        header("Content-type: text/html; charset=UTF-8");

        $url = preg_replace('/([&?])traced?=[^?&]*/', '', $url);

        $t = Util::getReq('t'); #isset($_REQUEST['t']) ? $_REQUEST['t'] : '';	// which subframe
        $d = Util::getReq('d'); #isset($_REQUEST['d']) ? $_REQUEST['d'] : '';	// debug identifier
        if ($t == 'top') {
            self::debug_top($url, $d);
            return;
        }
        if ($t == 'left') {
            self::debug_left($url, $d);
            return;
        }

        $tx = date('ymd_His_') . substr(md5(uniqid()), 0, 4);
        list($url_base, $query) = explode('?', $url);

        $query_on = $query . (($query != '') ? '&' : '') . 'traced=' . $tx;
        #$query_off = $query . (($query!='')?'&':'') . 'trace=off';
        $url_on = $url_base . '?' . $query_on;
        #$url_off = $url_base . '?' . $query_off;
        $xurl = urlencode($url);
        $xurl_on = htmlspecialchars($url_on);
        #$xurl_off = htmlspecialchars($url_off);

        $basepath = $GLOBALS['config']['baseurl'];
        $url_control = "index.php?debug=1&amp;t=top&amp;url=$xurl&amp;d=$tx";
        $url_left = "index.php?debug=1&amp;t=left&amp;url=$xurl&amp;d=$tx";
        echo /** @lang text */ <<<EOT
		<?xml version="1.0"?>
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml" lang="hu" xml:lang="hu">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<title>debug - $xurl</title>
            <base href="$basepath/" />
		</head>
		<!-- frames -->
		<frameset cols="300, *">
		    <frame id="left" name="left" src="" data-src="$url_left" marginwidth="4" marginheight="4" scrolling="auto" frameborder="1" />
		    <frameset  rows="100, *">
		        <frame id="control" name="control" src="$url_control" marginwidth="10" marginheight="10" scrolling="auto" frameborder="1" />
		        <frame id="main" name="main" data-src="$xurl_on" src="$xurl_on" marginwidth="10" marginheight="10" scrolling="auto" frameborder="1" />
		    </frameset>
		</frameset>
		</html>
EOT;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Outputs top (control) frame
     *
     * @param string $url
     * @param string $d
     * @return void
     */
    static function debug_top($url, $d) {
        $url = Util::substring_before($url, '#', true);
        list($url_base, $query) = strpos($url, '?') ? explode('?', $url) : [$url, ''];

        $query_off = $query . (($query != '') ? '&' : '') . 'trace=off';
        $url_off = $url_base . '?' . $query_off;
        $basepath = $GLOBALS['config']['baseurl'];

        $xurl = urlencode($url);
        #$xurl_on = htmlspecialchars($url_on);
        $xurl_off = htmlspecialchars($url_off);

        /** @noinspection JSUnresolvedVariable */
        echo <<<EOT
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
		<html lang="en">
		<head>
            <base href="$basepath/" />
			<link rel="stylesheet" href="Debug/css/debug.css" type="text/css" />
			<link rel="stylesheet" href="Fa/css/font-awesome.min.css" type="text/css" />
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<script xmlns="http://www.w3.org/1999/xhtml" type="text/javascript" src="jQuery/jquery-3.2.1.min.js"></script>
			<title>debug control pane</title>
		</head>
		<body>
		<table>
			<tr>
				<td style="font-weight: bold;">Debug:</td>
				<td style="background-color:#e0f0f8">$url</td>
				<td style="font-weight: bold;">debug ID:</td>
				<td style="background-color:#e0f0f8">$d</td>
			</tr>
		</table>
		<form action="index.php" id="form_top" onsubmit="return go_new()">
			URL: <input id="top_url" type="text" name="url" value="$url" style="width:400px" />
			<input type="hidden" name="debug" value="1" /> 
			<input type="hidden" name="d" value="$d" /> 
			<input type="button" name="ok" value="GO" onclick="go_new()"/>&#160;&#160;
			<input type="button" name="cancel" value="debug OFF" onclick="window.top.location='$xurl_off';"/>&#160;
			<input type="button" name="refreshleft" value="refresh trace" onclick="refreshTrace()"/>
			<input type="button" name="refreshmain" value="refresh main" onclick="window.parent.frames['main'].location.reload(); window.parent.parent.frames['left'].location.reload();"/>
			&#160;
			<!-- <input type="button" name="x640" value="640*480" onclick="window.parent.frames['main'].height=480;"/>
			<input type="button" name="x800" value="800*600" onclick="window.parent.frames['main'].height=600;"/> -->
			<script type="text/javascript">
			<!-- 
			try{
				console.log("Debug module console on.")
			}
			catch(e) {
				// noinspection JSValidateTypes
                /** @type {object} console */
				console = {};
				console.log=function(a) {return a;}
			}
			
			function checkMain() {
				//window.clearInterval(window.chkTimer1);
				//window.clearInterval(window.chkTimer2);
				//var p = window.parent;
				const newurl = window.parent.frames['main'].location.href;
				if(window.lastlocation !== newurl) {
					//console.log('checkMain old: '+window.lastlocation, newurl);
					window.lastlocation = newurl;
					const newx = newurl.replace(/traced=[^&]*/, '');
					console.log('checkMain changed: '+newx);
					//console.log('left: $url_base?debug=1&t=left&url='+escape(newx)+'&d=$d');
					//console.log('top: $url_base?debug=1&t=top&url='+escape(newurl)+'&d=$d');
					window.parent.frames['left'].location.href = '$url_base?debug=1&t=left&url='+escape(newx)+'&d=$d';
					window.parent.frames['control'].location.href = '$url_base?debug=1&t=top&url='+escape(newurl)+'&d=$d';
				}
			}
			
			window.chkTimer1 = window.setTimeout(function() {
				window.lastlocation = window.parent.frames['main'].location.href;
			},500);
			window.chkTimer2 = window.setInterval(checkMain, 2000);
			
			function refreshTrace() {
				//console.log('$xurl');
				//console.log('left: $url_base?debug=1&t=left&url=$xurl&d=$d');
				window.parent.parent.frames['left'].location.href='$url_base?debug=1&t=left&url=$xurl&d=$d';
			}
			function go_new() {
				let url = $('#top_url').val();
				if(url.indexOf('?') === -1) url += '?'; else url += '&'; 
				window.top.location = url+'&trace=on';
				return false;
			}
			// --> </script>
		</form>
		</body>
		</html>
EOT;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @param string $url
     * @param string $d
     */
    static function debug_left($url, $d) {
        $st = microtime(true);
        $to = 4;
        /** @noinspection PhpStatementHasEmptyBodyInspection */
        while (time() - $st < 2) ;
        $basepath = $GLOBALS['config']['baseurl'];
        #$modulepath = $basepath.'/modules/Debug';
        $debugpath = $GLOBALS['config']['debug']['path'];
        /** @noinspection PhpStatementHasEmptyBodyInspection */
        while (!file_exists($debugpath . '/' . $d . '.log') && time() - $st < $to) ;
        if (file_exists($debugpath . '/' . $d . '.log'))
            $tf = $debugpath . '/' . $d . '.log';
        else
            $tf = $debugpath . '/' . $d;
        /** @noinspection PhpStatementHasEmptyBodyInspection */
        while (!file_exists($tf) && microtime(true) - $st < $to + 2) ;
        $trace = file_get_contents($tf);
        echo <<<EOT
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
		<html lang="en">
		<head>
            <base href="$basepath/" />
			<link rel="stylesheet" href="Debug/css/debug.css" type="text/css" />
			<script src="jQuery/jquery-3.2.1.min.js" type="text/javascript"><!-- does not work without explicit close --></script>
			<script src="Debug/js/debug.js" type="text/javascript"><!-- does not work without explicit close --></script>
			<link rel="stylesheet" href="Fa/css/font-awesome.min.css" type="text/css" />
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<style type="text/css">
				dd {margin-left:20px;}
			</style>
			<title>debug trace pane</title>
		</head>
		<body data-url="$url">
			$trace
		</body>
		</html>
EOT;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns content of collected debug data
     *
     * @param string $d -- trace identifier
     * @return bool|string -- file contents
     */
    static function debug_content($d) {
        #$basepath = $GLOBALS['config']['baseurl'];
        #$modulepath = $basepath.'/modules/Debug';
        $debugpath = $GLOBALS['config']['debug']['path'];
        if (file_exists($debugpath . '/' . $d . '.log'))
            $tf = $debugpath . '/' . $d . '.log';
        else
            $tf = $debugpath . '/' . $d;
        if (!file_exists($tf)) return 'Trace content not found';
        return file_get_contents($tf);
    }

    /**
     * @throws InternalException
     */
    static function callback() {
        $url = Util::getReq('url'); #isset($_REQUEST['url']) ? $_REQUEST['url'] : '';
        // remove variables `traced` and `trace`
        $url = preg_replace('/([&?])traced?=[^?&]*/', '', $url);

        $t = Util::getReq('t'); #isset($_REQUEST['t']) ? $_REQUEST['t'] : '';
        $d = Util::getReq('d'); #isset($_REQUEST['d']) ? $_REQUEST['d'] : '';
        if ($t == 'top') {
            Debug::debug_top($url, $d);
            exit;
        }
        if ($t == 'left') {
            Debug::debug_left($url, $d);
            exit;
        }
        Debug::debug_start();
        exit;
    }

    /**
     * Converts object to displayable dl-indented html string
     * Uses depth limit
     *
     * @param mixed $obj
     * @param integer $depth -- depth limit, default 10.
     * @param string $name
     *
     * @return string
     * @throws ConfigurationException
     * @throws InternalException
     */
    static function objtostr($obj, $depth = 10, $name = 'array') {
        if (php_sapi_name() == "cli") return Util::objtostr($obj, false);
        if ($depth < 1) return '...';
        if (is_null($obj)) return '<span>null</span>';
        if (is_bool($obj)) return '<span>' . ($obj ? 'true' : 'false') . '</span>';
        if (is_int($obj)) return '<span>' . $obj . '</span>';
        if (is_float($obj)) return '<span>' . $obj . '</span>';
        if (is_string($obj)) return "<span>'" . htmlspecialchars($obj) . "'</span>";
        if (is_array($obj)) {
            $result = '<dl class="d_obj">';
            foreach ($obj as $key => $item) {
                //if($result!='') $result.=', ';
                $result .= '<dt>' . htmlspecialchars($key) . '</dt><dd>' . self::objtostr($item, $depth - 1) . '<dd>';
            }
            $result .= '</dl>';
            return '<span class="d_sw"><i>' . $name . '</i></span>' . $result;
        }
        if (is_object($obj)) {
            if ($obj instanceof BaseModel) {
                try {
                    return static::objtostr($obj->toArray(), $depth - 1, get_class($obj));
                } catch (Exception $e) {
                    return get_class($obj) . '(error)';
                }
            }
            $result = '';
            foreach ($obj as $key => $item) {
                if ($result != '') $result .= ',';
                $result .= $key . '=' . self::objtostr($item, $depth - 1);
            }
            return get_class($obj) . '{' . $result . '}';
        }
        return $obj;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Instant debug to HTML output
     *
     * @param mixed $any
     * @return void
     */
    static function p(...$any) {
        $bt = debug_backtrace();
        echo "<code style='background-color:lightgray;color: maroon'>", $bt[0]['file'], ':', $bt[0]['line'], '</code><br>';
        foreach ($any as $item)
            echo "<pre>" . Util::objtostr($item) . "</pre>";
    }

    /**
     * Mandatory function, not used here.
     *
     * @return void
     */
    function action() {

    }
}
