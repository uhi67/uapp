/*
	debug.js
	run only in debug frame
*/
(function ($) {
	$(function () {
		$('span.d_sw').click(function () {
			$(this).next().toggle();
			$(this).toggleClass('minus');
		});

		$('div#tabcontrol span.head').click(function () {
			$('div#head').show();
			$('div#main').hide();
			$('div#output').hide();
			$('span.tab', $(this).parent()).removeClass('active');
			$(this).addClass('active');
		});
		$('div#tabcontrol span.main').click(function () {
			$('div#head').hide();
			$('div#main').show();
			$('div#output').hide();
			$('span.tab', $(this).parent()).removeClass('active');
			$(this).addClass('active');
			// show all tags
			$('div.dd', 'div#main').show();
		});
		$('div#tabcontrol span.app').click(function () {
			$('div#head').hide();
			$('div#main').show();
			$('div#output').hide();
			$('span.tab', $(this).parent()).removeClass('active');
			$(this).addClass('active');
			// hide SQL tags
			$('div.dd[data-tags~="app"]', 'div#main').show();
			$('div.dd[data-tags~="sql"]', 'div#main').hide();
		});
		$('div#tabcontrol span.sql').click(function () {
			$('div#head').hide();
			$('div#main').show();
			$('div#output').hide();
			$('span.tab', $(this).parent()).removeClass('active');
			$(this).addClass('active');
			// show SQL tags only (and not UApp)
			$('div.dd', 'div#main').hide();
			$('div.dd[data-tags~="sql"]', 'div#main').show();
			$('div.dd[data-tags~="uapp"]', 'div#main').hide();

		});
		$('div#tabcontrol span.output').click(function () {
			$('div#head').hide();
			$('div#main').hide();
			$('div#output').show();
			$('span.tab').removeClass('active');
			$(this).addClass('active');
		});

		$('span.tag i.fa').click(function () {
			if ($(this).hasClass('fa-minus-square-o')) {
				$(this).nextAll('.sum').show();
				$(this).parent().next('.content').hide();
				$(this).removeClass('fa-minus-square-o');
				$(this).addClass('fa-plus-square-o');
				$(this).nextAll('.text').addClass('short');
			} else if ($(this).hasClass('fa-plus-square-o')) {
				$(this).nextAll('.sum').hide();
				$(this).parent().next('.content').show();
				$(this).removeClass('fa-plus-square-o');
				$(this).addClass('fa-minus-square-o');
				$(this).nextAll('.text').removeClass('short');
			}
		});
	});
})(jQuery);

console.log('debug.js');
