﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:md="urn:oasis:names:tc:SAML:2.0:metadata"
                xmlns:mdui="urn:oasis:names:tc:SAML:metadata:ui"
                xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="xml" version="1.0" indent="yes"
	            doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	            doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	            omit-xml-declaration="yes"
	/>

	<xsl:template match="/*">
		<div>
			<xsl:call-template name="syntax"/>
		</div>
	</xsl:template>

	<xsl:template name="syntax">
		<xsl:choose>
			<xsl:when test="name()=''">
				<xsl:value-of select="."/>
			</xsl:when>
			<xsl:otherwise>
				<span class="tag">
					<xsl:if test="count(*)>0 or text()">
						<i class="fa fa-minus-square-o" aria-hidden="true">&#160;</i>
					</xsl:if>
					<xsl:if test="count(*)=0 and not(text())">
						<i class="fa fa-square-o" aria-hidden="true">&#160;</i>
					</xsl:if>
					<xsl:text>&lt;</xsl:text>
					<b>
						<xsl:value-of select="name()"/>
					</b>

					<!-- namespace -->
					<xsl:if test="namespace-uri()!=namespace-uri(..)">
						<xsl:text> </xsl:text>
						xmlns="
						<i style="color:#93c">
							<xsl:value-of select="namespace-uri()"/>
						</i>
						"
						<xsl:text> </xsl:text>
					</xsl:if>

					<xsl:for-each select="@*">
						<xsl:text> </xsl:text>
						<span class="attr">
							<xsl:value-of select="name()"/>
						</span>
						="
						<span class="value">
							<xsl:value-of select="."/>
						</span>
						<xsl:text>"</xsl:text>
					</xsl:for-each>
					<xsl:if test="count(*)=0 and count(text())=0">
						<xsl:text> /&gt;</xsl:text>
						<br/>
					</xsl:if>
					<xsl:if test="count(*)=0 and count(text())>0">
						<xsl:text>&gt;</xsl:text>
						<span class="text">
							<xsl:value-of select="text()"/>
						</span>
						<span class="shorten">...</span>
						<span class="close">
							<xsl:text>&lt;/</xsl:text>
							<xsl:value-of select="name()"/>
							<xsl:text>&gt;</xsl:text>
							<br/>
						</span>
					</xsl:if>
					<xsl:if test="count(*)>0">
						<xsl:text>&gt;</xsl:text>
						<span class="sum">&#160;(<xsl:value-of select="count(.//*)"/>)
						</span>
					</xsl:if>
				</span>
				<xsl:if test="count(*)>0">
					<dl class="content">
						<dd>
							<xsl:for-each select="node()">
								<xsl:call-template name="syntax"/>
							</xsl:for-each>
						</dd>
					</dl>
					<span class="close">
						<xsl:text>&lt;/</xsl:text>
						<xsl:value-of select="name()"/>
						<xsl:text>&gt;</xsl:text>
						<br/>
					</span>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
