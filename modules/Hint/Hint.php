<?php /** @noinspection PhpUnused */
/**
 * Hint module
 *
 * @copyright 2016
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @license MIT
 * @package modules
 */

/**
 * Hint module coontains only css and js
 *
 * Requires
 * - jQuery
 * - jQueryUI
 * - Fa
 *
 * Creates buttons opening pop-up hints from `<div class="hint" ...>` blocks
 *
 * ## Details
 *
 * ### Source:
 *        `<div id="{$xid}" class="hint" data-title="Tipp"> ... (help text) ... </div>`
 * ### Result:
 *        ```
 *        <i id="button_hint_{$xid}" class="button hintbutton fa fa-question" title="Tipp">
 *        <div id="hint_{$xid}" class="hint ui-draggable">
 *            <i class="button hintclose fa fa-times" />
 *            <div id="{$xid}" class="hint-container">
 *                ... (help text) ...
 *            </div
 *        </div>
 *        ```
 * ### Classes
 *        - resizable
 *
 * ### Extra attributes:
 *        - data-parent: Selector for parent of the Hint window. If not specified, it open in the spot.
 *            It's needed if in the current place the viewport cut is too small (caused by a near parent with relative position)
 *        - data-icon: name of button icon, default is 'fa fa-question'
 *        - data-title: title of button
 */
class Hint extends Module {
    static function init() {
    }

    function action() {
    }
}
