/*
	hint.js

	Project: RR

	Az összes <div class="hint" ...> blokkból felugró hintgombot generál.
	Kell hozzá: hint.css, jQuery
*/


/*
	Ilyenekből:

		<div id="{$xid}" class="hint" data-title="Tipp"> ... szöveg ... </div>

	Ilyeneket csinál:

		<i class="button hintbutton fa fa-question" title="Tipp" />
		<div id="{$xid}" class="hint">
			<i class="button hintclose fa fa-times"/>
			<div id="{$xid}" class="hint-container">
				... szöveg ...
			</div>
		</div>

	## Osztályok

	- resizable

	### Extra attributumok:

	- data-parent: Hint ablak parent-jéhez selector. Ha nincs megadva, helyben nyílik.
	Ez akkor kell, ha helyben nyitva a képkivágás indokolatlanul kicsi lenne egy közeli felettes miatt, akinek relatív pozíciója van.
	- data-icon: ikon név ? helyett, pl. "fa fa-help"
	- data-title: gomb hintje

*/

(function ($) {
	$(function () {
		hints_init();
	});
})(jQuery);


function hints_init() {
	$('div.hint').on('init-hint', function () {
		const $d = $(this);
		let id = $d.attr('id');
		if (!id) {
			id = 'hint_' + Math.random().toString().substr(3, 8);
			$d.attr('id', id);
		}
		const dragCancelHint = 'Double click to cancel drag and copy text.';
		$d.attr('title', dragCancelHint);

		$d.draggable();
		$d.on('dblclick', function () {
			let dragDisabled = $(this).draggable("option", "disabled");
			if (dragDisabled) {
				$(this).draggable("option", "disabled", false).attr('title', dragCancelHint);
			} else {
				$(this).draggable("option", "disabled", true).attr('title', 'Double click to begin drag.');
			}
		});

		$d.html($r = $('<div class="hint-container"></div>').html($d.html()));

		if ($d.hasClass('resizable')) {
			$d.resizable();
		}

		$d.parent().css('position', 'relative');
		this.style.display = 'none';

		const icon = $d.data('icon') || 'fa fa-question';
		const hint = $d.data('title') || 'Help';
		const $q = $('<i id="button_' + id + '" class="button hintbutton ' + icon + '" title="' + hint + '"/>').on('click', function () {
			$d.toggle();
		});

		this.parentNode.insertBefore($q.get(0), this);

		if (!$('.hintclose', $d).length) $('<i class="button hintclose fa fa-times" />').prependTo($d).on('click', function () {
			$d.hide();
		});

		var ps = $d.data('parent');
		if (ps) $d.appendTo($(ps));
	}).trigger('init-hint');
}
