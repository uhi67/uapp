<?php
/**
 * Fa module
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 * @package modules
 */

/** @noinspection PhpClassNamingConventionInspection */

/**
 * Fa module is a simple wrapper for font-awsome css.
 *
 * ##Usage
 *
 * 1. Call `Fa::init($this);` in the AppPage::appConstruct() or AnyPage::preConstruct()
 * 2. Use `fa fa-xxx` classes in your XSL views as usual
 */
class Fa {
    /**
     * @param UAppPage $page
     *
     * @throws ConfigurationException
     * @throws InternalException
     */
    public static function init($page) {
        $page->addCss('Fa/css/font-awesome.min.css');
    }
}
