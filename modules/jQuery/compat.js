/**
 *	jQuery compatibility hack for namespace problem in XSL-generated XHTML documents
 */

/**
 * Use the given object to override the given methods in its prototype
 * with namespace-aware equivalents
 */
function addNS(obj, methods) {
	var proto = obj.constructor.prototype;
	var ns = document.documentElement.namespaceURI;

	for (var methodName in methods) {
		(function () {
			var methodNS = proto[methodName + "NS"];

			if (methodNS) {
				proto[methodName] = function () {
					var args = Array.prototype.slice.call(arguments, 0);
					args.unshift(ns);
					return methodNS.apply(this, args);
				};
			}
		})();
	}
}

// Play nice with IE -- who doesn't need this hack in the first place
if (document.constructor) {
	// Override document methods that are used by jQuery
	addNS(document, {
		createElement: 1,
		getElementsByTagName: 1
	});

	/* -- Ez kiiktatva, mert több kárt okoz, mint hasznot --
		// Override element methods that are used by jQuery
		addNS(document.createElement("div"), {
			getElementsByTagName: 1,
			getAttribute: 1,
			getAttributeNode: 1,
			removeAttribute: 1,
			setAttribute: 1
		});
	*/
}
