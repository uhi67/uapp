/*
	Menu module for UApp
    menu.js
*/

(function ($) {
	/* onload */
	$(function () {
		/* (context) menu automata gombjaihoz form submit */
		$('.menu .autosubmit').click(function () {
			//console.log('Autosubmit');
			if ($(this).hasClass('disabled')) return false;

			var $form = $(this).closest('form');
			if (!$form.length) return console.log('A menü nem űrlapon van!');
			if ($form.find('input[name=act]').val($(this).data('act')).length === 0) console.log($form.attr('name') + ' űrlapon az act változó hiányzik!');

			if ($(this).data('confirm')) {
				UApp.UConfirm(
					$(this).data('title') ? $(this).data('title') : 'Megerősítés',
					$(this).data('confirm'),
					function () {
						if ($(this).data('click')) $(this).data('click')();
						$form.submit();
					}
				);
			} else {
				if ($(this).data('click')) $(this).data('click')();
				$form.submit();
			}
		});

		/* select gombok akciója */
		$('.menu .select').click(function () {
			var target = $(this).data('target');
			if (target === '') return console.log('menu.select: data-target is missing');
			if ($(this).hasClass('disabled')) return false;
			var title = $(this).data('title');
			var inpid = $('#input_' + this.id + '_id', this.parentNode.parentNode);
			var inp = $('#input_' + this.id + '_name', this.parentNode.parentNode);
			var params = $(this).data('param');
			var offset = $(this).offset();
			var item = this;

			console.log('select', target);

			SelectFrom.selectFrom('select_' + target /*+'.php'*/, title, inpid, inp, 1, params, {
				after: function () {
					console.log('after', item);
					var $form = $(item).closest('form');
					if (!$form.length) return console.log('A menü nem űrlapon van!');
					if ($form.find('input[name=act]').val($(item).data('act')).length === 0) console.log($form.attr('name') + ' űrlapon az act változó hiányzik!');
					$form.submit();
				},
				top: parseInt(offset.top)
			});
		});
	});
}(jQuery));
