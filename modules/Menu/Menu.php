<?php
/**
 * The Menu module
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright 2018
 * @license MIT
 * @package modules
 */

/**
 * # Menu
 *
 * Using (legacy)
 * --------------
 * In the controller:
 * ```
 * $this->register('Menu'); // in the preconstruct
 * $this->addContextMenu($somenode, $menudata); // in the action
 * ```
 *
 * In the view (this already included in lists):
 * ```
 *    <xsl:apply-templates select="menu" mode="context-menu">
 *        <xsl:with-param name="divid">context-menu-list</xsl:with-param>
 *    </xsl:apply-templates>
 * ```
 *
 * Using (new)
 * --------------
 * In the controller:
 * ```
 * $this->register('Menu'); // in the preconstruct
 * $menu = new Menu($this, ['nodename'=>'otherthanmenu']);    // 1. Modules need a page 2. may override the default nodename
 * $menu->addItems([['caption'=>'One']]);
 * $menu->createNode($somenode);
 * ```
 *
 * or
 *
 * ```
 * $this->register('Menu'); // in the preconstruct
 * $menu = (new Menu($this, ['items'=>[
 *        ['caption'=>'One', 'enabled'=>true, 'url'=>'ide'],
 *        ['caption'=>'Two'],
 * ]]))->createNode($somenode);
 * ```
 *
 * In the view:
 * ```
 *    <xsl:apply-templates select="menu" mode="normal">
 *        <xsl:with-param name="tooltip">Description for the menu.</xsl:with-param>
 *    </xsl:apply-templates>
 * ```
 *
 * ## Menu properties:
 * - nodename (default is menu)
 * - items (may be added later by {@see addItems()} and {@see addItem()})
 * - options (creates attributes in XML -- not used)
 * - attributes (creates attributes in HTML div)
 * - ulAttributes -- attributes for menu node in XML -- not used
 *
 * see {@see addItem} for menu item properties.
 */
class Menu extends Xlet {
    // Constants about displaying context menu items in cases of row selections
    const BLOCK_NO = 0; // hide in selections
    const BLOCK_YES = 1; // display only in selections
    const BLOCK_FIX = 2; // display always

    /** @var array $items -- the items added to menu */
    public $items = [];
    /** @var string $nodename -- the XML nodename of the menu mode, default is 'menu' */
    public $nodename = 'menu';
    /** @var array $attributes -- the attributes for menu DIV */
    public $attributes = [];
    /** @var array $options -- attributes for menu node in XML -- not used */
    public $options = [];
    /** @var array $ulAttributes -- attributes for menu node in XML -- not used */
    public $ulAttributes = [];

    /**
     * Processes runtime actions when request contains act=module
     *
     * @return void
     */
    function action() {
    }

    /**
     * Creates the menu node in the page document
     *
     * @param UXMLElement|UAppPage|Xlet $node_parent
     * @param null $options -- not used
     * *
     * @return UXMLElement the result node
     * @throws InternalException
     * @throws UAppException
     */
    function createNode($node_parent = null, $options = null) {
        $node_parent = $this->parentNode($node_parent);
        /** @var UXMLDoc $doc */
        if (!(($doc = $node_parent->ownerDocument) instanceof UXMLDoc)) throw new UAppException('Invalid parent');
        $this->setNode($node_parent->getOrCreateElement($this->nodename));
        foreach ($this->options as $name => $value) $this->node->setAttribute($name, $value);
        if ($this->attributes) {
            $node_attributes = $this->node->addNode('attributes');
            foreach ($this->attributes as $name => $value) $node_attributes->setAttribute($name, $value);
        }
        if ($this->ulAttributes) {
            $node_attributes = $this->node->addNode('ul-attributes');
            foreach ($this->ulAttributes as $name => $value) $node_attributes->setAttribute($name, $value);
        }
        return $this->createItems();
    }

    /**
     * ## Adds an item to the menu
     *
     * Special values:
     * - add `@`target to url if a.target is needed
     * - add ##class to url if a.class is needed
     *
     * Data is a [key=>value...] array, like:
     * - enabled,
     * - caption,
     * - url,
     * - hint,
     * - confirm
     * - class -> 'a class'
     * - li-class -> 'li class'
     * - autosubmit -> act
     * - act -> act
     * - tooltip = message
     * - block = only in list-context menus: 0 (non-block), 1 (block), 2 (fix) see BLOCK_XXX constants
     * - id -> 'a id'
     * - icon,
     * - data-* values
     * - any other keys
     *
     * url may be an array, then createUrl() will be applied.
     *
     * Warning: use `confirm` only with `url` or `click`. If the menu item triggers custom action, built-in confirm will fail.
     * Ih this case use UApp.UConfirm() in your custom event handler.
     *
     * @param array $data {enabled, caption, url, hint, confirm, ...}
     * @return Menu
     * @throws InternalException
     */
    public function addItem($data) {
        if (!ArrayUtils::isAssociative($data, true)) throw new InternalException('Menu item must be an associative array');
        $this->items[] = $data;
        return $this;
    }

    /**
     * ## Adds multiple items to the menu
     * See {@see addItem()} for details
     *
     * @param array $data -- array of item data: {enabled, caption, url, hint, confirm, attributes}
     * @return Menu
     */
    public function addItems($data) {
        $this->items = array_merge($this->items, $data);
        return $this;
    }

    /**
     * ## Generates the previously defined items in the last menu node
     *
     * Item data contains:
     * - enabled,
     * - caption,
     * - url -- the link of the menu item
     *    - normal string url
     *    - javascript string: click event will be applied
     *    - array: url generated by createUrl()
     * - hint,
     * - confirm,
     * - attributes (deprecated: all other keys are attributes)
     * - any other keys
     *
     * @return UXMLElement -- the menu node
     * @throws InternalException
     * @throws Exception
     */
    private function createItems() {
        foreach ($this->items as $item) {
            if ($item === null) continue;
            Util::assertArray($item);
            $itemnode = $this->lastNode->ownerDocument->createElement('item');
            $itemnode = $this->lastNode->appendChild($itemnode);
            $itemnode->setAttribute('enabled', $enabled = (ArrayUtils::fetchValue($item, 'enabled', null) ? 1 : 0));
            $icon = ArrayUtils::fetchValue($item, 'icon', null);
            $caption = ArrayUtils::fetchValue($item, 'caption', null);
            if ($icon) $itemnode->setAttribute('icon', $icon);
            if ($caption || !$icon) $itemnode->setAttribute('caption', $caption ?: 'undefined');

            $click = ArrayUtils::fetchValue($item, 'click', null);

            $url = ArrayUtils::fetchValue($item, 'url', '');
            if (is_array($url)) $url = Util::createUrl($this->page->baseurl, $url);
            $jp = 'javascript:';
            if ($url && substr($url, 0, strlen($jp)) == $jp && !$click) {
                $click = substr($url, strlen($jp));
                $url = '';
            }
            if ($click)
                $itemnode->setAttribute('click', $enabled ? $click : '');
            if ($url) {
                if (strpos($url, '##') !== false) {
                    list($url, $class) = explode('##', $url);
                    $itemnode->setAttribute('class', $class);
                }
                if (strpos($url, '@') !== false) {
                    list($url, $target) = explode('@', $url);
                    $itemnode->setAttribute('target', $target);
                }
                $itemnode->setAttribute('url', $url);
            }

            if (isset($item['attributes'])) {
                foreach ($item['attributes'] as $key => $value) {
                    if (!is_numeric($key)) $itemnode->setAttribute($key, $value);
                }
                unset($item['attributes']);
            }

            // All other keys are attributes of item node.
            $itemnode->addAttributes($item);
        }
        return $this->lastNode;
    }
}
