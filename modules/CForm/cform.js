/*
	cform.js
	module CForm

*/

(function ($) { // Ez bezárja a jQuery névtérbe
	$(function () { // Ez regisztrálja betöltésre
		$('form.cform').submit(function (evt) {
			let ready = true;
			$(this).find('input.mandatory').each(function () {
				if (ready && this.value === '') {
					ready = false;
					const $dialog = $('#dialog_mandatory').data('fitem', this);
					$dialog.children('#fmname').text($('label[for=' + this.id + ']').text() || this.title || this.name);
					$dialog.dialog('open');
					$(this).focus();
					$(this).tooltip({
						items: document,
						content: "Kötelező mező",
						show: {effect: "clip", duration: 500}
					}).tooltip('open');
				}
			});
			$(this).find('input.mac').each(function () {
				if (ready) checkMac(this);
			});
			if (!ready) evt.preventDefault();
			return ready;
		});
		$('div.dialog').dialog({
			autoOpen: false, modal: true, title: 'Az űrlap nem küldhető el.',
			close: function () {
				setTimeout(function (dialog) {
					$(dialog).data('fitem').focus();
				}, 200, this);
			}
		});
		const $inputMac = $('input.mac');
		$inputMac.keyup(function () {
			$(this).removeClass('invalid');
		});
		$inputMac.change(function (evt) {
			if (!checkMac(this)) $(this).addClass('invalid');
			evt.preventDefault();
		});
		$('input.delete').after('<span class="button" title="Nincs lejárat, visszavonásig működik.">X</span>').next().click(function () {
			$(this).prev().val('');
		});
		$('input.datepicker').datepicker({
			dateFormat: 'yy.mm.dd',
			constrainInput: true,
			dayNamesMin: ['H', 'K', 'Sz', 'Cs', 'P', 'Sz', 'V'],
			monthNamesShort: ["Jan", "Febr", "Márc", "Ápr", "Máj", "Jún", "Júl", "Aug", "Szept", "Okt", "Nov", "Dec"],
			monthNames: ["Január", "Február", "Március", "Április", "Május", "Június", "Július", "Augusztus", "Szeptember", "Október", "November", "December"],
			showMonthAfterYear: true,
			gotoCurrent: true,
			showAnim: 'clip',
			buttonText: 'választ'
		});
		$('input.fromdate').datepicker('option', 'onSelect', function (date) {
			$('input.todate').datepicker('option', 'minDate', date);
		});
		$('input.todate').datepicker('option', 'onSelect', function (date) {
			$('input.fromdate').datepicker('option', 'maxDate', date);
		});
	});

	function checkMac(field) {
		if (/^[\dA-Fa-f]{2}[:.-]?[\dA-Fa-f]{2}[:.-]?[\dA-Fa-f]{2}[:.-]?[\dA-Fa-f]{2}[:.-]?[\dA-Fa-f]{2}[:.-]?[\dA-Fa-f]{2}$/.test(field.value)) return true;
		const $dialog = $('#dialog_invalid').data('fitem', field);
		$dialog.children('#fmname').text($('label[for=' + field.id + ']').text() || field.title || field.name);
		$dialog.dialog('open');
		return false;
	}
})(jQuery);
