Füles vezérlő használati példák
===============================

Kétféleképpen használható: szerver oldali és kliens oldali lapozással.

1. Szerver oldali lapozás

-------------------------

### php kód

```php
$this->register('TabControl');
//...
$tabs = array(
    array('Cím-1, 'tipp-1', $enabled1),
    array('Cím-2', 'tipp-2', $enabled2),
    //...
);

$tabcontrol = new TabControl($this->node_parent, $tabname, $tabs, $classname, $default, 0);
$tab = $tabcontrol->tab;
// A kiválaszottt fül adatainak betöltése
if($tab==1) $this->loadTabData1();
if($tab==2) $this->loadTabData2();
```

### xslt kód

```$xslt
<xsl:apply-templates select="tabcontrol" />
<xsl:choose>
    <xsl:when test="tabcontrol/@current=1"><xsl:call-template name="tabdata1" /></xsl:when>
    <xsl:when test="tabcontrol/@current=2"><xsl:call-template name="tabdata2" /></xsl:when>
    ...
</xsl:choose>
```

2. Kliens oldali lapozás

------------------------

### php kód

```php
$this->register('TabControl');
//...
$tabs = array(
    array('Cím-1, 'tipp-1', $enabled1),
    array('Cím-2', 'tipp-2', $enabled2),
    //...
);

$tabcontrol = new TabControl($this->node_parent, $tabname, $tabs, $classname, $default, 1);
// Az összes fül adatainak betöltése. Az adat olyan szerkezetű legyen, hogy ne keveredjenek össze.
$this->loadTabData1();
$this->loadTabData2();
```

### xslt kód

Ebben az esetben a tabcontrol/@current értéke mindig $default (1), a tab/@id szerint kell megjeleníteni
a fülek tartalmát. A lapok rejtését és megjelenítéét a modul intézi.

```$xslt
<xsl:apply-templates select="tabcontrol" />

<xsl:template match="tab[@id=1]" mode="user-content">
	<!-- 1. fül adatainak megjelenítése -->
	<xsl:call-template name="tabdata1" />
</xsl:template>

<xsl:template match="tab[@id=2]" mode="user-content">
	<xsl:call-template name="tabdata2" />
</xsl:template>
```
