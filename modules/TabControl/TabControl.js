/**
 * TabControl.js
 * javascript part for Tabcontrol module
 */

/**
 * Support for initializing later loaded DOM elements, e.g. ajax-loaded popup windows, @see Selectfrom
 */
$(function () {
	jQuery(document).on('init', '.init-controls', function () {
		initTabs(this);
	});

	initTabs(document);

	$('.focused').each(function () {
		this.scrollIntoView();
	});
});

/**
 * Initializes all tabcontrols within base element (e.g. document)
 */
function initTabs(base) {
	$('div.tab-container', base).each(function () {
		var tabcontainer = this;
		var $tabcontrol = $('.tabcontrol', tabcontainer); // 'div.tabcontrol' fails (bug?)
		var tabcontrol = $tabcontrol.get(0);
		var name = tabcontrol.id.substr('tabcontrol_'.length);
		var $form = $tabcontrol.closest('form');
		var $tabvar = $('input#tabvar_' + name, $form);

		if (!$form) {
			console.log('Nincs tabcontrol form');
			return;
		}
		$tabcontrol.find('.tab').click(function () {
			if ($(this).hasClass('disabled')) return false;
			//console.log('tabcontrol click');
			if ($(this).hasClass('tab_instant_1')) {
				var n = $(this).data('id');
				$('.tab_content', tabcontainer).hide();
				$('div#tab_' + n, tabcontainer).show();
				$('.tab.active', tabcontainer).removeClass('active');
				$('span#tab_' + n, tabcontainer).addClass('active');
			} else {
				$tabvar.val($(this).data('id'));
				$('input#focus_' + name, $form).val(1);
				$form.submit(); // jQuery submit may be captured
			}
		});
	});
}
