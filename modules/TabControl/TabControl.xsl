<?xml version="1.0" encoding="UTF-8"?>
<!-- tab.inc.xsl - v3.0 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="xml" version="1.0" indent="yes"
	            doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	            doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	            omit-xml-declaration="yes"
	/>

	<!--
		Tab control

		Using
			<xsl:call-template name="tabcontrol">
				<xsl:with-param name="tabs" select="'id;caption;url;hint|...|'" />
				<xsl:with-param name="current"  />

		... or
			<xsl:apply-templates select="tabcontrol"/>

		Contents
			<xsl:template name="tabcontrol">
			<xsl:template name="tabcontrol_click">
			<xsl:template name="tab">
			<xsl:template match="tabcontrol">
			<xsl:template name="tabtitle"> #Egy címet hoz ki belőle
		Callbacks
			<xsl:template match="tab" mode="user_content">
	-->
	<!-- ================================================================================== -->
	<xsl:template name="tabcontrol">
		<xsl:param name="tabs"/>    <!-- id;caption;url;hint|...| -->
		<xsl:param name="current" select="0"/>
		<div class="nav_tab_box">
			<div id="tab_{@name}" class="tabcontrol {@class}">
				<div id="tabcontrol-menu" class="tooltip inline"
				     data-title="Füles vezérlő. Az aktuális oldal címkéje világos színű. Kattints a kívánt fülre!">
					<xsl:call-template name="tab">
						<xsl:with-param name="tabs" select="$tabs"/>
						<xsl:with-param name="current" select="$current"/>
					</xsl:call-template>
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template name="tab"><!-- id;caption;url;hint|...| -->
		<xsl:param name="tabs"/>
		<xsl:param name="current"/>
		<xsl:param name="pos" select="1"/>
		<xsl:if test="$tabs!=''">
			<xsl:variable name="tab_i" select="substring-before($tabs, '|')"/>
			<xsl:variable name="tab_r" select="substring-after($tabs, '|')"/>
			<xsl:variable name="caption" select="substring-before($tab_i, ';')"/>
			<xsl:variable name="urlhint" select="substring-after($tab_i, ';')"/>
			<xsl:variable name="url" select="substring-before($urlhint, ';')"/>
			<xsl:variable name="hint" select="substring-after($urlhint, ';')"/>

			<xsl:choose>
				<xsl:when test="$pos=$current">
					<a href="{$url}" class="active" title="{$hint}">
						<xsl:value-of select="$caption"/>
					</a>
				</xsl:when>
				<xsl:otherwise>
					<a href="{$url}" title="{$hint}">
						<xsl:value-of select="$caption"/>
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="$tab_r!=''">
				<xsl:call-template name="tab">
					<xsl:with-param name="tabs" select="$tab_r"/>
					<xsl:with-param name="current" select="$current"/>
					<xsl:with-param name="pos" select="$pos + 1"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:if>
	</xsl:template>

	<!-- ================================================================================== -->
	<xsl:template match="tabcontrol">
		<xsl:variable name="tabname">
			<xsl:choose>
				<xsl:when test="@varname">
					<xsl:value-of select="@varname"/>
				</xsl:when>
				<xsl:when test="@name">
					<xsl:value-of select="@name"/>
				</xsl:when>
				<xsl:otherwise>tab_<xsl:value-of select="generate-id()"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="baseurl">
			<xsl:choose>
				<xsl:when test="@url != ''">
					<xsl:value-of select="@url"/>
				</xsl:when>
				<xsl:when test="not(/data/@url != '')">
					<xsl:value-of select="../@script"/>
				</xsl:when>
				<xsl:when test="contains(/data/@url, '&amp;traced=')">
					<xsl:value-of select="substring-before(/data/@url, '&amp;traced=')"/>
				</xsl:when>
				<xsl:when test="/data/@url != ''">
					<xsl:value-of select="/data/@url"/>
				</xsl:when>
				<xsl:otherwise>notabur.php</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<a name="tabform_{$tabname}" tabindex="99">
			<xsl:if test="@focus=1">
				<xsl:attribute name="class">focused</xsl:attribute>
			</xsl:if>
		</a>
		<div id="tab_{@varname}" class="tab-container {@class}">
			<xsl:choose>
				<xsl:when test="@standalone=1 or @standalone='true'">
					<form id="tab_{$tabname}" action="{$baseurl}" method="post">
						<xsl:apply-templates select="." mode="in-form"/>
					</form>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="." mode="in-form"/>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="@instant=1 or @instant='true'">
				<xsl:for-each select="tab">
					<xsl:variable name="hidden">
						<xsl:if test="@id!=../@current">display:none;</xsl:if>
					</xsl:variable>
					<div id="tab_{@id}" class="tab_content" data-id="{@id}" style="{$hidden}">
						<xsl:apply-templates select="." mode="user-content"/>
					</div>
				</xsl:for-each>
			</xsl:if>
		</div>
	</xsl:template>

	<xsl:template match="tabcontrol" mode="in-form">
		<xsl:variable name="varname">
			<xsl:choose>
				<xsl:when test="@varname">
					<xsl:value-of select="@varname"/>
				</xsl:when>
				<xsl:otherwise>tab</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<input id="tabvar_{$varname}" class="tabvar" type="hidden" name="{$varname}" value="{@current}"/>
		<input id="focus_{$varname}" type="hidden" name="focus_{$varname}" value="0"/>
		<xsl:apply-templates select="." mode="variables"/> <!-- callback -->
		<div class="nav_tab_box">
			<div id="tabcontrol_{$varname}" class="tabcontrol">
				<div id="tabcontrol-menu" class="tooltip inline"
				     data-title="Füles vezérlő. Az aktuális oldal címkéje világos színű. Kattints a kívánt fülre!">
					<xsl:apply-templates select="tab"/>
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="tab">
		<xsl:variable name="active">
			<xsl:if test="@id=../@current">active</xsl:if>
		</xsl:variable>
		<xsl:variable name="disabled">
			<xsl:if test="not(@enabled=1 or @enabled='true')">disabled</xsl:if>
		</xsl:variable>
		<xsl:variable name="instant">
			<xsl:choose>
				<xsl:when test="../@instant=1 or ../@instant='true'">1</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="@url">
				<a id="tab_{@id}" class="tab {$active} {$disabled} {@class}" href="{@url}" title="{@title}">
					<xsl:apply-templates select="." mode="icon"/>
					<xsl:value-of select="@caption"/>
				</a>
			</xsl:when>
			<xsl:otherwise>
				<span id="tab_{@id}" class="tab {$active} {$disabled} tab_instant_{$instant} {@class}" data-id="{@id}"
				      title="{@title}">
					<xsl:apply-templates select="." mode="icon"/>
					<xsl:value-of select="@caption"/>
				</span>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="tab" mode="icon" name="tab-icon">
		<xsl:if test="@icon">
			<i class="fa {@icon}" aria-hidden="true"></i>
		</xsl:if>
	</xsl:template>

	<xsl:template match="tab" mode="user-content">
		<p>[add tab sheet user content in override]</p>
	</xsl:template>

	<!-- ================================================================================== -->
	<xsl:template name="tabtitle"> <!-- Egy címet hoz ki belőle -->
		<xsl:param name="tabs"/>    <!-- caption;url;hint|...| -->
		<xsl:param name="current" select="0"/>
		<xsl:call-template name="tabtitle1">
			<xsl:with-param name="tabs" select="$tabs"/>
			<xsl:with-param name="current" select="$current"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="tabtitle1"><!-- caption;url;hint|...| -->
		<xsl:param name="tabs"/>
		<xsl:param name="current"/>
		<xsl:param name="pos" select="1"/>
		<xsl:if test="$tabs!=''">
			<xsl:variable name="tab_i" select="substring-before($tabs, '|')"/>
			<xsl:variable name="tab_r" select="substring-after($tabs, '|')"/>
			<xsl:variable name="caption" select="substring-before($tab_i, ';')"/>
			<xsl:variable name="urlhint" select="substring-after($tab_i, ';')"/>
			<xsl:variable name="url" select="substring-before($urlhint, ';')"/>
			<xsl:variable name="hint" select="substring-after($urlhint, ';')"/>
			<xsl:choose>
				<xsl:when test="$pos=$current">
					<xsl:value-of select="$caption"/>
				</xsl:when>
				<xsl:otherwise>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="$tab_r!=''">
				<xsl:call-template name="tabtitle1">
					<xsl:with-param name="tabs" select="$tab_r"/>
					<xsl:with-param name="current" select="$current"/>
					<xsl:with-param name="pos" select="$pos + 1"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	<!-- ================================================================================== -->
	<!--  ugyanaz, mint tabcontrol, csak url helyett onclick akciók vannak benne -->
	<xsl:template name="tabcontrol_click">
		<xsl:param name="tabs"/>    <!-- caption;click;hint|...| -->
		<xsl:param name="current" select="0"/>
		<div id="nav_tab_box">
			<div id="nav_tab">
				<xsl:call-template name="tab_click">
					<xsl:with-param name="tabs" select="$tabs"/>
					<xsl:with-param name="current" select="$current"/>
				</xsl:call-template>
			</div>
		</div>
	</xsl:template>

	<xsl:template name="tab_click"><!-- caption;click;hint|...| -->
		<xsl:param name="tabs"/>
		<xsl:param name="current"/>
		<xsl:param name="pos" select="1"/>
		<xsl:if test="$tabs!=''">
			<xsl:variable name="tab_i" select="substring-before($tabs, '|')"/>
			<xsl:variable name="tab_r" select="substring-after($tabs, '|')"/>
			<xsl:variable name="caption" select="substring-before($tab_i, ';')"/>
			<xsl:variable name="clickhint" select="substring-after($tab_i, ';')"/>
			<xsl:variable name="click" select="substring-before($clickhint, '~')"/>
			<xsl:variable name="hint" select="substring-after($clickhint, '~')"/>

			<span onclick="{$click}" style="cursor:pointer">
				<xsl:if test="$pos=$current">
					<xsl:attribute name="class">active</xsl:attribute>
				</xsl:if>
				<xsl:value-of select="$caption"/>
			</span>

			<xsl:if test="$tab_r!=''">
				<xsl:call-template name="tab_click">
					<xsl:with-param name="tabs" select="$tab_r"/>
					<xsl:with-param name="current" select="$current"/>
					<xsl:with-param name="pos" select="$pos + 1"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>
