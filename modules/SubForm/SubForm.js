/*
    SubForm.js

    toggles hidden parts
*/
(function ($) {
	$(function () {
	});
})(jQuery);

function sfStart(head, content) {
	$(head).hide();
	$(content).show();
	return false;
}

function sfCancel(head, content) {
	$(head).show();
	$(content).hide();
	return false;
}
