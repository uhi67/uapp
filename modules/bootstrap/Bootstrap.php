<?php

/**
 * Bootstrap module
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 * @package modules
 */

/** @noinspection PhpClassNamingConventionInspection */

/**
 * Bootstrap module is a simple wrapper for Bootstrap css and js.
 *
 * ##Usage
 *
 * 1. Call `Bootstrap::init($this);` in the AppPage::appConstruct() or AnyPage::preConstruct()
 * 2. Use Bootstrap classes in your XSL views as usual
 */
class Bootstrap {
    /**
     * @param UAppPage $page
     *
     * @throws ConfigurationException
     * @throws InternalException
     */
    public static function init($page) {
        $page->addCss('bootstrap/css/bootstrap-grid.css');
        $page->addJs('bootstrap/js/bootstrap.js'); // bootstrap-bundle.js
    }
}
