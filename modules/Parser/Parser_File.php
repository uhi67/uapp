<?php
/**
 * The Parser module
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright 2014-2016
 * @license MIT
 * @package modules
 */

/** @noinspection PhpClassNamingConventionInspection */

/**
 * A parser based on an opened text file
 *
 * @filesource    Parser_File.php (_ is important in the filename, UApp _loader will find the file in the module in this way)
 * @author Peter Uherkovich
 * @copyright 2015
 *
 * Using:
 *
 *    $syntax = file_get_contents('syntax.xml');
 *    $p = new ParserFile($fh, $syntax);
 *    $r = $p->apply();
 *    fclose($fh);
 *
 */
class Parser_File extends Parser {
    private $fh;

    /**
     * Creates a parser instance on an opened file and given XML string syntax
     *
     * @param resource $fh -- text file to parse
     * @param string $syntax -- an XML string
     */
    function __construct($fh, $syntax) {
        parent::__construct($syntax);
        $this->fh = $fh;
    }

    function getc() {
        return fgetc($this->fh);
    }

    function ungetc($c) {
        fseek($this->fh, -1, SEEK_CUR);
    }

    function getPos() {
        return ftell($this->fh);
    }

    /**
     * @param int $p
     *
     * @throws ParserException
     */
    function setPos($p) {
        if (fseek($this->fh, $p) == -1) throw new ParserException("Seek error at $p");
    }

}
