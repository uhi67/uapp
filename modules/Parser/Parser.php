<?php
/**
 * The Parser module
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright 2014-2016
 * @license MIT
 * @package modules
 */

/**
 * Exception occurred in Parser
 */
class ParserException extends Exception {

}

/**
 * Parser based on XML syntax definitions
 * Use descendants of abstract Parser
 * @see Parser_File
 */
abstract class Parser {
    /** @var UXMLDoc $syntax */
    private $syntax;
    private $cut;
    /** @var UXMLDoc $result */
    public $result;
    public $debug;

    function __construct($syntax) {
        $this->cut = [];
        $this->syntax = new UXMLDoc();
        $this->syntax->loadXML($syntax);
        $this->result = new UXMLDoc();
        $this->result->formatOutput = true;
    }

    /**
     * Must return a char or false on EOF
     * @return string
     */
    abstract function getc();

    abstract function ungetc($c);

    abstract function getPos();

    abstract function setPos($p);

    /**
     * Applies syntax on input.
     * Finds default match in syntax file and applies getSymbol(name) of it.
     * Builds XML representation structure
     * <defaultname>
     *    <submatch1>...</submatch1>
     *    ...
     * </defaultname>
     *
     * @return UXMLElement result node or null
     * @throws ConfigurationException
     * @throws Exception
     * @throws InternalException
     * @throws ParserException
     */
    function apply() {
        $defaultnodes = $this->syntax->documentElement->selectNodes("match");
        if ($defaultnodes->length != 1) throw new ParserException('Must be exactly one main match node.');
        /** @noinspection PhpUndefinedMethodInspection */
        $name = $defaultnodes->item(0)->getAttribute('name');
        $result_node = $this->getSymbol($name);
        return $result_node;
    }

    /**
     * Tries to read a symbol by named rule.
     * Inserts result into result structure. Returns result node or false on failure
     *
     * @param String $name
     *
     * @return UXMLElement -- result node
     * @throws ConfigurationException
     * @throws Exception
     * @throws InternalException
     * @throws ParserException
     */
    function getSymbol($name) {
        $this->cut = [];

        /** @var UXMLElement $node_symbol */
        $node_symbol = $this->result->createElement($name);
        if (!$this->result->documentElement) $node_symbol = $this->result->appendChild($node_symbol);
        else $node_symbol = $this->result->documentElement->appendChild($node_symbol);

        if (!$this->matchRule($name, $node_symbol)) return null;
        $this->joinAttr($node_symbol);
        return $node_symbol;
    }

    /**
     * tries to match a rule once (several rule versions or-ed)
     *
     * @param string $name -- rule name
     * @param DOMElement|DOMDocumentFragment $node_parent -- inserts result tree here on success
     *
     * @return boolean -- false on fail (tree result will be untouched)
     * @throws ConfigurationException
     * @throws Exception
     * @throws InternalException
     * @throws ParserException
     */
    function matchRule($name, $node_parent) {
        if ($this->debug) {
            if (!$node_parent) Debug::tracex('matchRule', 'nincs szülőnode', '#A0A');
            #Debug::tracex("matchRule", $name);
        }
        array_unshift($this->cut, false);
        $rules = $this->syntax->documentElement->selectNodes("rule[@name='$name']");
        if ($this->debug) Debug::tracex("rule count:", $rules->length);
        $r = false;
        /** @var DOMElement $rule */
        foreach ($rules as $rule) {
            if ($this->debug) Debug::tracex("rule #" . $rule->getLineNo());
            if ($this->matchSeq($rule, $node_parent, 0)) {
                $r = true;
                break;
            }
            if ($this->cut[0]) {
                // Hibakezelés?
                break;
            }
        }
        array_shift($this->cut);
        /*
        if($this->debug) {
            if($r) Debug::tracex("rule $name succeeded", $this->export($node_parent),'#090');
            else Debug::tracex("rule $name failed", $this->export($node_parent),'#900');
        }
        */
        return $r;
    }

    /**
     * matches a sequence of patterns under the node, and collects the result tree.
     * succeeds only if all of members suceeded consecutively
     *
     * @param DOMElement $rule -- evaluate all children of rule node
     * @param DOMElement|DOMDocumentFragment $node_parent -- inserts results here
     * @param int $f -- add chars to result
     *
     * @return boolean -- false if failed
     * @throws ConfigurationException
     * @throws Exception
     * @throws InternalException
     * @throws ParserException
     */
    function matchSeq($rule, $node_parent, $f = 0) {
        if ($this->debug) {
            if (!$node_parent) Debug::tracex('matchSeq', 'nincs szülőnode: ' . $this->export($rule), '#A0A');
            #$rulexml = $this->export($rule);
            #Debug::tracex("matchSeq", $rulexml);
        }
        $resultFragment = $this->result->createDocumentFragment();
        $p = $this->getPos();
        $r = true;

        if (!$rule->firstChild) {
            #if($this->debug) Debug::tracex("matchSeq", '(empty sequence)', '#90a');
            return true;
        }

        foreach ($rule->childNodes as $node) {
            if ($node->nodeType != XML_ELEMENT_NODE) continue;
            $r &= $this->matchNode($node, $resultFragment, $f);
            if (!$r) break;
        }

        if ($r) {
            /*
            if($this->debug) {
                Debug::tracex('matchSeq succeeded', $rulexml, '#090');
                Debug::tracex('matchSeq result', $this->export($resultFragment), '#009');
            }
            */
            if ($node_parent && $resultFragment->hasChildNodes()) {
                if ($resultFragment->childNodes->length == 1
                    && $resultFragment->firstChild->nodeType == XML_TEXT_NODE
                    && $node_parent->lastChild
                    && $node_parent->lastChild->nodeType == XML_TEXT_NODE) {
                    #if($this->debug) Debug::tracex('matchSeq append', array($node_parent->lastChild->nodeValue, $resultFragment->firstChild->nodeValue));
                    $node_parent->lastChild->nodeValue .= $resultFragment->firstChild->nodeValue;
                } else $node_parent->appendChild($resultFragment);
            }
            /*
            if($this->debug) {
                if(!$node_parent) Debug::tracex('matchSeq', '(nincs szülőnode) '.$this->export($rule), '#A0A');
                if(!$resultFragment->hasChildNodes()) Debug::tracex('matchSeq', 'nincs eredmény', '#B0A');
            }
            */
        } else {
            $this->setPos($p);
            #if($this->debug) Debug::tracex('matchSeq failed', $rulexml, '#930');
        }
        return $r;
    }

    /**
     * matches a node, and collects the result tree.
     * used in matchSequence and matchOr
     *
     * @param DOMElement $node -- syntax node
     * @param DOMElement|DOMDocumentFragment $resultFragment -- inserts results here
     * @param int $f -- add chars to result
     *
     * @return boolean -- false if failed
     * @throws ConfigurationException
     * @throws Exception
     * @throws InternalException
     * @throws ParserException
     */
    function matchNode($node, $resultFragment, $f = 0) {
        $r = true;
        #if($this->debug) Debug::tracex("node", $node->nodeName, '#960');
        switch ($node->nodeName) {
            case 'node':
                $name = $node->getAttribute('name');
                /** @var DOMElement $node_submatch */
                $node_submatch = $resultFragment->appendChild($this->result->createElement($name));
                $r &= $this->matchSeq($node, $node_submatch, $f);
                $this->joinAttr($node_submatch);
                #if(!$r) if($this->debug) Debug::tracex("node failed", array($name), '#960');
                break;
            case 'return':
                $r &= $this->matchSeq($node, $resultFragment, 1);
                /*
                if($this->debug && $r) {
                    $nodexml = $node->ownerDocument->saveXML($node);
                    Debug::tracex("return succeed", $nodexml, '#0c0');
                    Debug::tracex("return result", $this->export($resultFragment), '#00A');
                }
                */
                break;
            case 'attr':
                $v = null;
                $name = $node->getAttribute('name');
                if (!$name) throw new ParserException('Invalid attribute name');
                if ($value = $node->getAttribute('value')) {
                    $v = $value;
                } else {
                    #Debug::tracex("attr '$name' seq", $this->export($node));
                    $res1 = $this->result->createDocumentFragment();
                    $r &= $this->matchSeq($node, $res1, 0);
                    if ($r) {
                        #Debug::tracex("attr '$name' res", $this->export($res1));
                        $this->filterNodes($res1, '_attr');
                        $v = $res1->textContent;
                        #Debug::tracex("attr '$name' value", $v);
                    }
                }
                if ($v !== null) {
                    $attrnode = $this->result->createElement('_attr');
                    $attrnode->setAttribute('name', $name);
                    $attrnode->appendChild($this->result->createTextNode($v));
                    $resultFragment->appendChild($attrnode);
                }
                break;
            case 'value':
                #$value = $node->getAttribute('value');
                $value = $node->textContent;
                if ($f) {
                    #if($this->debug) Debug::tracex("value added", $value);
                    $lastChild = $resultFragment->lastChild;
                    if (!$lastChild || $lastChild->nodeType != XML_TEXT_NODE) $lastChild = $resultFragment->appendChild($this->result->createTextNode(''));
                    $lastChild->nodeValue = $lastChild->nodeValue . $value;
                }
                break;
            case 'char':
                $re = $node->nodeValue;
                $c = $this->getc();
                #f($this->debug) Debug::tracex("char $re", ord($c)<33 ? '('.ord($c).')' : $c);
                try {
                    if (!preg_match(Util::toRegExp($re), $c)) {
                        $this->ungetc($c);
                        #if($this->debug) Debug::tracex("char", "(failed)", '#900');
                        $r = false;
                    }
                } catch (Exception $e) {
                    $msg = $e->getMessage();
                    throw new Exception("Ipvalid pattern `~$re~`: $msg", 0, $e);
                }
                // Append to result
                if ($r && $f) {
                    #if($this->debug) Debug::tracex("char added", $c);
                    $lastChild = $resultFragment->lastChild;
                    while ($lastChild && $lastChild->nodeType != XML_TEXT_NODE) $lastChild = $lastChild->previousSibling;
                    if (!$lastChild || $lastChild->nodeType != XML_TEXT_NODE) $lastChild = $resultFragment->appendChild($this->result->createTextNode(''));
                    $lastChild->nodeValue = $lastChild->nodeValue . $c;
                }
                break;
            case 'term':
                $term = $node->nodeValue;
                $this->matchRule('s', null);
                $w = $this->getTerm($term);
                if ($w) $this->matchRule('s', null);
                if ($w === false) $r = false;
                if ($r && $f) {
                    #if($this->debug) Debug::tracex("term added", $term);
                    $lastChild = $resultFragment->lastChild;
                    if (!$lastChild || $lastChild->nodeType != XML_TEXT_NODE) $lastChild = $resultFragment->appendChild($this->result->createTextNode(''));
                    $lastChild->nodeValue = $lastChild->nodeValue . $term;
                }
                break;
            case 'list':    // default list min="0" max="inf"
                $min = (int)$node->getAttribute('min');
                $max = (int)$node->getAttribute('max');
                $r &= $this->matchList($node, $min, $max, $resultFragment, $f);
                break;
            case 'opt':        // same as list max="1"
                $r &= $this->matchList($node, 0, 1, $resultFragment, $f);
                break;
            case 'case':    // same as list min=1 max="1"
                $r &= $this->matchList($node, 1, 1, $resultFragment, $f);
                break;
            case 'or':        // elements or-ed
                $r &= $this->matchOr($node, $resultFragment, $f);
                break;
            case 'match':
                $rule = $node->getAttribute('rule');
                $name = $node->getAttribute('name');
                if ($rule == '' && $name != '') $rule = $name;
                if ($rule == '' && $name == '') throw new ParserException("invalid match in syntax");
                if ($name != '') $resultnode = $resultFragment->appendChild($this->result->createElement($name));
                else $resultnode = $resultFragment;
                $r &= $this->matchRule($rule, $resultnode);
                if ($name != '') $this->joinAttr($resultnode);
                break;
            case 'cut':
                $this->cut[0] = true;
                break;
            case 'temp':
                // TODO: kiértékeli (mint case), outputot is generál ha kell, de a végén az input pointert visszaállítja.
                $p = $this->getPos();
                $r &= $this->matchList($node, 1, 1, $resultFragment, $f);
                $this->setPos($p);
                break;
            case 'peek':
                if ($this->debug) {
                    $name = $node->getAttribute('name');
                    $length = $node->getAttribute('length');
                    if (!$length) $length = 15;
                    $p1 = $this->getPos();
                    $term = '';
                    for ($i = 0; $i < $length; $i++) $term .= $this->getc();
                    Debug::tracex("peek#$name", $term, '#f80');
                    $this->setPos($p1);
                }
                break;
            default:
                $rules = $this->syntax->documentElement->selectNodes("rule[@name='$node->nodeName']");
                if ($rules->length != 0) {
                    $r &= $this->matchRule($node->nodeName, $resultFragment);
                } else throw new ParserException("invalid node '$node->nodeName' in syntax");
                break;
        }
        return $r;
    }

    /**
     * 'list' matches if sequence matches [min, max] times.
     * Zero match terminates first time.
     *
     * @param DOMElement $node
     * @param int $min
     * @param int $max
     * @param DOMElement $node_parent
     * @param boolean $f -- add chars to result
     *
     * @return boolean
     * @throws ConfigurationException
     * @throws Exception
     * @throws InternalException
     * @throws ParserException
     */
    function matchList($node, $min, $max, $node_parent, $f) {
        /*
        if($this->debug) {
            $nodexml = $node->ownerDocument->saveXML($node);
            Debug::tracex("matchList", "$nodexml [$min, $max]", '#0c0');
        }
        */
        $i = 0;
        $p = $this->getPos();
        $r = true;
        while ($max == 0 || $i < $max) {
            $p1 = $this->getPos();
            $r1 = $this->matchSeq($node, $node_parent, $f);
            $p2 = $this->getPos();
            if ($i > 1000 || !$r1 || $p1 == $p2) break;
            $i++;
        }
        if ($i < $min) $r = false;
        if (!$r) {
            $this->setPos($p);
            #if($this->debug) Debug::tracex("matchList failed", array($nodexml), '#900');
        }
        /*
        if($this->debug) {
            Debug::tracex("matchList succeeded", $nodexml, '#0A0');
            Debug::tracex("matchList result", $this->export($node_parent), '#00A');
        }
        */
        return $r;
    }

    /**
     * 'or' matches if any of elements matches. (Use 'case' for group elements)
     * Matching element terminates early.
     *
     * @param DOMElement $node
     * @param DOMElement|DOMDocumentFragment $node_parent
     * @param boolean $f -- add chars to result
     *
     * @return boolean
     * @throws ConfigurationException
     * @throws Exception
     * @throws InternalException
     * @throws ParserException
     */
    function matchOr($node, $node_parent, $f) {
        /*
        if($this->debug) {
            if(!$node_parent) Debug::tracex('matchOr', 'nincs szülőnode: '.$this->export($node), '#A0A');
            $nodexml = $node->ownerDocument->saveXML($node);
            Debug::tracex("matchOr", $nodexml, '#00c');
        }
        */
        $p = $this->getPos();
        $r = false;
        $node1 = $node->firstChild;
        while ($node1) {
            $resultFragment = $this->result->createDocumentFragment();
            if ($node1->nodeType == XML_ELEMENT_NODE) {
                #if($this->debug) Debug::tracex("matchOr case", $node1->nodeName, '#909');
                $r |= $this->matchNode($node1, $resultFragment, $f);
                if ($r) {
                    if ($node_parent && $resultFragment->hasChildNodes()) {
                        if ($resultFragment->childNodes->length == 1
                            && $resultFragment->firstChild->nodeType == XML_TEXT_NODE
                            && $node_parent->lastChild
                            && $node_parent->lastChild->nodeType == XML_TEXT_NODE) {
                            #if($this->debug) Debug::tracex('matchSeq append', array($node_parent->lastChild->nodeValue, $resultFragment->firstChild->nodeValue));
                            $node_parent->lastChild->nodeValue .= $resultFragment->firstChild->nodeValue;
                        } else $node_parent->appendChild($resultFragment);
                    }
                    #if($this->debug) Debug::tracex("matchOr case succeed", $node1->nodeName, '#090');
                    break;
                } else {
                    #if($this->debug) Debug::tracex("matchOr case failed", $node1->nodeName, '#900');
                    $this->setPos($p);
                }
            }
            $node1 = $node1->nextSibling;
        }
        /*
        if(!$r) {
            if($this->debug) Debug::tracex("matchOr failed", array($nodexml), '#900');
        }
        */
        /*
        if($this->debug) {
            Debug::tracex("matchOr succeeded", $nodexml, '#0A2');
            Debug::tracex("matchOr result", $this->export($node_parent), '#00A');
        }
        */
        return $r;
    }

    function getTerm($term) {
        #if($this->debug) Debug::tracex("getTerm", $term);
        $p = $this->getPos();
        $s = '';
        for ($i = 0; $i < strlen($term); $i++) {
            $c = $this->getc();
            $s .= $c;
            if ($c != substr($term, $i, 1)) {
                $this->setPos($p);
                #if($this->debug) Debug::tracex("getTerm failed", "$term, '$s'", '#690');
                return false;
            }
        }
        #if($this->debug) Debug::tracex("getTerm succeeded", $term, '#039');
        return true;
    }

    /**
     * A megadott node tartalmát XML stringgé alakítja
     *
     * @param DOMElement $node
     * @param integer $ent -- 0:default, 1:htmlentities, 2:formatted
     * @param string $c -- színes span-ben adja vissza
     *
     * @return string -- az XML string
     * @throws Exception
     */
    function export($node, $ent = 0, $c = '') {
        if ($ent == 2) {
            $node->ownerDocument->formatOutput = false;
            $node->ownerDocument->preserveWhiteSpace = false;
        }
        if (!$node) $r = 'null';
        else {
            if (!$node->ownerDocument) throw new Exception('Érvénytelen DOM');
            $r = $node->ownerDocument->saveXML($node);
        }
        if ($ent == 1) $r = htmlentities($r);
        if ($c) $r = /** @lang text */
            '<span style="color:' . $c . '">' . $r . '</span>';
        return $r;
    }

    /**
     * Converts <_attr name>value</_attr> subnodes to attributes.
     * Duplicate attributes overwrite existing ones.
     * <_attr> subnodes without name attribute are be ignored
     * All other attributes and sub-subnodes are ignored
     * All <_attr> subnodes will be completely removed (including the irregular ones)
     *
     * @param DOMElement $node
     * @return void
     */
    function joinAttr($node) {
        $child = $node->firstChild;
        while ($child) {
            $next = $child->nextSibling;
            if ($child->nodeName == '_attr') {
                $name = $child->getAttribute('name');
                $value = $child->textContent;
                if ($name) $node->setAttribute($name, $value);
                $node->removeChild($child);
            }
            $child = $next;
        }
    }

    /**
     * Törli a megadott nevő gyermek node-okat.
     * Elsősorban az _attr node-ok eltávolítására szolgál, ahol a text tartalom kell csak.
     *
     * @param mixed $node
     * @param mixed $nodename
     * @return void
     */
    function filterNodes($node, $nodename) {
        $child = $node->firstChild;
        while ($child) {
            $next = $child->nextSibling;
            if ($child->nodeName == $nodename) {
                $node->removeChild($child);
            }
            $child = $next;
        }
    }
}
