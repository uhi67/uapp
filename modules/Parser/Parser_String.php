<?php
/**
 * The Parser module
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright 2014-2016
 * @license MIT
 * @package modules
 */

/** @noinspection PhpClassNamingConventionInspection */

/**
 * A Parser working on strings
 */
class Parser_String extends Parser {
    private $s;
    private $l;
    private $p;

    function __construct($s, $syntax) {
        parent::__construct($syntax);
        $this->s = $s;
        $this->p = 0;
        $this->l = strlen($s);
    }

    function getc() {
        if ($this->p >= $this->l) return false;
        return $this->s[$this->p++];
    }

    function ungetc($c = '') {
        $this->p--;
    }

    function getPos() {
        return $this->p;
    }

    function setPos($p) {
        $this->p = $p;
    }

}
