Roadmap for UApp project
========================

v1.4

- partial codeception support
  . file upload

v1.5 (current)

- UXMLElement
- DataSource
- Fake mailer

v1.6

- Docker image version

v1.7

- minor consistency changes in several components and modules

v1.8 - 1.16

- minor new functions

v2.0

- Composer project (still namespaceless)
- UList stretch javítás (showmin rule)
- Menu <ul> egységesítés
- User & AuthSource & Identity interface
- full codeception support
- stored post rendbetétel
- UXMLElement új metódusainak teljes körű használata

- PDO Connections
- :parameters

v3.0

- Namespaces
- converting modules to composer
- less support
- node/array mapping
- array/expression format
- all deprecated method is removed

v3.1

- RBAC support
- GenCommand
