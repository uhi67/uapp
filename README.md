UApp application framework
==========================

An XSLT-based MVC framework for php applications

Version: v2.0 (2024-09-25)

- See change log in [changes.md](changes.md)
- The complete [API documentation](http://uhisoft.hu/api/uapp/classes/UApp.html)
- Descriptive documentation in Hungarian [here](http://uhisoft.hu/doc/uapp/).

This old project uses no namespaces.
For a newer, namespace-based, more flexible version see [UXApp](https://bitbucket.org/uhi67/uxapp/src/master/) project.

Requirements
------------

- php >= 8.2 with XSLT extension
- composer 2.0
- mySQL >= 5.6 / PostgreSql >= 8.3 (Note: none of them is explicitly required)
- simpleSAMLphp >= 2.1 (optionally required by the application)

Usage
-----
The framework can be included into your project with composer.
Include in your application's `composer.json` file:

    ```json
    {
        "require": {
            "uhi67/uapp": "dev-feature/composer"
        },
        "repositories": [
            {
                "type": "vcs",
                "url": "git@bitbucket.org:uhi67/uapp.git"
            }
        ]
    }
    ```

Modules
-------
The project contains its old-style modules (without composer modularity)
Application-side modules can be manually implemented in the /app/modules directory.

Application testing with Codeception
------------------------------------

0. This feature is under construction.
1. See `lib/testhelper/UApp` for details of framework module.
2. Run page and unit tests separately (does not work in one session)

Generating documentation
========================

1. Install phpdocumentor in your system
2. Change version number in `phpdoc.dist.xml`, `phpdoc.xml`
3. ´$ php ../phpdocumentor/phpdocumentor.phar run -d . -t doc´

Usage in legacy environments
============================

UApp >= 2.0 is a composer project

1. clone from VCS (`git clone git@bitbucket.org:uhi67/uapp.git`)
2. use `/def/apptemplate` as a template for your application
3. configure your app:
    - 3.1. define modules and environment idependent parameters in the `config/config.php`
    - 3.2. define deployment parameters using environment variables in
        - a. in the virtual host config with SetEnv directives
        - b. or overwrite any of them in the `config/env.php` file

Supressing timezone warning
---------------------------
Set timezone in php.ini or in vhost config like:

    php_value date.timezone "Europe/Budapest"

Building docker image
===================== 

To build image `uapp-php:7.4-apache`:

0. Change to `docker` directory
1. `cp .env-dist .env`
2. set actual compose parameters in `.env`
3. `docker compose build`

Currently, the only available version of the image is php-7.4 with apache and debian.

Using image
------------

1. Use `/apptemplate/docker-compose-template.yml` as template for your application's docker-compose.yml
2. Always elaborate a `config/config-template.php` for your application. The `config/config.php` is always a private
   copy at a specific deployment, and may contain sensitive data. Never upload the `config/config.php` onto VCS.
3. You may deploy your application in three ways:
    - a. Deploy your application in the host filesystem and mount as an external volume into the container.
      The maintenance of the application is made on the host filesystem, no VCS env-var is required.
    - b. Mount an empty directory into the container. Define VCS environment variables and let the image maintain the
      code.
    - c. Do not mount external directory for the app. Define VCS environment variables and let the image maintain the
      code inside the conatiner.
4. Define deployment parameters using environment variables in the `docker-compose.yml`

- Author Uherkovich Péter (uherkovich.peter@gmail.com)
- Copyright (c) 2010-2023
- License MIT
