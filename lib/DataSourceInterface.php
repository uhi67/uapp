<?php
/**
 * interface DataSourceInterface
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/** @noinspection PhpClassNamingConventionInspection */

/**
 * DataSourceInterface must be implemented by data sources
 *
 * Data sources provide data for UList and similar components
 *
 * @see QueryDataSource
 * @property-read int $count -- Total number of items in the set
 * @property string $modelClass -- className to return as elements
 */
interface DataSourceInterface {
    /**
     * Total number of items in the set
     *
     * @return int the number of items in the set
     */
    public function getCount();

    /**
     * Returns the specified section of items
     *
     * @param int $start -- 0-based offset
     * @param int $count -- number of items
     * @param array $orders -- order array (same as for Query)
     * @return array -- the list of items
     */
    public function fetch($start, $count, $orders);

    /**
     * Sets the search pattern if the data source supports it.
     * The search pattern will narrow the set of items.
     * Optional params may specify the scope of the pattern.
     * For example, in case of UList, $params['fieldName'] will contain $list->fieldName()
     *
     * @param string $pattern
     * @param array $params
     * @return mixed
     */
    public function setPattern($pattern, $params = null);

    /**
     * @param string|BaseModel $modelClass
     */
    public function setModelClass($modelClass);

    /**
     * @return string|BaseModel
     */
    public function getModelClass();
}
