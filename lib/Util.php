<?php
/**
 * class Util
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/** @noinspection PhpMethodNamingConventionInspection */
/** @noinspection PhpUnused */

/**
 * A general helper class
 *
 * Common functions manipulating string, array, date, int and other data.
 */
class Util {
    static private $query = [];
    static private $uploadErrors = [
        UPLOAD_ERR_OK => 'The file uploaded with success.',
        UPLOAD_ERR_INI_SIZE => 'The uploaded file exceeds the upload_max_filesize',
        UPLOAD_ERR_FORM_SIZE => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.',
        UPLOAD_ERR_PARTIAL => 'The uploaded file was only partially uploaded.',
        UPLOAD_ERR_NO_FILE => 'No file was uploaded.',
        UPLOAD_ERR_NO_TMP_DIR => 'Missing a temporary folder.',
        UPLOAD_ERR_CANT_WRITE => 'Failed to write file to disk.',
        UPLOAD_ERR_EXTENSION => 'A PHP extension stopped the file upload.'
    ];

    /** @var array $dateformats : [preg, dateformat] */
    static public $dateformats = [
        ['/^\d{4}\.\d{2}\.\d{2}$/', '!Y.m.d'],
        ['/^\d{4}\.\d{2}\.\d{2}\.$/', '!Y.m.d.'],
        ['/^\d{4}\.\d{2}\.\d{2}\s\d{1,2}:\d{1,2}$/', '!Y.m.d H:i'],
        ['/^\d{4}\.\d{2}\.\d{2}\.\s\d{1,2}:\d{1,2}$/', '!Y.m.d. H:i'],
        ['/^\d{4}\.\d{2}\.\d{2}\s+\d{1,2}:\d{1,2}:\d{1,2}$/', '!Y.m.d H:i:s'],
        ['/^\d{4}\.\d{2}\.\d{2}\.\s+\d{1,2}:\d{1,2}:\d{1,2}$/', '!Y.m.d. H:i:s'],

        ['/^\d{4}\.\s\d{2}\.\s\d{2}$/', '!Y. m. d'],
        ['/^\d{4}\.\s\d{2}\.\s\d{2}\.$/', '!Y. m. d.'],
        ['/^\d{4}\.\s\d{2}\.\s\d{2}\.\s\d{1,2}:\d{1,2}$/', '!Y. m. d. H:i'],
        ['/^\d{4}\.\s\d{2}\.\s\d{2}\.\s\d{1,2}:\d{1,2}:\d{1,2}$/', '!Y. m. d. H:i:s'],
    ];

    /**
     * @param DateTime|string|int $dt
     *
     * @return DateTime
     * @throws InternalException*@throws Exception
     * @throws Exception
     */
    public static function createDateTime($dt) {
        if ($dt instanceof DateTime) return $dt;
        if (is_int($dt)) return DateTime::createFromFormat('U', $dt);
        Util::assertString($dt);
        foreach (self::$dateformats as $format) {
            if (preg_match($format[0], $dt)) return DateTime::createFromFormat($format[1], $dt);
        }
        return new DateTime($dt);
    }

    /**
     * Returns path part of request uri.
     *
     * @param string $baseurl
     *
     * @return string
     * @throws Exception
     */
    static function getUriFile($baseurl = '') {
        if (!isset($_SERVER['REQUEST_URI'])) throw new Exception('Illegal in CLI');
        $uri = $_SERVER['REQUEST_URI'];
        /** @noinspection PhpAssignmentInConditionInspection */
        if ($baseurl !== null && substr($uri, 0, $bul = strlen($baseurl)) == $baseurl) $uri = substr($uri, $bul);
        return parse_url($uri, PHP_URL_PATH);
    }

    /**
     * Returns path part of configured baseurl or throws an exception
     *
     * @return string -- configured baseurl
     * @throws InternalException
     */
    static function baseurl() {
        try {
            $baseurl = parse_url(UApp::config('baseurl'), PHP_URL_PATH);
        } catch (Exception $e) {
            throw new InternalException('baseurl nem határozható meg', null, $e);
        }
        return $baseurl;
    }

    /**
     * Returns request uri, relative to baseurl if possible.
     *
     * Returns null if no REQUEST_URI present.
     *
     * @return string
     * @throws InternalException
     */
    static function getUri() {
        if (!isset($_SERVER['REQUEST_URI'])) return null;
        $uri = $_SERVER['REQUEST_URI'];
        $baseuri = static::baseurl();
        /** @noinspection PhpAssignmentInConditionInspection */
        if (substr($uri, 0, $bul = strlen($baseuri)) == $baseuri) $uri = substr($uri, $bul);
        return $uri;
    }

    /**
     * Returns the url path or part of it
     *
     * Without or null index argument returns an array containing all elements of path separated by '/'.
     * If no path present (e.g. CLI) or no indexth element, default will be returned.
     *
     * @param int $index
     * @param string $default
     * @param string $baseurl -- relative to baseurl, default is baseurl of App's config
     *
     * @return array|string
     * @throws InternalException
     */
    static function getPath($index = null, $default = null, $baseurl = null) {
        $uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : $default;
        if (!$uri) return $default;
        if (!$baseurl) $baseurl = static::baseurl();
        $baseuri = $baseurl ? parse_url($baseurl, PHP_URL_PATH) : '';
        /** @noinspection PhpAssignmentInConditionInspection */
        if (substr($uri, 0, $bul = $baseuri ? strlen($baseuri) : 0) == $baseuri) $uri = substr($uri, $bul);


        $path = explode('/', trim(parse_url($uri, PHP_URL_PATH), '/ '));
        if ($index === null) return $path;
        if (isset($path[$index])) return $path[$index];
        return $default;
    }

    /**
     * Sets the query array to given value.
     *
     * This values will override request variables.
     * Array value in name will replace the entire stored request array.
     * Name-value pairs will set single value
     *
     * @param array|string $name
     * @param mixed $value
     *
     * @return void
     */
    public static function setReq($name, $value = null) {
        if (is_array($name)) self::$query = $name;
        else self::$query[$name] = $value;
    }

    /**
     * Returns a named request variable if present or default
     *
     * Returns a value from previously stored query if present.
     * This value can be set by Util::setReq()
     *
     * @param string $name
     * @param string $default
     *
     * @return string
     * @throws InternalException
     */
    static function getReq($name, $default = null) {
        if (isset(self::$query[$name])) return self::$query[$name];

        try {
            // Only instantiated app, avoiding init loop
            $app = UApp::$uniqueInstance;
            $page = is_a($app, 'UApp') ? $app->page : null;
            $pageName = $page ? $page->name : 'none';
        } catch (Exception $e) {
            throw new InternalException('Page cannot be determined', null, $e);
        }

        if (isset($storedpost[$name]) && $storedpost['_target'] == $pageName) {
            #Debug::tracex('... session[_post]', $name, '#8c0');
            return $storedpost[$name];
        }

        if (!isset($_REQUEST) || !isset($_REQUEST[$name])) return $default;
        return $_REQUEST[$name];
    }

    /**
     * Returns an uploaded file from the request
     *
     * Returns a value from previously stored query if present.
     * This value can be set by Util::setReq()
     *
     * Returns array of FileUpload if request variable is an array variable[]
     *
     * @param string $name
     *
     * @return FileUpload|FileUpload[]|null
     * @throws InternalException
     */
    static function getReqFile($name) {
        if (!isset($_FILES) || !isset($_FILES[$name])) return null;
        if (is_array($_FILES[$name]['name'])) {
            $result = [];
            foreach ($_FILES[$name]['name'] as $fieldname => $filename) {
                $result[$fieldname] = FileUpload::createFromField($name, $fieldname);
            }
            return $result;
        } else $fileupload = FileUpload::createFromField($name);
        return $fileupload;
    }

    /**
     * @param $name
     * @param null $default
     *
     * @return int|null
     * @throws InternalException
     */
    static function getReqInt($name, $default = null) {
        $value = self::getReq($name, $default);
        if ($value === '') $value = $default;
        if ($value !== null) $value = (int)$value;
        return $value;
    }

    /**
     * Returns an array value from a named request variable if present or default
     *
     * You may post array value form a form using variable name(s) containing []
     * You may post associative array as well.
     *
     * @param string $name
     * @param array $default
     *
     * @return array -- no non-array value will be accepted
     * @throws InternalException
     */
    static function getReqArray($name, $default = []) {
        $result = self::getReq($name, $default);
        if (!is_array($result)) return $default;
        return $result;
    }

    /**
     * Returns a value from multiple OR-ed field (e.g. checkbox set)
     *
     * @param mixed $name
     *
     * @return integer
     * @throws InternalException
     * @example
     * On the form use name 'var[]' for each field and values 1,2,4,8,...
     * The returned value will be 5 if first and third box is checked.
     * Variable indices will be ignored.
     * Returns null if variable not found.
     *
     */
    static function getReqMultiOr($name) {
        $a = array_values(self::getReqArray($name));
        if (count($a) == 0) return null;
        $r = 0;
        /** @noinspection PhpStatementHasEmptyBodyInspection */
        for ($i = 0; $i < count($a); $r |= $a[$i++]) ;
        return $r;
    }

    /**
     * Returns value of a session variable or default if not defined
     *
     * @param string $name
     * @param mixed $default
     *
     * @return mixed|null
     */
    static function getSession($name, $default = null) {
        if (!isset($_SESSION) || !isset($_SESSION[$name])) return $default;
        return $_SESSION[$name];
    }


    /**
     * Returns a named global variable if present, or default
     *
     * @param string $name
     * @param mixed $default
     *
     * @return mixed
     */
    static function getGlobal($name, $default = null) {
        if (isset($GLOBALS[$name]))
            return $GLOBALS[$name];
        else
            return $default;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @param string $text
     */
    function log($text) {
        $logfile = $GLOBALS['config']['logfile'];
        $ts = date('Y-m-d H:i');
        error_log($ts . "\t" . $text . "\n", 3, $logfile);
    }

    /**
     * Creates a technical compact visible form of any objects, similar to JSON
     *
     * @param mixed $obj
     * @param bool $quotestrings
     * @param bool $short -- do not list content of objects
     * @param int $maxdepth -- maximum depth of recursion (max 100)
     *
     * @return string
     */
    static function objtostr($obj, $quotestrings = true, $short = false, $maxdepth = 10) {
        if ($maxdepth > 100 || $maxdepth < 0) $maxdepth = 100;
        if (is_null($obj)) return 'null';
        if (is_bool($obj)) return $obj ? 'true' : 'false';
        if (is_int($obj)) return $obj;
        if (is_float($obj)) return $obj;
        $qs = ($quotestrings ? "'" : '');
        if (is_string($obj)) return $qs . $obj . $qs;
        if (is_array($obj)) {
            $result = '';
            foreach ($obj as $key => $item) {
                if ($result != '') $result .= ', ';
                $result .= $maxdepth ? $key . ':' . self::objtostr($item, $quotestrings, $short, $maxdepth - 1) : '[Array]';
            }
            return '[' . $result . ']';
        }
        if ($obj instanceof Inet) {
            return '{' . $obj->toString() . '}';
        }
        if ($obj instanceof Macaddr) {
            return '{' . $obj->toString() . '}';
        }
        if ($obj instanceof DateTime) {
            return '{' . $obj->format(DateTime::ATOM) . '}';
        }
        if (is_object($obj) && $short) return get_class($obj);
        if ($obj instanceof DOMElement) {
            if (!$obj->ownerDocument) {
                $doc = new UXMLDoc();
                $doc->documentElement->appendChild($doc->importNode($obj, true));
            }
            $r = $obj->ownerDocument->saveXML($obj);
            if (strlen($r) > 100) $r = substr($r, 0, 97) . '...';
            return htmlspecialchars($r);
        }
        if ($obj instanceof DOMDocument) {
            $r = $obj->saveXML($obj->documentElement);
            if (strlen($r) > 100) $r = substr($r, 0, 97) . '...';
            return htmlspecialchars($r);
        }
        if ($obj instanceof DOMNode) {
            return get_class($obj) . '{' . $obj->nodeName . '=' . $obj->nodeValue . '}';
        }
        if (is_object($obj)) {
            if (is_callable([$obj, '_toString'])) return get_class($obj) . '{' . $obj->_toString() . '}';
            if (is_callable([$obj, 'toArray'])) return get_class($obj) . self::objtostr($obj->toArray(), $quotestrings, $short, $maxdepth - 1);
            $result = '';
            foreach ($obj as $key => $item) {
                if ($result != '') $result .= ',';
                $result .= $maxdepth ? $key . '=' . self::objtostr($item, $quotestrings, $short, $maxdepth - 1) : '{Object}';
            }
            return get_class($obj) . '{' . $result . '}';
        }
        return $obj;
    }

    /**
     * Creates a human compact visible form of convertable objects
     *
     * @param mixed $obj
     * @param string|null $locale -- no internationalization on null
     *
     * @return string
     * @throws ConfigurationException
     * @throws InternalException
     */
    static function toString($obj, $locale = null) {
        if (is_null($obj)) {
            $value = 'not set';
            return $locale ? UApp::la('uapp', $value, null, $locale) : $value;
        }
        if (is_bool($obj)) {
            $value = $obj ? 'true' : 'false';
            return $locale ? UApp::la('uapp', $value, null, $locale) : $value;
        }
        if (is_int($obj)) return (string)$obj;
        if (is_float($obj)) return (string)$obj;
        if (is_string($obj)) return $obj;
        if (is_array($obj)) {
            $result = '';
            if (ArrayUtils::isAssociative($obj, false)) {
                foreach ($obj as $key => $item) {
                    if ($result != '') $result .= ', ';
                    $result .= $key . ':' . self::toString($item, $locale);
                }
            } else {
                foreach ($obj as $item) {
                    if ($result != '') $result .= ', ';
                    $result .= self::toString($item, $locale);
                }
            }
            return '[' . $result . ']';
        }
        if ($obj instanceof DateTime) {
            #Debug::tracex('locale', $locale);
            if ($obj->format('His') == '00000') {
                // Date only
                if (!$locale) return $obj->format('Y-m-d');
                else return UApp::df($obj, IntlDateFormatter::SHORT, IntlDateFormatter::NONE, $locale);
            }
            if (!$locale) return $obj->format(DateTime::ATOM);
            else return UApp::df($obj, IntlDateFormatter::SHORT, IntlDateFormatter::SHORT, $locale);
        }
        if (is_callable([$obj, 'toString'])) return $obj->toString();
        if (is_object($obj)) return get_class($obj);
        return gettype($obj);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns substring before delimiter
     *
     * @param string $s -- string
     * @param string $d -- delimiter
     * @param bool $full -- returns full string if pattern not found
     *
     * @return string -- substring to delimiter or empty string if not found
     */
    static function substring_before($s, $d, $full = false) {
        $p = strpos($s, $d);
        if ($full && $p === false) return $s;
        return substr($s, 0, $p);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns substring after delimiter
     *
     * @param string $s -- string
     * @param string $d -- delimiter
     * @param bool $full -- returns full string if pattern not found
     *
     * @return string -- substring to delimiter or empty string if not found
     */
    static function substring_after($s, $d, $full = false) {
        if (!is_scalar($s)) throw new Exception('Invalid string');
        $p = strpos($s, $d);
        if ($p === false) return $full ? $s : '';
        return substr($s, $p + strlen($d));
    }

    /**
     * Returns the best mime-type belongs to the file extension
     *
     * @param string $path
     *
     * @return string
     */
    static function mimetype($path) {
        $mimeTypes = [
            'bmp' => 'image/x-ms-bmp',
            'css' => 'text/css',
            'gif' => 'image/gif',
            'htm' => 'text/html',
            'html' => 'text/html',
            'shtml' => 'text/html',
            'ico' => 'image/vnd.microsoft.icon',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'js' => 'text/javascript',
            'map' => 'application/json',
            'pdf' => 'application/pdf',
            'png' => 'image/png',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',
            'swf' => 'application/x-shockwave-flash',
            'swfl' => 'application/x-shockwave-flash',
            'txt' => 'text/plain',
            'xht' => 'application/xhtml+xml',
            'xhtml' => 'application/xhtml+xml',
            'xsl' => 'text/xsl',
            'xslt' => 'text/xsl',
            'xml' => 'text/xml',
            'ttf' => 'font/ttf', //'application/x-font-truetype',
            'woff' => 'application/font-woff',
            'woff2' => 'application/font-woff2',
            'php' => 'php',
        ];
        /* Find MIME type for file, based on extension. */
        $contentType = NULL;
        if (preg_match('#\.([^/^.]+)$#D', $path, $type)) {
            $type = strtolower($type[1]);
            if (array_key_exists($type, $mimeTypes)) {
                $contentType = $mimeTypes[$type];
            }
        }
        if ($contentType === NULL) {
            $contentType = 'application/octet-stream';
        }
        return $contentType;
    }

    /**
     * returns binary representation of a value
     *
     * @param int $v
     * @param int $length
     *
     * @return string
     */
    static function bitstring($v, $length = 16) {
        if ($length == 0) return '';
        return str_pad(substr(decbin($v), -$length), $length, '0', STR_PAD_LEFT);
    }

    static function friendlyErrorType($type) {
        switch ($type) {
            case E_ERROR: // 1 //
                return 'E_ERROR';
            case E_WARNING: // 2 //
                return 'E_WARNING';
            case E_PARSE: // 4 //
                return 'E_PARSE';
            case E_NOTICE: // 8 //
                return 'E_NOTICE';
            case E_CORE_ERROR: // 16 //
                return 'E_CORE_ERROR';
            case E_CORE_WARNING: // 32 //
                return 'E_CORE_WARNING';
            case E_COMPILE_ERROR: // 64 //
                return 'E_COMPILE_ERROR';
            case E_COMPILE_WARNING: // 128 //
                return 'E_COMPILE_WARNING';
            case E_USER_ERROR: // 256 //
                return 'E_USER_ERROR';
            case E_USER_WARNING: // 512 //
                return 'E_USER_WARNING';
            case E_USER_NOTICE: // 1024 //
                return 'E_USER_NOTICE';
            case E_STRICT: // 2048 //
                return 'E_STRICT';
            case E_RECOVERABLE_ERROR: // 4096 //
                return 'E_RECOVERABLE_ERROR';
            case E_DEPRECATED: // 8192 //
                return 'E_DEPRECATED';
            case E_USER_DEPRECATED: // 16384 //
                return 'E_USER_DEPRECATED';
        }
        return "type_$type";
    }

    /**
     * Substitutes {$key} patterns of the text to values of associative data
     * Used primarily for native language texts, but used for SQL generation where substitution is not based on SQL data syntax.
     * If no substitution possible, the pattern remains unchanged without error
     * Special cases:
     *    - {DMY$var} - convert hungarian date to english (deprecated)
     *  - {$var/subvar} - array resolution within array values (using multiple levels possible)
     *  - Using special characters if necessary: `{{}` -> `{`, `}` -> `}`
     *    - values of DateTime will be substituted as SHORT date of the application's language.
     *
     * @param string $text
     * @param array $data -- name => value pairs (value can be string or DateTime)
     * @param string $locale
     * @param string|L10nBase $l10n
     *
     * @return string
     * @throws ConfigurationException
     * @throws InternalException
     * @throws ReflectionException
     */
    static function substitute($text, $data, $locale = 'hu-HU', $l10n = null) {
        if (!is_string($text)) {
            UApp::getInstance()->error_log('Non-string text passed to substitute');
            $text = self::objtostr($text, false, true);
        }
        return preg_replace_callback('#{(DMY|MDY)?(\\$[a-zA-Z_]+[/a-zA-Z0-9_-]*)}#', function ($mm) use ($data, $l10n, $locale) {
            // mm[2] = a {} zárójelek közötti rész
            // mm[1] = dátum formátum (opcionális, elavult, általában üres)
            if ($mm[2] == '{') return '{';
            if (substr($mm[2], 0, 1) == '$') {
                // a keyname, can be '/' separated list of subkeys
                $subvars = explode('/', substr($mm[2], 1));
                $d = $data;
                foreach ($subvars as $subvar) {
                    if (is_array($d) && array_key_exists($subvar, $d)) $d = $d[$subvar] === null ? '#null#' : $d[$subvar];
                    else return $mm[0]; // Variable not found in data: the key itself is returned
                }
            } else {
                // Other expression (not implemented)
                return $mm[0]; // Temporary bypass: the key itself is returned
            }
            if (is_a($d, 'DateTime')) $d = Util::formatDate($d, IntlDateFormatter::SHORT, IntlDateFormatter::NONE, $locale, $l10n);
            if ($mm[1] == 'MDY') {
                $d = L10n::formatDate(Util::createDateTime($d), IntlDateFormatter::SHORT, IntlDateFormatter::NONE, 'en');
            }
            if ($mm[1] == 'DMY') {
                $d = L10n::formatDate(Util::createDateTime($d), IntlDateFormatter::SHORT, IntlDateFormatter::NONE, 'en-GB');
            }

            // Other non-string parameter
            if (is_object($d) && method_exists($d, 'toString')) $d = $d->toString();
            if (!is_string($d) && !is_int($d) && !is_float($d)) {
                $app = UApp::hasInstance();
                if ($app) $app->error_log('Non-string parameter passed to substitute ' . json_encode(debug_backtrace()[2]));
                $d = self::objtostr($d, false, true);
            }

            return $d;
        }, $text);
    }

    /**
     * Formats a datetime using given localization class and locale
     *
     * @param DateTime $dt
     * @param int $datetype -- date format as IntlDateFormatter::NONE, type values are 'NONE', 'SHORT', 'MEDIUM', 'LONG', 'FULL'
     * @param int $timetype -- time format as IntlDateFormatter::NONE, type values are 'NONE', 'SHORT', 'MEDIUM', 'LONG', 'FULL'
     * @param string $locale
     * @param string $l10n -- Name of an L10nBase class to use for formatting
     *
     * @return string -- the formatted date
     */
    static function formatDate($dt, $datetype = IntlDateFormatter::SHORT, $timetype = IntlDateFormatter::NONE, $locale = 'hu-HU', $l10n = null) {
        if (!$l10n) $l10n = 'L10nBase';
        /** @var L10nBase $l10n */
        return $l10n::formatDate($dt, $datetype, $timetype, $locale);
    }

    /**
     * @param string $str
     * @param mixed $default
     *
     * @return bool
     */
    static function stringToBool($str, $default) {
        if ($str === null) return $default;
        $str = trim(strtolower($str));
        if ($str === '') return $default;
        $s = substr($str, 0, 1);
        if ($s == 't' || $s == 'i' || $s == 'y' || $str == -1 || $str > 0) return true;
        if ($s == 'f' || $s == 'h' || $s == 'n' || $str == 0) return false;
        if ($str > ' ') return true;
        return $default;
    }

    /**
     * Converts 'apple-banane' to 'AppleBanane'
     *
     * @param string $string -- string containing '_', '-' or ' ' as word separator
     * @param bool $pascalCase -- true if firs character should be uppercase
     *
     * @return string - camelized joint word without separators
     */
    static function camelize($string, $pascalCase = false) {
        $string = str_replace(['-', '_'], ' ', $string);
        $string = ucwords($string);
        $string = str_replace(' ', '', $string);
        if (!$pascalCase) {
            return lcfirst($string);
        }
        return $string;
    }

    /**
     * Uncamelize a string, joining the words by separator character.
     *
     * @param string $text to uncamelize
     * @param string $separator
     *
     * @return string Uncamelized text
     */
    static function uncamelize($text, $separator = '_') {
        // Replace all capital letters by separator followed by lowercase one
        $text = preg_replace_callback('/[A-Z]/', function ($matches) use ($separator) {
            return $separator . strtolower($matches[0]);
        }, $text);

        // Remove first separator (to avoid _hello_world name)
        return preg_replace("/^" . $separator . "/", '', $text);

    }

    /**
     * Returns first argument, which evaluates true
     *
     * @param mixed $arg1,...
     *
     * @return bool|mixed
     */
    static function coalesce($arg1) {
        if ($arg1) return $arg1;
        for ($i = 1; $i < func_num_args(); $i++) {
            $b = func_get_arg($i);
            if ($b) return $b;
        }
        return false;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Concatenates array values including keys.
     *
     * Similar to implode, but alpplies an additional glue between key and value.
     * If $recurse is not false, the glue may be an array for different levels (and uses the last one for additional levels).
     * If $recurse is not false it should be a two-element array for outer boundaries of array elements.
     * It may be also an array of two-elements arrays, each one for different leveles, likewise glue array.
     *
     * @param string|array $glue1 -- delimiter(s) between values
     * @param string|array $glue2 -- delimiter(s) between key and value
     * @param mixed $array -- data to convert (may be recursive)
     * @param boolean|array $recurse -- (array of) two-element array for boundaries.
     *
     * @return string
     */
    static function implode_with_keys($glue1, $glue2, $array, $recurse = false) {
        $output = [];
        $g1 = is_array($glue1) ? $glue1[0] : $glue1;
        $g2 = is_array($glue2) ? $glue2[0] : $glue2;
        /** @var string|array|object $item */
        foreach ($array as $key => $item) {
            if (is_array($item)) {
                if ($recurse) {
                    $glue1x = $glue1;
                    if (is_array($glue1x) && count($glue1x) > 1) array_shift($glue1x);
                    $glue2x = $glue2;
                    if (is_array($glue2x) && count($glue2x) > 1) array_shift($glue2x);
                    $recursex = $recurse;
                    if (is_array($recurse)) {
                        if (is_array($recurse[0])) {
                            $r1 = $recurse[0][0];
                            $r2 = $recurse[0][1];
                            if (count($recursex) > 1) array_shift($recursex);
                        } else {
                            $r1 = $recurse[0];
                            $r2 = $recurse[1];
                        }
                    } else {
                        $r1 = '[';
                        $r2 = ']';
                    }
                    $item = $r1 . self::implode_with_keys($glue1x, $glue2x, $item, $recursex) . $r2;
                } else $item = 'Array';
            }
            if ($item instanceof DateTime) $item = $item->format('Y-m-d H:i');
            if (is_object($item)) {
                if (is_callable([$item, '_toString'])) $item = $item->_toString();
                else $item = '{' . get_class($item) . '}';
            }
            $output[] = $key . $g2 . $item;
        }
        return implode($g1, $output);
    }

    /**
     * Generates a valid XML name-id based on given string
     *
     * Replaces invalid characters to valid ones. Replaces accented letters to ASCII letters.
     *
     * - Element names must start with a letter or underscore
     * - Element names can contain letters, digits, underscores, and the specified enabled characters
     * - Element names cannot contain spaces
     *
     * @param string|null $str
     * @param string $def -- replace invalid characters to, default _
     * @param string $ena -- more enabled characters, e.g. '-' (specify - last, escape ] chars.)
     * @param int $maxlen -- maximum length or 0 if no limit. Default is 64.
     *
     * @return string -- the correct output, or empty if input was empty or null
     */
    static function toNameID($str, $def = '_', $ena = '.-', $maxlen = 64) {
        if ($str === '' || $str === null) return $str;
        #if(strtolower(substr($str,0,3))=='xml') $str = '_'.$str; // xml prefix is valid!
        if ($maxlen > 0 && strlen($str) > $maxlen) $str = substr($str, 0, $maxlen);
        /** @noinspection PhpAssignmentInConditionInspection */
        if (($p = strpos($ena, '-')) < strlen($ena) - 1) $ena = substr($ena, 0, $p) . substr($ena, $p + 1) . '-';
        if (preg_match("~^[A-Za-z_][\w_$ena]*$~", $str)) return $str;
        if ($def === null) $def = '_';
        if ($ena === null) $ena = '';
        if (!preg_match("~^[A-Za-z_]~", $str)) $str = '_' . $str;
        $str = str_replace(
            preg_split('//u', 'áéíóöőúüűÁÉÍÓÖŐÚÜŰ', -1, PREG_SPLIT_NO_EMPTY),
            str_split('aeiooouuuAEIOOOUUU'),
            $str
        );
        if (strlen($ena) > 0) $ena = mb_ereg_replace('(.)', '\\\\1', $ena);
        return mb_ereg_replace('[^A-Za-z0-9' . $def . $ena . ']', $def, $str);
    }

    /**
     * Returns a random string contains only [a-z0-9]
     *
     * @param integer $len
     *
     * @return string
     */
    static function randStr($len) {
        $b = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $first = 24; // Range of first character is 0-first
        $l = strlen($b);
        $r = $b[rand(0, $first)];
        for ($i = 1; $i < $len; $i++) {
            $r .= $b[rand(0, $l - 1)];
        }
        return $r;
    }

    /**
     * If argument is not a string (or number), throws exception
     *
     * @param mixed $x
     *
     * @return void
     * @throws InternalException
     */
    static function assertString($x) {
        if (!is_string($x) && !is_numeric($x)) throw new InternalException('#1:parameter must be string, got ' . gettype($x));
    }

    /**
     * If argument is not a valid integer (maybe in string), throws exception
     *
     * @param mixed $x
     *
     * @return void
     * @throws InternalException
     */
    static function assertInt($x) {
        if (!is_scalar($x) || strval($x) != strval(intval($x))) throw new InternalException('#1:parameter must be int, got ' . gettype($x));
    }

    /**
     * If argument is not an array, throws exception
     *
     * @param mixed $x
     *
     * @return void
     * @throws InternalException
     */
    static function assertArray($x) {
        if (!is_array($x)) throw new InternalException('#1:parameter must be an array, got ' . gettype($x) . ': ' . Util::objtostr($x));
    }

    /**
     * Assures that $entity is an instance of $class or null
     *
     * @param object $entity
     * @param string $class
     * @param bool $null may be null
     *
     * @return void
     * @throws InternalException
     */
    static function assertClass($entity, $class, $null = true) {
        if ($null && $entity === null) return;
        if ($entity === null) throw new InternalException("Parameter must not be null");
        if (!is_object($entity)) throw new InternalException("Parameter must be object of $class, got " . gettype($entity));
        if (!is_a($entity, $class)) throw new InternalException("Parameter must be a $class, got " . get_class($entity));
    }

    /**
     * Calculates relative path to $to based on $from
     *
     * If a folder name is provided as 'to', it must be ended by '/'
     *
     * @author http://stackoverflow.com/users/208809/gordon
     *
     * @param string $from -- absolute basepath
     * @param string $to -- absolute path to where
     *
     * @return string -- the relative to $to
     */
    static function getRelativePath($from, $to) {
        #$xto = $to; $xfrom=$from;
        // some compatibility fixes for Windows paths
        $from = is_dir($from) ? rtrim($from, '\/') . '/' : $from;
        $to = is_dir($to) ? rtrim($to, '\/') . '/' : $to;
        $from = str_replace('\\', '/', $from);
        $to = str_replace('\\', '/', $to);

        $from = explode('/', $from);
        $to = explode('/', $to);
        $relPath = $to;

        foreach ($from as $depth => $dir) {
            // find first non-matching dir
            //if(!array_key_exists($depth, $to)) throw new Exception('Invalid to: '.$xto);
            if (array_key_exists($depth, $to) && $dir === $to[$depth]) {
                // ignore this directory
                array_shift($relPath);
            } else {
                // get number of remaining dirs to $from
                $remaining = count($from) - $depth;
                if ($remaining > 1) {
                    // add traversals up to first matching dir
                    $padLength = (count($relPath) + $remaining - 1) * -1;
                    $relPath = array_pad($relPath, $padLength, '..');
                    break;
                } else {
                    if (array_key_exists(0, $relPath)) $relPath[0] = './' . $relPath[0];
                }
            }
        }
        return implode('/', $relPath);
    }

    /**
     * Shows html error message without framework and exits
     *
     * @param mixed $code
     * @param mixed $title
     * @param mixed $message
     * @param Exception|null $e
     *
     * @return void
     */
    static function fatalError($code, $title, $message, $e = null) {
        $title1 = substr($title, 0, strpos($title, '/'));
        header("Status: $code $title1");
        if (php_sapi_name() == 'cli') {
            echo "Fatal error - UApp\n";
            echo "$code $title -- $message\n";
            if ($e) {
                printf("%s in file %s at line %s:\n %s\n", $e->getMessage(), $e->getFile(), $e->getLine(), $e->getTraceAsString());
                /** @noinspection PhpAssignmentInConditionInspection */
                while ($e = $e->getPrevious()) {
                    echo "\nPrevious exception:\n";
                    printf("%s in file %s at line %s:\n %s\n", $e->getMessage(), $e->getFile(), $e->getLine(), $e->getTraceAsString());
                }
            }
        } else {
            echo <<<EOT
<html lang="en">
<head>
	<title>Fatal error - UApp</title>
    <link rel="stylesheet" type="text/css" href="/Main/main.css" />
    <link rel="stylesheet" type="text/css" href="/css/design.css" />
</head>
<body>
    <div id="fatal">
	    <h2>$code $title</h2>
		<p>$message</p>
	</div>
</body>
</html>
EOT;
            $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
            header($protocol . ' ' . $code . ' ' . $title);
        }
        exit($code);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Similar to standard str_split, but operates on multibyte strings.
     * Converts the string to an array of equal length pieces.
     *
     * @param $str
     * @param int $l
     *
     * @return array|array[]|false|string[]
     */
    static function mb_str_split($str, $l = 0) {
        if ($l > 0) {
            $ret = [];
            $len = mb_strlen($str, "UTF-8");
            for ($i = 0; $i < $len; $i += $l) {
                $ret[] = mb_substr($str, $i, $l, "UTF-8");
            }
            return $ret;
        }
        return preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
    }

    /**
     * multibyte-string version of strcspn
     *
     * @param string $subject
     * @param string $mask
     * @param int $start
     * @param int $length
     *
     * @return int
     */
    static function mb_strcspn($subject, $mask, $start = 0, $length = null) {
        if ($start || $length) return static::mb_strcspn(mb_substr($subject, $start, $length), $mask);
        $len = mb_strlen($subject);
        for ($i = 0; $i < $len; $i++) {
            if (mb_strpos($mask, mb_substr($subject, $i, 1)) !== false) break;
        }
        return $i;
    }

    static function mb_strspn($subject, $mask, $start = 0, $length = null) {
        if ($start || $length) return static::mb_strspn(mb_substr($subject, $start, $length), $mask);
        $len = mb_strlen($subject);
        for ($i = 0; $i < $len; $i++) {
            if (mb_strpos($mask, mb_substr($subject, $i, 1)) === false) break;
        }
        return $i;
    }

    /**
     * @param $code
     *
     * @return mixed
     */
    static function getUploadError($code) {
        return ArrayUtils::getValue(self::$uploadErrors, $code, $code);
    }

    /**
     * Ha az argumentum tömb, azt adja vissza. Ha nem, egyelemű tömbbe teszi.
     *
     * @param mixed $data
     *
     * @return array
     */
    public static function forceArray($data) {
        return is_array($data) ? $data : [$data];
    }

    /**
     * Returns the number of bytes in the given string.
     * This method ensures the string is treated as a byte array by using `mb_strlen()`.
     *
     * @param string $string the string being measured for length
     *
     * @return int the number of bytes in the given string.
     */
    public static function byteLength($string) {
        return mb_strlen($string, '8bit');
    }

    /**
     * Check if given string starts with specified substring.
     * Binary and multibyte safe.
     *
     * @param string $string Input string
     * @param string $with Part to search inside the $string
     * @param bool $caseSensitive Case-sensitive search. Default is true. When case-sensitive is enabled, $with must exactly match the starting of the string in order to get a true value.
     *
     * @return bool Returns true if first input starts with second input, false otherwise
     */
    public static function startsWith($string, $with, $caseSensitive = true) {
        $bytes = static::byteLength($with);
        if (!$bytes) {
            return true;
        }
        if ($caseSensitive) {
            return strncmp($string, $with, $bytes) === 0;
        } else {
            return mb_strtolower(mb_substr($string, 0, $bytes, '8bit'), 'UTF-8') === mb_strtolower($with, 'UTF-8');
        }
    }

    /**
     * Check if given string ends with specified substring.
     * Binary and multibyte safe.
     *
     * @param string $string Input string to check
     * @param string $with Part to search inside the $string.
     * @param bool $caseSensitive Case-sensitive search. Default is true. When case-sensitive is enabled, $with must exactly match the ending of the string in order to get a true value.
     *
     * @return bool Returns true if first input ends with second input, false otherwise
     */
    public static function endsWith($string, $with, $caseSensitive = true) {
        $bytes = static::byteLength($with);
        if (!$bytes) {
            return true;
        }
        if ($caseSensitive) {
            // Warning check, see http://php.net/manual/en/function.substr-compare.php#refsect1-function.substr-compare-returnvalues
            if (static::byteLength($string) < $bytes) {
                return false;
            }
            return substr_compare($string, $with, -$bytes, $bytes) === 0;
        } else {
            return mb_strtolower(mb_substr($string, -$bytes, mb_strlen($string, '8bit'), '8bit'), 'UTF-8') === mb_strtolower($with, 'UTF-8');
        }
    }

    /**
     * Creates a new url based on given url and new data
     * - numeric indexed element will be new path elements
     * - associative elemnts will be new/overridden parameters
     * - #-index will be the fragment
     *
     * @param string $from
     * @param array|string $to -- data to build new url (or a single scalar)
     *
     * @return string
     * @throws InternalException
     */
    public static function createUrl($from, $to) {
        if (!is_array($to)) $to = [$to];
        $urla = parse_url($from);
        if ($urla === false) throw new InternalException('Malformed url', $from);
        $path = (isset($urla['path']) && $urla['path'] != '' && $urla['path'] != '/') ? array_filter(explode('/', $urla['path']), function ($a) {
            return $a != '';
        }) : [];

        $absolutepath = isset($urla['path']) && substr($urla['path'], 0, 1) == '/';
        $query = [];
        if (isset($urla['query'])) parse_str($urla['query'], $query);
        $fragment = (isset($urla['fragment']) && $urla['fragment'] != '') ? $urla['fragment'] : '';
        if (isset($to['#'])) {
            $fragment = $to['#'];
            unset($to['#']);
        }

        foreach ($to as $k => $v) {
            if (is_int($k)) $path[] = $v;
            else $query[$k] = $v;
        }

        return
            (isset($urla['scheme']) ? ($urla['scheme'] . '://') : '') .
            (isset($urla['host']) ? $urla['host'] : '') .
            (isset($urla['port']) ? (':' . $urla['port']) : '') .

            ((isset($urla['host']) && $urla['host'] && count($path) || $absolutepath) ? '/' : '') .

            (count($path) ? (implode('/', $path)) : '') .
            (count($query) ? ('?' . http_build_query($query)) : '') .
            ($fragment ? ('#' . $fragment) : '');
    }

    /**
     * Converts a free-spacing Regex to canonical form
     *
     * @see https://www.regular-expressions.info/freespacing.html
     *
     * @param string $fsre -- free spacing regex
     *
     * @return string -- canonical Regex
     */
    public static function canonRegex($fsre) {
        // eliminating white spaces and comments as(?#comment) and ...#comment\n
        echo htmlspecialchars('/\(\?\#[^)]*\)|(?<![\\\\\?])#[^\n]*\n|(?<!\(\?|\\\\)\s/m');
        return preg_replace('/\(\?#[^)]*\)|(?<![\\\\?])#[^\n]*\n|(?<!\(\?|\\\\)\s/m', '', $fsre);
    }

    /**
     * Visszaadja egy egész érték bináris képét, a bittömb alapján
     *
     * @param int $value
     * @param array $data -- bittérképdefiníció
     *
     * @return string
     * @deprecated Use {@see bitString()}
     * @codeCoverageIgnore
     */
    public static function bitStr($value, $data) { // eg. $rightbits
        $r = '';
        foreach ($data as $item)
            $r .= ($value & $item[0]) ? substr($item[1], 0, 1) : '.';
        return $r;
    }

    /**
     * Creates a delimited RegEx pattern from a bare pattern.
     *
     * @param $pattern -- pattern without delimiter
     *
     * @return string -- pattern with ~ delimiters and escaped inner ~s
     */
    public static function toRegExp($pattern) {
        return '~' . str_replace('~', '\~', $pattern) . '~';
    }

    /**
     * @param $dir
     * @param int $depth
     *
     * @return bool
     * @throws Exception
     */
    public static function makeDir($dir, $depth = 1) {
        if (!file_exists($dir)) {
            if ($depth == 0) return false;
            static::makeDir(dirname($dir), $depth - 1);
            if (@!mkdir($dir, 0774)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns $b coloured by matching to $a. Uses ansi colors.
     * Green is matching part, yellow is extra, red is missing
     *
     * @param string $a -- expected
     * @param string $b -- actual
     * @param bool $ignorelf -- ignores differences in line-endings
     *
     * @return string -- difference
     */
    public static function diff($a, $b, $ignorelf = false) {
        if ($ignorelf) {
            $a = str_replace("\r\n", "\n", $a);
            $b = str_replace("\r\n", "\n", $b);
        }
        $opcodes = FineDiff::getDiffOpcodes($a, $b);
        return FineDiff::renderDiffToANSIFromOpcodes($a, $opcodes);
    }

    /**
     * Returns associative array of bits of the value
     *
     * @param int $value
     * @param array $bitmap -- array of [bitvalue, name, descr, title] bit-definitions
     *
     * @param string $prefix
     *
     * @return array -- array of name => $value & bitvalue items
     * @throws Exception
     */
    public static function bitmapValues($value, $bitmap, $prefix = '') {
        return ArrayUtils::map($bitmap,
            function ($bit) use ($prefix) {
                return $prefix . $bit[1];
            },
            function ($bit) use ($value) {
                return $value & $bit[0];
            }
        );
    }

    /**
     * AAI egység-path konvertálása
     *
     * Kapott: AAI_ORG_PATH="Pécsi Tudományegyetem\0;Önálló Központi Igazgatási szervezeti egységek\6;Kancellária\40;Informatikai Igazgatóság\67;Üzemeltetési és Ügyfél-Támogatási Főosztály\386"
     * Konvertált: ou=Üzemeltetési és Ügyfél-Támogatási Főosztály,ou=Informatikai Igazgatóság,ou=Kancellária,ou=Önálló Központi Igazgatási szervezeti egységek,o=Pécsi Tudományegyetem,c=hu
     *
     * @param string $orgPath
     *
     * @return string
     * @see https://ldapwiki.com/wiki/Distinguished%20Names
     */
    public static function orgPathToDN($orgPath) {
        $escchr = ' "#+,;<>\\';
        $escrul = array_combine(str_split($escchr), array_map(function ($c) {
            return '\\' . $c;
        }, str_split($escchr)));
        return implode(',', array_merge(array_reverse(array_map(function ($item) use ($escrul) {
            /** @noinspection PhpAssignmentInConditionInspection */
            if ($p = strpos($item, '\\')) $item = substr($item, 0, $p);
            $item = strtr($item, $escrul);
            if (substr($item, 0, 2) == 'o=') return $item;
            return 'ou=' . $item;
        }, explode(';', 'o=' . $orgPath))), ['c=hu']));
    }

    /**
     * Returns any object in visual Html format (arrays and object as table)
     *
     * @param mixed $obj
     *
     * @return string
     * @throws ConfigurationException
     * @throws InternalException
     */
    public static function toHtml($obj, $depth = 5) {
        if (is_array($obj)) {
            if ($depth == 0) return '(array)';
            $result = '<table class="list">';
            if (ArrayUtils::isAssociative($obj, false)) {
                foreach ($obj as $key => $value) {
                    $value = static::toHtml($value, $depth - 1);
                    $result .= "<tr><th>$key</th><td>$value</td></tr>";
                }
            } else {
                foreach ($obj as $value) {
                    if (!is_scalar($value)) $value = Util::toString($value);
                    $value = htmlspecialchars($value);
                    $result .= "<tr><td>$value</td></tr>";
                }
            }
            return $result . '</table>';
        }
        if (is_object($obj)) {
            if ($depth == 0) return '(object)';
            return "<div>(" . get_class($obj) . ")</div>" . static::toHtml(get_object_vars($obj));
        }
        return htmlspecialchars(static::toString($obj));
    }

    /**
     * Stores a "flash" message to display on the next page
     * Can store more messages one after another
     *
     * Markdown syntax is accepted if msg starts with backtick, e.g.
     *
     * `´[caption](url)`
     *
     * Markdown is processed at client side by showdown module.
     *
     * @param string|array $msg -- Message text or array($msg, array($params)), with %s style params
     * @param string $class -- message class (error/ok)
     * @param bool $unique -- display same message is only once in one transaction (uses base message without params)
     * @param string|array $content -- optional extended content for message.
     *
     * @return void
     * @throws InternalException
     */
    public static function sendMessage($msg, $class = '', $unique = false, $content = null) {
        $msg1 = is_array($msg) ? $msg[0] : $msg; // Unique message base
        Util::assertString($msg1);
        if (is_array($msg)) {
            $msg = vsprintf($msg1, $msg[1]);
        }
        $msgs = Util::getSession('sendmessage', []);
        if ($unique) {
            $idx = array_search([$msg1, $class], $msgs);
            if ($idx === false) $msgs[] = [$msg, $class, $content];
        } else $msgs[] = [$msg, $class, $content];
        $_SESSION['sendmessage'] = $msgs;
    }
}
