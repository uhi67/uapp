<?php

/**
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 *
 */
class BaseUser extends Xlet {
    /** @var mixed $id -- Application level (internal) user id, usually integer */
    public $id;
    /** @var string $name -- User display name, from displayname or other attributes */
    public $name;
    /** @var string $email */
    public $email;
    /** @var boolean $enabled -- User login is really enabled in local database */
    public $enabled;

    public function finish() {
    }

    public function loadSession() {
        $this->id = (int)Util::getSession('user_id');
        $this->name = Util::getSession('user_name');
        $this->email = Util::getSession('user_email');
    }

    /**
     * Returns logged in user's id or null if not logged in.
     *
     * @return mixed
     */
    public function loggedIn() {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isEnabled() {
        return $this->id && $this->enabled;
    }

    /**
     * Creates the user node in the DOM
     *
     * @param UXMLElement|Xlet|UAppPage $node_parent
     * @param null $options -- not used here
     *
     * @return DOMElement
     */
    public function createNode($node_parent=null, $options=null) {
        $attr = [];
        if ($this->name) $attr['name'] = $this->name;
        if ($this->id) $attr['id'] = $this->id;
        if ($this->enabled) $attr['enabled'] = $this->enabled;
        Debug::tracex('id', $this->id);
        Debug::tracex('name', $this->name);
        $this->node = $this->parentNode($node_parent)->addNode('user', $attr);
		return $this->node;
	}

    /**
     * Logs out the user. Empties session data.
     */
    public function logout() {
        $this->emptyuser();
    }

    protected function emptyuser() {
        $_SESSION['user_id'] = $this->id = 0;
        $_SESSION['user_name'] = $this->name = null;
        $_SESSION['user_email'] = $this->email = null;
        $_SESSION['rightcache'] = null;
    }

    /**
     * User::loadUser()
     * Loads user data into session after login
     * @return void
     */
    protected function loadUser() {
        $this->loadSession();
    }

    function action() {
        // Just must be declared
    }
}
