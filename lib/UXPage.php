<?php
/**
 * class UXPage
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * Displaying pages based on XML/XSL transformation.
 *
 * Utilities for building page XML data.
 *
 * ### Render request parameters
 * - showxml
 * - render (client, server) {@see render()}
 *
 *    requires:
 *        UXMLDoc/UXMLElement
 *        Debug
 */
class UXPage extends Component {
    /** @var string $enc -- encoding of page DOM, default is 'UTF-8' */
    public $enc;
    /** @var UXMLDoc $xmldoc */
    public $xmldoc;
    /** @var UXMLElement $node_content -- /data/content -- main data block from SQL, etc */
    public $node_content;
    /** @var UXMLElement $node_control -- /data/control -- controller parameters */
    public $node_control;
    /** @var Module[] $modules -- registered modules (except autoloaded modules) */
    public $modules = [];
    /** @var string $agent -- signature of browser agent */
    public static $agent;
    /** @var string $baseurl -- base url of application from config */
    public $baseurl;
    /** @var string $script -- url of current script without parameters */
    public $script;

    /** @var UXMLElement $node_data -- global /data node in page DOM */
    protected $node_data;
    /** @var UXMLElement $node_head -- global /data/head node for page frame informations */
    protected $node_head;
    /** @var string $xsl -- name of main renderer XSL */
    public $xsl;
    /** @var bool $browser_xml -- client is capable of rendering xslt */
    protected $browser_xml = true;
    /** @var string $url -- complete url of current page */
    protected $url;
    /** @var bool $secondary -- set true if you launch another page in the same request to avoid closing trace early */
    protected $secondary;
    /** @var string $render -- 'server' or 'client'. Default 'server'. Determined in constructor, may be ovveride on user page, used in the final rendering */
    public $render;

    /** @var UXMLElement $node_main_menu -- /data/head/menu */
    private $node_main_menu;
    /** @var UXMLElement $node_left_menu -- /data/head/leftmenu */
    private $node_left_menu;
    /** @var UXMLElement $node_user_menu -- /data/menu */
    private $node_user_menu;
    /** @var UXMLElement $node_submenu -- /data/head/submenu */
    private $node_submenu;
    /** @var UXMLElement $langmenu -- /data/head/langmenu */
    private $langmenu;
    /** @var UXMLElement $quickmenu -- /data/head/quickmenu */
    private $quickmenu;


    /**
     * # UXPage constructor.
     * ### config elements
     * - enc -- xml encoding (default is UTF-8)
     * - secondary --
     * - baseurl --
     *
     * @param array $config
     *
     * @throws ConfigurationException
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     */
    function __construct($config = null) {
        parent::__construct($config);

        self::globalSettings();

        if (!$this->enc) $this->enc = 'UTF-8';
        if (!$this->baseurl) $this->baseurl = UApp::config('baseurl');

        $this->xmldoc = new UXMLDoc($this->enc);

        $this->node_data = $this->xmldoc->documentElement;

        $this->script = $this->baseurl . '/' . Util::getPath(0);

        $this->node_data->setAttribute('script', $this->script);
        $this->node_data->setAttribute('url', $this->url = $_SERVER['REQUEST_URI']);
        $this->node_data->setAttribute('session_id', session_id());
        $this->node_head = $this->xmldoc->createElement('head');
        $this->node_data->appendChild($this->node_head);
        if ($this->baseurl) $this->node_head->addNode('base', ['href' => $this->baseurl]);
        $this->node_content = $this->node_data->appendChild($this->xmldoc->createElement('content'));
        $this->node_control = $this->node_data->appendChild($this->xmldoc->createElement('control'));
        $this->addParams();
        if (Debug::$trace) {
            $this->addControl([['debug', null, Debug::$trace]]);
        }
        self::$agent = $_SERVER['HTTP_USER_AGENT'] ?? '';

        if ((Util::getSession('browser_xml')) === null) {
            $this->browser_xml = self::certifyAgent(self::$agent);
            Debug::tracex('browser-xml', $this->browser_xml);
        }
        $this->addControl([['agent', ['xml' => ($this->browser_xml ? 'Y' : 'N'), 'name' => self::$agent]]]);
        $this->render = $this->determineRender();
        Debug::tracex('planned render', $this->render);
    }

    /**
     * ## UXPage user destructor
     * - Writes out the page DOM to the tracefile
     *
     * @throws ConfigurationException
     * @throws InternalException
     * @throws ReflectionException
     */
    function finish() {
        if (Debug::$trace) {
            // Writes out the page DOM to the tracefile
            Debug::trace_xml('result', $this->xmldoc, 'output'); // Don't remove it
            if (!$this->secondary) Debug::trace_end();
        }
    }

    /**
     * Determines if given browser agent is capable of XSLT rendering
     * Uses $defpath/certified_user_agents.php definition file
     * Uses also optional user defined rules from `config/agents` path.
     * Returns false if no pattern matched
     *
     * @param string $agent -- signature from HTTP_USER_AGENT variable
     *
     * @return boolean
     * @throws ConfigurationException
     */
    public static function certifyAgent($agent) {
        // Load list of certified user agents
        $defpath = UApp::config('uapp/def', dirname(__DIR__) . '/def');
        $cuafile = $defpath . '/certified_user_agents.php';
        /** @var array $certified -- array of [pattern, min, max, result] */
        $certified = include($cuafile);
        $userfile = UApp::config('config/agents');
        if ($userfile && file_exists($userfile)) {
            // User defined additional or overlay data
            $userdata = include($userfile);
            if (is_array($userdata)) {
                $certified = array_merge($certified, $userdata);
            }
        }

        // Decides upon $cuafile patterns
        foreach ($certified as $rule) {
            [$pattern, $min, $max, $result] = $rule;
            if (preg_match($pattern, $agent, $mm)) {
                $submatch = $mm[1] ?? null;
                Debug::tracex('agent', $pattern . ' -- ' . $submatch);
                if ($min !== null && ($submatch === null || $submatch < $min)) continue;
                if ($max !== null && ($submatch === null || $submatch > $max)) continue;
                return $result;
            }
        }
        return false;
    }

    /**
     * @throws DOMException
     */
    function addParams() {
        /** @var UXMLElement $node_params */
        $node_params = $this->node_control->appendChild($this->xmldoc->createElement('params'));
        foreach ($_GET as $i => $value) {
            //$value = htmlspecialchars($value);
            if (is_array($value)) $value = '[' . implode(',', $value) . ']';
            $node_params->setAttribute(Util::toNameID($i), $value);
        }
    }

    /**
     * @param $name
     * @param string $type
     *
     * @return DOMElement
     * @throws DOMException
     */
    function addCss($name, $type = '') {
        $node_css = $this->addHead('css', $name);
        if ($type != '') $node_css->setAttribute('type', $type);
        return $node_css;
    }

    /**
     * Registers a js include to the page
     *
     * Locator will search for the file in www, modules/* and UApp/modules/* directories
     *
     * @param string $name
     *
     * @return DOMElement
     * @throws DOMException
     */
    function addJs($name) {
        return $this->addHead('js', $name);
    }

    /**
     * Simply creates an xsl node in the head with the given content
     * Do not use in user pages, because it does not find the location of the file
     * Use UAppPage::addXsl() instead.
     *
     * @param string $name -- with extension
     *
     * @return DOMElement -- teh added node
     * @throws DOMException
     */
    function addXsl($name) {
        return $this->addHead('xsl', $name);
    }

    /**
     * @param $name
     * @param int $limit
     *
     * @return bool|DOMElement
     * @throws ConfigurationException
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     */
    function addXslCall($name, $limit = 0) {
        #Debug::trace($name);
        if ($limit) {
            $nodes = $this->node_control->selectNodes("xsl-call[@name='$name']");
            $count = $nodes->length;
            if ($count >= $limit) return false;
        }
        return $this->addControl([['xsl-call', ['name' => $name]]]);
    }

    /**
     * Registers a Module on page with it's js, css and xsl
     *
     * @param string $module
     * @param bool $init -- create module object, else call init if exists
     *   classname of Module
     *
     * @return bool -- success
     * @throws ConfigurationException
     * @throws InternalException
     * @throws DOMException
     */
    function register($module, $init = false) {
        // Megkeresi a fájlokat és regisztrálja a lapon
        $c = Loader::filename($module);
        $basepath = UApp::config('basepath');
        $modulepath = UApp::config('modulepath', $basepath . '/modules');
        $uapp_path = UApp::getPath();
        $f = false;
        #$prefix = $GLOBALS['config']['baseurl'];

        $bases = ["$modulepath", "$uapp_path/modules"];
        for ($i = 0; $i < count($bases); $i++) {
            $base = $bases[$i];
            $dirname = "$base/$c";
            if (file_exists($dirname)) {
                if ($init && file_exists($dirname . '/' . $c . '.php')) {
                    Debug::tracex('register', $c);
                    $this->modules[$module] = new $module($this);
                }
                if (!$init && file_exists($dirname . '/' . $c . '.php')) {
                    // Ha $c osztálynak van init függvénye
                    if (is_callable([$c, 'init'])) {
                        call_user_func([$c, 'init'], $this);
                        Debug::tracex('init', $c);
                    }
                }
                $dir = dir($dirname);
                if (!$dir) {
                    Debug::tracex('Dir error. Path', $module);
                    return false;
                }
                /** @noinspection PhpAssignmentInConditionInspection */
                while (false !== ($entry = $dir->read())) {
                    $ext = substr($entry, strrpos($entry, '.') + 1);
                    #Debug::tracex('register', array($entry, $ext));
                    if ($ext == 'js') $this->addJs($c . '/' . $entry);
                    if ($ext == 'css') $this->addCss($c . '/' . $entry);
                    if ($ext == 'xsl') $this->addXsl($c . '/' . $entry);
                }
                $f = true;
                break;
            }
        }
        if (!$f) {
            Debug::tracex('bases', $bases);
            throw new InternalException("Registration error: Module '$c' not found");
        }
        return true;
    }

    /**
     * @throws DOMException
     * @noinspection PhpUnused
     */
    function registerJs($script, $position = 'after') {
        $this->addHead('js-inline', $script, ['position' => $position]);
    }

    /**
     * UXPage::addData()
     * data node-hoz adja a listát
     *
     * @param mixed $node_list : array(szöveg | array(node-név, attributum-tömb, [node-list]))
     *
     * @return DOMElement
     * @throws ConfigurationException
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     * @throws Exception
     */
    function addData($node_list) {
        //tracex('nodelist', $node_list);
        return $this->node_data->addNodeList($node_list);
    }

    /**
     * UXPage::addContent()
     * content node-hoz adja a listát
     *
     * @param mixed $node_list : array(szöveg | array(node-név, attributum-tömb, [node-list]))
     *
     * @return DOMElement -- the first added node
     * @throws ConfigurationException
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     * @throws Exception
     */
    function addContent($node_list) {
        return $this->node_content->addNodeList($node_list);
    }

    /**
     * Content Node alá létrehoz egy node-ot a megadott attributumokkal és szöveges tartalommal
     *
     * A szöveges tartalom speciális karakterei kódolásra kerülnek.
     *
     * @param string $name
     * @param array $attributes
     * @param string $content
     *
     * @return UXMLElement -- the added node
     * @throws DOMException
     */
    function addContentNode($name, $attributes = null, $content = null) {
        return $this->node_content->addNode($name, $attributes, $content);
    }

    function clearContent() {
        $this->node_content->removeChildren();
        $this->node_control->removeChildren();
    }

    /**
     * Hozzáad egy vagy több node-ot a /data/control fejezethez
     *
     * @param mixed $node_list - nem tömb: szöveges tartalom - tömb: [[node-név, attributum-tömb, [node-list]]]
     *
     * @return UXMLElement -- the node added
     * @throws ConfigurationException
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     * @throws Exception
     */
    function addControl($node_list) {
        return $this->node_control->addNodeList($node_list);
    }

    /**
     * @param string $var
     * @param string $value
     * @param array|null $attr
     *
     * @return UXMLElement
     * @throws ConfigurationException
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     * @throws Exception
     */
    function addControlVar($var, $value = '', $attr = null) {
        $node = $this->node_control->addNodeList([[$var, null, (string)$value]]);
        if ($attr) array_walk($attr,
            function ($v, $k, $n) {
                /** @var DOMElement $n */
                $n->setAttribute($k, $v);
            }, $node
        );
        return $node;
    }

    /**
     * @param array $ba
     * @param string $name
     *
     * @return UXMLElement
     * @throws ConfigurationException
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     */
    function addBitArray($ba, $name) {
        $node_ba = $this->addControl([[$name]]);
        foreach ($ba as $b) {
            $node_ba->addNode('bit', [
                'value' => $b[0],
                'attr' => $b[1],
                'name' => $b[2],
                'descr' => $b[3],
                'title' => $b[4]
            ]);
        }
        return $node_ba;
    }

    /**
     * A DOM <head> fejezetéhez ad egy szöveg tartalmú node-ot.
     *
     * @param string $name
     * @param string $content
     * @param null|array $attributes
     *
     * @return UXMLElement
     * @throws DOMException
     */
    function addHead($name, $content = '', $attributes = null) {
        return $this->node_head->addNode($name, $attributes, (string)$content);
    }

    /**
     * A DOM <head> fejezetéhez ad egy strukturált tartalmat.
     * Rekurzív.
     *
     * @param mixed $content -- szöveg vagy [[nodename, attributes, contents],...]
     *
     * @return UXMLElement -- az első hozzáadott node
     * @throws ConfigurationException
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     * @throws Exception
     */
    function addHeadTree($content) {
        return $this->node_head->addNodeList($content);
    }

    /**
     * Adds menudata to the existing menu node
     *
     * use `$this->menu->addItems()`
     *
     * @param UXMLElement $node_menu
     * @param array $menudata
     *   array of menu items: array(enabled, caption, url, hint, confirm, [key=>value...]
     *   add @target to url if a.target is needed
     *   add ##class to url if a.class is needed
     *     Attributes:
     *    - class=>'a class'
     *        - li-class=>'li class'
     *        - autosubmit=>act
     *        - act=>act
     *        - tooltip=message
     *        - block=0,1,2
     *
     * @return UXMLElement
     *   the menu node itself
     * @throws UAppException
     * @throws DOMException
     * @deprecated use Menu
     */
    protected function addMenu($node_menu, $menudata) {
        foreach ($menudata as $item) {
            /** @var DOMElement $itemnode */
            $itemnode = $this->xmldoc->createElement('item');
            $itemnode = $node_menu->appendChild($itemnode);
            $itemnode->setAttribute('enabled', $enabled = ($item[0] ? 1 : 0));
            $itemnode->setAttribute('caption', $item[1]);
            $url = $item[2];
            $jp = 'javascript:';
            if (substr($url, 0, strlen($jp)) == $jp)
                $itemnode->setAttribute('click', $enabled ? substr($url, strlen($jp)) : '');
            else {
                if (strpos($url, '##') !== false) {
                    [$url, $class] = explode('##', $url);
                    $itemnode->setAttribute('class', $class);
                }
                if (strpos($url, '@') !== false) {
                    [$url, $target] = explode('@', $url);
                    $itemnode->setAttribute('target', $target);
                }
                $itemnode->setAttribute('url', $url);
            }
            if (isset($item[3])) $itemnode->setAttribute('hint', $item[3]);
            if (isset($item[4])) $itemnode->setAttribute('confirm', $item[4]);
            if (!is_array($item)) throw new UAppException('array expected');
            foreach ($item as $key => $value) {
                if (!is_numeric($key)) $itemnode->setAttribute($key, $value);
            }
        }
        return $node_menu;
    }

    /**
     * Adds menudata to the existing main menu or creates if not exists.
     * Add `@target` to url if link target is needed
     * Add ##class to url if a.class is needed
     *
     * Use `$this->mainMenu->addItem(array('enabled', 'caption', 'url', 'hint', 'confirm', 'attributes'));`
     * where `$this->mainMenu` is an initialized Menu, and generated before rendering.
     *
     * @param array $menudata
     *   array of menu items: array(enabled, caption, url, hint, confirmtext, attributes)
     *
     * @return DOMElement
     *   the main menu node
     * @throws UAppException
     * @throws DOMException
     * @deprecated use Menu
     */
    function addMainMenu($menudata) {
        if (!$this->node_main_menu) $this->node_main_menu = $this->node_head->getOrCreateElement('menu');
        return $this->addMenu($this->node_main_menu, $menudata);
    }

    /**
     * Adds menudata to the existing submain menu or creates if not exists
     *
     * Add `@`target to url if link target is needed.
     * Add `##class` to url if a.class is needed.
     *
     * @param array $menudata -- array of menu items: array(enabled, caption, url, hint, confirmtext, key=>value)
     *
     * @return DOMElement -- the main menu node
     * @throws UAppException
     * @throws DOMException
     * @deprecated use Menu class
     */
    function addSubMenu($menudata) {
        if (!$this->node_submenu) $this->node_submenu = $this->node_head->getOrCreateElement('submenu');
        return $this->addMenu($this->node_submenu, $menudata);
    }

    /**
     * @param array $menudata
     *
     * @return DOMElement
     * @throws UAppException
     * @throws DOMException
     * @deprecated use Menu class
     */
    function addUserMenu($menudata) {
        if (!$this->node_user_menu) $this->node_user_menu = $this->node_data->getOrCreateElement('menu');
        return $this->addMenu($this->node_user_menu, $menudata);
    }

    /**
     * Létrehoz vagy bővít egy kontextusfüggő menüsort a megadott szülő node alá.
     *
     * ### members of menudata items:
     * - enabled
     * - caption
     * - url
     * - hint
     * - confirm
     * - [classname,...]
     * - [key=>value...]
     * - [data=>[var=>value]])
     *
     * ### Special extensions of url:
     * - add `@`target to url if a.target is needed
     * - add `##class` to url if a.class is needed
     *
     * Használati példa: többszörös kijelöléssel működő lista elemire vonatkozó alsó menüt a lista-node testvéreként kell elhelyezni
     *
     * @param UXMLElement $node_parent
     * @param array $menudata
     *
     * @return UXMLElement
     * @throws UAppException
     * @throws DOMException
     * @deprecated use Menu class
     */
    function addContextMenu($node_parent, $menudata) {
        $this->node_user_menu = $node_parent->getOrCreateElement('menu');
        return $this->addMenu($this->node_user_menu, $menudata);
    }

    /**
     * Adds menudata to the existing left menu or creates if not exists
     *
     * @param array $menudata -- array of menu items: array(enabled, caption, url, hint)
     *
     * @return DOMElement -- the leftmenu node
     * @throws UAppException
     * @throws DOMException
     * @deprecated use Menu class
     */
    function addLeftMenu($menudata) {
        if (!$this->node_left_menu) $this->node_left_menu = $this->node_head->getOrCreateElement('leftmenu');
        return $this->addMenu($this->node_left_menu, $menudata);
    }

    /**
     * Adds menudata to the existing quickmenu or creates if not exists
     *
     * @param array $menudata -- array of menu items: array(enabled, caption, url, hint)
     *
     * @return DOMElement -- the menu node
     * @throws UAppException
     * @throws DOMException
     * @deprecated use Menu class
     */
    function addQuickMenu($menudata) {
        if (!$this->quickmenu) $this->quickmenu = $this->node_head->getOrCreateElement('quickmenu');
        return $this->addMenu($this->quickmenu, $menudata);
    }

    /**
     * Adds menudata to the existing language menu or creates if not exists
     *
     * @param array $menudata -- array of menu items: array(enabled, caption, url, la)
     *
     * @return DOMElement -- the menu node
     * @throws UAppException
     * @throws DOMException
     * @deprecated use Menu class
     */
    function addLangMenu($menudata) {
        $this->langmenu = $this->node_head->getOrCreateElement('langmenu');
        return $this->addMenu($this->langmenu, $menudata);
    }

    /**
     * Determines rendering mode (server or client)
     *
     * Priority:
     *    0. If cannot use server side library => client
     *  0. If browser is not certified for client render => server
     *  1. config/render setting is `$mode only` => $mode
     *  2. User request
     *  3. Session stored user request
     *  4. debug will prefer server render
     *  5. config/render setting
     *
     * Result of determineRender method may be overridden on the user page.
     * However, zero priority rules will be applied again just before rendering.
     *
     * @return string ('server' or 'client')
     */
    function determineRender() {
        if (!$this->browser_xml) return 'server';
        $cxslt = Loader::classExists('XSLTProcessor');
        if (!$cxslt) return 'client';

        if (isset($GLOBALS['config']['render'])) {
            $renderconfig = $GLOBALS['config']['render'];
        } else {
            $renderconfig = '';
        }
        if ($renderconfig == 'server only') return 'server';
        if ($renderconfig == 'client only') return 'client';

        $userrender = Util::getSession('userrender');
        if ($userrender == 'server') return 'server';
        if ($userrender == 'client') return 'client';

        //if(Debug::$trace) return 'server';

        if ($renderconfig == 'server') return 'server';
        if ($renderconfig == 'client') return 'client';

        return 'client';
    }

    /**
     * ## UXPage::render()
     *
     * Generates HTML output from xmldoc data and xslt view.
     *
     * Dependencies:
     * - $this->xmldoc is the document to render.
     * - $this->xsl is the local name of XSL file to render with.
     * - xslpath, datapath/xsl, uapp-path/xsl are used to search the xsl file
     * - $_SESSION['showxml'g is used to display raw xml at the end
     * - $this->render is the render mode (client/server)
     *
     * @return void
     * @throws ConfigurationException
     * @throws InternalException
     * @throws UAppException
     */
    function render() {
        $_trace = Debug::$trace;
        $_showxml = Util::getSession('showxml');

        if ($_showxml) $this->node_data->setAttribute('showxml', $_showxml);
        if ($_trace) $this->node_data->setAttribute('trace', $_trace);
        $xslname = $this->xsl . '.xsl';
        $xslpaths = [
            UApp::config('xslpath') . '/',
            UApp::$dataPath . '/xsl/',
            UApp::getPath() . '/xsl/',
        ];
        $xslpath = false;
        foreach ($xslpaths as $xp) {
            if (file_exists($xp . $xslname)) {
                $xslpath = $xp;
                break;
            }
        }

        if (!$xslpath) {
            Debug::tracex('original-xsl', $xslname);
            $this->xmldoc->documentElement->setAttribute('xsl', $xslname);
            $xslname = 'xslnotfound.xsl';
        }

        $xslurl = UApp::config('xslurl', UApp::config('baseurl') . '/xsl') . '/';
        $enc = $this->enc;

        if ($this->render == 'client') {
            // Client render
            Debug::tracex('render-client', $xslname);
            $node_pi = $this->xmldoc->createProcessingInstruction('xml-stylesheet', 'type="text/xsl" href="' . $xslurl . $xslname . '"');
            $this->xmldoc->insertBefore($node_pi, $this->node_data);
            $this->xmldoc->formatOutput = true;
            $output = $this->xmldoc->saveXML();
            header("Content-Type: text/xml; charset=$enc");
            header("Cache-Control: no-cache, must-revalidate");  // HTTP/1.1
            header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
            echo $output;
        } else {
            // Server render
            Debug::tracex('render-server', $xslname);
            header("Cache-Control:   no-cache, must-revalidate");  // HTTP/1.1
            header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
            $c = $this->xmldoc->documentElement->getAttribute('content-type');

            if ($c || mb_internal_encoding() == 'UTF-8') {
                $content_type = $c ?: 'text/html';
                header("Content-Type: $content_type; charset=$enc");
                Debug::tracex('$content_type', $content_type);
                if ($result = $this->xmldoc->process_xml($xslpath . $xslname)) {
                    echo $result; //->saveXML();
                } else {
                    throw new UAppException("rendering error at $xslpath$xslname");
                }
            } else {
                Debug::tracex('Opera render', $c);
                // FireFox, Opera esetén csak html output működik!
                $result = $this->xmldoc->process_html($xslpath . $xslname);
                echo str_replace('&#251;', 'ű', str_replace('&#245;', 'ő', $result));
            }
        }
        flush(); // Nem hatásos...
    }

    public static function isHTTPS() {
        if (!array_key_exists('HTTPS', $_SERVER)) {
            /* Not a https-request. */
            return FALSE;
        }
        if ($_SERVER['HTTPS'] === 'off') {
            /* IIS with HTTPS off. */
            return FALSE;
        }
        /* Otherwise, HTTPS will be a non-empty string. */
        return $_SERVER['HTTPS'] !== '';
    }

    public static function getSelfHost() {
        if (array_key_exists('HTTP_HOST', $_SERVER)) {
            $currenthost = $_SERVER['HTTP_HOST'];
        } elseif (array_key_exists('SERVER_NAME', $_SERVER)) {
            $currenthost = $_SERVER['SERVER_NAME'];
        } else {
            /* Almost certainly not what you want, but ... */
            $currenthost = 'localhost';
        }
        if (strstr($currenthost, ":")) {
            $currenthostdecomposed = explode(":", $currenthost);
            $currenthost = $currenthostdecomposed[0];
        }
        return $currenthost;# . self::getFirstPathElement() ;
    }

    /**
     * @return string
     */
    public static function selfURLhost() {
        $currenthost = self::getSelfHost();

        $isHTTPS = strpos($_SERVER['REQUEST_URI'], 'https://') === 0;
        if ($isHTTPS) {
            $protocol = 'https';
        } else {
            $protocol = 'http';
        }
        $portnumber = $_SERVER["SERVER_PORT"];
        $port = ':' . $portnumber;
        if ($protocol == 'http') {
            if ($portnumber == '80') $port = '';
        } else {
            if ($portnumber == '443') $port = '';
        }
        return $protocol . "://" . $currenthost . $port;
    }

    /**
     * @param int $withHost
     *
     * @return string
     */
    public static function selfURLNoQuery($withHost = 0) {

        $selfURLhost = $withHost ? self::selfURLhost() : '';
        $selfURLhost .= $_SERVER['SCRIPT_NAME'];
        if (isset($_SERVER['PATH_INFO'])) {
            $selfURLhost .= $_SERVER['PATH_INFO'];
        }
        return $selfURLhost;

    }

    /**
     * Reads XML user settings from the request and stores them into SESSION
     *
     * @return void
     * @throws InternalException
     */
    static function globalSettings() {
        $showxml = Util::getReq('showxml');
        if ($showxml == 'on') $_SESSION['showxml'] = 1;
        if ($showxml == 'off') $_SESSION['showxml'] = 0;

        $userrender = Util::getReq('render');
        if ($userrender) $_SESSION['userrender'] = $userrender;
    }

    /**
     * @param DBX $db
     * @param UXMLElement $node_parent
     *
     * @throws ConfigurationException
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws UAppException
     * @deprecated
     */
    public static function loadRightTypes($db, $node_parent) {
        $node_righttypes = $node_parent->addNode('righttypes');
        $node_righttypes->query($db, 'righttype', /** @lang text */ "select * from righttype order by name");
    }

}
