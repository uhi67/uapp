<?php
/**
 * class ServerErrorException
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 *
 */
/** @noinspection PhpClassNamingConventionInspection */

/**
 * ServerErrorException represents an "Internal Server Error" HTTP exception with status code 500.
 */
class ServerErrorException extends UserException {
    /**
     * Constructor.
     * @param string $message |array error message or array(message, params, info) where message contains %s style param references
     * @param string $goto
     * @param mixed $extra
     * @param Exception $previous The previous exception used for the exception chaining.
     */
    public function __construct($message = null, $extra = null, $goto = null, $previous = null) {
        parent::__construct($message, $extra, $goto, 500, $previous);
    }
}
