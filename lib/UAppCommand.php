<?php
/**
 * class UAppCommand
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2017
 * @license MIT
 */
/** @noinspection PhpUnused */

/**
 * # UAppCommand
 *
 * Base class for Command line command  (counterpart of UAppPage for html pages).
 *
 * Override it in application level in AppCommand.
 * All command classes in `commands` dir should extend AppCommand instead of this
 *
 * ## Usage
 *
 * ### Command line:
 * `$ php appName controller1 action1 parameters`
 * - appName is your actual application name (originally uapp script in application root)
 * - The example is running actAction1 method in Controller1 class with unnamed parameters as arguments.
 *
 * Default action is `default`. The default implementation of default lists the actions of the command.
 *
 * ### Command-line parameters
 * - first numeric parameter is accessable as $this->id
 * - all named parameters (passed in `name=value` form) are accessable as $this->param($name).
 * - all non-named parameters are accessable via $params array which is passed as the first argument of the action method.
 */
abstract class UAppCommand extends Component {
    public $user;
    protected $title;
    protected $h1;
    protected $act = '';        // action word
    protected $id;
    public $appname;
    protected $name;            // Page name for logging, etc.
    /** @var UApp */
    protected $app;
    /** @var DBX $db */
    public $db;                // modulból el kell érni
    public $rights;            // rightsystem object based on Rights class. Must be initialized from $app->rights
    private $logged;        // Naplózott művelet

    private static $colors = [
        'ok' => 'light green',
        'warning' => 'yellow',
        'error' => 'light red',
        'info' => 'light blue',
        'default' => 'light gray',
    ];

    /**
     * Command line command base class
     *
     * @param UApp $app
     *
     * @throws ConfigurationException
     * @throws InternalException
     * @throws UAppException
     */
    function __construct($app, $config = []) {
        parent::__construct($config);
        $this->rights = $app->rights;

        $this->logged = [];
        $this->app = $app;
        $this->db = $app->db;

        $this->appConstruct();      // App level
        $this->preConstruct();      // Page level user callback

        $pagename = strtolower(preg_replace('/(\w+)Page/', '\1', get_class($this)));


        if (isset(UApp::$args['act'])) $this->act = UApp::$args['act'];
        else if (isset(UApp::$argn[0]) && !is_numeric(UApp::$argn[0])) $this->act = array_shift(UApp::$argn);
        if ($this->act == '') $this->act = 'default';

        if (isset(UApp::$args['id'])) $this->id = UApp::$args['id'];
        else if (isset(UApp::$argn[0]) && is_numeric(UApp::$argn[0])) $this->id = array_shift(UApp::$argn);

        try {
            $this->appLoadData();   // App level
            $this->loadData();      // user callback
        } catch (Exception $e) {
            echo Ansi::color('Exception at UAppCommand construct', 'yellow') . "\n";
            /** @var UAppException $e */
            $this->showException($e, is_a($e, 'RightException') ? 'Jogosultsági hiba' : 'Hiba az adatok betöltése közben');
        }
        if (!$this->name) $this->setName($pagename);
    }

    function finish() {
    }

    /**
     * Must initialize $user object.
     * The default implementation does nothing.
     *
     * @return void
     */
    protected function initUser() {
    }

    /**
     * Callback runs before initializing user.
     * The default implementation does nothing.
     *
     * @return void
     */
    protected function preConstruct() {
    }

    /**
     * Callback runs after initializing user.
     * The default implementation does nothing.
     *
     * @return void
     */
    protected function loadData() {
    }

    /**
     * Callback runs after render.
     * The default implementation does nothing.
     *
     * @return void
     */
    protected function afterRender() {
    }


    /**
     * Callback before loading parameters and creating user.
     * Called before page->preConstruct()
     * The default implementation does nothing.
     *
     * @return void
     */
    function appConstruct() {
    }

    /**
     * Callback after loading parameters and creating user.
     * Called before page->loadData()
     */
    function appLoadData() {
    }

    /**
     * Displays an exception in command line.
     * Uses ANSI colors.
     *
     * @param UAppException $e
     * @param string $title
     *
     * @return void
     */
    function showException($e, $title) {
        if ($title) echo Ansi::color($title, 'light red', 'black'), "\n";
        UApp::showException($e, 1);
    }

    function __destruct() {
        try {
            unset($this->user);
            #parent::__destruct();
        } catch (Exception $e) {
            echo "Exception at " . __CLASS__ . " destruct: " . $e->getMessage(), "\n";
        }
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @throws ConfigurationException
     */
    function go() {
        try {
            if ($this->act != '') $this->processActions($this->act);
        } catch (Exception $e) {
            echo Ansi::color('Exception at UAppCommand::go', 'brown') . "\n";
            /** @var UAppException $e */
            $title = is_a($e, 'UAppException') ? $e->getName() : 'Hiba a művelet feldolgozása során';
            $this->showException($e, $title);
        }
        $this->log(); // Végül mindenképpen logoljuk paraméterek nélkül, ha ezzel az akcióval még nem volt
    }

    function processActions($act) {
        $func = 'act' . ucfirst($act);
        if (is_callable([$this, $func])) {
            $params = UApp::$argn; //$GLOBALS['argv'];
            call_user_func([$this, $func], $params);
        } else $this->addMessages('Ismeretlen művelet: ' . $act, 'error');
    }

    /**
     * Gets a named command-line parameter
     * in form of name=value
     *
     * @param string $name
     * @param string $default
     *
     * @return string -- null if parameter does not exist
     */
    public function param($name, $default = null) {
        return isset(UApp::$args[$name]) ? UApp::$args[$name] : $default;
    }

    /**
     * Sets the name of the page
     * Used at logging, default view, etc.
     * @param string $name
     * @return string
     */
    protected function setName($name) {
        return $this->name = $name;
    }

    /**
     * Returns the base-path of the module. Searches the application and the UApp library
     * TODO: csak regisztrált modulok esetén, cache
     *
     * @param string $name
     * @param bool $relative
     *
     * @return string/false if not found
     * @throws ConfigurationException
     */
    function getModulePath($name, $relative = false) {
        $basepath = UApp::config('basepath');
        $modulepath = UApp::config('modulepath', $basepath . '/modules');
        $uapp_path = UApp::getPath(); //config('uapp/path');
        $bases = [$modulepath, "$uapp_path/modules"];
        for ($i = 0; $i < count($bases); $i++) {
            $base = $bases[$i];
            $dirname = "$base/$name";
            if (file_exists($dirname)) {
                $dirname = realpath($dirname);
                if ($relative)
                    return Util::getRelativePath(realpath($basepath), $dirname);
                else
                    return $dirname;
            }
        }
        return false;
    }

    /**
     * adds one or more msg node to control
     *
     * @param mixed $msg -- message or message array
     * @param array $attr -- message attributes pl. array('class'=>'error')
     */
    function addMessages($msg, $attr = null) {
        $types = explode(' ', ArrayUtils::getValue($attr, 'class', ''));
        $type = '';
        foreach ($types as $t) {
            if (in_array($t, array_keys(self::$colors))) $type = $t;
        }
        if (is_array($msg)) {
            for ($i = 0; $i < count($msg); $i++) {
                echo $type ? Ansi::color($type . ': ', self::msgcolor($types)) : '', $msg[$i], "\n";
            }
        }
        echo $type ? Ansi::color($type . ': ', self::msgcolor($type)) : '', $msg, "\n";
    }

    public static function msgcolor($msgtype) {
        if (is_array($msgtype)) {
            $color = '';
            foreach ($msgtype as $t)
                $color .= ArrayUtils::getValue(self::$colors, $t, self::$colors['default']);
        } else
            $color = ArrayUtils::getValue(self::$colors, $msgtype, self::$colors['default']);
        return $color;
    }

    /**
     * Tevékenységnapló fájlba
     * Ha az adott action már naplózva lett, paraméter nélkül mégegyszer nem kell.
     *
     * @param string $params
     * @param string $action -- ha meg van adva, felülírja a page->act értékét
     *
     * @return void
     * @throws ConfigurationException
     */
    function log($params = '', $action = null) {
        Debug::tracex('log ' . $action, $params);
        $page = $this->name ?: get_class($this);
        $action = $action ?: $this->act;
        if (isset($this->logged[$action]) && !$params) return;
        $paramx = is_array($params) ? implode("\t", $params) : $params;
        $this->logged[$action] = true;
        $this->app->log("$page\t$action\t$paramx");
    }

    /**
     * Megjelenít egy üzenetlapot egy vagy több megjelenítendő üzenetsorral
     *
     * @param string $title -- page title (not used here)
     * @param string $h1 -- main title
     * @param string $h2 -- secondary title
     * @param string|array $msg -- message or messages
     * @param string $url -- not used here
     * @param array $attr -- message attributes, e.g. class (not used)
     * @return void
     */
    function showMessage(/** @noinspection PhpUnusedParameterInspection */ $title, $h1, $h2, $msg, $url = null, $attr = null) {
        echo Ansi::color($msg, 'yellow'), "\n";
        $this->addMessages($msg, $attr);
        if ($h1 != null) $this->setH1($h1);
        if ($h2 != null) $this->setH2($h2);
    }

    /**
     * Displays an error message and finishes render.
     * Displays a next button with given url.
     *
     * @param mixed $cim
     * @param string $szoveg
     * @param mixed $url
     *
     * @return void
     *
     * @deprecated -- use Exception
     */
    function errorMessage($cim, $szoveg = '', $url = null) {
        Debug::tracex('cim', $cim);
        $bt = debug_backtrace();
        $b = $bt[1];
        $c = $bt[0];
        $hely = $b['class'] . $b['type'] . $b['function'] . ':' . $c['file'] . '#' . $c['line'];
        $e = UApp::la('uapp', 'Error');
        $this->showMessage($e, $e, $cim, [$szoveg, $hely], $url, ['class' => 'error']);
        exit;
    }

    /**
     * @param string $message
     * @param null $url
     *
     * @throws InternalException
     *
     * @deprecated -- use Exception
     */
    function internalError($message = '', $url = null) {
        throw new InternalException($message, null, $url);
    }

    /**
     * @param string $msg
     * @param string $url
     *
     * @throws ForbiddenException
     * @deprecated -- use Exception
     */
    function insufficientRights($msg = '', $url = '') {
        throw new ForbiddenException($msg, null, $url);
    }

    function setH1($h2, /** @noinspection PhpUnusedParameterInspection */ $h2l = null) {
        echo Ansi::color("\n$h2", 'blue', 'white'), "\n";
    }

    function setH2($h2, /** @noinspection PhpUnusedParameterInspection */ $h2l = null) {
        echo Ansi::color("\n$h2", 'light blue', 'white'), "\n";
    }

    /**
     * The default implementation of the default command lists the available subcommands
     *
     * @return void
     * @throws ConfigurationException
     * @throws ReflectionException
     */
    public function actDefault() {
        $name = $this->app->config('title', $this->app->config('appname', 'UApp'));
        $ver = $this->app->config('ver', '');
        echo Ansi::color($name . ' ' . $ver, 'green') . "\n";
        echo Ansi::color("Index of actions", 'green') . "\n";
        static::showHelp(static::class);
    }

    public static function showHelp($class) {
        $command = static::getCommandName();
        if (!is_a($class, UAppCommand::class, true)) throw new Exception($class . ' must be an UAppCommand');
        $reflector = new ReflectionClass($class);

        // Display command name with optional first line of class comment
        $comment = $reflector->getDocComment();
        $descr = $comment && preg_match_all("~\n\s+\*\s([^\n\r]*)~", $comment, $matches) ? $matches[1][0] : $class::getCommandFile();
        echo Ansi::color($command, 'blue') . "\t -- " . $descr . "\n";

        $methods = get_class_methods($command . 'Command');
        foreach ($methods as $method) {
            if (preg_match('/^act([A-Z]\w+)/', $method, $m) && $m[1] != 'Default') {
                $action = strtolower($m[1]);
                echo Ansi::color("\t\t$action", 'light blue') . "\t";
                // TODO: Find and print argument names by @param
                //  Case 1: @param array $params param1, param2
                //  Case 2: @param string|int $paramX
                // TODO: Find and print @example
                // Extract and print the first line of the method comment
                $comment = $reflector->getMethod($method)->getDocComment();
                if (preg_match_all("~\n\s+\*\s([^\n\r]*)~", $comment, $matches)) echo '-- ', $matches[1][0];
                echo PHP_EOL;
            }
        }
    }

    /**
     * Returns the command name based on uncamelized short classname
     * @return string
     */
    public static function getCommandName() {
        $reflect = new ReflectionClass(static::class);
        return Util::uncamelize(preg_replace('~Command$~', '', $reflect->getShortName()));
    }

    public static function getCommandFile() {
        $reflect = new ReflectionClass(static::class);
        return $reflect->getFileName();
    }
}
