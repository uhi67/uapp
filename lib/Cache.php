<?php
/**
 * Class Cache
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * A file cache v2.0
 *
 * No other cache implementations in this version.
 *
 * config: cache
 *        path    -- absolute path to save files. Deafult is datapath/cache
 *        enabled -- 1=caching is enabled
 *        ttl        -- default ttl (def 900 = 15 min)
 *        hash    -- levels of hash directories (default 0)
 *
 * - Data will be saved to the file computed from hash of given name,
 * - the ttl value into file's mtime
 *
 * Cache may be used
 * - by specifying ttl during creation (or using default)
 * - by specifying ttl during `get` (relative to the creation ttl time, so you best to create with 0)
 *
 * Changes in v2.0
 * - ttl files no longer used. Instead, file mod time is set to the expiration, if given.
 * - if expiration is specified at get, it is considered relative to the mod-time set at creation
 * - files are put into (multiple levels of) hash directories if hash setting is greater then 0.
 *
 */
class Cache {
    public $path;
    public $enabled;
    public $ttl = 0;
    /** @var int $hash -- levels of hashed directories, default is no hashing, maximum is 4 */
    public $hash = 0;

    /**
     * Cache constructor.
     *
     * @param array $cacheconfig
     *
     * @throws ConfigurationException
     */
    function __construct($cacheconfig = null) {
        if ($cacheconfig === null) $cacheconfig = UApp::config('cache', [
            'path' => UApp::$dataPath . '/cache', // Writeable path to store cache files
            'enabled' => 1,                       // Cache is enabled
            'ttl' => 1800,                        // Default time-to-live in seconds
            'hash' => 0,
        ]);
        $this->path = isset($cacheconfig['path']) ? $cacheconfig['path'] : $GLOBALS['config']['datapath'] . '/cache';
        $this->enabled = isset($cacheconfig['enabled']) ? $cacheconfig['enabled'] : 0;
        if (!file_exists($this->path)) mkdir($this->path, 0775);
        $this->ttl = isset($cacheconfig['ttl']) ? $cacheconfig['ttl'] : 900; // 15 mins
        $this->hash = isset($cacheconfig['hash']) ? $cacheconfig['hash'] : 0;
    }

    /**
     * Returns data from cache or null if not found or expired.
     * Deletes the expired item.
     * Expiration is counted from filemtime+ttl
     *
     * @param string $name
     * @param int $ttl -- if given, modifies expiration (only for this query)
     * @return mixed
     */
    public function get($name, $ttl = 0) {
        if (!$this->enabled) return null;
        $filename = $this->filename($name, false);
        if (!file_exists($filename)) return null;

        $expires = filemtime($filename) + $ttl;
        if (time() > $expires) {
            unlink($filename);
            return null;
        }
        $value = unserialize(file_get_contents($filename));
        return $value;
    }

    /**
     * Saves data into cache
     *
     * @param string $name
     * @param mixed $value
     * @param int $ttl
     * @return int -- number of bytes written or false on failure
     */
    public function set($name, $value, $ttl = null) {
        if (!$this->enabled) return null;
        $filename = $this->filename($name, true);
        if (file_exists($filename)) unlink($filename);
        if ($ttl === null) $ttl = $this->ttl;
        $r = file_put_contents($filename, serialize($value));
        touch($filename, time() + $ttl);
        return $r;
    }

    /**
     * Returns a stored named value if exists and not expired, creates otherwise
     *
     * Careful with null value - it will be never cached.
     * Use null value to not to use cache :-)
     *
     * @param string $name
     * @param int $ttl
     * @param callable $create -- callback to generate new value if not found in cache: function(): mixed
     * @param callable $condition -- callback to decide to store new value or not: function($value): bool
     *
     * @return mixed|null
     */
    public function useCache($name, $ttl, $create, $condition = null) {
        $stored = $this->get($name);
        if ($stored !== null) return $stored;
        $value = $create();
        if ($value === null) return null;
        if ($condition !== null && is_callable($condition) && !$condition($value)) return $value;
        $this->set($name, $value, $ttl);
        return $value;
    }

    /**
     * Empty the cache,
     * or deletes only items match the pattern
     *
     * @param string|null $pattern -- RegEx pattern (with terminators)
     */
    public function purge($pattern = null) {
        $this->gc(true, '', $pattern);
    }

    /**
     * Deletes expired files.
     * Deletes empty directorise.
     *
     * @param int $ttl -- relative ttl to the file times
     * @param string $path -- path relative to cache base path
     * @param string|null $pattern -- remove only files matching the RegEx pattern if given
     *
     * @return int -- coount of not-deleted files and subdirectories in the directory
     */
    public function gc($ttl = 0, $path = '', $pattern = null) {
        $dir = $this->path . ($path ? '/' : '') . $path;
        $dh = opendir($dir);
        $c = 0;
        while (($entry = readdir($dh)) !== false) {
            if ($entry == '.' || $entry == '..') continue;
            $file = $dir . '/' . $entry;
            if (is_dir($file)) {
                $n = $this->gc($ttl, $path . '/' . $entry, $pattern);
                if ($n == 0) rmdir($file);
                else $c++;
            } else {
                if (($ttl === true || time() > filemtime($file) + $ttl) && ($pattern === null || preg_match($pattern, $entry))) {
                    if (!unlink($file)) throw new Exception("File '$file' cannot be deleted");
                } else $c++;
            }
        }
        closedir($dh);
        return $c;
    }

    /**
     * Computes the directory name from the hash of the name using md5
     * Two-hex parts are used $this->hash times.
     * Optionally creates the directory
     *
     * @param $name -- name of the entry
     * @param $create -- create dir if not exist
     * @return string -- directory name to be prepended to name (relative to the cache base dir)
     */
    private function filename($name, $create = false) {
        $hash = md5($name);
        $dir = '/';
        for ($i = 0; $i < $this->hash; $i++) {
            $dir .= substr($hash, $i * 2, 2) . '/';
            if ($create && !is_dir($this->path . $dir)) mkdir($this->path . $dir, 0775);
        }
        $fileName = $this->path . $dir . Util::toNameID($name, '_', '.-', 220);
        return $fileName;
    }
}
