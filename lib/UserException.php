<?php
/**
 * class UserException
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * An UAppException class to log and display error data to end user
 * Features:
 *
 *  - writes an error logfile with exception details.
 *
 * @see UAppException
 */
class UserException extends UAppException {
    public $message = '';
    /** @var string $goto -- url for user to continue */
    public $goto;
    /** @var string|array $extra -- debug info */
    private $extra;
    /** @var int HTTP status code */
    public $statusCode;

    /**
     * @param string|array $message -- text message or [message, info]
     * @param string|array $extra
     * @param string $goto
     * @param int $code -- http response code
     * @param Exception $previous -- Exception chaining
     * @return void
     */
    public function __construct($message, $extra = null, $goto = null, $code = 0, $previous = null) {
        if (is_array($message)) {
            if (isset($message[0])) $this->message = $message[0];
            if (isset($message[1])) $this->info = $message[1];
        } else {
            $this->message = $message;
        }
        $this->goto = $goto;
        $this->extra = $extra;
        $this->info = Util::forceArray($this->info);
        $this->statusCode = $code;
        parent::__construct($this->getText(), $code, $previous);
    }

    /**
     * Returns the full text of the message, complemented type-specific prefix
     * @return string
     */
    function getText() {
        return vsprintf($this->message, $this->info);
    }

    /**
     * Returns the name of this exception (http response code)
     * This will displayed as main title on html page
     *
     * @return string
     * @throws ConfigurationException
     * @throws InternalException
     */
    public function getName() {
        $text = UApp::getHttpResponseCode($this->statusCode);
        if ($text) {
            return $text;
        } else {
            return UApp::la('uapp', 'Internal error');
        }
    }

    /**
     * Returns subtitle of message type (final type) to display on html page.
     * (Main title is http response code, for example)
     * Override in specific exception classes.
     * @return string
     */
    public function getSubtitle() {
        return '';
    }

    /**
     * Returns url of continue link for user, or an array containing the link caption and the url.
     *
     * @return string|array
     */
    public function getGoto() {
        return $this->goto;
    }

    public function getExtra() {
        return $this->extra;
    }
}
