<?php
/**
 * Class Session
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * a php session handler
 *
 * Descendants may store the session data in various other places, e.g. in datbase (currently not implemented in UApp)
 *
 * ###Session configuration example for `config.php`
 *
 * ```php
 * 'session' => array(
 *    'name' => 'sess_sample',
 *    'save_path' => $datapath . '/session',
 *    'lifetime' => 1800,
 *    'cookie_path' => '',
 *    'cookie_domain' => 'sample.hu',
 *    'logfile' => $datapath.'/session.log',
 * ),
 * ```
 */
class Session {
    /**
     * Session constructor.
     *
     * @param null $config
     *
     * @throws ConfigurationException
     * @throws InternalException
     */
    function __construct($config = null) {
        $sessionconfig = $config ? $config : UApp::config('session');
        #session_name($sessionconfig['name']);
        if (headers_sent($file, $line)) throw new InternalException("Cannot start session, headers already sent in $file:$line");
        if (isset($sessionconfig['save_path'])) {
            $sessionpath = $sessionconfig['save_path'];
            if (!file_exists($sessionpath)) mkdir($sessionpath, 0774);
            if (!file_exists($sessionpath)) throw new InternalException("Cannot create session directory", $sessionpath);
            session_save_path($sessionpath);
        }
        #session_cache_limiter('private_no_expire');
        ini_set("session.gc_maxlifetime", $sessionconfig['lifetime'] + 900);
        ini_set("session.lifetime", $sessionconfig['lifetime']);
        ini_set("session.gc_probability", "100");
        //ini_set("session.save_handler", 'user');
        $domain = $sessionconfig['cookie_domain'];
        if ($domain === true) $domain = parse_url(UApp::config('baseurl'), PHP_URL_HOST);
        session_set_cookie_params(0, $sessionconfig['cookie_path'], $domain);
        session_name('uapp_session');
        if (session_status() == PHP_SESSION_ACTIVE) throw new InternalException('Session is already active.');
        if (!@session_start()) throw new InternalException('Cannot start session: ' . Util::objtostr(error_get_last()));
    }

    /**
     * @throws ConfigurationException
     * @throws InternalException
     */
    function __destruct() {
        Debug::trace('Destructing session');
        #$this->finish();
    }

    /**
     * @throws ConfigurationException
     * @throws InternalException
     */
    function finish() {
        Debug::trace('Session is closing');
        if (session_status() == PHP_SESSION_ACTIVE) {
            session_write_close();
            unset($_SESSION);
        }
    }
}
