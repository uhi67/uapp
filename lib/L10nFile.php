<?php
/**
 * class L10nFile
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * L10n (Localization is 12 letter long)
 * File-based solution for L10n functions
 *
 * Uses php files in translation directory for translations
 * Uses default L10nBase for formatting dates.
 *
 * ### Configuration
 *
 * ```
 * 'l10n' => array(
 *        'class' => 'L10nFile', // or descendant
 *        'uappDir' => dirname(__DIR__).'/def/translations', // Place of UApp translation files. This is the default
 *        'dir' => $defpath.'/translations', // Place of app translation files. This is the default
 *        'language' => 'hu',        // Default language with optional locale, may be changed by UApp::setLang(lang/locale)
 *        'source' => 'hu',        // Default source language, default is 'en'
 * ),
 * ```
 *
 * ### Translation file format
 *
 * ```
 * return array(
 *        'original text' => 'eredeti szöveg',
 *        ...
 * );
 * ```
 */
class L10nFile extends L10nBase {
    /** @var string $dir -- directory of translation files in form 'la.php', default is def/traslations */
    public static $dir;

    /**
     * {@inheritdoc}
     * @throws InternalException
     * @throws Exception
     */
    public static function prepare() {
        parent::prepare();
        if (static::$dir) {
            if (substr(static::$dir, -1) != '/') static::$dir .= '/';
        } else {
            static::$dir = UApp::config('defpath', UApp::config('!apppath') . '/def') . '/translations/';
        }
        if (!Util::makeDir(static::$dir, 2)) throw new InternalException('Configuration error: directory does not exists for translation class.');
    }

    /**
     * Translates a system text from source language to given language.
     *
     * 'uapp' category is translated by parent class, all others from files in the configured directory.
     * If translation is not found, returns source*
     *
     * @param string $category -- message category, UApp uses 'uapp'. Application default is 'app'
     * @param string $source - source language text or text identifier
     * @param array $params - replaces $var parameters
     * @param string $lang - language code or user default language
     *
     * @return string
     */
    static function getText($category, $source, $params = NULL, $lang = null) {
        if ($lang === null) $lang = UApp::getLang();
        if ($category == 'uapp') return parent::getText($category, $source, $params, $lang);

        $text = static::getTextFile(static::$dir, $source, $lang);

        // substitute parameters
        if ($params) {
            if (!is_array($params)) $params = [$params];
            $text = Util::substitute($text, $params);
        }
        return $text;
    }
}
