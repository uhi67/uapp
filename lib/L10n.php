<?php
/**
 * class L10n
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * L10n (Localization is 12 letter long)
 * Basic solution for L10n functions
 *
 * Uses systext table for translations
 * (Apply UApp/sql/db_update_2017_1121_systext_create.sql to create table)
 * Uses default L10nBase for formatting dates.
 *
 * ### Configuration
 *
 * ```
 * 'l10n' => array(
 *        'class' => 'L10nFile', // or descendant
 *        'uappDir' => dirname(__DIR__).'/def/translations', // Place of UApp translation files. This is the default
 *        'textClass' => 'Systext',
 *        'db' => 'anydb',        // connection config name or config array. Default is 'db'
 *        'language' => 'hu',        // Default language with optional locale, may be changed by UApp::setLang(lang/locale)
 *        'source' => 'hu',        // Default source language, default is 'en'
 * ),
 * ```
 */
class L10n extends L10nBase {
    public static $textClass = 'Systext';
    public static $db;

    public static function prepare() {
        parent::prepare();

        if (static::$db) {
            if (is_array(static::$db)) static::$db = Component::create(static::$db);
            else if (is_string(static::$db)) {
                static::$db = Component::create('!' . UApp::config(static::$db));
            }
        }
    }

    /**
     * Translates a system text from source language to given language
     * 'uapp' category is translated by parent class, all others from `systext` table.
     * If translation is not found, creates a default translation as '*source*'
     * If source is not found, creates a default source record with default source language
     *
     * @param string $category -- message category, UApp uses 'uapp'. Application default is 'app'
     * @param string $source - source language text or text identifier
     * @param array $params - replaces $var parameters
     * @param string $lang - language code or user default language
     *
     * @return string
     * @throws ConfigurationException
     * @throws DatabaseException
     * @throws InternalException
     * @throws ModelException
     * @throws QueryException
     * @throws ReflectionException
     * @throws UAppException
     */
    static function getText($category, $source, $params = NULL, $lang = null) {
        if (!static::$db) static::$db = UApp::getInstance()->db;
        if ($lang === null) $lang = UApp::getLang();
        if ($category == 'uapp') return parent::getText($category, $source, $params, $lang);
        $la = substr($lang, 0, 2);

        /** @var Query $query */
        $query = Query::createSelect([
            'from' => static::$textClass,
            'fields' => ['value'],
            'joins' => ['s' => 'source1'],
            'condition' => [['=', 'lang', '$2'], ['or',
                [(is_numeric($source) ? 's.id' : 's.value') => '$1'],
                ['source' => null, (is_numeric($source) ? 'id' : 'value') => '$1']
            ]],
            'params' => [$source, $lang],
            'connection' => static::$db,
        ]);
        $text = $query->scalar();
        if ($text === null) {
            #throw new Exception("A szöveg nem található: $category, $source, $lang, $query->finalSQL");
            /** @var string $dsl default language */
            $dsl = UApp::config('l10n/source', 'en');
            $text = '*' . $source . '*';
            if (is_int($source)) {
                $systext_source = call_user_func([static::$textClass, 'first'], $source); // Systext::first($source);
                $text = '*' . $source . '*';
                if (!$systext_source) {
                    // We have no clue what the text actually should be.
                    /** @var Model $systext_source */
                    $systext_source = new static::$textClass(['id' => $source, 'lang' => $dsl, 'value' => $text]);
                    if (!$systext_source->save()) {
                        throw new InternalException('Error creating system text source item', $systext_source->errorValues);
                    }
                }
                /** @var Model $systext_translation */
                $systext_translation = new static::$textClass(['lang' => $la, 'value' => $text, 'source' => $systext_source->id]);
                if (!$systext_translation->save()) {
                    throw new InternalException('Error creating default translation', $systext_translation->errorValues);
                }
            } else {
                // Find default
                $systext_source = call_user_func([static::$textClass, 'first'], // $condition, $orders=null, $params=null, $connection=null, $nocache=false
                    ['value' => $source],
                    [[['=', 'lang', '$1'], 'desc']],
                    [$dsl],
                    static::$db
                ); // null lang, default lang, others
                // Create default record template
                if (!$systext_source) {
                    /** @var Model $systext_source */
                    $systext_source = new static::$textClass(['lang' => $dsl, 'value' => $source], static::$db); // $config=null, $connection=null
                    if (!$systext_source->save()) {
                        throw new InternalException('Error creating system text source item', $systext_source->errorValues);
                    }
                }
                if ($la != $dsl) {
                    // Create translation record template
                    /** @var Model $systext_translation */
                    $systext_translation = new static::$textClass(['lang' => $la, 'value' => $text, 'source' => $systext_source->id], static::$db);
                    if (!$systext_translation->save()) {
                        throw new InternalException('Error creating default translation', $systext_translation->errorValues);
                    }
                }
            }
        }

        // substitute parameters
        if ($params) {
            if (!is_array($params)) $params = [$params];
            $text = Util::substitute($text, $params);
        }
        return $text;
    }
}
