<?php
/**
 * class ConfigurationException
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * ConfigurationException represents an error in application's configuration
 */
class ConfigurationException extends ServerErrorException {

    public function getSubtitle() {
        return UApp::la('uapp', 'Application configuration error');
    }
}
