<?php
/**
 * class Model_template
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2018
 * @license MIT
 */

/** @noinspection PhpClassNamingConventionInspection */

/**
 * Model template for new database models
 *
 * @property int $field_1
 * Declare all table fields as properties
 * @codeCoverageIgnore
 */
class Model_template extends Model {
    // Constant values for unreferenced integer fields
    const CONST_0 = 0;

    /**
     * Returns database tablename of the model.
     * Optional, if tablename differs from classname
     * @return string
     */
    public static function tableName() {
        return parent::tableName();
    }

    /**
     * Must specify autoincrement field.
     * Optional, if differs from first primaryKey()
     * Currently only one fieldname is supported.
     * @return array
     */
    public static function autoIncrement() {
        return parent::autoIncrement();
    }

    /**
     * Must return an array of primary key fields
     * Optional, if differs from default array('id')
     *
     * @return array of fieldnames
     */
    public static function primaryKey() {
        return parent::primaryKey();
    }

    /**
     * Must return an associative array with foreign key symbolic names mapped to modelnames and field mappings
     * Default is empty array
     *
     * @return array of keyname => array(modelname, reference_field=>foreign_field)
     */
    public static function foreignKeys() {
        return [
        ];
    }

    /**
     * Returns fieldnames of default name of referenced models
     * @return array of foreign-key-name => fieldname pairs for all foreign keys.
     */
    public static function refNames() {
        return [
        ];
    }

    /**
     * Must return the attribute labels.
     * Attribute labels are mainly used for display purpose
     * Order of labels is the default order of fields.
     * @return array attribute labels (name => label)
     */
    public static function attributeLabels() {
        return [
        ];
    }

    /**
     * Must return comments associated with fields.
     * Every comment may be a simgle string or an array of followings:
     * - hint: for mouse hover
     * - comment: short comment displayed after the field value
     * - descr: medium long comment displayed in separate row(s), may be multiple
     * - help: long help displayed in popup after pressing [?]. Multiple strings are paragraphs in single popup.
     * - tooltip: auto-popup tutorial text when tutorial mode is on.
     * @return array
     */
    public static function attributeHints() {
        return [
        ];
    }

    /**
     * Returns validation rules for fields
     * Models are validated against rules before saving to database
     *
     *  - rule -- numeric indexed rules are global rule, e.g. unique for multiple fields
     *  - fieldname => array(rule, rule...) -- fieldname indexed rules are for single field
     *
     * Rule:
     *
     *    - rulename
     *  - array(rulename, arguments, ...)
     *
     * Rulename always refers to method `rulenameValidate` and will be added to the field class as `rule-rulename`
     * Arguments will be added to field as data-rulename-data (always an array, for all remaining arguments)
     * Associative arguments 'key'=>value will be used in php validator method as normal argument,
     * but in forms will be added as data-rulename-key and excluded from data-rulename.
     *
     * Global rules:
     *
     *    - 'unique' fieldnamelist
     *    - custom arglist -- any custom name refers to validateCustom(array $arglist) method
     *
     * Field rules:
     *
     *      - 'unique' -- field is unique without condition
     *    - 'not null' -- field is not null
     *    - 'mandatory' -- field is not null and not empty string
     *    - typenames (boolean, int, float, number, string, xml, time, date, datetime, )
     *      - ['length', min, max]
     *    - ['pattern', pattern(s)] -- valid if at least one of RE patterns is valid (second level: all of them)
     *      - ['type', typename]
     *    - ['between', lower, upper] -- valid value between (inluding) limits
     *    - [custom, arglist] -- any custom name refers to validateCustom($fieldname, array $arglist) method.
     *
     * `rulenameValidate` functions:
     *
     *    - must accept parameters (fieldname, arg1, arg2, ...) where fieldname is null on global calls.
     *  - first element of args is custom message or null if default should be used.
     *  - $1 is the place of the fieldname in the message
     *  - other elments of args are rule data, e.g. RegExp pattern for pattern rule.
     *  - must return boolean
     *  - may set error text of the invalid field on the form by setError(fieldname, message)
     *
     * @return array
     * @see validate()
     */
    public static function rules() {
        return [
        ];
    }

}
