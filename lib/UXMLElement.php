<?php
/**
 * class UXMLElement
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/** @noinspection PhpUnused */

/**
 * A DOM element in extended DOM functions
 *
 * See also {@see UXMLDoc}.
 *
 * ### Array representation of nodeList (see {@see addNodeList()})
 *
 * content:
 * - string
 * - array of nodes
 *
 * node:
 * - array(nodename, array of attributes, content)
 *
 * Example: [['alma', ['ize'=>'finom'], 'Gála'], ['körte', ['ize'=>'kásás'], [['fajta', ['name' => 'Boscobak']], ['text', null, 'szöveg']]
 *
 *
 * ### Array representation of subnode (see {@see addSubnode(), @see createNode()})
 *
 * content:
 *    - string (text node)
 *    - array with numeric key => text (text nodes 'text')
 *    - array element with _nodename key => content (subnodes with name 'nodename') !!! Warning: multiple subnodes with same name is not possible
 *    - array element name => scalar value (attribute with name 'name' and value)
 *
 * Example: ['_alma' => ['ize'=>'finom', 'Gála'], '_körte' => ['ize'=>'kásás', '_fajta' => ['name' => 'Boscobak']], '_text' => 'szöveg']
 *
 *
 * ### Array representation of subnode (see {@see createSubnode()})
 *
 * content:
 * - string
 * - array of subcontent
 *
 * subcontent:
 * - numeric => text
 * - subnodename => array of multiple contents. Creates multiple subnodes with same name.
 * - attributename => value
 *
 * Example: ['alma' => [['ize'=>'finom', 'Gála']], 'körte' => [['ize'=>'kásás', 'fajta' => [['name' => 'Boscobak']]], 'text' => [['szöveg']]]
 *
 *
 * @property UXMLDoc $ownerDocument
 * @property-read UXMLElement $firstChild
 * @property-read string $xml -- outer xml of the node
 * @property-read array $allAttributes -- All attributes of the Element as an array
 * @method UXMLElement appendChild() appendChild(DOMNode $node1)
 * @method UXMLNodeList getElementsByTagname() getElementsByTagname(string $name)
 */
class UXMLElement extends DOMElement {

    /**
     * Beszúrja a node alá a másik dokumentum gyökér alatti elemeit.
     *
     * @param DOMDocument $xmldoc -- beszúrandó dokumentum
     *
     * @return UXMLElement az eredeti node
     * @deprecated, use {@see addXmlContent()}
     */
    public function addDoc($xmldoc) {
        // Beszúrandó node-ok iterálása
        $node2 = $xmldoc->documentElement;
        $nodex = $node2->firstChild;
        while ($nodex) {
            $this->appendChild($this->ownerDocument->importNode($nodex, true));
            $nodex = $nodex->nextSibling;
        }
        return $this;
    }

    /**
     * Adds new node with attributes and content to parent node.
     *
     * ### Attributes is universal content data (see {@see addContent()})
     * - Literal data creates text content in the node.
     * - string => literal array elements: attribute values. Null attribute values will not create the attribute.
     * - string => array elements: creates subnodes (array of universal content data items)
     * - numeric index -> scalar: text content (DOM: subtree)
     * - numeric index -> array: Subnodes [nodename, contentdata]
     *
     * ### Content is universal content data again :-) This behavior is compatible with the former definition.
     *
     * ### Namespace
     * - may be null or empty string - no namespace will be added
     * - array - the namespace with prefix as index will be used
     *     - if prefix not found in indices and namespace has element at 1-index, it will be used
     *       - or if no prefix, and namespace has element at 0-index, it will be used
     *     - otherwise no namespace will be added
     *
     * @param string|array $name -- nodename of created node or array(name, namespace) @see addNodes
     * @param array $attributes -- associative array
     * @param string|array $content -- text content or contents for multiple text nodes
     * @param string|array $namespace namespace of node or namespace array to associate with current prefix
     *
     * @return UXMLElement -- the inserted node
     * @throws DOMException
     */
    public function addNode($name, $attributes = null, $content = null, $namespace = null) {
        /** @var string $ns1 -- namespace of the node */
        $ns1 = null;
        if (is_array($name)) {
            $ns1 = $name[1];
            $name = $name[0];
        }
        $prefix = Util::substring_before($name, ':');
        if (is_string($namespace)) {
            if ($ns1) {
                $namespace = [$namespace];
            } else {
                $ns1 = $namespace;
                if ($prefix) $namespace = [$prefix => $ns1];
                else $namespace = null;
            }
        }
        if (is_array($namespace)) {
            if ($ns1) {
                if ($prefix) $namespace[$prefix] = $ns1;
                else {
                    if (isset($namespace[0])) $namespace[1] = $namespace[0];
                    $namespace[0] = $ns1;
                }
            } else {
                if ($prefix && isset($namespace[$prefix])) $ns1 = $namespace[$prefix];
                else if ($prefix && isset($namespace[1])) $ns1 = $namespace[1];
                else if (!$prefix && isset($namespace[0])) $ns1 = $namespace[0];
                else $ns1 = null;
            }
        }
        $node1 = $ns1 ?
            $this->ownerDocument->createElementNS($ns1, Util::toNameID($name, '_', '_.-:')) :
            $this->ownerDocument->createElement(Util::toNameID($name, '_', '_.-'));
        $node1 = $this->appendChild($node1);

        $node1->addContent($attributes, $namespace);
        $node1->addContent($content, $namespace);
        return $node1;
    }

    /**
     * ## Adds an attribute to the node
     *
     * Handles special value types as:
     * - DateTime
     * - Inet
     * - MacAddress
     * - Array: creates multiple text nodes instead of attributes
     *
     * Skips attribute names containing invalid characters
     *
     * If value is an array, subnodes will be created with content as value element
     *
     * Namespace will be added to prefixed attribute names.
     *
     * @param string $name -- optionally prefixed name
     * @param mixed $value -- array value to generate subnodes
     * @param mixed $namespace -- single namespace or prefix-indexed array of namespaces
     *
     * @return DOMAttr|UXMLElement|null -- the first inserted subnode, the inserted attribute node, or null if invalid name
     * @throws ConfigurationException
     * @throws InternalException
     * @throws Exception
     */
    public function addAttribute($name, $value, $namespace = null) {
        if ($value === null) return null; // skip null attributes
        $doc = $this->ownerDocument;
        if (!is_array($value) && !is_string($value)) $value = Util::toString($value, is_bool($value) ? null : $doc->locale);
        $prefix = Util::substring_before($name, ':');

        // Prepare namespace
        if ($prefix != '') {
            if (is_array($namespace) && isset($namespace[$prefix]))
                $this->addNamespaceDeclaration($prefix, $namespace[$prefix]);
            if (is_string($namespace) && $namespace != '')
                $this->addNamespaceDeclaration($prefix, $namespace);
        }

        if (is_array($value)) {
            // Insert subnode
            $n1 = null;
            foreach ($value as $k => $v) if ($k !== 'count') {
                if ($v instanceof Component) {
                    $v = $v->toArray();
                }
                if (is_array($v)) {
                    $n = $this->addNode($name, $v);
                } else $n = $this->addTextNode($name, $v);
                if (!$n1) $n1 = $n;
            }
            return $n1;
        } else {
            $name = Util::toNameID($name, '-', '_:.-');
            if (preg_match('/^[_a-zA-Z][:a-zA-Z0-9_.-]*$/', $name)) {
                $locale = ($doc instanceof UXMLDoc) ? $doc->locale : 'hu';
                $value = Util::toString($value, $locale);
                if (@$this->setAttribute($name, $value) === false) {
                    if (Debug::$trace) throw new Exception("setAttribute failed. Name: `$name`, Value: `$value`");
                }
                return $this->getAttributeNode($name);
            }
        }
        return null;
    }

    /**
     * ## Adds attributes to the node
     * Handles special value types
     *
     * @param array $values -- name-value pairs
     *
     * @return UXMLElement -- self
     * @throws ConfigurationException
     * @throws InternalException
     * @see addAttribute
     *
     */
    public function addAttributes($values) {
        foreach ($values as $name => $value) $this->addAttribute($name, $value);
        return $this;
    }


    /**
     * Creates a list of similar nodes from each item of values array
     *
     * @param string|array $name -- name of created node or array(name, namespace) @see addNodes
     * @param array $values -- array of universal content data ({@see addContent()})*
     * @param string|array $namespace -- (!!!options removed)
     *
     * @return UXMLElement -- the parent node itself (!!!changed)
     *
     * TODO: check all usages, interface changed.
     * @throws DOMException
     */
    public function addNodes($name, $values, $namespace = null) {
        foreach ($values as $contentData) $this->addNode($name, $contentData, null, $namespace);
        return $this;
    }

    /**
     * Creates a subnode with given content.
     *
     * values is an array of the content or a text node:
     *        - numeric key => text node
     *        - _nodename => subnode content
     *        - attribute name => value
     *
     * @param string $tagname
     * @param string|array $values -- content, attribute and subnode values
     *
     * @return UXMLElement
     * @throws ConfigurationException
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     *
     * @deprecated use {@see addNode}($tagname, $values)
     * @codeCoverageIgnore
     */
    public function addSubNode($tagname, $values) {
        if (is_array($values)) {
            $content = [];
            $subnodes = [];
            foreach ($values as $k => $v) {
                if (is_int($k)) {
                    $content[] = $v;
                    unset($values[$k]);
                }
                if (substr($k, 0, 1) == '_') {
                    $subnodes[substr($k, 1)] = $v;
                    unset($values[$k]);
                }
            }
            $node = $this->addNode($tagname, $values, $content);
            foreach ($subnodes as $sk => $sv) /** @noinspection PhpDeprecationInspection */
                $node->addSubNode($sk, $sv);
        } else $node = $this->addNode($tagname, null, $values);

        return $node;
    }

    /**
     * ## Adds new node(s) based on node-path expression
     *
     * - Creates all parent nodes specified in the path if not exist
     * - Null attribute values will not create attributes.
     * - Array attribute values will generate series of subnodes with array element values as text value
     * - The generated node may not conform to the path specification if the attributes differ from path condition.
     *
     * @param string $name -- node-path (xpath expression) 'nodename[cond]/nodename[cond]/...nodename[cond]' format
     * @param array $attributes -- array of associative arrays for all levels
     * @param array $content -- array of text contents for all levels
     * @param array $namespaces -- optional array of prefix=>namespaceurl items
     *
     * @return UXMLElement -- the found or inserted node (last level)
     * @throws InternalException
     * @throws UAppException
     * @throws DOMException
     */
    public function addNodePathIfNotExists($name, $attributes = null, $content = null, $namespaces = null) {
        $node = $this->selectSingleNode($name, $namespaces);
        if (!$node) {
            $nodes = explode('/', $name);
            $parent = $this;
            foreach ($nodes as $i => $nodespec) {
                $node = $parent->selectSingleNode($nodespec, $namespaces);
                $nodename = Util::substring_before($nodespec, '[', true);

                if (!$node) {
                    $prefix = Util::substring_before($nodename, ':');
                    if ($prefix) {
                        $namespace = $namespaces[$prefix];
                        $node = $parent->addNode($nodename, ArrayUtils::getValue($attributes, $i), ArrayUtils::getValue($content, $i), $namespace);
                    } else {
                        $node = $parent->addNode($nodename, ArrayUtils::getValue($attributes, $i), ArrayUtils::getValue($content, $i));
                    }
                }

                $parent = $node;
            }
        }
        return $node;
    }

    /**
     * A megadott node alá generál egy-egy node-ot az sql eredménysoraiból
     * a mezőnevekből attributumok lesznek.
     * null érték esetén az attributum nem jön létre.
     * _ kezdetű mezőnevekből node-ok lesznek szöveges tartalommal
     * _ végű mezőnevekből eltávolítjuk a tag-eket
     * _content mezőnévből hypertext tartalom vagy szöveges node lesz
     *
     * @param DBX $db
     * @param string $name -- nodename for new nodes
     * @param string|Query $sql -- sql with $1 parameters
     * @param array $params -- sql parameters
     * @param bool $valuenodes -- generate subnodes for all field values
     *
     * @return UXMLELement|null the first inserted node
     * @throws InternalException
     * @throws ConfigurationException
     * @throws DOMException
     */
    public function query($db, $name, $sql, $params = [], $valuenodes = false) {
        if ($sql instanceof Query) {
            $query = $sql;
            if (!$query->connection) $query->connection = $db;
            if (!$query->connection) throw new InternalException('Database not defined.');
            if (!$query->type) $query->type = $query->from ? 'select' : 'sql';
            $query->bind($params);
            $result = $query->all();
            $firstchild = null;
            foreach ($result as $row) {
                /** @var UXMLElement $child */
                $child = $this->ownerDocument->createElement($name);
                if (!$firstchild) $firstchild = $child;
                if ($valuenodes) {
                    foreach ($row as $column => $value) {
                        $node_value = $child->addHypertextNode('value', $value);
                        $node_value->setAttribute('name', $column);
                    }
                } else $child->addAttributes($row);
                $this->appendChild($child);
            }
            return $firstchild;
        }
        if (!$db) throw new InternalException('Database not defined.');
        try {
            $r = $db->query($sql, $params);
            $n = $db->num_rows($r);
        } catch (Exception $e) {
            throw new InternalException($e->getMessage() . "\n SQL: " . $sql, 500, $e);
        }
        $firstchild = null;
        for ($i = 0; $i < $n; $i++) {
            /** @var UXMLElement $child */
            $child = $this->ownerDocument->createElement($name);
            if (!$firstchild) $firstchild = $child;
            $child->addFields($db, $r, $i, $valuenodes);
            $this->appendChild($child);
        }
        #tracex('UXMLDoc::query', array($sql, $params, $n));
        return $firstchild;
    }

    /**
     * Add attributes to a node from result set line
     *
     * @param DBX $db
     * @param resource $rs
     * @param int $i -- number of result line
     * @param bool $valuenodes -- create subnodes
     * @return UXMLElement
     * null érték esetén az attributum nem jön létre.
     * _ kezdetű mezőnevekből node-ok lesznek szöveges tartalommal
     * _ végű mezőnevekből eltávolítjuk a tag-eket
     * _content mezőnévből hypertext tartalom vagy szöveges node lesz
     * @throws DatabaseException
     * @throws InternalException
     * @throws DOMException
     */
    public function addFields($db, $rs, $i, $valuenodes = false) {
        $m = $db->fields($rs);
        for ($j = 0; $j < $m; $j++) {
            $column = $db->fieldname($rs, $j);
            $value = $db->result($rs, $i, $j);
            if ($value === null) continue;
            if (substr($column, -1) == '_') {
                $value = strip_tags($value);
                $column = substr($column, 0, -1);
            }
            if ($valuenodes) {
                $node_value = $this->addHypertextNode('value', $value);
                $node_value->setAttribute('name', $column);
            } else if (substr($column, 0, 1) == '_') {
                if ($column == '_content') $this->addHypertextContent($value);
                else $this->addHypertextNode(substr($column, 1), $value);
            } else $this->setAttribute($column, $value);
        }
        return $this;
    }

    /**
     * UXMLDoc::queryTree()
     *
     * @param DBX $db
     * @param string $name
     * @param string $sql
     * @param array $params
     * @param array $subqueries - array(array(name, sql, subquery), ...) - sql may contain @references to upper level attributes
     *
     * @return UXMLElement first child at top level
     * @throws DatabaseException
     * @throws InternalException
     * @throws DOMException
     * @deprecated
     * @codeCoverageIgnore
     */
    public function queryTree($db, $name, $sql, $params = [], $subqueries = null) {
        #tracex('queryTree', params($sql, $params));
        $r = $db->query($sql, $params);
        $n = $db->num_rows($r);
        /** @var DOMElement $firstchild */
        $firstchild = null;
        for ($i = 0; $i < $n; $i++) {
            $child = $this->ownerDocument->createElement($name);
            if (!$firstchild) $firstchild = $child;
            $child->addFields($db, $r, $i);
            $this->appendChild($child);
            if (is_array($subqueries)) {
                for ($s = 0; $s < count($subqueries); $s++) {
                    $sub = $subqueries[$s];
                    if (is_array($sub)) {
                        $name2 = $sub[0];
                        $sql2 = $sub[1];
                        // behelyettesítés
                        $mc = preg_match_all("/@\\w+/", $sql2, $ma);
                        for ($j = 0; $j < $mc; $j++) {
                            $namex = $ma[0][$j];
                            $valuex = $child->getAttribute(substr($namex, 1));
                            #tracex('querytree replace:', array($namex, $valuex));
                            $sql2 = str_replace($namex, $valuex, $sql2);
                        }
                        $sub2 = isset($sub[2]) ? $sub[2] : null;
                        /** @noinspection PhpDeprecationInspection */
                        $child->queryTree($db, $name2, $sql2, $params, $sub2);
                    }
                }
            }
        }
        return $firstchild;
    }

    /**
     * Creates and inserts a new text content
     *
     * @param string $cont
     * @return UXMLElement|DOMNode -- the inserted text node
     *
     * @throws InternalException
     */
    public function addText($cont) {
        Util::assertString($cont);
        /** @noinspection */
        return $this->appendChild($this->ownerDocument->createTextNode($cont));
    }

    /**
     * Creates or replaces a text content in the node
     * (Deletes all redundant text nodes)
     *
     * @param string $cont
     * @return DOMNode|UXMLElement -- the original node
     */
    public function replaceText($cont) {
        $this->nodeValue = $cont;
        return $this;
    }

    /**
     * Creates a new node with name and plain text content
     *
     * @param string $name -- the tagname of the new node
     * @param string $cont -- the text content of the new node
     * @param string|array $namespace -- simple namespace or namespace array
     *
     * @return UXMLElement|DOMNode -- the new node created (Always DOMElement)
     * @throws InternalException
     * @throws ConfigurationException
     * @throws DOMException
     */
    public function addTextNode($name, $cont, $namespace = null) {
        $prefix = Util::substring_before($name, ':');
        if (!is_string($cont)) $cont = Util::toString($cont, $this->ownerDocument->locale);
        if ($prefix != '' && (is_string($namespace) && $namespace != '' || is_array($namespace) && isset($namespace[$prefix]))) {
            $ns = is_array($namespace) ? $namespace[$prefix] : $namespace;
            return $this->appendChild($this->ownerDocument->createElementNS($ns, $name, $cont));
        }
        return @$this->appendChild($this->ownerDocument->createElement($name, $cont));
    }

    /**
     * Creates a new node with given XML content from string
     *
     * @param string $name -- the tagname of the new node
     * @param string $cont -- the XML content in a string
     *
     * @return UXMLElement -- the new node created
     * @throws InternalException
     * @throws DOMException
     */
    public function addHypertextNode($name, $cont) {
        $node1 = $this->ownerDocument->createElement($name);
        $node1 = $this->appendChild($node1);
        $node1->addHypertextContent($cont);
        return $node1;
    }

    /**
     * Adds XML content from a string
     * If string is not valid XML, a text node will be created
     * If string starts with <?xml, a complete XML document is assumed, and root node may be omitted
     * Otherwise the string is decoded as xml fragment, and root node cannot be omitted.
     *
     * @param string $cont - string xml content to insert. May be xml document (starting with <?xml) or xml fragment (starting with '<')
     * @param bool $strict
     * @param bool $root -- include root node of xml (not used if xml fragment is provided)
     * @param string $namespace -- insert/replace default namespace. Does not affect declared prefixed namespaces. Default is none, leaves declared
     *
     * @return DOMElement|DOMNode - (first) inserted node (may be text node)
     * @throws InternalException
     */
    function addHypertextContent($cont, $strict = false, $root = false, $namespace = null) {
        if ($cont instanceof DOMElement) {
            return $this->addXmlContent($cont, !$root);
        }
        if (is_null($cont)) return null;
        if (is_numeric($cont)) $cont = (string)$cont;
        if (!is_string($cont)) throw new InternalException('String expected, ' . (gettype($cont) == 'object' ? get_class($cont) : gettype($cont)));
        $dom2 = new DOMDocument();
        try {
            $contt = trim($cont);
            if (substr($contt, 0, 6) == '<?xml ' and substr($contt, -1) == '>') {
                $dom2->loadXML($contt);
                if ($root) {
                    return $this->appendChild($this->ownerDocument->importNode($dom2->documentElement, true));
                } else {
                    $nodex = $dom2->documentElement->firstChild;
                    /** @var DOMElement $first */
                    $first = null;
                    while ($nodex) {
                        $inserted = $this->appendChild($this->ownerDocument->importNode($nodex, true));
                        if (!$first) $first = $inserted;
                        $nodex = $nodex->nextSibling;
                    }
                    return $first;
                }
            } else if (substr($contt, 0, 1) == '<' and substr($contt, -1) == '>') {
                $xmlns = $namespace ? ' xmlns="' . $namespace . '"' : '';
                $r = @$dom2->loadXML("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><data$xmlns>$contt</data>");
                if (!$r) @$r = $dom2->loadXML("<?xml version=\"1.0\" encoding=\"ISO-8859-2\" ?><data$xmlns>$contt</data>");
                if (!$r) {
                    #throw new Exception('Invalid XML content '.$contt);
                    if ($strict) throw new UAppException('Invalid XML content');
                    return $this->addText($cont . ' (*)');
                }
                $nodex = $dom2->documentElement->firstChild;
                /** @var DOMElement $first */
                $first = null;
                while ($nodex) {
                    $inserted = $this->appendChild($this->ownerDocument->importNode($nodex, true));
                    if (!$first) $first = $inserted;
                    $nodex = $nodex->nextSibling;
                }
                return $first;
            } else {
                return $this->addText($cont);
            }
        } catch (Exception $e) {
            if ($strict) throw new InternalException('Invalid XML content', null, $e);
            return $this->addText($cont . ' (*)');
        }
    }

    /**
     * Adds XML subtree from DOMElement or DOMDocument or xml string
     *
     * XML document or xml fragment is supported.
     *
     * @param string|DOMElement $cont - xml content to insert
     * @param boolean $omitroot - omits root node of xml document
     * @return UXMLElement|DOMNode - the (first) inserted node.
     * @throws InternalException on invalid XML string
     */
    function addXmlContent($cont, $omitroot = false) {
        if ($cont instanceof DOMDocument) {
            if ($omitroot) {
                /** @var DOMElement $first */
                $first = null;
                foreach ($cont->documentElement->childNodes as $child) {
                    $inserted = $this->appendChild($this->ownerDocument->importNode($child, true));
                    if (!$first) $first = $inserted;
                }
                return $first;
            }
            return $this->appendChild($this->ownerDocument->importNode($cont->documentElement, true));
        }
        if ($cont instanceof DOMElement or $cont instanceof DOMDocumentFragment) {
            if ($omitroot or $cont instanceof DOMDocumentFragment) {
                /** @var DOMElement $first */
                $first = null;
                foreach ($cont->childNodes as $child) {
                    $inserted = $this->appendChild($this->ownerDocument->importNode($child, true));
                    if (!$first) $first = $inserted;
                }
                return $first;
            }
            return $this->appendChild($this->ownerDocument->importNode($cont, true));
        }
        if (is_string($cont)) {
            return $this->addHypertextContent($cont, true, !$omitroot);
        }
        throw new InternalException('DOMElement input expected. For xml string inputs use addHypertextContent method.');
    }

    /**
     * Megkeresei egy node alfájában az xpath által megadott node-ot.
     *
     * @param string $query -- Az xpath kifejezésnek node-ra kell hivatkoznia!
     * @param array $namespaces -- array of alias=>url items
     *
     * @return UXMLElement|DOMElement -- Több node találat esetén az első node referenciájával tér vissza. Ha nincs meg, null reference (remélem)
     * @throws InternalException
     */
    public function selectSingleNode($query, $namespaces = null) {
        $xpath = new DOMXPath($this->ownerDocument);
        if (is_array($namespaces)) foreach ($namespaces as $alias => $namespace) $xpath->registerNamespace($alias, $namespace);
        $result = $xpath->query($query, $this);
        if (!$result) {
            throw new InternalException('Érvénytelen xpath kifejezés: ' . $query, $namespaces);
        }
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $result->item(0);
    }

    /**
     * Megkeresei egy node alfájában az xpath által megadott node-okat.
     *
     * @param string $query -- Az xpath kifejezésnek node-ra kell hivatkoznia!
     * @param array $namespaces -- array of alias=>url items
     *
     * @return DOMNodeList
     */
    function selectNodes($query, $namespaces = null) {
        $xpath = new DOMXPath($this->ownerDocument);
        if (is_array($namespaces)) foreach ($namespaces as $alias => $namespace) $xpath->registerNamespace($alias, $namespace);
        return $xpath->query($query, $this);
    }

    /**
     * Kiértékel egy xpath által megadott kifejezést
     *
     * @param string $query -- Az xpath kifejezésnek node-ra kell hivatkoznia!
     * @param array $namespaces -- array of alias=>url items
     *
     * @return mixed
     * @throws InternalException
     */
    function evaluate($query, $namespaces = null) {
        $xpath = new DOMXPath($this->ownerDocument);
        if (is_array($namespaces)) foreach ($namespaces as $alias => $namespace) $xpath->registerNamespace($alias, $namespace);
        $result = $xpath->evaluate($query, $this);
        if ($result === false) throw new InternalException('Invalid xpath expression ' . $query);
        return $result;
    }

    /**
     * Adds one node and a subtree to the node defined in array
     * If content is not valid xml, it will be inserted as simple string content
     *
     * @param string $name -- name of the new node
     * @param string $cont -- XML content or non-xml content
     *
     * @return UXMLElement -- the original node
     * @throws InternalException
     * @throws DOMException
     */
    public function addSubtree($name, $cont) {
        /** @var UXMLDoc $document */
        $document = $this->ownerDocument;
        $node1 = $document->createElement($name);
        $this->appendChild($node1);
        $f = $document->createDocumentFragment();
        $r = $f->appendXML($cont);
        if (!$r) {
            #trace("Parsing error at uxml_add_subtree");
            $node1->addText($cont);
        } else {
            $node1->appendChild($f);
        }
        return $this;
    }

    /**
     * Adds text content or subnodes to a node, recursively.
     * Recursive
     *
     * @param array|string $node_list
     * - string: text content
     * - array: array of subnodes as [text or [nodename, attributes, subnodelist],...]
     *
     * @return UXMLElement első hozzáadott node
     * @throws
     *
     * @deprecated use {@see addNode()}
     */
    public function addNodeList($node_list) {
        $firstnode = null;
        for ($i = 0; $i < count($node_list); $i++) {
            if (is_array($node_list[$i])) {
                $node_array = $node_list[$i];
                if (!isset($node_array[1])) $node_array[1] = null;
                if (!isset($node_array[0])) throw new Exception('Node array expected, got ' . Util::objtostr($node_array, false, true));
                $node1 = $this->addNode($node_array[0], $node_array[1]);
                if (isset($node_array[2])) {
                    if (is_array($node_array[2])) /** @noinspection PhpDeprecationInspection */
                        $node1->addNodeList($node_array[2]);
                    elseif (is_string($node_array[2])) $node1->addText($node_array[2]);
                }
            } else $node1 = $this->addText($node_list[$i]);
            if (!$firstnode) $firstnode = $node1;
        }
        return $firstnode;
    }

    /**
     * Returns the first named subnode or creates if not exists
     *
     * @param string $nodename
     *
     * @return UXMLElement first node with this name if exists or created node if not
     * @throws DOMException
     */
    public function getOrCreateElement($nodename) {
        $document = $this->ownerDocument;
        $nodelist = $this->getElementsByTagname($nodename);
        if ($nodelist->length) {
            $node = $nodelist->item(0);
        } else {
            /** @var UXMLElement $node */
            $node = $this->appendChild($document->createElement($nodename));
        }
        return $node;
    }

    /**
     * Sets attributes for bit values of the value by a given bitmap definition
     *
     * @param array $bitarray [[value, name, descr, const],...] eg. bitmap definition
     * @param int $value -- the value to represent by bit-attributes
     * @param int $field index of name column in bitarray
     * @param string $prefix -- attribute name prefix to attached
     *
     * @return UXMLElement -- the node itself
     * @throws InternalException
     */
    public function setNodeBits($bitarray, $value, $field = 3, $prefix = '') {
        if (!is_array($bitarray)) throw new InternalException('Invalid bitarray');
        foreach ($bitarray as $rb) {
            $bv = (int)$rb[0];
            $vb = ((int)$value & $bv) > 0 ? '1' : '0';
            $this->setAttribute($prefix . $rb[$field], $vb);
        }
        return $this;
    }

    /**
     * ## Sets attributes for bit values on all child nodes.
     * Skips nodes where valueattr is not found.
     *
     * @param array $bitarray [[value, name, descr, const],...] eg. bitmap definition
     * @param string $valueattr -- the attribute holding value to represent by bit-attributes
     * @param int $field index of name column in bitarray
     *
     * @return UXMLElement -- the parent node itself
     * @throws InternalException
     * @see setNodeBits
     *
     */
    public function setChildNodeBits($bitarray, $valueattr, $field = 3) {
        /** @var UXMLElement $node */
        $node = $this->firstChild;
        while ($node) {
            $value = $node->getAttribute($valueattr);
            if (is_numeric($value)) $node->setNodeBits($bitarray, $value, $field);
            $node = $node->nextSibling;
        }
        return $this;
    }

    /**
     * Creates nodes for all bits
     *
     * @param array $bitarray [bit=>[value, name, descr, const],...]
     * @param string $nodename
     * @param int $json
     *
     * @return UXMLElement -- the node
     * @throws ConfigurationException
     * @throws InternalException
     * @throws UAppException
     * @throws DOMException
     */
    public function loadBitNodes($bitarray, $nodename, $json = 0) {
        if (!is_array($bitarray)) Debug::trace('Invalid bitarray', '', 2);
        foreach ($bitarray as $bit => $rb) {
            $value = $rb[0];
            //$bit = strlen(decbin($value))-1;
            /** @var UXMLDoc $doc */
            $doc = $this->ownerDocument;
            Util::assertClass($doc, 'UXMLDoc', false);
            $this->addNode($nodename, [
                'bit' => $bit,
                'value' => $value,
                'name' => $rb[1],
                'descr' => $rb[2],
                'const' => $rb[3],
            ]);
            if ($json) $this->setAttribute('json', json_encode($bitarray));

        }
        return $this;
    }

    /**
     * Add attributes to a node from SQL
     *
     * @param DBX $db
     * @param string $sql
     * @param array $params
     *
     * @return UXMLElement
     * @throws DatabaseException
     * @throws InternalException
     * @throws DOMException
     */
    public function setQueryAttributes($db, $sql, $params = null) {
        $rs = $db->query($sql, $params);
        if ($rs && $db->num_rows($rs) > 0) $this->addFields($db, $rs, 0);
        return $this;
    }

    /**
     * Removes all children of the node
     *
     * @return UXMLElement -- the original node
     */
    public function removeChildren() {
        $cha = $this->childNodes;
        $chc = $cha->length;
        #tracex('removeChildren: length', $chc);
        for ($i = $chc - 1; $i >= 0; $i--) $this->removeChild($cha->item($i));
        return $this;
    }

    /**
     * @param string $alias
     * @param string $namespace
     *
     * @return UXMLElement
     * @throws DOMException
     */
    function addNamespaceDeclaration($alias, $namespace) {
        $dom = $this->ownerDocument;
        return $this->appendChild($dom->createAttribute('xmlns:' . $alias))->appendChild($dom->createTextNode($namespace));
    }

    /**
     * Dumps subtree of node to XML string without the referred node itself
     *
     * @return string
     */
    function saveContent() {
        $result = '';
        $node_x = $this->firstChild;
        while ($node_x) {
            $result .= $this->ownerDocument->saveXML($node_x);
            $node_x = $node_x->nextSibling;
        }
        return $result;
    }

    /**
     * # UXMLElement::createNode()
     *
     * Creates a single XML node from a model or attribute array under current node (analogous to Xlet objects' createNode)
     *
     * ### Options:
     * - nodeName: the name of created node, default is 'item'
     * - attributes: the attributes to generate, default is null = all, or associative array to map attributes (alias=>attribute)
     * - contentField: name of attribute to include in node content (default none) (content field is excluded from attributes)
     * - contentValue: value of content, effective only if contentField is not given or has no value.
     * - subnodeFields: name of attributes to include in subnodes instead of attributes (or alias=>fieldname pairs). DOM values result nativ xml values
     *        Instead of fieldname you may specify:
     *        - callable($index, $model); callable may generate scalar or array, scalar may be an xml (DOMElement)
     *        - array:  multiple literal node values (recursive, {@see createSubnode()})
     * - extra: extra attributes array(name=>function(index, item),...)
     * - overwrite: true = overwrite/complement existing node
     *
     * Explicit declared and same-name associations are recursive.
     *
     * @param array|BaseModel $value -- node data or models
     * @param array $options
     * @param string|array|null $namespace
     *
     * @return UXMLElement -- the inserted node
     *
     * @throws InternalException
     * @throws UAppException
     * @throws DOMException
     * @see Model::createNode()    for model options
     */
    function createNode($value, $options, $namespace = null) {
        return $this->createNodes([$value], $options, $namespace);
    }

    /**
     * # UXMLElement::createNodes()
     *
     * Creates multiple XML nodes from an array under current node (analogous to Xlet objects' createNode)
     *
     * ### Options:
     * - nodeName: the name of created node, default is 'item'
     * - attributes: the attributes to generate, default is null = all, or associative array to map attributes (alias=>attribute)
     * - contentField: name of original attribute to include in node content (default none) (content field is excluded from attributes)
     * - contentValue: value of content, effective only if contentField is not given or has no value.
     * - subnodeFields: name of attributes to include in subnodes instead of attributes (or alias=>fieldname pairs). DOM values result nativ xml values
     *        Instead of fieldname you may specify:
     *        - callable($index, $values); callable may generate universal node data (see {@see addContent})
     *        - array:  multiple literal node values (see {@see addContent}) named alias
     * - extra: extra attributes array(name=>function(index, item),...) resulting attribute value
     *
     * Explicit declared and same-name associations are recursive.
     *
     * @param array[]|BaseModel[] $data -- array of node data arrays or models
     * @param array $options
     * @param string|array|null $namespace
     *
     * @return UXMLElement -- the first inserted node
     *
     * @throws InternalException
     * @throws UAppException
     * @throws DOMException
     * @see Model::createNode()    for model options
     * TODO: check uses, behavior changed!
     */
    function createNodes($data, $options, $namespace = null) {
        Util::assertArray($data);

        $nodename = ArrayUtils::getValue($options, 'nodeName', 'item');
        /** @var array $attributes -- finally it will be alias=>fieldname map */
        $attributes = ArrayUtils::getValue($options, 'attributes');
        $contentField = ArrayUtils::getValue($options, 'contentField');
        $defaultContentValue = ArrayUtils::getValue($options, 'contentValue', null);
        $subnodeFields = ArrayUtils::getValue($options, 'subnodeFields', []);
        $extra = ArrayUtils::getValue($options, 'extra', []);
        $contentValue = $defaultContentValue;
        $defaultAttributes = isset($data[0]) && is_array($data[0]) ? array_keys($data[0]) : [];

        if ($contentField !== null) {
            $ax = array_search($contentField, $defaultAttributes);
            if ($ax !== false) unset($defaultAttributes[$ax]);
        }

        if ($attributes === null) $attributes = $defaultAttributes;

        // Replaces integer keys to itself in attributes map.
        foreach ($attributes as $n => $f) {
            if (is_int($n)) {
                if (!is_int($f)) $attributes[$f] = $f;
                unset($attributes[$n]);
            }
        }

        foreach ($subnodeFields as $snf) {
            if (is_string($snf)) unset($attributes[array_search($snf, $attributes)]);
        }

        $firstNode = null;
        foreach ($data as $index => $values) {
            /** @var array $attributevalues -- associative array of attributename=>attributevalue pairs */
            $attributevalues = array_combine(
                array_keys($attributes),
                array_map(function ($key) use ($values) {
                    return ArrayUtils::getValue($values, $key);
                }, array_values($attributes))
            );

            if ($contentField !== null) {
                $contentValue = ArrayUtils::getValue($values, $contentField);
                if ($contentValue === null) $contentValue = $defaultContentValue;
            }

            // Creating subnodes
            foreach ($subnodeFields as $key => $snf) {
                $tagname = is_int($key) ? (is_string($snf) ? $snf : 'item_' . $key) : $key;
                if (is_callable($snf)) {
                    $attributevalues[$tagname] = call_user_func($snf, $index, $values);
                } else if (is_string($snf)) {
                    $attributevalues[$tagname] = [ArrayUtils::getValue($values, $snf)];
                } else if (is_array($snf)) {
                    $attributevalues[$tagname] = $snf;
                } else {
                    $attributevalues[$tagname] = [$snf];
                }
            }

            // extra (computed) attributes (array valus generate subnodes!)
            foreach ($extra as $key => $fn) {
                $name = is_int($key) ? (is_string($fn) ? $fn : 'item_' . $key) : $key;
                if (is_callable($fn)) {
                    $attributevalues[$name] = call_user_func($fn, $index, $values);
                } else {
                    $attributevalues[$name] = $fn;
                }
            }

            $node = $this->addNode($nodename, $attributevalues, $contentValue, $namespace);
            if (!$firstNode) $firstNode = $node;
        }
        return $firstNode;
    }

    /**
     * Generates a subnode of the node using value array, recursively
     *
     * @param string $tagname -- nodename
     * @param string|array|null $value -- data of the subnode
     *          - null makes to skip subnode/attribute creation
     *        - string will be text content
     *        - associative scalar values are attributes
     *        - associative values of arrays are lists of subnodes
     *        - numbered scalar values are text nodes
     *        - numbered array values are illegal
     * @param boolean $snf -- generate hypertext content from value
     *
     * @return DOMElement -- the inserted subnode
     * @throws InternalException
     * @throws ModelException
     * @throws UAppException
     * @throws DOMException
     * @throws ReflectionException
     * @throws Exception
     *
     * @deprecated use {@see addNode()}
     */
    public function createSubnode($tagname, $value, $snf = null) {
        if (!is_null($value)) {
            $subnode = $this->addNode($tagname);
            if ($snf || is_a($value, 'DOMNode')) {
                $subnode->addHypertextContent($value, true, true);
            } else {
                if (is_array($value)) {
                    foreach ($value as $k => $v) {
                        if (is_int($k)) {
                            if ($v !== null && $v != '') $subnode->addText($v);
                        } else if (is_array($v)) {
                            foreach ($v as $sv) /** @noinspection PhpDeprecationInspection */
                                $subnode->createSubnode($k, $sv);
                        } else if (is_object($v)) throw new Exception('String expected');
                        else if ($v !== null) $subnode->addAttribute($k, $v);
                    }
                } else if (is_object($value)) throw new Exception('String expected');
                else $subnode->addText((string)$value);
            }
            return $subnode;
        }
        return null;
    }

    /**
     * Generates content of the node
     *
     * ### contentData:
     *
     * - string: single text content
     * - DOMElement or DOMDocument: subtree
     * - array:
     *        - string index -> scalar: attribute of parent node
     *        - string index -> array of contentData: multiple subnodes with same (index) name, content data for each
     *        - numeric index -> scalar: text content (DOM: subtree)
     *        - numeric index -> array of subNode [nodename, content-data]
     *
     * ### subNode:
     *        - first numeric item is nodename or [nodename, namespace] array, the rest is content data.
     *        - if no numeric item, or not a nodename, content will be applied to parent
     *
     * ## Example
     *
     * <parent>
     *        <subnode id="1">Első</subnode>
     *        <subnode id="2">Második</subnode>
     *        <subnode id="3">
     *            <n2>21</n2><n2>22</n2><n3>33</n3>
     *        </subnode>
     *        Textnode
     * </parent>
     *
     * [
     *        'subnode' => [                // or ['subnode', 'id'=>1, 'Első'], ['subnode', 'id'=>2, ...
     *            ['id'=>1, 'Első'],
     *            ['id'=>2, 'Második'],
     *            ['id'=>3,
     *                'n2' => [21, 22],        // or ['n2', 21], ['n2', 22],
     *                'n3' => [33],        // or ['n3', 33]
     *            ],
     *        'Textnode',
     * ]
     *
     * @param string|array|null $contentData -- Universal content data
     *          - null makes to skip subnode/attribute creation
     *        - string will be text content
     *        - associative scalar values are attributes (boolean will not be localized)
     *        - associative values of arrays are lists of subnodes
     *        - numbered scalar values are text nodes
     *        - numbered array values are illegal
     *
     * @param string|array $nameSpace
     *
     * @return UXMLElement -- the parent node itself
     * @throws
     */
    public function addContent($contentData, $nameSpace = null) {
        if (is_null($contentData)) return $this;
        if (is_array($contentData)) {
            foreach ($contentData as $k => $v) {
                if (is_int($k)) {
                    if (is_array($v)) { // subnode
                        if (isset($v[0]) && (is_string($v[0]) || is_array($v[0]) && isset($v[0][0]) && is_string($v[0][0]))) {
                            $nodeName1 = array_shift($v);
                            if (is_array($nodeName1)) {
                                $nameSpace1 = ArrayUtils::getValue($nodeName1, 1);
                                $nodeName1 = $nodeName1[0];
                                $prefix1 = Util::substring_before($nodeName1, ':');

                                if ($prefix1) $nameSpace[$prefix1] = $nameSpace1;
                                else {
                                    if ($nameSpace[0]) $nameSpace[1] = $nameSpace[0];
                                    $nameSpace[0] = $nameSpace1;
                                }
                            }
                            if ($nodeName1) $this->addNode($nodeName1, $v, null, $nameSpace);
                        } else $this->addContent($v, $nameSpace);
                    } // numeric => scalar
                    else if ($v !== null && $v != '') $this->addContent($v, $nameSpace);
                } // String => contentData
                else if (is_array($v)) $this->addNodes($k, $v, $nameSpace);
                // String => scalar
                else {
                    if ($v !== null) $this->addAttribute($k, Util::toString($v, is_bool($v) ? null : $this->ownerDocument->locale), $nameSpace);
                }
            }
        } else if (is_a($contentData, 'DOMNode')) {
            $this->addHypertextContent($contentData, true, true, $nameSpace);
        } else { // single scalar
            $this->addText(Util::toString($contentData, $this->ownerDocument->locale));
        }
        return $this;
    }

    /**
     * All attributes of the Element as an array
     * @return array
     */
    public function getAllAttributes() {
        $allAttributes = [];
        foreach ($this->attributes as $attributeNode) {
            /** @var DOMNode $attributeNode */
            $allAttributes[$attributeNode->nodeName] = $attributeNode->nodeValue;
        }
        return $allAttributes;
    }

    public function getXml() {
        return $this->ownerDocument->saveXML($this);
    }

    /**
     * Returns the value of a property.
     *
     * @param string $name the property name
     *
     * @return mixed the property value
     * @throws InternalException if the property is not defined or the property is write-only.
     * @see          __set()
     * @noinspection DuplicatedCode
     */
    public function __get($name) {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        }

        if (method_exists($this, 'set' . $name)) {
            throw new InternalException('Getting write-only property: ' . get_class($this) . '::' . $name);
        }

        throw new InternalException('Getting unknown property: ' . get_class($this) . '::' . $name);
    }
}

/**
 * Class UXMLNodeList
 *
 * @method UXMLElement item() item(int $num)
 */
class UXMLNodeList extends DOMNodeList {

}
