<?php
/**
 * SimpleDebug
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * A simplified replacement for Debug if direct output is necessary
 *
 * ### Using:
 * ```
 * include 'Simpledebug.php';
 * use SimpleDebug as Debug;
 * ```
 */
class SimpleDebug {
    /** @var boolean $nodebug -- a swith to switch off debug messages. */
    static public $nodebug = false;
    static public $trace;

    /**
     * @param string $a
     * @param null $b
     * @param string $c
     */
    function tracex($a, $b = null, $c = '#009') {
        if (self::$nodebug) return;
        if (is_array($b)) $b = '[' . implode(',', $b) . ']';
        $b = str_replace(' ', '_', htmlspecialchars($b));
        $eq = ($b === null) ? '' : '=';
        echo <<<EOZ
<div><strong>$a</strong>$eq<span style="color:$c;">$b</span></div>
EOZ;
    }

    /**
     * @param string $a
     */
    function trace($a) {
        if (self::$nodebug) return;
        self::tracex($a);
    }
}
