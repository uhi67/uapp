<?php
/**
 * class FakeMailer
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * Fake mailer which saves sent mails into files
 *
 * ##Example for main config
 * ```
 *    ...
 *      'mailer' => [
 *          'class' => 'FakeMailer',
 *          'dir' => $datapath . '/FakeMail',
 *      ],
 * ```
 */
class FakeMailer extends Component implements MailerInterface {
    public $dir;

    public function send($address, $subject, $message, $headers = '') {
        if (!is_dir($this->dir) && is_dir(dirname($this->dir))) @mkdir($this->dir);
        if (!is_dir($this->dir)) return false;

        $filename = $this->dir . '/' . date('ymd_His') . '_' . Util::toNameID($address) . '.eml';
        if (is_array($headers)) $headers = Util::implode_with_keys("\n", ': ', $headers);
        $f = fopen($filename, 'w');
        if (!$f) return false;
        fputs($f, $headers);
        if ($headers) fputs($f, "\n");
        fputs($f, "To: $address\n");
        fputs($f, "Subject: $subject\n\n$message");
        fclose($f);
        return true;
    }
}
