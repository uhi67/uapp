<?php
/**
 * class NotFoundException
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * NotFoundException represents a "Not Found" HTTP exception with status code 404.
 */
class NotFoundException extends UserException {
    /**
     * @param string|array $message
     * @param array $extra -- debug details
     * @param string $goto -- clickable redirection for end user
     * @param Exception $previous The previous exception used for the exception chaining.
     */
    public function __construct($message = null, $extra = null, $goto = null, $previous = null) {
        parent::__construct($message, $extra, $goto, 404, $previous);
    }

    public function getSubtitle() {
        return UApp::la('uapp', 'The requested page or resource is not found');
    }
}
