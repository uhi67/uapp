<?php
/**
 * class Mailer
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * A classic mailer using the standard mail function of the OS
 */
class Mailer extends Component implements MailerInterface {
    /**
     * @inheritDoc
     *
     * @param string $address
     * @param string $subject
     * @param string $message
     * @param string $headers
     *
     * @return bool
     */
    public function send($address, $subject, $message, $headers = '') {
        return mail($address, $subject, $message, $headers);
    }
}
