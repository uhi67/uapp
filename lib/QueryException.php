<?php
/**
 * class QueryException
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * QueryException is thrown in Query and DBX query builder functions
 */
class QueryException extends InternalException {
}
