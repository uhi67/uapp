<?php /** @noinspection HtmlPresentationalElement */
/**
 * class UApp
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/** @noinspection PhpUnused */
/** @noinspection PhpFunctionNamingConventionInspection */

/**
 * @param $errno
 * @param $errstr
 * @param $errfile
 * @param $errline
 *
 * @throws ErrorException
 */
function exception_error_handler($errno, $errstr, $errfile, $errline) {
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
}

set_error_handler("exception_error_handler", E_ERROR);

if (!class_exists('XSLTProcessor', false)) {
    fatal_error(500, 'Internal Server Error', 'No XSLT support');
}

/**
 * Base class for application (singleton)
 *
 * - Handles user login
 * - Default database connection
 * - Default locale and translator
 * - Rights system
 * - Finds and loads controllers
 * - Error and default text logging
 * - Fallback exception display
 *
 * ## Usage
 *
 * Regular usage: use via _uapp.php loader
 *
 * Minimal example:
 *
 * ```
 * $pageclass = 'StartPage';    // Classname of an UAppPage descendant
 * $path = [];  // url path elements following pagename
 * $app = new UApp($config);
 * $app->run($pageclass, $path);
 * $app->finish();
 * ```
 *
 * @see _uapp.php
 */
class UApp {
    /** @var string $sapi -- the type of interpreter -- codeception may simulate web on cli */
    public static $sapi = 'cli';
    public static $config;

    /** @var string $ip -- client's ip */
    public $ip;
    /** @var UAppPage $page the primary page controller instance loaded */
    public $page;
    /** @var DBX $sb -- the primary (default) database connection */
    public $db;
    /** @var Rights $rights -- The instance of right system of this application. Must be initialized to a class derived from Rights */
    public $rights;
    /** @var string|L10nBase $l10n -- Classname of the actual localizator L10nBase class */
    public $l10n;

    /** @var UApp $uniqueInstance -- the only instance if exists */
    public static $uniqueInstance = null;
    /** @var string $path -- UApp base path */
    private static $path;
    /** @var array $args Named argument options: name=>value pairs */
    public static $args;
    /** @var array $argn Not named argument values */
    public static $argn;
    /** @var string $_lang language code, see getLang/setLang */
    private static $_lang = 'en';
    /** @var string $_locale locale code, see getLocale/setLang */
    private static $_locale = 'GB';
    /** @var Session $session */
    public $session;
    /** @var MailerInterface $mailer */
    public $mailer;
    /** @var string $timeZone -- name of time zone */
    public $timeZone;

    private static $_httpResponseCodes;
    public static $dataPath;

    /**
     * # UApp constructor
     *
     * Creates a new single instance of UApp.
     * If the instance already exists, throws an InternalException.
     *
     * To get existing UApp instance or create new one, use {@see UApp::getInstance()}
     *
     * @param array $config
     * @param DBX $db
     * @param string $sapi -- force sapi mode while testing
     *
     * @throws ConfigurationException
     * @throws InternalException
     * @throws ReflectionException
     * @see init()
     * @see getInstance()
     *
     */
    public function __construct($config = null, $db = null, $sapi = null) {
        if (self::$uniqueInstance === -1) throw new InternalException('UApp loop');
        if (self::$uniqueInstance) throw new InternalException('UApp instance already exists. Use UApp::getInstance()');
        self::$uniqueInstance = -1; // loop prevention

        $this->init($config, $db, $sapi);
        self::$uniqueInstance = $this;
    }

    /**
     * # UApp::init()
     *
     * Initializes UApp with given configuration and environment
     *
     * @param array $config -- main configuration array, default uses global $config variable
     * @param DBX $db -- do not create default database, use existing (for testing environments)
     * @param string $sapi -- override automatic SAPI name (for testing environments)
     *
     * @throws ConfigurationException
     * @throws InternalException
     * @throws ReflectionException
     */
    public function init($config = null, $db = null, $sapi = null) {
        self::$dataPath = static::config('datapath', dirname(__DIR__) . '/data');
        if (!file_exists(self::$dataPath)) {
            if (!file_exists(dirname(self::$dataPath)))
                Util::fatalError(500, 'Configuration error', 'Data directory does not exists');
            if (!mkdir(self::$dataPath, 0774))
                Util::fatalError(500, 'Configuration error', "Data directory cannot be created at " . self::$dataPath);
        }

        if (!$config) $config = ArrayUtils::getValue($GLOBALS, 'config');
        if (!$config) throw new InternalException('No config given for UApp');
        static::$config = $config;
        $this->db = $db ?: self::createDb();
        self::$sapi = $sapi ?: php_sapi_name();

        $this->timeZone = static::config('timeZone', static::config('l10n/timezone', @date_default_timezone_get()));
        date_default_timezone_set($this->timeZone);

        $this->l10n = static::config('l10n/class', 'L10nBase');
        if (!is_callable([$this->l10n, 'getText'])) throw new InternalException('Invalid L10n class', $this->l10n);
        /** @noinspection PhpAssignmentInConditionInspection */
        if (is_callable($init = [$this->l10n, 'init'])) call_user_func($init, static::config('l10n'));

        static::setLang(static::config('l10n/language', 'en-GB'));

        if (self::$sapi == "cli") static::parseArgs();
        else {
            $sessionconfig = static::config('session');
            $sessionclass = ArrayUtils::getValue($sessionconfig, 'classname', 'Session');
            $this->session = new $sessionclass($sessionconfig);
        }

        Debug::init(!isset($GLOBALS['nodebug']), ArrayUtils::getValue($config, 'basepath'));

        $this->ip = getenv('REMOTE_ADDR');

        $this->mailer = Component::create(static::config('mailer', ['class' => 'Mailer']));
    }

    function __destruct() {
        static::$uniqueInstance = null;
        // Debug is based on real sapi.
        if (php_sapi_name() == "cli" && Debug::$trace) {
            echo "Trace: " . Debug::$trace . "\n";
            echo Debug::debug_content(Debug::$trace);
            echo "\n";
        }
    }

    /**
     * @throws ConfigurationException
     * @throws InternalException
     * @throws ReflectionException
     */
    function finish() {
        Debug::trace('Finishing Uapp');
        if ($this->rights) $this->rights->save();
    }

    /**
     * Singleton constructor
     *
     * Returns existing instance of UApp, or creates a new one if not exists.
     *
     * @param null $config
     *
     * @return UApp
     * @throws ConfigurationException
     * @throws InternalException
     * @throws ReflectionException
     * @see __construct()
     *
     */
    static function getInstance($config = null) {
        if (!static::$uniqueInstance instanceof UApp) {
            new static($config); // ha nem, akkor létrehozza azt
        }
        return static::$uniqueInstance;
    }

    /**
     * Returns the instance if valid or null if not
     * @return UApp
     */
    static function hasInstance() {
        if (!self::$uniqueInstance instanceof UApp) return null;
        return self::$uniqueInstance;
    }

    /**
     * Created for the codeception intercface
     *
     * @throws ConfigurationException
     * @throws InternalException
     * @throws ReflectionException
     */
    static function destroyInstance() {
        if (!self::$uniqueInstance instanceof UApp) return;
        self::$uniqueInstance->finish();
        self::$uniqueInstance = null;
    }

    /**
     * Returns a value from application's global configuration file.
     * Name can be a name/subname path.
     * If first character is '!', this will be omitted, but the value is mandatory.
     * Exception will occur if not found, and default will be ignored.
     *
     * @param string $name -- configuration variable or path
     * @param mixed $default
     *
     * @return mixed
     * @throws ConfigurationException
     */
    static function config($name, $default = null) {
        $mandatory = false;
        $config = static::$config; #$GLOBALS['config'];
        if (substr($name, 0, 1) == '!') {
            $name = substr($name, 1);
            $mandatory = true;
        }
        $na = explode('/', $name);
        for ($i = 0; $i < count($na); $i++) {
            if (isset($config[$na[$i]])) $config = $config[$na[$i]];
            else {
                if ($mandatory) throw new ConfigurationException("Configuration variable `$name` is missing");
                return $default;
            }
        }
        return $config;
    }

    /**
     * Returns base path of UApp framework
     *
     * @return string
     */
    static function getPath() {
        if (!self::$path) self::$path = dirname(__DIR__);
        return self::$path;
    }

    /**
     * Creates and starts an UAppPage (controller)
     *
     * @param UAppPage|UAppCommand|string $pageclass -- classname to use, must be an UAppPage
     * @param array $path -- url path elements after scriptname
     *
     * @return void
     * @throws Exception
     */
    function run($pageclass, $path = null) {
        try {
            if (!class_exists($pageclass)) throw new NotFoundException("Page class `$pageclass` not found");
            $this->page = new $pageclass($this, $path);
            $this->page->go();
            $this->page->finish();
        } /** @noinspection PhpRedundantCatchClauseInspection */
        catch (HandledException $e) { // Codeception miatt kell
            // nothing to do
        } catch (Throwable $e) {
            $x = new UAppException('Error displaying page ' . $pageclass, 500, $e);
            static::showException($x);
        }
    }

    /**
     * Displays an Exception on CLI or HTML output.
     * TODO: translation
     *
     * @param Exception $e
     * @param bool $d
     */
    static function showException($e, $d = true, $subtitle = null) {
        if (!$subtitle && is_a($e, 'UAppException')) {
            $subtitle = $e->getSubtitle();
        }
        if (php_sapi_name() == "cli") {
            if ($subtitle) echo Ansi::color($subtitle, 'yellow', 'black'), "\n";
            /** @noinspection PhpAssignmentInConditionInspection */
            if (is_a($e, 'UAppException') && ($extra = $e->getExtra())) {
                echo Ansi::color(Util::objtostr($extra), 'purple'), "\n";
            }

            $msg = 'UApp belső hiba: ' . $e->getMessage();
            $details = sprintf(" in file '%s' at line '%d'", $e->getFile(), $e->getLine());
            echo Ansi::color($msg, 'light red'), $details, "\n";
            if ($d || Debug::$trace) {
                $trace = explode("\n", $e->getTraceAsString());
                $baseurl = dirname(__DIR__);
                foreach ($trace as $line) {
                    $basepos = strpos($line, $baseurl);
                    $color = ($basepos > 1 && $basepos < 5) ? 'brown' : 'red';
                    echo Ansi::color($line, $color), "\n";
                }

                /** @noinspection PhpAssignmentInConditionInspection */
                while ($e = $e->getPrevious()) {
                    echo Ansi::color("\n\n" . html_entity_decode($e->getMessage()), 'light purple') . "\n";
                    if ($d || Debug::$trace) {
                        $trace = explode("\n", $e->getTraceAsString());
                        foreach ($trace as $line) {
                            $basepos = strpos($line, $baseurl);
                            $color = ($basepos > 1 && $basepos < 5) ? 'brown' : 'red';
                            echo Ansi::color($line, $color), "\n";
                        }
                    }
                }
            }
            return;
        }
        if ($d) Debug::tracex('trace', $e->getTrace());
        echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        echo '<html lang="hu">';
        echo '<head><title>UApp internal error</title>';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
        echo '</head><body>';
        echo '<h1>Belső hiba</h1>';
        echo '<p>Ajjaj, nagy a baj, nem tudjuk a lapot megjeleníteni :-(</p>';
        echo '<p>(UApp keretrendszer belső hiba)</p>';
        $details = sprintf(" in file '%s' at line '%d'", $e->getFile(), $e->getLine());
        echo '<div><b>' . htmlspecialchars($e->getMessage()) . '</b>' . (Debug::$trace ? $details : '') . '</div>';
        $extra = $e instanceof UAppException ? $e->getExtra() : '';
        if ($extra) echo '<pre>' . htmlspecialchars(Util::objtostr($extra)) . '</pre>';

        if (Debug::$trace) {
            echo '<pre>';
            echo htmlspecialchars($e->getTraceAsString());
            /** @noinspection PhpAssignmentInConditionInspection */
            while ($e = $e->getPrevious()) {
                $details = sprintf(" in file '%s' at line '%d'", $e->getFile(), $e->getLine());
                echo "\n\n<b>" . htmlspecialchars($e->getMessage()) . '</b>' . $details . "\n";
                echo htmlspecialchars($e->getTraceAsString());
                $extra = $e instanceof UAppException ? $e->getExtra() : '';
                echo '<pre>' . htmlspecialchars(Util::objtostr($extra)) . '</pre>';
            }
            echo '</pre>';
        }
        echo '</body>';
    }

    /**
     * Creates a db connection with given configuration name.
     *
     * The db configuration array will be taken from main configuration array.
     *
     * @param string $name
     * @param string $default
     *
     * @return DBX -- the connection
     * @throws ConfigurationException
     * @throws InternalException
     * @throws ReflectionException
     */
    static function createDb($name = 'database', $default = '') {
        $dx = $default ? "or $default" : '';
        $dbc = self::config($name, self::config($default));
        if (!$dbc) throw new ConfigurationException("Database $name $dx not found");
        if (isset($dbc[0]) || isset($dbc['class'])) {
            $connection = Component::create($dbc);
            if (!($connection instanceof DBX)) throw new ConfigurationException(sprintf("Invalid database class `%s`", $dbc['class']));
            return $connection;
        }
        /** @noinspection PhpDeprecationInspection */
        return self::createDbConf($dbc);
    }

    /**
     * Creates a db connection with given configuration array
     *
     * @param array $config with items:
     *   - type (optional, default is pg)
     *   - host
     *   - port
     *   - user
     *   - password
     *   - name
     *   - encoding
     *
     * @return DBX
     * @throws ConfigurationException
     * @throws DatabaseException
     * @throws InternalException
     * @deprecated: use Component::create
     *
     */
    static function createDbConf($config) {
        $type = ArrayUtils::getValue($config, 'type', 'pg');
        switch ($type) {
            case 'my':
                $port = isset($config['port']) ? $config['port'] : null;
                return new DBXMY(isset($config['host']) ? $config['host'] : '', $port,
                    $config['user'], $config['password'], $config['name'], $config['encoding']);
            case 'pg':
                $port = isset($config['port']) ? $config['port'] : 5432;
                return new DBXPG(isset($config['host']) ? $config['host'] : '', $port,
                    $config['user'], $config['password'], $config['name'], $config['encoding']);
            default:
                throw new ConfigurationException('Server configuration error. Invalid database type');
        }
    }

    /**
     * Creates a URL relative to baseurl
     *
     * @param array $to
     *
     * @return string
     * @throws ConfigurationException
     * @throws InternalException
     * @throws Exception
     * @see Util::createUrl()
     *
     */
    public static function createUrl($to) {
        return Util::createUrl(UApp::getInstance()->config('baseurl'), $to);
    }

    /**
     * @param string $title
     * @param string $h1
     * @param string $h2
     * @param string $msg
     * @param string $url
     *
     * @throws ConfigurationException
     * @throws Exception
     * @throws InternalException
     * @throws UAppException
     */
    function pageMessage($title, $h1 = '', $h2 = '', $msg = '', $url = '') {
        $page = new EmptyAppPage($this);
        $page->setTitle($title);
        $page->setH1($h1 ?: $title);
        $page->setH2($h2);
        $page->addMessages($msg, ['class' => 'main']);
        $page->addData([['url', null, $url]]);
        $page->render();
    }

    /**
     * Logs an application event into text log
     *
     * @param string $str
     *
     * @throws ConfigurationException
     * @throws Exception -- if logfile or logdir cannot be created
     */
    function log($str) {
        if (php_sapi_name() == "cli") {
            $_user_id = 'cli';
        } else {
            if (!$this->page) return;
            $_user_id = $this->page->user->id; //Util::getSession('user_id');
        }
        $logFile = static::config('logfile');
        if (!is_dir(dirname($logFile))) {
            if (!mkdir(dirname($logFile), 0774))
                throw new Exception('Cannot create log dir: ' . dirname($logFile));
        }
        if (!error_log(date('y.m.d H:i') . "\t" . $_user_id . "\t" . $this->ip . "\t" . $str . "\n", 3, $logFile))
            throw new Exception('Cannot open logfile: ' . $logFile);
        @chmod($logFile, 0774);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Logs an error event into error log
     *
     * @param $str
     *
     * @param string|array $tags
     * @param int $depth
     *
     * @throws ConfigurationException
     */
    function error_log($str, $tags = 'app', $depth = 3) {
        if (is_array($tags)) $tags = implode(',', $tags);
        if (php_sapi_name() == "cli") {
            $_user_id = 'cli';
        } else {
            $_user_id = $this->page->user->id; //Util::getSession('user_id');
            Debug::tracex('error', $str, '#C00', '', $depth, $tags);
        }
        $logFile = static::config('errorlogfile');
        if (!$logFile) {
            $dataPath = static::config('datapath');
            if (!$dataPath) throw new ConfigurationException('No datapath is configured');
            if (class_exists('\Codeception\Util\Debug')) \Codeception\Util\Debug::debug('UApp config: ' . json_encode(static::$config));
            $logFile = $dataPath . '/error.log';
        }
        error_log(date('Y-m-d H:i') . "\t$_user_id\t$this->ip\t$tags\t" . $str . "\n", 3, $logFile)
        or die('Cannot open error-logfile: ' . static::config('errorlogfile'));
    }

    /**
     * Logs an error event into error log
     *
     * @param $str
     * @param string $tags
     * @param int $depth
     *
     * @throws ConfigurationException
     * @throws InternalException
     * @throws ReflectionException
     */
    public static function error($str, $tags = 'app', $depth = 4) {
        static::getInstance()->error_log($str, $tags, $depth);
    }

    /**
     * Reads command line arguments and stores
     * - named values to self::$args,
     * - other values to self::$argn
     *
     * @return void
     */
    function parseArgs() {
        global $argv, $argc;
        self::$args = []; // Named argument option values name=>value
        self::$argn = []; // Not named values

        // Parsing args
        for ($i = 2; $i < $argc; $i++) {
            if (preg_match('/^(\w+)=(.*)$/', $argv[$i], $m)) {
                self::$args[$m[1]] = $m[2];
            } else self::$argn[] = $argv[$i];
        }

        #echo Ansi::color("Args:\n", 'light purple');
        #foreach(self::$args as $i=>$a) echo "$i=>$a\n";
        #echo Ansi::color("Argn:\n", 'light purple');
        #foreach(self::$argn as $i=>$a) echo "$i=>$a\n";
    }

    /**
     * Sets current language or language with locale
     * If locale is unknown, will be empty
     *
     * @param string $lang alpha-2 language code with optional alpha-2 locale code (ISO 639-1 && ISO 3166-1)
     *
     * @return string -- language code only
     * @throws InternalException
     */
    public static function setLang($lang) {
        $matches = [];
        if (preg_match('/([a-z]{2})(-([A-Z]{2}))?/', $lang, $matches)) {
            self::$_lang = $matches[1];
            if (isset($matches[3]) && $matches[3] != '') self::$_locale = $matches[3];
            else self::$_locale = '';
        } else throw new InternalException("Invalid language format `$lang`");
        return self::$_lang;
    }

    /**
     * Gets current language without locale
     *
     * @return string -- language in ISO 639-1
     */
    public static function getLang() {
        return self::$_lang;
    }

    /**
     * Gets current language with locale if known
     *
     * @return string -- locale in ll-cc format (ISO 639-1 && ISO 3166-1)
     */
    public static function getLocale() {
        return self::$_lang . (self::$_locale ? '-' . self::$_locale : '');
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Translates a text using configured localization class
     *
     * @param string $category -- message category, UApp uses 'uapp'. Application default is 'app'
     * @param string $text -- message or textid to translate
     * @param array $params -- parameters to substitute into translated text at {$var} positions
     * @param string $locale -- language (ll)or locale (ll-LL) code to translate into.
     * @return string -- the translated text with parameters substituted
     * @throws InternalException
     */
    public static function la($category, $text, $params = [], $locale = null) {
        if (!$locale) $locale = static::getLocale();
        $app = UApp::hasInstance();
        if ($app) $l10n = $app->l10n; else $l10n = 'L10nBase';
        return $l10n::getText($category, $text, $params, $locale);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Formats a datetime using configured localization class
     *
     * @param DateTime $dt
     * @param int $datetype -- date format as IntlDateFormatter::NONE, type values are 'NONE', 'SHORT', 'MEDIUM', 'LONG', 'FULL'. Default is SHORT.
     * @param int $timetype -- time format as IntlDateFormatter::NONE, type values are 'NONE', 'SHORT', 'MEDIUM', 'LONG', 'FULL'. Default is NONE.
     * @param string $locale
     *
     * @return string -- the formatted date
     * @throws InternalException
     */
    public static function df($dt, $datetype = IntlDateFormatter::SHORT, $timetype = IntlDateFormatter::NONE, $locale = null) {
        $dt = Util::createDateTime($dt);
        if (!$locale) $locale = static::getLocale();
        $app = UApp::hasInstance();
        $l10n = $app ? $app->l10n : 'L10nBase';
        return $l10n::formatDate($dt, $datetype, $timetype, $locale);
    }

    /**
     * Returns a http response code title in the given or default language
     *
     * @param int $code
     * @param string $lang
     *
     * @return string
     * @throws InternalException
     */
    public static function getHttpResponseCode($code, $lang = null) {
        if (!$lang) $lang = static::getLocale();
        $uappdef = static::config('uapp/def', dirname(__DIR__) . '/def') . '/';
        if (!self::$_httpResponseCodes) {
            $filename = 'http_response_codes.php';
            self::$_httpResponseCodes = include($uappdef . $filename);
        }
        $la = $lang;
        if (!isset(self::$_httpResponseCodes[$la]) && strlen($lang) == 5) {
            $la = substr($lang, 0, 2);
            if (!isset(self::$_httpResponseCodes[$la])) {
                $languages = array_keys(self::$_httpResponseCodes[$lang]);
                if (count($languages) == 0) return static::la('uapp', 'Missing http response definitions');
                $la = $languages[0];
            }
        }
        if (isset(self::$_httpResponseCodes[$la][$code])) return self::$_httpResponseCodes[$la][$code];
        #Debug::tracex('_httpResponseCodes', self::$_httpResponseCodes);
        #return static::la('uapp', 'Missing http response code `{$code}` ({$la})', array('code'=>$code, 'la'=>$la));
        return null;
    }

    /**
     * Send a mail via configured mailer class
     *
     * @param string $address -- e-mail cím (mailto: prefix lehetséges)
     * @param string $subject
     * @param string $message
     * @param array|string $headers
     *
     * @return bool
     * @throws ConfigurationException
     * @throws InternalException
     * @throws ReflectionException
     */
    public static function sendMail($address, $subject, $message, $headers) {
        $prefix = 'mailto:';
        if (strpos($address, $prefix) === 0) $address = substr($address, strlen($prefix));
        return static::getInstance()->mailer->send($address, $subject, $message, $headers);
    }

    /**
     * Sending plain-text/HTML combined mail as a multipart MIME message
     *
     * see https://www.rfc-editor.org/rfc/rfc2046#section-5.1.1
     *
     * @param $address -- e-mail cím (mailto: prefix lehetséges)
     * @param $subject
     * @param $plainMessage
     * @param $htmlMessage
     * @param $headers
     * @return bool
     * @throws ConfigurationException
     * @throws InternalException
     * @throws ReflectionException
     */
    public static function sendMailMultiple($address, $subject, $plainMessage, $htmlMessage, $headers = '') {
        $boundary = '======' . md5(uniqid(rand())) . '======';
        if ($headers) $headers = trim($headers) . "\r\n";
        $headers .= "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type: multipart/alternative;boundary=\"$boundary\"\r\n";

        $message = "This is multipart message using MIME / Ez egy többrészes üzenet MIME kódolással.\r\n";
        $message .= "\r\n--" . $boundary . "\r\n";
        $message .= "Content-Type: text/plain; charset=UTF-8\r\n";
        $message .= "Content-Transfer-Encoding: 7bit" . "\r\n\r\n";
        $message .= trim($plainMessage);
        $message .= "\r\n\r\n--" . $boundary . "\r\n";
        $message .= "Content-Type: text/html; charset=UTF-8\r\n";
        $message .= "Content-Transfer-Encoding: 7bit" . "\r\n\r\n";
        $message .= trim($htmlMessage);
        $message .= "\r\n\r\n--" . $boundary . "--";
        return static::sendMail($address, $subject, $message, $headers);
    }

    /**
     * Gets to the output a file searching in application, framework and modules
     * php files will be executed.
     *
     * @param string $pathinfo
     *
     * @param string $uapp_path
     *
     * @return bool|int
     * @throws ConfigurationException
     * @throws Exception
     */
    static function getFile($pathinfo, $uapp_path = null) {
        $c = $pathinfo;
        $bases = Loader::getBases();
        if (!$uapp_path) $uapp_path = UApp::config('!uapp/path');

        $isxsl = substr($pathinfo, 0, 5) == '/xsl/';
        if ($isxsl) {
            array_unshift($bases,
                UApp::config('xslpath', UApp::config('apppath') . '/xsl'),
                UApp::config('genxslpath', UApp::$dataPath . '/xsl'),
                $uapp_path . '/xsl'
            );
        }
        $type = Util::mimetype($pathinfo);
        $expires = 3600 * 24;

        if ($type != 'application/octet-stream')
            for ($i = 0; $i < count($bases); $i++) {
                $base = $bases[$i];
                if ($isxsl) $c = substr($pathinfo, 4);
                $filename = str_replace('//', '/', "$base/$c");
                if (file_exists($filename)) {
                    if ($type == 'php') {
                        require_once $filename;
                        exit;
                    } else {
                        $timeZone = static::config('timeZone', static::config('l10n/timezone', @date_default_timezone_get()));
                        date_default_timezone_set($timeZone);
                        return static::serveFile($filename, $type, $expires);
                    }
                }
            }
        return false;
    }

    /**
     * Outputs a file or returns 304 if not modified
     *
     * @param string $filename
     * @param string $type
     * @param int $expires
     *
     * @return false|int
     * @throws Exception
     */
    public static function serveFile($filename, $type, $expires) {
        $ims = isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? new DateTime($_SERVER['HTTP_IF_MODIFIED_SINCE']) : null;
        $last_modified = new DateTime('@' . filemtime($filename));
        header("Last-Modified: " . $last_modified->format(DATE_RFC1123));
        header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . 'GMT');
        header('Cache-Control: private');
        if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && ($last_modified <= $ims)) {
            #self::log("304\t$ip\t$pathinfo\t$ims\t$lm");
            header("HTTP/1.1 304 Not Modified");
            die;
        }
        header('Content-Type: ' . $type);
        return readfile($filename);
    }

}
