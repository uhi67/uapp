<?php /** @noinspection PhpUnused */
/**
 * class UXMLDoc
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * Extended DOM functions
 *
 * @see UXMLElement
 * @property UXMLElement $documentElement
 * @property-read string $xml -- canonical xml representation of the document
 * @method UXMLElement createElement() createElement(string $name, string $value = null)
 * @method UXMLElement createElementNS() createElementNS(string $name, string $namespace, string $value = null)
 */
class UXMLDoc extends DOMDocument {
    /** @var string $xslpath -- for XSL processing */
    public $xslpath;
    /** @var string $locale -- for attribute conversion */
    public $locale;

    /**
     * @param string $enc
     * @param string $root -- root node neve, ha üres, nem hozza létre
     * @param string $xslpath -- location of xsl files for process() method
     * @param string $locale
     * @throws DOMException
     */
    function __construct($enc = 'UTF-8', $root = 'data', $xslpath = null, $locale = null) {
        parent::__construct('1.0', $enc);
        $this->formatOutput = true;
        $this->preserveWhiteSpace = false;
        $this->encoding = $enc;
        $this->substituteEntities = false;
        $this->resolveExternals = false;

        $this->registerNodeClass('DOMElement', 'UXMLElement');

        if ($root) $this->appendChild($this->createElement($root));
        $uapp = UApp::hasInstance();
        $this->xslpath = $xslpath;
        if (!$this->xslpath && $uapp) {
            try {
                $this->xslpath = UApp::config('xslpath');
            } catch (Exception $e) {
                $this->xslpath = null;
            }
        }
        $this->locale = $locale;
        if (!$this->locale && $uapp) $this->locale = UApp::getLocale();
    }

    /**
     * Beszúrja a dokumentumba a node alá a másik dokumentum gyökér alatti elemeit.
     *
     * @param DOMElement $node -- ez alá
     * @param DOMDocument $xmldoc -- beszúrandó dokumentum
     * @return DOMElement az eredeti node
     * @deprecated
     */
    static function addDoc($node, $xmldoc) {
        // Beszúrandó node-ok iterálása
        $node2 = $xmldoc->documentElement;
        $nodex = $node2->firstChild;
        while ($nodex) {
            $node->appendChild($node->ownerDocument->importNode($nodex, true));
            $nodex = $nodex->nextSibling;
        }
        return $node;
    }

    /**
     * Adds new node with attributes and content to parent node
     * Null attribute values will not create attributes.
     * Array attribute values will generate series of subnodes with array element values as text value
     *
     * ### Namespace
     * - single namespace will be the namespace of the node
     * - may be null or empty string - no namespace will be added
     * - array - the namespace with prefix as index will be used
     *     - if prefix not found in indices and namespace has element at 1-index, it will be used
     *       - or if no prefix, and namespace has element at 0-index, it will be used
     *     - otherwise no namespace will be added
     *
     * @param UXMLElement|DOMElement $node -- parent node
     * @param string $name -- nodename of created node or array(name, namespace) @see addNodes
     * @param array $attributes -- associative array
     * @param string|array $content -- text content or contents for multiple text nodes
     * @param string|array $namespace namespace of node or namespace array to associate with current prefix
     *
     * @return UXMLElement -- the inserted node
     * @deprecated
     */
    public static function addNode($node, $name, $attributes = null, $content = null, $namespace = null) {
        return $node->addNode($name, $attributes, $content, $namespace);
    }

    /**
     * ## Adds an attribute to the node
     *
     * Handles special value types as:
     * - DateTime
     * - Inet
     * - MacAddress
     * - Array: creates multiple text nodes instead of attributes
     * - bool: skips locale
     *
     * Skips attribute names containing invalid characters
     *
     * If value is an array, subnodes will be created with content as value element
     *
     * Namespace will be added to prefixed attribute names.
     *
     * @param UXMLElement $node
     * @param string $name -- optionally prefixed name
     * @param mixed $value -- array value to generate subnodes
     * @param mixed $namespace -- single namespace or prefix-indexed array of namespaces
     *
     * @return DOMAttr|UXMLElement|null -- the first inserted subnode, the inserted attribute node, or null if invalid name
     * @throws ConfigurationException
     * @throws InternalException
     * @deprecated
     */
    public static function addAttribute($node, $name, $value, $namespace = null) {
        if ($value === null) return null; // skip null attributes
        $doc = $node->ownerDocument;
        if (!is_array($value) && !is_string($value)) $value = Util::toString($value, is_bool($value) ? null : $doc->locale);
        $prefix = Util::substring_before($name, ':');

        // Prepare namespace
        if ($prefix != '') {
            if (is_array($namespace) && isset($namespace[$prefix]))
                $node->addNamespaceDeclaration($prefix, $namespace[$prefix]);
            if (is_string($namespace) && $namespace != '')
                $node->addNamespaceDeclaration($prefix, $namespace);
        }

        if (is_array($value)) {
            // Insert subnode
            $n1 = null;
            foreach ($value as $k => $v) if ($k !== 'count') {
                $n = $node->addTextNode($name, $v);
                if (!$n1) $n1 = $n;
            }
            return $n1;
        } else {
            if (preg_match('/^[_a-zA-Z][:a-zA-Z0-9_-]*$/', $name)) {
                $locale = ($doc instanceof UXMLDoc) ? $doc->locale : 'hu';
                $value = Util::toString($value, $locale);
                $node->setAttribute($name, $value);
                return $node->getAttributeNode($name);
            }
        }
        return null;
    }

    /**
     * Adds attributes to the node
     * Handles special types
     *
     * @param UXMLElement $node
     * @param array $values
     *
     * @return UXMLElement self
     * @throws ConfigurationException
     * @throws InternalException
     * @see addAttribute
     *
     * @deprecated
     */
    public static function addAttributes($node, $values) {
        return $node->addAttributes($values);
    }


    /**
     * Creates a list of similar nodes from each item of values array
     *
     * @param UXMLElement $parent_node
     * @param string|array $name -- name of created node or array(name, namespace) @see addNodes
     * @param array $values -- array of associative arrays in attribute=>value form.
     * @param array $options
     *        'subnodeFields' -- associative array of callable($index, $attributes) which generates subnode(s);
     *            where index is the index of current node, $attributes are attributes of current node.
     *            Callable may generate scalar or array, scalar may be an xml (DOMElement)
     *
     * @return UXMLElement -- first inserted node or null if none
     * @deprecated
     */
    public static function addNodes($parent_node, $name, $values, /** @noinspection PhpUnusedParameterInspection */ $options = null) {
        return $parent_node->addNodes($name, $values);
    }

    /**
     * Creates subnodes using variing values array with keys:
     *    - numeric: generates text node (multiple is possible)
     *    - _nodename: generates subnode (recursive)
     *  - others: generates attribute
     *
     * @param UXMLElement $parent_node
     * @param string $tagname
     * @param string|array $values -- content, attribute and subnode values
     *
     * @return UXMLElement
     * @throws ConfigurationException
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     * @deprecated
     */
    public static function addSubNode($parent_node, $tagname, $values) {
        /** @noinspection PhpDeprecationInspection */
        return $parent_node->addSubNode($tagname, $values);
    }

    /**
     * Adds new node optionally with parents, attributes and content to parent node, if not exists
     * Creates all parent nodes specified in the path if not exist
     * Null attribute values will not create attributes.
     * Array attribute values will generate series of subnodes with array element values as text value
     * The generated node may not conform to the path specification if the attributes differ from path condition.
     *
     * @param UXMLElement $parent -- parent node
     * @param string $name -- node-path (xpath expression) 'nodename[cond]/nodename[cond]/...nodename[cond]' format
     * @param array $attributes -- array of associative arrays for all levels
     * @param array $content -- array of text contents for all levels
     * @param array $namespaces -- optional array of prefix=>namespaceurl items
     *
     * @return UXMLElement -- the found or inserted node (last level)
     * @throws InternalException
     * @throws UAppException
     * @deprecated
     */
    public static function addNodePathIfNotExists($parent, $name, $attributes = null, $content = null, $namespaces = null) {
        return $parent->addNodePathIfNotExists($name, $attributes, $content, $namespaces);
    }

    /**
     * A megadott node alá generál egy-egy node-ot az sql eredménysoraiból
     * a mezőnevekből attributumok lesznek.
     * null érték esetén az attributum nem jön létre.
     * _ kezdetű mezőnevekből node-ok lesznek szöveges tartalommal
     * _ végű mezőnevekből eltávolítjuk a tag-eket
     * _content mezőnévből hypertext tartalom vagy szöveges node lesz
     *
     * @param DBX $db
     * @param UXMLElement $node -- parent node
     * @param string $name -- nodename for new nodes
     * @param string|Query $sql -- sql with $1 parameters
     * @param array $params -- sql parameters
     * @param bool $valuenodes -- generate subnodes for all field values
     *
     * @return UXMLElement|null az első beszúrt node-ot
     * @throws ConfigurationException
     * @throws InternalException
     *
     * @deprecated use UXMLElement::query()
     */
    public static function query($db, $node, $name, $sql, $params = [], $valuenodes = false) {
        return $node->query($db, $name, $sql, $params, $valuenodes);
    }

    /**
     * Formats XML fragment string to standard format.
     * Detects xml declaration.
     *
     * @param string $xml
     *
     * @return string
     * @throws DOMException
     * @throws InternalException
     */
    public static function formatFragment($xml) {
        if (preg_match('/^(<\?xml\s+version="1.0"\s+encoding="([^"]+)"\s*\?>\s*)/', $xml, $mm)) {
            // Complete document
            $doc = new UXMLDoc($mm[2], false);
            $doc->preserveWhiteSpace = false;
            $doc->formatOutput = true;
            if (!$doc->loadXML($xml)) throw new DOMException('Invalid xml: ' . $xml);
            return $doc->saveXML($doc->documentElement);
        }

        // Fragment
        $doc = UXMLDoc::createFragment($xml, 'UTF-8', true);
        if ($doc === null) throw new DOMException('Invalid xml: ' . $xml);
        $doc->preserveWhiteSpace = false;
        $doc->formatOutput = true;
        return $doc->saveXML($doc->documentElement->firstChild);
    }

    /**
     * Add attributes to a node from result set line
     *
     * @param DBX $db
     * @param UXMLElement $node
     * @param resource $rs
     * @param int $i -- number of result line
     * @param bool $valuenodes -- create subnodes
     *
     * @return UXMLElement self
     * null érték esetén az attributum nem jön létre.
     * _ kezdetű mezőnevekből node-ok lesznek szöveges tartalommal
     * _ végű mezőnevekből eltávolítjuk a tag-eket
     * _content mezőnévből hypertext tartalom vagy szöveges node lesz
     * @throws DatabaseException
     * @throws InternalException
     * @deprecated
     */
    static protected function addFields($db, $node, $rs, $i, $valuenodes = false) {
        return $node->addFields($db, $rs, $i, $valuenodes);
    }

    /**
     * UXMLDoc::queryTree()
     *
     * @param DBX $db
     * @param UXMLElement $node
     * @param string $name
     * @param string $sql
     * @param array $params
     * @param array $subqueries - array(array(name, sql, subquery), ...) - sql may contain @references to upper level attributes
     *
     * @return UXMLElement first child at top level
     * @throws DatabaseException
     * @throws InternalException
     * @deprecated
     */
    public static function queryTree($db, $node, $name, $sql, $params = [], $subqueries = null) {
        /** @noinspection PhpDeprecationInspection */
        return $node->queryTree($db, $name, $sql, $params, $subqueries);
    }

    /**
     * Processes an DOM document using an XSLT loaded from given file
     *
     * @param DOMDocument $xmlsource
     * @param string $xslfile -- full path
     * @param array $params -- global accessable parameters in XSLT processing
     * @param bool $xml -- true = XML string output, false = DOMDocument (default)
     * @return DOMDocument|string
     * @throws InternalException
     */
    static function processXml($xmlsource, $xslfile, $params = NULL, $xml = false) {
        if (!Loader::classExists('XSLTProcessor')) throw new InternalException('XSL module is missing');
        if (is_null($xmlsource)) throw new InternalException('XML document is null');
        $xsldoc = new XSLTProcessor();
        $xmldoc = new DomDocument();
        if (!file_exists($xslfile)) echo "Nincs meg a fájl: $xslfile";
        $xmldoc->load($xslfile);
        $xsldoc->importStylesheet($xmldoc);
        if (is_array($params))
            $xsldoc->setParameter('', $params);
        if ($xml) {
            $result = $xsldoc->transformToXml($xmlsource);
        } else {
            $result = $xsldoc->transformToDoc($xmlsource);
            $result->formatOutput = true;
        }
        return $result;
    }

    /**
     * Processes the document into a new DOMDocument or string using given xsl
     *
     * @param string $xslfile -- name of xsl file. Default is located in xslpath
     * @param array $params -- global parameters accessible in xsl variables
     * @param bool $xml -- transform into XML string (default: to DOMDocument)
     *
     * @return DomDocument|string -- the resulting document
     * @throws InternalException
     */
    function process($xslfile, $params = NULL, $xml = false) {
        try {
            #$result = new DomDocument(); // Empty for exceptions
            if (!Loader::classExists('XSLTProcessor')) throw new Exception('XSL module is missing');
            $xsldoc = new XSLTProcessor();
            $xmldoc = new DomDocument();
            if (!file_exists($xslfile)) throw new ConfigurationException(['Nincs meg a fájl %s', [$xslfile]]);
            if (!$xmldoc->load($xslfile)) {
                $ee = error_get_last();
                throw new Exception("Error loading `$xslfile`: " . Util::objtostr($ee));
            }
            if ($this->xslpath) {
                $xmldoc->documentURI = $this->xslpath . '/' . pathinfo($xslfile, PATHINFO_FILENAME);
            } else {
                $xmldoc->documentURI = $xslfile;
            }
            if (!@$xsldoc->importStylesheet($xmldoc)) {
                $ee = error_get_last();
                $message = ArrayUtils::getValue($ee, 'message');
                // Filter `xsl:include : unable to load file:/D:/data/www/rr/xsl/samlres.xsl`
                $filename = '?';
                if (preg_match('~unable to load file:(.+)$~', $message, $mm)) {
                    $filename = $mm[1];
                    $include = UXMLDoc::load($filename);
                    $ee = error_get_last();
                    if (!$include) {
                        throw new Exception("Error loading include `$filename` in `$xslfile`: " . $ee['message']);
                    }
                }
                throw new Exception("Error interpreting `$xslfile`: " . $message);
            }

            if (is_array($params))
                $xsldoc->setParameter('', $params);
            if ($xml) {
                $result = $xsldoc->transformToXml($this);
                if (!$result) throw new InternalException('XSL transformation failed', $xslfile);
            } else {
                $result = $xsldoc->transformToDoc($this);
                if (!$result) throw new InternalException("Error processing stylesheet $xslfile");
                $result->formatOutput = true;
            }
        } catch (Exception $e) {
            Debug::tracex('exception', $e->getMessage());
            $ee = error_get_last();
            if ($ee) Debug::tracex('php error', sprintf('%s(%s): %s', $ee['file'], $ee['line'], $ee['message']));
            Debug::tracex('trace', $e->getTrace());
            Debug::backtrace();
            Debug::trace_xml('Document', $this);
            /** @noinspection PhpUnhandledExceptionInspection */
            throw new InternalException('Renderelési hiba', $ee, $e);
        }
        return $result;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @param string $xslfile
     *
     * @return DomDocument|string|null
     * @throws InternalException
     */
    function process_xml($xslfile) {
        $result = $this->process($xslfile, null, true);
        if ($result) return $result; //@$result->saveXML();
        else return null;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @param string $xslfile
     *
     * @return string
     * @throws InternalException
     */
    function process_html($xslfile) {
        $result = $this->process($xslfile);
        return $result->saveHTML();
    }

    /**
     * Creates and inserts a new text content
     *
     * @param UXMLElement $node -- node to insert into
     * @param string $cont
     * @return UXMLElement|DOMNode -- the inserted text node
     * @throws InternalException
     * @deprecated
     */
    static function addText($node, $cont) {
        return $node->addText($cont);
    }

    /**
     * Cretaes or replaces a text content in the node
     * (Deletes all redundant text nodes)
     *
     * @param UXMLElement $node
     * @param string $cont
     * @return DOMNode -- the original node
     * @deprecated
     */
    static function replaceText($node, $cont) {
        return $node->replaceText($cont);
    }

    /**
     * Creates a new node with name and plain text content
     *
     * @param DOMElement|UXMLElement $node -- the parent node
     * @param string $name -- the tagname of teh new node
     * @param string $cont -- the text content of the new node
     * @param string|array $namespace -- simple namespace or namespace array
     *
     * @return UXMLElement|DOMNode -- the new node created (Always UXMLElement)
     * @throws InternalException
     * @throws ConfigurationException
     * @deprecated
     */
    static function addTextNode($node, $name, $cont, $namespace = null) {
        return $node->addTextNode($name, $cont, $namespace);
    }

    /**
     * Creates a new bode with given XML content from string
     *
     * @param UXMLElement $node -- the parent node
     * @param string $name -- the tagname of teh new node
     * @param string $cont -- the XML content in a string
     *
     * @return UXMLElement -- the new node created
     * @throws InternalException
     * @deprecated
     */
    static function addHypertextNode($node, $name, $cont) {
        return $node->addHypertextNode($name, $cont);
    }

    /**
     * ## Adds XML content to the node from a string
     *
     * If string is not valid XML, a text node will be created
     * If string starts with <?xml, a complete XML document is assumed, and root node may be omitted
     * Otherwise the string is decoded as xml fragment, and root node cannot be omitted.
     *
     * @param UXMLElement $node - parent node to add to
     * @param string $cont - string xml content to insert. May be xml document (starting with <?xml) or xml fragment (starting with '<')
     * @param bool $strict -- only valid XML content is accepted (Default: invalid content is converted to text, with a (*) appended.)
     * @param bool $root -- include root node of xml (not used if xml fragment is provided)
     * @param string $namespace -- insert/replace default namespace. Does not affect declared prefixed namespaces. Default is none, leaves declared
     *
     * @return UXMLElement|DOMNode - (first) inserted node (may be text node)
     * @throws InternalException
     * @deprecated
     */
    static function addHypertextContent($node, $cont, $strict = false, $root = false, $namespace = null) {
        return $node->addHypertextContent($cont, $strict, $root, $namespace);
    }

    /**
     * Adds XML subtree from DOMElement or DOMDocument or xml string
     *
     * XML document or xml fragment is supported.
     *
     * @param UXMLElement $node_parent - parent node to add to
     * @param string|DOMElement $cont - xml content to insert
     * @param boolean $omitroot - omits root node of xml document
     * @return UXMLElement|DOMNode - the (first) inserted node.
     * @throws InternalException on invalid XML string
     * @deprecated
     */
    static function addXmlContent($node_parent, $cont, $omitroot = false) {
        return $node_parent->addXmlContent($cont, $omitroot);
    }

    /**
     * Megkeresi egy node alfájában az xpath által megadott node-ot.
     *
     * @param UXMLElement $node_parent
     * @param string $query -- Az xpath kifejezésnek node-ra kell hivatkoznia!
     * @param array $namespaces -- array of alias=>url items
     *
     * @return DOMNode -- Több node találat esetén az első node referenciájával tér vissza. Ha nincs meg, null reference (remélem)
     * @throws InternalException
     * @deprecated
     */
    static function selectSingleNode($node_parent, $query, $namespaces = null) {
        return $node_parent->selectSingleNode($query, $namespaces);
    }

    /**
     * Megkeresi egy node alfájában az xpath által megadott node-okat.
     *
     * @param UXMLElement $node_parent
     * @param string $query -- Az xpath kifejezésnek node-ra kell hivatkoznia!
     * @param array $namespaces -- array of alias=>url items
     * @return DOMNodeList
     * @deprecated use the similar method in UXMLElement
     */
    static function selectNodes($node_parent, $query, $namespaces = null) {
        return $node_parent->selectNodes($query, $namespaces);
    }

    /**
     * Kiértékel egy xpath által megadott kifejezést
     *
     * @param UXMLElement $node_parent
     * @param string $query -- Az xpath kifejezésnek node-ra kell hivatkoznia!
     * @param array $namespaces -- array of alias=>url items
     * @return mixed
     * @throws InternalException
     * @deprecated
     */
    static function evaluate($node_parent, $query, $namespaces = null) {
        return $node_parent->evaluate($query, $namespaces);
    }

    /**
     * Adds one node and a subtree to the node defined in array
     * If content is not valid xml, it will be inserted as simple string content
     *
     * @param UXMLElement $node -- parent to append under
     * @param string $name -- name of the new node
     * @param string $cont -- XML content or non-xml content
     * @return UXMLElement -- the original node
     * @throws InternalException
     * @deprecated
     */
    static function addSubtree($node, $name, $cont) {
        return $node->addSubtree($name, $cont);
    }

    /**
     * Adds text content or subnodes to a node.
     * Recursive
     *
     * @param UXMLElement $node
     * @param array|string $node_list
     * - string: text content
     * - array: array of subnodes as [[nodename, attributes, subnodelist],...]
     *
     * @return UXMLElement első hozzáadott node
     * @deprecated
     */
    static function addNodeList($node, $node_list) {
        /** @noinspection PhpDeprecationInspection */
        return $node->addNodeList($node_list);
    }

    /**
     * Returns the first named subnode or creates if not exists
     *
     * @param UXMLElement $parent
     * @param string $nodename
     *
     * @return UXMLElement first node with this name if exists or created node if not
     * @deprecated
     */
    static function getOrCreateElement($parent, $nodename) {
        return $parent->getOrCreateElement($nodename);
    }

    /**
     * Sets attributes for bit values of the value by a given bitmap definition
     *
     * @param UXMLElement $node -- the target node to insert attributes into
     * @param array $bitarray [[value, name, descr, const],...] eg. bitmap definition
     * @param int $value -- the value to represent by bit-attributes
     * @param int $field index of name column in bitarray
     * @param string $prefix -- attribute name prefix to attached
     *
     * @return UXMLElement -- the node itself
     * @throws InternalException
     * @deprecated
     */
    static function setNodeBits($node, $bitarray, $value, $field = 3, $prefix = '') {
        return $node->setNodeBits($bitarray, $value, $field, $prefix);
    }

    /**
     * Sets attributes for bit values on all child nodes.
     * Skips nodes where valueattr is not found.
     * @param UXMLElement $node_parent
     * @param array $bitarray [[value, name, descr, const],...] eg. bitmap definition
     * @param string $valueattr -- the attribute holding value to represent by bit-attributes
     * @param int $field index of name column in bitarray
     *
     * @return UXMLElement -- the parent node itself
     * @throws InternalException
     * @see setNodeBits
     * @deprecated
     */
    static function setChildNodeBits($node_parent, $bitarray, $valueattr, $field = 3) {
        return $node_parent->setChildNodeBits($bitarray, $valueattr, $field);
    }

    /**
     * Creates nodes for all bits
     *
     * @param UXMLElement $node
     * @param array $bitarray [bit=>[value, name, descr, const],...]
     * @param string $nodename
     * @param int $json
     *
     * @return UXMLElement -- the node
     * @throws ConfigurationException
     * @throws InternalException
     * @throws UAppException
     * @deprecated
     */
    static function loadBitNodes($node, $bitarray, $nodename, $json = 0) {
        return $node->loadBitNodes($bitarray, $nodename, $json);
    }

    /**
     * Add attributes to a node from SQL
     *
     * @param DBX $db
     * @param UXMLElement $node
     * @param string $sql
     * @param array $params
     *
     * @return UXMLElement
     * @throws DatabaseException
     * @throws InternalException
     * @deprecated
     */
    static function setQueryAttributes($db, $node, $sql, $params = null) {
        return $node->setQueryAttributes($db, $sql, $params);
    }

    /**
     * Removes all children of the node
     *
     * @param UXMLElement $node
     * @return UXMLElement -- the original node
     * @deprecated
     */
    static function removeChildren($node) {
        return $node->removeChildren();
    }

    /**
     * Clones a node replacing namespace to specified (or removed)
     *
     * This function is based on a [comment to the PHP documentation](http://www.php.net/manual/de/domnode.clonenode.php#90559)
     *
     * @param DOMElement $node
     * @param string $namespace
     *
     * @return DOMElement
     * @throws DOMException
     */
    function cloneNodeNS($node, $namespace = null) {
        $unprefixedName = preg_replace('/.*:/', '', $node->nodeName);
        if ($namespace)
            $nd = $this->createElementNS($namespace, $unprefixedName);
        else
            $nd = $this->createElement($unprefixedName);

        foreach ($node->attributes as $value)
            $nd->setAttribute($value->nodeName, $value->value);

        if (!$node->childNodes)
            return $nd;

        foreach ($node->childNodes as $child) {
            if ($child->nodeName == "#text")
                $nd->appendChild($this->createTextNode($child->nodeValue));
            else
                $nd->appendChild($this->cloneNodeNS($child, $namespace));
        }
        return $nd;
    }

    /**
     * @param UXMLElement $node
     * @param string $alias
     * @param string $namespace
     *
     * @return UXMLElement
     * @deprecated
     */
    static function addNamespaceDeclaration($node, $alias, $namespace) {
        return $node->addNamespaceDeclaration($alias, $namespace);
    }

    /**
     * Creates a new document from xml fragment, using a new <root> node.
     *
     * Replaces uxml_open_fragment()
     *
     * @param string $xmlstring
     * @param string $enc
     * @param bool $strict -- if false (default), returns text node if xmlstring is invalid xml
     *
     * @return UXMLDoc
     * @throws InternalException|DOMException
     */
    static function createFragment($xmlstring, $enc = 'UTF-8', $strict = false) {
        if (!is_string($xmlstring)) throw new DOMException('string was expected');
        $dom = new UXMLDoc($enc, 'root');
        $dom->preserveWhiteSpace = false;
        $f = $dom->createDocumentFragment();
        $r = $f->appendXML($xmlstring);
        if ($r) $dom->documentElement->appendChild($f);
        else if ($strict) return null; else $dom->documentElement->addText($xmlstring);
        return $dom;
    }

    /**
     * Creates a new document from valid xml
     *
     * @param string $xmlstring
     * @param string $enc
     *
     * @return UXMLDoc
     * @throws DOMException
     */
    static function createDocument($xmlstring, $enc = 'UTF-8') {
        $dom = new UXMLDoc($enc, false);
        $dom->preserveWhiteSpace = false;
        $f = $dom->loadXML($xmlstring);
        if (!$f) throw new DOMException('Invalid xml');
        return $dom;
    }

    public function getXml() {
        return $this->saveXML();
    }

    /**
     * Returns the value of a property.
     *
     * @param string $name the property name
     *
     * @return mixed the property value
     * @throws InternalException if the property is not defined or the property is write-only.
     * @see          __set()
     * @noinspection DuplicatedCode
     */
    public function __get($name) {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        }

        if (method_exists($this, 'set' . $name)) {
            throw new InternalException('Getting write-only property: ' . get_class($this) . '::' . $name);
        }

        throw new InternalException('Getting unknown property: ' . get_class($this) . '::' . $name);
    }

    /**
     * Creates a new, equivalent copy of the document with given nodename prefixes.
     * Prefixes of not listed namespaces are preserved.
     *
     * @param array $prefixes -- [namespace => prefix, ...]
     * @return UXMLDoc
     * @throws ConfigurationException
     * @throws DOMException
     * @throws InternalException
     */
    public function canonizePrefixes($prefixes = null) {
        $result = new UXMLDoc('UTF-8', null);
        $result->appendChild($result->canonizedCopy($this->documentElement, $prefixes));
        return $result;
    }

    /**
     * Creates a new, equivalent copy of the given element using given nodename prefixes.
     * Prefixes of not listed namespaces are preserved.
     *
     * @param UXMLElement $source -- original element with arbitrary prefix
     * @param array $prefixes -- [namespace => prefix, ...]
     * @return UXMLElement
     * @throws ConfigurationException
     * @throws DOMException
     * @throws InternalException
     */
    public function canonizedCopy($source, $prefixes = null) {
        $nodeName = $source->nodeName;
        $nameSpace = $source->namespaceURI;
        if ($prefixes && array_key_exists($nameSpace, $prefixes) && $prefixes[$nameSpace] != $source->prefix) {
            $nodeName = $prefixes[$nameSpace] . ':' . $source->localName;
        }
        $result = $this->createElementNS($nameSpace, $nodeName);
        // attributes
        foreach ($source->attributes as $attribute) {
            /** @var DOMAttr $attribute */
            $result->addAttribute($attribute->name, $attribute->value, $attribute->namespaceURI);
        }
        // Recurse children, including text nodes in the original order
        foreach ($source->childNodes as $childNode) {
            /** @var DOMNode $childNode */
            if ($childNode->nodeType == XML_ELEMENT_NODE) {
                /** @var UXMLElement $childNode */
                $result->appendChild($this->canonizedCopy($childNode, $prefixes));
            } else {
                $result->appendChild($this->importNode($childNode, false));
            }
        }

        return $result;
    }


}
