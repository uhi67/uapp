<?xml version="1.0" encoding="UTF-8"?>
<!-- message.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="xml" version="1.0" indent="yes"
	            doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	            doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	            omit-xml-declaration="yes"
	/>

	<xsl:include href="../modules/Main/main.xsl"/>

	<xsl:template match="data">
		<!-- Az üzenetek a head.inc.xsl-ben már megjönnek -->
		<xsl:apply-templates select="url"/>
	</xsl:template>

	<xsl:template match="h3">
		<h3>
			<xsl:value-of select="text()" disable-output-escaping="yes"/>
		</h3>
	</xsl:template>

	<xsl:template match="url">
		<p>
			<xsl:call-template name="button">
				<xsl:with-param name="caption" select="'Tovább'"/>
				<xsl:with-param name="click">document.location='<xsl:value-of select="text()"/>'
				</xsl:with-param>
			</xsl:call-template>
		</p>
	</xsl:template>

</xsl:stylesheet>
