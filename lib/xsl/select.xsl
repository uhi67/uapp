<?xml version="1.0" encoding="UTF-8"?>
<!-- select.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="xml" version="1.0" indent="yes"
	            doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	            doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	            omit-xml-declaration="yes"
	/>

	<xsl:include href="../modules/Main/main.xsl"/>
	<!--<xsl:include href="../modules/pte-layout/pte_popup.xsl" />-->
	<xsl:include href="../modules/UList/UList.xsl"/>

	<xsl:variable name="submitx">
		<xsl:choose>
			<xsl:when test="$control/submit/text()=1">_submit</xsl:when>
			<xsl:when test="$control/submit/text()=0"></xsl:when>
			<xsl:when test="$control/submit/text()!=''">_action</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="submity">
		<xsl:choose>
			<xsl:when test="$control/submit/text()=1"></xsl:when>
			<xsl:when test="$control/submit/text()=0"></xsl:when>
			<xsl:when test="$control/submit/text()!=''">, '<xsl:value-of select="$control/submit/text()"/>'
			</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:template match="content">
		<div class="abc">
			<xsl:call-template name="abc"/>
		</div>
		<i>A kiválasztáshoz kattintson a megfelelő névre.</i>
		<xsl:if test="/data/control/search/@p!=''">
			[<a href="select.php?id={@id}&amp;s={/data/@submit}">Teljes lista</a>]
		</xsl:if>
		<xsl:apply-templates select="list">
			<xsl:with-param name="with-navigator" select="1"/>
			<xsl:with-param name="width" select="600"/>
		</xsl:apply-templates>
		<xsl:if test="not(item)">
			<p>
				<em>Üres lista, nincs kiválasztható tétel.</em>
			</p>
		</xsl:if>
	</xsl:template>

	<xsl:template name="abc">
		<xsl:param name="letter" select="'A'"/>
		<xsl:param name="remain" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
		<xsl:choose>
			<xsl:when test="concat('^', $letter)=/data/control/search/@pattern">
				<b>
					<xsl:value-of select="$letter"/>
				</b>
			</xsl:when>
			<xsl:otherwise>
				<a href="select_person.php?id={@id}&amp;f={/data/control/params/@f}&amp;pattern=^{$letter}&amp;s={/data/@submit}">
					<xsl:value-of select="$letter"/>
				</a>
			</xsl:otherwise>
		</xsl:choose>
		&#160;
		<xsl:if test="string-length($remain)!=0">
			<xsl:call-template name="abc">
				<xsl:with-param name="letter" select="substring($remain,1,1)"/>
				<xsl:with-param name="remain" select="substring($remain,2)"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<xsl:template match="item" mode="list-item">
		<tr>
			<td>
				<a class="nodot" href="#" onclick="ret{$submitx}({@id}, '{@name}'{$submity})">
					<xsl:value-of select="@id"/>
				</a>
			</td>
			<td nowrap="nowrap">
				<a href="javascript:ret{$submitx}('{@id}', '{@name}'{$submity});">
					<xsl:value-of select="@name"/>
				</a>
			</td>
			<td>
				<xsl:value-of select="@descr"/>
			</td>
		</tr>
	</xsl:template>

	<xsl:template match="list" mode="in-form">
		<input type="hidden" name="id" value="{/data/control/params/@id}"/>
	</xsl:template>

</xsl:stylesheet>
