<?xml version="1.0" encoding="UTF-8"?>
<!-- redirect.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="xml" version="1.0" indent="yes"
	            doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	            doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	            omit-xml-declaration="yes"
	/>

	<xsl:template match="/">
		<html>
			<head>
				<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
			</head>
			<body>
				<xsl:if test="not(/data/head/redirect/@pause)">
					<script>
						location.href = '<xsl:value-of select="/data/head/redirect/text()"/>';
					</script>
				</xsl:if>
				<xsl:apply-templates select="/data/control/sendmsg"/>
				<h3>Átirányítás</h3>
				[<a href="{/data/head/redirect/text()}">Tovább</a>]
			</body>
		</html>
	</xsl:template>

	<xsl:template match="sendmsg">
		<div class="flash {@class}">
			<xsl:value-of select="title"/>
		</div>
	</xsl:template>

</xsl:stylesheet>
