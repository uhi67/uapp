<?xml version="1.0" encoding="UTF-8"?>
<!-- viewnotfound.xsl -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="xml" version="1.0" indent="yes"
	            doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	            doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	            omit-xml-declaration="yes"
	/>
	<xsl:include href="../modules/Main/main.xsl"/>
	<!--<xsl:include href="../modules/pte-layout/pte.xsl" />-->

	<!-- ================================================================================== -->
	<xsl:template match="content">
		<h1>Belső hiba</h1>
		<div align="center">Az xsl fájl nem található.</div>
		<div align="center">
			<em>
				<xsl:value-of select="/data/@xsl"/>
			</em>
		</div>
	</xsl:template>

</xsl:stylesheet>
