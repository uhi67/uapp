﻿<?xml version="1.0" encoding="ISO-8859-2"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/*">
		<div>
			<xsl:call-template name="syntax"/>
		</div>
	</xsl:template>

	<xsl:template name="syntax">
		<xsl:choose>
			<xsl:when test="name()=''">
				<xsl:value-of select="."/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>&lt;</xsl:text>
				<b>
					<xsl:value-of select="name()"/>
				</b>

				<!-- namespace -->
				<xsl:if test="namespace-uri()">
					<span class="namespace">xmlns="
						<i>
							<xsl:value-of select="namespace-uri()"/>
						</i>
						"
					</span>
					<xsl:text> </xsl:text>
				</xsl:if>
				<xsl:for-each select="@*">
					<xsl:text> </xsl:text>
					<span class="attr_name">
						<xsl:value-of select="name()"/>
					</span>
					="
					<span class="attr_value">
						<xsl:value-of select="."/>
					</span>
					<xsl:text>"</xsl:text>
				</xsl:for-each>
				<xsl:if test="count(*)=0 and count(text())=0">
					<xsl:text> /&gt;</xsl:text>
					<br/>
				</xsl:if>
				<xsl:if test="count(*)=0 and count(text())>0">
					<xsl:text>&gt;</xsl:text>
					<xsl:value-of select="text()"/>
					<span class="tag_close">
						<xsl:text>&lt;/</xsl:text>
						<xsl:value-of select="name()"/>
						<xsl:text>&gt;</xsl:text>
						<br/>
					</span>
				</xsl:if>
				<xsl:if test="count(*)>0">
					<xsl:text>&gt;</xsl:text>
					<dl>
						<dd>
							<xsl:for-each select="node()">
								<xsl:call-template name="syntax"/>
							</xsl:for-each>
						</dd>
					</dl>
					<span class="tag_close">
						<xsl:text>&lt;/</xsl:text>
						<xsl:value-of select="name()"/>
						<xsl:text>&gt;</xsl:text>
						<br/>
					</span>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
