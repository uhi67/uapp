<?php /** @noinspection PhpUnused */

/**
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 *
 */
class QueryIterator extends Component implements Iterator {
    /** @var Query $query */
    public $query;
    /** @var resource $rs */
    public $rs = false;
    /** @var int $length -- number of result rows */
    public $length = null;
    /** @var array|Model $current */
    public $current = 0; // null=before first, false: after the last
    /** @var string|boolean -- use moodel (true = query's model) */
    public $modelClass = false;

    /**
     * @throws QueryException
     */
    public function prepare() {
        parent::prepare();
        if (!$this->query) throw new QueryException('QueryIterator: the query is not specified');
        if (!$this->query->connection) throw new QueryException('QueryIterator: the query has no connection');
        if (!in_array($this->query->type, ['select', 'sql'])) throw new QueryException('QueryIterator: the query is not a select');
    }

    /**
     * Return the current element
     *
     * @link https://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type. Returns null if position is out of bounds.
     * @since 5.0.0
     * @throws
     */
    public function current() {
        if ($this->current === false) return null;
        if ($this->current === null) $this->current = 0;

        if (!$this->initResult()) return null;
        if ($this->current >= $this->length) return null;

        $data = $this->query->connection->fetch_assoc($this->rs, $this->current);
        if (($this->modelClass === true) && $this->query->modelname) $this->modelClass = $this->query->modelname;
        if (!$this->modelClass) return $data;
        if (is_a($this->modelClass, BaseModel::class, true)) {
            $attributes = call_user_func([$this->modelClass, 'attributes'], $this->query->connection);
            $data1 = [];
            foreach ($attributes as $a) {
                $data1[$a] = ArrayUtils::getValue($data, $a);
            }
            /** @var Model $model */
            $model = new $this->modelClass($data1, $this->query->connection);
            $model->setOldAttributes($data1);
            return $model;
        }
        return $data;
    }

    /**
     * Move forward to next element
     *
     * @link https://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     * @throws
     */
    public function next() {
        if ($this->current === false) return;
        if ($this->current === null) $this->current = 0;
        else $this->current++;
        if (!$this->initResult()) return;
        if ($this->current >= $this->length) $this->current = false;
    }

    /**
     * Return the key of the current element
     *
     * @link https://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key() {
        return $this->current;
    }

    /**
     * Checks if current position is valid
     *
     * @link https://php.net/manual/en/iterator.valid.php
     * @return bool The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid() {
        return $this->current !== null && $this->current !== false;
    }

    /**
     * Rewind the Iterator to the first element
     *
     * @link https://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     * @throws
     */
    public function rewind() {
        $this->current = 0;
        if (!$this->initResult()) return;
        if ($this->current >= $this->length) $this->current = false;
    }

    /**
     * Initializes `rs` and `length`
     *
     * @return bool -- if resultset and length is valid
     * @throws DatabaseException
     * @throws QueryException -- if length query failed
     */
    private function initResult() {
        if (!$this->rs) $this->rs = $this->query->connection->query($this->query->sql, $this->query->params);
        if (!$this->rs) {
            $this->current = false;
            return false;
        }
        $this->length = $this->query->connection->num_rows($this->rs);
        if ($this->length < 0) throw new QueryException('QueryIterator result length error', $this->query->sql);
        return true;
    }
}
