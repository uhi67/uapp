<?php
/**
 * class StaticComponent
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * A component without instance, using only static methods.
 */
class StaticComponent {

    /**
     * Initializes static properties from config.
     *
     * @param array $config
     *
     * @throws InternalException
     */
    public static function init($config = null) {
        if ($config) {
            if (!is_array($config)) throw new InternalException('Configuration error at component ' . get_called_class() . ': ' . Util::objtostr($config));
            foreach ($config as $key => $value) {
                if ($key == 'class') continue;
                /** @noinspection PhpVariableVariableInspection */
                static::$$key = $value;
            }
        }
        static::prepare();
    }

    /**
     * After initializing static properties, this method may prepare the class.
     */
    public static function prepare() {
    }

}
