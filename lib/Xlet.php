<?php /** @noinspection PhpUnused */
/**
 * class XLet
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * Xlet is a gadget based on a Module, which can have instances represented by a DOM Element (a "node")
 *
 * @property UXMLElement $node -- The first created node representing this gadget in the XML document
 * @property-read  UXMLElement $lastNode -- The last created node of the Xlet instance
 */
abstract class Xlet extends Module {
    /** @var UXMLElement $_node */
    private $_node = null;
    /** @var UXMLElement $_lastnode */
    private $_lastnode = null;

    /**
     * Registers the created node in the Xlet
     *
     * @param UXMLElement $node the currently created node
     * @return UXMLElement the first created node
     */
    protected function setNode($node) {
        if (!$this->_node) $this->_node = $node;
        $this->_lastnode = $node;
        return $this->_node;
    }

    /**
     * The first created node of the Xlet instance
     *
     * @return UXMLElement
     */
    public function getNode() {
        return $this->_node;
    }

    /**
     * The last created node of the Xlet instance
     *
     * @return DOMElement
     */
    public function getLastNode() {
        return $this->_lastnode;
    }

    /**
     * ## Determines the real parent node based on object class.
     * ### Supports
     * - UAppPage: uses node_content
     * - Xlet: uses first generated node
     *
     * @param UXMLElement|Xlet|UAppPage|null $node_parent
     * @return UXMLElement|DOMNode
     * @throws InternalException -- if invalid class given or refernced node is missing
     */
    public function parentNode($node_parent) {
        if (!$node_parent && $this->page) return $this->page->node_content;
        if ($node_parent instanceof UAppPage) $node_parent = $node_parent->node_content;
        elseif ($node_parent instanceof Xlet) $node_parent = $node_parent->node;
        elseif ($node_parent instanceof BaseModel) $node_parent = $node_parent->node;
        else Util::assertClass($node_parent, DOMNode::class);
        return $node_parent;
    }

    /**
     * Generates subnodes under this based on Query
     *
     * @param string $nodename
     * @param Query $query
     *
     * @return UXMLElement -- the first generated subnode
     * @throws DatabaseException
     * @throws Exception
     * @throws InternalException
     * @throws QueryException
     */
    public function query($nodename, $query) {
        if (!$this->_node) throw new Exception('Xlet has no node yet.');
        if (!$query->connection) $query->connection = $this->page->db;
        return $query->nodes($this->_node, $nodename, $query->connection);
    }

	/**
	 * Generates a new node under this. Equivalent to {@see UXMLElement::addNode()}
	 *
	 * @param string $nodename
	 * @param mixed $attributes -- universal content data (see {@see UXMLElement::addContent()})
	 * @param mixed $content -- universal content data
	 * @param string $namespace
	 *
	 * @return UXMLElement
	 * @throws Exception
	 */
   	public function addNode($nodename, $attributes, $content = null, $namespace = null) {
		if(!$this->_node) throw new Exception('Xlet has no node yet.');
   		return $this->_node->addNode($nodename, $attributes, $content, $namespace);
   	}

	/**
	 * createNode() method must create a new node of the XLet under the parent, and register it with setNode()
	 *
	 * @param UXMLElement|Xlet|UAppPage $parent
	 * @param array|null $options
	 *
	 * @return UXMLElement
	 */
   	abstract public function createNode($parent=null, $options=null);
}
