<?php
/**
 * @author uhi
 * @copyright 2022
 * @abstract
 * This is the main router/dispatcher of UApp framework.
 * It also responsible for including libraries and serving assets.
 * In the framework, this file can be found at /lib/_index.php
 * Please copy this file to the into application's web-root directory as 'index.php'
 * Redirect to this file in .htaccess if file not found
 * You can also copy this file into the root dir of the application with name 'app', and use it for invoke app's cli commands: app <command-name>
 *
 * This file must not be in .gitignore
 */

$appdir = dirname(__DIR__);
$configFile = $appdir . '/config/config.php';
if (!file_exists($configFile)) {
    echo "Internal error: Main configuration file is missing.";
    exit;
}
require $appdir . '/vendor/autoload.php';
$config = require $configFile;    // path to config
/** @noinspection PhpIncludeInspection */
require $appdir . '/vendor/uhi67/uapp/lib/_uapp.php';
