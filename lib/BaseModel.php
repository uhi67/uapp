<?php /** @noinspection PhpUnused */
/**
 * class BaseModel
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * Simple data model without database
 *
 * Features:
 * - validation rules
 * - attribute labels and hints
 * - attribute types
 * - fields
 * - toArray()
 *
 * @property array $attributes
 * @property-read UXMLElement $node
 * @property-read array $errorNodes -- error data for UXMLPage::addContent() -- [['error', ['field'=>fieldname], error_message]]
 * @property-read array $errors  -- validation errors associated to field names:
 * @property-read array $errorValues  -- fieldname indexed array of numeric indexed errors and 'value'=> value
 * @property-read array $friendlyErrors --  array of fieldname => errorstring. Friendly error texts contain field labels.
 * @property-read string $firstError --  First friendly error texts (contains field label).
 */
abstract class BaseModel extends Component {
    const VALID_EMAIL = '/^\w+[\w\-\.]*\@\w+[\w\-\.]+$/';
    #const VALID_URL = '/^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i';
    #const VALID_URL = '/https?:\/\/[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/';
    const VALID_URL = '/^(https?|ftp):\/\/[^\s\/$.?#].[^\s]*$/iS'; // stephenhay
    #const VALID_URL = '/(?i)\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))/iS'; // gruber v2

    /**
     * @var array $errors -- validation errors associated to field names:
     * `fieldname => array(message)`
     * Messages are sentence fragments: "Fieldname $fragment."
     * E.g.: 'is mandatory'.
     * validate() empties this at the beginning
     */
    protected $_errors = [];

    /** @var UXMLElement $_node -- first created node in the output DOM document */
    protected $_node = null;

    /**
     * Must return the list of all attribute names of the model.
     * Default is all public properties
     *
     * @return array list of attribute names.
     * @throws ReflectionException
     */
    static function attributes() {
        return static::publicProperties();
    }

    /**
     * Must return the list of all attribute types of the model (name=>type)
     *
     * ### Valid type names:
     *
     *  - basic php types (lowercase) Default is 'string'
     *  - 'xml' denotes xml valid string
     *  - php class names (Uppercase, eg. DateTime)
     *  - date (denotes DateTime without time)
     *  - time (denotes DateTime without date or integer in sec)
     *
     * @param DBX|null $connection [optional] -- only for compatibility with Model
     *
     * @return array|void -- list of attribute name=>type associations
     * @throws ModelException
     * @codeCoverageIgnore
     */
    public static function attributeTypes($connection = null) {
        throw new ModelException('attributeTypes() is not implemented.', get_called_class());
    }

    /**
     * ## BaseModel::rules()
     * Must return validation rules for fields.
     *
     * Models may be validated with {@see validate()} method
     *
     * ### Member of rules may be:
     *
     * - rule -- numeric indexed rules are global rule, e.g. unique for multiple fields
     * - fieldname => array(rule, rule...) -- fieldname indexed rules are for single field
     *
     * Where rule is:
     *
     * - rulename
     * - array(rulename, arguments, ...)
     *
     * Rulename always refers to method `rulenameValidate`.
     *
     * ### Rules in forms
     *
     * - rulename will be added to the field class as `rule-rulename`
     * - Arguments will be added to field as data-rulename-data (always an array, for all remaining arguments)
     * - Associative arguments 'key'=>value will be used in php validator method as normal argument,
     * but in forms will be added as data-rulename-key and excluded from data-rulename.
     *
     * Some validators may change values during validation. Some of them even passes always.
     *
     * ### Predefined rules:
     *
     * - 'mandatory' -- field is not null and not empty string
     * - 'number' -- passes any numeric valu, excluding boolean
     * - 'notNull' -- fails only if null
     * - 'empty' -- fails if not empty (false is not empty)
     * - ['length', min, max]
     * - ['pattern', pattern(s)] -- valid if at least one of RE patterns is valid (second level: all of them)
     * - ['type', typename]
     * - ['between', lower, upper] -- valid value between (inluding) limits
     * - [custom, arglist] -- any custom name refers to validateCustom($fieldname, array $arglist) method.
     * - ['if', fieldname2, refvalue, rule, ...] -- conditional validation (generates meta-error)
     * - ['if2', fieldname2, refvalue, rule, ...] -- conditional validation (does not generate meta-error)
     * - ['cond', condition, rules] -- conditional validation
     * - 'xml' -- valid if string is a valid XML fragment or DOMDocument or DOMNode
     * - 'email' -- string pattern validation
     * - 'url' -- string pattern validation
     *
     * ### Value conversion rules
     *
     * - ['default', value] -- always passes, replaces null value to default.
     * - 'defaultNow' -- for date/time fields, always passes, replaces null value to current timestamp.
     * - 'lowercase' -- converts to lowercase if (convertable to) string, fails otherwise
     * - 'trim' -- trims whitespaces if (convertable to) string, fails otherwise
     * - 'string' -- converts to string all scalars, otherwise fails
     * - 'boolean' -- converts to boolean if possible, otherwise fails
     * - 'integer' -- converts to integer if possible, otherwise fails
     * - 'int' -- converts to string-represented integer
     * - 'float' -- converts to float if possible, otherwise fails
     * - 'file' -- passes FileUpload instances
     * - 'macaddr', 'inet', 'datetime', 'date', 'time',
     * - 'timestamp' -- like datetime, but removes time zone
     * - ['array', 'pattern'] -- explodes string to array
     * - 'bitarray' -- converts array to integer [1=>1, 2=>0, 4=>4] => 5
     *
     *
     * ### Signature of `rulenameValidate` functions:
     *
     * - must accept parameters (fieldname, arg1, arg2, ...) where fieldname is null on global calls.
     * - parameters in rule definition after rulename will be passed as arg1, arg2.
     * - must return boolean
     * - may set error text of the invalid field on the form by setError(fieldname, message)
     *
     * ### Global rules
     * If a rule is called as global rule, the first (fieldname) argument will be null, any other arguments
     * defined in rule after rulename will be passed from the second position.
     * In this way you may use the same validator method as gloabal as well as field validator.
     *
     * For predefided database-related rules see {@see Model::rules()}
     *
     * @return array
     * @see validate()
     * @codeCoverageIgnore
     */
    public static function rules() {
        return [];
    }

    /**
     * ## Validates the model data.
     * Invalid data returns false and errors will contain the errors in form field=>errors array.
     *
     * Default validates against user defined rules().
     * If you override it, and want to use rules as well, don't forget to call parent::validate()
     *
     * For rules of rule definitions, see {@see rules()}
     *
     * @param array $attributeNames -- if an array is specified, validates only given fields (ignores unknown fieldnames)
     * @return boolean -- true if data is valid and may saved to the database.
     * @throws InternalException
     * @throws ModelException
     * @throws UAppException
     */
    public function validate($attributeNames = null) {
        $this->_errors = [];
        $valid = true;
        $rules = static::rules();
        foreach ($rules as $field => $def) {
            if ($def === null) continue; // Overridden rule may be null
            if (is_numeric($field)) {
                if ($attributeNames) continue;
                // Global rule
                #Debug::tracex('validating global rule ', $def);
                $rulename = is_array($def) ? array_shift($def) : $def;
                Util::assertString($rulename);
                $args = is_array($def) ? $def : [];
                $functionname = Util::camelize($rulename) . 'Validate';
                if (!is_callable([$this, $functionname])) throw new UAppException("Validator function `$functionname` is missing");
                if (!call_user_func_array([$this, $functionname], array_merge([null], $args))) {
                    Debug::tracex('Global validate failed', $functionname, '#900');
                    $valid = false;
                }
            } else if (!$attributeNames || is_array($attributeNames) && in_array($field, $attributeNames)) {
                $valid = $this->validateRules($field, $def) && $valid; // Order is important!
            }
        }
        return $valid;
    }

    /**
     * Validates the record against multiple rules
     *
     * def format is:
     *
     *      ['rulename', 'rulename'],
     *      [['rulename', args...], ...]
     *
     *
     * @param string $fieldName
     * @param array $def -- array of multiple rules to validate against
     *
     * @return bool
     * @throws InternalException
     * @throws ModelException
     */
    public function validateRules($fieldName, $def) {
        $valid = true;
        Util::assertArray($def);
        foreach ($def as $rule) {
            $rulename = is_array($rule) ? $rule[0] : $rule;
            try {
                $valid = ($valid1 = $this->validateRule($fieldName, $rule)) && $valid;
            } // @codeCoverageIgnoreStart
            catch (Exception $e) {
                throw new ModelException('Invalid rule `' . $rulename . '`: ' . $e->getMessage() . ' in file ' . $e->getFile() . ' at line ' . $e->getLine(), $rule, $e);
            }
            // @codeCoverageIgnoreEnd
            if (!$valid1) {
                Debug::tracex('Validate failed', $rule, '#900');
                #Debug::tracex('Value', $this->$fieldName, '#850');
            }
            $rulename = is_array($rule) ? array_shift($rule) : $rule;
            if (!$valid1 && $rulename == 'mandatory') break; // If mandatory failed, no more check.
        }
        return $valid;
    }

    /**
     * Validates a field against a rule definition
     *
     * @param string $fieldname
     * @param string|array $rule -- rulename or array(rulename, params...)
     *
     * @return bool
     * @throws UAppException
     */
    public function validateRule($fieldname, $rule) {
        #Debug::tracex('validating field '.$field, $rule);
        $rulename = is_array($rule) ? array_shift($rule) : $rule;
        $functionname = Util::camelize($rulename) . 'Validate';
        if (!ctype_alnum($functionname)) throw new UAppException("Invalid validator rule: `$rulename`.");
        $args = is_array($rule) ? $rule : [];
        if (!is_callable([$this, $functionname])) throw new UAppException("Validator function `$functionname` is missing");
        $params = array_merge([$fieldname], $args);
        try {
            if (!call_user_func_array([$this, $functionname], array_values($params))) {
                Debug::tracex($fieldname . ' validate failed', $functionname, '#900');
                return false;
            }
        } catch (Throwable $e) {
            throw new Exception("Invalid rule '$functionname' at field '$fieldname' of " . get_called_class(), 0, $e);
        }
        return true;
    }

    /**
     * Inserts fieldname and it's error message into $errors array.
     *
     * Markdown is supported, if message ends with '~'. Markdown is processed at client side by Showdown module.
     * Relative url-s are handled specially, best to use absolute urls
     *
     * @param string|array $fieldname -- the name of field or multiple fieldnames
     * @param string $message ($1 is a placeholder for fieldname)
     *
     * @return false -- always
     */
    public function setError($fieldname, $message) {
        if (is_array($fieldname)) {
            $field = array_shift($fieldname);
            if (count($fieldname)) {
                $db = $this;
                $fieldname = array_map(function ($f) use ($db) {
                    return $db->attributeLabel($f);
                }, $fieldname);
                $message = UApp::la('uapp', 'with {$fields} {$message}', ['fields' => implode(', ', $fieldname), 'message' => $message]);
            }
            $fieldname = $field;
        }
        $message = str_replace('$1', $fieldname, $message);
        if (!isset($this->_errors[$fieldname])) $this->_errors[$fieldname] = [];
        $this->_errors[$fieldname][] = $message;
        return false;
    }

    /**
     * Returns last error of fieldname. Null if none.
     *
     * @param string $fieldname
     * @param bool $remove
     *
     * @return string|null;
     */
    public function lastError($fieldname, $remove = false) {
        if (!isset($this->_errors[$fieldname])) return null;
        $last = count($this->_errors[$fieldname]);
        $err = $this->_errors[$fieldname][$last - 1];
        if ($remove) array_pop($this->_errors[$fieldname]);
        return $err;
    }

    /**
     * Converts empty string value of the field to NULL value.
     * Witespaces of string values are trimmed only if required.
     *
     * Writes back the value to the field.
     *
     * @param string $fieldname
     * @param bool $trim -- trim string value (default is false)
     *
     * @return mixed -- the value
     */
    public function emptyIsNull($fieldname, $trim = false) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if (is_string($value) && $trim) $value = trim($value);
        if ($value === '')
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = ($value = null);
        return $value;
    }

    /**
     * Validates field if condition returns true, skips to true otherwise
     *
     * Valid rules formats:
     *      'rulename'
     *      ['rulename, 'rulename', ...],
     *      [['rulename', args], ...],
     *
     * @param $fieldName
     * @param bool|callable $condition -- literal or function($model)
     * @param string|array $rules -- single rule name or array of multiple rules
     * @param string|array|null $rulesElse -- single rule name or array of multiple rules to run if condition is false
     *
     * @return bool
     * @throws InternalException
     * @throws ModelException
     */
    public function condValidate($fieldName, $condition, $rules, $rulesElse = null) {
        if (!is_scalar($condition)) {
            if (is_callable($condition)) $condition = $condition($this);
            else return $this->setError($fieldName, 'Invalid condition');
        }
        if ($condition) {
            if (!is_array($rules)) $rules = [$rules];
            return $this->validateRules($fieldName, $rules);
        } else {
            if (!$rulesElse) return true;
            return $this->validateRules($fieldName, $rulesElse);
        }
    }

    /**
     * ## Validates field data to php native data types and classes.
     * Null values always pass
     *
     * ### Supported types are:
     * - string
     * - integer
     * - double
     * - boolean
     * - array
     * - resource
     * - class names
     *
     * @param string $fieldname -- name of field
     * @param string $type -- common type (mapped from database by $conn->mapType())
     *
     * @return bool -- true is value is compatible to field type
     */
    public function typeValidate($fieldname, $type) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if (is_null($value)) return true;
        if (gettype($value) == $type) return true;
        if (is_object($value) && is_a($value, $type)) return true;
        $functionname = Util::camelize($type) . 'Validate';
        if (is_callable([$this, $functionname])) return $this->$functionname($fieldname);
        return $this->setError($fieldname, 'is not a ' . $type . ' object.');
        #throw new UAppException(sprintf('Validator %s does not exist.', $functionname));
    }

    /**
     * Validates an xml text or DOMDocument or DOMNode
     *
     * @param string $fieldname
     * @param bool $toString -- Converts DOM objects to xml string
     *
     * @return bool
     * @throws ModelException
     */
    public function xmlValidate($fieldname, $toString = true) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if ($value === null) return true;
        if (gettype($value) == 'string') {
            $doc = new DOMDocument();
            $r = @$doc->loadXML($value);
            if ($r) return true;
            return $this->setError($fieldname, UApp::la('uapp', 'is invalid XML string'));
        }
        if (is_object($value)) {
            $r = false;
            if (is_a($value, 'DOMDocument')) {
                if ($toString) $r = $value->saveXML();
            } else if (is_a($value, 'DOMNode')) {
                if (is_null($value->ownerDocument)) throw new ModelException($fieldname . 'is invalid XML data ');
                if ($toString) $r = $value->ownerDocument->saveXML($value);
            } else {
                throw new ModelException($fieldname . ' is invalid XML object');
            }
            if ($toString && !$r) {
                return $this->setError($fieldname, UApp::la('uapp', 'is invalid DOM Node'));
            }
            if ($toString) {
                /** @noinspection PhpVariableVariableInspection */
                $this->$fieldname = $r;
            }
            return true;
        }
        return $this->setError($fieldname, UApp::la('uapp', 'is not an XML'));
    }

    /**
     * Validates a string
     *
     * Fails if not scalar, otherwise converts value to string.
     *
     * @param string $fieldname
     *
     * @return bool
     */
    public function stringValidate($fieldname) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if ($value === null) return true;
        if (is_object($value) && is_callable([$value, 'toString'])) {
            $value = $value->toString();
        }
        if (!is_scalar($value)) return $this->setError($fieldname, UApp::la('uapp', 'is not a string'));
        if (!is_string($value))
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = (string)$value;
        return true;
    }

    /**
     * Validates a scalar or array length
     *
     * @param string $fieldname
     * @param int $minlength -- 0 or null to skip check
     * @param int $maxlength -- 0 or null to skip check
     *
     * @return bool
     * @throws InternalException
     */
    public function lengthValidate($fieldname, $minlength = -1, $maxlength = null, $message = null) {
        if ($minlength === -1) throw new ModelException('Missing min length argument in length validator rule');
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if ($value === null) return true;

        $longMessage = function () use ($message, $maxlength) {
            return $message ?: UApp::la('uapp', 'must not be exceed {$max} characters', ['max' => $maxlength]);
        };
        $shortMessage = function () use ($message, $minlength) {
            return $message ?: UApp::la('uapp', 'must not be shorter than {$min} characters', ['min' => $minlength]);
        };

        if (is_array($value)) {
            if ($maxlength && count($value) > $maxlength) return $this->setError($fieldname, $longMessage());
            if ($minlength && count($value) < $minlength) return $this->setError($fieldname, $shortMessage());
            return true;
        }

        if (!is_scalar($value)) return $this->setError($fieldname, UApp::la('uapp', '$1 is not a scalar'));
        if ($maxlength && strlen($value) > $maxlength) return $this->setError($fieldname, $longMessage());
        if ($minlength && strlen($value) < $minlength) return $this->setError($fieldname, $shortMessage());
        return true;
    }

    /**
     * @param string $fieldname
     *
     * @return bool|false
     */
    public function booleanValidate($fieldname) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if ($value === null) return true;
        if (is_bool($value)) return true;
        if (is_string($value)) $value = strtolower($value);
        if (in_array($value, [true, 1, -1, '1', '-1', 'true', 't', 'y'], true)) {
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = true;
            return true;
        }
        if (in_array($value, [false, 0, '0', 'false', 'f', 'n'], true)) {
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = false;
            return true;
        }
        return $this->setError($fieldname, UApp::la('uapp', 'is invalid boolean'));
    }

    /**
     * ## Validates an integer
     *
     * - Ignores null
     * - Accepts also boolean and converts to 0/1
     * - Accepts float if not exceeds PHP_INT_MAX and converts (truncates) to integer
     * - Converts to integer all non-integer representation
     *
     * @param string $fieldname
     * @return bool
     */
    public function integerValidate($fieldname) {
        $value = $this->emptyIsNull($fieldname);
        if ($value === null) return true;
        if (!is_scalar($value)) return $this->setError($fieldname, UApp::la('uapp', 'is not a scalar'));
        if (is_int($value)) return true;
        if (is_numeric($value) && is_float($value + 0) && ($value + 0) > PHP_INT_MAX) return $this->setError($fieldname, UApp::la('uapp', 'is too big for integer'));
        if (is_numeric($value) && (is_float($value + 0) || (int)$value == $value)) {
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = (int)(float)$value;
            return true;
        }
        if (is_bool($value)) {
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = ($value ? 1 : 0);
            return true;
        }
        return $this->setError($fieldname, UApp::la('uapp', 'is invalid integer'));
    }

    /**
     * ## Validates any integer-like string
     * - Ignores empty string and null
     * - Accepts float if not exceeds PHP_INT_MAX and converts (truncates) to string
     * - Accepts also boolean and converts to 0/1
     * - Converts to string
     *
     * @param string $fieldname
     * @return bool
     */
    public function intValidate($fieldname) {
        $value = $this->emptyIsNull($fieldname);
        if ($value === null) return true;
        if (is_int($value)) {
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = (string)$value;
            return true;
        }
        if (is_numeric($value) && is_float($value + 0)) {
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = number_format(floor($value), 0, '', '');
            return true;
        }
        if (is_bool($value)) {
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = ($value ? '1' : '0');
            return true;
        }
        if (!is_string($value) || !preg_match('~^((:?+|-)?[0-9]+)$~', $value)) {
            return $this->setError($fieldname, UApp::la('uapp', 'is invalid integer'));
        }
        return true;
    }

    /**
     * Validates any number (no conversion)
     *
     * @param string $fieldname
     *
     * @return bool
     */
    public function numberValidate($fieldname) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if ($value === null) return true;
        if (is_numeric($value)) return true;
        return $this->setError($fieldname, UApp::la('uapp', '$1 is invalid number'));
    }

    /**
     * Validates any number and converts to float
     *
     * @param $field
     *
     * @return bool
     */
    public function floatValidate($field) {
        /** @noinspection PhpVariableVariableInspection */
        if ($this->$field === null) return true;
        if ($this->numberValidate($field)) {
            /** @noinspection PhpVariableVariableInspection */
            $this->$field = (float)$this->$field;
            return true;
        }
        return false;
    }

    /**
     * @param string $fieldname
     *
     * @return bool|false
     */
    public function fileValidate($fieldname) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if ($value === null) return true;
        if ($value instanceof FileUpload) return true;
        return $this->setError($fieldname, UApp::la('uapp', 'is not an uploaded file'));
    }

    /**
     * Converts a string to lowercase
     *
     * Always passes
     *
     * @param string $fieldname
     *
     * @return bool
     */
    public function lowercaseValidate($fieldname) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if ($value === null) return true;
        if (!$this->stringValidate($fieldname)) return false;
        /** @noinspection PhpVariableVariableInspection */
        $this->$fieldname = strtolower($value);
        return true;
    }

    /**
     * Checks and convert value to a valid Macaddr.
     * Empty values including only-spaces are converted to null.
     *
     * @param string $fieldname
     *
     * @return boolean
     * @throws InternalException
     */
    public function macaddrValidate($fieldname) {
        $value = $this->emptyIsNull($fieldname, true);
        if ($value === null) return true;
        if (is_object($value) && $value instanceof Macaddr) return true;
        Util::assertString($value);
        $value = trim($value);
        Debug::tracex('mac-value', $value);
        if (!Macaddr::isValid($value)) return $this->setError($fieldname, UApp::la('uapp', 'is invalid MAC address'));
        $value = new Macaddr($value);
        /** @noinspection PhpVariableVariableInspection */
        $this->$fieldname = $value;
        return true;
    }

    /**
     * Trims whitespaces
     * Fails if not a string
     *
     * @param string $fieldname
     *
     * @return boolean
     */
    public function trimValidate($fieldname) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if ($value === null) return true;
        if (!$this->stringValidate($fieldname)) return false;
        /** @noinspection PhpVariableVariableInspection */
        $this->$fieldname = trim($value);
        return true;
    }

    /**
     * Checks and convert value to a valid Inet
     *
     * @param string $fieldname
     *
     * @return boolean
     * @throws InternalException
     * @throws UAppException
     */
    public function inetValidate($fieldname) {
        $value = $this->emptyIsNull($fieldname);
        if ($value === null) return true;
        if (is_object($value)) {
            if ($value instanceof Inet && $value->valid) return true;
            return $this->setError($fieldname, UApp::la('uapp', 'is invalid Inet object'));
        }
        Util::assertString($value);
        $value = new Inet($value);
        if (!$value->valid) return $this->setError($fieldname, UApp::la('uapp', 'is invalid IP address'));
        /** @noinspection PhpVariableVariableInspection */
        $this->$fieldname = $value;
        return true;
    }

    /**
     * @param string $fieldname
     *
     * @return bool
     * @throws InternalException
     */
    public function datetimeValidate($fieldname) {
        $value = $this->emptyIsNull($fieldname);
        if ($value === null) return true;
        if (is_a($value, 'DateTime')) return true;
        Util::assertString($value);
        $formats = [DateTime::ATOM, 'Y-m-d H:i:s', 'Y-m-d H:i', 'Y.m.d. H:i:s', 'd-M-y H:i:s', 'Y.m.d. H:i', 'd-M-y H:i',
            'Y.m.d.', 'd-M-y', 'Y. m. d. H:i', 'Y. m. d.'];
        foreach ($formats as $i => $format) {
            $v = DateTime::createFromFormat($format, $value);
            if ($v !== false) {
                if ($i > 6) $v->setTime(0, 0);
                break;
            }
        }
        if ($v !== false) {
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = $v;
            return true;
        }
        return $this->setError($fieldname, UApp::la('uapp', 'is invalid DateTime'));
    }

    /**
     * If value is empty, defaults to current timestamp.
     * Fails if not a datetime
     *
     * @param string $fieldname
     *
     * @return bool
     * @throws InternalException
     */
    public function defaultNowValidate($fieldname) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if (is_a($value, 'DateTime')) return true;
        if ($value === null || $value === '') {
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = new DateTime();
            return true;
        }
        return $this->datetimeValidate($fieldname);
    }

    /**
     * @param string $fieldname
     *
     * @return bool
     */
    public function dateValidate($fieldname) {
        $value = $this->emptyIsNull($fieldname);
        if ($value === null) return true;
        if (is_a($value, 'DateTime')) return true;
        $formats = ['Y.m.d.', 'Y.m.d', 'Y. m. d.', 'd-M-y', 'Y-m-d'];
        foreach ($formats as $format) {
            $value1 = DateTime::createFromFormat($format, $value);
            if ($value1) break;
        }
        if ($value1 !== false) {
            $value1->setTime(0, 0);
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = $value1;
            return true;
        }
        return $this->setError($fieldname, UApp::la('uapp', 'is invalid date') . ': `' . $value . '`');
    }

    /**
     * ## Validates a datetime (without date)
     *
     * - accepts positive integer as number of seconds
     * - does not removes date part
     * - converts string to DateTime using accepting only time formats
     *
     * @param $field
     *
     * @return bool
     * @throws Exception
     */
    public function timeValidate($field) {
        $value = $this->emptyIsNull($field);
        if ($value === null) return true;

        if (is_a($value, 'DateTime')) {
            return true;
        }

        if (is_int($value) && $value >= 0) {
            $new_value = new DateTime();
            /** @noinspection PhpVariableVariableInspection */
            $this->$field = $new_value->setTimestamp($value);
            return true;
        }

        if (is_string($value)) {
            $formats = ['H:i:s', 'H:i'];
            foreach ($formats as $format) {
                $value1 = DateTime::createFromFormat($format, $value);
                if ($value1) break;
            }
            if ($value1 !== false) {
                /** @noinspection PhpVariableVariableInspection */
                $this->$field = $value;
                return true;
            }
        }

        return $this->setError($field, UApp::la('uapp', 'is invalid time'));
    }

    /**
     * ## Validates a datetime without time zone (removes time zone)
     *
     * @param string $field
     *
     * @return bool
     * @throws InternalException
     */
    public function timestampValidate($field) {
        if (!$this->datetimeValidate($field)) return false;
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$field;
        if ($value !== null) {
            $timezone = L10nBase::$timezone ?: 'Europe/Budapest';
            $value->setTimezone(new DateTimeZone($timezone));
        }
        return true;
    }

    /**
     * ## Validates a field using preg pattern (use // delimiters)
     *
     * Field type must be string or covertible to string
     * Null values always pass!
     * If multiple patterns given, at least one of them must match.
     * (if multiple patterns given at level 2, all of them must match)
     *
     * @param string $field
     * @param $patterns
     * @param string $custommessage
     *
     * @return bool
     * @throws InternalException
     * @throws ModelException
     */
    public function patternValidate($field, $patterns, $custommessage = null) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$field;
        if (empty($value)) return true;
        if (!is_array($patterns)) $patterns = [$patterns];

        try {
            if (!static::checkPatterns($value, $patterns)) {
                return $this->setError($field, $custommessage ?: UApp::la('uapp', 'has invalid format'));
            }
        } catch (ModelException $e) {
            throw new ModelException(UApp::la('uapp', 'Field `{$field}`: {$message}', ['field' => $field]), $e->getExtra(), $e);
        }
        return true;
    }

    /**
     * ## Validates multiple values using preg pattern array (use // delimiters)
     *
     * If value is an array, all elements validated consequently, and returns on first error.
     * If multiple patterns given, at least one of them must match (if no pattern at all, always fails)
     * If multiple patterns given at level 2, all of them must match (zero patterns always match)
     *
     * @param string|array $values
     * @param array $patterns
     *
     * @return bool
     * @throws InternalException
     * @throws ModelException
     */
    public static function checkPatterns($values, $patterns) {
        Util::assertArray($patterns);
        if (!is_array($values)) $values = [$values];
        foreach ($values as $value) {
            Util::assertString($value);
            $f = false;
            foreach ($patterns as $pattern) {
                if (!is_string($pattern) && !is_array($pattern)) throw new ModelException(UApp::la('uapp', 'pattern is not a string or array'));
                if (is_string($pattern) && preg_match($pattern, $value) == 1) return true;
                if (is_array($pattern)) {
                    $m = true;
                    foreach ($pattern as $p) {
                        if (!is_string($p)) throw new ModelException(UApp::la('uapp', 'pattern is not a string ({$p})', ['p' => $p]), print_r($patterns, true));
                        if (!preg_match($p, $value) == 1) {
                            $m = false;
                            break;
                        }
                    }
                    if ($m) {
                        $f = true;
                        break;
                    }
                }
            }
            if (!$f) return false;
        }
        return true;
    }

    /**
     * Checks if field value is not null
     *
     * @param mixed $fieldname
     *
     * @return bool
     */
    public function notNullValidate($fieldname) {
        /** @noinspection PhpVariableVariableInspection */
        $valid = !is_null($this->$fieldname);
        if (!$valid) return $this->setError($fieldname, UApp::la('uapp', 'must not be null'));
        return true;
    }

    /**
     * Checks if field value is not empty
     *
     * @param string $fieldname
     *
     * @return boolean
     */
    public function mandatoryValidate($fieldname) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if ($value !== false && empty($value)) {
            return $this->setError($fieldname, UApp::la('uapp', 'is mandatory'));
        }
        return true;
    }

    /**
     * Checks if field value is empty
     *
     * @param string $fieldname
     *
     * @return boolean
     */
    public function emptyValidate($fieldname) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if ($value === false || !empty($value)) {
            return $this->setError($fieldname, UApp::la('uapp', 'must be empty'));
        }
        return true;
    }

    /**
     * validate a rule, if another field has a value, pass otherwise.
     *
     * @param string $fieldname
     * @param string $fieldname2
     * @param mixed $refvalue
     * @param array $rule ...
     *
     * @return boolean
     * @throws InternalException
     * @throws ModelException
     * @throws UAppException
     * @throws ReflectionException
     */
    public function ifValidate($fieldname, $fieldname2, $refvalue, $rule) {
        /** @noinspection PhpVariableVariableInspection */
        $value2 = $this->$fieldname2;
        Debug::tracex('If', $value2);
        if (!$rule) throw new ModelException('Invalid arguments for if rule');
        if ($value2 != $refvalue) return true;
        $f = $this->validateRule($fieldname, $rule);
        if (func_num_args() > 4) {
            for ($i = 4; $i < func_num_args(); $i++) {
                $rulex = func_get_arg($i);
                if ($f) $f = $this->validateRule($fieldname, $rulex);
            }
        }
        if (!$f) {
            $type2 = $this->attributeType($fieldname2);
            Debug::tracex('type2', $type2);
            $err = $type2 == 'boolean' ?
                ($refvalue ? '{$err} if `{$fieldname2}`' : '{$err} if not `{$fieldname2}`') :
                ($refvalue === null ? '{$err} if `{$fieldname2}` is not specified' : '{$err} if `{$fieldname2}` is {$refvalue}');
            return $this->setError($fieldname,
                UApp::la('uapp', $err, [
                    'fieldname2' => $this->attributeLabel($fieldname2),
                    'refvalue' => $refvalue,
                    'err' => $this->lastError($fieldname, true)
                ])
            );
        }
        return true;
    }

    /**
     * Validate a rule, if another field has a value, pass otherwise.
     * Repeats original messages
     *
     * @param string $fieldname
     * @param string $fieldname2
     * @param mixed $refvalue
     * @param array $rule ...
     *
     * @return boolean
     * @throws InternalException
     * @throws ModelException
     * @throws UAppException
     */
    public function if2Validate($fieldname, $fieldname2, $refvalue, $rule) {
        /** @noinspection PhpVariableVariableInspection */
        $value2 = $this->$fieldname2;
        Debug::tracex('If', $value2);
        if (!$rule) throw new ModelException('Invalid arguments for if rule');
        if ($value2 != $refvalue) return true;
        $f = $this->validateRule($fieldname, $rule);
        if (func_num_args() > 4) {
            for ($i = 4; $i < func_num_args(); $i++) {
                $rulex = func_get_arg($i);
                if ($f) $f = $this->validateRule($fieldname, $rulex);
            }
        }
        return $f;
    }

    /**
     * Checks if field value is between given limits (including)
     * null limits are ignored
     *
     * @param string $fieldname
     * @param mixed $min
     * @param mixed $max
     *
     * @return boolean
     * @throws InternalException
     */
    public function betweenValidate($fieldname, $min, $max = null) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if (is_null($value)) return true;
        $valid = ($min === null || $value >= $min) && ($max === null || $value <= $max);
        if (!$valid) return $this->setError($fieldname, Util::substitute(UApp::la('uapp', 'must be between {$min} and {$max}'), ['min' => $min, 'max' => $max]));
        return true;
    }

    /**
     * Special validator which always succeeds.
     * Modifies empty values to given default.
     *
     * @param string $fieldname
     * @param mixed $default
     *
     * @return true
     */
    public function defaultValidate($fieldname, $default) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if (empty($value)) {
            /** @noinspection PhpVariableVariableInspection */
            $this->$fieldname = $default;
        }
        return true;
    }

    /**
     * Converts string value to array
     *
     * @param string $fieldname
     * @param string $pattern -- delimiter to explode with
     *
     * @return boolean
     */
    public function arrayValidate($fieldname, $pattern) {
        /** @noinspection PhpVariableVariableInspection */
        $value = $this->$fieldname;
        if (is_null($value)) return true;
        if (is_array($value)) return true;
        if (!is_string($value)) return $this->setError($fieldname, UApp::la('uapp', 'Invalid array value'));
        /** @noinspection PhpVariableVariableInspection */
        $this->$fieldname = explode($pattern, $value);
        return true;
    }

    /**
     * Ha a mező értéke egy tömb, akkor [2^n => 2^n|0,...] alakúnak kell lennie.
     * Ezt konvertálja integerré.
     *
     * @param $fieldName
     *
     * @return bool|false
     */
    public function bitarrayValidate($fieldName) {
        $value = $this->$fieldName;
        if (!is_array($value)) return true;
        $v = 0;
        foreach ($value as $bit => $bitvalue) {
            if (!is_numeric($bit) || !is_numeric($bitvalue) || (int)$bit < 1) return $this->setError($fieldName, 'Érvénytelen bitérték');
            if ((int)$bit == (int)$bitvalue) $v |= $bitvalue;
        }
        $this->$fieldName = $v;
        return true;
    }

    /**
     * Validates field as e-mail address
     *
     * @param string $fieldname
     *
     * @return bool
     * @throws InternalException
     * @throws ModelException
     */
    public function emailValidate($fieldname) {
        return $this->patternValidate($fieldname, [self::VALID_EMAIL], UApp::la('uapp', 'is invalid e-mail address'));
    }

    /**
     * Validates field as url address
     *
     * @param string $fieldname
     *
     * @return bool
     * @throws InternalException
     * @throws ModelException
     */
    public function urlValidate($fieldname) {
        return $this->patternValidate($fieldname, [self::VALID_URL], UApp::la('uapp', 'is invalid url address'));
    }

    /**
     * Returns the value of a property
     * Attributes and related objects can be accessed like properties.
     *
     * Do not call this method directly as it is a PHP magic method that
     * will be implicitly called when executing `$value = $component->property;`.
     *
     * 1. if value in _attributes array is set, return that; or
     * 2. if getter function exists, use that; or
     * 3. if attribute is registered, but not loaded, return null.
     * 4. use parent's
     *
     * @param string $name the property name
     *
     * @return mixed the property value
     * @throws InternalException
     * @throws ReflectionException
     */
    public function __get($name) {
        // Getter function (function names are case-insensitive in PHP!)
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        }

        // Existing but not loaded attribute
        if ($this->hasAttribute($name)) {
            return null;
        }

        // Other
        return parent::__get($name);
    }

    /**
     * Sets the value of a Model attribute via PHP setter magic method.
     *
     * 1. If attribute setter exists, use that; or
     * 2. Set value in _attrinbutes array.
     *
     * @param string $name property name
     * @param mixed $value property value
     *
     * @throws InternalException
     */
    public function __set($name, $value) {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            $this->$setter($value);
            return;
        }
        if (method_exists($this, 'get' . $name)) {
            throw new ModelException('Setting read-only property: ' . get_class($this) . '::' . $name);
        }
        throw new ModelException('Setting non existing property: ' . get_class($this) . '::' . $name);
    }

    /**
     * ## Checks if a property is set, i.e. defined and not null.
     * This method will check in the following order and act accordingly:
     *
     *  - a property defined by a setter: return whether the property is set
     *  - return `false` for non existing properties
     *
     * Do not call this method directly as it is a PHP magic method that
     * will be implicitly called when executing `isset($component->property)`.
     *
     * @param string $name the property name or the event name
     * @return bool whether the named property is set
     * @see http://php.net/manual/en/function.isset.php
     */
    public function __isset($name) {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter() !== null;
        }

        return false;
    }

    /**
     * ## Sets a component property to be null
     *
     * Do not call this method directly as it is a PHP magic method that
     * will be implicitly called when executing `unset($component->property)`.
     *
     * @param string $name the property name
     * @throws ModelException if the property is read only.
     */
    public function __unset($name) {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            $this->$setter(null);
            return;
        }
        throw new ModelException('Unsetting an unknown or read-only property: ' . $name, get_class($this));
    }

    /**
     * Returns the type of the named attribute.
     *
     * @param string $name - the attribute name
     *
     * @return string - the common typename of the attribute (see {@see self::attributeTypes()})
     * @throws ModelException if attribute not exists
     * @throws InternalException
     * @throws ReflectionException
     */
    public static function attributeType($name) {
        if (static::hasAttribute($name)) {
            $types = static::attributeTypes();
            return ArrayUtils::getValue($types, $name, 'string');
        }
        throw new ModelException("Unknown attribute $name");
    }

    /**
     * Returns the type of the named attribute.
     * The live version allows instance-based inheritance (Used in Model for different database connections)
     *
     * @param string $name - the attribute name
     *
     * @return string - the common typename of the attribute (@see self::attributeTypes())
     * @throws ModelException if attribute not exists
     * @throws UAppException
     * @throws ReflectionException
     */
    public function attributeTypeLive($name) {
        return static::attributeType($name);
    }

    /**
     * ## Must return the attribute labels.
     * Attribute labels are mainly used for display purpose
     * Order of labels is the default order of fields.
     *
     * For getting label for a specific attribute, see {@see attributeLabel()}
     *
     * @return array attribute labels (name => label)
     */
    public static function attributeLabels() {
        return [];
    }

    /**
     * If overridden, must return the attribute hints.
     * Attribute hints are mainly used for display purpose on forms
     * It may be an array, with following keys:
     * - hint: for mouse over it
     * - comment: short, always visible after field
     * - help: long, visible under [?] button
     * - descr: in next line (may be more)
     * - tooltip: tutorial, in yellow
     * @return array attribute hints (name => hints)
     */
    public static function attributeHints() {
        return [];
    }

    /**
     * Returns the text label for the specified attribute, based on attributeLabels() array.
     * Default returns the fieldname itself, if label is not defined.
     * @param string $attribute the attribute name
     * @return string the attribute label
     * @see attributeLabels()
     */
    public static function attributeLabel($attribute) {
        $labels = static::attributeLabels();
        return isset($labels[$attribute]) ? $labels[$attribute] : $attribute;
    }

    /**
     * Returns the text hint (or array of hint-like texts) for the specified attribute.
     * Keys of array may be:
     * - hint: for mouse over it
     * - comment: short, always visible after field
     * - help: long, visible under [?] button
     * - descr: in next line (may be more)
     * - tooltip: tutorial, in yellow
     * @param string $attribute the attribute name
     * @param string $type -- null or a specific key of multiple hint-type texts
     * @return string|array the attribute hint
     * @see attributeHints()
     */
    public static function attributeHint($attribute, $type = null) {
        $hints = static::attributeHints();
        $hint = isset($hints[$attribute]) ? $hints[$attribute] : '';
        if (is_string($type) && is_array($hint)) $hint = ArrayUtils::getValue($hint, $type, '');
        return $hint;
    }

    /**
     * Returns attribute values.
     *
     * @param array $names list of attributes whose value needs to be returned.
     * Defaults to null, meaning all attributes listed in [[attributes()]] will be returned.
     * If it is an array, only the attributes in the array will be returned.
     *
     * @return array attribute values (name => value).
     * @throws ReflectionException
     */
    public function namedAttributes($names = null) {
        $values = [];
        if ($names === null) {
            $names = static::attributes();
        }
        foreach ($names as $name) {
            if (!is_string($name)) continue; // because subnodeFields may contain callables
            /** @noinspection PhpVariableVariableInspection */
            $values[$name] = $this->$name;
        }
        return $values;
    }

    /**
     * Returns all attribute values.
     *
     * For getting selective attributes, see {@see namedAttributes()} method.
     *
     * @return array attribute values (name => value).
     * @throws ReflectionException
     */
    public function getAttributes() {
        return $this->namedAttributes(static::attributes());
    }

    /**
     * Sets the attribute values in a massive way.
     *
     * @param array $values attribute values (name => value) to be assigned to the model.
     *
     * @throws ReflectionException
     * @see attributes()
     */
    public function setAttributes($values) {
        if (is_array($values)) {
            $attributes = array_flip(static::attributes());
            foreach ($values as $name => $value) {
                if (isset($attributes[$name])) {
                    /** @noinspection PhpVariableVariableInspection */
                    $this->$name = $value;
                }
            }
        }
    }

    /**
     * Returns the list of fields that should be returned by default by [[toArray()]] when no specific fields are specified.
     *
     * A field is a named element in the returned array by [[toArray()]].
     *
     * This method should return an array of field names or field definitions.
     * If the former, the field name will be treated as an object property name whose value will be used
     * as the field value. If the latter, the array key should be the field name while the array value should be
     * the corresponding field definition which can be either an object property name or a PHP callable
     * returning the corresponding field value. The signature of the callable should be:
     *
     * ```php
     * function ($model, $field) {
     *     // return field value
     * }
     * ```
     *
     * For example, the following code declares four fields:
     *
     * - `email`: the field name is the same as the property name `email`;
     * - `firstName` and `lastName`: the field names are `firstName` and `lastName`, and their
     *   values are obtained from the `first_name` and `last_name` properties;
     * - `fullName`: the field name is `fullName`. Its value is obtained by concatenating `first_name`
     *   and `last_name`.
     *
     * ```php
     * return [
     *     'email',
     *     'firstName' => 'first_name',
     *     'lastName' => 'last_name',
     *     'fullName' => function ($model) {
     *         return $model->first_name . ' ' . $model->last_name;
     *     },
     * ];
     * ```
     *
     * The default implementation of this method returns [[attributes()]] indexed by the same attribute names.
     *
     * @return array the list of field names or field definitions.
     * @throws ReflectionException
     * @see toArray()
     */
    public static function fields() {
        $fields = static::attributes();
        return array_combine($fields, $fields);
    }


    /**
     * Returns a value indicating whether the model has an attribute with the specified name.
     *
     * @param string $name the name of the attribute
     *
     * @return bool whether the model has an attribute with the specified name.
     * @throws InternalException
     * @throws ReflectionException
     */
    public static function hasAttribute($name) {
        Util::assertString($name);
        return in_array($name, static::attributes(), true);
    }

    /**
     * Non-static wrapper for hasAttribute()
     *
     * @param $name
     * @param $connection
     * @return bool
     * @throws InternalException
     * @throws ReflectionException
     */
    public function hasClassAttribute($name, $connection = null) {
        return static::hasAttribute($name, $connection);
    }

    /**
     * Returns the named attribute value.
     *
     * @param string $name the attribute name
     *
     * @return mixed the attribute value. `null` if the attribute is not set or does not exist.
     * @throws InternalException -- if name is not a string
     */
    public function getAttribute($name) {
        Util::assertString($name);
        /** @noinspection PhpVariableVariableInspection */
        return $this->$name;
    }

    /**
     * ## Sets the named attribute value.
     *
     * @param string $name the attribute name
     * @param mixed $value the attribute value.
     * @return static
     *
     * @throws InternalException
     */
    public function setAttribute($name, $value) {
        Util::assertString($name);
        /** @noinspection PhpVariableVariableInspection */
        $this->$name = $value;
        return $this;
    }

    /**
     * ## Determines which fields can be returned by {@see toArray()} or {@see createNode()}
     *
     * This method will check the requested fields against those declared in {@see fields()}
     * to determine which fields can be returned.
     *
     * If a specified field is not found, the attribute with the same name will be used.
     *
     * @param array $fieldlist -- the fields (or attributes) being requested for exporting. Default is all of fields(). Associative array indicates name mapping.
     * @return array the list of fields to be exported. The array keys are the field names, and the array values
     * are the corresponding object property names or PHP callables returning the field values.
     * @throws ReflectionException
     */
    public function resolveFields($fieldlist = null) {
        $result = [];
        $fields = $this->fields();

        if (empty($fieldlist)) {
            foreach ($fields as $field => $definition) {
                if (is_int($field)) $field = $definition;
                $result[$field] = $definition;
            }
            return $result;
        }

        foreach ($fieldlist as $alias => $field) {
            if (is_int($alias)) $alias = $field;
            if (array_key_exists($field, $fields)) {
                $definition = $fields[$field];
            } else {
                $definition = $field;
            }
            $result[$alias] = $definition;
        }
        return $result;
    }

    /**
     * ## Converts the model into an array
     *
     * This method will first identify which fields to be included in the resulting array by calling [[resolveFields()]].
     * It will then turn the model into an array with these fields. If `$recursive` is true,
     * any embedded objects will also be converted into arrays.
     *
     * @param array $fields the fields being requested. If empty, all fields as specified by [[fields()]] will be returned.
     * @param bool $recursive whether to recursively return array representation of embedded objects.
     *
     * @return array the associative array representation of the object
     * @throws ReflectionException
     */
    public function toArray(array $fields = null, $recursive = false) {
        $data = [];
        foreach ($this->resolveFields($fields) as $field => $definition) {
            /** @noinspection PhpVariableVariableInspection */
            $data[$field] = is_string($definition) ? $this->$definition : call_user_func($definition, $this, $field);
        }

        return $recursive ? ArrayUtils::toArray($data) : $data;
    }

    /**
     * Must return a name for XML nodes.
     * Default implementation returns the classname in lowercase letters.
     * See {@see createNode()} method
     *
     * @return string
     */
    static public function nodeName() {
        return strtolower(get_called_class());
    }


    /**
     * # BaseModel::createNode()
     *
     * Creates an XML node from the model under parent node (does not handle associations like Model::createNode does)
     *
     * ### Options:
     * - nodeName: the name of created node, default is model name
     * - attributes: the attributes to generate, default is null = use fields(), or array of field names to use.
     * - contentField: name of attribute to include in node content (default none) (content field is excluded from attributes)
     * - contentValue: value of content, effective only if contentField is not given or has no value.
     * - subnodeFields: name of attributes to include in subnodes instead of attributes (or alias=>fieldname pairs). xml fields will include native xml values
     *        Instead of fieldname you may specify:
     *        - callable($index, $model); callable may generate scalar or array, scalar may be an xml (DOMElement)
     *        - array:  multiple literal node values (recursive, {@see createSubnode()})
     *
     *           - 'nodeName' may define the name of subnode
     *           - key '_nodename' defines subnode field
     *           - key '__content' will create the content of the child node
     * - extra: extra attributes array(name, name=>value, name=>function(index, item),...)
     * - overwrite: true = overwrite/complement existing node (given as parent)
     * - labels: If set to true, generates label subnodes. Default is false. May be an associative array of custom labels.
     *
     * Explicit declared and same-name associations are recursive.
     *
     * @param UXMLElement|UAppPage $node_parent
     * @param array $options
     *
     * @return UXMLElement
     * @throws InternalException
     * @throws ModelException
     * @throws UAppException
     * @throws DOMException
     * @throws ReflectionException
     * @see Model::createNode()    for model options
     */
    public function createNode($node_parent, $options = []) {
        if ($node_parent instanceof DOMDocument) $node_parent = $node_parent->documentElement;
        if ($node_parent instanceof UAppPage) $node_parent = $node_parent->node_content;

        $nodename = ArrayUtils::getValue($options, 'nodeName', static::nodeName());
        $attributes = $this->resolveFields(ArrayUtils::getValue($options, 'attributes'));
        $contentField = ArrayUtils::getValue($options, 'contentField');
        $defaultContentValue = ArrayUtils::getValue($options, 'contentValue');
        $subnodeFields = ArrayUtils::getValue($options, 'subnodeFields', []);
        $extra = ArrayUtils::getValue($options, 'extra', []);
        $index = ArrayUtils::getValue($options, 'index', 0); // set by createNodes
        $overwrite = ArrayUtils::getValue($options, 'overwrite', false); // Overwrite existing node (given as parent)
        $contentValue = $defaultContentValue;
        $labels = ArrayUtils::getValue($options, 'labels', false);

        foreach ($attributes as $n => $f) {
            if (is_int($n)) {
                $attributes[$f] = $f;
                unset($attributes[$n]);
            }
        }

        if ($contentField) {
            /** @noinspection PhpVariableVariableInspection */
            $contentValue = $this->$contentField;
            if ($contentValue === null) $contentValue = $defaultContentValue;
            unset($attributes[array_search($contentField, $attributes)]);
        }

        foreach ($subnodeFields as $snf) {
            unset($attributes[array_search($snf, $attributes)]);
        }

        /** associative array of attributename=>attributevalue pairs */
        $attributevalues = [];
        foreach ($attributes as $key => $value) {
            if (!is_string($value) && is_callable($value)) $attributevalues[$key] = $value($this);
            else if (is_scalar($value)) {
                /** @noinspection PhpVariableVariableInspection */
                $attributevalues[$key] = $this->$value;
            }
        }

        $content = null;
        foreach ($attributevalues as $name => $value) {
            if (substr($name, 0, 2) == '__') {
                $name = substr($name, 2);
                $content = $value;
                unset($attributevalues['__' . $name]);
            } elseif (substr($name, 0, 1) == '_') {
                $name = substr($name, 1);
                $subnodeFields[$name] = $name;
                $subnodeValues[$name] = $value;
                unset($attributevalues['_' . $name]);
            }
        }

        if ($overwrite) {
            $node = $node_parent;
            foreach ($attributevalues as $name => $value) $node->addAttribute($name, $value);
        } else {
            $node = $node_parent->addNode($nodename, $attributevalues, $content);
        }
        if ($contentValue) {
            $node->addHypertextContent($contentValue, false, true);
        }

        foreach ($subnodeFields as $key => $snf) {
            $tagname = is_int($key) ? (is_string($snf) ? $snf : 'item_' . $key) : $key;
            if (is_callable($snf)) {
                // Subnodes generated by function: `array function($index, $model)`
                $values = call_user_func($snf, $index, $this);
                if (is_array($values)) {
                    foreach ($values as $value) {
                        // TODO: use addNode()
                        $node->createSubnode($tagname, $value);
                    }
                } else $node->createSubnode($tagname, $values);
            } else if (is_array($snf)) {
                foreach ($snf as $v) $node->createSubnode($tagname, $v);
            } else {
                /** @noinspection PhpVariableVariableInspection */
                $node->createSubnode($tagname, $this->$snf, $this->attributeType($snf) == 'xml');
            }
        }

        // extra (computed) attributes
        foreach ($extra as $name => $fn) {
            if (is_numeric($name) && is_string($fn)) {
                /** @noinspection PhpVariableVariableInspection */
                $node->addAttribute($fn, $this->$fn);
            } else if (is_callable($fn)) {
                $node->addAttribute($name, call_user_func($fn, $index, $this));
            } else if (is_string($fn)) {
                $node->addAttribute($name, $this->$fn);
            } else {
                $node->addAttribute($name, $fn);
            }
        }

        if ($labels) {
            if (!is_array($labels)) $labels = $this->attributeLabels();
            $node->createSubnode('label', array_map(function ($name, $value) {
                return [['name' => $name], $value];
            }, array_keys($labels), array_values($labels)));
        }

        if (!$this->_node) $this->_node = $node;
        return $node;
    }

    /**
     * Creates XML nodes into DOM from all model entities of the array
     *
     * Options are processed by Model->createNode, except following special options:
     * - after: function(item, index) -- called after creating each item's node
     *
     * @param DOMElement|UAppPage $node_parent -- If an UAppPage is given, it's content node will be applied
     * @param BaseModel[] $models -- numeric indexed
     * @param mixed $options -- {@see BaseModel::createNode()}, {@see Model::createNode()}
     *
     * @return DOMElement -- the first node inserted or null if none
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws UAppException
     * @see BaseModel::createNode()    for basic options
     * @see Model::createNode()    for model options
     * @throws ReflectionException
     */
    public static function createNodes($node_parent, $models, $options = []) {
        if ($node_parent instanceof UAppPage) $node_parent = $node_parent->node_content;
        if (empty($models)) return null;
        $after = ArrayUtils::fetchValue($options, 'after');
        $first_node = null;
        foreach ($models as $index => $item) {
            $options['index'] = $index;
            Util::assertClass($item, BaseModel::class);
            $node = $item->createNode($node_parent, $options);
            if ($after && is_callable($after)) call_user_func($after, $item, $index);
            if (!$first_node) $first_node = $node;
        }
        return $first_node;
    }

    /**
     * Returns errors extended with values.
     *
     * @return array -- fieldname indexed array of numeric indexed errors and 'value'=> value
     */
    public function getErrorValues() {
        $errors = $this->_errors;
        foreach ($errors as $fieldname => $fielderrors) {
            /** @noinspection PhpVariableVariableInspection */
            $errors[$fieldname]['value'] = $this->$fieldname;
        }
        return $errors;
    }

    /**
     * Returns friendly error texts.
     * Error strings contains field labels.
     *
     * @return array of fieldname => errorstring
     */
    public function getFriendlyErrors() {
        $errortexts = [];
        foreach ($this->_errors as $fieldname => $fielderrors) {
            $label = $this->attributeLabel($fieldname);
            /** @noinspection PhpVariableVariableInspection */
            $value = $this->$fieldname;
            $errortexts[$fieldname] = UApp::la(
                'uapp',
                'Field `{$fieldname}` {$message}',
                ['fieldname' => $label, 'message' => implode(', ', $fielderrors), 'value' => $value]);
        }
        return $errortexts;
    }

    public function getFirstError() {
        $fe = array_values($this->friendlyErrors);
        return $fe ? $fe[0] : '';
    }

    /**
     * Returns friendly errors as node array.
     * Suitable for UXMLPage::addContent()
     *
     * @return array -- array(array('error', array('field'=>fieldname), error_message)
     */
    public function getErrorNodes() {
        $errors = $this->friendlyErrors;
        return array_map(function ($item, $key) {
            return ['error', ['field' => $key], $item];
        }, $errors, array_keys($errors));
    }

    /**
     * Returns the ruleset for the named field
     * Returns empty array if rules not defined for the field.
     *
     * @param string $name
     * @return array
     */
    public function getRule($name) {
        $rules = $this->rules();
        return ArrayUtils::getValue($rules, $name, []);
    }

    /**
     * Returns first created node if any or null
     * @return DOMElement|null
     * @see property $node
     */
    public function getNode() {
        return $this->_node;
    }

    /**
     * Returns translatable validation rule message for client validation
     *
     * @param $rule_name
     *
     * @return string|null
     */
    static public function ruleMessage($rule_name) {
        $rulemessages = [
            'mandatory' => 'is mandatory',
            'pattern' => 'has invalid format',
            //'length' => 'is too long or too short',
            'length' => 'length must be between $1 and $2',
            'between' => 'must be between $1 and $2',
        ];
        return ArrayUtils::getValue($rulemessages, $rule_name);
    }

    /**
     * Checks if value fits to common php typename
     * No side effects, no error message
     *
     * @param string $type
     * @param mixed $value
     * @return boolean
     */
    public static function checkType($type, $value) {
        if (is_null($value)) return true;
        if (gettype($value) == $type) return true;
        if (is_object($value) && is_a($value, $type)) return true;
        if ($type == 'integer') {
            if (is_int($value)) return true;
            if (is_float($value) && ($value) > PHP_INT_MAX) return false;
            return (string)(int)$value == $value;
        }
        if ($type == 'float') return is_numeric($value);
        if ($type == 'string') return true;
        if ($type == 'boolean') {
            return in_array(strtolower($value), ['true', 'false', 't', 'f', '0', '1']);
        }
        if ($type == 'xml') {
            if (is_a($value, 'DOMNode')) return true;
            if (is_string($value) && substr($value, 0, 1) == '<') return true;
            return false;
        }
        // TODO: other types
        return false;
    }

    public function getErrors() {
        return $this->_errors;
    }

    public function resetErrors() {
        $this->_errors = [];
    }

}
