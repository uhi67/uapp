<?php
/**
 * class InvalidParamsException
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/** @noinspection PhpUnused */
/** @noinspection PhpClassNamingConventionInspection */

/**
 * An 500 type http exception
 */
class InvalidParamsException extends UserException {
    /**
     * @param string|array -- $message message text or [message, params, info]
     * @param array $extra -- debug details
     * @param string $goto -- clickable redirection for end user
     * @param Exception $previous The previous exception used for the exception chaining.
     */
    public function __construct($message = null, $extra = null, $goto = null, $previous = null) {
        parent::__construct($message, $extra, $goto, 500, $previous);
    }

    public function getName() {
        return UApp::la('uapp', 'Invalid parameters');
    }
}
