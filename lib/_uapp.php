<?php
/** @noinspection PhpUnhandledExceptionInspection */
/**
 * This is the main router/dispatcher of UApp framework, included from index.php OR the CLI starter script.
 *
 * 1. uapp.dev/page/params -> pages/pagePage.php
 * 2. uapp.dev/js/filename -> www/js/filename (it's redundant if .htaccess redirects properly)
 * 3. uapp.dev/module/filename -> modules/module/filename | ../uapp/modules/module/filename
 *
 * @filesource _uapp.php
 * @author uhi
 * @copyright 2017
 */

/** @var array $config -- must be defined before including this file */
if (!isset($config)) fatal_error(500, 'Application configuration error. Config file not found');

$uapp_path = dirname(__DIR__);

require_once $uapp_path . '/lib/Loader.php';
Loader::registerAutoload();

/** @var string|UApp $appClass */
$appClass = $config['class'] ?? UApp::class;
if (!class_exists($appClass)) throw new Exception("Class '$appClass' not exists");

// Own asset first (needed if no mod_rewrite is available)
if (php_sapi_name() != "cli" && isset($_SERVER['PATH_INFO'])) {
    $path = $_SERVER['PATH_INFO'];
    $basepath = $config['basepath'];
    $ext = substr($path, strrpos($path, '.'));
    $filename = $basepath . '/' . $path;
    if (file_exists($filename) && $ext != '.php') {
        require_once $uapp_path . '/lib/Loader.php';
        Loader::registerAutoload();
        header("X-Uapp-Source: 0; $path");
        $type = Util::mimetype($path);
        $expires = 3600 * 24;
        if ($type && $type != 'application/octet-stream') {
            try {
                $appClass::$config = $config;
                $timeZone = $appClass::config('timeZone', $appClass::config('l10n/timezone', @date_default_timezone_get()));
                date_default_timezone_set($timeZone);
                return $appClass::serveFile($filename, $type, $expires);
            } catch (Exception $e) {
                $appClass::showException($e);
            }
        }
        exit;
    }
}

if (isset($config['saml']) && (!isset($config['saml']['disabled']) || !$config['saml']['disabled']) && $config['saml']['dir'])
    require_once($config['saml']['dir'] . '/lib/_autoload.php');

$datapath = $config['datapath'];
if (!file_exists($datapath)) {
    if (!file_exists(dirname($datapath)))
        Util::fatalError(500, 'Configuration error', 'Data directory does not exists' . $datapath);
    if (!mkdir($datapath, 0774))
        Util::fatalError(500, 'Configuration error', "Data directory cannot be created at `$datapath`");
}

if (isset($config['saml']) && (!isset($config['saml']['disabled']) || !$config['saml']['disabled']) && $config['saml']['dir'])
    require_once($config['saml']['dir'] . '/lib/_autoload.php');

$pages = $config['pages'];
/**
 * maintain version of your application in this file,
 * e.g. in container init using git describe --tags --long > version
 */
$cv = $config['apppath'] . '/version';
if (file_exists($cv))
    $config['ver'] = $ver = file_get_contents($cv);

$appClass::$config = $config;

$cli = false;
if (php_sapi_name() == "cli") {
    $cli = true;
    $commands = isset($config['commands']) ? $config['commands'] : 'commands';
    $command = isset($argv[1]) ? $argv[1] : 'index';
    $commandClass = Util::camelize($command, true) . 'Command';
    try {
        if (file_exists($commands . '/' . $commandClass . '.php')) {
            $app = new $appClass();
            $app->run($commandClass);
            exit;
        } else if (file_exists($uapp_path . '/commands/' . $commandClass . '.php')) {
            $app = new $appClass();
            $app->run($commandClass);
            exit;
        } else {
            echo "\nError: File $commandClass not found\n";
            exit(1);
        }
    } /** @noinspection PhpElementIsNotAvailableInCurrentPhpVersionInspection */
    catch (Throwable $e) {
        Util::fatalError(500, 'Error creating application object', $e->getMessage(), $e);
    }
}

$page = null;
try {
    // non-cli mode only
    /** @var array $path -- url path elements including pagename */
    $baseurl = $config['baseurl'];
    $path = Util::getPath(null, null, $baseurl);

    $page = array_shift($path);
    $script = $_SERVER['SCRIPT_NAME'];
    if ('/' . $page == $script) $page = array_shift($path);
    if ('/' . $page == $script) $page = array_shift($path);
    if ($page == '') $page = isset($config['default']) ? $config['default'] : 'start';
    header("HTTP/1.1 200 OK");
    header("X-Powered-By: UApp");

    # 1. trying page controller
    $pageclass = Util::camelize($page, true) . 'Page';
    if (file_exists($pages . '/' . $pageclass . '.php')) {
        header("X-Uapp-Source: 1; $page");
        if (!empty($_REQUEST['debug'])) {
            Debug::callback();
            return;
        }
        try {
            $app = $appClass::getInstance();
            $app->run($pageclass, $path);
            $app->finish();
        } catch (Exception $e) {
            $appClass::showException($e);
        }
        unset($app);
        exit;
    }

    Debug::disable();

    # 2. includes, libs
    if (isset($_SERVER['PATH_INFO'])) {
        /** @var string $path */
        $path = $_SERVER['PATH_INFO'];
        $c = strlen($script);
        if (substr($path, 0, $c) == $script) $path = substr($path, $c);
        if (!$cli) header("X-Uapp-Source: 2; $path");
        if ($appClass::getFile($path, $uapp_path)) {
            exit;
        }
    }

    $urf = Util::getUriFile(parse_url($baseurl, PHP_URL_PATH));
    $type = Util::mimetype($urf);

    # 3. module assets
    if (!$cli) header("X-Uapp-Source: 3; $urf");
    if ($appClass::getFile($urf, $uapp_path)) exit;


    if (!empty($_REQUEST['debug'])) {
        Debug::callback();
        return;
    }

    if (!file_exists($config['basepath'] . $_SERVER['REQUEST_URI'])) {
        if (isset($_REQUEST['trace'])) {
            echo '<ol>';
            echo '<li>' . $pages . '/' . $pageclass . '.php' . '</li>';
            echo '<li>getFile(' . Util::objtostr($path) . ',' . $uapp_path . ')</li>';
            echo '<li>getFile(' . Util::objtostr($urf) . ',' . $uapp_path . ')</li>';
            echo '<li>' . $config['basepath'] . $_SERVER['REQUEST_URI'] . '</li>';
            echo '</ol>';
        }
        Util::fatalError(404, 'Not Found', "Resource $page: File " . $_SERVER['REQUEST_URI'] . " not found (_uapp)");
    } else {
        /*
            Runs original file (index.php is included only for base libraries)
        */
        return;
    }
} /** @noinspection PhpElementIsNotAvailableInCurrentPhpVersionInspection */
catch (Throwable $e) {
    try {
        $appClass::showException($e, false);
    } catch (Exception $e) {
        Util::fatalError(500, 'Error loading page `' . $page . '`', $e->getMessage());
    }
}

/**
 * Shows fatal error without autoloader
 *
 * @param mixed $code
 * @param string $subtitle
 * @param mixed $message
 *
 * @return void
 */
function fatal_error($code, $subtitle = '', $message = '') {
    $title = 'Fatal application error';
    $e = new Exception();
    $trace = isset($_REQUEST['trace']) ? $e->getTraceAsString() : '';
    if (php_sapi_name() == "cli") {
        echo "\nError $code $title \n";
        echo $subtitle, "\n";
        echo "$message \n";
        echo $trace;
    } else {
        $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
        header($protocol . ' ' . $code . ' ' . $title);
        header("Status: $code $title");
        echo <<<EOT
<html lang="en">
<head>
	<title>Fatal error - UApp</title>
    <link rel="stylesheet" type="text/css" href="/Main/main.css" />
    <link rel="stylesheet" type="text/css" href="/css/design.css" />
</head>
<body>
    <div id="fatal">
	    <h2>$code $title</h2>
        <h3>$subtitle</h3>
		<p>$message</p>
        <pre>$trace</pre>
	</div>
</body>
</html>
EOT;
    }
    exit($code);
}
