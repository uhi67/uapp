<?php
/**
 * class Module
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2016
 * @license MIT
 */

/**
 * Base class for modules
 *
 * Modules may be defined in UApp/modules or application/modules directory
 */
abstract class Module extends Component {
    protected $page;

    /**
     * Initializes module and registers it to the page
     *
     * @param UAppPage $page
     * @param array $options -- config for module initialization
     *
     * @throws InternalException
     * @throws UAppException
     */
    function __construct($page, $options = []) {
        $this->page = $page;
        parent::__construct($options);
        /* it will be called at page->register */
        /* override it */
    }

    /** @noinspection PhpUnused */
    function ajaxResult($a) {
        header('Content-Type: application/json');
        echo json_encode($a);
        exit;
    }

    /**
     * Processes runtime actions when request contains act=module
     *
     * @return void
     */
    abstract function action();
}
