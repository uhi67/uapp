<?php
/**
 * class DBXPG
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */
/** @noinspection PhpUnused */
if (!function_exists('pg_connect')) {
    fatal_error(500, 'Internal Server Error / Belső szerverhiba', 'PostgreSQL not supported / PostgreSQL támogatás hiányzik.');
}

/**
 * MySQL implementation for UApp
 *
 * ##Example
 * ```
 * $connection = Component::create([
 *     'DBXPG',
 *     'name' => 'uapp_test',
 *     'user' => 'uapp',
 *     'password' => '****',
 *     'encoding' => 'UTF-8',
 * ]);
 * ```
 */
class DBXPG extends DBX {
    private $db_last_error; // muszáj tárolni, mert a debug kiolvassa, akkor másnak nem marad
    /**
     * @var array $typeNames -- sql_type => common_type mapping
     * common type names:
     *  - basic php types (lowercase)
     *  - 'xml' denotes xml valid string
     *  - php classnames (Uppercase, eg. DateTime)
     *  - 'string' is default, not indicated
     *  - date (DateTime without time)
     *  - time (DateTime without date or integer in sec)
     */
    public static $typeNames = [
        'int4' => 'integer',
        'integer' => 'integer',
        'numeric' => 'float',
        'xml' => 'xml',
        'bool' => 'boolean',
        'boolean' => 'boolean',
        'timestamp' => 'DateTime',
        'timestamptz' => 'DateTime',
        'timestamp without time zone' => 'DateTime',
        'timestamp with time zone' => 'DateTime',
        'date' => 'date',
        'time' => 'time',
        'interval' => 'time',
        'money' => 'float',
        'bit' => 'integer',
        'serial' => 'integer',
        'inet' => 'Inet',
        'cidr' => 'Inet',
        'macaddr' => 'Macaddr',
        'bytea' => 'Bytea',
    ];

    /**
     * DBXPG constructor.
     *
     * @param string $db_host
     * @param string $db_port
     * @param string $db_user
     * @param string $db_password
     * @param string $db_name
     * @param string $db_encoding
     * @param bool $connectnow
     *
     * @throws ConfigurationException
     * @throws DatabaseException
     * @throws InternalException
     * @throws ReflectionException
     */
    function __construct($db_host, $db_port = '5432', $db_user = '', $db_password = '', $db_name = '', $db_encoding = 'UTF-8', $connectnow = false) {
        $db_type = 'PG';
        parent::__construct($db_host, $db_port, $db_user, $db_password, $db_name, $db_encoding, $connectnow, $db_type);
    }

    /**
     * @param string $db_host
     * @param string $db_port
     * @param string $db_user
     * @param string $db_password
     * @param string $db_name
     * @param string $db_encoding
     * @return null|resource
     * @throws Exception
     */
    protected function connect($db_host, $db_port, $db_user, $db_password, $db_name, $db_encoding = 'UTF-8') {
        #Debug::tracex('connecting', "$db_host, $db_port, $db_name, $db_user, $db_password");
        $hostx = $db_host ? " host=$db_host " : '';
        $this->connection = pg_connect("port=$db_port user=$db_user password=$db_password dbname=$db_name $hostx");
        if (!$this->connection) {
            $this->connerr = true;
            throw new Exception(UApp::la('uapp', 'Database connection error'));
        } else pg_exec($this->connection, "SET client_encoding to '$db_encoding'");
        return $this->connection;
    }


    /**
     * Terminates this database connection
     *
     * @throws DatabaseException
     */
    public function close() {
        if ($this->connection) {
            if (($t = get_resource_type($this->connection)) != 'pgsql link') throw new DatabaseException('invalid connection resource: ' . $t);
            pg_close($this->connection);
        }
    }

    /**
     * Returns error message associated to a resultset, or the last error
     * The message is stored internally, it is readable multiple times.
     *
     * @param resource $rs
     * @return string
     */
    function error($rs = null) {
        if ($rs) $e = pg_result_error($rs);
        else $e = pg_last_error($this->connection);
        if (!$e) $e = $this->db_last_error;
        else $this->db_last_error = $e;
        return $e;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @param resource $rs
     * @return int
     * @throws DatabaseException
     */
    function affected_rows($rs) {
        if (!$rs) throw new DatabaseException('Invalid result set');
        return pg_affected_rows($rs);
    }

    /**
     * @throws DatabaseException
     */
    function beginTransaction() {
        $rs = pg_query($this->connection, "BEGIN");
        if (!$rs) throw new DatabaseException('Could not start transaction');
    }

    /**
     * @throws DatabaseException
     */
    function commit() {
        $rs = pg_query($this->connection, "COMMIT");
        if (!$rs) throw new DatabaseException('Transaction commit failed');
    }

    /**
     * @throws DatabaseException
     */
    function rollBack() {
        $rs = pg_query($this->connection, "ROLLBACK");
        if (!$rs) throw new DatabaseException('Transaction rollback failed');
    }

    /**
     * Alias of query()
     *
     * @param string $sql
     * @param array $params -- asszociatív tömb
     * @return resource
     * @throws ConfigurationException
     * @throws DatabaseException
     * @throws Exception
     * @throws InternalException
     *
     * @deprecated use {@see query()}
     */
    function queryx($sql, $params = NULL) {
        return $this->query($sql, $params);
    }

    /**
     * SQL query futtatása $1, $2..., $varx, ...  paraméterek behelyettesítésével
     *
     * @param string $sql
     * @param array $params -- 0-ás indexen az $1-es paraméter és/vagy név=>érték párok
     *
     * @return resource|false -- false on failure
     *
     * @throws ConfigurationException
     * @throws DatabaseException
     * @throws Exception
     * @throws InternalException
     */
    function query($sql, $params = NULL) {
        $this->autoconnect();
        if ($params) $sql = self::bindParams($sql, $params);
        $st = Debug::$trace ? microtime(true) : 0;
        if (!($r = @pg_query($this->connection, $sql))) $this->errtrace($sql, $params);
        if (Debug::$trace) Debug::tracex('sql', [$sql, $params], '#080', null, 3, 'sql', microtime(true) - $st);
        return $r;
    }

    /**
     * @param resource $resultset
     * @param int $row
     * @param int $field
     *
     * @return array|bool|string
     * @throws Exception on error or non-existing row
     */
    function result($resultset, $row, $field = 0) {
        if (!$this->connection) return false;
        $r = @pg_fetch_result($resultset, $row, $field);
        if ($r === false) throw new Exception('Result error ' . $this->error($resultset));
        return $this->decode($r);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @param resource $rs
     *
     * @return bool
     */
    function free_result($rs) {
        return pg_free_result($rs);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @param resource $resultset
     * @return int -- -1 = error
     * @throws DatabaseException
     */
    function num_rows($resultset) {
        $n = @pg_num_rows($resultset);
        /** @noinspection PhpStrictComparisonWithOperandsOfDifferentTypesInspection */
        if ($n === FALSE) {
            throw new DatabaseException($this->error());
        }
        return $n;
    }

    /**
     * Returns last OID
     * @param resource $resultset
     * @return bool
     */
    function lastoid($resultset) {
        return pg_last_oid($resultset);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns last inserted autoincrement id
     * Only single id is supported
     *
     * @param string $table -- not used
     * @param string $fieldname -- not used
     * @return false|string|null -- null if not defined yet
     */
    function last_id($table = '', $fieldname = '') {
        $r = @pg_query($this->connection, "select lastval()");
        if (!$r) return null;
        return @pg_fetch_result($r, 0, 0);
    }

    /**
     * Returns the number of fields in a result
     * @param resource $resultset
     * @return integer
     */
    function fields($resultset) {
        return pg_num_fields($resultset);
    }

    /**
     * Returns a fieldname in the resultset
     *
     * @param resource $resultset
     * @param integer $i
     * @return string
     */
    function fieldname($resultset, $i) {
        return pg_field_name($resultset, $i);
    }

    /**
     * Returns the common type of a field in the resultset
     *
     * @param resource $resultset
     * @param integer $i
     * @return string
     */
    function fieldtype($resultset, $i) {
        return self::mapType(pg_field_type($resultset, $i));
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns a row from result set
     *
     * @param resource $resultset
     * @param int $row -- 0-based index of row or next row if omitted or null
     * @return array -- numeric indexed string values
     *
     * An array, indexed from 0 upwards, with each value represented as a string. Database NULL values are returned as NULL.
     * FALSE is returned if row exceeds the number of rows in the set, there are no more rows, or on any other error.
     * @throws DatabaseException
     */
    function fetch_row($resultset, $row = null) {
        $value = @pg_fetch_row($resultset, $row);
        if (!is_array($value)) throw new DatabaseException(sprintf("Invalid row (%d) %s", $row, $this->error()));
        return $this->decode($value);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns a record to associative array
     * All field values will be strings
     *
     * @param resource $resultset
     * @param int $row
     * @return array fieldname=>stringvalue
     */
    function fetch_assoc($resultset, $row = null) {
        return $this->decode(pg_fetch_assoc($resultset, $row));
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns a record to associative array.
     * Field values will be mapped to php types.
     *
     * @param resource $resultset
     * @param int|null $row
     * @return array
     * @throws InternalException
     */
    function fetch_assoc_type($resultset, $row = null) {
        $result = $this->decode(pg_fetch_assoc($resultset, $row));
        // Mezőtípusok
        for ($i = 0; $i < pg_num_fields($resultset); $i++) {
            $name = pg_field_name($resultset, $i);
            $type = pg_field_type($resultset, $i);
            $result[$name] = $this->mapData($result[$name], $type);
        }
        return $result;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns table metadata as
     *  array(
     *     'field name' => array(
     *        'num' => <Field number starting at 1>
     *        'type' => <data type, eg varchar, int4>
     *        'len' => <internal storage size of field. -1 for varying>
     *        'not null' => <boolean>
     *        'has default' => <boolean>
     *     ),
     *       ...
     *  )
     *
     * Returns false if table not exists.
     * Database types are vendor-dependent internal types. Use {@see mapType} to convert to uMXC types.
     *
     * @param string $table
     * @return array|false -- false if table not found
     * @throws DatabaseException
     * @throws UAppException -- on connection error
     */
    public function table_metadata($table) {
        $sql = /** @lang text */
            'SELECT column_name, 
			data_type as type, 
			character_maximum_length as len, 
			ordinal_position as num,
			(column_default is not null) as "has default",
			is_nullable=\'NO\' as "not null"
		FROM information_schema.columns WHERE table_name=$1 ORDER BY column_name';
        $r = $this->select_all($result, $sql, [$table]);
        if($r===false) throw new UAppException("Error fetching metadata of table '$table'");
        if($r===0) return false; // throw new UAppException("Table '$table' does not exist.");
        if (empty($result)) return false;
        return array_combine(
            array_map(function ($v) {
                return $v['column_name'];
            }, $result),
            array_map(function ($v) {
                unset($v['column_name']);
                return $v;
            }, $result)
        );
    }

    /**
     * Must return the identifier with vendor-specific quoting
     * E.g. 'where' => '`where`'
     *
     * @param string $name
     * @return string
     * @throws ConfigurationException
     * @throws Exception
     * @throws InternalException
     */
    public function quoteIdentifier($name) {
        if (function_exists('pg_escape_identifier')) {
            $this->autoconnect();
            return pg_escape_identifier($this->connection, $name); // > php 5.4
        }
        return '"' . str_replace('"', '_', $name) . '"';
    }

    /**
     * Retruns true if database driver supports `Nulls Last` option in `Order` clause
     *
     * @return bool
     */
    public function supportsOrderNullsLast() {
        return true;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns a quoted string literal value
     *
     * @param string $literal
     * @param bool $binary -- binary string is expected (e.g. for 'bytea' field)
     * @return string
     * @throws ConfigurationException
     * @throws Exception
     * @throws InternalException
     */
    public function escape_literal($literal, $binary = false) {
        $this->autoconnect();
        if ($binary && function_exists('pg_escape_bytea')) {
            return "'" . @pg_escape_bytea($this->connection, $literal) . "'";
        }
        if (function_exists('pg_escape_literal')) {// > php 5.4.4
            $lit = @pg_escape_literal($this->connection, $literal);
            if ($lit === false) $lit = "'" . @pg_escape_bytea($this->connection, $literal) . "'";
            return $lit;
        }
        return "'" . pg_escape_string($this->connection, $literal) . "'";
    }

    /**
     * Creates a quoted literal for sql command
     *
     * Appends single quotes around the string, and replaces inner single quotes to ''
     * Returns 'NULL' on NULL input if tostring is true.
     *
     * @param mixed $value
     * @param bool $tostring -- convert null, integer and boolean to string
     * @param bool $binary -- binary string is expected
     *
     * @return string the value in '-s or 'NULL'
     * @throws DatabaseException
     * @throws InternalException
     */
    public function literal($value, $tostring = false, $binary = false) {
        if (is_object($value)) {
            if ($value instanceof DateTime) {
                if ($value->getOffset() == 0) $value = $value->format('Y-m-d H:i:s');
                else $value = $value->format('Y-m-d H:i:sP');
            } else if ($value instanceof Macaddr) {
                $value = $value->toString();
            } else if ($value instanceof Inet) {
                $value = $value->toString();
            }
        }
        return parent::literal($value, $tostring, $binary);
    }

    /**
     * Returns local name of a 'standard' operator
     *
     * @param string $op
     * @return string
     */
    public function operatorName($op) {
        if ($op == 'RLIKE') return '~*';
        if ($op == 'NOT RLIKE') return '!~*';
        if ($op == 'REGEXP') return '~';
        if ($op == 'NOT REGEXP') return '!~';
        return $op;
    }

    /**
     * Returns true if table exists int database
     * @param string $name
     * @return bool
     * @throws Exception
     * @throws InternalException
     */
    public function tableExists($name) {
        #$sql = "SELECT table_name FROM information_schema.columns WHERE table_name=$1"; // Ez csak akkor látszik, ha joga is van hozzá
        $sql = /** @lang text */
            "SELECT relname FROM pg_class WHERE relname=$1"; // Ez csak akkor is látszik, ha nincs joga hozzá
        $table_name = $this->selectvalue($sql, [$name]);
        Debug::tracex('table_name', $table_name);
        Debug::tracex('db_user', $this->db_user);
        return $table_name == $name;
    }

    /**
     * Returns php type for a connection-specific SQL type
     *
     * @param string $typename -- SQL type
     * @return string - php type or classname
     */
    function mapType($typename) {
        return ArrayUtils::getValue(self::$typeNames, $typename, 'string');
    }

    /**
     * @param string $value
     * @param string $type
     *
     * @return bool|DateTime|Inet|int|Macaddr|null|string
     * @throws InternalException
     * @throws Exception
     */
    public function mapData($value, $type) {
        if ($value === null) return null;
        try {
            switch ($type) {
                case 'int4':
                case 'int2':
                    return (int)$value;
                case 'bool':
                    return $value == 't';
                case 'timestamp':
                    $value = preg_replace('~(\d{4})[./-](\d{2})[./-](\d{2})\.?\s+(\d{1,2}):(\d{2})~', '$1-$2-$3 $4:$5', $value);
                    return new DateTime($value);
                case 'timestamp with time zone':
                case 'timestamptz':
                    return new DateTime($value); // TODO: complement, test
                case 'timestamp without time zone':
                    return new DateTime($value); // TODO: complement, test
                case 'inet':
                    return new Inet($value);
                case 'macaddr':
                    return new Macaddr($value);
                case 'bytea':
                    return substr($value, 0, 2) == '\\x' ? hex2bin(substr($value, 2)) : $value;
                default:
                    return $value;
            }
        } catch (Exception $e) {
            throw new Exception(sprintf('Failed to map data `%s` to type `%s`: %s', $value, $type, $e->getMessage()));
        }
    }

    /**
     * Returns foreign keys information of the table as
     *
     *    [
     *        'constraint_name' => [
     *            'column_name' => 'columnName',
     *            'foreign_schema' => 'schemaName',
     *            'foreign_table' => 'tableName'
     *            'foreign_column' => 'columnName'
     *        ],
     *        ...
     *  ]
     *
     * @param string $tablename
     *
     * @return array|bool
     * @throws DatabaseException
     * @throws InternalException
     */
    public function foreign_keys($tablename) {
        $sql = "SELECT
					tc.table_schema, 
					tc.constraint_name, 
					tc.table_name, 
					kcu.column_name, 
					ccu.table_schema AS foreign_schema,
					ccu.table_name AS foreign_table,
					ccu.column_name AS foreign_column 
				FROM 
					information_schema.table_constraints AS tc 
					JOIN information_schema.key_column_usage AS kcu
					  ON tc.constraint_name = kcu.constraint_name
					  AND tc.table_schema = kcu.table_schema
					JOIN information_schema.constraint_column_usage AS ccu
					  ON ccu.constraint_name = tc.constraint_name
					  AND ccu.table_schema = tc.table_schema
				WHERE tc.constraint_type = 'FOREIGN KEY' AND tc.table_name=$1";
        $this->select_all($result, $sql, [$tablename]);
        if (empty($result)) return false;
        $rr = [];
        foreach ($result as $r) $rr[$r['constraint_name']] = $r;
        return $rr;
    }

    /**
     * Returns remote foreign keys referred to this table
     *
     *    [
     *        'constraint_name' => [
     *            'remote_schema' => 'schemaName',
     *            'remote_table' => 'tableName'
     *            'remote_column' => 'columnName'
     *            'table_schema' => 'columnName',
     *            'table_name' => 'columnName',
     *            'column_name' => 'columnName',
     *        ],
     *        ...
     *  ]
     *
     * @param string $tablename
     *
     * @return array|bool
     * @throws DatabaseException
     * @throws InternalException
     */
    public function referrers($tablename) {
        $sql = "SELECT
					tc.table_schema AS remote_schema, 
					tc.constraint_name, 
					tc.table_name AS remote_table, 
					kcu.column_name AS remote_column, 
					ccu.table_schema,
					ccu.table_name,
					ccu.column_name 
				FROM 
					information_schema.table_constraints AS tc 
					JOIN information_schema.key_column_usage AS kcu
					  ON tc.constraint_name = kcu.constraint_name
					  AND tc.table_schema = kcu.table_schema
					JOIN information_schema.constraint_column_usage AS ccu
					  ON ccu.constraint_name = tc.constraint_name
					  AND ccu.table_schema = tc.table_schema
				WHERE tc.constraint_type = 'FOREIGN KEY' AND ccu.table_name=$1";
        $this->select_all($result, $sql, [$tablename]);
        if (empty($result)) return false;
        $rr = [];
        foreach ($result as $r) $rr[$r['constraint_name']] = $r;
        return $rr;
    }

}
