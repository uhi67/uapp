<?php
/**
 * Class ArrayUtils
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 *
 */

/**
 * Array-related static helper functions
 */
class ArrayUtils {
    /**
     * Paging an array array(array(attribute=>value))
     *
     * @param array $aa -- array of attributes names
     * @param string $order -- attribute name(s) with optional " desc" postfix
     * @param int $offset -- 0-based
     * @param int $limit
     * @return array
     */
    static function paginate($aa, $order, $offset, $limit) {
        if (count($aa) < 1) return $aa;
        if ($order == '') $order = 'match_quality';
        $orders = explode(',', $order);
        $dirs = [];
        foreach ($orders as $k => $order) {
            $order = trim($order);
            $dir = 1;
            if (substr($order, -5) == ' desc') {
                $dir = -1;
                $order = substr($order, 0, -5);
            }
            $orders[$k] = $order;
            $dirs[$k] = $dir;
        }
        usort($aa, function ($a, $b) use ($orders, $dirs) {
            $r = 0;
            foreach ($orders as $k => $order) {
                if ($a[$order] == $b[$order]) continue;
                return $a[$order] < $b[$order] ? -$dirs[$k] : $dirs[$k];
            }
            return $r;
        });
        return array_slice($aa, $offset, $limit);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Finds a value in the array where user function returns true
     * Arguments of user function: $item, $value
     * If return_data is false or omitted, returns index of found item, or false if not found
     * If return_data is true, returns value of found item, or null if not found
     *
     * @param array $aa -- haystack to search in
     * @param mixed $value -- value or parameter for search
     * @param callable $func -- search function($item, $value)
     * @param bool $return_data -- returns element found, instead of index (default)
     * @return mixed -- index/value of found item, or false/null if not found
     */
    static function array_usearch($aa, $value, $func, $return_data = false) {
        foreach ($aa as $k => $v) {
            if ($func($v, $value)) return $return_data ? $v : $k;
        }
        // Not found
        return $return_data ? null : false;
    }

    /**
     * Replaces all occourences of all keys of $rules array to it's values using PCRE in $aa array recursively
     *
     * @param array|string $aa
     * @param array $rules -- pattern=>replace
     * @return array -- the array with replaced values
     */
    public static function rreplace($aa, $rules) {
        if (is_array($aa)) foreach ($aa as $i => $a) $aa[$i] = self::rreplace($a, $rules);
        else if (is_string($aa)) $aa = preg_replace(array_keys($rules), array_values($rules), $aa);
        return $aa;
    }

    /**
     * Converts an object or an array of objects into an array.
     *
     * The properties specified for each class is an array of the following format:
     *
     * ```php
     * [
     *     'Post' => [
     *         'id',
     *         'title',
     *         // the key name in array result => property name
     *         'createTime' => 'created_at',
     *         // the key name in array result => anonymous function
     *         'length' => function ($post) {
     *             return strlen($post->content);
     *         },
     *     ],
     * ]
     * ```
     *
     * The result of `ArrayHelper::toArray($post, $properties)` could be like the following:
     *
     * ```php
     * [
     *     'id' => 123,
     *     'title' => 'test',
     *     'createTime' => '2013-01-01 12:00AM',
     *     'length' => 301,
     * ]
     * ```
     *
     * @param object|array|string $object the object to be converted into an array
     * @param array $properties a mapping from object class names to the properties that need to put into the resulting arrays.
     * @param bool $recursive whether to recursively converts properties which are objects into arrays.
     * @return array the array representation of the object
     */
    public static function toArray($object, $properties = [], $recursive = true) {
        if (is_array($object)) {
            if ($recursive) {
                foreach ($object as $key => $value) {
                    if (is_array($value) || is_object($value)) {
                        $object[$key] = static::toArray($value, $properties, true);
                    }
                }
            }

            return $object;
        } elseif (is_object($object)) {
            if (!empty($properties)) {
                $className = get_class($object);
                if (!empty($properties[$className])) {
                    $result = [];
                    foreach ($properties[$className] as $key => $name) {
                        if (is_int($key)) {
                            /** @noinspection PhpVariableVariableInspection */
                            $result[$name] = $object->$name;
                        } else {
                            $result[$key] = static::getValue($object, $name);
                        }
                    }

                    return $recursive ? static::toArray($result, $properties) : $result;
                }
            }
            $result = [];
            foreach ($object as $key => $value) {
                $result[$key] = $value;
            }
            return $recursive ? static::toArray($result, $properties) : $result;
        } else {
            return [$object];
        }
    }

    /**
     * Retrieves the value of an array element or object property with the given key or property name.
     * If the key does not exist in the array or object, the default value will be returned.
     *
     * The key may be specified as array like `['x', 'y', 'z']`.
     *
     * Examples
     *
     * ```php
     * // working with array
     * $username = ArrayUtils::getValue($_POST, 'username');
     * // working with object
     * $username = ArrayUtils::getValue($user, 'username');
     * // working with anonymous function
     * $fullName = ArrayUtils::getValue($user, function ($user, $defaultValue) {
     *     return $user->firstName . ' ' . $user->lastName;
     * });
     * // using an array of keys to retrieve the value
     * $value = ArrayUtils::getValue($versions, ['1.1', 'date']);
     * ```
     *
     * @param array|object $array array or object to extract value from
     * @param string|Closure|array $key key name of the array element, an array of keys or property name of the object,
     * or an anonymous function returning the value. The anonymous function signature should be:
     * `function($array, $defaultValue)`.
     * @param mixed $default the default value to be returned if the specified array key does not exist. Not used when
     * getting value from an object.
     *
     * @return mixed the value of the element if found, default value otherwise
     */
    public static function getValue($array, $key, $default = null) {
        if (is_null($array)) return $default;
        if ($key instanceof Closure) {
            return $key($array, $default);
        }

        if (is_array($key)) {
            $lastKey = array_pop($key);
            foreach ($key as $keyPart) {
                $array = static::getValue($array, $keyPart);
                if ($array === null) return $default;
            }
            $key = $lastKey;
        }

        if (is_array($array) && (isset($array[$key]) || array_key_exists($key, $array))) {
            return $array[$key];
        }

        if (is_object($array)) {
            // this is expected to fail if the property does not exist, or __get() is not implemented
            /** @noinspection PhpVariableVariableInspection */
            return $array->$key;
        } elseif (is_array($array)) {
            return (isset($array[$key]) || array_key_exists($key, $array)) ? $array[$key] : $default;
        } else {
            return $default;
        }
    }

    /**
     * Retrieves and removes the value of an array element with the given key.
     * If the key does not exist in the array, the default value will be returned.
     *
     * The key must be scalar.
     *
     * Examples
     *
     * ```php
     * // working only with array
     * $username = ArrayUtils::fetchValue($_POST, 'username');
     * ```
     *
     * @param array|null $array -- array to extract value from (null will result default)
     * @param string $key -- key name
     * @param mixed $default the default value to be returned if the specified array key does not exist
     *
     * @return mixed the value of the element if found, default value otherwise
     * @throws InternalException
     */
    public static function fetchValue(&$array, $key, $default = null) {
        if ($array === null) $array = [];
        Util::assertArray($array);

        if (isset($array[$key]) || array_key_exists($key, $array)) {
            $result = $array[$key];
            unset($array[$key]);
            return $result;
        }
        return $default;
    }

    /**
     * Shifts off the first element, or return default if not exists
     *
     * @param array &$array
     * @param mixed $default
     * @return mixed
     */
    public static function shiftDefault(&$array, $default = null) {
        $value = array_shift($array);
        if ($value === null) $value = $default;
        return $value;
    }

    /**
     * Check the given array if it is an associative array.
     *
     * If `$strict` is true, the array is associative if all its keys are strings.
     * If `$strict` is false, the array is associative if at least one of its keys is a string.
     *
     * An empty array will be considered associative only in strict mode.
     *
     *    - `isAssociative(array, false)` means the array has associative elements,
     *    - `!isAssociative(array, true)` means the array has non-associative elements.
     *
     * @param array $array the array being checked
     * @param bool $strict the array keys must be all strings and not empty to be treated as associative.
     * @return bool the array is associative
     */
    public static function isAssociative($array, $strict = true) {
        if (!is_array($array)) return false;
        foreach ($array as $key => $value) {
            if (!is_string($key) && $strict) return false;
            if (is_string($key) && !$strict) return true;
        }
        return $strict;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Finds first array element which satisfies $fn
     *
     * @param array $aa
     * @param callable $fn ($item, $key)
     *
     * @return mixed -- index of first match or false if not found
     */
    static function array_find($aa, $fn) {
        foreach ($aa as $k => $v) if ($fn($v, $k)) return $k;
        return false;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Finds first array element with key which satisfies $fn
     *
     * @param array $aa
     * @param callable $fn ($key, $item, $pars)
     * @param array $params -- more parameters for match function
     *
     * @return mixed -- index of first match or NULL if not found
     */
    static function array_find_key($aa, $fn, $params = []) {
        foreach ($aa as $k => $v) if ($fn($k, $v, $params)) return $k;
        return null;
    }

    /**
     * Returns first not-null scalar value form an array.
     * Scalar input will return itself.
     * Empty array will return null.
     * Multidimensional array will search for first scalar recursively.
     * Array keys will be ignored.
     *
     * @param mixed $value
     * @return string
     */
    static function firstScalar($value) {
        if (is_array($value)) {
            if (empty($value)) return null;
            $v = null;
            foreach ($value as $v) {
                if (is_array($v)) $v = self::firstScalar($v);
                if (!is_scalar($v)) continue;
                if ($v !== null) return $v;
            }
            return $v;
        }
        return is_scalar($value) ? $value : null;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * ## Removes item(s) from the array by value
     * Removes all items containing the given value.
     * Modifies the input array, returns the number of items removed.
     * Does not affect keys. In case of numeric keys, the result may not have successive keys.
     *
     * @param array &$array -- input/output array to modify
     * @param mixed $value -- value to remove
     * @param bool $strict -- compare the value with ===
     * @return integer
     */
    static function array_delete(&$array, $value, $strict = true) {
        $c = count($keys = array_keys($array, $value, $strict));
        foreach ($keys as $key) {
            unset($array[$key]);
        }
        return $c;
    }

    /**
     * Creates an associative array from an array of objects
     * The original keys are ignored (except of null index)
     * null indices will not generate output.
     * Multiple indices with the same value yields the value of last occurrence
     *
     * @param array|object $objects
     * @param string|callable|null $indexprop -- property name or callable($obj) for index, null will keep original key
     * @param string|callable|null $valueprop -- property name or callable($obj) for value; in case of null the original object will return
     * @return array -- mapped associative array
     * @throws Exception -- On illegal index type
     */
    static function map($objects, $indexprop, $valueprop = null) {
        $result = [];
        foreach ($objects as $key => $obj) {
            if ($indexprop === null) $index = $key;
            else $index = is_callable($indexprop) ? call_user_func($indexprop, $obj) : static::getValue($obj, $indexprop);
            if ($index !== null) {
                if (!is_scalar($index)) throw new Exception('Illegal index ' . print_r($index, true));
                $result[$index] = is_null($valueprop) ?
                    $obj :
                    (is_callable($valueprop) ? call_user_func($valueprop, $obj) : static::getValue($obj, $valueprop));
            }
        }
        return $result;
    }
}
