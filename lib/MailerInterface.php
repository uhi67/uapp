<?php
/**
 * interface MailerInterface
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * MailerInterface is implemented by mailer components, which can send an e-mail to the adressee.
 * You may configure a mailer component in your main config.
 *
 * @see Mailer
 * @see Fakemailer
 */
interface MailerInterface {
    /**
     * Sends a mail to the adress
     * Must return true on success
     *
     * @param string $address -- e-mail cím (mailto: prefix NEM lehetséges)
     * @param string $subject
     * @param string $message
     * @param string $headers
     * @return bool
     */
    public function send($address, $subject, $message, $headers = '');
}
