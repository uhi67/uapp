<?php
/**
 * class UAppException
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * UAppException
 *
 * A Base Exception class for all exceptions thrown within UApp framework and application
 * Has informational methods which may be implemented by descendants.
 *    - getExtra() - It's used to show debug information in debug mode.
 *  - getSubTitle()
 *  - getName()
 *
 * Descendants
 *
 * - BaseException -- for logging (deprecated)
 * - UserException -- displayable for end user
 *      - NotFoundException
 *        - ForbiddenException
 *        - InvalidParamsException
 *        - ServerErrorException
 *            - ConfigurationException
 *
 * - InternalException -- for framework internal errors, not for translation
 *        * ModelException -- for Model class and descendants
 *        * QueryException -- for Query class
 *        * DatabaseException -- for DBX, provides SQL command in getExtra()
 */
class UAppException extends Exception {
    /** @var string|array $info -- extra message block(s) connected to the message with additional informations displayed to the end user */
    public $info;

    /**
     * Returns an extra info to conditional display, e.g. the SQL command
     * For debug purposes only
     *
     * @return string|null -- returns null if no extra info.
     */
    public function getExtra() {
        return get_class($this);
    }

    public function getSubtitle() {
        return 'An unspecified internal error occured';
    }

    public function getName() {
        return 'Unspecified application error';
    }

    public function showTrace() {
        return true;
    }

    public function getInfo() {
        return $this->info;
    }

    public function getInfoArray() {
        return is_array($this->info) ? $this->info : [$this->info];
    }
}
