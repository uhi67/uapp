<?php
/**
 * Class BaseException
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 *
 */

/**
 * A Base Exception class
 * Features:
 *  - writes an error logfile with exception details.
 *
 * @deprecated use UAppException
 */
class BaseException extends UAppException {
    /**
     * @param string $message -- Üzenet, paraméterek %s,  %2$s alakban
     * @param array $params -- logolandó (egyben $message paraméter)
     * @param array $info -- tájékoztató ($message paraméterek folytatása)
     * @param array $reason -- not used
     * @return void
     */
    public function __construct($message, $params = null, $info = [], $reason = null) {
        if ($info === null) $info = [];
        $info = Util::forceArray($info);
        if ($params) $message = vsprintf($message, array_merge($params, $info));
        parent::__construct((string)$message);
        $this->params = is_array($params) ? $params : [$params];
        $this->info = Util::forceArray($info); //($info) ? $info : array($info);
    }

    public function getExtra() {
        return $this->info;
    }

    public function getSubtitle() {
        return 'Internal error';
    }
}
