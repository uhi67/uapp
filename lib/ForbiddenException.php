<?php
/**
 * class ForbiddenException
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * ForbiddenException represents a "Forbidden" HTTP exception with status code 403.
 *
 * Use this exception when a user has been authenticated but is not allowed to
 * perform the requested action. If the user is not authenticated, consider
 * using a 401 [[UnauthorizedHttpException]]. If you do not want to
 * expose authorization information to the user, it is valid to respond with a
 * 404 [[NotFoundException]].
 *
 * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.4
 */
class ForbiddenException extends UserException {
    /**
     * @param string|array -- $message message text or [message, params, info]
     * @param array $extra -- debug details as array($type, $object, $effective)
     * @param Exception $previous The previous exception used for the exception chaining.
     */
    public function __construct($message = null, $extra = null, $goto = null) {
        parent::__construct($message, $extra, $goto, 403);
    }

    public function getSubtitle() {
        return UApp::la('uapp', 'You have no permission for this action.');
    }
}
