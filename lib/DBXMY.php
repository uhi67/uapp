<?php
/**
 * class DBXMY
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */
/** @noinspection PhpUnused */
if (!function_exists('mysqli_connect')) {
    fatal_error(500, 'Internal Server Error / Belső szerverhiba', 'MySQL not supported / MySQL támogatás hiányzik.');
}

/**
 * MySQL implementation for UApp
 *
 * ##Example
 * ```
 * $connection = Component::create([
 *     'DBXMY',
 *     'name' => 'uapp_test',
 *     'user' => 'uapp',
 *     'password' => '****',
 *     'encoding' => 'UTF-8',
 * ]);
 * ```
 */
class DBXMY extends DBX {
    private $db_last_error;

    /**
     * @var array $typeNames -- sql_type => common_type mapping
     * common type names:
     *  - basic php types (lowercase)
     *  - 'xml' denotes xml valid string
     *  - php classnames (Uppercase, eg. DateTime)
     *  - 'string' is default, not indicated
     *  - date (DateTime without time)
     *  - time (DateTime without date or integer in sec)
     */
    public static $typeNames = [
        1 => 'tinyint',
        2 => 'smallint',
        3 => 'int',
        4 => 'float',
        5 => 'double',
        7 => 'timestamp',
        8 => 'bigint',
        9 => 'mediumint',
        10 => 'date',
        11 => 'time',
        12 => 'DateTime',
        13 => 'year',
        16 => 'bit',
        252 => 'string',
        //252 is currently mapped to all text and blob types (MySQL 5.0.51a)
        253 => 'string',
        254 => 'string',
        246 => 'double'
    ];

    /**
     * DBXMY constructor.
     *
     * @param $db_host
     * @param $db_port
     * @param $db_user
     * @param $db_password
     * @param $db_name
     * @param string $db_encoding
     * @param bool $connectnow
     *
     * @throws ConfigurationException
     * @throws DatabaseException
     * @throws InternalException
     */
    function __construct($db_host, $db_port = '', $db_user = '', $db_password = '', $db_name = '', $db_encoding = 'UTF8', $connectnow = false) {
        $db_type = 'MY';
        parent::__construct($db_host, $db_port, $db_user, $db_password, $db_name, $db_encoding, $connectnow, $db_type);
    }

    /**
     * @param string $db_host
     * @param string $db_port
     * @param string $db_user
     * @param string $db_password
     * @param string $db_name
     * @param string $db_encoding
     *
     * @return mixed|mysqli|null|resource
     * @throws Exception
     */
    protected function connect($db_host, $db_port, $db_user, $db_password, $db_name, $db_encoding = 'UTF8') {
        Debug::tracex('dbname', $db_name);
        #$hostx = $db_host ? " host=$db_host " : '';
        $this->connection = mysqli_connect($db_host, $db_user, $db_password, $db_name, $db_port);
        if (!$this->connection) {
            $this->connerr = true;
            throw new Exception('Database connection error');
        } else mysqli_set_charset($this->connection, $db_encoding);
        return $this->connection;
    }


    function close() {
        if ($this->connection) mysqli_close($this->connection);
    }

    function error($rs = null) {
        $e = mysqli_error($this->connection);
        if (!$e) $e = $this->db_last_error;
        else $this->db_last_error = $e;
        return $e;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @param mysqli $rs
     *
     * @return int
     * @throws Exception
     */
    function affected_rows($rs) {
        if (!$rs) throw new Exception('Invalid result set');
        return mysqli_affected_rows($rs);
    }

    /**
     * Run SQL query substituting $1, $2 style parameters
     *
     * @param string $sql
     * @param array $params -- $1 parameter has 0-index
     *
     * @return bool|mysqli_result
     * @throws ConfigurationException
     * @throws DatabaseException
     * @throws Exception
     * @throws InternalException
     */
    function query($sql, $params = NULL) {
        $this->autoconnect();
        if ($params) $sql = $this->bindParams($sql, $params);
        if (!($r = mysqli_query($this->connection, $sql))) $this->errtrace($sql);
        return $r;
    }

    /**
     * @param mysqli_result $resultset
     * @param int $row
     * @param int $field
     *
     * @return array|bool|string
     */
    function result($resultset, $row, $field = 0) {
        if (!$this->connection) return false;
        mysqli_data_seek($resultset, $row);
        #$rowid = 'my_'.spl_object_hash($resultset).'_'.$row;
        $row = mysqli_fetch_assoc($resultset);
        if (is_numeric($field)) {
            $row1 = array_values($row);
            $r = $row1[$field];
        } else $r = $row[$field];
        return $r;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    function result_status() {
        if (!$this->connection) return false;
        // TODO:
        return false;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @param $rs
     */
    function free_result($rs) {
        mysqli_free_result($rs);
    }

    /**
     * @param mysqli_result $resultset
     *
     * @return int
     */
    function fields($resultset) {
        return mysqli_num_fields($resultset);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @param mysqli_result $resultset
     *
     * @return int
     * @throws Exception
     */
    function num_rows($resultset) {
        if (!$resultset) throw new Exception($this->error());
        $n = @mysqli_num_rows($resultset);
        if ($n === FALSE) {
            throw new Exception($this->error());
        }
        return $n;
    }

    function lastoid(/** @noinspection PhpUnusedParameterInspection */ $recordset) {
        return false;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @param string $recordset
     * @param string $table
     * @param string $fieldname
     *
     * @return int|mixed|null
     * @throws InternalException
     */
    function last_id($recordset = '', $table = '', $fieldname = '') {
        return $this->selectvalue("select lastval()");
    }

    /**
     * @param mysqli_result $resultset
     * @param int $i
     *
     * @return string
     */
    function fieldname($resultset, $i) {
        $fx = mysqli_fetch_field_direct($resultset, $i);
        return $fx->name;
    }

    /**
     * Must return the common type of a field in the resultset
     *
     * @param mysqli_result $resultset
     * @param integer $i
     * @return string
     */
    function fieldtype($resultset, $i) {
        $fx = mysqli_fetch_field_direct($resultset, $i);
        return self::mapType($fx->type);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Elhoz egy sort indexelt tĂ¶mbbe
     *
     * @param mysqli_result $resultset
     * @param int $row -- sor indexe (0-tĂłl) -- default: kĂ¶vetkezĹ‘ sor
     * @return array -- stringek; vagy false
     *
     * An array, indexed from 0 upwards, with each value represented as a string. Database NULL values are returned as NULL.
     * FALSE is returned if row exceeds the number of rows in the set, there are no more rows, or on any other error.
     */
    function fetch_row($resultset, $row = null) {
        mysqli_data_seek($resultset, $row);
        return $this->decode(mysqli_fetch_row($resultset));
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @param mysqli_result $resultset
     * @param null $row
     *
     * @return array|string
     */
    function fetch_assoc($resultset, $row = null) {
        mysqli_data_seek($resultset, $row);
        return $this->decode(mysqli_fetch_assoc($resultset));
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns a record to associative array
     * Fields will be converted to mapped php types
     *
     * @param mysqli_result $resultset
     * @param null $row
     *
     * @return array
     * @throws Exception
     */
    function fetch_assoc_type($resultset, $row = null) {
        if ($row) mysqli_data_seek($resultset, $row);
        $data = mysqli_fetch_row($resultset);

        /** @var array $fields -- array of field definitions */
        $fields = mysqli_fetch_fields($resultset);

        $result = [];
        foreach ($fields as $i => $field) {
            $name = $field->name;
            $type = $field->type;
            $value = $data[$i];
            $result[$name] = $this->mapData($value, $type);
        }
        return $result;
    }

    /**
     * Must return the identifier with vendor-specific quoting
     * E.g. 'where' => '`where`'
     *
     * @param string $name
     * @return string
     */
    public function quoteIdentifier($name) {
        return '`' . $name . '`';
    }

    public function supportsOrderNullsLast() {
        return false;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @param string $v
     *
     * @return string
     * @throws ConfigurationException
     * @throws InternalException
     */
    public function escape_literal($v, $binary = false) {
        $this->autoconnect();
        return "'" . mysqli_escape_string($this->connection, $v) . "'";
        #return "'".str_replace("'", "''", str_replace("\\", "\\\\", str_replace("\\'", "'", $v)))."'";
    }

    /**
     * Returns php type for a connection-specific SQL type
     *
     * @param string $typename -- SQL type
     * @return string - php type or classname
     */
    function mapType($typename) {
        return ArrayUtils::getValue(self::$typeNames, $typename, 'string');
    }

    /*
     * Replaces common operator names to MYSQL version
     */
    function operatorName($op) {
        return $op;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    function tableExists($name) {
        $result = true;
        try {
            $this->table_metadata($name);
        } catch (Exception $e) {
            $result = false;
        }
        return $result;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Returns table metadata for function {@see DBX::getMetaData()}
     * (Associative to field names)
     *
     * @param string $table
     *
     * @return array|boolean -- returns false if table does not exist
     * @throws ConfigurationException
     * @throws InternalException
     */
    function table_metadata($table) {
        $this->autoconnect();
        try {
            $rs = mysqli_query($this->connection, 'show fields from ' . $table);
        } catch (mysqli_sql_exception $e) {
            return false;
        }
        if (!$rs) return false;
        $i = 1;
        $result = [];
        while ($row = $rs->fetch_assoc()) {
            $type = $row['Type'];
            $len = -1;
            if (preg_match('/(\w+)\((\d+)\)/', $type, $mm)) {
                $type = $mm[1];
                $len = (int)$mm[2];
            }
            $result[$row['Field']] = [
                'num' => $i++,
                'type' => $type,
                'len' => $len,
                'not null' => $row['Null'] == 'NO',
                'has default' => $row['Default'] !== null,
            ];
        }
        return $result;
    }

    function beginTransaction() {
        mysqli_begin_transaction($this->connection);
    }

    function commit() {
        mysqli_commit($this->connection);
    }

    function rollBack() {
        mysqli_rollback($this->connection);
    }

    /**
     * Converts a value from MySQL data type to php data type
     *
     * @param string $value
     * @param string $type
     *
     * @return DateTime|float|int|mixed|string|null
     * @throws Exception
     */
    public function mapData($value, $type) {
        if ($value === null) return null;
        switch ($type) {
            case MYSQLI_TYPE_DECIMAL:
            case MYSQLI_TYPE_NEWDECIMAL:
            case MYSQLI_TYPE_FLOAT:
            case MYSQLI_TYPE_DOUBLE:
                return (double)$value;
            case MYSQLI_TYPE_BIT:
            case MYSQLI_TYPE_TINY:
            case MYSQLI_TYPE_SHORT:
            case MYSQLI_TYPE_LONG:
            case MYSQLI_TYPE_LONGLONG:
            case MYSQLI_TYPE_INT24:
            case MYSQLI_TYPE_YEAR:
            case MYSQLI_TYPE_ENUM:
                return (int)$value;

            case MYSQLI_TYPE_TIMESTAMP:
            case MYSQLI_TYPE_DATE:
            case MYSQLI_TYPE_TIME:
            case MYSQLI_TYPE_DATETIME:
            case MYSQLI_TYPE_NEWDATE:
            case MYSQLI_TYPE_INTERVAL:
                return new DateTime($value);

            case MYSQLI_TYPE_SET:
            case MYSQLI_TYPE_VAR_STRING:
            case MYSQLI_TYPE_STRING:
            case MYSQLI_TYPE_CHAR:
            case MYSQLI_TYPE_GEOMETRY:
            case MYSQLI_TYPE_TINY_BLOB:
            case MYSQLI_TYPE_MEDIUM_BLOB:
            case MYSQLI_TYPE_LONG_BLOB:
            case MYSQLI_TYPE_BLOB:
            default:
                return $value;
        }
    }

    /**
     * @inheritDoc
     */
    public function foreign_keys($tablename) {
        // TODO: Implement foreign_keys() method.
    }

    /**
     * @inheritDoc
     */
    public function referrers($tablename) {
        // TODO: Implement referrers() method.
    }
}
