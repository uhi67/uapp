<?php
/**
 * class SamlUser
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/** @noinspection PhpUnused */

/**
 * # SamlUser - User auhtenticated by SAML authsource
 *
 * ### Requirements
 * - SimplesamlPhp >= v1.9
 *
 * ### Required SAML attributes
 * - `schacHomeOrganization` or `o` or `eduPersonScopedAffiliation` (one of them is mandatory)
 * - `uid` or attribute specified in `config/saml/uidfield` (one of them is mandatory)
 * - `eduPersonTargetedID` or `eduPersonPrincipalName` (one of them is mandatory)
 * - `mail`
 * - `displayName` or `cn` or `givenName` + `sn`
 * - `jpegPhoto`
 *
 * @property string $jpegPhoto
 */
class SamlUser extends BaseUser {
    /** @var string $uid Remote user id, by config/saml/uidfield */
    public $uid;
    /** @var string eduPersonTargetedID */
    public $tid;
    /** @var string $org auth organization */
    protected $org;
    /** @var string $idp authenticator entity */
    protected $idp;
    /** @var SimpleSAML\Auth\Simple $auth */
    public $auth;
    /** @var array $attributes SAML attributes */
    public $attributes;
    /** @var bool $isAuth -- true if user is authenticated */
    protected $isAuth;
    /** @var string $scope -- user's current scope from SAML aut source */
    public $scope;
    /** @var string $jpegPhoto -- user's photo from SAML */
    public $jpegPhoto;

    /** @noinspection PhpMissingParentConstructorInspection */

    /**
     * SamlUser constructor.
     *
     * @param SimpleSAML\Auth\Simple $as
     *
     * @throws ConfigurationException
     * @throws InternalException
     * @throws ReflectionException
     */
    function __construct($as) {
        $this->auth = $as;
        $this->isAuth = $this->auth->isAuthenticated();
        if (Util::getSession('trace') != '') ini_set('display_errors', 'stdout');
        if (array_key_exists('login', $_REQUEST) && !$this->isAuth) {
            $this->auth->requireAuth();
        }
        if ($this->isAuth) {
            $this->loadUser();
        }
        if (array_key_exists('logout', $_REQUEST)) {
            $this->id = (int)Util::getSession('user_id');
            $this->logout(); // never returns
        }
    }

    function finish() {
        return parent::finish();
    }

    function loadSession() {
        $this->id = (int)Util::getSession('user_id');
        $this->uid = Util::getSession('user_uid');
        $this->name = Util::getSession('user_name');
        $this->org = Util::getSession('user_org');
        $this->idp = Util::getSession('user_idp');
    }

    function loggedIn() {
        return $this->isAuth;
    }

    /**
     * @param UXMLElement|Xlet|UAppPage $parent
     * @param array|null $options
     *
     * @return DOMElement
     * @throws InternalException
     */
    function createNode($node_parent = null, $options=null) {
        $attr = [];
        if ($this->name) $attr['name'] = $this->name;
        if ($this->id) $attr['id'] = $this->id;
        if ($this->uid) $attr['uid'] = $this->uid;
        if ($this->enabled) $attr['enabled'] = $this->enabled;
        return $this->node = $this->parentNode($node_parent)->addNode('user', $attr);
    }

    function logout() {
        $this->emptyuser();
        Rights::reload();
        $http = new SimpleSAML\Utils\HTTP;
        /** @noinspection PhpDeprecationInspection */
        $this->auth->logout($http->getSelfURLNoQuery()); /* will never return. */
    }

    function emptyuser() {
        $_SESSION = [];
        session_write_close();
        /*
        $_SESSION['user_id'] = $this->id = 0;
        $_SESSION['user_uid'] = $this->uid = null;
        $_SESSION['user_name'] = $this->name = null;
        $_SESSION['user_email'] = $this->email = null;
        $_SESSION['user_org'] = $this->org = null;
        $_SESSION['user_idp'] = $this->idp = null;
        */
    }

    /**
     * ## User::loadUser()
     * Loads user data into session after login
     *
     * @return void
     * @throws ConfigurationException
     * @throws InternalException
     * @throws ReflectionException
     */
    protected function loadUser() {
        $this->loadSession();
        $this->attributes = $this->auth->getAttributes();
        #Debug::tracex('attributes', array_keys($this->attributes));
        #Debug::tracex('attributes', $this->attributes);

        // Determine name of home organization
        $edupersonScopedAffiliation = isset($this->attributes['eduPersonScopedAffiliation']) && $this->attributes['eduPersonScopedAffiliation'][0] ? $this->attributes['eduPersonScopedAffiliation'][0] : '';
        $this->org = isset($this->attributes['o']) ? $this->attributes['o'][0] : null;
        if (!$this->org && isset($this->attributes['schacHomeOrganization'])) $this->org = $this->attributes['schacHomeOrganization'][0];
        if (!$this->org && $edupersonScopedAffiliation) $this->org = preg_replace('/^[^@]*@/', '', $edupersonScopedAffiliation);

        $this->getIdp();
        #Debug::tracex('attributes', $this->attributes);
        $uidfield = UApp::getInstance()->config('saml/uidfield', 'uid');
        if (is_array($uidfield)) {
            foreach ($uidfield as $uidf) {
                if (isset($this->attributes[$uidf])) {
                    $uidfield = $uidf;
                    break;
                }
            }
            if (is_array($uidfield)) throw new InternalException(UApp::la('uapp', 'Missing user attributes `[{$attr}]`, given `{$set}`', ['attr' => implode(', ', $uidfield), 'set' => Util::objtostr($this->attributes)]));
        } else {
            if (!isset($this->attributes[$uidfield])) {
                throw new InternalException(UApp::la('uapp', 'Missing user attribute `{$attr}`, given `{$set}`', ['attr' => $uidfield, 'set' => Util::objtostr($this->attributes)]));
            }
        }
        $targeteidattribute = Util::coalesce(
            ArrayUtils::getValue($this->attributes, 'eduPersonTargetedID', false),
            ArrayUtils::getValue($this->attributes, 'eduPersonPrincipalName', false)
        );
        if (!$targeteidattribute) throw new InternalException(UApp::la('uapp', 'Missing eduPersonPrincipalName or eduPersonTargetedID attribute.'));

        #Debug::tracex('uidvalue', $this->attributes[$uidfield]);
        $this->uid = $this->attributes[$uidfield][0];
        $this->tid = $targeteidattribute[0];
        Debug::tracex('tid', $this->tid);
        if (is_array($this->tid)) {
            $this->scope = parse_url(ArrayUtils::getValue($this->tid, 'NameQualifier'), PHP_URL_HOST);
            $this->tid = $this->tid['Value'];
        } else {
            $this->scope = Util::substring_after($this->tid, '@');
        }
        /** @var array $mails */
        $mails = ArrayUtils::getValue($this->attributes, 'mail');
        $this->email = ArrayUtils::getValue($mails, 0);

        $this->enabled = 0;
        #Debug::tracex('uid', $this->uid);
        if (!isset($_SESSION['user_uid']) || $this->uid != $_SESSION['user_uid']) $this->getUserData();
    }

    /**
     * Betölti a felhasználói attributumokat SAML-ból vagy más forrásból a session-be
     *
     * @return bool - success
     */
    protected function getUserData() {
        /* Override it in DbUser */
        // First, load current object data (got from SAML)
        #Debug::trace('load from SAML');
        if (isset($this->attributes['jpegPhoto'])) $this->jpegPhoto = $this->attributes['jpegPhoto'];
        unset($this->attributes['jpegPhoto']);
        $_SESSION['attributes'] = $this->attributes;
        $_SESSION['user_uid'] = $this->uid;
        $_SESSION['user_id'] = $this->id;
        $this->name = coalesce(
            isset($this->attributes['displayName']) ? $this->attributes['displayName'][0] : null,
            isset($this->attributes['cn']) ? $this->attributes['cn'][0] : null,
            isset($this->attributes['givenName']) && isset($this->attributes['sn']) ?
                $this->attributes['givenName'][0] . ' ' . $this->attributes['sn'][0] : null,
            'anonymous');
        $_SESSION['user_name'] = $this->name;
        $_SESSION['user_org'] = $this->org;
        $_SESSION['user_idp'] = $this->idp;
        $_SESSION['user_idp'] = $this->idp;
        return true;
    }

    /**
     * Returns idp entity identifier of the IdP identified the current user
     *
     * @return NULL|string
     */
    function getIdp() {
        $this->idp = null;
        if (method_exists($this->auth, 'getAuthData')) {
            $this->idp = $this->auth->getAuthData('saml:sp:IdP');
            #Debug::tracex('idp1', $this->idp);
        } else if (method_exists('SimpleSAML_Session', 'getInstance')) {
            // SimpleSamlPhp Older than 1.9 version
            /** @var SimpleSAML_Session $session */
            /** @noinspection PhpUndefinedMethodInspection */
            $session = SimpleSAML_Session::getInstance();
            /** @noinspection PhpUndefinedMethodInspection */
            $this->idp = $session->getIdP();
            #Debug::tracex('idp2', $this->idp);
        }
        return $this->idp;
    }
}

function coalesce($a) {
    if ($a) return $a;
    for ($i = 1; $i < func_num_args(); $i++) {
        if ($b = func_get_arg($i)) return $b;
    }
    return false;
}
