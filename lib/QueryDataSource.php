<?php
/**
 * class QueryDataSource
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * # Class QueryDataSource
 *
 * A data source based on a Query
 *
 * ### Using
 *
 * ```
 * $ds = new QueryDataSource(array(
 *        'query' => new Query()
 * ));
 * $list = new UList(array('dataSource'=>$ds));
 * $list->createNode($this);
 * ```
 *
 * ### All parameters
 * - **query** (Query) -- the query the datasource is based on. Must be of type 'select' or 'sql'
 * - **modelClass** (string) -- name of model class implemented by the Query (if any). Default is the Query's modelname. false to disable.
 * - **countQuery** (Query) -- the query which computes total number of items from database. Optional, default computed from $query
 * - **patternSetter** (callable) -- function($dataSource, $pattern, $params) -- called when search pattern is set. Default no effect
 */
class QueryDataSource extends Component implements DataSourceInterface {
    /** @var Query $query -- the query which fetches data from database */
    public $query;
    /** @var string $modelClass -- name of model class implemented by the Query (if any). Specify false to disable auto-using Query's one */
    public $modelClass;
    /** @var Query $countQuery -- the query which computes total number of items from database. Optional, default computed from $query */
    public $countQuery;
    /** @var callable $patternSetter -- function($dataSource, $pattern, $params) -- called when search pattern is set */
    public $patternSetter;

    /**
     * @inheritdoc
     * @throws InternalException
     * @throws Exception
     */
    public function prepare() {
        if ($this->query && !$this->countQuery) {
            $type = $this->query->type;
            if ($type == 'sql') {
                // SQL based
                $this->countQuery = new Query([
                    'type' => 'sql',
                    'sql' => 'select count(*) from (' . $this->query->sql . ') query',
                    'params' => $this->query->params,
                    'connection' => $this->query->connection,
                ]);
            } else if ($type == 'select') {
                if ($this->query->groupby) {
                    if (count($this->query->groupby) == 1) {
                        $connection = $this->query->connection;
                        if (!$connection) throw new QueryException('Query for QueryDataSource must have valid db connection');
                        // Pontosan egy GROUP BY kifejezés
                        $this->countQuery = new Query([
                            'params' => $this->query->params,
                            'connection' => $this->query->connection,
                            'type' => 'select',
                            // This construct may be vendor-specific...
                            'fields' => ['(count(distinct ' . $connection->quoteName($this->query->groupby[0]) . '))'],
                            'from' => $this->query->from,
                            'joins' => $this->query->joins,
                            'where' => $this->query->condition,
                        ]);
                    } else {
                        // Több GROUP BY kifejezés
                        $this->countQuery = new Query([
                            'connection' => $this->query->connection,
                            'type' => 'select',
                            'fields' => ['(count(*))'],
                            'from' => ['main' => new Query([
                                'params' => $this->query->params,
                                'connection' => $this->query->connection,
                                'type' => 'select',
                                'fields' => $this->query->groupby,
                                'from' => $this->query->from,
                                'joins' => $this->query->joins,
                                'where' => $this->query->condition,
                                'groupby' => $this->query->groupby,
                            ])],
                        ]);
                    }
                } else {
                    // Nincs GROUP BY kifejezés
                    $this->countQuery = new Query([
                        'params' => $this->query->params,
                        'connection' => $this->query->connection,
                        'type' => 'select',
                        'fields' => ['(count(*))'],
                        'from' => $this->query->from,
                        'joins' => $this->query->joins,
                        'where' => $this->query->condition,
                    ]);
                }
            } else throw new InternalException('Invalid query for data source');
        }

        if ($this->modelClass === null && $this->query->modelname) $this->modelClass = $this->query->modelname;
    }

    /**
     * @param $params
     *
     * @return $this
     * @throws QueryException
     */
    public function bind($params) {
        $this->query->bind($params);
        $this->countQuery->bind($params);
        return $this;
    }

    /**
     * Total number of items in the set
     *
     * @return int the number of items in the set
     *
     * @throws InternalException
     * @throws QueryException
     */
    public function getCount() {
        try {
            return $this->countQuery->scalar();
        } catch (DatabaseException $e) {
            throw new InternalException('Invalid count query (see previous exception)', $this->countQuery->toString(), $e);
        }
    }

    /**
     * Returns the specified section of items
     * Returns array of Models if modelClass is specified, array of arrays otherwise.
     *
     * @param int $start -- 0-based offset
     * @param int $count -- number of items
     * @param array $orders -- order array for Query
     *
     * @return array -- the list of items
     * @throws DatabaseException
     * @throws InternalException
     * @throws QueryException
     */
    public function fetch($start, $count, $orders) {
        if ($this->query->fields === '') throw new QueryException('Invalid fields definition');
        return $this->query->offset($start)->limit($count)->orderBy($orders)->all($this->modelClass);
    }

    /**
     * @param string $pattern
     * @param array $params
     * @return QueryDataSource
     */
    public function setPattern($pattern, $params = null) {
        if (is_callable($this->patternSetter)) {
            call_user_func($this->patternSetter, $this, $pattern, $params);
        }
        return $this;
    }

    public function setModelClass($modelClass) {
        $this->modelClass = $modelClass;
    }

    public function getModelClass() {
        return $this->modelClass;
    }
}
