<?php
/**
 * class ModelException
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * ModelException
 * @see UAppException
 */
class ModelException extends InternalException {
}
