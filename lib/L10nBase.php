<?php
/**
 * class L10nBase
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * L10nBase
 * Template (and base and default class) for L10n functions
 *
 * You must provide your class for L10n functionality and configure classname in the config file at 'L10n' key.
 * Your class must provide getText and formatDate for UApp::la and UApp::fd functions
 * The current language and locale may be set and get via UApp::setLocale/getLocale/getLang
 *
 * This template does not use database, and does not translate any text.
 * Uses IntlDateFormatter for formatting dates.
 *
 * ### Configuration
 *
 * ```
 * 'l10n' => array(
 *        'class' => 'L10nBase', // or descendant
 *        'uappDir' => $uapppath.'/def/translations', // Place of translation files. This is the default
 *        'language' => 'hu',        // Default language with optional locale, may be changed by UApp::setLang(lang/locale)
 *        'source' => 'hu',        // Default source language, default is 'en'
 * ),
 * ```
 *
 * ### Translation file format
 *
 * ```
 * return array(
 *        'original text' => 'eredeti szöveg',
 *        ...
 * );
 * ```
 */
class L10nBase extends StaticComponent {
    private static $_messages;
    /** @var string $dir -- directory of translation files in form 'la.php', default is def/translations */
    public static $uappDir;
    /** @var string $language -- Default language with optional locale, may be changed by UApp::setLang(lang/locale) */
    public static $language;
    /** @var string $source -- Default source language, default is 'en' */
    public static $source;
    /** @var string $timezone -- Time zone, default is 'Europe/Budapest', used by BaseModel */
    public static $timezone;

    /**
     * {@inheritdoc}
     * @throws ConfigurationException
     * @throws InternalException
     * @throws Exception
     */
    public static function prepare() {
        if (static::$uappDir) {
            if (substr(static::$uappDir, -1) != '/') static::$uappDir .= '/';
        } else {
            static::$uappDir = UApp::getPath() . '/def/translations/';
            if (!Util::makeDir(static::$uappDir)) {
                throw new InternalException('Configuration error: directory does not exists for translation class: ' . static::$uappDir);
            }
        }
        if (!static::$timezone) static::$timezone = 'Europe/Budapest';
    }

    /**
     * localized system text
     *
     * @param string $category -- message category, UApp uses 'uapp'. Application default is 'app'
     * @param string $source - source language text or text identifier
     * @param array $params - replaces $var parameters
     * @param integer $lang - language code or user default language (ll or ll-LL) to translate into
     *
     * @return string
     * @throws InternalException
     */
    public static function getText($category, $source, $params = NULL, $lang = null) {
        if ($category == 'uapp') {
            if (!$lang) $lang = UApp::getLocale();
            $text = static::getTextFile(static::$uappDir, $source, $lang);
        } else {
            // Default is shortcut solution
            $text = $source;
        }
        // substitute parameters
        if ($params && !is_array($params)) $params = [$params];
        return Util::substitute($text, $params);
    }

    /**
     * Formats a date for given locale
     * Hungarian dates will be without extra spaces (yyyy.MM.dd. H:mm)
     *
     * @param DateTime $datetime
     * @param int $datetype -- date format as IntlDateFormatter::NONE, type values are 'NONE', 'SHORT', 'MEDIUM', 'LONG', 'FULL'
     * @param int $timetype -- time format as IntlDateFormatter::NONE, type values are 'NONE', 'SHORT', 'MEDIUM', 'LONG', 'FULL'
     * @param string $locale -- locale in ll-cc format (ISO 639-1 && ISO 3166-1)
     * @return string
     */
    public static function formatDate($datetime, $datetype, $timetype, $locale = null) {
        if (!$locale) $locale = "en-GB";
        $pattern = null;
        if (substr($locale, 0, 2) == 'hu') {
            if ($datetype == IntlDateFormatter::SHORT && $timetype == IntlDateFormatter::SHORT)
                $pattern = 'yyyy.MM.dd. H:mm';
            if ($datetype == IntlDateFormatter::SHORT && $timetype == IntlDateFormatter::NONE)
                $pattern = 'yyyy.MM.dd.';
        }
        $formatter = new IntlDateFormatter($locale, $datetype, $timetype, null, null, $pattern);
        return $formatter->format($datetime);
    }

    /**
     * Translates an uapp category text using built-in translation file in $dir
     * Does not substitute parameters.
     * If language definition does not exist, returns original without error.
     * If specific text does not exist, returns original with an appended '*'.
     *
     * @param string $dir
     * @param string $source -- text in original language
     * @param string $lang -- language to translate to
     *
     * @return string
     */
    public static function getTextFile($dir, $source, $lang) {
        if (!self::$_messages) self::$_messages = [];
        if (!isset(self::$_messages[$lang])) {
            $la = $lang;
            if (strlen($lang) == 5 && !file_exists($dir . $lang . '.php')) $la = substr($lang, 0, 2);
            $messagefile = $dir . $la . '.php';
            if (!file_exists($messagefile)) return $source;
            self::$_messages[$lang] = include($messagefile);
        }
        if (!isset(self::$_messages[$lang][$source])) return $source . '*';
        return self::$_messages[$lang][$source];
    }
}
