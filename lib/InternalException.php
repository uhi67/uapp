<?php
/**
 * class InternalException
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/** @noinspection PhpClassNamingConventionInspection */

/**
 * InternalException represents an application internal error
 * has a parameter to extra info to display on debug
 * @see UAppException
 */
class InternalException extends UAppException {
    protected $extra;

    /**
     * @param string|array $message message text or [message, params, info]
     * @param string $extra
     * @param Exception $previous
     */
    public function __construct($message, $extra = null, $previous = null) {
        parent::__construct($message, 1, $previous);
        $this->extra = $extra;
    }

    public function getExtra() {
        return $this->extra;
    }
}
