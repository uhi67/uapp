<?php
/**
 * class Pint
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * PInt class handles packed arbitrary length integers
 * These integers are represented as strings. Any string represents a valid intger with it's characters bits.
 * In fact, it's a base 256 system.
 *
 * The first character is the most significant byte.
 *
 * The same format is generated using inet_pton() function.
 */
class PInt {
    /**
     * Creates a packed integer from integer
     * The first character is the most significant byte.
     *
     * @param int $i
     *
     * @return string
     */
    static function pack($i) {
        if (!is_int($i)) return false;
        if ($i < 0) return false;
        $r = '';
        while ($i) {
            $b = $i % 256;
            $r = chr($b) . $r;
            $i = $i >> 8;
        }
        return $r;
    }

    /**
     * Creates an integer from packed, if possible.
     * The first character is the most significant byte.
     *
     * @param string $p
     * @return integer|boolean -- return false on failure
     */
    static function unpack($p) {
        if ($p === false) return false;
        $r = 0;
        for ($i = 0; $i < strlen($p); $i++) {
            $r = ($r << 8) + ord(substr($p, $i, 1));
        }
        return $r;
    }

    /**
     * Computes a bitwise AND from (multiple) arguments
     *
     * Single array argument may hold multiple arguments as well.
     * Zero arguments results zero ('')
     * Result will be truncated to minimal length of bytes.
     *
     * This is equivalent with php expression: $p1 & $p2 & ...
     *
     * param string|array $arg -- first argument
     * @return string -- packed integer
     */
    static function pand() {
        if (func_num_args() == 1 and is_array(func_get_arg(0))) {
            $arg = func_get_arg(0);
        } else {
            $arg = func_get_args();
        }
        $r = false;
        foreach ($arg as $a) {
            if ($r === false) {
                $r = $a;
                continue;
            }
            $l = min($rl = strlen($r), $al = strlen($a));
            if ($rl > $l) $r = substr($r, $rl - $l);
            if ($al > $l) $a = substr($a, $al - $l);
            #Debug::trace("'$r' AND '$a'");
            for ($i = 0; $i < $l; $i++) {
                $r[$i] = chr(ord($r[$i]) & ord($a[$i]));
            }
            #Debug::trace("is '$r'");
        }
        return ltrim($r, "\x00");
    }

    /**
     * Computes a bitwise OR from (multiple) arguments
     *
     * Single array argument may hold multiple arguments as well.
     * Zero arguments results zero ('')
     *
     * This is equivalent with php expression: $p1 | $p2 | ...
     *
     * param string|array $arg -- first argument
     * @return string -- packed integer
     */
    static function por() {
        if (func_num_args() == 1 and is_array(func_get_arg(0))) {
            $arg = func_get_arg(0);
        } else {
            $arg = func_get_args();
        }
        $r = false;
        foreach ($arg as $a) {
            if ($r === false) {
                $r = $a;
                continue;
            }
            $l = max($rl = strlen($r), $al = strlen($a));
            if ($rl < $l) $r = str_pad($r, $l, "\x00", STR_PAD_LEFT);
            if ($al < $l) $a = str_pad($a, $l, "\x00", STR_PAD_LEFT);
            $r = $r | $a;
            /*
            for($i=0;$i<$l; $i++) {
                $r[$i] = $r[$i] | $a[$i]; #chr(ord($r[$i]) | ord($a[$i]));
            }
            */
        }
        return ltrim($r, "\x00");
    }

    /**
     * Computes a bitwise NOT of argument
     *
     * Returns same byte-length result as input.
     * This is equivalent with php expression: ~$v
     *
     * @param string $v -- first argument
     * @return string -- packed integer
     */
    static function inv($v) {
        return ~$v;
        /*
                $l = strlen($v);
                for($i=0;$i<$l; $i++) {
                    #Debug::tracex('inv: ', bin2hex($v[$i]). '->'. dechex((~ ord($v[$i])) & 255));
                    $v[$i] = ~$v[$i]; #chr((~ ord($v[$i])) & 255);
                }
                return $v;
        */
    }
}
