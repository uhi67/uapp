<?php
/**
 * class DatabaseException
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/** @noinspection PhpClassNamingConventionInspection */

/**
 * DatabaseException represents an exception occured during execution an SQL command
 */
class DatabaseException extends InternalException {
}
