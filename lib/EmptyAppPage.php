<?php
/**
 * class EmptyAppPage
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * An empty controller, does nothing.
 */
class EmptyAppPage extends UAppPage {
    function preconstruct() {
    }

    function loadData() {
    }

    function processActions($act) {
    }

    function afterRender() {
    }

    function initUser() {
    }
}
