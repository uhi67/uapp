<?php
/**
 * class UAppLoader
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

require_once __DIR__ . '/Loader.php';

/**
 * UAppLoader is a simple autoloader
 *
 * which loads UApp internal classes and modules only
 * (for testing purpose)
 *
 * Using:
 * require_once $uapp_path.'/lib/UAppLoader.php';
 * UAppLoader::registerAutoload();
 *
 */
class UAppLoader extends Loader {
    public static function registerAutoload($config = null) {
        if (!$config) $config = isset($GLOBALS['config']) ? $GLOBALS['config'] : [];
        static::$config = $config;
        return spl_autoload_register([__CLASS__, 'includeClass']);
    }

    public static function unregisterAutoload() {
        return spl_autoload_unregister([__CLASS__, 'includeClass']);
    }

    /**
     * Returns search paths of library files
     *
     * @return array
     */
    public static function getBases() {
        $uapp_path = dirname(__DIR__);
        return ["$uapp_path/lib", "$uapp_path/modules", "$uapp_path/commands"];
    }
}
