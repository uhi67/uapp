<?php /** @noinspection PhpUnused */
/**
 * class UAppPage
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2014-2018
 * @license MIT
 */

/**
 * Abstract page controller. You have to extend it in your application.
 * All page and command of an application has to be a descendant of this.
 * ## Writing a Page controller:
 * ### 1. Define the following abstract methods:
 * - __preConstruct()__ -- tasks, declarations before user creation
 * - __initUser()__ -- It must create the user object
 * - __loadData()__ --  tasks, declarations after user creation
 *
 * ### 2. Define all __actAction()__ methods
 * - where Action is the name of action verb in the path after the page name.
 * - optionally define the __actDefault()__ which will be run if no action verb is supplied.
 *
 * ### 3. Override the followings optionally
 * - __afterRender()__  -- called after XSL render is finished, and result has been sent to the output.
 *
 * ### 4. Override these only in application's common controller:
 * - __appConstruct()__ -- Called before `preConstruct()`
 * - __appLoadData()__  -- Called before `loadData()`
 *
 * ## Using a Page Controller:
 * -
 *
 * ## Request parameters processed by UAppPage
 * - showxml (via {@see UXPage})
 * - render (via {@see UXPage})
 * - msg ({@see __construct()})
 * - id ({@see __construct()})
 * - act ({@see __construct()})
 * - resetxsl ({@see __construct()})
 * - module ({@see actModule()})
 *
 * {@inheritdoc}
 *
 * @property string $view
 */
abstract class UAppPage extends UXPage {
    /** @var BaseUser $user -- the logged in user (not a model) */
    public $user;
    /** @var string $appname -- The name of the application from the config */
    public $appname;
    /** @var string name -- name of the page for logging, etc. Default for view. */
    public $name;
    /** @var DBX $db -- default database connection of this page */
    public $db;
    /** @var Rights $rights - rightsystem object based on Rights class. Must be initialized from $app->rights */
    public $rights;
    /** @var UApp $app - the host application instance */
    public $app;
    /** @var bool $resetxsl -- XSL regenerate is needed */
    public $resetxsl = false;

    /** @var string $title */
    protected $title;
    protected $h1;
    protected $h1link;
    protected $h2;
    protected $h2link;
    /** @var string $act -- action word from path or act parameter in get or post */
    protected $act = '';
    /** @var array $path -- path words separated by / in the url. [0] is the function name */
    protected $path;
    /** @var bool $defaultmenu -- if set, addDefaultmenu() will be invoced in the page */
    protected $defaultmenu = true;
    /** @var integer id -- the default id of the user object from the path if contains integer or value of id request parameter */
    protected $id;
    /** @var string $error_url - redirect on error. Default is the base url of the function */
    protected $error_url;
    /** @var string $back_url - redirect on success. Default is the base url of the function */
    protected $back_url;
    /** @var array $logged -- names of already logged actions */
    private $logged;
    /** @var string $view - name of the view. Can be set by setView() only. Does not contain .xsl extension. RAW = no render, response is already sent by user. */
    private $view;
    /** @var string $viewfile -- setView() sets it, render() uses */
    protected $viewfile;
    /** @var string $pageview -- combination of pagename and viewname, e.g. dns_redirect. createXsl() sets it, available in the XML doc at /data/control/pageview */
    protected $pageview;
    /** @var string $designmodule -- name of module contains the design. Can be set in config/design or with {@see setDesign()} */
    protected $designmodule;
    /** @var string $design - name of the design layout. Can be set by {@see setDesign()}('module/layout'). Default is from config, it's default is 'basic'. */
    private $design;
    /** @var boolean $exp -- indicates export action: the lists are not paginated. User page should set it. */
    protected $exp;
    /** @var bool $nolog -- disables logging of current action. Use $this->nolog=true; in the action method. */
    protected $nolog;

    /**
     * Application page base class constructor.
     *
     * Never override it, use preConstruct()
     *
     * @param UApp $app -- the host Application instance
     * @param array $path -- URL path elements
     * @param bool $secondary -- don't close debug if true
     *
     * @return void
     * @throws Exception
     * @throws HandledException
     * @noinspection PhpAssignmentInConditionInspection
     */
    function __construct($app, $path = null, $secondary = false) {
        $baseurl = UApp::config('baseurl');
        parent::__construct([
            'app' => $app,
            'db' => $app->db,
            'enc' => 'UTF-8',
            'secondary' => $secondary,
            'baseurl' => $baseurl
        ]);

        $this->logged = [];

        $this->register('jQuery');
        $this->register('jQueryUI');
        if (!$this->register('Main')) throw new Exception('Main module cannot be registered');
        $this->appConstruct();
        if (!$this->design) $this->setDesign($app->config('design'));
        Debug::tracex('Default designmodule/variant', "$this->designmodule/$this->design");
        $this->preConstruct();

        // Page további elemei
        $classname = get_class($this);
        $pagename = Util::uncamelize(preg_replace('/(\w+)Page/', '\1', $classname));
        #Debug::tracex('pagename', $pagename);
        if (file_exists($filename = "js/$classname.js")) $this->addJs($filename);
        if (file_exists($filename = "css/$classname.css")) $this->addCss($filename);
        if (file_exists($filename = "js/$pagename.js")) $this->addJs($filename);
        if (file_exists($filename = "css/$pagename.css")) $this->addCss($filename);
        if (!$this->xsl && file_exists("../xsl/$classname.xsl")) $this->setXSL($pagename);
        if (!$this->xsl && file_exists("../xsl/$pagename.xsl")) $this->setXSL($pagename);

        $this->node_data->setAttribute('ver', $app->config('ver', isset($GLOBALS['ver']) ? $GLOBALS['ver'] : ''));
        $this->error_url = $this->back_url = $this->script;

        if ($msg = Util::getReq('msg')) {
            $this->addHead('message', $this->app->la('app', $msg));
        }

        if ($this->appname = $app->config('appname')) $this->addHead('appname', $this->appname);
        if (file_exists($configfile = $app->config('configpath') . '/config.php')) $this->addHead('appdate', date('Y.m.d.', filemtime($configfile)));
        $this->addHead('appversion', trim($app->config('ver')));
        $this->addHead('environment', $app->config('environment'));
        if ($this->defaultmenu) $this->addDefaultMenu();
        #Debug::tracex('path', $path);
        if (isset($path[0]) && !is_numeric($path[0])) $this->act = array_shift($path);
        if (isset($path[0]) && is_numeric($path[0])) $this->id = array_shift($path);
        $this->path = $path;
        $this->id = Util::getReqInt('id', $this->id);
        $this->act = Util::getReq('act', $this->act);
        $this->resetxsl = Util::getReqInt('resetxsl', 0);
        $this->addControlVar('script', $this->script);
        $node_path = $this->addControlVar('path');
        if ($this->path) foreach ($this->path as $v) if ($v != '') $node_path->addNode(Util::toNameID($v));

        /** @var array $enabled_ip */
        $enabled_ip = $app->config('enabled_ip');
        $sorry_message = $app->config('sorry_message');
        if (isset($sorry_message) && (!is_array($enabled_ip) || !in_array($app->ip, $enabled_ip))) {
            throw new ForbiddenException($sorry_message);
        }

        if (array_key_exists('logout', $_REQUEST)) $this->log(Util::getSession('user_uid'), 'logout');

        $this->initUser();        // Application level callback

        if ($this->user instanceof SamlUser && $this->user->auth->isAuthenticated()) {
            if(array_key_exists('login', $_REQUEST)) $this->log($this->user->getIdp(), 'login');
        }

        try {
            $this->appLoadData();   // App level
            if ($this->act != 'module') $this->loadData();      // user callback
            if ($this->act) {
                $this->addControlVar('act', $this->act);
                $this->node_content->setAttribute('act', $this->act);
            }
            if ($this->id) $this->addControlVar('id', $this->id);
        } // Executed only in PHP 7, will not match in PHP 5
        catch (Throwable $e) {
            $this->showException($e, is_a($e, 'ForbiddenException') ? 'Jogosultsági hiba' : 'Hiba történt.');
            throw new HandledException('', 0, $e);
        }
            // Executed only in PHP 5, not reached in PHP 7
            /** @noinspection PhpRedundantCatchClauseInspection */
            /** @noinspection PhpWrongCatchClausesOrderInspection */
        catch (Exception $e) {
            $this->showException($e, is_a($e, 'ForbiddenException') ? 'Jogosultsági hiba' : 'Hiba történt.');
            throw new HandledException('', 0, $e);
        }

        // Instance message from config
        $message = UApp::config('message');
        if ($message) $this->addMessages($message, ['class' => 'instruction']);

        $this->getMessage(); // Saved message
        if (!$this->name) $this->setName($pagename);
        $this->addControlVar('render', $this->render);
    }

    /**
     * Callback before creating user and loading request parameters
     * Called before `page->preConstruct()`
     * Overload in your application's AppPage
     */
    function appConstruct() {
    }

    /**
     * Callback after creating user and loading request parameters
     * Called after `page->preConstruct()` but before `page->loadData()`
     * Overload in your application's AppPage
     */
    function appLoadData() {
    }

    /**
     * Shows exceptions raised during displaying a page (__construct and go)
     *
     * @param Exception|UAppException|UserException $e
     * @param string $h -- cím
     *
     * @return void
     * @throws ConfigurationException
     * @throws Exception
     * @throws InternalException
     * @throws UAppException
     */
    function showException($e, $h) {
        $this->setH2($h);
        $msg = $e->getMessage();
        #$subtitle = 'Váratlan belső hiba';
        $showtrace = UApp::config('environment') != 'production';
        $goto = false;

        if (is_a($e, 'UAppException')) {
            #$subtitle = $e->getSubtitle();
            $showtrace = $showtrace || $e->showTrace();
            $goto = is_a($e, 'UserException') ? $e->getGoto() : '';
        }

        #$content = array($msg);
        #if(is_a($e, 'UAppException') && ($extra=$e->getExtra())) $content[] = Util::objtostr($extra);
        $extra = is_a($e, 'UAppException') ? $e->getExtra() : 'no details';
        if (!is_scalar($extra)) {
            if (is_array($extra)) {
                $extra = Util::toHtml($extra);
            } else $extra = Util::objtostr($extra);
        } else $extra = htmlspecialchars($extra);
        $this->addMessages($msg, ['class' => 'error main line-232'], Debug::$trace ? '<div>' . $extra . '</div>' : '');

        if ($goto) {
            #$tovabb = is_array($goto) ? $goto[0] : 'Tovább';
            $goto = is_array($goto) ? $goto[1] : $goto;
        }

        if ($showtrace) {
            $content = [];
            if (is_a($e, 'UAppException') && $e->info) {
                foreach ($e->info as $v) $content[] = $v;
            }
            $error = error_get_last();
            if ($error) Debug::tracex('errortype', $error['type'] . '&' . E_ERROR);
            if ($error && (Debug::$trace != '' || $error['type'] & E_ERROR)) {
                $content[] = '<div>' . sprintf('[%s] %s(%d): %s', Util::friendlyErrorType($error['type']), $this->colorTrace($error['file']), $error['line'], $error['message']) . '</div>';
            }

            $ee = $e;
            while ($ee) {
                $info = $ee->getFile() . '(' . $ee->getLine() . '): ' . $ee->getMessage();
                try {
                    $this->log([$info], 'error');
                } catch (Exception $e) {
                    Debug::trace('Error writing into the log', $e->getMessage());
                    Debug::tracex('Error', $info);
                }
                if (Debug::$trace) {
                    $content[] = 'Trace: ' . Debug::$trace;
                    $content[] = '<div>' . sprintf("<b>%s</b> in file '%s' at line '%d'", htmlspecialchars($ee->getMessage()), $ee->getFile(), $ee->getLine()) . '</div>';
                    if ($ee instanceof UAppException && $extra = $ee->getExtra()) $content[] = '<i>' . Util::objtostr($extra) . '</i>';
                    $taa = explode("\n", htmlspecialchars($ee->getTraceAsString()));
                    foreach ($taa as &$trl) {
                        $trl = '<div>' . $this->colorTrace($trl) . '</div>';
                    }
                    $content[] = implode("\n", $taa);
                }
                $ee = $ee->getPrevious();
            }

            $this->addMessages('Részletek', ['class' => 'main'], $content);
        }

        $this->addControlVar('url', $goto ? $goto : $this->error_url);
        $this->addControlVar('original-view', $this->view);
        $this->setName('uapp');
        $this->setView('message');
        $this->render();
        $this->view = 'raw';
    }

    /**
     * Appends color classes to parts of stack trace lines depending of position of the source file.
     *
     * @param string $trl
     * @return string
     */
    function colorTrace($trl) {
        $trl = preg_replace('#(uapp\\\\lib\\\\)([a-z0-9_-]+\.php)#i', '$1<span class="trace-lib">$2</span>', $trl);
        $trl = preg_replace('#(uapp\\\\modules\\\\[a-z0-9_-]+\\\\)([a-z0-9_-]+\.php)#i', '$1<span class="trace-uappmod">$2</span>', $trl);
        $trl = preg_replace('#(\\\\modules\\\\[a-z0-9_-]+\\\\)([a-z0-9_-]+\.php)#i', '$1<span class="trace-module">$2</span>', $trl);
        $trl = preg_replace('#(\\\\(pages|www)\\\\)([a-z0-9_-]+\.php)#i', '$1<span class="trace-app">$3</span>', $trl);
        $trl = preg_replace('#(\\\\(inc|models)\\\\)([a-z0-9_-]+\.php)#i', '$1<span class="trace-inc">$3</span>', $trl);
        return $trl;
    }

    abstract function initUser();

    /**
     * ## UAppPage end actions (instead of destruct)
     * - Removes user
     * - Closes session
     * - Deletes stored post if this page is the target
     * - Destructs UXPage
     *
     * {@inheritdoc}
     */
    function finish() {
        try {
            if ($this->user) $this->user->finish();
            unset($this->user);
            session_write_close();
            if (isset($_SESSION['_post'])) {
                $_target = $_SESSION['_post']['_target'];
                if ($this->name == $_target) {
                    unset($_SESSION['_post']);
                }
            }
            parent::finish();
        } // Executed only in PHP 7, will not match in PHP 5
        catch (Throwable $e) {
            echo "Exception at UAppPage finish: " . $e->getMessage();
        }
            // Executed only in PHP 5, not reached in PHP 7
            /** @noinspection PhpRedundantCatchClauseInspection */
            /** @noinspection PhpWrongCatchClausesOrderInspection */
        catch (Exception $e) {
            echo "Exception at UAppPage finish: " . $e->getMessage();
        }
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Processes actions of the page, and renders the result after.
     * Usually called by UApp::run() just after creating page instance.
     *
     * @return void
     * @throws ConfigurationException
     * @throws Exception
     * @throws InternalException
     * @throws UAppException
     */
    function go() {
        try {
            $this->processActions($this->act);
        } catch (UAppException $e) {
            $title = is_a($e, 'UAppException') ? $e->getName() : 'Hiba a művelet feldolgozása során';
            $this->showException($e, $title);
        }
        $this->render();
        $this->afterRender();
        if (!$this->nolog) $this->log();
    }

    /**
     * @param $act
     *
     * @return bool|mixed
     * @throws ConfigurationException
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws NotFoundException
     * @throws ReflectionException
     * @throws UAppException
     */
    function processActions($act) {
        $this->addControlVar('script-go', $this->script);
        /** @noinspection PhpAssignmentInConditionInspection */
        if ($act == '' && is_callable([$this, ($func = 'actDefault')])) {
            return call_user_func([$this, $func]);
        } else if ($act) {
            $func = 'act' . Util::camelize($act, true);
            if (!is_callable([$this, $func])) throw new NotFoundException(UApp::la('uapp', 'Unknown action `{$act}`', ['act' => $act]));
            Debug::tracex('act', $act);
            return call_user_func([$this, $func]);
        }
        return false;
    }

    /**
     * User callback: controller initialization before loading user
     */
    abstract function preConstruct();

    /**
     * User callback: controller initialization after loading user, before actions
     */
    abstract function loadData();

    /**
     * User callback: controller finish after rendering and sending output
     *
     */
    function afterRender() {
    }

    /**
     * Sets the name of the page
     * Used at logging, default view, etc.
     * @param string $name
     * @return string
     */
    protected function setName($name) {
        return $this->name = $name;
    }

    /**
     * Sets the XSL name for rendering
     * Do not use in user page directly, use setView instead.
     * Normally UAppPage->createXSL() will call it after creating dynamic xsl
     * Sets also the name of page, if not set
     *
     * @param string $xsl -- name of xsl file (path to file relative from xsl dir, without .xsl extension)
     * @return string
     */
    public function setXSL($xsl) {
        if (!$this->name) $this->name = $xsl;
        return $this->xsl = $xsl;
    }

    /**
     * Sets the view name for the page.
     * The view is the name of the simple XSL file without includes.
     * Includes will be generated before rendering.
     * Sets also the name of page, if not set
     * View file may be in application's xsl directory or in UApp's xsl directory.
     * If not found, UApp's error view will be used.
     *
     * @param string $view (relative to /xsl or UApp/xsl; without extension)
     *
     * @return string -- the view name (may be different from input)
     * @throws ConfigurationException
     * @throws InternalException
     */
    public function setView($view) {
        if (!$this->name) $this->name = $view;
        $xslpath = UApp::config('xslpath') . '/';
        $uappxslpath = UApp::getPath() . '/xsl/';
        $this->viewfile = $xslpath . $view . '.xsl';
        if (!file_exists($this->viewfile)) {
            Debug::tracex('viewfile', $this->viewfile, '#C88');
            $this->viewfile = $uappxslpath . $view . '.xsl';
        }
        if (!file_exists($this->viewfile)) {
            $this->addHead('original-view', $view);
            $view = 'viewnotfound';
            $this->viewfile = $uappxslpath . 'viewnotfound.xsl';
        }
        $this->xsl = null;
        return $this->view = $view;
    }

    public function getView() {
        return $this->view;
    }

    /**
     * Sets the name of design (module/ or module/layout or layout)
     *
     * @param string $name
     * @return string -- layout name
     */
    public function setDesign($name) {
        $mn = explode('/', $name);
        if (count($mn) == 2) {
            if ($mn[0] != '') $this->designmodule = $mn[0];
            if ($mn[1] != '') $this->design = $mn[1];
        } else {
            if ($this->designmodule == '') $this->designmodule = $name;
            else $this->design = $name;
        }
        return $this->design;
    }

    public function getDesign() {
        return $this->design;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setH1($h1, $h1link = '') {
        $this->h1 = $h1;
        $this->h1link = $h1link;
    }

    function setH2($h2, $h2link = '') {
        $this->h2 = $h2;
        $this->h2link = $h2link;
    }


    /**
     * Registers a Module on page with it's js, css and xsl
     * (modules with or without constructor)
     *
     * Module's method init(page, dir) function may register additional assets
     *
     * With current implementation, registering modules with xsl includes must not be conditional on a page.
     *
     * @param string $module
     * @param bool $init -- create module object, else call init if exists
     *   classname of Module
     *
     * @return bool
     * @throws Exception
     * @throws UAppException
     */
    function register($module, $init = false) {
        if (array_key_exists($module, $this->modules)) return false;
        $c = Loader::filename($module);
        $dirname = $this->getModulePath($c) . '/';
        if ($dirname) {
            $instance = null;
            if ($init && file_exists($dirname . $c . '.php')) {
                $instance = new $module($this);
            }
            if (file_exists($dirname . $c . '.php')) {
                if (is_callable([$c, 'init'])) {
                    $result = call_user_func([$c, 'init'], $this, $dirname);
                    if (!$instance) $instance = $result;
                }
            }
            $this->modules[$module] = $instance;

            // Registering assets
            $dir = dir($dirname);
            if (!$dir) {
                return false;
            }

            // Collect files
            $files = [];
            while (false !== ($entry = $dir->read())) {
                $eee = strrpos($entry, '.');
                $ext = $eee === false ? '' : substr($entry, $eee + 1);
                if (in_array($ext, ['js', 'css', 'xsl'])) $files[] = $entry;
            }
            $dir->close();

            // Drop parent files if 'min' version exists
            foreach ($files as &$entry) {
                $eee = strrpos($entry, '.');
                $ext = $eee === false ? '' : substr($entry, $eee + 1);
                $p = strstr($entry, '.min.');
                if ($p == '.min.' . $ext) {
                    $name = strstr($entry, '.min', true);
                    $p = array_search($name . '.' . $ext, $files);
                    if ($p !== false) unset($files[$p]);
                }
            }
            unset($entry);

            // Apply files
            /** @noinspection PhpAssignmentInConditionInspection */
            foreach ($files as $entry) {
                $eee = strrpos($entry, '.');
                $ext = $eee === false ? '' : substr($entry, $eee + 1);
                if ($ext == 'js') $this->addJs($c . '/' . $entry);
                if ($ext == 'css') {
                    $this->addCss($c . '/' . $entry);
                }
                if ($ext == 'xsl') {
                    $this->addXsl($module . ':' . $entry);
                }
            }
        } else {
            throw new Exception("Registration error: Module '$c' not found");
        }
        return true;
    }

    /**
     * Returns the base-path of the module. Searches the application and the UApp library
     * TODO: only for registered modules, cache
     *
     * @param string $name -- module name
     * @param bool $relative
     *
     * @return string/false if not found
     * @throws ConfigurationException
     */
    function getModulePath($name, $relative = false) {
        $basepath = UApp::config('basepath');
        $modulepath = UApp::config('modulepath', $basepath . '/modules');
        $uapp_path = UApp::getPath();
        $bases = [$modulepath, "$uapp_path/modules"];
        for ($i = 0; $i < count($bases); $i++) {
            $base = $bases[$i];
            $dirname = "$base/$name/";
            if (file_exists($dirname)) {
                $dirname = realpath($dirname);
                if ($relative)
                    return Util::getRelativePath(realpath($basepath), $dirname);
                else
                    return $dirname;
            }
        }
        return false;
    }


    /**
     * ## Adds an XSL file to the page.
     *
     * The xsl may be located in:
     *
     * - xsl (simply use it's name with extension')
     * - module (use modulename:path)
     *
     * @param string $filename -- may be 'module:filename'
     *
     * @return DOMElement node
     * @throws ConfigurationException
     * @throws InternalException
     * @throws UAppException
     */
    function addXsl($filename) {
        $xslpath = UApp::config('xslpath') . '/';

        if (strpos($filename, ':')) {
            [$module, $filename] = explode(':', $filename);
            $path = $this->getModulePath(Loader::filename($module));
            if ($path === false) throw new UAppException(sprintf('Module `%s` not found', $module));
            $path = $path . '/';
        } else {
            $module = null;
            $path = $xslpath;
            if (!file_exists($path . $filename)) $path = UApp::getPath() . '/xsl/';
        }
        if (!file_exists($path . $filename)) throw new UAppException(sprintf('XSL file `%s` not found', $path . $filename));

        $node = parent::addXsl(($module ? ($module . '/') : '') . $filename);
        // determining place of XSL file (views, modules, UApp/modules)
        $relpath = Util::getRelativePath($xslpath, $path . $filename);
        if ($module) $node->setAttribute('module', $module);
        $node->setAttribute('path', $relpath);
        $node->setAttribute('order', 0);
        return $node;
    }

    /**
     * Adds an user XSL file to the page.
     * The xsl may be located in:
     * - xsl (simply use it's name with extension')
     * - module (use modulename:path)
     * The difference to addXsl() is that this will include xsl in specified order, default AFTER design
     *
     * @param string $filename
     * @param int $order -- order of includes, default=11
     *
     * @return DOMElement
     * @throws ConfigurationException
     * @throws InternalException
     * @throws UAppException
     */
    function addUserXsl($filename, $order = 11) {
        $node = $this->addXsl($filename);
        $node->setAttribute('order', $order);
        return $node;
    }

    /**
     * Builds the actual base XSL for the pageview to be render with
     * The name of pageview set is will be available in DOM at control/pageview.
     *
     * The built `pageview.xsl` files will contain proper references for all of xsl files of the page (view and modules) in two version:
     *
     * 1. for server render `__pageview.xsl` contains local paths
     * 2. for client render `_pageview.xsl` contains remote paths
     * 3. for syntax validation, `___pageview.xsl` contains local paths relative to data/xsl
     *
     * The xsl will be rebuilt only if not exists or the php file of the page is newer, or the file is older than 2 hours.
     *
     * inputs: $this->name, $this->view, $this->design, $this->designmodule
     * outputs: $this->pageview, $this->xsl; _pageview.xsl, __pageview.xsl files;
     *
     * @return void
     * @throws ConfigurationException
     * @throws Exception
     * @throws InternalException
     * @throws UAppException
     */
    function createXSL() {
        $genxslpath = UApp::config('genxslpath', UApp::$dataPath . '/xsl'); // Ahová a generált fájlok kerülnek
        $xslpath = UApp::config('xslpath', UApp::config('apppath') . '/xsl'); // Ahol a nézetek vannak

        if (!$this->view) $this->setView($this->xsl ? $this->xsl : $this->name);
        $this->addHead('name', $this->name);
        $node_view = $this->addHead('view', $this->view);
        Debug::tracex('viewfile', "$this->view, $this->viewfile");
        $node_view->setAttribute('path', $viewpath = Util::getRelativePath($xslpath, $this->viewfile));
        Debug::tracex('view path', "$xslpath, $viewpath");

        // Determines the design xsl and adds it to the xsl set.
        if ($this->designmodule) {
            if (!$this->design) $this->design = 'basic';
            $nodedesign = $this->addHead('design', $this->design);
            $nodedesign->setAttribute('module', $this->designmodule);
            $this->register($this->designmodule);
            $node = $this->addXsl($this->designmodule . ':design/' . $this->design . '.xsl');
            $node->setAttribute('order', 10);
        }

        if (!file_exists($genxslpath)) mkdir($genxslpath);

        $this->pageview = $this->name . '_' . Util::toNameID($this->view);

        // Builds the base xsl files of the pageview for both render modes
        // These file contain proper references for all other xsl files of the page view and used modules
        $this->genXSL($genxslpath . '/_' . $this->pageview . '.xsl', 'server');
        $this->genXSL($genxslpath . '/__' . $this->pageview . '.xsl', 'client');
        $this->genXSL($genxslpath . '/___' . $this->pageview . '.xsl', 'validate');

        Debug::tracex('render, xsl, pageview', "$this->render, $this->xsl, $this->pageview");

        // Determine the current name of the base xsl file of the pageview depending on render mode
        $cxslt = Loader::classExists('XSLTProcessor');
        if (!$cxslt) $this->render = 'client';
        if ($this->render == 'client') {
            $this->setXSL('__' . $this->pageview);
        }
        if ($this->render == 'server' || !$this->xsl) {
            $this->setXSL(Util::getRelativePath($xslpath, $genxslpath . '/_' . $this->pageview));
        }
        $this->addControlVar('xsl', $this->xsl);
        $this->addControlVar('pageview', $this->pageview);
    }

    /**
     * Refreshes an xsl file based on template name using $mode
     * Creates new file only if the php file of this class is newer then the existing result file
     * The template is an xsl transformation applied to xml of this page
     *
     * @param string $xslfile -- name of result xsl file
     * @param string $mode -- template selector
     *
     * @return void
     * @throws ConfigurationException
     * @throws InternalException
     */
    function genXSL($xslfile, $mode = 'server') {
        $rc = new ReflectionClass($this);
        $phpfile = $rc->getFileName();
        $mtime = filemtime($phpfile);
        if (!file_exists($xslfile) || filemtime($xslfile) < $mtime || $this->resetxsl) {
            Debug::tracex('xslfile', $xslfile);
            // _pagename.xsl generálása _page_template.xsl alapján, head/xsl elemekből
            $genpage = UApp::getPath() . "/lib/xsl/gen_page_$mode.xsl";
            $xsl = $this->xmldoc->process($genpage);
            $xsl->save($xslfile);
            @chmod($xslfile, 0764);
        }
    }

    /**
     * ## UAppPage::render()
     *
     * Generates base XSL,
     * completes the head data with server and title data,
     * then calls legacy render with the base xsl.
     *
     * {@inheritdoc}
     *
     * @return void
     * @throws ConfigurationException
     * @throws Exception
     * @throws InternalException
     * @throws UAppException
     */
    function render() {
        if ($this->view == 'raw') return;
        $this->createXSL();
        $this->addHead('title', $this->title);
        if (isset($GLOBALS['status_message'])) $this->addHead('status', $GLOBALS['status_message']);
        if (isset($GLOBALS['server_name'])) {
            $sa = explode('#', $GLOBALS['server_name']);
            $node_server = $this->addHead('server', $sa[0]);
            if ($sa[1]) $node_server->setAttribute('title', $sa[1]);
            if ($sa[2]) $node_server->setAttribute('link', $sa[2]);
            if ($sa[3]) $node_server->setAttribute('color', $sa[3]);
        }
        if ($this->h1) $this->addHead('h1', $this->h1);
        if ($this->h1link) $this->addHead('h1link', $this->h1link);
        if ($this->h2) $this->addHead('h2', $this->h2);
        if ($this->h2link) $this->addHead('h2link', $this->h2link);
        if (!$this->xsl) {
            if ($this->view) $this->setXSL('_' . $this->view);
            else if ($this->name) $this->setXSL($this->name);
        }
        Debug::tracex('xsl, view, name', "$this->xsl, $this->view, $this->name");
        parent::render();
    }

    /**
     * Checks if the user is logged in.
     * If not, and redirect is enabled, calls login action.
     * If redirect is not enabled, a ForbiddenException occurs.
     *
     * @param bool $enablelogin -- enables redirect to login
     * @param string $goto -- return link for exception message
     *
     * @return boolean -- is logged in
     * @throws ConfigurationException
     * @throws ForbiddenException
     * @throws InternalException
     * @throws UAppException
     * @throws DOMException
     * @throws ReflectionException
     */
    function mustLogin($enablelogin = true, $goto = null) {
        #Debug::tracex('loggedin', $this->user->loggedIn());
        if ($this->user->isEnabled()) return true;
        $url = $_SERVER['REQUEST_URI'];
        if (strpos($url, 'login.php')) $url = '';
        $_SESSION['loginurl'] = $url;
        $uid = $this->user->uid;
        if ($this->user->loggedIn() || !$enablelogin) throw new ForbiddenException("A rendszer használatához nincs joga ($uid)", null, $goto);
        $this->redirect(['start', 'login' => 1, 'url' => $url, 'msg' => 'You must login for this function']);
        return false;
    }

    /**
     * Adds one or more msg node to control.
     * msg node represents a closeable system message with a title, an optional content.
     * Rendered in Main/main.xsl or application's design
     *
     * If message ends with '~', it will be converted to '`' prefix (indicates markdown-formatted text)
     *
     * @param string|array $msg -- message or message array
     * @param string|array|null $attr -- message attributes pl. array('class'=>'error')
     * @param string|array $contents -- contents of a content block attached to a single message (paragraph of multiple paragraphs)
     *
     * @return UAppPage
     * @throws ConfigurationException
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     */
    function addMessages($msg, $attr = null, $contents = null) {
        if ($attr !== null && !is_array($attr)) $attr = ['class' => $attr];
        if (is_array($msg)) {
            $ma = [];
            for ($i = 0; $i < count($msg); $i++) {
                if ($msg[$i] != '') $ma[] = ['msg', $attr, [['title', null, $msg[$i]]]];
            }
            $this->addControl($ma);
        } else {
            if (substr($msg, -1) == '~') $msg = '`' . substr($msg, 0, -1);
            $node_msg = $this->addControl([['msg', $attr]]);
            $node_msg->addTextNode('title', $msg);
            if (is_string($contents) && $contents !== '') $contents = [$contents];
            if (is_array($contents)) {
                foreach ($contents as $content) {
                    if (!is_scalar($content)) continue;
                    if (!is_string($content)) $content = (string)$content;
                    if ($content != '' && substr($content, -1) == '~') $content = '`' . substr($content, 0, -1);
                    $node_msg->addHypertextNode('content', $content);
                }
            }
        }
        return $this;
    }

    /**
     * Override in AppPage if you want to add default menu contents in your app.
     */
    protected function addDefaultMenu() {
    }

    /**
     * Writes a log record to the file
     * If the given action is already logged, logging will be repeated only with parameters given
     *
     * @param string $params
     * @param string $action -- Action to be logged, default is $this->act
     *
     * @return void
     * @throws ConfigurationException
     */
    function log($params = '', $action = null) {
        if ($this->nolog) return;
        Debug::tracex('log ' . $action, $params);
        $page = $this->name ? $this->name : get_class($this);
        $action = $action ? $action : ($this->act ? $this->act : '');
        if (isset($this->logged[$action]) && !$params) return;
        $paramx = is_array($params) ? implode("\t", $params) : $params;
        $this->logged[$action] = true;
        $this->app->log("$page\t$action\t$paramx");
    }

    /**
     * ## Redirect to the given url.
     * In debug mode, redirect is paused with a link page.
     *
     * **Always returns** after redirection written to the output.
     *
     * - If you use array location, Util::createUrl($this->baseurl) will be applied.
     * - If no location is given, $this->error_url will be applied.
     *
     * @param string|array $location or location components relative to app's baseurl
     *
     * @return void
     * @throws ConfigurationException
     * @throws InternalException
     * @throws UAppException
     * @throws DOMException
     * @throws ReflectionException
     * @see createUrl()
     *
     */
    function redirect($location = null) {
        if (is_array($location)) $location = Util::createUrl($this->baseurl, $location);
        if ($location === null) $location = $this->error_url;
        if (!Debug::$trace) {
            header("Location: $location");
            $this->setView('redirect');
            $this->addHead('redirect', htmlentities($location));
        } else {
            $this->setView('redirect');
            $node_redirect = $this->addHead('redirect', htmlentities($location));
            $node_redirect->setAttribute('pause', 1);

            $msgs = Util::getSession('sendmessage');
            if (is_array($msgs)) foreach ($msgs as $msga) {
                $this->addControlVar('sendmsg', $msga[0], ['class' => $msga[1]]);
            }
        }
        $this->render();
        exit;
    }

    /**
     * Displays a message page with one or more lines of message
     * The render is completed on this way.
     *
     * @param string $title -- Title of the window
     * @param string $h1 -- Main title
     * @param string $h2 -- Subtitle
     * @param mixed $msg -- message or array of messages
     * @param string $url -- url for the [Continue] link
     * @param array $attr -- attributes for the messages, e.g. class
     *
     * @return void
     * @throws ConfigurationException
     * @throws Exception
     * @throws InternalException
     * @throws UAppException
     */
    function showMessage($title, $h1, $h2, $msg, $url = null, $attr = null) {
        Debug::tracex('title', $title);
        $this->addMessages($msg, $attr);
        $this->setXSL('message');
        if ($h1 != null) $this->setH1($h1);
        if ($h2 != null) $this->setH2($h2);
        if ($url != null) $this->addControl([['url', null, $url]]);
        $this->render();
    }

    /**
     * Displays an error message and finishes render.
     * Displays a next button with given url.
     *
     * @param mixed $title
     * @param string $message
     * @param mixed $url
     *
     * @return void
     * @throws ConfigurationException
     * @throws Exception
     * @throws InternalException
     * @throws UAppException
     */
    function errorMessage($title, $message = '', $url = null) {
        $bt = debug_backtrace();
        $b = $bt[1];
        $c = $bt[0];
        $hely = $b['class'] . $b['type'] . $b['function'] . ':' . $c['file'] . '#' . $c['line'];
        $e = UApp::la('uapp', 'Error');
        if ($url === null) $url = $this->error_url;
        $this->showMessage($e, $e, $title, [$message, $hely], $url, ['class' => 'main error']);
        exit;
    }

    /**
     * Displays an internal error directly
     *
     * @param string $message
     * @param null $url
     *
     * @throws ConfigurationException
     * @throws Exception
     * @throws InternalException
     * @throws UAppException
     * @deprecated -- use InternalException
     */
    function internalError($message = '', $url = null) {
        $title = UApp::la('uapp', 'Internal error');
        if ($url == null) $url = $this->error_url;
        $this->errorMessage($title, $message, $url);
        exit;
    }

    /**
     * @param string $msg
     * @param string $url
     *
     * @throws ForbiddenException
     * @deprecated use ForbiddenException
     */
    function insufficientRights($msg = '', $url = '') {
        throw new ForbiddenException($msg, null, $url);
    }

    /**
     * Stores a "flash" message to display on the next page
     * Can store more messages one after another
     *
     * Markdown syntax is accepted if msg starts with backtick, e.g.
     *
     * `´[caption](url)`
     *
     * Markdown is processed at client side by showdown module.
     *
     * @param string|array $msg -- Message text or array($msg, array($params)), with %s style params
     * @param string $class -- message class (error/ok)
     * @param bool $unique -- display same message is only once in one transaction (uses base message without params)
     * @param string|array $content -- optional extended content for message.
     *
     * @return void
     * @throws InternalException
     * @deprecated see {@see Util::sendMessage()}
     */
    function sendMessage($msg, $class = '', $unique = false, $content = null) {
        Util::sendMessage($msg, $class, $unique, $content);
    }

    /**
     * Retrieves all flash messages from session to the DOM
     *
     * @return string -- the last flash message or '' if none.
     * @throws ConfigurationException
     * @throws DOMException
     * @throws InternalException
     * @throws ModelException
     * @throws ReflectionException
     * @throws UAppException
     */
    function getMessage() {
        $msg = '';
        $msgs = Util::getSession('sendmessage');
        while (is_array($msgs) && count($msgs) > 0) {
            $msga = array_shift($msgs);
            $msg = $msga[0];
            $class = $msga[1];
            $clx = ['class' => "flash $class"];
            $this->addMessages(html_entity_decode($msg), $clx, ArrayUtils::getValue($msga, 2));
        }
        $_SESSION['sendmessage'] = $msgs;
        return $msg;
    }

    /**
     * Generates nodes named `$name` into content node of the page DOM
     *
     * @param string $name
     * @param string|Query $sql -- sql text or Query object
     * @param array $params
     *
     * @return UXMLElement -- the first node inserted or null
     * @throws ConfigurationException
     * @throws DatabaseException
     * @throws InternalException
     * @throws QueryException
     * @throws DOMException
     */
    function query($name, $sql, $params = []) {
        if ($sql instanceof Query) {
            $query = $sql;
            $node = $this->node_content;
            $query->bind($params);
            $result = $query->all();
            $firstchild = null;
            foreach ($result as $row) {
                $child = $node->ownerDocument->createElement($name);
                if (!$firstchild) $firstchild = $child;
                $child->addAttributes($row);
                $node->appendChild($child);
            }
            return $firstchild;
        }

        // Legacy
        return $this->node_content->query($this->db, $name, $sql, $params);
    }

    /**
     * @param $name
     * @param $sql
     * @param $params
     * @param null $subqueries
     *
     * @return DOMElement
     * @throws DatabaseException
     * @throws InternalException
     * @throws DOMException
     * @deprecated
     * @codeCoverageIgnore
     */
    function queryTree($name, $sql, $params, $subqueries = null) {
        /** @noinspection PhpDeprecationInspection */
        return $this->node_content->queryTree($this->db, $name, $sql, $params, $subqueries);
    }

    /**
     * Creates a displayable and pageable list object
     *
     * @param string $sql
     * @param $listname
     * @param $itemname
     * @param array $orders
     *   list of order clauses
     * @param string $sql1
     *   sql for count items
     *
     * @return DOMNode
     * @throws ConfigurationException
     * @throws DOMException
     * @throws DatabaseException
     * @throws InternalException
     * @throws QueryException
     * @throws UAppException
     * @throws ReflectionException
     */
    function loadList($sql, $listname, $itemname, $orders, $sql1 = '') {
        $params = [];
        $sql1 = $sql1 ? $sql1 : /** @lang  text */
            "select count(id) from ($sql) x";
        $count = $this->db->selectvalue($sql1, $params);

        $list = new UList($listname, $itemname, $count, $orders, 0);
        $list->createNode($this->node_content);
        $order = $list->getOrderName();
        $offset = $list->offset();
        $limit = $list->limit();
        $sql .= " order by $order limit $limit offset $offset";
        Debug::tracex('sql', $sql);
        return $this->query($itemname, $sql, $params);
    }

    /**
     * Creates a displayable and pageable list object with sql parameters
     * Considers exp property for export all
     *
     * @param string $sql
     * @param array $params
     * @param string $listname
     * @param string $itemname
     * @param array $orders -- array of order definitions (column orders). Mandatory.
     * Each order definition is an order by string, containining one or more fieldnames, separated with commas.
     * Example: `array('id', 'name, id desc')`
     * @param string $sql1 -- sql for count results (default is calculated from main)
     * @param bool $withSearch -- List header will contain a filter control.
     * @param DOMElement $node_parent -- parent node to insert list into. Default is page's content node.
     * @param string $search -- amit a search mezőbe kell írni (a standard mechanizmus felülírása)
     *
     * @return DOMNode -- the list node created
     * @throws ConfigurationException
     * @throws DOMException
     * @throws DatabaseException
     * @throws InternalException
     * @throws UAppException
     * @throws ReflectionException
     */
    function loadListP($sql, $params, $listname, $itemname, $orders, $sql1 = '', $withSearch = false, $node_parent = null, $search = '') {
        $node_parent = $node_parent ? $node_parent : $this->node_content;
        $sql1 = $sql1 ? $sql1 : /** @lang  text */
            "select count(id) from ($sql) x";
        $count = $this->db->selectvalue($sql1, $params);
        Debug::tracex('count', [$count, $this->db->bindParams($sql1, $params)]);

        $list = new UList($listname, $itemname, $count, $orders, 0);
        if ($withSearch) $list->setSearch($search);

        $node_list = $list->createNode($node_parent);
        $sql = $list->sqlTail($sql, 0, $this->exp);
        Debug::tracex('sql', [$sql, $params]);
        $node_parent->query($this->db, $itemname, $sql, $params);
        return $node_list;
    }

    /**
     * Returns the indexed one from the path words separated by / in the url of the page. [0] is the function name
     *
     * @param integer $i
     * @return string
     */
    public function getPath($i) {
        return isset($this->path[$i]) ? $this->path[$i] : null;
    }

    /**
     * ## Creates a new url based on the url of this page
     *
     * Creates another action of the same controller.
     *
     * - numeric indexed element will be new path elements
     * - associative elements will be new/overridden parameters
     * - `#`-index will be the fragment
     *
     * @param array $params -- url tags and parameters
     *
     * @return string -- the new url
     * @throws InternalException
     * @see Util::createUrl()
     *
     */
    public function createUrl($params = []) {
        return Util::createUrl($this->script, $params);
    }

    /**
     * ## Saves post data to the session and binds to this page by name
     * @return bool
     */
    function savePost() {
        $post = [];
        foreach ($_POST as $i => $value) $post[$i] = $value;
        $post['_target'] = $this->name;
        $_SESSION['_post'] = $post;
        return true;
    }

    /**
     * @throws InternalException
     * @throws NotFoundException
     */
    function actModule() {
        $modulename = Util::getReq('module');
        /** @var Module $module */
        $module = ArrayUtils::getValue($this->modules, $modulename);
        if ($module) $module->action();
        else throw new NotFoundException("Invalid module action: `$modulename`");
    }

    /**
     * Creates a JSON response from given data
     *
     * @noinspection PhpMethodNamingConventionInspection
     * @param mixed $data
     */
    function json_response($data) {
        $data = json_encode($data);
        header('Content-Type: application/json');
        print $data;
        $this->view = 'raw';
    }
}
