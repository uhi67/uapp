<?php
/**
 * class HandledException
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * Exception already rendered for user
 *
 * Previous contains the original exception
 */
class HandledException extends Exception {
}
