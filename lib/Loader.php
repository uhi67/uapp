<?php
/**
 * class Loader
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * Loader is the main autoloader of an UApp application
 *
 * Using:
 *
 * require_once $uapp_path.'/lib/Loader.php';
 * Loader::registerAutoload();
 *
 */
class Loader {
    protected static $config;

    public static function registerAutoload($config = null) {
        if (!$config) $config = $GLOBALS['config'];
        static::$config = $config;
        return spl_autoload_register([__CLASS__, 'includeClass']);
    }

    public static function unregisterAutoload() {
        return spl_autoload_unregister([__CLASS__, 'includeClass']);
    }

    /**
     * Returns search paths for application and UApp library files based on application's configuration
     *
     * @return array
     */
    public static function getBases() {
        $config = static::$config;
        $apppath = $config['apppath'];
        $incpath = isset($config['incpath']) ? $config['incpath'] : $apppath . '/inc';
        $modulepath = isset($config['modulepath']) ? $config['modulepath'] : $apppath . '/modules';
        $modelpath = isset($config['modelpath']) ? $config['modelpath'] : $apppath . '/models';
        $uapp_path = dirname(__DIR__);
        $sapi = class_exists('UApp') ? UApp::$sapi : php_sapi_name();
        $pages = $sapi == 'cli' ? (isset($config['commands']) ? $config['commands'] : $apppath . '/commands') : $config['pages'];
        return [$config['basepath'], $pages, "$incpath", "$uapp_path/lib", $modulepath, "$uapp_path/modules", $modelpath, "$uapp_path/commands"];
    }

    /**
     * Returns directory path for a classname
     *
     * @param string $class
     *
     * @return string|null -- null if not found
     */
    public static function getPath($class) {
        $c = self::filename($class);
        $bases = static::getBases();
        foreach ($bases as $path) {
            if (file_exists("$path/$c.php")) return $path;
            if (file_exists("$path/$c/$class.php")) return "$path/$c";
        }
        return null;
    }

    /**
     * Includes a class file
     *
     * @param string $class
     */
    public static function includeClass($class) {
        $c = self::filename($class);
        if (substr($c, 0, 7) == 'Symfony') return;

        if (class_exists('\Codeception\Util\Debug'))
            \Codeception\Util\Debug::debug("Loading class `$class`, filename `$c`");
        $bases = static::getBases();
        foreach ($bases as $path) {
            if (file_exists($f = "$path/$c.php")) {
                require($f);
                return;
            }
            if (file_exists($f = "$path/$c/$class.php")) {
                require($f);
                return;
            }
        }
        $basesx = implode(', ', $bases);
        if (class_exists('\Codeception\Util\Debug'))
            \Codeception\Util\Debug::debug("Loader: filename `$c` not found in `$basesx`");

        #echo "_loader: class file '$c' not found - [$basesx] \n<br/>";
        //throw new Exception('Class file not found: '.$c); // Nem kell exception, mert még lehet másik loader is regisztrálva
    }

    /**
     * Determines class-filename of the classname
     * (Translates _ characters)
     *
     * @param string $class
     * @return string
     */
    public static function filename($class) {
        $p = strpos($class, '_');
        if ($p === false) return $class;
        return substr($class, 0, $p); #strtr($class, '_\\', '//');
    }

    public static function classExists($name) {
        try {
            $r = class_exists($name);
        } catch (Exception $e) {
            $r = false;
        }
        return $r;
    }
}
