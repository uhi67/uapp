<?php /** @noinspection PhpUnused */
/**
 * Class Query
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2018-2019
 * @license MIT
 */

/**
 * SQL Query builder
 *
 * Usage:
 *
 * - insert (update, delete)
 *
 * ```php
 *  $query = Query::createInsert($table, $fields, $values, $connection);
 *  $result = $query->execute(); // returns number of inserted rows
 * ```
 *
 * - Non-select other query
 *
 * ```php
 *   $query = new Query(array('type'=>'sql', 'sql'=>$sql, 'params'=>$params, 'connection'=>$connection);
 *   $result = $query->execute();
 * ```
 *
 * - Select
 *
 * ```php
 *   $query = new Query(array('type'=>'select', 'from'=>$from, 'fields'=>$fields, 'where'=>$where, 'connection'=>$db, 'params'=>$params);
 *   //or
 *   $query = new Query(array('sql'=>$sql, 'connection'=>$db);
 *   $query->bind($params);
 *   //or
 *   $query = Query::createSelect($fields, $table, $joins, $conditions, $orders, $limit, $offset, $connection);
 *   //or
 *   $query = Query::createSelect($options); // including params
 *   $result = $query->all(); // first,
 * ```
 *
 * ### Properties to be set on creation
 *
 * - **connection** (DBX) -- database connection
 * - **type** (string) -- 'insert', 'select', 'update', 'delete', 'sql'
 * - **modelname** (string) -- name of primary model (first element of from-list in select)
 * - **fields** (array) -- select part of SELECT, or field list of INSERT
 * - **from** (array, string) -- model list (optionally indexed with aliases) of FROM part of SELECT or UPDATE or a single model name
 * - **joins** (array) -- model list of joined table by JOIN clause in SELECT (foreign keys) OR alias=>fk OR alias=>array(model, jointype, conditions) element
 * - **condition** (array) -- expression list of WHERE conditions in SELECT, DELETE or UPDATE
 * - **where** (array) -- alias of condition
 * - **groupby** (array) -- GROUP BY part of select
 * - **orders** (array) -- expression list of ORDERS clause in SELECT. Does not use automatic alias of main table!
 * - **limit** (int) -- LIMIT part of SELECT
 * - **offset** (int) -- OFFSET part of SELECT
 * - **values** (array) -- value list of VALUES part of insert or fieldname=>value pairs of UPDATE
 * - **params** (array) -- the actual binded parameters
 * - **sql** (string) -- the last generated raw sql before binding parameters    {@see Query::getSql()}
 *
 * @property string $modelname - name of primary model (first element of from-list in select)
 * @property array $fields - select part of SELECT, or field list of INSERT
 * @property array $from  - model list (optionally indexed with aliases) of FROM part of SELECT or UPDATE or a single model name
 * @property array $joins - model list of joined table by JOIN clause in SELECT. [alias=>fk OR alias=>[model, jointype, conditions], ...]
 * @property array $condition - expression list of WHERE conditions in SELECT, DELETE or UPDATE
 * @property array $where - alias of condition
 * @property array $groupby - GROUP BY part of select
 * @property array|string|null $orders - expression list of ORDERS clause in SELECT. Does not use automatic alias of main table!
 * @property int $limit - LIMIT part of SELECT
 * @property int $offset - OFFSET part of SELECT
 * @property array $values - value list of VALUES part of insert or fieldname=>value pairs of UPDATE
 * @property array $params - the actual binded parameters
 * @property array $combination - union (or intersect or except) combination data
 * @property string $sql - the last generated raw sql before binding parameters    or user specified if type='sql' {@see Query::getSql()}
 * @property-read string $last - Last inserted id after executing insert query
 * @property-read resource $rs - result set of the query - set by execute functions
 * @property-read string $finalSQL - the generated SQL with parameter values
 */
class Query extends Component {
    public static $integerRangePattern = /** @lang RegExp */
        '~^\s*(\d+|null|<null>|(\.\.\.?|--?|<=?|>=?|<>|!=)\s*\d+|\d+\s*(\.\.\.?|--?|<|<=)\s*\d*)(\s*[,;]\s*(\d+|null|<null>|(\.\.\.?|--?|<=?|>=?|<>|!=)\s*\d+|\d+\s*(\.\.\.?|--?|<|<=)\s*\d*))*\s*$~';
    const partialdateTime = /** @lang RegExpPart */
        '\d[\d./-]+(?:[T ]\d[\d:,.]+)?(?:Z|[+-]\d[\d:,.]+)?';
    const dateTime = /** @lang RegExpPart */
        '\d{4}[./-]\d{1,2}[./-]\d{1,2}\.?(?:[T ]\d{1,2}(?::\d{1,2}(?::\d{1,2})?(?:[.]\d{1,5})?)?)?(?:Z|[+-]\d{1,2}(?:[:.]?\d{1,2})?)?';
    public static $datetimeRangePattern = /** @lang RegExp */
        '~^\s*(?:(' . self::partialdateTime . ')|null|<null>|(?:\.\.\.?|--?|<=?|>=?|<>|!=)\s*(' . self::dateTime . ')|(' . self::dateTime . ')\s*(?:\.\.\.?|--?|<|<=)\s*(' . self::dateTime . ')?)(?:\s*[,;]\s*(?:(' . self::partialdateTime . ')|null|<null>|(?:\.\.\.?|--?|<=?|>=?|<>|!=)\s*(' . self::dateTime . ')|(' . self::dateTime . ')\s*(?:\.\.\.?|--?|<|<=)\s*(' . self::dateTime . ')))*\s*$~';
    public static $partialDateTimePattern = /** @lang RegExp */
        '~^' . self::partialdateTime . '$~';
    public static $dateTimePattern = /** @lang RegExp */
        '~^' . self::dateTime . '$~';

    /** @var DBX $connection - the actual connection of the Query */
    public $connection;

    /** @var string $type - type of query assigned by createXXX methods, eg. 'insert', 'select', 'update', 'delete', 'sql' */
    public $type;
    /** @var bool -- DISTINCT type for SELECT (ignored for others) */
    public $distinct = false;

    /** @var string $_modelname - name of primary model (first element of from-list in select) */
    private $_modelname;
    /** @var array $_fields - select part of SELECT, or field list of INSERT */
    private $_fields;
    /** @var string|array $_from - model list (optionally indexed with aliases) of FROM part of SELECT or UPDATE or a single model name */
    private $_from;
    /** @var array $_joins - model list of joined table by JOIN clause in SELECT */
    private $_joins;
    /** @var array $_condition - expression list of WHERE conditions in SELECT, DELETE or UPDATE */
    private $_condition;
    /** @var array $_groupby - expression list of ORDERS clause in SELECT. Does not use automatic alias of main table! */
    private $_groupby;
    /** @var array $_orders - expression list of ORDERS clause in SELECT. Does not use automatic alias of main table! */
    private $_orders;
    /** @var int $_limit - LIMIT part of SELECT */
    private $_limit;
    /** @var int $_offset - OFFSET part of SELECT */
    private $_offset;
    /** @var array $_values - value list of VALUES part of insert or fieldname=>value pairs of UPDATE */
    private $_values;
    /** @var array $_combination - union (or intersect or except) with the specified select type Query(ies) with similar fields. [Query, type, all] */
    private $_combination = [null, 'UNION', false];

    /** @var array $_params - the actual binded parameters */
    private $_params;
    /** @var string $_sql - the last generated raw sql before binding parameters */
    private $_sql;
    /** @var string $_finalSql - the last generated sql after binding parameters */
    private $_finalSql;
    /** @var string $_last - Last inserted id after executing insert query */
    private $_last;
    /** @var resource $_rs - result set of query */
    private $_rs = null;

    public function prepare() {
        if (is_string($this->_from))
            $this->_from = [$this->_from];

        if (!$this->_modelname && $this->_from) {
            if (!is_array($this->_from)) $this->_from = [$this->_from];
            $this->_modelname = $this->_from[ArrayUtils::getValue(array_keys($this->_from), 0, 0)];
        }
    }

    /**
     * @return string
     */
    public function getLast() {
        return $this->_last;
    }

    /**
     * @return resource
     */
    public function getRs() {
        return $this->_rs;
    }

    /**
     * Returns calculated SQL without binded parameters
     *
     * @return string
     * @throws
     */
    public function getSql() {
        if (!$this->_sql) {
            if (!$this->createConnection()) throw new QueryException('No database connection specified');
            $this->_sql = $this->connection->buildSQL($this);
        }
        return $this->_sql;
    }

    /**
     * @param string $sql
     *
     * @return Query
     * @throws QueryException
     */
    public function setSql($sql) {
        if (!$this->type) $this->type = 'sql';
        if ($this->type != 'sql') throw new QueryException('Sql property can be set directly only if type is `sql`');
        $this->_sql = $sql;
        $this->_finalSql = '';
        return $this;
    }

    /**
     * @return string
     */
    public function getModelname() {
        return $this->_modelname;
    }

    /**
     * @param $modelname
     */
    public function setModelname($modelname) {
        $this->_modelname = $modelname;
        $this->_sql = '';
        $this->_finalSql = '';
    }

    /**
     * @return array
     */
    public function getFields() {
        return $this->_fields;
    }

    /**
     * @param array $fields
     * @return Query
     */
    public function setFields($fields) {
        $this->_fields = $fields;
        $this->_sql = '';
        $this->_finalSql = '';
        return $this;
    }

    /**
     * @param array $fields
     * @return Query
     */
    public function updateFields($fields) {
        foreach ($fields as $name => $value) {
            if ($value === null) {
                unset($this->_fields[$name]);
            } else {
                $this->_fields[$name] = $value;
            }
        }
        $this->_sql = '';
        $this->_finalSql = '';
        return $this;
    }

    /**
     * @return string|array
     */
    public function getFrom() {
        return $this->_from;
    }

    /**
     * @param string|array $from
     * @return Query
     */
    public function setFrom($from) {
        $this->_from = $from;
        $this->_sql = '';
        $this->_finalSql = '';
        return $this;
    }

    /**
     * @return array
     */
    public function getJoins() {
        return $this->_joins;
    }

    /**
     * @param $joins
     * @return Query
     */
    public function setJoins($joins) {
        $this->_joins = $joins;
        $this->_sql = '';
        $this->_finalSql = '';
        return $this;
    }

    /**
     * @return array
     */
    public function getCondition() {
        return $this->_condition;
    }

    /**
     * @param $condition
     * @return Query
     */
    public function setCondition($condition) {
        $this->_condition = $condition;
        $this->_sql = '';
        $this->_finalSql = '';
        return $this;
    }

    /**
     * @return array
     */
    public function getWhere() {
        return $this->_condition;
    }

    /**
     * Sets the where condition. See {@see DBX::buildExpression()}
     * @param $condition
     * @return Query
     */
    public function setWhere($condition) {
        $this->_condition = $condition;
        $this->_sql = '';
        $this->_finalSql = '';
        return $this;
    }

    /**
     * @return array
     */
    public function getGroupby() {
        return $this->_groupby;
    }

    /**
     * @param $groupby
     * @return Query
     */
    public function setGroupby($groupby) {
        $this->_groupby = $groupby;
        $this->_sql = '';
        $this->_finalSql = '';
        return $this;
    }

    /**
     * @return array
     */
    public function getOrders() {
        return $this->_orders;
    }

    /**
     * @param $orders
     * @return Query
     */
    public function setOrders($orders) {
        $this->_orders = $orders;
        $this->_sql = '';
        $this->_finalSql = '';
        return $this;
    }

    /**
     * @return int
     */
    public function getLimit() {
        return $this->_limit;
    }

    /**
     * @param $limit
     * @return Query
     */
    public function setLimit($limit) {
        $this->_limit = $limit;
        $this->_sql = '';
        $this->_finalSql = '';
        return $this;
    }

    /**
     * @return int
     */
    public function getOffset() {
        return $this->_offset;
    }

    /**
     * @param $offset
     * @return Query
     */
    public function setOffset($offset) {
        $this->_offset = $offset;
        $this->_sql = '';
        $this->_finalSql = '';
        return $this;
    }

    /**
     * @return array
     */
    public function getValues() {
        return $this->_values;
    }

    /**
     * @param $values
     * @return Query
     */
    public function setValues($values) {
        $this->_values = $values;
        $this->_sql = '';
        $this->_finalSql = '';
        return $this;
    }

    /**
     * @param array $combine -- [Query, string, bool]
     * @return Query -- itself
     */
    public function setCombination($combine) {
        $this->_combination = $combine;
        $this->_sql = '';
        $this->_finalSql = '';
        return $this;
    }

    /**
     * @return array
     */
    public function getCombination() {
        return $this->_combination;
    }

    /**
     * @return array
     */
    public function getParams() {
        return $this->_params;
    }

    /**
     * @param $params
     * @return Query
     */
    public function setParams($params) {
        $this->_params = $params;
        $this->_finalSql = '';
        return $this;
    }

    // Query builder steps

    /**
     * @param $fields
     *
     * @return $this
     */
    public function select($fields) {
        $this->fields = $fields;
        return $this;
    }

    /**
     * Note: Fileds cannot be added if fields is null, because null denotes the build-time determination of the field names.
     *
     * @param $fields
     *
     * @return $this
     */
    public function addFields($fields) {
        $this->fields = array_merge($this->fields, $fields);
        return $this;
    }

    /**
     * Sets the distinct property. TODO: implement ON later
     *
     * @return $this
     */
    public function distinct() {
        $this->distinct = true;
        return $this;
    }

    /**
     * @param $from
     *
     * @return $this
     */
    public function from($from) {
        $this->from = $from;
        return $this;
    }

    /**
     * Replaces joins with given list
     * @param $joins
     *
     * @return $this
     */
    public function joins($joins) {
        $this->joins = $joins;
        return $this;
    }

    /**
     * Adds or replaces a join into the joins list
     *
     * @param string|array $join -- foreign key name or [model, jointype, condition] with alias (default is LEFT JOIN)
     * @param string $alias -- optional alias name. Overwrites former join with same alias name.
     *
     * @return $this
     * @throws Exception
     */
    public function join($join, $alias = null) {
        $this->addElement('joins', $join, $alias);
        return $this;
    }

    /**
     * Overwrites the where part of the Query.
     *
     * @param $condition
     *
     * @return $this
     */
    public function where($condition) {
        $this->condition = $condition;
        return $this;
    }

    /**
     * Appends a new condition to the existing ones using AND operator
     *
     * @param $condition
     *
     * @return $this
     * @throws Exception
     */
    public function andWhere($condition) {
        if (!is_array($this->condition)) $this->condition = ['AND'];
        if (!isset($this->condition[0])) $this->condition = ['AND', $this->condition];
        if (!is_string($this->condition[0]) || strtoupper($this->condition[0]) != 'AND') $this->condition = ['AND', $this->condition];
        $this->addElement('condition', $condition);
        return $this;
    }

    /**
     * Builds a condition expression with REGEXP operator if the filter value is nuot empty
     *
     * @param string|array $field -- field name or expression
     * @param string $value -- RegEx filter
     * @return Query
     * @throws Exception
     */
    public function filterRegEx($field, $value) {
        if ($value == '<null>') $this->andWhere(['IS NULL', $field]);
        if ($value == '<empty>') $this->andWhere(['OR', ['=', $field, "''"], ['IS NULL', $field]]);
        else if ($value != '' and $value !== null) {
            $db = $this->connection ?: UApp::getInstance()->db;
            $this->andWhere(['REGEXP', $field, $db->literal($value)]);
        }
        return $this;
    }

    /**
     * Builds a condition expression for integer ranges if the filter value is not empty.
     * Pattern must meet {@see Query::$integerRangePattern}
     * Operators can be used before number: .. ... - -- = == != <> < <= > >=
     * Operators can be used between numbers: - -- < <= .. ...
     * Operators can be used after number: .. ... - -- < <=
     *
     * @param string|array $field -- field name or expression
     * @param string $filterPattern -- integer or `< to` or `from -- to` or `> from` or list of them
     * @param array $orPart -- optional OR part is appended only if there was any filter applied.
     * @return Query
     * @throws Exception
     */
    public function filterIntegerRange($field, $filterPattern, $orPart = null) {
        if ($filterPattern != '' and $filterPattern !== null) {
            $filters = preg_split('~[,;]~', $filterPattern);
            $condition = ['OR'];

            $rules = [
                '~^(==?\s*)?(\d+)$~' => ['=', $field, '$2'],
                '~^(null|<null>)$~' => ['IS NULL', $field],
                '~^(\d+)\s*(--?|<|\.\.\.?)\s*(\d+)$~' => ['AND', ['>', $field, '$1'], ['<', $field, '$3']],
                '~^(\d+)\s*<=\s*(\d+)$~' => ['AND', ['>=', $field, '$1'], ['<=', $field, '$2']],
                '~^(--?|<|\.\.\.?)\s*(\d+)$~' => ['<', $field, '$2'],
                '~^<=\s*(\d+)$~' => ['<=', $field, '$1'],
                '~^>\s*(\d+)$~' => ['>', $field, '$1'],
                '~^>=\s*(\d+)$~' => ['>=', $field, '$1'],
                '~^(<>|!==?)\s*(\d+)$~' => ['!=', $field, '$2'],
                '~^(\d+)\s*(--?|<)$~' => ['>', $field, '$1'],
                '~^(\d+)\s*<=$~' => ['>=', $field, '$1'],
            ];
            foreach ($filters as $filter) {
                $filter = trim($filter);
                foreach ($rules as $pattern => $rule) {
                    if (preg_match($pattern, $filter, $mm)) {
                        $c = $this->replaceMatchInteger($rule, $mm);

                        // Join consecutive ['=', $field, '$1'] conditions
                        $last = max(array_keys($condition));
                        $c0 = $condition[$last];
                        if ($c[0] == '=' && is_array($c0) && $c0[0] == '=' && $c0[1] === $c[1]) {
                            $condition[$last] = ['IN', $c0[1], [$c0[2], $c[2]]];
                        } elseif ($c[0] == '=' && is_array($c0) && $c0[0] == 'IN' && $c0[1] === $c[1]) {
                            $condition[$last] = ['IN', $c0[1], array_merge($c0[2], [$c[2]])];
                        } else {
                            $condition[] = $c;
                        }
                        break;
                    }
                }
            }
            if (count($condition) > 1 && $orPart) $condition[] = $orPart;
            if (count($condition) == 1) $condition = null;
            elseif (count($condition) == 2) $condition = $condition[1];

            if ($condition) {
                $this->andWhere($condition);
            }
        }
        return $this;
    }

    /**
     * Builds a condition expression for datetime ranges if the filter value is not empty.
     * Pattern must meet {@see Query::$datetimeRangePattern}
     * Operators can be used before datetimes: .. ... - -- = == != <> < <= > >=
     * Operators can be used between datetime: - -- < <= .. ...
     * Operators can be used after datetime: .. ... - -- < <=
     *
     * Datetime format is `YYYY-MM-DDTH:i:s.000+01.00` or `YYYY-MM-DDTH:i:s` or `YYYY. MM. DD. H:i:s`
     *
     * @param string|array $field -- field name or expression
     * @param string $filterPattern -- integer or `< to` or `from -- to` or `> from` or list of them
     * @param array $orPart -- optional OR part is appended only if there was any filter applied.
     * @param string $formFieldInfo
     * @return Query
     * @throws ConfigurationException
     * @throws InternalException
     * @throws QueryException
     * @throws ReflectionException
     */
    public function filterDateTimeRange($field, $filterPattern, $orPart = null, $formFieldInfo = null) {
        if (!$formFieldInfo) $formFieldInfo = $field;
        if ($filterPattern != '' and $filterPattern !== null) {
            $filters = preg_split('~[,;]~', $filterPattern);
            $condition = ['OR'];
            $rules = [
                '~^(==?\s*)?(' . Query::dateTime . ')$~' => ['=', $field, '$2'],
                '~^(' . Query::partialdateTime . ')$~' => ['RLIKE', $field, '$1'],
                '~^(null|<null>)$~' => ['IS NULL', $field],
                '~^(' . Query::dateTime . ')\s*(--?|<|\.\.\.?)\s*(' . Query::dateTime . ')$~' => ['AND', ['>', $field, '$1'], ['<', $field, '$3']],
                '~^(' . Query::dateTime . ')\s*<=\s*(' . Query::dateTime . ')$~' => ['AND', ['>=', $field, '$1'], ['<=', $field, '$2']],
                '~^(--?|<|\.\.\.?)\s*(' . Query::dateTime . ')$~' => ['<', $field, '$2'],
                '~^<=\s*(' . Query::dateTime . ')$~' => ['<=', $field, '$1'],
                '~^>\s*(' . Query::dateTime . ')$~' => ['>', $field, '$1'],
                '~^>=\s*(' . Query::dateTime . ')$~' => ['>=', $field, '$1'],
                '~^(<>|!==?)\s*(' . Query::dateTime . ')$~' => ['!=', $field, '$2'],
                '~^(' . Query::dateTime . ')\s*(--?|<)$~' => ['>', $field, '$1'],
                '~^(' . Query::dateTime . ')\s*<=$~' => ['>=', $field, '$1'],
            ];
            foreach ($filters as $filter) {
                $filter = trim($filter);
                $matched = false;
                foreach ($rules as $pattern => $rule) {
                    \Codeception\Util\Debug::debug('Pattern: ' . $pattern);
                    \Codeception\Util\Debug::debug('Rule: ' . json_encode($rule));
                    if (preg_match($pattern, $filter, $mm)) {
                        \Codeception\Util\Debug::debug('Matched ' . json_encode($mm));
                        $matched = true;
                        $c = $this->replaceMatches($rule, $mm);

                        // Join consecutive ['=', $field, '$1'] conditions
                        $last = max(array_keys($condition));
                        $c0 = $condition[$last];
                        if ($c[0] == '=' && is_array($c0) && $c0[0] == '=' && $c0[1] === $c[1]) {
                            $condition[$last] = ['IN', $c0[1], [$c0[2], $c[2]]];
                        } elseif ($c[0] == '=' && is_array($c0) && $c0[0] == 'IN' && $c0[1] === $c[1]) {
                            $condition[$last] = ['IN', $c0[1], array_merge($c0[2], [$c[2]])];
                        } else {
                            $condition[] = $c;
                        }
                        break;
                    }
                }
                if (!$matched) throw new QueryException("Invalid timestamp filter expression `$filter` at field `$field`", $formFieldInfo);
            }
            if (count($condition) > 1 && $orPart) $condition[] = $orPart;
            if (count($condition) == 1) $condition = null;
            elseif (count($condition) == 2) $condition = $condition[1];

            if ($condition) {
                $this->andWhere($condition);
            }
        }
        return $this;
    }

    /**
     * Adds a filter condition on a foreign table using RegEx on foreign name field if pattern is not empty.
     *
     * @param string $field
     * @param string $pattern -- RegEx
     * @param string|Model $foreignClass
     * @param string $refName
     * @param string $refID
     * @return $this
     * @throws Exception
     */
    public function filterRef($field, $pattern, $foreignClass, $refName = 'name', $refID = 'id') {
        if ($pattern) {
            if (preg_match('~^\d+$~', trim($pattern))) $this->andWhere(['=', $field, (int)$pattern]);
            elseif (trim($pattern) == '<null>') $this->andWhere(['IS NULL', $field]);
            else {
                $db = $this->connection ?: UApp::getInstance()->db;
                $this->andWhere(['IN', $field, $foreignClass::createSelect([$refID])->where(['REGEXP', $refName, $db->literal($pattern)])]);
            }
        }
        return $this;
    }

    /**
     * Replace $n matches in array elements recursively.
     *
     * @param array $array -- array with some string leaf elements containing $n patterns
     * @param array $matches -- matches from a preg_match operation
     * @return array
     * @throws ConfigurationException
     * @throws InternalException
     * @throws ReflectionException
     */
    private function replaceMatchInteger($array, $matches) {
        if ($array === null || $matches === null) return $array;
        foreach ($array as &$value) {
            if (is_array($value)) $value = $this->replaceMatchInteger($value, $matches);
            if (is_string($value) && preg_match('~\$\d~', $value)) {
                if (preg_match('~^\$(\d+)$~', $value, $mm) && is_numeric($matches[$mm[1]])) $value = (int)$matches[$mm[1]];
                else $value = preg_replace_callback('~\$(\d+)~', function ($mm) use ($matches) {
                    return isset($matches[$mm[1]]) ? $matches[$mm[1]] : '';
                }, $value);
            }
        }
        return $array;
    }

    /**
     * Replace $n matches in array elements recursively.
     *
     * @param array $array -- array with some string leaf elements containing $n patterns
     * @param array $matches -- all matches from a previous preg_match operation
     * @return array
     * @throws ConfigurationException
     * @throws InternalException
     * @throws ReflectionException
     */
    private function replaceMatches($array, $matches) {
        if ($array === null || $matches === null) return $array;
        foreach ($array as &$value) {
            if (is_array($value)) $value = $this->replaceMatches($value, $matches);
            if (is_string($value) && preg_match('~\$\d~', $value)) {
                if (preg_match('~^\$(\d+)$~', $value, $mm)) $value = "'" . $matches[$mm[1]] . "'";
                else $value = preg_replace_callback('~\$(\d+)~', function ($mm) use ($matches) {
                    return isset($matches[$mm[1]]) ? $matches[$mm[1]] : '';
                }, $value);
            }
        }
        return $array;
    }


    /**
     * Sets the limit part of the query.
     * Returns itself for chaining
     *
     * @param int $limit
     * @return $this
     */
    public function limit($limit) {
        $this->limit = $limit;
        return $this;
    }

    /**
     * Sets the offset part of the query.
     * Returns itself for chaining
     *
     * @param int $offset
     *
     * @return $this
     */
    public function offset($offset) {
        $this->offset = $offset;
        return $this;
    }

    /**
     * Sets the order part of the query.
     * Returns itself for chaining
     *
     * @param array|string|null $orders -- array of order strings or order definition arrays or a single order field name.
     *
     * @return $this
     */
    public function orderBy($orders) {
        $this->orders = $orders;
        return $this;
    }

    /**
     * Sets the GROUP BY part of the query.
     * Returns itself for chaining
     *
     * @param array $groupby -- array of order strings or order definition arrays.
     *
     * @return $this
     */
    public function groupBy($groupby) {
        $this->groupby = $groupby;
        return $this;
    }

    /**
     * Sets the combine property.
     * Parameters of combined query will merge into main parameters.
     *
     * @param Query $query
     * @param string $type
     * @param bool $all
     * @return $this
     * @throws QueryException
     */
    public function combine($query, $type = 'UNION', $all = false) {
        if ($this->type !== 'select') throw new QueryException('Base query must be a SELECT.');
        if ($query->type !== 'select') throw new QueryException('Combined query must be a SELECT.');
        $type = strtoupper($type);
        if (!in_array($type, ['UNION', 'INTERSECT', 'EXCEPT'])) throw new QueryException('Érvénytelen Query kombináció: ', $type);
        $this->combination = [$query, $type, $all];
        $this->setParams($query->params);
        return $this;
    }

    /**
     * Defines an UNION select
     * Parameters of combined query will merge into main parameters.
     *
     * @param Query $query
     * @param bool $all
     * @return $this
     * @throws QueryException
     */
    public function union($query, $all = false) {
        if ($this->type !== 'select') throw new QueryException('Base query must be a SELECT.');
        if ($query->type !== 'select') throw new QueryException('Union query must be a SELECT.');
        $this->combination = [$query, 'UNION', $all];
        return $this;
    }

    /**
     * Sets given connection or default
     *
     * Returns current connection.
     *
     * @param DBX $connection
     *
     * @return DBX
     * @throws InternalException
     * @throws QueryException
     */
    public function createConnection($connection = null) {
        if ($connection && !$connection instanceof DBX) throw new QueryException('Invalid connection parameter', gettype($connection));
        if ($connection) $this->connection = $connection;
        try {
            if (!$this->connection) $this->connection = UApp::getInstance()->db;
        } catch (Exception $e) {
            throw new QueryException('Invalid connection', $connection, $e);
        }
        return $this->connection;
    }

    /**
     * Executes a non-select query
     * Returns number of affected rows or false on failure.
     *
     * @param DBX $connection -- database connection. Default is defined for query or default of application
     *
     * @return integer|false
     * @throws InternalException
     * @throws QueryException
     */
    public function execute($connection = null) {
        $this->createConnection($connection);
        $this->_last = null;
        $this->_rs = $this->connection->query($this->sql, $this->params);
        // Last lekérdezése nem megbízható
        if ($this->type == 'insert') @$this->_last = $this->connection->last_id();
        return $this->rs ? $this->connection->affected_rows($this->rs) : false;
    }

    /**
     * Executes a select query.
     * Returns all rows or false on failure.
     * The result is an array of values arrays only, empty array if no rows.
     * The difference between all() and selectAll() is that all returns associative rows while selectAll returns only values.
     *
     * @param DBX $connection -- database connection. Default is defined for query or default of application
     *
     * @return array
     * @throws InternalException
     * @throws QueryException
     */
    public function selectAll($connection = null) {
        $this->createConnection($connection);
        if (!($this->type == 'select' || !$this->type)) throw new QueryException('`select` can be performed on select queries only.');
        return $this->connection->select_rows($this->sql, $this->params);
    }

    /**
     * Executes a select query
     * Returns all rows or false on failure.
     * The result is an array of Models if modelclass is specified, associative arrays of field=>value pairs otherwise.
     *
     *
     * @param string|bool $modelclass -- optional modelclass to convert result into. If true is specified, the modelname of the Query will be used.
     * @param DBX $connection -- database connection. Default is defined for query or default of application
     *
     * @return array
     * @throws DatabaseException
     * @throws InternalException
     * @throws QueryException
     */
    public function all($modelclass = null, $connection = null) {
        if (!($this->type == 'select' || $this->type == 'sql' || !$this->type)) throw new QueryException('`all` can be performed on `select` or `sql` queries only.');
        if ($modelclass === true && $this->modelname) $modelclass = $this->modelname;

        $this->createConnection($connection);

        $result = []; // select_all will return results here
        $n = $this->connection->select_all($result, $this->sql, $this->params);
        if ($n === false) {
            throw new QueryException('SQL hiba ', $this->connection->error());
        }

        if ($modelclass) {
            if (!class_exists($modelclass)) throw new QueryException("Not existing model '$modelclass'");
            if (!is_callable([$modelclass, 'attributes'])) throw new QueryException("Incompatible model class '$modelclass'");
            $attributes = call_user_func([$modelclass, 'attributes'], $this->connection); // OK
            try {
                $db = $this->connection;
                $result = @array_map(function ($row) use ($modelclass, $attributes, $db) {
                    //Restrict row to attributes of the model
                    $row1 = [];
                    foreach ($attributes as $a) {
                        $row1[$a] = ArrayUtils::getValue($row, $a);
                    }
                    /* php v5.6 only version:
                        $row = array_filter($row, function(
                            $v, $k) use($attributes) { return in_array($k, $aa); }, ARRAY_FILTER_USE_BOTH);
                    */

                    /** @var Model $model */
                    $model = new $modelclass($row1, $db);
                    $model->setOldAttributes($row1);
                    return $model;
                }, $result);
            } catch (Exception $e) {
                throw new InternalException("Query is incompatible with model `$modelclass`: " . $e->getMessage(), null, $e);
            }
        }

        return $result;
    }

    /**
     * Executes a select query
     *
     * Returns first rows or false on empty set or error.
     * The result is an associative arrays in field=>value form.
     *
     * @param DBX $connection -- database connection. Default is defined for query or default of application
     * @param string|bool $modelClass -- modelClass to use for result, true=Query's primary model, false=return array
     *
     * @return array|BaseModel|false
     * @throws DatabaseException
     * @throws InternalException
     * @throws QueryException
     */
    public function first($connection = null, $modelClass = false) {
        $this->createConnection($connection);
        if (!($this->type == 'select' || !$this->type)) throw new QueryException('`first` can be performed on select queries only.');
        $this->limit(1);

        if ($modelClass === true && $this->modelname) $modelClass = $this->modelname;

        $data = $this->connection->select_assoc($this->sql, $this->params);
        if ($data === false || !$modelClass) return $data;

        // Prepare model
        if (!class_exists($modelClass)) throw new QueryException("Not existing model '$modelClass'");
        if (!is_callable([$modelClass, 'attributes'])) throw new QueryException("Incompatible model class '$modelClass'");
        $attributes = call_user_func([$modelClass, 'attributes'], $this->connection);
        $data1 = null;
        foreach ($attributes as $a) {
            $data1[$a] = ArrayUtils::getValue($data, $a);
        }
        /** @var Model $model */
        $model = new $modelClass($data1, $this->connection);
        $model->setOldAttributes($data1);
        return $model;
    }

    /**
     * Returns one scalar result
     * If number of result records is 0, the default value will be returned.
     *
     * @param mixed $default -- default value returned if no record found
     * @param DBX $connection -- database connection. Default is defined for query or default of application
     *
     * @return mixed
     * @throws DatabaseException
     * @throws InternalException
     * @throws QueryException
     */
    public function scalar($default = null, $connection = null) {
        $this->createConnection($connection);
        if (!($this->type == 'select' || !$this->type)) throw new QueryException('`scalar` can be performed on select queries only.');
        return $this->connection->selectvalue($this->sql, $this->params, $default);
    }

    /**
     * Executes the select query, and returns the data in the first column in an array
     *
     * @param integer $c -- the number of the column, default 0
     * @param DBX $connection -- database connection. Default is defined for query or default of application
     *
     * @return array
     * @throws DatabaseException
     * @throws InternalException
     * @throws QueryException
     */
    public function column($c = 0, $connection = null) {
        $this->createConnection($connection);
        if (!($this->type == 'select' || !$this->type)) throw new QueryException('`column` can be performed on select queries only.');
        return $this->connection->select_column($this->sql, $this->params, $c);
    }

    /**
     * Creates nodes from full select
     * Result field values will be attributes, '_content' will be node text content.
     *
     * @param UXMLElement $node_parent -- parent of new nodes
     * @param string $nodename -- name of new nodes, default is tablename
     * @param null $connection
     *
     * @return UXMLElement -- the first node created or null if none
     * @throws DatabaseException
     * @throws InternalException
     * @throws QueryException
     * @throws UAppException
     * @throws DOMException
     */
    public function nodes($node_parent, $nodename = null, $connection = null) {
        $this->createConnection($connection);
        $node_first = null;
        if (!$nodename) $nodename = Util::uncamelize($this->modelname);
        $result = $this->all();
        if (!$result) return null;
        foreach ($result as $row) {
            $content = isset($row['_content']) ? $row['_content'] : null;
            unset($row['_content']);
            $node = $node_parent->addNode($nodename, $row, $content);
            if (!$node_first) $node_first = $node;
        }
        return $node_first;
    }

    /**
     * Returns the error message associated to the last execution
     *
     * @return string
     */
    public function error() {
        return $this->connection->error($this->rs);
    }

    /**
     * Associates parameters to the Query
     * (The SQL of the query is not modified, substitution occures only at execution)
     * The substituted SQL may be extracted by finalSQL
     *
     * - array: parameters are added to existing ones (including ones with numeric indices)
     * - single scalar value: added to the end of the list of numeric parameters
     *
     * @param array|mixed $values -- the parameter array, or associative array or may be single literal except of null
     * @return Query -- the query itself
     * @throws QueryException
     */
    public function bind($values) {
        $this->_finalSql = '';
        if (empty($values)) return $this;
        if (is_scalar($values)) {
            $this->_params[] = $values;
        } else if (is_array($values)) {
            foreach ($values as $key => $value) {
                $this->_params[$key] = $value;
            }
        } else throw new QueryException('Invalid parameter value type ' . gettype($values));
        return $this;
    }

    /**
     * Creates final SQL from parametrized SQL and parameter array.
     * Substitutes both $1 and $name style parameters
     *
     * @return string
     * @throws DatabaseException
     * @throws InternalException
     * @throws QueryException
     */
    public function getFinalSQL() {
        if (!$this->_finalSql) {
            $this->createConnection();
            $this->_finalSql = $this->connection->bindParams($this->sql, $this->params);
        }
        return $this->_finalSql;
    }

    /**
     * Creates a Query object and builds a SELECT sql with single main table
     *
     * Alternative call method: only parameter is an associative options array
     *
     * ### Available options:
     * - `from` (replaces _from_ parameter) -- model classname or array of model names
     * - `modelname` -- primary modelname, default is first model of 'from'
     * - `joins` (see _joins_ parameter)
     * - `fields` (see _fields_ parameter)
     * - `condition` (see _condition_ parameter)
     * - `where` (array) -- alias of condition
     * - `groupby` (array) -- GROUP BY part of select
     * - `orders` (see _orders_ parameter)
     * - `limit` (see _limit_ parameter)
     * - `offset` (see _offset_ parameter)
     * - `params` (see _params_ parameter)
     * - `connection` (see _connection_ parameter)
     *
     * @param string|array $from -- main table model name or options array (Model name is classname, usually begins with capital)
     * @param array $fields -- field names to select, or alias => fieldname
     *        Default is all fields of the main table.
     *        Dotted notation is accepted for joined relations
     *        Undotted fields will use first model's alias if given
     *        Fieldname may in alias => (expression) or alias => array(expression) form.
     * @param array $joins -- list of joined models (foreign keys) OR alias=>fk OR alias=>array(model, [jointype], conditions) element
     *        Join ON conditions may be
     *            - an associative array with mainfield=>foreignfield form, or
     *        - any other (non-associative) expression, use array($expression)
     * @param array $condition -- Condition expression. Use literal values as numbered ($1) or named ($name) parameters.
     *        For and-ed list of conditions use `array('AND', expression, expression)` or `array(fieldname=>expression,...)` form
     *        For literal condition expression use array('(conditon)'); (see also {@see DBX::buildExpression()})
     *        You may use a single scalar value for matching to single primary key.
     * @param array $orders -- numeric indexed array of fields or [field, direction] arrays (priority order is important); order may be expression in second form
     * @param integer $limit
     * @param integer $offset
     * @param array $params -- number-referenced values for $1 sql parameters or name=>value pairs for $name parameters (params may be single literal, except of null)
     * @param DBX $connection
     *
     * @return Query
     * @throws QueryException
     * @throws InternalException*@throws UAppException
     * @throws UAppException
     */
    public static function createSelect($from = null, $fields = null, $joins = null, $condition = null, $orders = null, $limit = null, $offset = null, $params = null, $connection = null) {
        if (is_array($from) && func_num_args() == 1) {
            $options = $from;
            $modelname = ArrayUtils::getValue($options, 'modelname');
            $from = ArrayUtils::getValue($options, 'from');
            if (is_array($from)) {
                $options['from'] = $from;
                $options['modelname'] = $from[ArrayUtils::getValue(array_keys($from), 0, 0)];
            } else if (is_array($modelname)) {
                $options['from'] = $modelname;
                $options['modelname'] = $modelname[ArrayUtils::getValue(array_keys($modelname), 0, 0)];
            }
            if (is_string($from)) {
                $options['from'] = [$from];
                $options['modelname'] = $from;
            } else if (is_string($modelname)) {
                $options['from'] = [$modelname];
                $options['modelname'] = $modelname;
            }
            $query = new Query(array_merge(['type' => 'select'], $options));
        } else {
            if ($connection && !$connection instanceof DBX) throw new QueryException('Invalid connection parameter', gettype($connection));
            $query = new Query(['type' => 'select',
                'modelname' => is_array($from) ? $from[ArrayUtils::getValue(array_keys($from), 0, 0)] ?? null : $from,
                'from' => is_array($from) ? $from : [$from],
                'joins' => $joins,
                'fields' => $fields,
                'condition' => $condition,
                'orders' => $orders,
                'limit' => $limit,
                'offset' => $offset,
                'params' => $params,
                'db' => $connection,
            ]);
        }
        return $query;
    }

    /**
     * Creates an 'sql' type qurey, based on a parametrized sql string.
     *
     * @param $sql
     * @param array|null $params
     * @param DBX|null $connection
     *
     * @return Query
     * @throws InternalException
     * @throws UAppException
     */
    public static function createSql($sql, $params = null, $connection = null) {
        return new Query(['sql' => $sql, 'params' => $params, 'connection' => $connection]);
    }

    /**
     * Creates a Query object and builds an INSERT sql
     *
     * @param $modelname -- model name or options array for all other parameters
     * @param array $fields --
     *   - if null, model attributes are used (or values' keys if associative)
     *   - if $values is null, $field must contain fieldname=>expression pairs. For literal values use associative $values
     * @param array $values -- array of row value arrays
     *   - if $fields includes values, must be NULL, otherwise NULL is not allowed.
     *   - if $values is a string, will be used literally
     *   - if values is an array, the literal values will be inserted.
     *     - if values is an associtive array, it considered as fieldname=>literalvalues. $fields must be null. Use for single row only.
     *   - if values is a Query, a subquery will be used.
     *   expressions not allowed in $values.
     * @param DBX $connection -- default is app's connection
     *
     * Alternative call method: first parameter is an associative options array with the above options
     * Insert does not support parameters.
     *
     * Alternative build:
     *
     * createInsert(modelname)->into(fieldnames)->values(values-or-query)
     *
     * @return Query -- the query object
     * @throws InternalException
     * @throws QueryException
     * @throws UAppException
     */
    public static function createInsert($modelname, $fields = null, $values = null, $connection = null) {
        if (is_array($modelname) && func_num_args() == 1) {
            $options = $modelname;
            $query = new Query(array_merge(['type' => 'insert'], $options));
        } else {
            if ($connection && !$connection instanceof DBX) throw new QueryException('Invalid connection parameter', gettype($connection));
            $query = new Query(['type' => 'insert',
                'modelname' => $modelname,
                'fields' => $fields,
                'values' => $values,
                'db' => $connection,
            ]);
        }
        return $query;
    }

    /**
     * Creates a Query object and builds an UPDATE sql
     *
     * @param string|array $modelname -- modelname of the main table or options array
     * @param array $values -- fieldname=>expression pairs (for explicit literal values, use $db->literal() on values)
     * @param array $condition
     * @param array $params
     * @param DBX $connection
     *
     * Alternative call method: first parameter is an associative options array with the above options
     *
     * @return Query -- the query object
     * @throws InternalException
     * @throws QueryException
     * @throws UAppException
     */
    public static function createUpdate($modelname, $values = null, $condition = null, $params = null, $connection = null) {
        if (is_array($modelname) && func_num_args() == 1) {
            $options = $modelname;
            $query = new Query(array_merge(['type' => 'update'], $options));
        } else {
            if ($connection && !$connection instanceof DBX) throw new QueryException('Invalid connection parameter', gettype($connection));
            $query = new Query(['type' => 'update',
                'modelname' => $modelname,
                'values' => $values,
                'condition' => $condition,
                'params' => $params,
                'db' => $connection,
            ]);
        }
        return $query;
    }

    /**
     * Creates a Query object and builds a DELETE  sql
     *
     * @param string|array $modelname -- main table modelname or options array
     * @param array $condition -- expression
     * @param array $params
     * @param DBX $db
     *
     * Alternative call method: first parameter is an associative options array with the above options
     *
     * @return Query -- the query object
     * @throws InternalException
     * @throws QueryException
     * @throws UAppException
     */
    public static function createDelete($modelname, $condition = null, $params = null, $db = null) {
        if (is_array($modelname) && func_num_args() == 1) {
            $options = $modelname;
        } else {
            $options = [
                'modelname' => $modelname,
                'condition' => $condition,
                'params' => $params,
                'db' => $db,
            ];
        }
        if (isset($options['db']) && !($options['db'] instanceof DBX)) throw new QueryException('Invalid connection parameter', $options['db']);
        return new Query(array_merge(
                ['type' => 'delete'],
                $options)
        );
    }

    /**
     * @return DBX
     * @deprecated use connection field
     */
    public function getDb() {
        return $this->connection;
    }

    public function setDb($connection) {
        $this->connection = $connection;
        return $this;
    }

    /**
     * Returns Query data as array
     *
     * @return array
     */
    public function toArray() {
        $fields = [
            'sql' => ['sql'],
            'select' => ['fields', 'from', 'joins', 'condition', 'orders', 'limit', 'offset'],
            'insert' => ['modelname', 'fields', 'values'],
            'update' => ['modelname', 'values', 'condition'],
            'delete' => ['modelname', 'condition'],
        ];
        $result = [
            'type' => $this->type,
        ];
        $fields = ArrayUtils::getValue($fields, $this->type, [
            'modelname', 'fields', 'from', 'joins', 'condition', 'orders', 'limit', 'offset', 'values'
        ]);
        foreach ($fields as $fieldName) {
            /** @noinspection PhpVariableVariableInspection */
            $result[$fieldName] = $this->$fieldName;
        }
        return $result;
    }

    /**
     * Returns Query data for debugging
     *
     * @return string
     */
    public function toString() {
        return Util::objtostr($this->toArray());
    }

    /**
     * Returns an iterator to iterate thru Query select results.
     *
     * May be used only on `select` type queryies.
     * Iterator's current() method returns array or the specified model object.
     *
     * @param bool|string|BaseModel $modelClass -- use specified modelClass, or Query's primary model. false = return arrays
     * @param null $connection -- sets connection or uses default connection
     *
     * @return QueryIterator
     * @throws InternalException
     * @throws QueryException
     * @throws UAppException
     */
    public function iterate($modelClass = true, $connection = null) {
        $this->createConnection($connection);
        return new QueryIterator([
            'query' => $this,
            'modelClass' => $modelClass
        ]);
    }

    /**
     * Returns number of records in the query
     * (Replaces fields to count(fields) and returns {@see scalar()} result)
     *
     * @param array|string $fields
     * @param null $connection
     *
     * @return int|null
     * @throws DatabaseException
     * @throws InternalException
     * @throws QueryException
     * @throws UAppException
     */
    public function count($fields = '*', $connection = null) {
        $this->createConnection($connection);
        $oldFields = $this->fields;
        if (!is_array($fields)) $fields = [$fields];
        $this->fields = ['(count(' . $this->connection->buildExpressionList($fields) . '))'];
        $result = $this->scalar();
        $this->fields = $oldFields;
        return $result;
    }
}
