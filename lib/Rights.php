<?php
/**
 * class Rights
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2019.
 * @license MIT
 */

/**
 * A right source.
 *
 * Implements session caching computed effective rights.
 * Use only one instance.
 */
abstract class Rights {
    static private $cache;
    static public $rightbits; // array(bit=>array(value, title, descr), ...)

    public function __construct() {
        /*
            Override:
                define self::$rightbits table
        */
        static::initCache();
    }

    public static function initCache() {
        if (!is_array(self::$cache))
            self::$cache = Util::getSession('rightcache', []);
        return self::$cache;
    }

    public static function save() {
        #Debug::tracex('cache', self::$cache);
        $_SESSION['rightcache'] = self::$cache;
    }

    abstract function loadRightBits($node_parent);

    /**
     * Empty right cache
     *
     * @return void
     */
    public static function reload() {
        #Debug::trace('Rightcache emptied');
        $_SESSION['rightcache'] = self::$cache = [];
    }

    /**
     *    Returns an effective permission bit's value of this user
     *
     * @param int $type - righttype, as defined in custom descendant of Rights
     * @param int $user - user id
     * @param int $obj - reference to an object of given type or global if empty
     * @param int $bit - bitmask of an elementary permission (2^n), as defined in custom descendant of Rights
     *
     * @return int - 0 on no right, bit's value on
     * @throws UAppException
     */
    public function right($type, $user, $obj, $bit) {
        $rv = $this->rightValue($type, $user, $obj);
        return $rv & $bit;
    }

    /**
     * Gets a bitmapped rightvalue of right of type for an object. Uses local session cache
     *
     * @param int $type - righttype, as defined in custom descendant of Rights
     * @param int $user - user id
     * @param int $obj - reference to an object of given type or global if empty
     *
     * @return int - the value of rights
     * @throws UAppException
     */
    public function rightValue($type, $user, $obj = null) {
        if ($obj !== null) Util::assertInt($obj);
        $class = get_class($this);
        if (!is_callable([$this, 'getRightValue'])) throw new UAppException("class '$class' has no method 'getRightValue'");
        if (gettype($type) == 'object' || gettype($user) == 'object') throw new UAppException('Invalid argument type. Number excepted.');
        if (isset(self::$cache[$key = $type . '_' . $user . '_' . $obj])) {
            #Debug::tracex('Right from cache', "$type.'_'.$user.'_'.$obj = ".self::$cache[$key]);
            return self::$cache[$key];
        }
        return self::$cache[$key] = $this->getRightValue($type, $user, $obj);
    }

    /**
     * Stores a value into cache for the session-time.
     *
     * @param int $type
     * @param int $user
     * @param int $obj
     * @param int $value
     * @return int -- the value
     */
    public static function cacheValue($type, $user, $obj, $value) {
        $key = $type . '_' . $user . '_' . $obj;
        return self::$cache[$key] = $value;
    }

    /**
     * Retrieves a rightvalue from database. Use rightValue instead.
     *
     * @param int $type
     *   righttype, one of RT_XXX constants
     * @param int $user
     *   user id
     * @param int $obj
     *   reference to an object of given type
     * @return int
     *   the value of rights
     */
    abstract protected function getRightValue($type, $user, $obj);

    /**
     * Uses rightcache to store a user-calculated right-like value
     *
     * @param string $name -- reference base name beginning with letters
     * @param array $params -- params for function and cache name
     * @param callable $fn -- inline function to calculate value if it does not exist in cache
     *
     * @return mixed -- the cached value (as it returned by anonymous function))
     * @throws InternalException
     */
    public static function useCache($name, $params, $fn) {
        Util::assertArray($params);
        $key = $name . '_' . implode('_', $params);
        if (!$key) throw new InternalException('Invalid cache parameters');
        static::initCache();
        $cf = array_key_exists($key, self::$cache);
        #Debug::tracex('cache', "$key, $cf");
        return $cf ? self::$cache[$key] : (self::$cache[$key] = $fn($params));
    }
}
