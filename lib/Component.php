<?php
/**
 * class Component
 *
 * @author Peter Uherkovich
 * @copyright 2017-2019
 * @access public
 */

/**
 * A configurable base class for most of others.
 * - Implements *property* features: magic getter and setter uses getProperty and setProperty methods
 * - Configurable: constructor accepts a configuration array containing values for public properties
 */
abstract class Component {
    /**
     * # Component constructor
     * The default implementation does two things:
     *
     * - Initializes the object with the given configuration `$config`.
     * - Calls [[prepare()]].
     *
     * If this method is overridden in a child class, it is recommended that
     *
     * - the last parameter of the constructor is a configuration array, like `$config` here.
     * - call the parent implementation in the constructor.
     *
     * @param array $config name-value pairs that will be used to initialize the object properties
     *
     * @throws InternalException
     * @throws UAppException
     */
    public function __construct($config = []) {
        if (!empty($config)) {
            if (array_key_exists('class', $config)) {
                $className = $config['class'];
                unset($config['class']);
                $current = get_called_class();
                if ($className !== $current) throw new UAppException("Invalid component config. Class must be `$current`, got `$className`");
            }
            static::configure($this, $config);
        }
        $this->prepare();
    }

    /**
     * Initializes the object.
     * This method is invoked at the end of the constructor after the object is initialized with the
     * given configuration. Default does nothing, override it if you want to use.
     */
    public function prepare() {
    }

    /**
     * Returns the fully qualified name of this class.
     * @return string the fully qualified name of this class.
     */
    public static function className() {
        return get_called_class();
    }

    /**
     * Returns names of public properties in an array
     *
     * @return array
     * @throws ReflectionException
     */
    static public function publicProperties() {
        $class = new ReflectionClass(static::className());
        $names = [];
        foreach ($class->getProperties(ReflectionProperty::IS_PUBLIC) as $property) {
            if (!$property->isStatic()) {
                $names[] = $property->getName();
            }
        }
        return $names;
    }

    /**
     * Returns the value of a component property.
     * @param string $name the property name
     * @return mixed the property value
     * @throws InternalException if the property is not defined or the property is write-only.
     * @see __set()
     */
    public function __get($name) {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        }

        if (method_exists($this, 'set' . $name)) {
            throw new InternalException('Getting write-only property: ' . get_class($this) . '::' . $name);
        }

        throw new InternalException('Getting unknown property: ' . get_class($this) . '::' . $name);
    }

    /**
     * Sets the value of a component property.
     * @param string $name the property name
     * @param mixed $value the property value
     * @throws InternalException if the property is not defined or the property is read-only.
     * @see __get()
     */
    public function __set($name, $value) {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            $this->$setter($value);
            return;
        }
        if (method_exists($this, 'get' . $name)) {
            throw new InternalException('Setting read-only property: ' . get_class($this) . '::' . $name);
        }
        throw new InternalException('Setting unknown property: ' . get_class($this) . '::' . $name);
    }

    /**
     * Checks if a property is set: defined and not null.
     * @param string $name the property name
     * @return bool whether the named property is set
     * @see http://php.net/manual/en/function.isset.php
     */
    public function __isset($name) {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter() !== null;
        }
        return false;
    }

    /**
     * Sets a component property to be null.
     * @param string $name the property name
     * @throws InternalException if the property is read only.
     * @see http://php.net/manual/en/function.unset.php
     */
    public function __unset($name) {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            $this->$setter(null);
            return;
        }
        throw new InternalException('Unsetting an unknown or read-only property: ' . get_class($this) . '::' . $name);
    }

    /**
     * Returns a value indicating whether a property is defined for this component.
     * @param string $name the property name
     * @param bool $checkVars whether to treat member variables as properties
     * @return bool whether the property is defined
     * @see canGetProperty()
     * @see canSetProperty()
     */
    public function hasProperty($name, $checkVars = true) {
        return $this->canGetProperty($name, $checkVars) || $this->canSetProperty($name, false);
    }

    /**
     * Returns a value indicating whether a property can be read.
     * @param string $name the property name
     * @param bool $checkVars whether to treat member variables as properties
     * @return bool whether the property can be read
     * @see canSetProperty()
     */
    public function canGetProperty($name, $checkVars = true) {
        if (method_exists($this, 'get' . $name) || $checkVars && property_exists($this, $name)) {
            return true;
        }
        return false;
    }

    /**
     * Returns a value indicating whether a property can be set.
     * @param string $name the property name
     * @param bool $checkVars whether to treat member variables as properties
     * @return bool whether the property can be written
     * @see canGetProperty()
     */
    public function canSetProperty($name, $checkVars = true) {
        if (method_exists($this, 'set' . $name) || $checkVars && property_exists($this, $name)) {
            return true;
        }
        return false;
    }

    /**
     * Configures an object with the initial property values.
     *
     * @param object $object the object to be configured
     * @param array $properties the property initial values given in terms of name-value pairs.
     *
     * @return object the object itself
     * @throws InternalException
     */
    public static function configure($object, $properties = null) {
        if ($properties === null) return $object;
        Util::assertArray($properties);
        foreach ($properties as $name => $value) {
            /** @noinspection PhpVariableVariableInspection */
            $object->$name = $value;
        }
        return $object;
    }

    /**
     * Creates a new Component using the given configuration.
     *
     * You may view this method as an enhanced version of the `new` operator.
     * The method supports creating an object based on a class name, a configuration array or
     * an anonymous function.
     *
     * Below are some usage examples:
     *
     * ```php
     * // create an object using a class name
     * $object = Component::create('Customer');
     *
     * // create an object using a configuration array
     * $object = Component::createObject([
     *     'class' => 'Customer',
     *     'name' => 'Foo Baar',
     * ]);
     *
     * // create an object with two constructor parameters
     * $object = Component::create('Customer', [$param1, $param2]);
     * ```
     *
     * @param string|array|callable $type the object type. This can be specified in one of the following forms:
     *
     * - a string: representing the class name of the object to be created
     * - a configuration array: the array must contain a `class` element which is treated as the object class,
     *   and the rest of the name-value pairs will be used to initialize the corresponding object properties
     * - a configuration array: the array must contain a `0` element which is treated as the object class,
     *   and the rest of the name-value pairs will be used to initialize the corresponding object properties
     * - a PHP callable: either an anonymous function or an array representing a class method (`[$class or $object, $method]`).
     *   The callable should return a new instance of the object being created.
     *
     * @param array $params the constructor parameters
     *
     * @return Component|object the created object
     * @throws InternalException if the configuration is invalid.
     * @throws ReflectionException
     */
    public static function create($type, array $params = []) {
        if (is_string($type)) {
            return static::createClass($type, $params);
        } elseif (is_array($type) && isset($type['class'])) {
            $class = $type['class'];
            unset($type['class']);
            return static::createClass($class, $type);
        } elseif (is_array($type) && isset($type[0])) {
            $class = $type[0];
            unset($type[0]);
            return static::createClass($class, $type);
        } elseif (is_callable($type, true)) {
            return call_user_func_array($type, $params);
        } elseif (is_array($type)) {
            throw new InternalException('Object configuration must be an array containing a "class" element.');
        }
        throw new InternalException('Unsupported configuration type: ' . gettype($type));
    }

    /**
     * @param $class
     * @param $config
     *
     * @return object
     * @throws ReflectionException
     */
    private static function createClass($class, $config) {
        $reflection = new ReflectionClass($class);
        $object = $reflection->newInstanceArgs([$config]);
        return $object;
    }

    /**
     * Converts object to array using it's public properties
     *
     * @return array
     * @throws ReflectionException
     */
    public function toArray() {
        $data = [];
        foreach ($this->publicProperties() as $field) {
            /** @noinspection PhpVariableVariableInspection */
            $data[$field] = $this->$field;
        }
        return $data;
    }

    /**
     * Append or assign an element to the array property.
     * If the property is null, a new array is initialized.
     *
     * @param $property -- the name of the array property
     * @param $value -- the element to be assigned or appended
     * @param $index -- an optional index to assign to, the default is appending to the end.
     * @return static
     * @throws Exception -- If the property does not exist or is not an array
     */
    public function addElement($property, $value, $index = null) {
        // Check that $property exists in the object
        if (property_exists($this, $property) || $this->hasProperty($property)) {
            if ($this->$property === null) $this->$property = [];
            if (!is_array($this->$property)) throw new Exception('Porperty ' . $property . ' is not an array');
            if ($index !== null) {
                $v = $this->$property;
                $v[$index] = $value;
                $this->$property = $v;
            } else {
                $v = $this->$property;
                $v[] = $value;
                $this->$property = $v;
            }
        } else {
            throw new Exception('Porperty ' . $property . ' is not found');
        }
        return $this;
    }
}
