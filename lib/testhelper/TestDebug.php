<?php

/**
 * A test-replacement form UApp/Debug module
 *
 * Using:
 * 1. include this file in `tests/unit/_bootstrap.php`
 * 2. enable _bootstrap in (global or suite) yml:
 * settings:
 *     # name of bootstrap that will be used
 *     # each bootstrap file should be
 *     # inside a suite directory.
 *     bootstrap: _bootstrap.php
 *
 */
class TestDebug {
    /** @var boolean $nodebug -- a swith to switch off debug messages. */
    static public $nodebug = false;
    static $trace = 'test-trace';

    /**
     * @param string $msg
     * @param string $params
     * @param int $depth
     * @param string $tags
     *
     * @throws ConfigurationException
     * @throws InternalException
     */
    static function trace($msg = '*', /** @noinspection PhpUnusedParameterInspection */
                          $params = '', $depth = 1, $tags = 'app') {
        if (is_array($msg) || is_object($msg)) $msg = Util::objtostr($msg);
        $da = debug_backtrace();
        if ($tags == 'sql') {
            for ($i = $depth; $i < count($da); $i++) {
                $dx = $da[$i];
                if (!preg_match('#uapp[/\\\\]lib[/\\\\](DBX|Query)[a-z]*.php#i', ArrayUtils::getValue($dx, 'file', '$?'))) {
                    $depth = $i + 1;
                    break;
                }
            }
        }
        $dd = isset($da[$depth]) ? $da[$depth] : [];
        $where = (isset($dd['class']) ? $dd['class'] : '') . (isset($dd['type']) ? $dd['type'] : '') . (isset($dd['function']) ? $dd['function'] : '');

        \Codeception\Util\Debug::debug("\t" . $where . ': ' . $msg);
    }

    /**
     * @param $name
     * @param $value
     * @param string $color
     * @param string $params
     * @param int $depth
     * @param string $tags
     * @param null $mt
     *
     * @throws ConfigurationException
     * @throws InternalException
     */
    static function tracex($name, $value, $color = '', $params = '', $depth = 2, $tags = 'app', $mt = null) {
        if (is_null($depth)) $depth = 2;
        if (!is_string($name)) $name = Util::objtostr($name);
        $value = Util::objtostr($value);
        if ($tags == 'sql' && strpos($value, 'information_schema.columns')) $tags = 'sql uapp';
        if ($mt) $mtx = $mt > 2.0 ? floor($mt * 10) / 10 . ' s ' : floor($mt * 1000.0) . ' ms '; else $mtx = '';
        if (php_sapi_name() == "cli") {
            if ($mt) $mtx = Ansi::color($mtx, 'yellow', null, false);
            $msg = $mtx . Ansi::color($name, 'green', null, false) . "\t" . Ansi::color($value, Ansi::htmltoansi($color), null, false);

        } else {
            if ($mt) $mtx = '<i>' . $mtx . '</i> ';
            $msg = $mtx . '<b style="color=#090">' . $name . '</b>=<b style="color:' . $color . '">' . $value . '</b>';
        }
        self::trace($msg, $params, $depth, $tags);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @param $name
     * @param $xmldoc
     * @param string $tags
     *
     * @throws ConfigurationException
     * @throws InternalException
     */
    static function trace_xml($name, $xmldoc, $tags = 'app') {
        if (!self::$trace) return;
        if (is_null($xmldoc)) {
            self::tracex($name, null);
            return;
        }

        if (is_string($xmldoc)) {
            $doc = new DOMDocument();
            if (!$doc->loadXML($xmldoc)) {
                self::tracex($name, 'Invalid XML:' . $xmldoc, '#E00');
                return;
            }
            $xmldoc = $doc;
        }

        $result = $xmldoc->saveXML();
        if ($tags == 'output') {
            self::trace('Output:');
        }
        $txt = $name . '=<div class="dx">' . $result . '</div>';
        self::trace($txt, '', 2, $tags . ' xml');
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @throws ConfigurationException
     * @throws InternalException
     */
    static function trace_end() {
        self::trace('Trace end.');
    }

    static function init($enabled) {
    }

    /** @noinspection PhpMethodNamingConventionInspection
     * @param $d
     *
     * @return string
     */
    static function debug_content(/** @noinspection PhpUnusedParameterInspection */
        $d) {
        return '';
    }
}

// Aliasing class to Debug without multiple class definition warning :-)
if (!class_exists('Debug')) eval("class Debug extends TestDebug {}");
