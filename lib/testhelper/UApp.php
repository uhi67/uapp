<?php

namespace Helper;

use ArrayUtils;
use Codeception\Configuration;
use Codeception\Exception\ExternalUrlException;
use Codeception\Exception\ModuleConfigException;
use Codeception\Exception\ModuleException;
use Codeception\Lib\Framework;
use Codeception\TestInterface;
use Codeception\Util\Debug;
use ConfigurationException;
use Exception;
use InternalException;
use Loader;
use Model;
use ModelException;
use QueryException;
use ReflectionException;
use RuntimeException;
use Symfony\Component\DomCrawler\Crawler;
use UAppException;
use UAppLoader;

require_once 'UAppConnector.php';

/**
 * # Test helper for UApp framework
 *
 * ### Using:
 *
 * 1. Create a file named 'UApp.php' in your project's tests/_support/Helper folder referring to this:
 *
 * `include dirname(dirname(dirname(__DIR__))).'/uapp/lib/testhelper/UApp.php';`
 *
 * 2. Include in your `...suite.yml` as:
 *
 * ```
 * modules:
 *   enabled:
 *     - \Helper\UApp:
 *           configFile: 'config/config.php'
 *           loader: 'full' # optional, default is core only
 *           sapi: 'cli' for unit, 'apache' for functional
 * ```
 *
 * 3. Alternatively, you may use a separate test config in the above yml.
 *
 * ### Limitations
 * - `amLoggedIn()` assertion does not work yet.
 *
 * @property UAppConnector $client
 */
class UApp extends Framework {
    protected $requiredFields = ['configFile'];
    protected $config = ['loader' => 'core', 'sapi' => 'apache']; // optional parameters and defaults of module

    /** @var \UApp $app -- the application instance started by client */
    public $app;
    /** @var string $configfile -- path of your configfile */
    protected $configfile;
    /** @var array $uappconfig -- config of your UApp application */
    protected $uappconfig;
    /** @var array $server -- backup of $_SEVER variables for the test */
    private $server;


    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @throws ModuleConfigException
     * @throws ModuleException
     */
    public function _initialize() {
        $this->server = $_SERVER;
        $this->configfile = Configuration::projectDir() . $this->config['configFile'];
        Debug::debug('Configfile:' . $this->configfile);
        if (!is_file($this->configfile)) {
            throw new ModuleConfigException(
                __CLASS__,
                "The application config file does not exist: " . $this->configfile
            );
        }
        $this->uappconfig = require($this->configfile);
        if (!is_array($this->uappconfig)) throw new ModuleException(__CLASS__, "The UApp config is invalid: `$this->configfile` (may be return is missing)");
        $this->uappconfig['testsuite'] = $this->config;
        $GLOBALS['config'] = $this->uappconfig;

        $uapppath = $this->uappconfig['uapp']['path'];
        if (!is_dir($uapppath)) throw new ModuleException(__CLASS__, "The UApp path specified in config does not exists: `$uapppath` in " . print_r($this->uappconfig, true));
        if ($this->config['loader'] == 'full') {
            Debug::debug('registering full loader');
            include_once $uapppath . '/lib/Loader.php';
            Loader::registerAutoload($this->uappconfig);
        } else {
            Debug::debug('registering core loader');
            include_once $uapppath . '/lib/UAppLoader.php';
            UAppLoader::registerAutoload($this->uappconfig);
        }
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Minden teszt előtt
     *
     * @param TestInterface $test
     *
     * @throws ConfigurationException
     * @throws Exception
     * @throws InternalException
     */
    public function _before(TestInterface $test) {
        Debug::debug('_before');
        $this->client = new UAppConnector();
        $this->client->uappconfig = $this->uappconfig;
        $this->client->sapi = $this->config['sapi'];
        $this->app = $this->client->getApplication();
        if (!$this->app) {
            Debug::debug('No application instance created.');
        }

        $samlconfig = ArrayUtils::getValue($this->uappconfig, 'saml');
        if ($samlconfig && (!isset($samlconfig['disabled']) || !$samlconfig['disabled'])) {
            if ($samlconfig['dir']) {
                require_once($samlconfig['dir'] . '/lib/_autoload.php');
            } else {
                // No explicit simplesamlphp autoloader is needed
            }
            Debug::debug('Registering SimpleSaml: ' . $samlconfig['dir'] . '/lib/_autoload.php');
        }
        Debug::debug('_before done.');
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @param TestInterface $test
     * @throws ConfigurationException
     * @throws InternalException
     * @throws ReflectionException
     */
    public function _after(TestInterface $test) {
        Debug::debug('_after');
        $_SESSION = [];
        $_FILES = [];
        $_GET = [];
        $_POST = [];
        $_COOKIE = [];
        $_REQUEST = [];

        if ($this->app) {
            if ($this->app->rights) $this->app->rights->save();
            Debug::debug('Destroying application');
            \UApp::destroyInstance();
            $this->client->destroyApplication();
        } else {
            Debug::debug('Application instance not found');
        }

        parent::_after($test);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @param array $settings
     *
     * @throws Exception
     */
    public function _beforeSuite($settings = []) {
        Debug::debug('_beforeSuite');
    }


    /** @noinspection PhpMethodNamingConventionInspection */

    public function _afterSuite() {
        parent::_afterSuite();
        Debug::debug('_afterSuite');
        $_SERVER = $this->server;
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @param TestInterface $test
     * @param $fail
     *
     */
    public function _failed(TestInterface $test, $fail) {
        Debug::debug('_failed');
//		$logDir = Configuration::outputDir(); // prepare log dir
        parent::_failed($test, $fail);
    }


    /** @noinspection PhpMethodNamingConventionInspection */

    public function _cleanup() {
        Debug::debug('_cleanup');
    }

    /**
     * To support to use the behavior of urlManager component
     * for the methods like this: amOnPage(), sendAjaxRequest() and etc.
     *
     * @param $method
     * @param $uri
     * @param array $parameters
     * @param array $files
     * @param array $server
     * @param null $content
     * @param bool $changeHistory
     *
     * @return Crawler
     * @throws ExternalUrlException
     * @throws ConfigurationException
     * @throws Exception
     * @throws InternalException
     */
    protected function clientRequest($method, $uri, array $parameters = [], array $files = [], array $server = [], $content = null, $changeHistory = true) {
        if (is_array($uri)) {
            $uri = \UApp::createUrl($uri);
        }
        return parent::clientRequest($method, $uri, $parameters, $files, $server, $content, $changeHistory);
    }

//	public function amLoggedInAs($userid) {
//        $this->fail("'amLoggedInAs' must be overridden by an application level login helper");
//		$_SESSION['user_id'] = $userid; // Must be overridden by an application level login helper
//	}

    /**
     * Inserts record into the database.
     *
     * ``` php
     * <?php
     * $user_id = $I->haveRecord('User', array('name' => 'Davert'));
     * ?>
     * ```
     *
     * @param string $model - modelname
     * @param array $attributes
     *
     * @return boolean
     * @throws ModelException
     * @throws QueryException
     * @throws UAppException
     * @throws Exception
     */
    public function haveRecord($model, $attributes = []) {
        if (!is_a($model, 'Model')) throw new Exception('Invalid modelname ' . $model);
        $record = new $model($attributes);
        return $record->save();
    }

    /**
     * Checks that record exists in database.
     *
     * ``` php
     * $I->seeRecord('User', array('name' => 'davert'));
     * ```
     *
     * @param $model
     * @param array $attributes
     */
    public function seeRecord($model, $attributes = []) {
        $record = $this->findRecord($model, $attributes);
        if (!$record) {
            $this->fail("Couldn't find $model with " . json_encode($attributes));
        }
        $this->debugSection($model, json_encode($record));
    }

    /**
     * Checks that record does not exist in database.
     *
     * ``` php
     * $I->dontSeeRecord('app\models\User', array('name' => 'davert'));
     * ```
     *
     * @param $model
     * @param array $attributes
     * @part orm
     */
    public function dontSeeRecord($model, $attributes = []) {
        $record = $this->findRecord($model, $attributes);
        $this->debugSection($model, json_encode($record));
        if ($record) {
            $this->fail("Unexpectedly managed to find $model with " . json_encode($attributes));
        }
    }

    /**
     * Asserts that there's not an error message on the page
     * @return void
     */
    public function dontSeeError() {
        $selector = 'div.message.error';
        $nodes = $this->match($selector);
        if ($nodes->count()) {
            $message = $nodes->getNode(0)->nodeValue;
            $this->fail("An error message is displayed with message '$message'");
        }
    }

    /**
     * Asserts that there is an error message with the given text on the page
     * @return void
     */
    public function seeError($text) {
        $selector = 'div.message.error';
        $nodes = $this->match($selector);
        $message = '';
        for ($i = 0; $i < $nodes->count(); $i++) {
            $message = $nodes->getNode($i)->nodeValue;
            if ($message == $text) return;
        }
        $this->fail("The expected error message with message '$text' is not displayed on the page. Last message is: '$message'");
    }

    /**
     * Retrieves record from database
     *
     * ``` php
     * $category = $I->grabRecord('app\models\User', array('name' => 'davert'));
     * ```
     *
     * @param $model
     * @param array $attributes
     * @return mixed
     * @part orm
     */
    public function grabRecord($model, $attributes = []) {
        return $this->findRecord($model, $attributes);
    }

    protected function findRecord($model, $attributes = []) {
        $this->getModelRecord($model);
        return call_user_func([$model, 'first'], $attributes);
    }

    protected function getModelRecord($model) {
        if (!class_exists($model)) {
            throw new RuntimeException("Model $model does not exist");
        }
        $record = new $model;
        if (!$record instanceof Model) {
            throw new RuntimeException("Model $model is not a Model");
        }
        return $record;
    }

}
