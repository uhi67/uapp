<?php

namespace Helper;

use ArrayUtils;
use Codeception\Lib\Connector\Shared\PhpSuperGlobalsConverter;
use Codeception\Util\Debug;
use Codeception\Util\Debug as CCDebug;
use ConfigurationException;
use DBX;
use Exception;
use InternalException;
use ReflectionException;
use Symfony\Component\BrowserKit\AbstractBrowser;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\BrowserKit\Response;
use UAppException;
use Util;

/** @noinspection PhpIncludeInspection -- hidden dir is not detected -- azért kell betölteni, mert a simplesaml tartalmaz egy azonos nevűt, ami nem kompatibilis ezzel. */
#include 'C:\Users\uhi\AppData\Roaming\Composer\vendor\codeception\codeception\src\Codeception\Lib\Connector\Shared\PhpSuperGlobalsConverter.php';
/** az is megoldás, hogy: simplesamlphp$ composer update --no-dev */

/**
 * Class UAppConnector
 *
 * Using: see UApp.php
 *
 * @package Helper
 */
class UAppConnector extends AbstractBrowser {
    use PhpSuperGlobalsConverter;

    /** @var array the config array of UApp application */
    public $uappconfig;

    /** @var string $sapi -- Framework module sets to 'cli' for unit suite based on yml */
    public $sapi;

    public $defaultServerVars = [];

    /** @var array */
    public $headers;
    public $statusCode;

    /** @var \UApp */
    private $app;

    /** @var DBX */
    public static $db;

    /**
     * @return \UApp
     * @throws ConfigurationException
     * @throws Exception
     * @throws InternalException
     */
    public function getApplication() {
        if (!isset($this->app)) {
            $this->startApp($this->sapi);
        }
        return $this->app;
    }

    /**
     * @throws ReflectionException
     * @throws InternalException
     * @throws ConfigurationException
     */
    public function resetApplication() {
        $this->app->destroyInstance();
        $this->app = null;
    }

    /**
     * @param string $sapi
     *
     * @throws ConfigurationException
     * @throws InternalException
     * @throws UAppException
     * @throws ReflectionException
     */
    public function startApp($sapi = 'apache') {
        CCDebug::debug('Connector startApp');
        $t = get_resource_type(static::$db->connection);
        if (static::$db && $t != 'pgsql link') throw new UAppException('invalid connection resource: ' . $t);
        if ($this->app) {
            CCDebug::debug('Using existing App');
            $this->app->init($this->uappconfig, static::$db, $sapi);
            return;
        }
        $this->app = \UApp::hasInstance();
        if ($this->app) {
            CCDebug::debug('Refurbishing old UApp');
            $this->app->init($this->uappconfig, static::$db, $sapi);
            return;
        }

        CCDebug::debug('Creating new UApp');
        $this->app = new \UApp($this->uappconfig, static::$db, $sapi);
        CCDebug::debug('UAppConnector::startApp done.');
    }

    public function destroyApplication() {
        CCDebug::debug('destroyApplication');
        if ($this->app) $this->app->session = null;
    }

    public function resetPersistentVars() {
        static::$db = null;
        // TODO: reset uploaded file
    }

    /**
     *
     * @param Request $request
     *
     * @return Response
     * @throws ConfigurationException
     * @throws Exception
     * @throws InternalException
     */
    public function doRequest($request) {
        CCDebug::debug('doRequest...');
        CCDebug::debug('doRequest ' . Util::objtostr($request));
        $_COOKIE = $request->getCookies();
        $_SERVER = $request->getServer();
        $this->restoreServerVars();
        $_FILES = $this->remapFiles($request->getFiles());
        $_REQUEST = $this->remapRequestParameters($request->getParameters());
        $_POST = $_GET = [];

        if (strtoupper($request->getMethod()) === 'GET') {
            $_GET = $_REQUEST;
        } else {
            $_POST = $_REQUEST;
        }

        $uri = $request->getUri();

        $pathString = parse_url($uri, PHP_URL_PATH);
        $queryString = parse_url($uri, PHP_URL_QUERY);
        $_SERVER['REQUEST_URI'] = $queryString === null ? $pathString : $pathString . '?' . $queryString;
        $_SERVER['REQUEST_METHOD'] = strtoupper($request->getMethod());

        parse_str($queryString, $params);
        foreach ($params as $k => $v) {
            $_GET[$k] = $v;
        }

        $this->headers = [];
        $this->statusCode = null;

        CCDebug::debug('start');

        ob_start();
        // __
//        $uapp_path = $this->uappconfig['uapp']['path'];
        $apppath = ArrayUtils::getValue($this->uappconfig, 'apppath');
        $pages = ArrayUtils::getValue($this->uappconfig, 'pages', $apppath . '/pages');

        // parsing Url-path
        $path = Util::getPath();
        #$uri = \Util::getUri();
        $page = array_shift($path);
        $script = ArrayUtils::getValue($_SERVER, 'SCRIPT_NAME');
        if ('/' . $page == $script) $page = array_shift($path);
        if ('/' . $page == $script) $page = array_shift($path);

        if ($page == '') $page = isset($this->uappconfig['default']) ? $this->uappconfig['default'] : 'start';
        header("HTTP/1.1 200 OK");
        header("X-Powered-By: UApp");

        $pageclass = Util::camelize($page, true) . 'Page';
        CCDebug::debug('Pageclass: ' . $pageclass);
        if (file_exists($pages . '/' . $pageclass . '.php')) {
            header("X-Uapp-Source: 1; $page");
            if (!empty($_REQUEST['debug'])) {
                return \Debug::callback();
            }
            CCDebug::debug('getApplication:');
            $app = $this->getApplication();
            CCDebug::debug('Run:');
            try {
                $app->run('\\' . $pageclass, $path);
            } catch (Exception $e) {
                CCDebug::debug('UAppConnector catched Exception: ' . $e->getMessage());
            }
            CCDebug::debug('Run.');
        }
        // __
        $content = ob_get_clean();

        $this->statusCode = 200;
        $this->app->destroyInstance();

        // catch "location" header and display it in debug, otherwise it would be handled
        // by symfony browser-kit and not displayed.
        if (isset($this->headers['location'])) {
            Debug::debug("[Headers] " . json_encode($this->headers));
        }

        CCDebug::debug('Headers:');
        CCDebug::debug($this->headers);
        CCDebug::debug('Content:');
        CCDebug::debug($content);
        return new Response($content, $this->statusCode, $this->headers);
    }

    public function restoreServerVars() {
        $this->server = $this->defaultServerVars;
        foreach ($this->server as $key => $value) {
            $_SERVER[$key] = $value;
        }
    }

}
