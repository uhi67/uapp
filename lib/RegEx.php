<?php

/**
 * Class RegEx
 *
 * Represents a regular expression value for database
 *
 * Purpose: insert into database without further escaping of \-s
 *
 * Usage:
 *
 * $model->value = new Regex($pattern);
 * $model->save();
 *
 */
class RegEx extends Component {
    /** @var string $pattern */
    public $pattern;

    /**
     * RegEx constructor.
     *
     * @param string|array $pattern -- pattern string or config array
     * @param array $config
     *
     * @throws InternalException
     * @throws Exception
     */
    public function __construct($pattern, $config = []) {
        if (is_array($pattern) && func_num_args() == 1) $config = $pattern;
        else if (is_string($pattern)) $config['pattern'] = $pattern;
        else throw new Exception('Invalid pattern');
        parent::__construct($config);
    }

    public function __toString() {
        return $this->pattern;
    }
}
