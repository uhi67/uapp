<?php
/**
 * class Utf8
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2013
 * @license MIT
 */

/**
 * Utf8 encoding/decoding with ISO-8859-2 support
 */
class Utf8 {
    static $pattern = "á|é|í|ó|ö|ő|ú|ü|ű|Á|É|Í|Ó|Ö|Ő|Ú|Ü|Ű|ć|Ć|č|Č|ç|Ç|Ł|ł|Ĺ|ĺ|ń|Ń|ť|Ť|ţ|Ţ|Ż|ż|ž|Ž|";

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * @param $text
     * @return mixed|null|string|string[]
     *
     * @deprecated use mb_strtoupper($text, 'UTF-8')
     */
    static function upper($text) {
        return mb_strtoupper($text, 'UTF-8');
    }

    /**
     * @param $x
     * @return mixed|null|string|string[]
     *
     * @deprecated use mb_strtolower($text, 'UTF-8')
     */
    static function lower($text) {
        return mb_strtolower($text, 'UTF-8');
    }

    /** @noinspection PhpMethodNamingConventionInspection */
    /**
     * Encodes ISO-8859-2 to UTF8
     *
     * @param string $text -- ISO-8859-2 text
     * @return string -- UTF-8 text
     *
     * @deprecated use iconv('ISO-8859-2', 'UTF-8', $text)
     */
    static function encode_2($text) {
        return iconv('ISO-8859-2', 'UTF-8', $text);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Decodes UTF8 to ISO-8859-2
     *
     * @param string $text -- UTF-8 text
     * @return string -- ISO-8859-2 text
     *
     * @deprecated use iconv('UTF-8', 'ISO-8859-2', $text)
     */
    static function decode_2($text) {
        return iconv('UTF-8', 'ISO-8859-2', $text);
    }

    /**
     * Decodes UTF8 to ISO-8859-2 if contains UTF8-encoded ISO-8859-2 chars
     *
     * @param string $text -- UTF-8 text
     * @return string -- ISO-8859-2 text
     */
    static function decodeif($text) {
        if (preg_match("/" . self::$pattern . "/", $text)) $text = iconv('UTF-8', 'ISO-8859-2', $text);
        return $text;
    }

    /**
     * Encodes ISO-8859-2 to UTF8 if contains extended ISO-8859-2 chars
     *
     * @param string $text -- ISO-8859-2 text
     * @return string -- UTF-8 text
     */
    static function encodeif($text) {
        if (preg_match("/" . self::decode_2(self::$pattern) . "/", $text)) $text = iconv('ISO-8859-2', 'UTF-8', $text);
        return $text;
    }

    /**
     * Test a text if contains east european UTF8 characters
     * @param string $szoveg -- test to test
     *
     * @return false|int -- 1 if contains, 0 if not, false on error.
     */
    static function isutf8($szoveg) {
        return preg_match("/" . self::$pattern . "/", $szoveg);
    }

    /** @noinspection PhpMethodNamingConventionInspection */

    /**
     * Converts ISO-8859-1/2 to UTF8 preserving hungarian and italian characters
     *
     * @param $szoveg
     *
     * @return array|mixed|string
     */
    static function encode_hi($szoveg) {
        if (is_array($szoveg)) {
            $result = [];
            foreach ($szoveg as $item) $result[] = self::encodeif($item);
            return $result;
        }
        if ($x = strstr($szoveg, 'xxx')) {
            $szoveg .= '.' . dechex(ord(substr($x, 3, 1))) . '.' . dechex(ord(substr($x, 4, 1))) . '.' . dechex(ord(substr($x, 5, 1))) . '.' . dechex(ord(substr($x, 6, 1)));
        }
        $szoveg = str_replace('~', "*~", $szoveg);
        $szoveg = str_replace('ő', 'o~', $szoveg);
        $szoveg = str_replace('ű', 'u~', $szoveg);
        $szoveg = str_replace('Ő', 'O~', $szoveg);
        $szoveg = str_replace('Ű', 'U~', $szoveg);
        $szoveg = utf8_encode($szoveg);
        $szoveg = str_replace('o~', "\xC5\x91", $szoveg);
        $szoveg = str_replace('u~', "\xC5\xB1", $szoveg);
        $szoveg = str_replace('O~', "\xC5\x90", $szoveg);
        $szoveg = str_replace('U~', "\xC5\xB0", $szoveg);
        $szoveg = str_replace('*~', "~", $szoveg);
        return $szoveg;
    }

    /** @noinspection PhpMethodNamingConventionInspection
     * @param string $szoveg
     *
     * @return string
     *
     * @deprecated use iconv('ISO-8859-2', 'UTF-8', $text);
     */
    static function encode_ce($text) {
        return iconv('ISO-8859-2', 'UTF-8', $text);
    }
}
