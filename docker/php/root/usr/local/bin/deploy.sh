# Clone or update application from VCS
cd /var/www/app
if [ "$BRANCH" = "" ]; then
    echo "BRANCH is not defined, using 'master'"
    BRANCH="master"
fi
if [ -d ".git" ]; then
    echo "Updating '$BRANCH' from VCS '$VCS'"
    sudo -u www-data git checkout -- . # destroy local changes
    sudo -u www-data git checkout $BRANCH
    sudo -u www-data git pull
    git describe --tags --abbrev=0 > version
    chown www-data:www-data version
    rm -rf web/assets/*
fi
if [ ! -d ".git" ] && [ ! -f "/var/www/app/yii" ] && [ "$VCS" != "" ]; then
    echo "Removing existing source code"
    rm -rf /var/www/app/*
    rm -rf /var/www/app/.*
    echo "Cloning '$BRANCH' from VCS '$VCS'"
    sudo -u www-data git clone -b $BRANCH $VCS .
    git describe --tags --abbrev=0 > version
    chown www-data:www-data version
fi
if [ ! -d ".git" ] && [ ! -f "/var/www/app/yii" ] && [ "$VCS" = "" ]; then
    echo "Source is missing, please specify VCS to clone from"
fi
