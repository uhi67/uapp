cat <<'MSG'

|888 |888     , \8b
|888 |888    /8b \8b    |88 |8e  |88 |8e
|888 |888   /888b \8b   |88 |88b |88 |88b
|888 |888  /888888888b  |88 |88P |88 |88P
'Y88 |8P' /8888888b \8b |88 |8"  |88 |8"
                        |88      |88
                        |88      |88

MSG

echo "PHP version: ${PHP_VERSION}"

if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion.d/yii ]; then
    . /etc/bash_completion.d/yii
  fi
fi
