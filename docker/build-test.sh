#!/bin/bash
# Builds the test stack, for unit testing.
# Including vendor libraries with dev option, test database.
# Notice: for production, use UApp as composer library.

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd "$SCRIPT_DIR"/.. || echo "Invalid directory '$SCRIPT_DIR'" && return

composer install

git describe --tags --abbrev=0 > version

docker/docker compose -f docker/docker-compose.yml up

