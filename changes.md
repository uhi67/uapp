UApp release history
====================

## v2.0 (2024-09-25)

- Converted to composer project
- logging login/logout
- Query Builder: array operator added, subqueries, [] and [:] operators
- Query::addFields(), updateFields() added
- removed some deprecated code parts (inputfrom module)
- select2 support; select2 + selectfrom combo
- UList enhancements, e.g. data-attributes to header fields
- UApp class override
- UXPage::registerJs() added
- many bugfixes

## v1.17 (2024-04-23)

- Added "FROM" part to "UPDATE" queries in SQL builder
- php8 compatibility fixes
- fixed mysql encoding
- other minor fixes

## v1.16.1 (2024-02-18)

- logfile dir creation & exception
- Debug::p enhancement
- vendor require fix

## v1.16 (2024-01-21)

- Form: field 'order' parameter added, fieldOptions
- Form: find model nodes from one level up
- Basemodel::hasClassAttribute() added
- Query: $datetimepattern, filterDateRange(), filterDateTimeRange(), filterIntegerRange(), filterRegEx(), filterRef()
  added
- Inet::size property added
- Component::addElement()
- DBX: IS xxx operators, CAST operator added
- CLI: default default action with method comments
- several bugfixes

## v1.15 (2023-08-22)

- testhelper: simplesamlphp vendor version support
- Form: accept array-type error
- form.xsl: class parameter, mode="input" added
- 80-redirect removed
- docker init fixes
- codecept unit test fixes
- code reformat (spaces)

## v1.14.1 (2022-12-05)

- sendMail accepts 'mailto:' prefix
- sendMailMultiple boundary fix
- codecept fixes

## v1.14 (2022-12-01)

- UApp::sendMailMultiple(9 fixes (MIME rfc2046 conformity)
- Util::substitute() handles non-string

## v1.13 (2022-11-08)

- using version file (version.php should be deleted)
- supress timezone warning
- multipart html email
- Exception info fix

## v1.12 (2022-10-19)

- uniqueifnullValidate, uniqueValidate SQL error fixed
- UMessage js dialog added
- UXMLElement::addNodeList structure check
- phpdoc fixes
- unit test fixes

## v1.11 (2022-08-24)

- Inet: constructor can receive mask;
- Inet: tostring optional short parameter;
- Inet: add(), next(), prev() methods;

## v1.10.2 (2022-07-25)

- macaddrValidate: trim empty values

## v1.10.1 (2022-01-04)

- SelectFrom bug: wrong placement of the client validation classes

## v1.10 (2021-11-09)

- DBX: escape_literal \\ bug (duplication)
- Else branch in CondValidate() added
- SelectFrom fields: display link after input if exists
- UList: column definition may be 'Model:field'
- An XSL view now can be in a subdirectory
- Query builder bugs (integer value; subquery alias)
- Query builder: UNION, INTERSECT, EXCEPT combinations now available
- Util::toHtml() helper
- Enhanced display of exceptions

## v1.9.1 (2021-07-02)

- nolog check
- checksedForm patterns: dnsname allows *

## v1.9 (2021-03-01)

- minor bugs
- codeception issues
- Query::count() added

## v1.8 (2020-04-22)

- BaseModel: firstError property added
- Component: class check in config (if class option is set)
- Modules: custom 'class' property is renamed, see ***Form** and **TabControl**
- Flash messages: ~ postfix also indicates markdown format (see UAppPage::addMessages())

## v1.7.5 (2020-01-29)

- New feature: CreateCommand added (auto-generating Models from database)
- DBX: referrers, foreign_keys: retrieves information from database (implemented in PgSQL only)
- UList: search focus bug (Chrome bug)
- SelectFrom: input select hiba (bad event name)
- Bootstrap module init

## v1.7.4 (2020-01-23)

- UXMLDoc::createDocument(), canonizePrefixes() added, formatFragment() modified

## v1.7.3 (2020-01-16)

- BaseModel::patternValidate now passes any empty value (use mandatory if non-empty value required)
- checkedForm module: skip inputs in '.hidden' blocks
- Form module: form layout change at 'descr' row
- Hint module javascript bug
- Selectfrom module javascript bug

## v1.7.2 (2020-01-13)

- Menu item disabled bug
- Model: getter: using any registered attribute with .-ed syntax, createNode bug
- BaseModel: createNodes bug
- Util::fetchValue: accepts null array
- SQL builder: Order bug (no closing par check needed)
- UList: tooltip-ids; empty ord in columns bug

## v1.7.1 (2020-01-10)

- SelectFrom bug
- Hint module changes, new params
- Showdown: convert icons and classes

## v1.7 (2020-01-07)

- UList: column nodes in XML; column limit with colspan; order by new columns
- UXMLElement::addAttribute() adds array of components as subnodes with fields (affects BaseModel::createNode)
- BaseModel: 'extra' fields reference resolving; setAttribute() now returns itself
- SelectFrom: returning values by classnames and data-* tags; width limit (600px) removed
- Util::bitmapValues() added; orgPathToDN (AAI path to LDAP DN format) added
- DBX SQL builder bug: tranlsating associative NULL correctly
- Form: skip null attributes; data-orig added; attribute/before-row, after-row callbacks; field error markdown support
- Tabcontrol: active tab default color
- Showdown: Changed initialization, using configured relative path

## v1.6.1 (2019-12-18)

- Custom initialization added with simplesamlphp certificate generating sample (not tested)
- Hash option in the file cache
- QueryDataSource: GROUP BY issue
- Bytea literals in PGSQL
- UXMLElement: xml property added
- Model::getreferer(): filter condition added
- other bugfixes

## v1.6 (2019-11-30)

- Many internal documentation became more elaborate
- Lot of minor bugs
- Now error handling catches Throwable (php 7 compatibility)

+ Docker image builder added
+ Application template (not yet complete)

- Configurable without mod_rewrite
- DBX qury builder:
    + substring (SQL'92 standard) function is added as operator
    + DISTINC added

+ Cache: use control added

- Component:
    + publicProperties() added
    + toArray() added
- Query:
    + `DISTINCT` clause added
    + first(): modelClass parameter added
    + iterate() added (returns QueryIterator)

+ QueryIterator added (better for huge result sets)
+ Regex class added

- UApp:
    + using `timeZone` config parameter
    + serveFile() for faster asset serving

    - enhanced exception display
- UpdateDbCommand: splits SQL into separate commands at double \n

### Modules

- UList
    + formclass option added
    + attr-data option added

    - Enhanced header and filter fields (button type added)

## v1.5.8 (2019-09-23) (minor hotfix)

- Minor bugfixes (L10n, Model, BaseModel::createNode)
- UXMLElemen::getAllAttributes added
- BaseModel::bitarrayValidate added
- documentations

## v1.5.7 (2019-03-19) (minor hotfix)

- command updatedb/admit literal bug;
- UXMLElement: addAttribute trace on failure
- Form field link option phpdoc

## v1.5.6 (2018-12-12)

- Fixes in UXMLElement

+ Query::gropBy() method added
+ BaseModel::condValidate(), validateRules() added
  . Model::createNode(): attribute of null referenced field bugfix
+ Util::objToStr() uses toArray() on objects if exists

- Form enhancements

+ UList: empty, formclass attributes, list-first xsl-callback

- Menu bugfixes and enhancements
- Upgrades in Parser
  . SelectFrom: colour of mandatory; enhancements on generic use

## v1.5.5 (2018-11-15)

- Bugfix: UXMLElement::addContent: do not localize boolean attributes
- BaseModel::createNode: callable cannot be a string value (fixed)
- SelectFrom: simple field (without separate _name pair) js bug (fixed)

## v1.5.4 (2018-11-05)

- Util::SubEval() removed (was not in use)
- L10n, Cache, UXPAge minor fixes

+ UXMLElement: createNode(), addContent() methods added, and replaces many others

- BaseModel: createSubNode() moved to UXMLElement; minor bugs

+ Model::first(): nocache parameter added, minor issues

- Query: join alias fixed
- DBX expression: '' is empty
- DBXMY fixes

+ FineDiff class
+ UXMLDoc: formatFragment
+ UApp, UAppPage: generating syntax validation root xsl for PS inspection

- TabControl: minor bugfixes
- UList: minor bugfixes
  . Debug issues

+ codecept _support and tests

## v1.5.3 (2018-10-04)

* Utf8 mostly deprecated

+ Util::baseurl(), toNameID fix

* BaseModel: simple extra attributes, composit attributes, partial validation, attribute handling

+ StaticComponent
+ L10nFile

* L10n: textClass, db parameters
  . Debug issues

## v1.5.2 (2018-09-28)

+ DBX: desc() static method, buildOrderList() added; E' string literals

* BaseModel __get and __set changes: uses property getter/setter; using fields in createNode()

- UXPage: decision on render agent moved to UAppPage
  . Debug issues

+ TabControl: effective parent; varname . character bugfix
+ UList:
    + with and ord properties of column definition added,
    + order fix (accepts array),
    + row selection with hidden cb,

    * default item nodename from datasource class if specified
      . initialization with datasource fixed
+ Menu: BLOCK_XXX constants

## v1.5.1 (2018-09-19)

- Debug: Model properties and top frame bugfix
- Utf8: bugfix
- BaseModel: validation accepts attribute list to restrict to; attribute and validation issues, resetError()
- Util::toNameID() bugfix
- Util: date representation change
- Form: rule message issues
- Query::all() default returns arrays again
- BaseModel::if2Validate() added

## v1.5 (2018-09-13)

### Changes

- UXMLElement added
- Model refactorings:
    - getAttributeLabel(name) -> attributeLabel()
    - getType(name) -> attributeType()
    - getAttributes(names) -> namedAttributes()
    - getTypeLive(name) -> attributeTypeLive()
    - getForeignKey(name) -> foreignKey(name)
    - getAttributeHint() -> attributeHint()
- QueryDataSource added
- Query and querybiulder enhancements; :: EXIST operators
- Abstract and fake mailer
- Emptycache command
- UAppPage::json_response
- Redirect url encoding bug

- Menu:
    - options added
    - attributes added
    - ulAttributes added
- UList: dataSource, searchParam, modeOptions fields added
- UList: column definition by model's field, disabling of paging, client sorting
- Form: markdown support in field's popup help
- SelectFrom: triggers changed fields

### Bugs

- Destructor issues
- L10nBase: short Hungarian date pattern
- redirect bug
- BaseModel::betweenValidate bug,
- SelectFrom bugs
- Form xsl bugs, client validation issues

## v1.4.8 (2018-09-04)

- UAppPage: Ability of disabling logging on specific actions
- Codecept testhelper issues

## v1.4.7 (Hotfix: 2018-08-30)

- form.js bug

## v1.4.6 (2018-08-29)

### Changes

- Selectfrom: generating data-opt attribute into form field
- Menu: applying data-xxx options to attributes, attribute handling changed
- Select2 module added
- ArrayUtils::fetchValue() added, getValue() bugfix

### Bugfixes:

- Form: selectfrom pretype bugs
- minor css, debug bugs

## v1.4.5 (2018-08-08)

### Changes

- Query: default nodename is lowercase modelname
- SelectFrom module: select-from magic class
- BaseModel::$errorNodes property added
- Module::init(): dir parameter added
- UAppCommand::param(): default parameter added
- UAppPage: resetxsl=1 request parameter added

### Bugfixes:

- tableExists
- BDX: buildOrder, selectValue, escaped literal, single fieldname
- updatedb init
- UList module: empty defaults for callback
- Util::substitute: {MDY$var} format fix
- render: viewnotfound.xsl added, exception forwarding
- UXMLDoc: createFragment(), default locale

## v1.4.4 (2018-04-12)

- validation bugfixes
- BaseModel::MacAddrValidate(): trim added
- BaseModel::trimValidate(), lowercaseValidate() added
- BaseModel::emptyIsNull(): trim added
- Client side integer validation added
- SelectFrom module: pretype option added

## v1.4.3 (2018-04-09)

- UXMLDoc::addHypertextContent namespace parameter
- Dátum valid formátumok, Form dátum mező értéke
- boolean attributum generálása nem nemzetiesítve

## v1.4.2 (2018-03-22)

- Debug top panel bugfix
- DBX::literal() now converts arrays recursively
- UList: variable attribute to create hidden inputs
- UXPage: createUrl() creates absolute path
- Form module: showComments feature, comment-class (etc)
- Menu module: all menus converted to ul/li structure

## v1.4.1 (2018-03-14)

- DBXPG: table metadata: predictable order of fields
- Util::toRegExp() added
- Parser: Regex pattern issue fix

## v1.4

### New features

- SQL build moved to DBX. Sql will be built only before execution.
- Automatic mapping of database types to php types (PostgreSQL)
- Transaction support
- Partial codeception support. The framework module is in `lib/testhelper/` directory.
- Util::createDateTime()
- Model: conditional validator
- File upload support, FileUpload class, fileValidate
- createUrl functions
- Packed integer (PInt) supportłł
- Browser detection (BrowserHelper class)
- LRED template (without further support)

### Changes

- changes in _uapp loading. `App` class removed.
- Util::toNameID() default length is 64
- UList changes (constructor, columns, etc)
- Xlet, Inet, L10n, MacAddr changes
- UXMLDoc data type support

- Lot of bugfixes

## v1.3.3

- lot of minor fixes, docblocks

## v1.3.2

- Debug: show uploaded files
- Inet, Macaddr: __tostring() added
- DBX: select_all() fetches data by mapped type
- minor fixes
- new Loader

## v1.3.1

- Bugfixes, docs,
- BaseModel::setError is public
- DBX::paramValue: string used always as string even if contains number.
- Query: spaces reduced in generated SQL string

## v1.3

- ArrayUtils::map(), shiftDefault() added
- BaseModel validators fixed, translations, timezone fix, macValidate, inetValidate
- BaseModel, Model: getType() is now static, other fixes
- DBX DateTime format fix, default of default is now null, numeric parameters are not strings
- Form: custom client validators, rowclass, focused field, other fixes
- Form: field ids; validator message override, explicit used fields property
- Inet: fixes
- L10n changes (search for numeric id)
- Menu attributes
- MacAddr class
- Model::createSubNode() modified, getRelation() fix,
- Model::addConditionForeignkeys(), addOrdersForeignkeys(), getRefName() added, uniqueifnullValidate() validator added
- Model: always uses default connections if available (fix)
- Model-template added
- Query: interval, current_timestamp operators; non-associative arrays with integer indices only, simple boolean
  expression
- Rights: initCache() added
- SelectFrom fields and Select fields rule-class fixed, user-defined emptyvalue
- Session, Debug data directory creation fixes
- Showdown module added
- TabControl associative page indices, focus and other fixes, css
- UApp: Displays exception details
- UAppCommand and Updatedb command fixes
- UAppPage: content/@act added for clearer view structures
- UAppPage::createUrl() added, backtrace colors, doc translated
- UList attributes, headerfields parameter added, js fix
- UList: xsl uses default list parameters from XML
- version tracking: app version number now is in @app/version.php
- Xlet class for node-creating modules, used in Form, Menu, UList
- XSLT client render hack for jQuery & jQueryUI
- XSLT certified agent table, new rules for render mode, XSLT client gen fixed

## v1.2.5

- Strict static calls fix (Utf8)

## v1.2.4

- added Model::refreshRelated() to flush cached related models

## v1.2.3

- Module is a Component
- design is a Module
- Menu module continued (functions moved from UList and design)
- DBX::fieldType()
- Form module (not ready)
- bootstrap module (not used yet)

## v1.2.2

- UAppPage::log() jav
- Util::startsWith(), endWith()
- other minor bugfixes

## v1.2.1

- Menu module (separated from UList)
- Enhanced Model and Query classes
  -- Model::first(): auto join foreign keys if referenced
  -- Model::first(): cached, attributeTypes() cached
  -- Model::createNode() computed attributes
  -- Model::$friendlyErrors
  -- Query::$finalSQL
  -- Query: REGEXP operators
  -- Query: join type on explicit joins
- UAppCommands: params to actions
- ArrayUtils:_array_delete() added
- UApp.js: ConfirmDialog

## v1.2

- New data model added, see Model, Query
- UXMLDoc: enhanced namespace handling
- DBX: literal and name quoting; data types
- Flash message contents, positions
- Debug enhancements, tagging
- UList: auto-hide columns while stretch
- Fa module
- Commands
- Updatedb support
- Wiki added

## v1.1.0 - 2017.05.25

- New exceptions (based on UAppException)

## v1.0.3 - 2016.10.15

BaseException changes

SamlUser::

- checking authenticated state
- config/saml/uidfield parameter
- checking uidfield attribute

Session changes

## v1.0.2 - 2016.10.15

index.php

- fatal error

lib/DBXPG.php

- PG support check

UApp::

- checking XSLT support

## v1.0.1 - 2016.10.12

UApp: singleton pattern

UAppPage::

- automatic XSL based on pagename
- appdate, appversion values inserted into DOM under /data/head
- loadListP() with $search

Debug::

- disable() -- to revoke started trace
- trace_end() bugfix

UList::

- loadList() static version

## Before v1.0

Earlier versions was without VCS, no history available
