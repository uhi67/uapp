<?php
/**
 * class IndexCommand
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2017
 * @license MIT
 * @package commands
 */

/** @noinspection PhpUnused */

/**
 * Implements the default command for UApp application.
 *
 * Lists available commands in UApp and application commands dir.
 * Use phpDoc comments to describe the commands and actions. The first line must be a summary.
 * Use @params and @example tags for usage information.
 *
 * @example php uapp
 * @example php uapp [<command>[/<action>]] [parameters] [option=value]
 */
class IndexCommand extends UAppCommand {
    /**
     * Default action: List all commands and actions
     *
     * @return void
     * @throws ConfigurationException
     * @throws Exception
     */
    public function actDefault() {
        $name = $this->app->config('title', $this->app->config('appname', 'UApp'));
        $ver = $this->app->config('ver', '');
        echo Ansi::color($name . ' ' . $ver, 'green') . "\n";
        echo Ansi::color("Index of commands", 'green') . "\n";
        $apppath = $this->app->config('!apppath');
        $uapppath = dirname(__DIR__);
        $commands = [];

        // UApp commands
        $dir = $uapppath . '/commands';
        $dh = opendir($dir);
        if (!$dh) throw new Exception("Invalid dir $dir");
        while (($file = readdir($dh)) !== false) {
            if (filetype($dir . '/' . $file) == 'file' && preg_match('/^(\w+)Command.php/', $file, $m)) {
                $commands[$m[1]] = $dir . '/' . $file;
            }
        }
        closedir($dh);

        // User commands (overwrites UApp commands)
        $dir = $this->app->config('commands', $apppath . '/commands');
        if (file_exists($dir)) {
            $dh = opendir($dir);
            if (!$dh) throw new Exception("Invalid dir $dir");
            while (($file = readdir($dh)) !== false) {
                if (filetype($dir . '/' . $file) == 'file' && preg_match('/^(\w+)Command.php/', $file, $m)) {
                    $commands[$m[1]] = $dir . '/' . $file;
                }
            }
            closedir($dh);
        } else {
            echo Ansi::color("Application's commands directory does not exist.", 'red') . "\n";
        }

        foreach ($commands as $command => $file) {
            $descr = $file;
            echo Ansi::color($command, 'yellow') . "\t" . $descr . "\n";
            $methods = get_class_methods($command . 'Command');
            /** @var string[]|null $methods */
            /** @noinspection PhpConditionAlreadyCheckedInspection */
            if (is_array($methods)) foreach ($methods as $method) {
                if (preg_match('/^act([A-Z]\w+)/', $method, $m) && $m[1] != 'Default') {
                    $action = strtolower($m[1]);
                    echo Ansi::color("\t\t$action", 'brown') . "\n";
                }
            }
            else {
                echo Ansi::color("\t\tInvalid class", 'red') . "\n";
            }
        }
    }
}
