<?php /** @noinspection ALL */
/**
 * class UpdatedbCommand
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2017
 * @license MIT
 * @package commands
 */

/**
 * Maintains database migration state in `updatedb` table
 * Migration files are `db_xxx.sql` files in `sql` directory
 *
 * Using: php app.php updatedb action arguments
 *
 * Syntax of db_xxx files:
 * - standard SQL commands
 * - you may use `{$name}` params, they will be substituted from config/updatedb array.
 * - recommended to use `db_$creationdate_$purpose.sql` name convention for these files.
 *
 * Configure updatedb in config['updatedb']:
 * - database (default is application's database connection)
 * - initfile (default db_update_2017_0101_updatedb.sql, located in UApp/sql)
 * - tablename (default is 'updatedb')
 *
 * updatedb update action replaces all `{\$var}` to values configured in config['updatedb'].
 */
class UpdatedbCommand extends UAppCommand {
    private $initfile;
    private $updatetable;

    /**
     * ## UpdatedbCommand constructor.
     *
     * @param UApp $app
     *
     * @throws ConfigurationException
     * @throws DatabaseException
     * @throws InternalException
     */
    function __construct($app) {
        $this->initfile = $app->config('updatedb/initfile', 'db_update_2017_0101_updatedb.sql');
        Debug::tracex('initfile', $this->initfile);
        $this->updatetable = $app->config('updatedb/tablename', 'updatedb');
        if ($dbconfig = $app->config('updatedb/database')) {
            $app->db = $app->createDbConf($dbconfig);
        }
        if (!$app->db) throw new InternalException('No database connection defined');
        parent::__construct($app);
    }

    function initUser() {
    }

    function preConstruct() {
    }

    function loadData() {
    }

    /**
     * Displays update status
     *
     * @throws DatabaseException
     * @throws Exception
     * @throws InternalException
     */
    public function actStatus() {
        echo Ansi::color("updatedb status\n", 'green');
        if (!$this->db->tableExists($this->updatetable)) {
            echo Ansi::color("$this->updatetable table does not exist. Please run updatedb init", 'red'), "\n";
            exit(1);
        }

        $query = Query::createSelect([
            'fields' => ['name', 'applied'],
            'from' => $this->updatetable,
            'orders' => [['applied', DBX::ORDER_DESC]],
            'connection' => $this->db,
        ]);
        $last = $query->first(); //$this->db->select_assoc('select name, applied from '.$this->db->quoteIdentifier($this->updatetable).' order by applied desc limit 1');
        if ($last) {
            /** @var DateTime $applied */
            $applied = $last['applied'];
            $appliedtime = $applied->format('Y-m-d H:i');
            echo "Last updated: {$last['name']} $appliedtime\n";
        } else echo "Update database is empty yet.\n";

        // Determine new files
        $sqlpath = $this->app->config('!apppath') . '/sql';
        $dh = opendir($sqlpath);
        if (!$dh) throw new Exception("Invalid dir $sqlpath");
        $new = [];
        while (($file = readdir($dh)) !== false) {
            if (filetype($sqlpath . '/' . $file) == 'file' && preg_match('/^db_update_/', $file)) {
                $file = pathinfo($file, PATHINFO_FILENAME);

                /** @var DateTime $applied */
                $applied = Query::createSelect([
                    'fields' => ['applied'],
                    'from' => $this->updatetable,
                    'condition' => ['name' => '$1'],
                    'orders' => [['applied', DBX::ORDER_DESC]],
                    'params' => [$file],
                    'connection' => $this->db,
                ])->scalar();
                //$applied = $this->db->selectvalue('select applied from '.$this->db->quoteIdentifier($this->updatetable).' where name=$1', array($file));

                if (!$applied) $new[] = $file;
            }
        }
        closedir($dh);
        if (empty($new)) {
            echo Ansi::color("No new updates.", 'green'), "\n";
        } else {
            sort($new);
            echo Ansi::color(sprintf("There are %d new updates:", count($new)), 'yellow'), "\n";
            foreach ($new as $i => $n) {
                $max = 4;
                $more = count($new) - $max * 2;
                if ($i < $max) echo $n . "\n";
                else if ($i == $max && $i <= count($new) - $max - 1) {
                    if ($more > 1) echo sprintf(" ... (%d more) ...\n", $more);
                    else echo $n . "\n";
                } else if ($i > count($new) - $max - 1) echo $n . "\n";
            }
            echo "Use " . Ansi::color('updatedb update', 'light cyan') . " command to update.\n";
        }
    }

    /**
     * Registers given update(s) into database without actually applying them.
     *
     * @param array $params patterns
     * @return void
     * @throws ConfigurationException
     * @throws DatabaseException
     * @throws Exception
     * @throws InternalException
     */
    public function actAdmit($params) {
        $all = false;
        echo Ansi::color("updatedb admit\n", 'green');
        foreach ($params as $i => $pattern) {
            if ($pattern == '*') {
                $all = true;
                unset($params[$i]);
            }
        }

        // Determine new files mathcing patterns
        $sqlpath = $this->app->config('!apppath') . '/sql';
        $dh = opendir($sqlpath);
        if (!$dh) throw new Exception("Invalid dir $sqlpath");
        $new = [];
        while (($file = readdir($dh)) !== false) {
            if (filetype($sqlpath . '/' . $file) == 'file' && preg_match('/^db_update_/', $file)) {
                $file = pathinfo($file, PATHINFO_FILENAME);
                $applied = Query::createSelect([
                    'fields' => ['applied'],
                    'from' => $this->updatetable,
                    'condition' => ['name' => '$1'],
                    'params' => [$file],
                    'connection' => $this->db,
                ])->scalar();
                //$this->db->selectvalue('select applied from '.$this->db->quoteIdentifier($this->updatetable).' where name=$1', array($file));
                if (!$applied) {
                    // check pattern match
                    $m = $all;
                    #$re = !(@preg_match($pattern, null)===false);
                    foreach ($params as $i => $pattern) {
                        $re = !(@preg_match($pattern, null) === false);
                        $m = $m || ($re && preg_match($pattern, $file)) || (strpos($file, $pattern) !== false);
                    }
                    if ($m) $new[] = $file;
                }
            }
        }
        closedir($dh);

        // Execute admit
        if (empty($new)) {
            echo Ansi::color("No new matching updates.", 'green'), "\n";
        } else {
            sort($new);
            echo Ansi::color(sprintf("There are %d matching new updates:", count($new)), 'brown'), "\n";
            foreach ($new as $i => $n) {
                $r = Query::createInsert([
                    'modelname' => $this->updatetable,
                    'fields' => ['name', 'applied'],
                    'values' => [[$n, '(now())']],
                    'connection' => $this->db,
                ])->execute();
                //$r = $this->db->query('insert into '.$this->db->quoteIdentifier($this->updatetable).' (name, applied) values ($1, now())', array($n));
                if ($r) echo "Update " . $n . " has been admitted.\n";
                else echo "Admitting " . $n . " failed\n";
            }
        }
    }

    /**
     * This action revokes updates from registry database without undoing them.
     *
     * @return void
     * @throws DatabaseException
     * @throws InternalException
     * @throws ModelException
     * @throws QueryException
     * @throws UAppException
     */
    public function actRevoke($params) {
        $all = false;
        $connection = $this->db;
        echo Ansi::color("updatedb revoke\n", 'green');
        foreach ($params as $i => $pattern) {
            if ($pattern == '*') {
                $all = true;
                unset($params[$i]);
            }
        }

        // Determine registered files mathcing patterns
        if ($all) {
            Updatedb::deleteAll(['true']);
            echo Ansi::color("All updates has been revoked.\n", 'ligh purple');
            exit;
        }

        $condition = array_merge(['or'], array_map(function ($p) use ($connection) {
            return ['rlike', 'name', $connection->literal($p)];
        }, $params));
        $updates = Updatedb::all($condition);
        $n = count($updates);
        $c = 0;

        /** @var Updatedb $upd */
        foreach ($updates as $upd) {
            echo $upd->name . "\n";
            $upd->delete();
        }

        echo Ansi::color("$c/$n updates has been revoked.\n", 'light cyan');
    }

    /**
     * This action lists latest n applied updates from registry.
     * Optionally filtered by a pattern
     *
     * @param array $params patterns
     * @return void
     * @throws DatabaseException
     * @throws InternalException
     * @throws QueryException
     * @throws UAppException
     */
    public function actLast($params) {
        $n = array_shift($params);
        if (!$n || !is_numeric($n)) {
            $pattern = $n;
            $n = 12;
        } else $pattern = array_shift($params);

        $connection = $this->db;
        echo Ansi::color("updatedb last $n $pattern\n", 'green');

        $condition = $pattern ? ['rlike', 'name', $connection->literal($pattern)] : null;
        $query = Query::createSelect('Updatedb', null, null, $condition, [['applied', 'desc']], $n, null, null, $connection);
        $updates = $query->all();
        foreach ($updates as $upd) {
            echo $upd['name'] . "\n";
        }
    }

    /**
     * Update database to last state.
     * Applies all new updates and registers them into database.
     * Selective update is currently not available.
     * Substitutes {$name} parameters from config/updatedb
     *
     * @param array $params -- more command line parameters, later may be considered as filename patterns
     * @return void
     * @throws Exception
     * @throws InternalException
     * @throws UserException
     */
    public function actUpdate(/** @noinspection PhpUnusedParameterInspection */ $params) {
        echo Ansi::color("updatedb update\n", 'green');

        // Checking update table
        if (!$this->db->tableExists('updatedb')) {
            // TODO: include explicit init with overriddable confirmation
            echo Ansi::color("Updatedb table is not initialized. Please run first `updatedb init`", 'red'), "\n";
            return;
        }

        // Determine new files
        $sqlpath = $this->app->config('!apppath') . '/sql';
        $dh = opendir($sqlpath);
        if (!$dh) throw new Exception("Invalid dir $sqlpath");
        $new = [];
        while (($file = readdir($dh)) !== false) {
            if (filetype($sqlpath . '/' . $file) == 'file' && preg_match('/^db_update_/', $file)) {
                $name = pathinfo($file, PATHINFO_FILENAME);
                $applied = Query::createSelect([
                    'fields' => ['applied'],
                    'from' => $this->updatetable,
                    'condition' => ['name' => '$1'],
                    'params' => [$name],
                    'connection' => $this->db,
                ])->scalar();
                //$applied = $this->db->selectvalue('select applied from '.$this->db->quoteIdentifier($this->updatetable).' where name=$1', array($name));
                if (!$applied) $new[] = $file;
            }
        }
        closedir($dh);

        // Execute new updates
        if (empty($new)) {
            echo Ansi::color("Everything is up to date!", 'green'), "\n";
        } else {
            sort($new);
            echo Ansi::color(sprintf("There are %d new updates:", count($new)), 'yellow'), "\n";
            $n = 0;
            foreach ($new as $file) {
                $name = pathinfo($file, PATHINFO_FILENAME);
                $filename = $sqlpath . '/' . $file;
                try {
                    echo Ansi::color("Applying $name from $filename\n", 'light cyan');
                    $sql = file_get_contents($filename);
                    $params = $this->app->config('updatedb', []);
                    $sql = Util::substitute($sql, $params);
                    $sql = rtrim($sql);

                    // Split to individual SQL commands
                    $sqls = explode(";" . PHP_EOL . PHP_EOL, $sql);
                    $rs = true;
                    foreach ($sqls as $sql) {
                        echo Ansi::color("---- Executing SQL ----\n$sql\n\n", 'gray');
                        $rs = $this->db->query($sql);
                        if (!$rs) {
                            Debug::tracex('Error', error_get_last());
                            break;
                        }
                    }
                    if ($rs) {
                        echo Ansi::color("Update " . $name . " has been applied.", 'green') . "\n";
                        $data = pg_fetch_assoc($rs);
                        if ($data) Debug::trace(Util::objtostr($data));
                        $r = Query::createInsert([
                            'modelname' => $this->updatetable,
                            'fields' => ['name', 'applied'],
                            'values' => [[$name, new DateTime()]],
                            'connection' => $this->db
                        ])->execute();
                        //$r = $this->db->query('insert into '.$this->db->quoteIdentifier($this->updatetable).' (name, applied) values ($1, now())', array($name));
                        if (!$r) echo Ansi::color("Registering update " . $n . " failed.", 'red') . "\n";
                        else $n++;
                    } else echo Ansi::color("Applying " . $name . " failed", 'red') . "\n";
                }
                    // Executed only in PHP 7, will not match in PHP 5
                    /** @noinspection PhpElementIsNotAvailableInCurrentPhpVersionInspection */
                catch (Throwable $e) {
                    throw new UserException("Applying " . $name . " failed", null, null, 1, $e);
                }
                    // Executed only in PHP 5, not reached in PHP 7
                    /** @noinspection PhpRedundantCatchClauseInspection */
                    /** @noinspection PhpWrongCatchClausesOrderInspection */
                catch (Exception $e) {
                    throw new UserException("Applying " . $name . " failed", null, null, 1, $e);
                }
            }
            if ($n == 0) echo Ansi::color("No updates applied", 'yellow'), PHP_EOL;
            else echo Ansi::color("$n update(s) applied.", 'cyan'), PHP_EOL;
        }
    }

    /**
     * initializes database table storing applied database updates - force deletes existing
     *
     * @param array $params force
     * @throws DatabaseException
     * @throws InternalException
     * @throws NotFoundException
     * @throws UserException
     * @example php uapp updatedb force
     */
    public function actInit($params) {
        echo Ansi::color("updatedb init\n", 'green');
        $force = in_array('force', $params);
        if (!$this->db->tableExists($this->updatetable) || $force) {
            if ($this->db->tableExists($this->updatetable)) {
                $sql = "drop table " . $this->db->quoteIdentifier($this->updatetable);
                $rs = $this->db->query($sql);
                if (!$rs) {
                    echo "Deleting '" . $this->db->quoteIdentifier($this->updatetable) . "' table failed.\n";
                } else {
                    echo "'" . $this->db->quoteIdentifier($this->updatetable) . "' table deleted.\n";
                }
            }
            $sqlpath = dirname(__DIR__) . '/sql';
            $initfile = $sqlpath . '/' . $this->initfile;
            if (!file_exists($initfile)) throw new NotFoundException(['Init file (%s) not found', $initfile]);
            $sql = file_get_contents($initfile);
            $params = $this->app->config('updatedb', []);
            if (!isset($params['owner'])) throw new UserException('Please specify table owner in `config/updatedb/owner`.');
            if (!isset($params['user'])) throw new UserException('Please specify table user in `config/updatedb/user`.');
            if (!isset($params['tablename'])) $params['tablename'] = $this->updatetable;
            $sql = Util::substitute($sql, $params);
            $rs = $this->db->query($sql);
            if (!$rs) {
                echo "Creating '" . $this->db->quoteIdentifier($this->updatetable) . "' table failed.\n";
            } else {
                echo "'" . $this->db->quoteIdentifier($this->updatetable) . "' table created successfully.\n";
            }
        } else {
            echo "'" . $this->db->quoteIdentifier($this->updatetable) . "' table already exists.\n";
        }
    }
}

/**
 * Updatedb table model
 *
 * @author Peter Uherkovich
 * @copyright 2017
 *
 * @property int $id
 * @property string $name
 * @property DateTime $applied
 */
class Updatedb extends Model {

}
