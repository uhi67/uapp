<?php
/**
 * class EmptycacheCommand
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright (c) 2018
 * @license MIT
 * @package commands
 */

/** @noinspection PhpUnused */
/** @noinspection PhpClassNamingConventionInspection */

/**
 * CLI command to empty the xsl cache of the renderer
 */
class EmptycacheCommand extends UAppCommand {
    function initUser() {
    }

    function preConstruct() {
    }

    function loadData() {
    }

    /**
     * Default action: help
     *
     * @return void
     */
    public function actDefault() {
        echo Ansi::color("Emptycache command", 'green') . "\n";
        echo "Using:
- emptycache xsl [v=1|2]
";
    }

    /**
     * Empties the xsl cache directory.
     *
     * Options
     * - v|verbose: 0=quiet, 1(default)=summary, 2=detailed
     *
     * @throws Exception
     */
    public function actXsl() {
        $v = (int)($this->param('verbose') ?? $this->param('v') ?? 1);

        $genxslpath = UApp::config('genxslpath', UApp::$dataPath . '/xsl'); // Ahová a generált fájlok kerülnek
        if ($v) echo Ansi::color("Emptying `$genxslpath`...", 'green') . "\n";

        /** @noinspection PhpAssignmentInConditionInspection */
        if ($handle = opendir($genxslpath)) {
            $n = 0;
            $files = [];
            /** @noinspection PhpAssignmentInConditionInspection */
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    $ext = strtolower(substr(strrchr($entry, "."), 1));
                    if ($ext == 'xsl') {
                        unlink($genxslpath . '/' . $entry);
                        if ($v == 1) $files[] = $entry;
                        if ($v > 1) echo Ansi::color("File `$entry` is deleted", 'light blue') . "\n";
                        $n++;
                    }
                }
            }
            if ($v == 1) {
                $nx = $n > 3 ? $n - 3 : 0;
                $fx = '';
                for ($i = 0; $i < min($n, 3); $i++) {
                    if ($i) $fx .= ', ';
                    $fx .= "`$files[$i]`";
                }
                echo Ansi::color(($n ? "Files $fx" : 'No files') . ($nx ? " and $nx more" : '') . ' has been deleted', 'green') . "\n";
            } elseif ($v) {
                echo Ansi::color("$n files has been deleted", 'green') . "\n";
            }
            closedir($handle);
        }
    }
}
