<?php
/** @noinspection PhpUnused */

/**
 * # Class CreateCommand
 *
 * Usage in CLI: `php umxc create model tablename`
 *
 * @package uhi67\umxc\commands
 */
class CreateCommand extends UAppCommand {

    /**
     * Creates a php file of a new model class based on the given existing database table.
     * Model name will be the camelized version of teh table-name.
     *
     * @param array $params modelname
     * @option o
     * @throws ConfigurationException
     * @throws InternalException
     * @throws DOMException
     * @example create model o=1 tablename
     */
    public function actModel($params) {
        if (!isset($params[0])) {
            echo 'Missing table name';
            return;
        }
        $tableName = $params[0];
        $modelName = Util::camelize($tableName, true);
        $nameSpace = 'app\models';
        $fileName = UApp::config('apppath') . '/models/' . $modelName . '.php';
        if (class_exists($nameSpace . '\\' . $modelName) && !$this->param('o')) {
            echo "Model $modelName already exists, skipping\n";
            return;
        }

        $metadata = $this->db->getMetaData($tableName);
        $foreign_keys = $this->db->foreign_keys($tableName);
        $referrers = $this->db->referrers($tableName);
        $genPath = UApp::getPath() . '/def/gen';

        $xmldoc = new UXMLDoc('UTF-8', 'data');
        $xmldoc->documentElement->setAttribute('date', date('Y-m-d'));
        $node_table = $xmldoc->documentElement->addNode('table', [
            'name' => $tableName,
            'model-class' => $modelName,
            'namespace' => $nameSpace,
        ]);
        foreach ($metadata as $column => $data) {
            $node_table->addNode('column', array_merge($data, [
                'php-name' => Util::camelize($column),
                'php-type' => $this->db->mapType($data['type']),
                'label' => ucwords(str_replace('_', ' ', $column)),
            ]), $column);
        }
        if ($foreign_keys) foreach ($foreign_keys as $constraint => $data) {
            $column_name = $data['column_name'];
            $node_table->addNode('reference', array_merge($data, [
                'name' => substr($column_name, strlen($column_name) - 3) == '_id' ? substr($column_name, 0, -3) : $column_name . '1',
                'foreign_model' => Util::camelize($data['foreign_table'], true),
                'foreign_field' => Util::camelize($data['foreign_column']),
            ]), $constraint);
        }

        // referrers?
        if ($referrers) foreach ($referrers as $constraint => $data) {
            #$column_name = $data['column_name'];
            $node_table->addNode('referrer', array_merge($data, [
                'name' => Util::camelize($data['remote_table']),
                'remote_model' => Util::camelize($data['remote_table'], true),
                'remote_field' => Util::camelize($data['remote_column']),
            ]), $constraint);
        }

        $result = $xmldoc->process($genPath . '/model.xsl', null, true);
        // TODO: <!DOCTYPE eltüntetése
        file_put_contents($fileName, $result);
        echo "Model $modelName created succesfully\n";

    }
}
